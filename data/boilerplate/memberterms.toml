# Be sure to retain the first line, content = """, and the final """ after the text

content = """

<em>Updated June 2022</em>

These Crossref Terms of Membership (these "Terms") set forth the terms and conditions of membership in The Publishers International Linking Association, Inc. d/b/a Crossref ("Crossref"), a nonprofit corporation organized under the laws of New York, USA.

## Background

<div class="legal-lists">

<ol style="list-style-type: upper-alpha;">
    <li>Crossref is a not-for-profit membership organization that exists to make scholarly communications better. Its mission is to "make research outputs easy to find, cite, link, assess, and reuse." To that end, Crossref:
        <ul>
            <li>manages and maintains a database of information ("Metadata") that describes and identifies professional and scholarly materials and content (collectively, "Content") and persistent identifiers such as Digital Object Identifiers ("Identifiers") that point to or give context to the Content online;</li>
            <li>facilitates the deposit and retrieval of Metadata and Identifiers;</li>
            <li>enables linking among Content online through embedded reference citations; and</li>
            <li>offers other online information management tools.</li>
        </ul>
All of the above functions and offerings, including associated systems, hardware, software, and know-how, are referred to in these Terms as the "Crossref Infrastructure and Services."</li>
<li>Membership in Crossref is open to organizations that produce Content and otherwise meet the terms and conditions of membership established from time to time by Crossref, and to such other entities as Crossref may determine from time to time. Together with Crossref's <a href="/board-and-governance/incorporation-certificate/">Articles of Incorporation</a> and <a href="/board-and-governance/bylaws/">Bylaws</a> (collectively, the "Crossref Governing Documents"), these Terms govern membership in Crossref.</li>
<li>By submitting a membership application, the applicant agrees to be bound by these Terms and, upon Crossref's approval of that application, and receipt of the first annual membership fee, the applicant becomes a "Member."</li>
</ol>

</div>

## Terms

<div class="legal-lists">

<ol>
    <li><strong>Member's Rights</strong>. Subject to these Terms, the Crossref Governing Documents, and Crossref's policies and procedures as promulgated by Crossref's board and staff and made available on the Website (as defined below) from time to time, the Member shall:
        <ol style="list-style-type: lower-alpha;">
            <li>be entitled to use the Crossref Infrastructure and Services as set forth herein; and</li>
            <li>have the governance rights afforded to Members in the Crossref Governing Documents.</li>
        </ol>
    </li>
    <li><strong>Member's Obligations</strong>. As a condition of its membership, the Member shall comply with the provisions of these Terms, including this Section 2.
        <ol style="list-style-type: lower-alpha;">
            <li><em>Metadata Deposits</em>. The Member is responsible for depositing accurate Metadata for each Content item:
                <ul>
                    <li>produced by the Member, and/or</li>
                    <li>for which the Member otherwise has rights to cause such Content to be included in the Crossref Infrastructure and Services.</li>
                </ul>
            All Content described in the two bullet points above is referred to in these Terms as the Member's Content.</li>
            <li><em>Timely Metadata Deposits</em>. Prior to, or as soon as reasonably practicable after, online publication of the Member's Content, the Member shall deposit with Crossref the Metadata corresponding to such Content. All deposits of Metadata shall comply with Crossref's technical documentation and schemas, including fields, parameters and other metadata criteria, set forth from time to time in support and best practice documentation on Crossref's website (the "Website") and/or email notices.</li>
            <li><em>Rights to Content</em>. The Member will not deposit or register Metadata for any Content for which the Member does not have legal rights to do so.</li>
            <li><em>Registering Identifiers</em>. The Member shall assign an Identifier to each of its Content items, for registration within the Crossref Infrastructure and Services.</li>
            <li><em>Linking</em>. Promptly upon becoming a Member, the Member shall embed the appropriate Identifier(s) within each reference citation appearing in the Member's Content.</li>
            <li><em>Reference Linking</em>. Throughout the Term, the Member shall use best efforts to maximize linking through Identifiers to other Content within the Crossref Infrastructure and Services, known in these Terms as "Reference Linking". </li>
            <li><em>Display Identifiers</em>. With respect to each Identifier assigned to the Member's Content, the Member shall use commercially reasonable efforts to (i) display each Identifier in a location and format that comply with the <a href="https://doi.org/10.13003/5jchdy">Crossref Display Guidelines</a>, as updated on the Website from time to time (the "Display Guidelines"), and (ii) ensure each Identifier is hyperlinked so as to be citable.</li>
            <li><em>Maintaining and Updating Metadata</em>. The Member shall ensure that each Identifier assigned to the Member's Content continuously resolves to a response page (a "Response Page") containing, at a minimum, (i) complete bibliographic information about the corresponding Content (including the Identifier), visible on the initial page, with reasonably sufficient information detailing how the Content can be cited and accessed, and/or (ii) a hyperlink leading to the Content itself, in each case in accordance with the Display Guidelines. The Identifier shall serve as the permanent URL link to the Response Page. The Member shall register the Response Page URL with Crossref, keep it up-to-date and active, and promptly correct any errors or variances communicated to the Member by Crossref. The Member shall be exclusively responsible for maintaining the accuracy of data associated with each Identifier relating to the Member's Content, and the validity and operation of the corresponding URL(s) containing the Response Page, and related pages. Some examples of failures to maintain and update Metadata consistent with this Section 2(h) include: 1) publishing or communicating Identifiers without registering them with Crossref; 2) withdrawing content without posting a notification and updating the record's URL/metadata with Crossref; or 3) registering new Identifiers with the Member's own prefix for content that already had Identifiers registered by a prior publisher.</li>
            <li><em>Archives</em>. The Member shall use best efforts to contract with a third-party archive or other content host (an "Archive") (a list of which can be found <a href="https://keepers.issn.org/keepers">here</a>) for such Archive to preserve the Member's Content and, in the event that the Member ceases to host the Member's Content, to make such Content available for persistent linking. The Member hereby authorizes Crossref, solely in the event an Archive becomes the primary location of the Member's Content, to contract directly with such Archive for the purpose of ensuring the persistence of links to such Content. The Member agrees that, in the event that the Content permanently ceases to be maintained by the Member, Crossref is entitled to redirect Identifiers to an Archive or a "Defunct DOI" page hosted by Crossref.</li>
            <li><em>Content-Specific Obligations</em>. Should the Member choose to register different types of Content and Metadata, such as but not limited to journal articles, book chapters, datasets, conference proceedings, preprints, components, data, peer review reports, versions, or relations, the Member shall be bound by all obligations applicable to each specific <a href="/services/content-registration/">content type</a> as set forth on the Website from time to time.</li>
        </ol>
    </li>
    <li><strong>Fees</strong>. The Member shall pay the Fees described in this Section 3. These Terms refer to Annual Fees and Content Registration Fees collectively as "Fees."
        <ol style="list-style-type: lower-alpha;">
            <li><em>Annual Fee</em>. The Member is responsible to pay an annual membership fee (the "Annual Fee"). The <a href="/fees/#annual-membership">Annual Fee</a> for a Member's first year of membership is invoiced as a prorated amount for the Member's initial calendar year of membership, to be paid in full for membership. Thereafter, the Annual Fee is invoiced at the beginning of each calendar year. Payment terms are 45 days from the date of invoice.</li>
            <li><em>Content Registration Fees</em>. Crossref charges Members a Content Registration fee (collectively, "Content Registration Fees") to deposit content with Crossref, as more fully described on the Website from time to time. <a href="/fees/#content-registration">Content Registration Fees</a> are invoiced on a quarterly basis. Payment terms are 45 days from the date of invoice. </li>
            <li><em>Wire Transfer Fees</em>. The Member is responsible for any wire transfer fees and other ancillary costs incurred by Crossref that are associated with the Member's chosen payment methods.</li>
            <li><em>Fees for Optional Services</em>. From time to time Crossref charges Members other optional service fees for various optional services offered by Crossref, if and to the extent elected by the Member. These are set forth on the Website and updated from time to time. </li>
        </ol>
    </li>
    <li><strong>Intellectual Property Rights</strong>.
        <ol style="list-style-type: lower-alpha;">
            <li><em>General License</em>. Subject to these Terms, the Member hereby grants to Crossref and its agents a fully-paid, non-exclusive, worldwide license for any and all rights necessary to use, reproduce, transmit, distribute, display and sublicense Metadata and Identifiers corresponding to the Member's Content, in the reasonable discretion of Crossref in connection with the Crossref Infrastructure and Services, including all aspects of Reference Linking and Crossref's various other service offerings.</li>
            <li><em>Metadata Rights and Limitations</em>. Except as set forth herein and without limiting Section 4(a) above, Crossref shall not use, or acquire or retain any rights in the deposited Metadata of a Member. Nothing in these Terms gives a Member any rights (including copyrights, database compilation rights, trademarks, trade names, and other intellectual property rights, currently in existence or later developed) to any Metadata belonging to another Member. </li>
            <li><em>Crossref Intellectual Property</em>. The Member acknowledges that, as between itself and Crossref, Crossref has all right, title and interest in and to the Crossref Infrastructure and Services, including all related copyrights, database compilation rights, trademarks, trade names, and other intellectual property rights, currently in existence or later developed, with the exception of rights in the deposited Metadata as set forth in Section 4(b) or expressly provided elsewhere in writing. The Member shall not delete or modify any of Crossref's logos or notices of intellectual property rights on documents, online text or interfaces made available by Crossref.</li>
        </ol>
    </li>
    <li><strong>Distribution of Metadata by Crossref</strong>. Without limiting the provisions of Section 4 above, the Member acknowledges and agrees that all Metadata and Identifiers registered with Crossref are made available for reuse without restriction through (but not limited to) public APIs and search interfaces, which enhances discoverability of Content. Metadata and Identifiers may also be licensed to third party subscribers along with an agreement for Crossref to provide third parties with certain higher levels of support and service.</li>
    <li><strong>Use of Marks</strong>. Crossref may use the Member's name(s) and mark(s) to identify the Member's status as a member of Crossref. The Member may identify itself as a Crossref member by placing the Crossref mark or Crossref badges (without modification) on its website, by <a href="/brand/">referencing the code provided</a> on the Website. The Member may also identify use of Crossref Identifiers and Metadata, for example within reference lists, using the label "Crossref."</li>
    <li><strong>Maintenance of the Crossref Infrastructure and Services</strong>. Crossref shall use commercially reasonable efforts to maintain the Crossref Infrastructure and Services and to make it continually available for use by Members. </li>
    <li><strong>Term</strong>. These Terms shall remain in effect until and unless superseded by updated Crossref Terms of Membership amended as set forth in Section 18 below.</li>
    <li><strong>Termination of Membership; Effect</strong>.
        <ol style="list-style-type: lower-alpha;">
            <li><em>Termination of Membership</em>. A Member's Crossref membership may be terminated:
                <ol style="list-style-type: lower-roman;">
                    <li>By the Member for convenience upon written notice to Crossref;</li>
                    <li>By the Member for cause (1) in the event of Crossref's material breach of these Terms, which breach remains uncured following 45 days' notice from the Member to Crossref (or is by its nature incapable of cure) or (2) in the event Crossref provides notice of a material amendment to these Terms pursuant to the provisions of Section 18 hereof, and the Member provides notice to Crossref within 60 days of such notice of the Member's objection to such amendment and its intention to terminate; and</li>
                    <li>By Crossref upon written notice to the Member, in accordance with the Crossref Governing Documents, including for (1) a misrepresentation in the Member's membership application; (2) legal sanctions or judgments against the Member or its home country; (3) fraudulent use of Identifiers or Metadata; (4) failure to pay Fees due, which failure persists for 120 or more days following the initial invoice therefor; or (5) any other basis set forth in the Crossref Governing Documents.</li>
                </ol>
            </li>
            <li><em>Review of Termination of Membership</em>. Except where termination is on account of nonpayment of fees, the Executive Committee of Crossref's board shall review and ratify any Crossref decision to permanently terminate a Member's membership or any significant membership benefit (e.g., blocking access to or removing significant amounts of Metadata for multiple items of Content for an extended period), within 10 days of such decision. As part of this review, the Member will have an opportunity to be heard under such reasonable procedures as the board may determine in its good faith. Crossref or the Member may petition the Executive Committee to review and ratify any Crossref decision temporarily restricting the Member's access to or use of the Crossref Infrastructure and Services for a limited period, and the Executive Committee shall determine in its sole discretion whether to conduct such a review. </li>
            <li><em>Effect of Termination of Membership</em>. An outgoing Member shall not be entitled to a refund of any Fees that have been paid or waiver of any Fees that have accrued, except that a Member will be entitled to a refund of any prepaid fees representing the remaining portion of the then-current term of such Member's membership in the event of a termination for cause pursuant to Section 9(a)(ii) above. Termination of Membership shall have no adverse effect on Crossref's intellectual property rights in any Metadata or upon any related licenses then in effect. Following termination of its membership, an outgoing Member shall have no further obligation to deposit Metadata with Crossref or to assign Identifiers to its Content, and Crossref shall have no further obligation to register such Identifiers. With respect to Metadata deposited and Identifiers registered prior to such termination: (i) Crossref shall have the right to keep, maintain and use such Metadata and Identifiers within the Crossref Infrastructure and Services; and (ii) the obligations of the Member set forth in Sections 2(h) (i), and (j) of these Terms will survive. </li>
        </ol>
    </li>
    <li><strong>Enforcement</strong>. Crossref shall take reasonable steps to enforce these Terms, provided that Crossref shall not be obligated to take any action with respect to any Metadata that is the subject of an intellectual property dispute, but reserves the right, in its sole discretion, to remove or suspend access from, to or through such Metadata and/or its associated Content or to take any other action it deems appropriate. </li>
    <li><strong>Governing Law</strong>. These Terms shall be interpreted, governed and enforced under the laws of New York, USA, without regard to its conflict of law rules. All claims, disputes and actions of any kind arising out of or relating to these Terms shall be settled in Boston, Massachusetts, USA.</li>
    <li><strong>Disputes</strong>.
        <ol style="list-style-type: lower-alpha;">
            <li><em>Alternative Dispute Resolution</em>. The Member shall promptly notify Crossref of any claim, dispute or action, whether against other Members or Crossref, related to these Terms or any Identifiers or Metadata. Pursuant to the Commercial Arbitration Rules of the American Arbitration Association, a single arbitrator reasonably familiar with the publishing (including online publishing) and internet industries shall settle all claims, disputes or actions of any kind arising from or relating to the subject matter of these Terms between Crossref and the Member. The decision of the arbitrator shall be final and binding on the parties, and may be enforced in any court of competent jurisdiction.</li>
            <li><em>Injunctive Relief</em>. Notwithstanding Section 12(a), no party shall be prevented from seeking injunctive or preliminary relief in anticipation, but not in any way in limitation, of arbitration. The Member acknowledges that the unauthorized deposit or use of Metadata would cause irreparable harm to Crossref, the Crossref Infrastructure and Services, and/or other Members, that could not be compensated by monetary damages. The Member therefore agrees that Crossref may seek injunctive relief to remedy any actual or threatened unauthorized deposit or use of Metadata.</li>
        </ol>
    </li>
    <li><strong>Indemnification</strong>. To the extent authorized by law, the Member agrees to indemnify and hold harmless Crossref, its representatives, and their respective directors, officers and employees, from and against any and all liability, damage, loss, cost or expense, including reasonable attorney fees, costs, and other expenses, to the extent arising from or resulting from such Member's or its agent's or representative's acts or omissions, breach of these Terms, or violation of any third-party intellectual property right.</li>
    <li><strong>Limitations of Liability</strong>. <strong>NEITHER PARTY SHALL BE LIABLE TO THE OTHER FOR ANY INDIRECT, SPECIAL, INCIDENTAL, PUNITIVE, CONSEQUENTIAL DAMAGES OR LOST PROFITS ARISING FROM OR RELATING TO THESE TERMS OR THE CROSSREF INFRASTRUCTURE AND SERVICES, EVEN IF IT HAS BEEN INFORMED IN ADVANCE OF THE POSSIBILITY OF SUCH DAMAGES. NEITHER PARTY SHALL BE LIABLE TO THE OTHER FOR (I) ANY LOSS, CORRUPTION OR DELAY OF DATA OR (II) ANY LOSS, CORRUPTION OR DELAY OF COMMUNICATIONS WITH OR CONNECTION TO ANY CROSSREF SERVICE OR ANY CONTENT.</strong></li>
    <li><strong>Taxes</strong>. The Member is responsible for all sales and use taxes imposed, if any, with respect to the services rendered or products provided to the Member hereunder, other than taxes based upon or credited against Crossref's income.</li>
    <li><strong>Other Terms</strong>.
        <ol style="list-style-type: lower-alpha;">
            <li><em>Independent Contractors</em>. These Terms will not create or be deemed to create any agency, partnership, employment relationship, or joint venture between Crossref and any Member. The Member shall not have any right, power or authority to enter into any agreement for or on behalf of, or incur any obligation or liability of, or to otherwise bind, Crossref.</li>
            <li><em>No Third-Party Beneficiaries</em>. Except to the extent expressly set forth herein, neither party intends that these Terms shall benefit, or create any right or cause of action in or on behalf of, any person or entity other than Crossref and the Member.</li>
            <li><em>No Assignment</em>. A Member may not assign, subcontract or sublicense these Terms without the prior written consent of Crossref, and any attempted assignment in violation of the foregoing shall be void.</li>
            <li><em>Notices</em>. Written notice under these Terms shall be given as follows:
                <ol style="list-style-type: lower-roman;">
                    <li>If to Crossref: by emailing <a href="mailto:member@crossref.org">member@crossref.org</a> addressing Mr. Edward Pentz, Executive Director.</li>
                    <li>If to a Member: To the name and email address designated by the Member as the Primary Contact (previously "Business Contact") in such Member's membership application. This information may be changed by the Member by giving notice to Crossref by email at member@crossref.org. The Member shall also designate a technical, business, voting, billing, and metadata quality contact, and advise Crossref of any changes to such information.</li>
                </ol>
            </li>
            <li><em>Survival</em>. Sections (and the corresponding subsections, if any) 2(g), (h), and (i), 4, 9, 10, 11, 12, 13, 14, and 16, and any other provisions that by their express terms or nature survive, and any rights to payment, shall survive the expiration or termination of these Terms.</li>
            <li><em>Headings</em>. The headings of the sections and subsections used in these Terms are included for convenience only and are not to be used in construing or interpreting these Terms. </li>
            <li><em>Severability</em>. If any provision of these Terms (or any portion thereof) is determined to be invalid or unenforceable, the remaining provisions of these Terms will not be affected thereby and will be binding upon the parties and will be enforceable, as though said invalid or unenforceable provision (or portion thereof) were not contained in these Terms.</li>
        </ol>
    </li>
    <li><strong>Entire Agreement</strong>. These Terms, together with any Addenda of Terms executed between Crossref and a Member, constitute and contain the entire agreement between Crossref and such Member with respect to the subject matter hereof, and supersede any prior or contemporaneous oral or written agreements. The "Background" section at the beginning of these Terms forms a part of these Terms and is incorporated by reference herein. </li>
    <li><strong>Amendment</strong>. These Terms may be amended by Crossref, via updated Terms posted on the Website and emailed to each Member no fewer than sixty (60) days prior to effectiveness. By using the Crossref Infrastructure and Services after the effective date of any such amendment hereto, the Member accepts the amended Terms. These Terms may also be amended by mutual agreement of a given Member and Crossref by execution of an Addendum of Terms.</li>
    <li><strong>Data Privacy</strong>. By providing Crossref with personal data which was provided to the Member by a natural person(s), including Member staff (the "origin party"), the Member guarantees that:
        <ol style="list-style-type: lower-alpha;">
            <li>the Member collected and processed the data in accordance with applicable law, including the General Data Protection Regulation;</li>
            <li>the Member acquired the origin party's informed consent to share the data with Crossref;</li>
            <li>the Member acquired the origin party's consent for the data to be transferred to the United States for processing.</li>
        </ol>
    The Member further agrees that it will maintain appropriate mechanisms to ensure that it will provide natural person(s) whose personal data it provides to Crossref with a means to have access to, to correct, and to delete such data and understands that the burden is on the Member to communicate such corrections or deletions to Crossref.<br /><br />
    Crossref's Privacy Policy is located <a href="/operations-and-sustainability/privacy">here</a>.</li>
    <li><strong>Compliance</strong>. Each of the Member and Crossref shall perform under this Agreement in compliance with all laws, rules, and regulations of any jurisdiction which is or may be applicable to its business and activities, including anti-corruption, copyright, privacy, and data protection laws, rules, and regulations.<br /><br />
    The Member warrants that neither it nor any of its affiliates, officers, directors, employees, or members is (i) a person whose name appears on the list of Specially Designated Nationals and Blocked Persons published by the Office of Foreign Assets Control, U.S. Department of Treasury ("OFAC"), (ii) a department, agency or instrumentality of, or is otherwise controlled by or acting on behalf of, directly or indirectly, any such person; (iii) a department, agency, or instrumentality of the government of a country subject to comprehensive U.S. economic sanctions administered by OFAC; or (iv) is subject to sanctions by the United Nations, the United Kingdom, or the European Union.</li>
</ol>

</div>"""
