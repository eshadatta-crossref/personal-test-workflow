## About

Hugo theme based on [Start Bootstrap Clean Blog](http://startbootstrap.com/template-overviews/clean-blog/).

## Dev Setup

### On cazincdev

Running compass watch to keep the scss conversion happening
Running hugo to keep building the website on changes

### Self Hosted

Install Hugo as per the instructions at https://gohugo.io/overview/installing/
Use compass watch to build the sass stylesheets into regular .css
Run hugo to keep building on change

## Normal Dev
git pull to your dev of choice
make changes - probably in the theme
hugo should build and you can see your updates happen (at xref.cazincdev.com/xref/)
rinse and repeat
once you are happy, add your changes `git add -A`
commit with a reasonable message eg `git commit -m 'more navigation stylings'`
push to the repo `git push`
if other changes have happened then do a `git pull` and make good any merges if you have to and `git push` again

github changes should then be reflected on testweb.crossref.org within a minute

## Most likely areas of change
layouts
partials
static (js and scss) - DO NOT EDIT .css files directly - they are generated by a scss parser.






