var visited = function ($) {

  function fnCheck() {
    var $dismissedJoin = Cookies.get('dismissedjoin');
    if (!$dismissedJoin) {
      $("#myModal").modal({
        //backdrop: 'static',
        //keyboard: false
      })
    }
    //Cookies.set('dismissedjoin', true, { expires: 1 });
  }

  function init() {
    fnCheck();
  }

  return {
    init: init
  };
}(jQuery);



var joinflow = function ($) {

  var $metaDataSelectValue_var;

  function fnPanelConditionals() {
    /**
    * Moves user straight to Q4
    * @param $currentQuestion
    */
    function moveToQ4($currentQuestion) {
      var $parentSection = $currentQuestion.closest('.section'),
      $q4 = $('.section-2');

      $parentSection.hide();
      $q4.show().scrollTo(500, 'linear');
    }

    /**
    * If conditional is false, move user through the "JoinFlow" normally
    * @param $currentQuestion
    */
    function proceedToNextQuestion($currentQuestion) {
      var $current = $currentQuestion.closest('.jf-question'),
      $next = $current.next();

      $next.show().scrollTo(500, 'linear');
    }

    /**
    * Hides element on click
    * @param $selector
    */
    function hideQuestion($selector) {
      $('.' + $selector).hide();
    }

    /**
    * Makes hidden elements visible again
    * @param $selector
    */
    function showQuestion($selector) {
      $('.' + $selector).show();
    }

    /**
    * Moves user breadcrumb to signify that we are on the next section
    */
    function moveBreadCrumb() {

      var $container = $('.triangular-steps'),
      $item = $container.find('li:first-child'),
      $next = $item.next();

      $item.removeClass('active').addClass('completed');
      $next.addClass('active');

    }

    //Check metadata value, then skip to Q4 if true
    var $metaDataValue = $("[name='11_whattodo']"),
    $publishOwnValue = $("[name='12_publishown']"),
    $nq = $('.next-question-nq');





    //Check metadata value, then skip to Q4 if true
    var $agreement = $("#agreement-modal"),
    $nq = $('.next-question-nq');


    $agreement.on('click', function () {
      var $this = $(this);

      
        visited.init();
        $nq.off('click').click(function () {
          var $agreementCheck = $("#agreement-modal");
          
          if ($agreementCheck.is(':checked')) {
            //console.log("Agreement checked OK");
            var submitBtn = $('.jf-sumbit-form');
            submitBtn.removeClass('disabled');  
            proceedToNextQuestion($this);

          } else {
            //console.log("Agreement not checked");
            sweetAlert("Member Terms", $("#question_1_popup").text(),  "error");
            visited.init();
          }
        });
    });




    //funder id is known

    var $funderid_unknown = $("#funderid_unknown");
    var $funderid_known = $("#funderid_known");
    $funderid_unknown.on('change', function () {
      console.log("funderid_unknown has changed" + $funderid_unknown.val());
      if ($funderid_unknown.val() == 'No') {
        console.log("don't show question ");
        $('#havefundertrue').hide();
      } 
    });

    $funderid_known.on('change', function () {
      console.log("funderid_known has changed" + $funderid_known.val());
      if ($funderid_known.val() == 'Yes') {
        console.log("need to show slot for funder id ");
        $('#havefundertrue').show();
      } 
    });



    var $award_or_publish = $("#select_award_or_publish");
    $award_or_publish.on('change', function () {
      console.log("award_or_publish has changed" + $award_or_publish.val());

      if ($award_or_publish.val() == '1') { 

        $("#short-revenue-list").show();
        $("#short-revenue-question").show();
        $("#short-revenue-info-para").show();
        $("#membership_org_revenue_short").show();

        $("#full-revenue-list").hide();
        $("#full-revenue-question").hide();
        $("#full-revenue-info-para").hide();
        $("#membership_org_revenue_full").hide();
        $("#full-revenue-question-lower").hide();
        $('#funderidwrapper').show();

      } else if ($award_or_publish.val() == '2') {
        $("#short-revenue-list").hide();
        $("#short-revenue-question").hide();
        $("#short-revenue-info-para").hide();
        $("#membership_org_revenue_full").show();

        $("#full-revenue-list").show();
        $("#full-revenue-question-lower").show();
        $("#full-revenue-info-para").show();
        $("#membership_org_revenue_short").hide();

        $('#funderidwrapper').hide();

      } else {
        $("#full-revenue-list").hide();
        $("#full-revenue-question").hide();
        $("#full-revenue-info-para").hide();
        $("#membership_org_revenue_full").hide();
        $("#short-revenue-list").hide();
        $("#short-revenue-question").hide();
        $("#short-revenue-info-para").hide();
        $("#membership_org_revenue_short").hide();
        $("#full-revenue-question-lower").hide();
        $('#funderidwrapper').show();

      }
    });


    var $org_type = $("#organisation_type");

    $org_type.on('change', function () {
      console.log("org type has changed" + $org_type.val());
      

      // if funder then show next question
      // changed for multiselect
      //if ($org_type.val() == 'Research funder') {  

      if($.inArray("Research funder",$org_type.val()) != -1) {
        // show the 2 specifics for this route      
        $("#pre-funder-revenue-question").show();
        $("#award-or-publish").show();

        // hide the 3 standards for now
        $("#full-revenue-list").hide();
        $("#full-revenue-question").hide();
        $("#full-revenue-info-para").hide();
        $(".awardblock").show();
      } else {
        // clear selected values in fields?
        $(".awardblock").hide();
        $("#pre-funder-revenue-question").hide();
        $("#award-or-publish").hide();
        $("#short-revenue-list").hide();
        $("#short-revenue-question").hide();
        $("#short-revenue-info-para").hide();

        $("#full-revenue-list").show();
        $("#full-revenue-question").show();
        $("#full-revenue-info-para").show();

        // these are the ones in the summary
        $("#membership_org_revenue_full").show();
        $("#membership_org_revenue_short").hide();
        
        $("#full-revenue-question-lower").hide();

      }

      /*if ($org_type.val() == 'Research funder') {
        console.log("need to show alt list");

        $("#short-revenue-list").show();
        $("#short-revenue-question").show();
        $("#short-revenue-info-para").show();
        $("#membership_org_revenue_short").show();

        $("#full-revenue-list").hide();
        $("#full-revenue-question").hide();
        $("#full-revenue-info-para").hide();
        $("#membership_org_revenue_full").hide();
      } else {
        $("#short-revenue-list").hide();
        $("#short-revenue-question").hide();
        $("#short-revenue-info-para").hide();
        $("#membership_org_revenue_full").show();

        $("#full-revenue-list").show();
        $("#full-revenue-question").show();
        $("#full-revenue-info-para").show();
        $("#membership_org_revenue_short").hide();
      }*/



      if($.inArray("Other",$org_type.val()) != -1) {
        $("#org_type_other_block").show();
      } else {
        $("#org_type_other_block").hide();
      }
      
    });

    var $pubown = $('input[name=12_publishown]');
    $pubown.on('change', function () {
      $pubown_val = $('input[name=12_publishown]:checked').val();
      if ($pubown_val == '3') {
              $("#voting-contact").hide();
              //$(".jf-question-2").hide();
            } else {
              $("#voting-contact").show();
              //$(".jf-question-2").show();
            }
    });


    //show_platform_list - id of yes

    var $show_platforms = $("#show_platform_list");
    var $hide_platforms = $("#hide_platform_list");
    $show_platforms.on('change', function () {
      console.log("platform_radio has changed" + $show_platforms.val());
      if ($show_platforms.val() == 'yes') {
        console.log("need to show platforms ");
        $("#platform").show();
      } 
    });

    $hide_platforms.on('change', function () {
      console.log("platform_radio has changed" + $hide_platforms.val());
      if ($hide_platforms.val() == 'no') {
        console.log("need to hide platforms ");
        $("#platform").hide();
      } 
    });



    var $platform = $("#registration_platform");

    $platform.on('change', function () {
      console.log("org type has changed" + $platform.val());
      if ($platform.val() == 'Other (please specify)') {
        console.log("need to show extra box ");
        $("#8_extra").show();
      } else {
        $("#8_extra").hide();
      }
    });







    var $nbtn = $agreement.closest('.section').find('.next-section');
    $nbtn.removeClass('disabled');
 
  }

  function fnPopulateDataForContacts() {


    var checkbox_tech = $('#same_as_technical_contact'),
    checkbox_business = $('#same_as_business_contact'),
    checkbox_same_postal = $('#same_as_postal_address');

    /**
    * This fills the conatct contact fields if the checkbox is selected, nullifies them if unchecked
    * @param $selector
    * @param $identifier
    * @param $filler
    */
    function fillFormOnClick($selector, $identifier, $filler) {

      var blank = "";

      $selector.on('change', function () {

        var $this = $(this);

        if ($this.is(':checked')) {

          var firstname = $('#' + $identifier + '_first_name').val(),
          lastname = $('#' + $identifier + '_last_name').val(),
          email = $('#' + $identifier + '_email').val();

          $('#' + $filler + '_first_name').val(firstname);
          $('#' + $filler + '_last_name').val(lastname);
          $('#' + $filler + '_email').val(email);


        } else {

          $('#' + $filler + '_first_name').val(blank);
          $('#' + $filler + '_last_name').val(blank);
          $('#' + $filler + '_email').val(blank);
        }

      });
    }

    function fillAddressFormOnClick($selector, $identifier, $filler) {

      var blank = "";

      $selector.on('change', function () {

        var $this = $(this);

        if ($this.is(':checked')) {

          var street1 = $('#' + $identifier + '_street_1').val(),
          street2 = $('#' + $identifier + '_street_2').val(),
          city = $('#' + $identifier + '_city').val(),
          state = $('#' + $identifier + '_state').val(),
          postal_code = $('#' + $identifier + '_postal_code').val(),
          country = $('#' + $identifier + '_country option:selected').val();
          
          console.log("country from source=" + country);

          $('#' + $filler + '_street_1').val(street1);
          $('#' + $filler + '_street_2').val(street2);
          $('#' + $filler + '_city').val(city);
          $('#' + $filler + '_state').val(state);
          $('#' + $filler + '_postal_code').val(postal_code);


          // remove any other selected
          $('#' + $filler + '_country option').each(
              function() {
                  $(this).removeAttr('selected');
                  //console.log('ici');
              }
          ); 
          // select the right one


          //$('input[name="23_billing_country"]').val("Greece");
          //$('#' + $filler + '_country option[value="' + country + '"]').prop('selected',true);
          //$('.typeahead_billing').data("combobox").refresh();

          $('#' + $filler + '_country').val(country);
          $('#' + $filler + '_country').combobox("refresh");
          $('#' + $filler + '_country').combobox("triggerChange");




        } else {
          $('#' + $filler + '_street_1').val(blank);
          $('#' + $filler + '_street_2').val(blank);
          $('#' + $filler + '_city').val(blank);
          $('#' + $filler + '_state').val(blank);
          $('#' + $filler + '_postal_code').val(blank);  

          // remove any other selected
          $('#' + $filler + '_country option').each(
              function() {
                  $(this).removeAttr('selected');
                  //console.log('ici');
              }
          );    
          $('#' + $filler + '_country option[value="none"]').prop('selected','selected');
        }


        console.log("country = " + $('#' + $filler + '_country').val());

      });
    }



    fillFormOnClick(checkbox_business, 'contact_1', 'contact_3');
    fillFormOnClick(checkbox_tech, 'contact_4', 'contact_6');
    fillAddressFormOnClick(checkbox_same_postal, 'postal', 'billing');
  }

  function fnEditEntry() {
    var $container = $('.jf-membership-table'),
    $btns = $container.find('.jf-btn-small');


    $btns.each(function () {

      var $this = $(this);

      $this.click(function () {

        $this = $(this);

        var $parent = $this.data('parent'),
        $current = $this.closest('.section'),
        $section = $($parent).closest('.section'),
        $fieldId = $this.data('id');

        //console.log($parent);

        $current.hide();
        $section.show(function () {

          var $header = $($fieldId).closest('.jf-question').find('.jf-question-header');

          //console.log($header);

          $header.scrollTo(600, 'linear');
          $($fieldId).focus();
        });
      });
    });
  }

  function fnOpenByHash() {

    var $requestedHash = ((window.location.hash.substring(1).split("#", 1)) + "?").split("?", 1),
    $hashToOpen = $('#' + $requestedHash),
    $stepNavContainer = $('.join-steps-container'),
    $currentPanel = $('.section');

    //console.log($hashToOpen);

    if ($hashToOpen.length) {
      // Get panel header element
      var $requestedPanel = $('.section[data-id=' + $requestedHash + ']');

      //console.log($requestedPanel);

      if ($requestedPanel.length) {

        // Hide all panels if they are visible
        if ($currentPanel.is(':visible')) {
          $currentPanel.hide();
        }

        // Show requested panel
        $requestedPanel.show();
      }
    }
  }

  function fnClickableBreadcrumbNav() {
    var $container = $('.triangular-steps'),
    $el = $container.find('li'),
    $flag = false;

    $el.click(function (e) {
      var $this = $(this);
      e.preventDefault();

      if ($this.hasClass('completed')) {


        var $id = $this.attr('id'),
        $sectionVisible = $('.section:visible'),
        $next = $('.' + $id),
        $currentSectionId = $sectionVisible.data('id'),
        $currentSection = $('.' + $currentSectionId);

        var $addCompletedClass = Cookies.get('endOfJoinFlow');

        $this.removeClass('completed').addClass('active');
        $('#' + $currentSectionId).removeClass('active');

        if ($currentSectionId == 'your-membership') {
          $flag = true;
          Cookies.set('endOfJoinFlow', true);
        }

        if ($addCompletedClass) {
          $el.each(function () {
            if (!$(this).hasClass('active')) {
              $(this).addClass('completed');
            }
          });
        }

        $currentSection.hide();
        $next.show();
        $container.parent().scrollTo(400, 'linear');

      }
    });

    if (!$flag) {
      Cookies.set('endOfJoinFlow', false);
    }
  }

  function fnEnableQuestionIfFilled() {

    var $container = $(".jf-question"),
    $joinFlowForm = $('#WebToLeadForm'),
    $el = $container.find('input, select');

    $el.each(function () {
      var $this = $(this),
      $type = $this.attr('type');

      switch ($type) {
        case 'checkbox':
        case 'radio':
        var $this = $(this),
        $question = $this.closest('.jf-question');

        if ($this.is(':checked')) {

          $question.show();
        }
        break;
        case 'text':
        case 'url':
        case 'email':
        var $this = $(this),
        $question = $this.closest('.jf-question');

        if ($this.val().length > 0) {
          $question.show();
        }
        break;
        default:
        var $this = $(this),
        $btnContainer = $this.closest('.section').find('.btn-container'),
        $question = $this.closest('.jf-question'),
        $btn = $btnContainer.find('.next-section');


        if ($this.val()) {
          $question.show();

          if ($joinFlowForm.valid()) {
            $btn.removeClass('disabled');
          }

        }
      }
    });
  }

  function fnEnableNextSectionBtn() {

    var $agreement = $('#agreement-modal'),


    $nq = $('.next-question-nq');


    joinFlowForm = $('#WebToLeadForm');

    var $agreementContainer = $agreement.closest('.section').find('.jf-accordion-step-4'),
    $btn = $agreement.closest('.section').find('.jf-sumbit-form'),
    $finalNb = $('.final-n-b');

    $finalNb.click(function () {
      if ($agreementContainer.is(':hidden')) {
        $btn.removeClass('disabled');
      }
    });
    
    visited.init();
    $nq.off('click').click(function () {
      var $agreementCheck = $("#agreement-modal");
      
      if ($agreementCheck.is(':checked')) {
        //console.log("Agreement checked OK");
        var submitBtn = $('.jf-sumbit-form');
        submitBtn.removeClass('disabled');  
        proceedToNextQuestion($this);
      } else {
        //console.log("Agreement not checked");
        sweetAlert("Member Terms", $("#question_1_popup").text(),  "error");
        visited.init();
      }
    });

    var submitBtn = $('.jf-sumbit-form');

    submitBtn.click(function () {
      fnFillReplyToData();
      fnFillIDACountry();
	  // Submit user agent with form so we can sgather more info for those that have issues
	  document.getElementById('UserAgent').value = navigator.userAgent;
      var $this = $(this);
      if ($this.hasClass('disabled')) {
        sweetAlert("One last thing...", "Please agree to the member guidelines.", "error");
      } else {
        console.log("Submitting form");
        console.log("ReplyTo definitely set to " + $('#special-replyto').val() );

        //alert("not submitting for testing");
        joinFlowForm.submit();
      }
    });
  }


  function fnPanelNav() {


    var joinFlowForm = $('#WebToLeadForm');

    var $pr = $('input:radio[name="13_publishrep"]'),
    $next = $pr.closest('.section').find('.btn-container');

    $pr.on('change', function () {
      if ($pr.is(":checked")) {
        if (joinFlowForm.valid()) {
          $next.fadeIn();
        }
      } else {
        $next.hide();
      }
    });

    if ($pr.is(":checked")) {
      if (joinFlowForm.valid()) {
        $next.fadeIn();
      }
    } else {
      $next.hide();
    }

    var organisation_revenue = $('#organisation_revenue'),
    showFinal = $('.show-final');

    showFinal.click(function () {
      $next = organisation_revenue.closest('.section').find('.btn-container');
      $next.show();
    });
    // organisation_revenue.on('change', function () {
    //
    //     var $this = $(this),
    //
    //         $next = $this.closest('.section').find('.btn-container');
    //
    //
    //     console.log($next);
    //
    //
    //     if (joinFlowForm.valid()) {
    //
    //         $next.fadeIn();
    //
    //     } else {
    //         $next.hide();
    //     }
    // });


    // var $nextBtn = organisation_revenue.closest('.section').find('.btn-container'),
    //     $finalnb = $('.final-n-b');
    //
    // $finalnb.click(function () {
    //     if ($form.valid()) {
    //         $next.show().scrollTo(500, 'linear');
    //     }
    // });
  }

  function fnScrollNextSection() {

    var section = $('.section');

    section.each(function () {

      var $this = $(this),
      $nextBtn = $this.find('.next-section'),
      $prevBtn = $this.find('.prev-section'),
      $stepNavContainer = $('.join-steps-container'),
      $stepNav = $('.triangular-steps');

      function progressBarForward(current) {

        var $currentSectionId = current.data('id'),
        $nextSectionId = current.next().data('id');


        console.log('current section = ' + $currentSectionId);
        if ($nextSectionId != 'your-metadata') {
          $(".firstpageonly").hide();
          console.log('hiding header');
        } else {
          $(".firstpageonly").show();
          console.log('showing header');
        }

        $stepNav.find('#' + $currentSectionId).removeClass('active').addClass('completed');
        $stepNav.find('#' + $nextSectionId).addClass('active');

      }

      function progressBarBack(current) {

        var $currentSectionId = current.data('id'),
        $prevSectionId = current.prev().data('id');

        $stepNav.find('#' + $currentSectionId).removeClass('active');
        $stepNav.find('#' + $prevSectionId).addClass('active').removeClass('completed');

      }

      $nextBtn.click(function (e) {

        var $this = $(this),
        $current = $this.closest('.section'),
        joinFlowForm = $('#WebToLeadForm'),
        $next = $this.closest('.section').next(),
        $nextId = $next.data('id');

        if(joinFlowForm.valid()) {
          if ($this.hasClass('disabled')) {

            e.preventDefault();
            sweetAlert("Oops...", "Please answer the questions to proceed.", "error");
            joinFlowForm.valid();

          } else {

            e.preventDefault();
            $current.hide();
            $next.show();
            $stepNavContainer.scrollTo(400, 'linear');

            progressBarForward($current);

          }
        }
      });

      $prevBtn.click(function (e) {

        e.preventDefault();

        var $this = $(this),
        $current = $this.closest('.section'),
        $prev = $this.closest('.section').prev();

        $current.hide();
        $prev.show();
        $stepNavContainer.scrollTo(400, 'linear');

        progressBarBack($current);
      });
    });

  }

  function fnValidateJoinFLow() {

    var joinFlowForm = $('#WebToLeadForm'),
    blacklist = ['Cuba', 'Iran, Islamic Republic of', 'Korea, Democratic People\'s Republic of', 'Syrian Arab Republic'];


    //Disable enter key if form isn't valid
    function validationFunction() {
      if (joinFlowForm.valid()) {
        console.log("trace 3");
        return true;
      }
    }

    $(window).keydown(function (e) {
      if ((e.keyCode == 13) && (validationFunction() == false)) {
        e.preventDefault();
        return false;
      }
    });


    country = $('#postal_country');
    country.on('change', function () {
    countryVal = $('#postal_country').val();
    var $OFACfield = $('#OFAC_country');
      if($.inArray(countryVal, blacklist) != -1) {
        console.log("country on the OFAC list");
        $("<label id='postal_country-error' class='error-ignored' for='postal_country'>It looks like you're in an OFAC-sanctioned country. Please do click 'Next' to continue your application and 'submit', then we'll get back to you with more information.</label>").insertAfter( "#postal_country" );
        $OFACfield.val("OFAC message displayed");
        console.log("OFAC message displayed");
      } else {
        $('#postal_country-error').remove();        
        $OFACfield.val("OFAC message not required");
        console.log("OFAC message not required");
      }
    });

    billing_country = $('#billing_country');
    billing_country.on('change', function () {
      billing_countryVal = $('#billing_country').val();
      if($.inArray(billing_countryVal, blacklist) != -1) {
        console.log("billing_country on the OFAC list");
        $("<label id='billing_country-error' class='error-ignored' for='billing_country'>It looks like you're in an OFAC-sanctioned country. Please do click 'Next' to continue your application and 'submit', then we'll get back to you with more information.</label>").insertAfter( "#billing_country" );
        //$OFACfield.val("OFAC message displayed");
        console.log("OFAC message displayed");
      } else {
        $('#billing_country-error').remove();        
        //$OFACfield.val("OFAC message not required");
        console.log("OFAC message not required");
      }
    });


    //Checks for duplicate email addresses in the contacts section.
    //This now checks for 3 unique emails
    $.validator.addMethod("atleastthree", function (value, element, options) {

      // get all the elements passed here with the same class
      var elems = $(element).parents('form').find(options[0]);

      // the value of the current element
      var valueToCompare = value;

      // count
      var matchesFound = 0;

      // loop each element and compare its value with the current value
      // and increase the count every time we find one
      // not any more - check 3 unique out of all emails.
      var emails = [];
      var totalemails = 0;
      jQuery.each(elems, function () {
        thisVal = $(this).val();
        //console.log($(this));
        if (thisVal != '') {
          totalemails += 1;
          if (emails.indexOf(thisVal) === -1) {
            emails.push(thisVal)
            console.log("New - lets add to array");
          } else {
            console.log("This item already exists");
          }
        }

      });

      console.log("Total Emails = " + totalemails);
      console.log(emails);

      count = emails.length;
      console.log("Unique Emails = " + count);

      // count should be either 0 or 1 max
      // count should be 3 if total >=3

      if (totalemails >= 3) {
        if (count >= 3) {
          console.log("here1");
          elems.removeClass('error');
          return true;
        } else {
          console.log("here2");
          elems.addClass('error');
        }
      } else {
        console.log("here3");
        elems.removeClass('error');
        return true;
      }
    }, "Please provide email addresses for three separate individuals.");

    joinFlowForm.validate({
      focusInvalid: false,
      invalidHandler: function (form, validator) {
        if ($(validator.errorList[0].element).hasClass("ignoreme")) {
          console.log("trace 1");
          console.log($(validator.errorList[0].element));
          $(validator.errorList[0].element).rules("remove", "restricted");
          return;
        } 
        if (!validator.numberOfInvalids())
          console.log("trace 2");
        return;
        $('html, body').animate({
          scrollTop: $(validator.errorList[0].element).offset().top
        }, 200);
      },
      rules: {
        "14_first_name": {
          required: true,
          minlength: 2
        },
        "15_last_name": {
          required: true,
          minlength: 2
        },
        "16_email_address": {
          required: true
        },
        "23_billing_country": {
          required: true,
          restricted: true
        },
        "23_postal_country": {
          required: true,
          restricted: true
        },
        "40_organisation_name": {
          required: true,
          maxlength: 100
        },
        "41_profit_or_not_for_profit": {
          required: true
        },
        "42_organisation_type": {
          required: true
        },
        "43_organisation_url": {
          required: true,
          url: true
        },
        "43_organisation_url_published_content": {
          required: true,
          url: true
        },
        "44_organisation_revenue": {
          required: true
        },
        "30_billing_contact_first_name": {
          required: true
        },
        "30_billing_contact_last_name": {
          required: true
        },
        "30_billing_contact_email": {
          required: true,
          atleastthree: ['.uniqueish']
        },
        "30_billing_contact_postal_address": {
          required: true
        },
        

        "24_primary_contact_first_name": {
          required: true
        },
        "24_primary_contact_last_name": {
          required: true
        },
        "24_primary_contact_email": {
          required: true,
          atleastthree: ['.uniqueish']
        },

        "25_resolution_contact_first_name": {
          required: false
        },
        "25_resolution_contact_last_name": {
          required: false
        },
        "25_resolution_contact_email": {
          required: false,
          atleastthree: ['.uniqueish']
        },

        "26_voting_contact_first_name": {
          required: function () {
            var $pubown = $('input[name=12_publishown]:checked').val();
            if ($pubown == '3') {
              console.log('voting not required');
              return false;
            } else {
              console.log('pubown=' + $pubown);
              console.log('voting is required');
              return true;
            }
          }
        },
        "26_voting_contact_last_name": {
          required: function () {
            var $pubown = $('input[name=12_publishown]:checked').val();
            if ($pubown == '3') {
              console.log('voting not required');
              return false;
            } else {
              console.log('pubown=' + $pubown);
              console.log('voting is required');
              return true;
            }
          }
        },
        "26_voting_contact_email": {
          required: function () {
            var $pubown = $('input[name=12_publishown]:checked').val();
            
            console.log("checked" + $pubown);
            if ($pubown == '3') {
              console.log('voting not required');
              return false;
            } else {
              console.log('pubown=' + $pubown);
              console.log('voting is required');
              return true;
            }
          },
          atleastthree: ['.uniqueish']
        },

        "27_technical_contact_first_name": {
          required: true
        },
        "27_technical_contact_last_name": {
          required: true
        },
        "27_technical_contact_email": {
          required: true,
          atleastthree: ['.uniqueish']
        },


        "28_technical_extra_contact_first_name": {
          required: false
        },
        "28_technical_extra_contact_last_name": {
          required: false
        },
        "28_technical_extra_contact_email": {
          required: false
        },

        "29_metadata_quality_contact_first_name": {
          required: true
        },
        "29_metadata_quality_contact_last_name": {
          required: true
        },
        "29_metadata_quality_contact_email": {
          required: true,
          atleastthree: ['.uniqueish']
        },

        "30_billing_contact_first_name": {
          required: true
        },
        "30_billing_contact_last_name": {
          required: true
        },
        "30_billing_contact_email": {
          required: true,
          atleastthree: ['.uniqueish']
        },

        "31_billing_extra_contact_first_name": {
          required: false
        },
        "31_billing_extra_contact_last_name": {
          required: false
        },
        "31_billing_extra_contact_email": {
          required: false
        },

        "29_metadata_quality_contact_first_name": {
          required: function () {
            var $pr = $('[name=13_publishrep]');
            if ($pr.is(':checked') && $pr.val() == 'get') {
              return false;
            } else {
              return true;
            }
          }
        },
        "29_metadata_quality_contact_last_name": {
          required: function () {
            var $pr = $('[name=13_publishrep]');
            if ($pr.is(':checked') && $pr.val() == 'get') {
              return false;
            } else {
              return true;
            }
          }
        },
        "29_metadata_quality_contact_email": {
          required: function () {
            var $pr = $('[name=13_publishrep]');
            if ($pr.is(':checked') && $pr.val() == 'get') {
              return false;
            } else {
              return true;
            }
          },
          atleastthree: ['.uniqueish']
        },
        "45_agreement": {
          required: true
        },
        "17_postal_street_1": {
          required: true
        },
        "19_postal_street_2": {
          required: false
        },
        "20_postal_city": {
          required: true
        },
        "21_postal_state": {
          required: false
        },
        "22_postal_postal_code": {
          required: true
        },
        "11_whattodo": {
          required: true
        },
        "12_publishown": {
          required: true
        },
        "13_publishrep": {
          required: true
        },
        "agreement_modal": {
          required: true
        }
      },
      messages: {
        "34_organisation_url" : "Please enter a valid url, for example http://www.example.com"
      }
    });
  }

  function fnNextQuestion() {
    var $question = $('.jf-question-1,.jf-question-2,.jf-question-3, .jf-question-4, .jf-question-5, .jf-question-6, .jf-question-7, .jf-question-8, .jf-question-9'),
    $form = $('#WebToLeadForm');


    //     $checkOne = $('#country'),
    //     $checkTwo = $('#organisation_revenue');
    //
    // function nextQuestionProceed($selector) {
    //
    //     $selector.on('change', function () {
    //         var $this = $(this),
    //             $current = $this.closest('.jf-question'),
    //             $next = $current.next();
    //
    //         if ($form.valid()) {
    //             $next.show().scrollTo(500, 'linear');
    //         }
    //     });
    // }
    //
    // nextQuestionProceed($checkOne);
    // nextQuestionProceed($checkTwo);


    $question.each(function () {
      var _this = $(this),
      $nextQuestion = _this.find('.next-question');

      $nextQuestion.click(function (e) {


        //if(grecaptcha.getResponse() == "") {
        //  e.preventDefault();
        //  alert("reCAPTCHA response is required");
        //  return false;
        //} else {
        //}

        e.preventDefault();

        var $this = $(this),
        $current = $this.closest('.jf-question'),
        $next = $current.next(), $nextplus = $current.next().next();
        var $pubown = $('input[name=12_publishown]');
        
        $pubown_val = $('input[name=12_publishown]:checked').val();

        if ($pubown_val == '3') {
          if ($next.hasClass('jf-question-2')) {
          
            $next = $nextplus;
            var submitBtn = $('.jf-sumbit-form');
            submitBtn.removeClass('disabled');
          }
        } 



        if ($form.valid()) {
          $next.show().scrollTo(500, 'linear');
        }
      });
    });

  }

  function fnPopulateTableData() {

    //Data setter functions

    function setFieldData(tableRowName, fieldName) {
      var tableId = $('td' + '.' + tableRowName),
      fieldId = Cookies.get(fieldName);

      return tableId.text(fieldId);
    }

    function setTwoFieldData(tableRowName, fieldNameOne, fieldNameTwo) {
      var tableId = $('td' + '.' + tableRowName),
      fieldIdOne = Cookies.get(fieldNameOne),
      fieldIdTwo = Cookies.get(fieldNameTwo);

      return tableId.text(fieldIdOne + ' ' + fieldIdTwo);
    }

    function setThreeFieldData(tableRowName, fieldNameOne, fieldNameTwo, fieldNameThree) {

      var tableId = $('td' + '.' + tableRowName),
      fieldIdOne = Cookies.get(fieldNameOne),
      fieldIdTwo = Cookies.get(fieldNameTwo),
      fieldIdThree = Cookies.get(fieldNameThree);

      return tableId.text(fieldIdOne + ' ' + fieldIdTwo + ' (' + fieldIdThree + ')');
    }

    function setPostalAddress(tableRowName, fieldNameOne, fieldNameTwo, fieldNameThree, fieldNameFour, fieldNameFive) {

      var tableId = $('td' + '.' + tableRowName),
      fieldIdOne = Cookies.get(fieldNameOne),
      fieldIdTwo = Cookies.get(fieldNameTwo),
      fieldIdThree = Cookies.get(fieldNameThree),
      fieldIdfour = Cookies.get(fieldNameFour),
      fieldIdFive = Cookies.get(fieldNameFive);

      return tableId.text(fieldIdOne + ' ' + fieldIdTwo + ', ' + fieldIdThree + ', ' + fieldIdfour + ', ' + fieldIdFive);
    }

    function setOptionData(tableRowName, id) {

      var tableId = $('td' + '.' + tableRowName),
      selectTxt = $('#' + id).find("option:selected").text();

      // hacky fix for comma separated list of org types
      if(id == 'organisation_type') {
        selectTxt = $("#organisation_type").val();
      }
      console.log("selectTxt = " + selectTxt);
      tableId.text(selectTxt);
    }

    var $trigger = $('.next-section');

    $trigger.click(function () {

      if ($(this).hasClass('disabled')) {
        console.log('disabled');
      } else {
        autoPopulate();
      }
    });


    function autoPopulate() {

      //Set single field data

      $singles = {
        'membership_email': '16_email_address',
        'membership_org_name': '40_organisation_name',
        'membership_org_url': '43_organisation_url'
      };

      $.each($singles, function (index, value) {
        setFieldData(index, value);
      });


      //Set two field data

      setTwoFieldData('membership_full_name', '14_first_name', '15_last_name');


      //Set Option Data

      var $options = {
        'membership_country': 'postal_country',
        'membership_org_type': 'organisation_type',
        'membership_org_revenue_full': 'organisation_revenue_full',
        'membership_org_revenue_short': 'organisation_revenue_short'
      };

      $.each($options, function (index, value) {
        setOptionData(index, value);
      });


      //Set three field data

      var $contacts = {
        'membership_contact_one': '24_primary_contact',
        'membership_contact_two': '27_technical_contact',
        'membership_contact_three': '30_billing_contact',
        'membership_contact_four': '26_voting_contact',
        'membership_contact_five': '29_metadata_quality_contact'
      };


      $.each($contacts, function (index, value) {

        console.log(index + '= ' + value + '_first_name' + ' - ' + value + '_last_name' + ' - ' + value + '_email' );

        setThreeFieldData(index, value + '_first_name', value + '_last_name', value + '_email');
      });

      setPostalAddress('membership_postal_address', '17_postal_street_1', '19_postal_street_2', '20_postal_city', '21_postal_state', '22_postal_postal_code');

    }

  }

  function fnSaveFormData() {

    var $trigger = $('.next-section'),
    $form = $('#WebToLeadForm'),
    $el = $form.find('input, select'),
    $blacklist = ["Submit", "campaign_id", "assigned_user_id", "team_id", "team_set_id", "req_id", "bool_id", "agreement_c"],
    $names = [];

    $trigger.click(function () {
      if (!$(this).hasClass('disabled')) {
        $el.each(function () {
          var $this = $(this),
          $type = $this.attr('type');

          switch ($type) {
            case 'checkbox':
            case 'radio':
            if ($this.is(':checked')) {
              $names.push({
                name: $this.attr('name'),
                value: $this.val()
              });
            }
            break;
            case 'text':
            case 'url':
            case 'email':
            if ($this.val().length > 0) {
              $names.push({
                name: $this.attr('name'),
                value: $this.val()
              });
            }
            break;
            default:
            if ($this.val()) {
              $names.push({
                name: $this.attr('name'),
                value: $this.val()
              });
            }
          }
        });

        //Set Cookies with our form data

        $.each($names, function (index, value) {
          if ($.inArray(value.name, $blacklist) == -1) {
            Cookies.set(value.name, value.value, {
              expires: 1
            });
          }
        });
      }
    });


    //Populate the form if we leave the joinflow

    var $cookiesData = Cookies.get();

    if ($cookiesData) {
      $form.values($cookiesData);
    }

    //Delete cookies after form submit

    var $exitFormBtn = $('.your-membership').find('.jf-sumbit-form');

    $exitFormBtn.click(function () {
      if (!$(this).hasClass('disabled')) {
        $.each($names, function (index, value) {

          if ($.inArray(value.name, $blacklist) == -1) {
            Cookies.remove(value.name);
          }
        });
      }
    });
  }

  function fnFillReplyToData(){

    var $replyToField = $('#special-replyto'),
    $nextQuestionBtn = $('.ns-final-btn'),
    $replyToEmail = Cookies.get('16_email_address');

    //hmmm email address cookie not always set when form is submitted.
    //but field should be filled.
    $replyToEmail = $('#email_address').val();

    console.log("ReplyTo set to " + $replyToEmail);

    $replyToField.val($replyToEmail);

  }
  
  function fnFillUserAgentData() {
	  
  }

  function fnAgreeWatch() {
    $('.modal-content').scroll(function() {
      var disable = $('.modal-content-join').height() != ($(this).scrollTop() + $(this).height());
      //console.log($('.modal-content-join').height() + " - "  + ($(this).scrollTop() + $(this).height()));
      //console.log(disable);
      //$('#agreement-modal').prop('disabled', disable);
    });

    $('#agreement-modal').change(function () {
      $('#close-join-modal').prop("disabled", !this.checked);
  }).change()
  }

  function fnFillIDACountry() {
    
    postal_countryVal = $('#postal_country').val();
    billing_countryVal = $('#billing_country').val();
    postal_found = ida_countries.includes(postal_countryVal);
    billing_found = ida_countries.includes(billing_countryVal);

    //console.log("Postal ", postal_countryVal);
    //console.log("Billing ", billing_countryVal);
    
    if (postal_found && billing_found) {      
      $('#IDA_Country').val("Yes");
    } else if (postal_found || billing_found) {
      $('#IDA_Country').val("Partial");
    } else {          
      $('#IDA_Country').val("No");
    }
  }


  function fnContentAgreement() {

    var $target = $("a[title='member-obligations']"),
    $agreementContent = $('#agreement-content'),
    $options = {
      maxWidth: 1000,
      maxHeight: 800,
      fitToView: false,
      width: '90%',
      height: '90%',
      autoSize: false,
      closeClick: false,
      openEffect: 'none',
      padding: 0,
      closeEffect: 'none'
    };


    $.ajax({
      url: '/membership/terms/',
      type: 'GET',
      success: function (data) {
        var $externalHtml = $(data).find('#thanks-for-joining-crossref-and-participating-in-the-scholarly-content-network').parent().html();
        $agreementContent.html($externalHtml);
      }
    });

    $target.fancybox($options);
  }


  function init() {
    fnEditEntry();
    fnContentAgreement();
    fnSaveFormData();
    //fnEnableQuestionIfFilled();
    fnValidateJoinFLow();
    fnPopulateTableData();
    fnPanelNav();
    fnScrollNextSection();
    fnEnableNextSectionBtn();
    fnClickableBreadcrumbNav();
    fnOpenByHash();
    fnNextQuestion();
    fnPopulateDataForContacts();
    fnPanelConditionals();
  }

  return {
    init: init
  };

}(jQuery);

jQuery(document).ready(function () {
  console.log(ida_countries);
  //visited.init();
  jQuery("#organisation_url").val('https://');
  jQuery("#organisation_url_published_content").val('https://');
  
  joinflow.init();
  jQuery("#billing_country").combobox(); 
  jQuery(".typeahead_postal").combobox(); 
 

});