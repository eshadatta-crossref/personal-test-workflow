var visited = function ($) {

  function fnCheck() {
    var $dismissedJoin = Cookies.get('dismissedjoin');
    if (!$dismissedJoin) {
      $("#myModal").modal({
        //backdrop: 'static',
        //keyboard: false
      })
    }
    //Cookies.set('dismissedjoin', true, { expires: 1 });
  }

  function init() {
    fnCheck();
  }

  return {
    init: init
  };
}(jQuery);


var joinflow = function ($) {

  var $yes_integrate = $("#yes_integrate_similarity_check");
  var $no_integrate  = $("#no_integrate_similarity_check");

  $yes_integrate.on('change', function () {
    console.log("integrate has changed" + $yes_integrate.val());
    if ($yes_integrate.val() == 'yes') {
      console.log("need to do nothing but hide the question?");
      $("#no_integration").hide();
    } 
  });

  $no_integrate.on('change', function () {
    console.log("integrate has changed has changed" + $no_integrate.val());
    if ($no_integrate.val() == 'no') {
      console.log("need to show next question ");
      $("#no_integration").show();
    } 
  });


  var $man_sub = $('#manuscript_transition');
  var $man_sub_first = $("#manuscript_transition option:first").val();
  var $man_sub_last = $("#manuscript_transition option:last").val();
  $man_sub.on('change', function () {
    // if it's the first one then show integration question
    if ($man_sub.val() != $man_sub_first) {     
        // if other (the last option) was selected then 
      if ($man_sub.val() != $man_sub_last) {      
        $("#yes_other").hide();
        $("#yes_manuscript_submissions").show();
      } else {
        $("#yes_other").show();
        $("#yes_manuscript_submissions").show();
      } 
    } else {
      $("#yes_manuscript_submissions").hide();
    }



  });

  $man_sub.on('change', function () {
  });


  var $metaDataSelectValue_var;

  function fnPanelConditionals() {
    /**
    * Moves user straight to Q4
    * @param $currentQuestion
    */
    function moveToQ4($currentQuestion) {
      var $parentSection = $currentQuestion.closest('.section'),
      $q4 = $('.section-2');

      $parentSection.hide();
      $q4.show().scrollTo(500, 'linear');
    }

    /**
    * If conditional is false, move user through the "JoinFlow" normally
    * @param $currentQuestion
    */
    function proceedToNextQuestion($currentQuestion) {
      var $current = $currentQuestion.closest('.jf-question'),
      $next = $current.next();

      $next.show().scrollTo(500, 'linear');
    }

    /**
    * Hides element on click
    * @param $selector
    */
    function hideQuestion($selector) {
      $('.' + $selector).hide();
    }

    /**
    * Makes hidden elements visible again
    * @param $selector
    */
    function showQuestion($selector) {
      $('.' + $selector).show();
    }

    /**
    * Moves user breadcrumb to signify that we are on the next section
    */
    function moveBreadCrumb() {

      var $container = $('.triangular-steps'),
      $item = $container.find('li:first-child'),
      $next = $item.next();

      $item.removeClass('active').addClass('completed');
      $next.addClass('active');

    }

    //Check metadata value, then skip to Q4 if true
    var $agreement = $("#agreement-modal");
    var $nq = $('.sponsored-followup');


    visited.init();
    fnSponsorNameStuff();

     
    var $nbtn = $agreement.closest('.section').find('.next-section');
    //$nbtn.removeClass('disabled');


  }

  function fnPopulateDataForContacts() {


    var checkbox_tech = $('#same_as_business_contact2'),
    checkbox_business = $('#same_as_business_contact');

    /**
    * This fills the conatct contact fields if the checkbox is selected, nullifies them if unchecked
    * @param $selector
    * @param $identifier
    * @param $filler
    */
    function fillFormOnClick($selector, $identifier, $filler) {

      var blank = "";

      $selector.on('change', function () {

        var $this = $(this);

        if ($this.is(':checked')) {

          var firstname = $('#' + $identifier + '_first_name').val(),
          lastname = $('#' + $identifier + '_last_name').val(),
          email = $('#' + $identifier + '_email').val();

          $('#' + $filler + '_first_name').val(firstname);
          $('#' + $filler + '_last_name').val(lastname);
          $('#' + $filler + '_email').val(email);

        } else {

          $('#' + $filler + '_first_name').val(blank);
          $('#' + $filler + '_last_name').val(blank);
          $('#' + $filler + '_email').val(blank);
        }

      });
    }

    fillFormOnClick(checkbox_business, 'contact_3', 'contact_4');
    fillFormOnClick(checkbox_tech, 'contact_3', 'contact_4_2');
  }

  function fnEditEntry() {
    var $container = $('.jf-membership-table'),
    $btns = $container.find('.jf-btn-small');


    $btns.each(function () {

      var $this = $(this);

      $this.click(function () {

        $this = $(this);

        var $parent = $this.data('parent'),
        $current = $this.closest('.section'),
        $section = $($parent).closest('.section'),
        $fieldId = $this.data('id');

        //console.log($parent);

        $current.hide();
        $section.show(function () {

          var $header = $($fieldId).closest('.jf-question').find('.jf-question-header');

          //console.log($header);

          $header.scrollTo(600, 'linear');
          $($fieldId).focus();
        });
      });
    });
  }

  function fnOpenByHash() {

    var $requestedHash = ((window.location.hash.substring(1).split("#", 1)) + "?").split("?", 1),
    $hashToOpen = $('#' + $requestedHash),
    $stepNavContainer = $('.join-steps-container'),
    $currentPanel = $('.section');

    //console.log($hashToOpen);

    if ($hashToOpen.length) {
      // Get panel header element
      var $requestedPanel = $('.section[data-id=' + $requestedHash + ']');

      //console.log($requestedPanel);

      if ($requestedPanel.length) {

        // Hide all panels if they are visible
        if ($currentPanel.is(':visible')) {
          $currentPanel.hide();
        }

        // Show requested panel
        $requestedPanel.show();
      }
    }
  }

  function fnClickableBreadcrumbNav() {
    var $container = $('.triangular-steps'),
    $el = $container.find('li'),
    $flag = false;

    $el.click(function (e) {
      var $this = $(this);
      e.preventDefault();

      if ($this.hasClass('completed')) {


        var $id = $this.attr('id'),
        $sectionVisible = $('.section:visible'),
        $next = $('.' + $id),
        $currentSectionId = $sectionVisible.data('id'),
        $currentSection = $('.' + $currentSectionId);

        var $addCompletedClass = Cookies.get('endOfJoinFlow');

        $this.removeClass('completed').addClass('active');
        $('#' + $currentSectionId).removeClass('active');

        if ($currentSectionId == 'your-membership') {
          $flag = true;
          Cookies.set('endOfJoinFlow', true);
        }

        if ($addCompletedClass) {
          $el.each(function () {
            if (!$(this).hasClass('active')) {
              $(this).addClass('completed');
            }
          });
        }

        $currentSection.hide();
        $next.show();
        $container.parent().scrollTo(400, 'linear');

      }
    });

    if (!$flag) {
      Cookies.set('endOfJoinFlow', false);
    }
  }

  function fnEnableQuestionIfFilled() {

    var $container = $(".jf-question"),
    $joinFlowForm = $('#WebToLeadForm'),
    $el = $container.find('input, select');

    $el.each(function () {
      var $this = $(this),
      $type = $this.attr('type');

      switch ($type) {
        case 'checkbox':
        case 'radio':
        var $this = $(this),
        $question = $this.closest('.jf-question');

        if ($this.is(':checked')) {

          $question.show();
        }
        break;
        case 'text':
        case 'url':
        case 'email':
        var $this = $(this),
        $question = $this.closest('.jf-question');

        if ($this.val().length > 0) {
          $question.show();
        }
        break;
        default:
        var $this = $(this),
        $btnContainer = $this.closest('.section').find('.btn-container'),
        $question = $this.closest('.jf-question'),
        $btn = $btnContainer.find('.next-section');


        if ($this.val()) {
          $question.show();

          if ($joinFlowForm.valid()) {
            $btn.removeClass('disabled');
          }

        }
      }
    });
  }

  function fnEnableNextSectionBtn() {

    var $agreement = $('#agreement-modal'),
    joinFlowForm = $('#WebToLeadForm');

    var $agreementContainer = $agreement.closest('.section').find('.jf-accordion-step-4'),
    $btn = $agreement.closest('.section').find('.jf-sumbit-form'),
    $finalNb = $('.final-n-b');

    $finalNb.click(function () {
      if ($agreementContainer.is(':hidden')) {
        $btn.removeClass('disabled');
      }
    });

    if (!$agreement.is(":checked")) {
      $btn.addClass('disabled');
    }

    $agreement.on('change', function () {
      var $this = $(this);
      if ($this.is(":checked")) {
        if (joinFlowForm.valid()) {
          $btn.removeClass('disabled');
        } else {
          $btn.addClass('disabled');
        }
      } else {
        $btn.addClass('disabled');
      }
    });

    var submitBtn = $('.jf-sumbit-form');

    submitBtn.click(function () {
      fnFillReplyToData();
      var $this = $(this);
      if ($this.hasClass('disabled')) {
        sweetAlert("One last thing...", "Sorry but you'll need to agree to the terms before you can proceed.", "error");
      } else {
        console.log("Submitting form");
        console.log("ReplyTo definitely set to " + $('#special-replyto').val() );

        joinFlowForm.submit();
      }
    });
  }


  function fnPanelNav() {


    var joinFlowForm = $('#WebToLeadForm');

    var $pr = $('input:radio[name="13_publishrep"]'),
    $next = $pr.closest('.section').find('.btn-container');

    $pr.on('change', function () {
      if ($pr.is(":checked")) {
        if (joinFlowForm.valid()) {
          $next.fadeIn();
        }
      } else {
        $next.hide();
      }
    });

    if ($pr.is(":checked")) {
      if (joinFlowForm.valid()) {
        $next.fadeIn();
      }
    } else {
      $next.hide();
    }

    var organisation_revenue = $('#organisation_revenue'),
    showFinal = $('.show-final');

    showFinal.click(function () {
      $next = organisation_revenue.closest('.section').find('.btn-container');
      $next.show();
    });
    // organisation_revenue.on('change', function () {
    //
    //     var $this = $(this),
    //
    //         $next = $this.closest('.section').find('.btn-container');
    //
    //
    //     console.log($next);
    //
    //
    //     if (joinFlowForm.valid()) {
    //
    //         $next.fadeIn();
    //
    //     } else {
    //         $next.hide();
    //     }
    // });


    // var $nextBtn = organisation_revenue.closest('.section').find('.btn-container'),
    //     $finalnb = $('.final-n-b');
    //
    // $finalnb.click(function () {
    //     if ($form.valid()) {
    //         $next.show().scrollTo(500, 'linear');
    //     }
    // });
  }

  function fnScrollNextSection() {

    var section = $('.section');

    section.each(function () {
      var $this = $(this),
      $nextBtn = $this.find('.next-section'),
      $prevBtn = $this.find('.prev-section'),
      $stepNavContainer = $('.join-steps-container'),
      $stepNav = $('.triangular-steps');

      function progressBarForward(current) {

        var $currentSectionId = current.data('id'),
        $nextSectionId = current.next().data('id');

        console.log('current section = ' + $currentSectionId);
        if ($nextSectionId != 'your-metadata') {
          $(".firstpageonly").hide();
          console.log('hiding header');
        } else {
          $(".firstpageonly").show();
          console.log('showing header');
        }
        

        $stepNav.find('#' + $currentSectionId).removeClass('active').addClass('completed');
        $stepNav.find('#' + $nextSectionId).addClass('active');

      }

      function progressBarBack(current) {

        var $currentSectionId = current.data('id'),
        $prevSectionId = current.prev().data('id');

        $stepNav.find('#' + $currentSectionId).removeClass('active');
        $stepNav.find('#' + $prevSectionId).addClass('active').removeClass('completed');

      }

      $nextBtn.click(function (e) {


        var $this = $(this),
        $current = $this.closest('.section'),
        joinFlowForm = $('#WebToLeadForm'),
        $next = $this.closest('.section').next(),
        $nextId = $next.data('id');

        if(joinFlowForm.valid()) {
          if ($this.hasClass('disabled')) {

            e.preventDefault();
            sweetAlert("Oops...", "Please answer the questions to proceed.", "error");
            joinFlowForm.valid();

          } else {

            e.preventDefault();
            $current.hide();
            $next.show();
            $stepNavContainer.scrollTo(400, 'linear');

            progressBarForward($current);

          }
        }
      });

      $prevBtn.click(function (e) {

        e.preventDefault();

        var $this = $(this),
        $current = $this.closest('.section'),
        $prev = $this.closest('.section').prev();

        $current.hide();
        $prev.show();
        $stepNavContainer.scrollTo(400, 'linear');

        progressBarBack($current);
      });
    });

  }

  function fnValidateJoinFLow() {
    var joinFlowForm = $('#WebToLeadForm'),
    blacklist = ['Cuba', 'Iran, Islamic Republic of', 'Korea, Democratic People\'s Republic of', 'Syrian Arab Republic'];


    //Disable enter key if form isn't valid
    function validationFunction() {
      console.log("in validationFunction");
      if (joinFlowForm.valid()) {
        return true;
      }
    }

    $(window).keydown(function (e) {
      if ((e.keyCode == 13) && (validationFunction() == false)) {
        e.preventDefault();
        return false;
      }
    });

    //Blacklist certain countries
    $.validator.addMethod("restricted", function (value) {
      return $.inArray(value, blacklist) == -1;
    }, "It looks like you're in an OFAC-sanctioned country. Please do click 'Next' to continue your application and 'submit', then we'll get back to you with more information.");


    //Checks for duplicate email addresses in the contacts section.
    //This now checks for 3 unique emails
    $.validator.addMethod("atleastthree", function (value, element, options) {

      // get all the elements passed here with the same class
      var elems = $(element).parents('form').find(options[0]);

      // the value of the current element
      var valueToCompare = value;

      // count
      var matchesFound = 0;

      // loop each element and compare its value with the current value
      // and increase the count every time we find one
      // not any more - check 3 unique out of all emails.
      var emails = [];
      var totalemails = 0;
      jQuery.each(elems, function () {
        thisVal = $(this).val();
        if (thisVal != '') {
          totalemails += 1;
          if (emails.indexOf(thisVal) === -1) {
            emails.push(thisVal)
            //console.log("New - lets add to array");
          } else {
            //console.log("This item already exists");
          }
        }

      });

      //console.log("Total Emails = " + totalemails);
      //console.log(emails);

      count = emails.length;
      //console.log("Unique Emails = " + count);

      // count should be either 0 or 1 max
      // count should be 3 if total >=3

      if (totalemails >= 3) {
        if (count >= 3) {
          elems.removeClass('error');
          return true;
        } else {
          elems.addClass('error');
        }
      } else {
        elems.removeClass('error');
        return true;
      }
    }, "Please provide email addresses for three separate individuals.");

    joinFlowForm.validate({
      focusInvalid: false,
      invalidHandler: function (form, validator) {

      console.log("validating");
        if ($(validator.errorList[0].element).hasClass("ignoreme")) {
          $(validator.errorList[0].element).rules("remove", "restricted");
          return;
        } 
        if (!validator.numberOfInvalids())
        return;
        $('html, body').animate({
          scrollTop: $(validator.errorList[0].element).offset().top
        }, 200);
      },
      rules: {
        "41_sponsored_or_not": {
          required: true
        },
        "prefixes": {
          required: true
        },
        "integrate_similarity_check": {
          required: true
        },
        "14_first_name": {
          required: true,
          minlength: 2
        },
        "15_last_name": {
          required: true,
          minlength: 2
        },
        "16_email_address": {
          required: true
        },
        "23_country": {
          required: true,
          restricted: true
        },
        "40_organisation_name": {
          required: true
        },
        "41_profit_or_not_for_profit": {
          required: true
        },
        "42_organisation_type": {
          required: true
        },
        "43_organisation_url": {
          required: true,
          url: false
        },
        "44_organisation_pub_url": {
          required: false,
          url: false
        },
        "43_organisation_url": {
          required: true,
          url: false
        },
        "45_organisation_doi": {
          required: false
        },
        "30_billing_contact_first_name": {
          required: true
        },
        "30_editorial_contact_last_name": {
          required: true
        },
        "30_editorial_contact_email": {
          required: true,
          atleastthree: ['.uniqueish']
        },
        "30_editorial_contact_postal_address": {
          required: true
        },
        "28_technical_contact_first_name": {
          required: true
        },
        "28_technical_contact_last_name": {
          required: true
        },
        "28_technical_contact_email": {
          required: true,
          atleastthree: ['.uniqueish']
        },
        "24_application_contact_first_name": {
          required: true
        },
        "24_application_contact_last_name": {
          required: true
        },
        "24_application_contact_email": {
          required: true,
          atleastthree: ['.uniqueish']
        },
        "27_voting_contact_first_name": {
          required: function () {
            var $pr = $('[name=13_publishrep]');
            if ($pr.is(':checked') && $pr.val() == 'get') {
              return false;
            } else {
              return true;
            }
          }
        },
        "27_voting_contact_last_name": {
          required: function () {
            var $pr = $('[name=13_publishrep]');
            if ($pr.is(':checked') && $pr.val() == 'get') {
              return false;
            } else {
              return true;
            }
          }
        },
        "27_voting_contact_email": {
          required: function () {
            var $pr = $('[name=13_publishrep]');
            if ($pr.is(':checked') && $pr.val() == 'get') {
              return false;
            } else {
              return true;
            }
          },
          atleastthree: ['.uniqueish']
        },
        "29_metadata_quality_contact_first_name": {
          required: function () {
            var $pr = $('[name=13_publishrep]');
            if ($pr.is(':checked') && $pr.val() == 'get') {
              return false;
            } else {
              return true;
            }
          }
        },
        "29_metadata_quality_contact_last_name": {
          required: function () {
            var $pr = $('[name=13_publishrep]');
            if ($pr.is(':checked') && $pr.val() == 'get') {
              return false;
            } else {
              return true;
            }
          }
        },
        "29_metadata_quality_contact_email": {
          required: function () {
            var $pr = $('[name=13_publishrep]');
            if ($pr.is(':checked') && $pr.val() == 'get') {
              return false;
            } else {
              return true;
            }
          },
          atleastthree: ['.uniqueish']
        },
        "40_sponsoring_organisation_name": {
          required:true
        },
        "45_agreement": {
          required: true
        },
        "17_postal_street_1": {
          required: true
        },
        "19_postal_street_2": {
          required: false
        },
        "20_postal_city": {
          required: true
        },
        "21_postal_state": {
          required: false
        },
        "22_postal_postal_code": {
          required: true
        },
        "11_whattodo": {
          required: true
        },
        "12_publishown": {
          required: true
        },
        "13_publishrep": {
          required: true
        },
        "manuscript_transition": {
          required: true
        }
      },
      messages: {
        "34_organisation_url" : "Please enter a valid url, for example http://www.example.com"
      }
    });
  }

  function fnNextQuestion() {
    var $question = $('.jf-question-1, .jf-question-2, .jf-question-3,.jf-question-4, .jf-question-5b, .jf-question-6, .jf-question-7'),
    $form = $('#WebToLeadForm');

    $question.each(function () {
      var _this = $(this),
      $nextQuestion = _this.find('.next-question');


      $nextQuestion.click(function (e) {

        e.preventDefault();

        var $this = $(this),
        $current = $this.closest('.jf-question'),
        $next = $current.next();

        if ($form.valid()) {
          console.log("checking valid");
          $next.show().scrollTo(500, 'linear');
        }
      });
    });

  }

  function fnPopulateTableData() {

    //Data setter functions

    function setFieldData(tableRowName, fieldName) {
      var tableId = $('td' + '.' + tableRowName),
      fieldId = Cookies.get(fieldName);

      return tableId.text(fieldId);
    }

    function setTwoFieldData(tableRowName, fieldNameOne, fieldNameTwo) {
      var tableId = $('td' + '.' + tableRowName),
      fieldIdOne = Cookies.get(fieldNameOne),
      fieldIdTwo = Cookies.get(fieldNameTwo);

      return tableId.text(fieldIdOne + ' ' + fieldIdTwo);
    }

    function setThreeFieldData(tableRowName, fieldNameOne, fieldNameTwo, fieldNameThree) {

      var tableId = $('td' + '.' + tableRowName),
      fieldIdOne = Cookies.get(fieldNameOne),
      fieldIdTwo = Cookies.get(fieldNameTwo),
      fieldIdThree = Cookies.get(fieldNameThree);

      return tableId.text(fieldIdOne + ' ' + fieldIdTwo + ' (' + fieldIdThree + ')');
    }

    function setPostalAddress(tableRowName, fieldNameOne, fieldNameTwo, fieldNameThree, fieldNameFour, fieldNameFive) {

      var tableId = $('td' + '.' + tableRowName),
      fieldIdOne = Cookies.get(fieldNameOne),
      fieldIdTwo = Cookies.get(fieldNameTwo),
      fieldIdThree = Cookies.get(fieldNameThree),
      fieldIdfour = Cookies.get(fieldNameFour),
      fieldIdFive = Cookies.get(fieldNameFive);

      return tableId.text(fieldIdOne + ' ' + fieldIdTwo + ', ' + fieldIdThree + ', ' + fieldIdfour + ', ' + fieldIdFive);
    }

    function setOptionData(tableRowName, id) {

      var tableId = $('td' + '.' + tableRowName),
      selectTxt = $('#' + id).find("option:selected").text();

      tableId.text(selectTxt);
    }


    var $trigger = $('.next-section');

    $trigger.click(function () {

      if ($(this).hasClass('disabled')) {
        //console.log('disabled');
      } else {
        autoPopulate();
      }
    });


    function autoPopulate() {

      //Set single field data

      $singles = {
        'membership_email': '16_email_address',
        'membership_org_name': '40_organisation_name',
        'membership_org_url': '43_organisation_url'
      };

      $.each($singles, function (index, value) {
        setFieldData(index, value);
      });


      //Set two field data

      setTwoFieldData('membership_full_name', '14_first_name', '15_last_name');


      //Set Option Data

      var $options = {
        'membership_country': 'country',
        'membership_org_type': 'organisation_type',
        'membership_org_revenue': 'organisation_revenue'
      };

      $.each($options, function (index, value) {
        setOptionData(index, value);
      });


      //Set three field data

      var $contacts = {
        'membership_contact_one': '30_billing_contact',
        'membership_contact_two': '28_technical_contact',
        'membership_contact_three': '24_business_contact',
        'membership_contact_four': '27_voting_contact',
        'membership_contact_five': '29_metadata_quality_contact'
      };

      $.each($contacts, function (index, value) {

        //console.log(index + '= ' + value + '_first_name' + ' - ' + value + '_last_name' + ' - ' + value + '_email' );

        setThreeFieldData(index, value + '_first_name', value + '_last_name', value + '_email');
      });

      setPostalAddress('membership_postal_address', '17_postal_street_1', '19_postal_street_2', '20_postal_city', '21_postal_state', '22_postal_postal_code');

    }

  }

  function fnSaveFormData() {

    var $trigger = $('.next-section'),
    $form = $('#WebToLeadForm'),
    $el = $form.find('input, select'),
    $blacklist = ["Submit", "campaign_id", "assigned_user_id", "team_id", "team_set_id", "req_id", "bool_id", "agreement_c"],
    $names = [];

    $trigger.click(function () {
      if (!$(this).hasClass('disabled')) {
        $el.each(function () {
          var $this = $(this),
          $type = $this.attr('type');

          switch ($type) {
            case 'checkbox':
            case 'radio':
            if ($this.is(':checked')) {
              $names.push({
                name: $this.attr('name'),
                value: $this.val()
              });
            }
            break;
            case 'text':
            case 'url':
            case 'email':
            if ($this.val().length > 0) {
              $names.push({
                name: $this.attr('name'),
                value: $this.val()
              });
            }
            break;
            default:
            if ($this.val()) {
              $names.push({
                name: $this.attr('name'),
                value: $this.val()
              });
            }
          }
        });

        //Set Cookies with our form data

        //$.each($names, function (index, value) {
        //  if ($.inArray(value.name, $blacklist) == -1) {
         //   Cookies.set(value.name, value.value, {
         //     expires: 1
        //    });
        //  }
        //});
      }
    });


    //Populate the form if we leave the joinflow

    var $cookiesData = Cookies.get();

    if ($cookiesData) {
      $form.values($cookiesData);
    }

    //Delete cookies after form submit

    var $exitFormBtn = $('.your-membership').find('.jf-sumbit-form');

    $exitFormBtn.click(function () {
      if (!$(this).hasClass('disabled')) {
        $.each($names, function (index, value) {

          if ($.inArray(value.name, $blacklist) == -1) {
            Cookies.remove(value.name);
          }
        });
      }
    });
  }

  function fnFillReplyToData(){

    var $replyToField = $('#special-replyto'),
    $nextQuestionBtn = $('.ns-final-btn'),
    $replyToEmail = Cookies.get('24_application_contact_email');

    //hmmm email address cookie not always set when form is submitted.
    //but field should be filled.
    $replyToEmail = $('#contact_3_email').val();

    //console.log("ReplyTo set to " + $replyToEmail);

    $replyToField.val($replyToEmail);

  }
  
  


  function fnContentAgreement() {

    var $target = $("a[title='member-obligations']"),
    $agreementContent = $('#agreement-content'),
    $options = {
      maxWidth: 1000,
      maxHeight: 800,
      fitToView: false,
      width: '90%',
      height: '90%',
      autoSize: false,
      closeClick: false,
      openEffect: 'none',
      padding: 0,
      closeEffect: 'none'
    };


    $.ajax({
      url: '/membership/terms/',
      type: 'GET',
      success: function (data) {
        var $externalHtml = $(data).find('#thanks-for-joining-crossref-and-participating-in-the-scholarly-content-network').parent().html();
        $agreementContent.html($externalHtml);
      }
    });

    $target.fancybox($options);
  }

  function fnSponsorNameStuff() {
    var urlParams = new URLSearchParams(window.location.search);

    var $apiURL = "https://api.crossref.org/v1"
    var $apiUser = "?mailto=ginny@crossref.org"

    var $queryID = urlParams.get('id');

    if ($queryID != null) {
      $queryID =  $queryID.replace(/[^0-9.]/g, "");
    }

    var $intacct_id = null;

    if($queryID == null) {
      console.log('no id passed');
      $('#supplied_organisation_name').val("none supplied");
      $('#supplied_organisation_id').val("none supplied");
      $('#member_id_from_url').val("none supplied");
      $('#intacct_id').val("none supplied");
      $('.sponsor_org_present').hide();
      $('.sponsor_org_not_present').show();
      $('#prefixes').prop('readonly', false);


      $('#WebToLeadForm').prop('action', $('#WebToLeadForm').data('action_alt'));
      $('#hidden_next').val($('#hidden_next').data('target_alt'));
      $('#email_subject').val($('#email_subject').data('value_alt'));
      $('#message').val($('#message').data('message_alt'));
      $('#application_page_title').text($('#application_page_title').data('pagetitle_alt'));


    } else {


      //console.log('ID passed');

      $('#supplied_organisation_name').val('Member ID currently not found');


      // make prefixes editable since we have not populated it.
      $('#prefixes').prop('readonly', false);


      $intacct_id = urlParams.get('intacct_id');

      if ($intacct_id != null) {
        $intacct_id = $intacct_id.replace(/[^0-9.]/g, "");
      }
      $('#intacct_id').val($intacct_id);

      $('#supplied_organisation_id').val($queryID);

      var memberLookupUrl = $apiURL + "/members/" + $queryID + $apiUser;

      var promise = $.getJSON( memberLookupUrl );
      $.when(promise).done(function( data, textStatus, jqXHR ) {
        //console.log(jqXHR.status); 
        var $primaryName = data.message["primary-name"];  
        //console.log("Primary Name=", $primaryName);   
        if($primaryName == '') {
          console.log('Primary Name not currently found');
          $primaryName = 'Member ID not currently found';
        } else {          
          // make prefixes readonly  since we have populated it.
          $('#prefixes').prop('readonly', true);
        }
        $('#supplied_organisation_name').val($primaryName);

        var prefixes = data.message["prefixes"];     
        $('#prefixes').val(prefixes);

        $('#member_id_from_url').val($queryID);
      });  
    }


    //console.log("queryID=",$queryID);
    //console.log("intacct_id=", $intacct_id);


  }


  function init() {
    fnEditEntry();
    fnContentAgreement();
    //fnSaveFormData();
    //fnEnableQuestionIfFilled();
    fnValidateJoinFLow();
    //fnPopulateTableData();
    fnPanelNav();
    fnScrollNextSection();
    fnEnableNextSectionBtn();
    fnClickableBreadcrumbNav();
    fnOpenByHash();
    fnNextQuestion();
    fnPopulateDataForContacts();
    fnPanelConditionals();

    //fnFillReplyToData();
  }

  return {
    init: init
  };

}(jQuery);



jQuery(document).ready(function () {
  //visited.init();
  joinflow.init();
});