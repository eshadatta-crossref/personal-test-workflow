+++
draft = false
title = "Members with open references"
date = "2021-02-26"
author = "Ginny Hendricks"
rank = 4
weight = 1
+++

This table shows a list of our members that have set (some of) their references to Open. A new [reference distribution policy](/education/content-registration/descriptive-metadata/references#00259) was adopted by the board in 2017 and came into effect on January 1st, 2018. Any member joining since then are set to 'Open' by default. Any member can choose to set their references to 'Open', 'Limited', or 'Closed' at any time. The policy can be set differently per DOI prefix.

## Using this report

- If you do not see a publisher on this list, then their references are not Open. Check the [Closed](/reporting/members-with-closed-references/) list and if not there either, they are 'Limited'.
- Not all members deposit references with the rest of their metadata. At the moment, around 11,000 out of our 15,000 members are depositing references. The table below lists all members with the Open setting that also deposit some references. Click the headings to sort.
- Some members have more than one prefix, and some may be set to 'Open' but other prefixes not. This could be because a prefix is used for a specific content type that doesn't generally have references at all e.g. datasets or components.
- This table updates with every build of this website, so it may be a couple of days out of date.
- 'Backfiles' are anything older than two years. 'Current' is the last two years, rolling.


---

Please contact our <a href="mailto:support@crossref.org?subject=question-about-open-references">technical support specialists</a> with any questions.
