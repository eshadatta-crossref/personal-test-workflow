+++
title = "Organization chart"
date = "2021-04-20"
draft = false
author = "Ginny Hendricks"
[menu.main]
parent = "About us"
rank = 4
weight = 25
+++

Here is an overview of our team's structure, also take a look at our [people directory](/people) and click through to read about each of our roles and interests.

<iframe src="https://v5.organimi.com/p/o/5673da03df38a203000e8795/c/5673da1768da2f030041928c/chart?viewKey=595231ae0e96100400c57bb9&collapseTo=5&sortBy=role.hierarchy&topRole=&search=true&zoom=true&expandcollapse=true&breakBy=level&fitToScreen=true&hideLegend=true" style="height:1000px;width:100%;background:#fff;"></iframe>
