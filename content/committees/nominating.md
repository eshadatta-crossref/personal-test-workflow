+++
title = "Nominating committee"
author = "Lucy Ofiesh"
date = "2023-03-10"
draft = false
rank = 2
[menu.main]
parent = "Committees"
weight = "12"
+++

The Nominating Committee is defined in the [by-laws](/board-and-governance/bylaws/). There are five members of the committee and they can be either representations of organizations on the board or other regular members. Common practice is for membership to be made up of three board members not up for election that year, and two regular (non-board) members. The purpose of this committee is to review and create the slate each year for nominations to the board, ensuring fair representation of membership.

The Committee meets to discuss the charge, process, criteria, and potential candidates, and puts forward a slate which is at least equal to or greater than the number of Board seats up for election. The slate may or may not consist of some Board members up for re-election.

## 2023 Nominating Committee members

Staff facilitator: Lucy Ofiesh  

* Aaron Wood, APA, US, committee chair*
(committee in formation)

(*) Indicates Crossref board member

---
Please contact [Lucy Ofiesh](mailto:voting@crossref.org) with any questions
