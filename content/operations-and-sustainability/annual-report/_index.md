+++
title = "Annual report"
date = "2019-04-13"
draft = false
author = "Ginny Hendricks"
DOI = "https://doi.org/10.13003/y8ygwm5"
aliases = [
    "/annual_report_archive/",
    "/crossref_annual_report_2014/index.html",
    "/annual-report",
    "/annual-report/"
]
[menu.main]
rank = 4
parent = "Operations & sustainability"
weight = 8
+++

## 2019

The 2019 annual report is an expanded version we've called a "fact file".

#### Cite as:
> "Crossref Annual Report & Fact File 2018-19", retrieved [date], [https://doi.org/10.13003/y8ygwm5](https://doi.org/10.13003/y8ygwm5)

<iframe allowfullscreen allow="fullscreen" style="border:none;width:100%;height:326px;" src="//e.issuu.com/embed.html?backgroundColor=%233eb1c8&backgroundColorFullscreen=%233eb1c8&d=cr19010_crossref_factpack_digital_singles&hideIssuuLogo=true&showOtherPublicationsAsSuggestions=true&u=crossref"></iframe>

Download the [PDF](/pdfs/annual-report-factfile-2018-19.pdf).

## 2018

<iframe allowfullscreen allow="fullscreen" style="border:none;width:100%;height:326px;" src="//e.issuu.com/embed.html?backgroundColor=%234f5858&backgroundColorFullscreen=%234f5858&d=final_crossref-annual-report-2018&hideIssuuLogo=true&showOtherPublicationsAsSuggestions=true&u=crossref"></iframe>

Download the [PDF](/pdfs/annual-report-2017-18.pdf).

## 2017

<iframe allowfullscreen allow="fullscreen" style="border:none;width:100%;height:326px;" src="//e.issuu.com/embed.html?backgroundColor=%234f5858&backgroundColorFullscreen=%234f5858&d=crossref-annual-report-2016-2017&hideIssuuLogo=true&showOtherPublicationsAsSuggestions=true&u=crossref"></iframe>

Download the [PDF](/pdfs/annual-report-2016-17.pdf).


## 2016

<iframe allowfullscreen allow="fullscreen" style="border:none;width:100%;height:326px;" src="//e.issuu.com/embed.html?backgroundColor=%234f5858&backgroundColorFullscreen=%234f5858&d=crossref_annual_report_2015-16&hideIssuuLogo=true&showOtherPublicationsAsSuggestions=true&u=crossref"></iframe>

Download the [PDF](/pdfs/annual-report-2015-16.pdf).

## Previous annual reports

* 2014-15 annual report: [PDF](/pdfs/annual-report-2014-15.pdf) or [Digital](https://issuu.com/crossref/docs/crossref-annual-report-2014-15)
* 2013-14 annual report: [PDF](/pdfs/annual-report-2013-14.pdf) or  [Digital](https://issuu.com/crossref/docs/crossref-annual-report-2013-14)
* 2012-13 annual report: [PDF](/pdfs/annual-report-2012-13.pdf) or  [Digital](https://issuu.com/crossref/docs/crossref-annual-report-2012-13)
* 2011-12 annual report: [PDF](/pdfs/annual-report-2011-12.pdf) or  [Digital](https://issuu.com/crossref/docs/crossref-annual-report-2011-12)
* 2010-11 annual report: [PDF](/pdfs/annual-report-2010-11.pdf) or  [Digital](https://issuu.com/crossref/docs/crossref-annual-report-2010-11)
* 2009-10 annual report:  [PDF](/pdfs/annual-report-2009-10.pdf) or  [Digital](https://issuu.com/crossref/docs/crossref-annual-report-2009-10)
* 2008-09 annual report:  [PDF](/pdfs/annual-report-2008-09.pdf) or  [Digital](https://issuu.com/crossref/docs/crossref-annual-report-2008-09)
* 2007-08 annual report:  [PDF](/pdfs/annual-report-2007-08.pdf) or  [Digital](https://issuu.com/crossref/docs/crossref-annual-report-2007-08)

---
Please contact our [outreach team](mailto:feedback@crossref.org) if you have any questions.
