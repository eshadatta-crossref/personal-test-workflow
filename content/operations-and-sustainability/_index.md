+++
title = "Operations & sustainability"
date = "2022-03-23"
draft = false
author = "Lucy Ofiesh"
image = "/images/banner-images/succulents-pixabay.png"
[menu.main]
aliases = [
	"/sustainability",
	"/sustainability/"
]
rank = 4
weight = 4
parent = "About us"
+++


We are a not-for-profit organization, registered in the United States of America as a 501(c)6 (a "trade association"). We are sustained by [annual membership fees](/fees/#annual-membership-fees) that are tiered according to organizational size. We also charge a [Content Registration fee](/fees/#content-registration-fees) for each item of content whose metadata is registered with us. We also provide both free and paid-for [metadata retrieval](/services/metadata-retrieval) options for access to this metadata.

Members can and do [participate](/community) in many ways and contribute to our global network of linked scholarly content. We are governed by sixteen of our members that form [our board](/board-and-governance) serving three-year terms.

Crossref adopted the [Principles of Open Scholarly Infrastructure](http://openscholarlyinfrastructure.org/) and this section of our website is part of our commitment to transparent operations. From here you can find information about our financials, our membership operations, our policies, and eventually our staff handbooks and other formerly-internal documents.

## Sustainability at Crossref

Revenue from Crossref's annual dues and services sustains operations. The relevant POSI goals we strive to meet are:

* **Time-limited funds are used only for time-limited activities** – day-to-day operations should be supported by day-to-day sustainable revenue sources.
* **Goal to generate surplus** – it is not enough to merely survive. Producing a small surplus allows us to respond nimbly to opportunities or weather economic downtimes.
* **Goal to create a contingency fund to support operations for 12 months** – generating an operating surplus also allows us to create a separate fund that could support operations for a year.
* **Mission-consistent revenue generation** – any revenue we generate must be mission-aligned and not run counter to the aims of the organization.
* **Revenue based on services, not data** – data related to the running of the research enterprise should be community property. Appropriate revenue sources might include value-added services, consulting, API Service Level Agreements, or membership fees.
* **Transparent operations** – achieving trust in the selection of representatives to governance groups will be best achieved through transparent processes and operations in general (within the constraints of privacy laws).

### Fee principles

In July 2019 our board voted to approve the following principles to inform all future fee modeling and decisions.

Crossref’s fees should:
1. Enable us to fulfil our mission to make research outputs easy to find, cite, link, assess, and reuse
2. Encourage best practice and discourage bad practice, as our policies and obligations advise
3. Be non-discriminatory, encouraging broad participation from organizations of all sizes and types
4. Support the long-term persistence of our services and infrastructure, so long as relevant and valuable to the community
5. Deliver value to our members
6. Be transparent and openly available, recommended by the Membership & Fees Committee and approved by the board
7. Be the same for all, not discounted or negotiated individually, to ensure fairness
8. Be independent of our members’ own business models
9. Not always be necessary (e.g., new content types are not usually separate services)
10. Be based on providing services not metadata

## We are accredited in handling your data

Crossref was awarded the SOC 2® accreditation most recently in 2022 after an independent assessment of our controls and procedures by the American Institute of CPA’s (AICPA).

The SOC 2® accreditation is awarded to service organizations that have passed standard trust services criteria relating to the security, availability, and processing integrity of systems used to process users’ data and the confidentiality and privacy of the information processed by these systems.

The AICPA’s assessment also reviewed our vendor management programs, internal corporate governance and risk management processes, and regulatory oversight.

[Find out more about the SOC accreditation structure](https://www.aicpa.org/interestareas/frc/assuranceadvisoryservices/socforserviceorganizations.html)
