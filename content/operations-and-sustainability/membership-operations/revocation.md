+++
title = "Suspending or revoking membership"
date = "2022-03-23"
draft = false
author = "Amanda Bartell"
rank = 4
[menu.main]
parent = "Membership operations"
weight = 10
+++

Once a member joins Crossref, we expect them to remain a member for the long term. As an organization that’s obsessed with persistence, we really try to avoid suspending or revoking membership. However, there are three key reasons why we may be forced to do this:

* A member leaves invoices unpaid for a long time
* Legal sanctions or judgments against the Member or its home country
* A member contravenes the membership terms.

## Process for revoking membership due to unpaid invoices

Revoking membership due to unpaid fees is an absolute last step for us, but as a not-for-profit membership organization we have a duty to remain sustainable and manage our finances in a responsible way. Financial sustainability means we can keep the organization afloat and keep our dedicated service to scholarly communications running.

After each invoice is sent, we send a series of automated reminders to the member’s billing contact (and secondary billing contact if available) to make sure they’re clear on when their invoice payment is due. If the invoice remains unpaid after the due date, we send an email to the Billing, Secondary Billing, Primary and Technical contacts on the account to let them know that their account is at risk of suspension. This gives the member time to contact us if there are any issues with the invoice or if they didn’t realise that there was an outstanding invoice.

If the invoice remains unpaid, we suspend the member's access to register or update content, but they remain a member. Once a year  we contact all members who were suspended due to unpaid invoices to let them know that if the invoices remain unpaid for a further two weeks, their membership of Crossref will be revoked.

Once an organization’s membership has been revoked, they would need to re-apply if they wanted to become a member again in the future. We reserve the right to decline the application. If accepted, the applicant would need to pay all outstanding invoices before re-joining.

## Process for revoking membership due to Legal sanctions or judgments against the Member or its home country

 Very occasionally there may be a legal requirement or government order requiring us to suspend or revoke membership, most commonly as a result of economic sanctions. In this case, we are unable to work with the member from a set date and will suspend or revoke membership as appropriate. We will provide notification to any impacted members unless notification is prohibited by the legal requirement or order.

 You can find out more about [current sanctions impacting Crossref members](/operations-and-sustainability/membership-operations/sanctions/) here.

## Process for revoking membership due to contravention of the membership terms

There are limited occasions where we are forced to revoke membership of Crossref due to a contravention of the membership terms. This may be due to:

* Misrepresentation in the original membership application
* Fraudulent use of identifiers or metadata
* Contravening the [code of conduct](/code-of-conduct/)
* Any other basis set forth in the governing documents.

**Step One**
We contact the member to confirm the issue, investigate further and try to work with the member to rectify the problem if possible.

**Step Two**
If the issue cannot be resolved in a timely manner, we suspend the member's access to register new DOIs or update their existing DOIs. We continue to work with the member to try to rectify the problem.

**Step Three**
If the issue still cannot be resolved, we move to revoke membership. The Executive Committee of Crossref’s board reviews and ratifies the decision. The member is able to appeal this decision, and the Executive Committee of Crossref’s board will review and respond to an appeal.

**Step Four**
We add the member information to the [record of revoked membership](https://docs.google.com/spreadsheets/d/1cCkdvtqEM1urmrUQZ4-LGz_Omf5812aVkRFJc5UryHw/edit#gid=1891705587).
