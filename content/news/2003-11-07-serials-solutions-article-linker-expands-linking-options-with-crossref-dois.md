+++
title = "Serials Solutions' Article Linker Expands Linking Options With Crossref DOIs"
date = "2003-11-07"
parent = "News"
weight = 1
+++

*07 November 2003*

**Seattle, WA, November 7, 2003** -- Serials Solutions, Inc. incorporated DOI linking through Crossref into Article Linker, their OpenURL resolver, in a software release in late-summer. The company announced today that many libraries have taken advantage of DOI linking through Crossref, and it will continue to further integrate DOI linking into Article Linker.

"We're committed to offering libraries the best possible combination of options for full-text linking, accurate holdings data, and value," commented Steve McCracken, President and Co-Founder of Serials Solutions. "We're pleased to report that several subscribers to Article Linker have implemented DOI linking and have been enthusiastic about the broader linking options this makes available to them."

John Creech, Electronic Resources & System Librarian at Central Washington University commented, "Use of this service enhances access to several journals in our collection, and adds to the comprehensive full-text linking options available through Article Linker. Faculty and students at Central Washington University are delighted with the extended access."

Amy Brand, Director of Business Development for Crossref, added, "We are delighted to have Serials Solutions as an affiliate, and to have Crossref integrated into their Article Linker software. OpenURL link resolvers require accurate and up-to-date metadata to enable librarians and patrons to link into a library's content menus at the article level. Adding the Crossref system to Article Linker supplements the reliable full-text links already available to librarians and patrons."

Article Linker leverages the comprehensive data tracked in Serials Solutions' A-to-Z title list to present users with a single, comprehensive interface for linking to full-text articles, journals, and e-journal databases. Patrons can also access content by linking to document delivery services, library union, lists, interlibrary loan, OPACs, and web search engines. More information about Article Linker can be found at https://web.archive.org/web/20060407011234/https://www.serialssolutions.com/articlelinker.asp.

Serials Solutions Inc. provides complete e-journal access services to over 1000 libraries worldwide. Founded by a librarian for librarians, Serials Solutions helps librarians and patrons find and use electronic content through comprehensive A-to-Z title lists, full MARC records for e-journals, Article Linker™ Serials Solutions' comprehensive OpenURL link resolver, and Journal Linker™ a journal-level link resolver. For more information, please visit [www.serialssolutions.com](https://www.proquest.com/) or call 1-866-SERIALS.





---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
