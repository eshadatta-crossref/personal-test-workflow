+++
title = "Crossref Drops DOI Retrieval Fees As Steep Growth In Adoption Continues"
date = "2003-11-15"
parent = "News"
weight = 1
+++

*15 November 2003*

**LYNNFIELD, MA, November 15, 2003** -- Crossref, the cross-publisher reference linking service, announced today that it will drop its DOI retrieval fees for all members and affiliates starting in January 2004. This move gives all Crossref users unlimited access to DOIs, and is particularly significant for secondary publishers, as DOI links from citations and bibliographic databases to full text are expected to increase greatly as a result.

The decision by the Crossref Board to drop DOI retrieval fees is the latest strategic move intended to open access to the Crossref system. In May of this year, Crossref made library accounts on its system completely free. According to Ed Pentz, Crossref's Executive Director, "Crossref is always evolving. Our linking network ranges across all scholarly and professional disciplines, serves libraries at no charge, and by dropping DOI retrieval fees, will better suit the needs of secondary publishers. Crossref will continue to evolve, but our mission won't change -- to improve the user's experience in navigating content online." Crossref integrates with the OpenURL, and several local linking platforms now incorporate it; these include ExLibris, Serials Solutions, SISIS, Endeavor, Fretwell Downing, SIRSI, and Openly Informatics.

Crossref continues to experience dramatic membership growth. Fifty scholarly and professional publishers have signed on in the last six months alone, bringing the total members to 250. Some of the newest members are: University of California Press, Walter de Gruyter, Ashley Publications, the International Journal of Psychoanalysis, the Human Factors & Ergonomics Society, the National Strength & Conditioning Association, ASTM International, Guilford Publications, the Society for Neuroscience, the Canadian Academic Accounting Association, the Japan Endocrine Society, the American Marketing Association, the European Association of Cardiothoracic Surgery, the Indiana University Mathematics Journals, and American Scientist.

Smaller publishers, in particular, are seeing obvious gains from participating in a global interlinking network. According to Bohumir Valter, Editorial Manager of the Collection of Czechoslovak Chemical Communications, an interdisciplinary chemistry journal published by the Czech Academy of Sciences: "After two years in Crossref, we see a significant increase in traffic to our journal website, meaning that the articles published in our journal are more accessible than before thanks to the reference linking service."

Additionally, several hundred thousand book and conference proceeding DOIs have been added to the Crossref system, including 700 books from Oxford University Press. With these additions, Crossref now covers over 8,700 journals and nearly 10 million individual content items. To see a book chapter DOI in action, click on http://dx.doi.org/10.1002/0470841559.ch1 for an example from John Wiley & Sons, or http://dx.doi.org/10.1093/0198297122.001.0001 for an example from OUP.
---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
