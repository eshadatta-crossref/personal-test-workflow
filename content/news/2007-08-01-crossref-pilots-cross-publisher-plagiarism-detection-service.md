+++
title = "Crossref Pilots Cross-Publisher Plagiarism Detection Service"
date = "2007-08-01"
parent = "News"
weight = 1
+++

*01 August 2007*

**Lynnfield, MA. August 1, 2007** -- Crossref, the association behind publishing's preeminent shared digital infrastructure,
announced today that it had entered into an agreement with [iParadigms](https://web.archive.org/web/20070819002242/http://www.iparadigms.com/), the company that runs Turnitin and iThenticate, to develop a system that allows scholarly and professional publishers to verify the originality of
submitted and published works. The service, when it is launched, will be called "CrossCheck." Six leading academic publishers have agreed to allow their full-text content to be indexed for the pilot, and to test the service along several measures.

The six participating publishers are: the [Association for Computing Machinery](https://www.acm.org/), [BMJ Publishing Group Ltd.](https://www.bmj.com/company/), [Elsevier](http://www.elsevier.com), the [Institute of Electrical and Electronics Engineers](http://www.ieee.org), [Taylor & Francis](http://www.taylorandfrancisgroup.com/), and [Wiley-Blackwell](http://www.wiley.com).

According to Ed Pentz, Crossref's Executive Director, “We’re quite excited about the launch of the CrossCheck pilot.  For the first time we are applying the principles of publisher collaboration beyond reference linking.  Crossref is in a unique position to help create a comprehensive database of published academic content because of its broad membership.   CrossCheck will be a valuable service for our publishers enabling them to verify the originality of content.  But the real beneficiaries of the service will be researchers and scholars who want trustworthy online content.  On the technology side, iParadigms is the leading provider of the technology that we need.”

John Barrie PhD, President and CEO of iParadigms, adds: “We are honored to be working with some of the most prestigious academic publishers in the world to create a service designed to improve the quality and integrity of STM research.  CrossCheck is the next logical evolution of the technology behind Turnitin -- a service that has already become part of how secondary and higher education work.”

Headquartered in Oakland, CA, iParadigms, LLC (http://www.iparadigms.com) is the leading provider of web-based solutions to check documents for originality and plagiarism. The company’s products include Turnitin, an internet service used by tens of millions of students and faculty for the digital assessment of academic work, and iThenticate, an internet service which enables companies to determine originality and check documents for misappropriation.



---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
