+++
title = "Gallery of Reference Pages"
date = "2000-06-02"
parent = "News"
weight = 1
+++

*02 June 2000*

**The central source for reference linking**  
**Gallery of Reference Pages**


The following publishers have enabled reference links utilizing the Crossref system. The sample articles below have been provided by the publishers to demonstrate Crossref-enabled links. The links will appear in a few different formats, depending on the preferences of each publisher (for example: Crossref button or "Article"). While the articles are being hosted on the Crossref website the reference links are real and link to the cited publishers' websites.

* Oxford University Press  [EMB_J](https://web.archive.org/web/20000816215005/http://www.emboj.org/cgi/content/full/17/6/1675)

* IDEAL
Academic Press: [Molecular Therapy](https://web.archive.org/web/20000816215005/http://www.crossref.org/demos/ideal/mthe_2000_0049.htm)  
Churchill Livingstone: [Growth Hormone & IGF Research](https://web.archive.org/web/20000816215005/http://www.crossref.org/demos/ideal/ghir_1999_0129.htm)  
WB Saunders: [European Journal of Pain](https://web.archive.org/web/20000816215005/http://www.crossref.org/demos/ideal/eujp_1999_0154.htm)

* Blackwell Science/Synergy  
[European Journal of Biochemistry](https://web.archive.org/web/20000816215005/http://www.crossref.org/demos/blackwell/blackwell.htm)

* ScienceDirect [Elsevier]  
[Pharmacology & Therapeutics](https://web.archive.org/web/20000816215005/http://www.crossref.org/demos/sd/cr_demo_art.htm)

* LINK [Springer-Verlag]  
World Journal of Surgery

* Wiley InterScience  
[Developmental Dynamics](https://web.archive.org/web/20000816215005/http://www.crossref.org/demos/wiley/DVDY/DVDY72502433f.html)  
[Human Mutation](https://web.archive.org/web/20000816215005/http://www.crossref.org/demos/wiley/humu63003542e.html)

© 2000 PILA, Inc.
---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
