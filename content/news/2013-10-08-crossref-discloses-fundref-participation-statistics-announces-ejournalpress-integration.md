+++
title = "Crossref discloses FundRef participation statistics; announces eJournalPress integration"
date = "2013-10-08"
parent = "News"
weight = 1
+++

*13 October 2013*

**October 8, 2013, Frankfurt, Germany** - Crossref announced today the results of early participation in FundRef, its new funder identification service.

The number of publishers signed up to participate in FundRef as of the first of October was 19. These publishers have already deposited 28,000 bibliographic records containing funding data, 18,000 of which include at least one FundRef Funder ID.

The FundRef deposits include many funder names that are not already in the [FundRef Registry](/services/funder-registry), an openly available taxonomy of more than 4000 funder names contributed by Elsevier for the FundRef service. FundRef personnel are evaluating these new names to determine if they are variations of names already in the taxonomy, or new additions. The first revision to the taxonomy is due to be released this month, and will incorporate changes that better reflect the hierarchy of some funding bodies. Future revisions will add additional funding bodies.

In the meantime, eJournalPress has announced that it has integrated FundRef functionality into its manuscript tracking system. “Our systems collect and track a wide range of metadata and files, tailored to the needs of our clients, who are publishers of scholarly journals,” remarked Joel Plotkin, Founder and CEO of eJournalPress. “Implementing a standardized way to store funding and grant or award information is a logical best practice. We enjoy working with forward-thinking organizations like Crossref who implement beneficial initiatives and services for scholarly journal publishers, helping our clients manage and grow their journals.”

"FundRef is an extremely important initiative. We are working very hard to share information with publishers about how they can add funding information to the metadata records they already deposit with Crossref,” said Crossref Executive Director Ed Pentz.  “This timely integration by eJournalPress will help publishers to collect accurate funding data from authors. We look forward to seeing growth in the FundRef metadata as journals using eJournalPress start to deposit funding information."

Publishers must be members of Crossref to participate in FundRef, which is an optional service. There is no cost to deposit funding data. Publishers who wish to display FundRef data in Crossmark will only pay established Crossmark fees*. Publishers may submit data for any content accepted by Crossref, whether current or previously published. More information, including the complete list of participants and links to the FundRef Registry and a [recorded introductory webinar is available](/services/funder-registry).

**About eJournal Press**
Founded in 1999, eJournalPress (www.ejpress.com) is focused on providing individualized online peer review and journal production tracking solutions to the scholarly publishing community. Our goal is to find the best solutions for each journal’s needs through innovative technology. Our Continuous Innovation Process allows us to tailor our software to fit any workflow.

---
* Update: Extra fees for Crossmark have been removed as of 1st January 2020.

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
