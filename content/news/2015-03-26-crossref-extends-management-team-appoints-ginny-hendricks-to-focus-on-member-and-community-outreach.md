+++
title = "Crossref Extends Management Team, Appoints Ginny Hendricks To Focus on Member and Community Outreach"
date = "2015-03-26"
parent = "News"
weight = 3
+++

*26 March 2015*

**26 March 2015, Lynnfield, MA** – Crossref, the global not-for profit digital hub for scholarly communications, is pleased to announce the addition of Ginny Hendricks to its management team in the newly-created role of Director of Member and Community Outreach, where she will be responsible for marketing, business development, member services, and product support. The appointment reflects Crossref’s mission to innovate for the future of scholarly content, and to foster collaboration among an increasingly diverse community of publishers, researchers, authors, libraries, funders, and beyond.

Executive Director, Ed Pentz, says:  

> I’m very pleased Ginny is joining the Crossref team; her international experience, background in scholarly publishing, and digital marketing expertise, make her the perfect person to spearhead the Crossref brand, lead outreach around the world, and contribute to our ongoing success.  

Ginny Hendricks says:  

> Crossref is indispensable to the reliable running and progression of scholarly communications and its scope is broadening to accommodate changing publisher needs and serve the wider communities. I’m excited to work with some great people and to be able to contribute to such a central part of scholarly publishing.  

Ginny has run Ardent Marketing for nine years where she consulted with publishers to develop multichannel marketing plans, brand and launch online products, and build engaged communities. Prior to consulting she managed the launch of Scopus at Elsevier, where she established advisory boards and outreach programs with library and scientific communities. In 1998 Ginny started an early e-resources help desk for Blackwell’s information Services and later led training and communication programs for Swets’ digital portfolio in Asia Pacific, Middle East, and Africa. She’s lived and worked in many parts of the world and has managed globally dispersed creative, technical, and commercial teams. She co-hosts the Scholarly Social networking events in London, and is considering finishing her Master’s of Science in Digital Marketing Communications. Ginny will start on Monday 30th March and can be reached via twitter [@GinnyLDN](http://www.twitter.com/ginnyldn) or email [ginny@crossref.org](mailto:ginny@crossref.org).

**About Crossref**  
Crossref [(www.crossref.org)](/) serves as a digital hub for the scholarly communications community. A global not-for profit membership organization of scholarly publishers, Crossref's innovations shape the future of scholarly communications by fostering collaboration among multiple stakeholders. Crossref provides a wide spectrum of services for identifying, locating, linking to, and assessing the reliability and provenance of scholarly content.

---

Please contact [our communications team](mailto:news@crossref.org) with any questions.
