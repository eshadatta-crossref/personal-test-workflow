+++
title = 'Crossref and DataCite announce new initiative to accelerate the adoption of DOIs for data publication and citation'
date = "2014-11-10"
parent = "News"
weight = 1
+++

*10 November 2014*

**10 November 2014, Oxford, UK** – In 2014 DataCite celebrates its fifth year of operation and Crossref its fifteenth. Together we have registered almost 75 million DOIs. Today the two organisations are committing to accelerate the adoption of DOIs for data publication and citation.

Data are essential building blocks of scholarship and research in the sciences, medicine, technology, social sciences, and humanities. DataCite and Crossref will work together to ensure that researchers can seamlessly navigate among all research results including articles and data and to make data a first class, identifiable, referenceable, and citable element in the scholarly record.

DataCite and Crossref have agreed to collaborate to:

* Enhance the interoperability of their respective systems in order to make it easier for publishers, data centres, libraries and third parties to integrate with the scholarly DOI ecosystem.
* Provide comprehensive support for interlinking between articles and data.
* Develop open APIs and open source tools to surface citations and other relationships between publications and data sets.
* Integrate into their services other existing scholarly communications initiatives such as ORCID and Crossref’s FundRef.
* Develop systems, workflows and best practices for using DOIs to reference large, highly granular and dynamic data.

Geoffrey Bilder, Crossref’s Director of Strategic Initiatives, said “Nobody in the scholarly communications community can have failed to notice the surge in interest in data publication and the widespread desire to link publications to their underlying data. About once a month we are invited to participate in yet another conference, workshop or initiative to address the issues around data and data citation. This is a fast moving area, and in order to meet the needs of researchers and our respective memberships, we need to make sure that Crossref and DataCite are working together closely, quickly and efficiently.”

Adam Farquhar, President of DataCite, says “Collectively, DataCite and Crossref manage nearly 75 million DOIs that identify critical research objects in the scholarly record. It is vital to the DOI ecosystem to ensure our systems are interoperable and that we make the most efficient use of our resources through collaboration and sharing. This is particularly true as we try to meet the requirements raised in the fast-moving community that is focused on data publication, citation, and reuse.”

**About Datacite**  
DataCite (www.datacite.org) is a global consortium that assigns persistent identifiers to research data. DataCite makes research better by enabling people to find, share, use, and cite data. DataCite is a leading global membership organization that engages with stakeholders including researchers, scholars, data centers, libraries, publishers, and funders through advocacy, guidance and services.

**About Crossref**  
Crossref (www.crossref.org) serves as a digital hub for the scholarly communications community. A global not-for profit membership organization of scholarly publishers, Crossref's innovations shape the future of scholarly communications by fostering collaboration among multiple stakeholders. Crossref provides a wide spectrum of services for identifying, locating, linking to, and assessing the reliability and provenance of scholarly content.  



---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
