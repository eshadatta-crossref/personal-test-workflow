+++
title = "Crossref Launches Free Citation Look-up Tool For Bloggers"
date = "2008-02-12"
parent = "News"
type = "News"
weight = 1
+++

*12 February 2008*
## Crossref Launches Free Citation Look-up Tool For Bloggers

**Lynnfield, MA . February 12, 2008** -- Crossref, the association behind the well-known publisher linking network, announced today that it had launched the beta version of a new plug-in that allows bloggers to look up and insert DOI®-enabled citations in the course of authoring a blog.

The plug-in, which is available for download at: [https://sourceforge.net/projects/crossref-cite/](https://sourceforge.net/projects/crossref-cite/), allows the blogger to use a widget-based interface to search Crossref metadata using citations or partial citations. The results of the search, with multiple hits, are displayed and the author can then either click on a hit to follow the DOI to the publisher’s site, or click on an icon next to the hit to insert the citation into their blog entry (as either a full citation or as a short  “op. cit.”).

According to Geoffrey Bilder, Crossref’s Director of Strategic Initiatives, “Crossref is helping jumpstart the process of citing the formal literature from blogs. While there is a growing trend in scientific and academic blogging toward referring to formally published literature, until now there were few guidelines and very few tools to make that process easy.”

At present, the Crossref plug-in is available for the WordPress platform, and a Moveable Type version is under development. The tool is designed with the bulk of the functionality contained in a shared back-end hosted at Crossref, making the plug-in GUI readily portable to other blogging and authoring platforms.

About Crossref: Crossref is a non-profit membership association founded and directed by publishers. Its mission is to enable easy identification and use of trustworthy electronic content by promoting the cooperative development and application of a sustainable infrastructure. Crossref operates a cross-publisher citation linking system, and is the official DOI name registration agency for scholarly and professional content. More information is available at http://www.crossref.org.

_DOI® and DOI.ORG® are registered trademarks and the DOI> logo is a trademark of The International DOI Foundation_

_Crossref® and Crossref.org® are registered trademarks and the Crossref logo is a trademark of PILA (Publishers International Linking Association_




---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
