+++
title = "Reference Linking Service to Aid Scientists Conducting Online Research"
date = "1999-11-16"
parent = "News"
weight = 1
+++

*16 November 1999*
_Scientific and Scholarly Publishers Collaborate to Offer Ground-Breaking Initiative_

**New York, N.Y. November 16, 1999** - Twelve leading scientific and scholarly publishers announced today that they are collaborating on an innovative, market-driven reference-linking initiative that will change the way scientists use the Internet to conduct online research. The reference-linking service represents an unprecedented, cooperative effort among Academic Press, a Harcourt Science and Technology Company (NYSE:H); American Association for the Advancement of Science (the publisher of Science); American Institute of Physics (AIP); Association for Computing Machinery (ACM); Blackwell Science; Elsevier Science (NYSE:ENL) (NYSE:RUK); The Institute of Electrical and Electronics Engineers, Inc. (IEEE); Kluwer Academic Publishers( a Wolters Kluwer Company); Nature; Oxford University Press; Springer-Verlag; and John Wiley & Sons, Inc. (NYSE:JWa) (NYSE:JWb). It is expected to launch during the first quarter of 2000.  

Researchers will be able to move easily from a reference in a journal article to the content of a cited journal article, typically located on a different server and published by a different publisher. At the outset, approximately three million articles across thousands of journals will be linked through this service, and more than half a million more articles will be linked each year thereafter. This will enhance the efficiency of browsing and reading the primary scientific and scholarly literature. Such linking will enable readers to gain access to logically related articles with one or two clicks -- an objective widely accepted among researchers as a natural and necessary part of scientific and scholarly publishing in the digital age.  

The reference-linking service will be run from a central facility which will be managed by an elected Board and will operate in cooperation with the International Digital Object Identifier (DOI) Foundation. It will contain a limited set of metadata, allowing the journal content and links to remain distributed at publishers' sites. Each publisher will set its own access standards, determining what content is available to the researcher following a link (such as access to the abstract or to the full text of an article, by subscription, document delivery, or pay-per-view, etc.). The service is being organized as a not-for-profit entity to safeguard the independence of each participating publisher to set their own access standards and conditions.  

The service, which is based on a prototype developed by Wiley and Academic Press, was developed in cooperation with the International DOI Foundation and builds on work by the Association of American Publishers and the Corporation for National Research Initiatives. It takes advantage of the DOI standard and other World Wide Web standards and Internet technology. By taking a standards-based approach the international initiative is confident that the sophisticated demands of the readers of scientific and scholarly journals for linking of references can be implemented broadly and rapidly.

Representatives of the participating publishers and the International DOI Foundation are in active discussions with other scientific and scholarly primary journal publishers to make this a broad-based, industry-wide initiative. Through the reference-linking service publishers will have an easy, efficient and scalable means to add links to their online journals.

**Interviews will be arranged upon request.**  

Contact:  
Ken Metzner  
(619) 699-6830  
kmetzner@acad.com  
[kmetzner@acad.com](mailto:kmetzner@acad.com)   

QUOTE FROM DR. PIETER BOLMAN, PRESIDENT OF ACADEMIC PRESS
"Using open standards will allow any primary publisher to easily participate in the reference-linking service while retaining control of their content. Linking in this way enables 'virtual aggregation' and removes the need for libraries to have multiple electronic subscriptions to the same content through different vendors. Readers of online journals will be able to go directly from references to the original content at publishers' sites."  

AMERICAN ASSOCIATION FOR THE ADVANCEMENT OF SCIENCE  
(THE PUBLISHER OF SCIENCE)   
www.sciencemag.org  

Contact:  
Nan Broadbent  
Director, News and Information  
(202) 326-6440  
[nbroadbe@aaas.org](mailto:nbroadbe@aaas.org)

QUOTE FROM RICHARD NICHOLSON, EXECUTIVE OFFICER OF THE AMERICAN ASSOCIATION FOR THE ADVANCEMENT OF SCIENCE AND PUBLISHER OF SCIENCE:
"This reference-linking service marks the natural progression in online publishing by science journals. More researchers today are using the Internet in their work and this service will allow them to do so more quickly and efficiently. By providing this service we are able to fulfill one of our long-time goals -- meeting the needs of the scientific community.”  

QUOTE FROM FLOYD E. BLOOM, MD, EDITOR-IN-CHIEF OF SCIENCE:
"This cooperative service creating links among online scientific papers should be recognized as a major step forward in gaining access to the recent body of literature, greatly surpassing any other private or government sponsored resource. Scholars everywhere should rejoice."  

AMERICAN INSTITUTE OF PHYSICS (AIP)
[www.aip.org](http://www.aip.org)  

Contact:   
Tim Ingoldsby  
Director of Business Development  
(631) 576 2266  
[TINGOLDSBY@AIP.ORG ](mailto:TINGOLDSBY@AIP.ORG)  

QUOTE FROM EXECUTIVE DIRECTOR AND CEO, MARC BRODSKY:  

"When physicists first conceived of the World Wide Web, they envisioned that it would allow researchers to easily follow ideas from concept through completion. This initiative is a giant step toward the realization of that vision."  

ASSOCIATION FOR COMPUTING MACHINERY  
www.acm.org  

Contact:  
Bernard Rous  
ACM Deputy Director of Publications  
(212) 626--0660  

BLACKWELL SCIENCE  
https://www.blackwellpublishing.com/

Contact:  
Robert Campbell  
Managing Director  
+44 1865 206000  
[robert.campbell@blacksci.co.uk](mailto:robert.campbell@blacksci.co.uk)    

QUOTE FROM ROBERT CAMPBELL, GROUP MANAGING DIRECTOR  

"We are delighted that the STM publishing community has been able to follow up so quickly on the Press Release issued by the STM Association at the start of the International Book Fair at Frankfurt. The enthusiasm for this initiative from the societies for whom we publish has been extremely encouraging."  

ELSEVIER SCIENCE  
www.elsevier.com  

Contact:  
Karen Hunter  
Senior Vice President, Elsevier Science  
(212) 633-3787  
[k.hunter@elsevier.com](mailto:k.hunter@elsevier.com)  

QUOTE FROM JOHN REGAZZI, MANAGING DIRECTOR, ELECTRONIC PUBLISHING, ELSEVIER SCIENCE  

“It is said that 'a rising tide lifts all boats.' By working together to further enhance the value of electronic journals, primary publishers will improve the efficiency and productivity of all scientists.”  

THE INSTITUTE OF ELECTRICAL AND ELECTRONICS ENGINEERS, INC. (IEEE)  http://www.ieee.org  

Contact:  
Helen L. Horwitz  
Staff Director Corporate Communications  
(732) 562 6821  
[h.horwitz@ieee.org](mailto:h.horwitz@ieee.org)  

QUOTE FROM LLOYD “PETE” MORLEY, IEEE VICE PRESIDENT, PUBLICATIONS ACTIVITIES, AND INTERIM DEPARTMENT HEAD, ELECTRICAL ENGINEERING, THE UNIVERSITY OF ALABAMA AT TUSCALOOSA.  

“As a professional association, one of the IEEE's primary goals is to facilitate the dissemination of technical information. Participating in this reference linking initiative is an important way to achieve that goal. Adding new functions to journal articles like reference linking is a major step towards realizing the promise of electronic publishing.”

 INTERNATIONAL DIGITAL OBJECT IDENTIFIER FOUNDATION  
 http://www.doi.org   

Contact:  
Norman Paskin  
Director, International DOI Foundation  
+44 1865 843798  
[n.paskin@doi.org](mailto:n.paskin@doi.org)  

QUOTE FROM NORMAN PASKIN, DIRECTOR OF THE INTERNATIONAL DOI FOUNDATION:  

"The Foundation is very pleased to see the commitment to implement DOIs in this large-scale application by major publishing houses way so soon after we have defined a framework for such applications. Scientific communication is founded on persistent links between articles - citations - which create a permanent record of the relationships between scientists' publications. Building a system that embodies these links using unique persistent identifiers, actionable on the Web, is an excellent implementation of the DOI."  

 KLUWER ACADEMIC PUBLISHERS  
(A WOLTERS KLUWER COMPANY)  
http://wolterskluwer.com/  

Contact:  
Alexander Schimmelpenninck  
Director Corporate Communications  
Wolters Kluwer N.V.  
tel. +31 20 60 70 335  
[aschimmelpenninck@wolterskluwer.com](mailto:aschimmelpenninck@wolterskluwer.com)  

QUOTE FROM JEFF SMITH, PRESIDENT KLUWER ACADEMIC PUBLISHERS  

"This initiative is in line with Kluwer Academic Publishers' strategy to meet the specific requirements of scientists to have available a universe of linked scientific content and articles. Meanwhile, we preserve our independence in maintaining the special relationship we are building with our own group of customers."  

 OXFORD UNIVERSITY PRESS  
 http://www.oup.co.uk  

Contact:  
Martin J Richardson  
Publishing Director  
+44 (0)1865 267780  
[richarm@oup.co.uk](mailto:richarm@oup.co.uk)  

 NATURE  
www.nature.com  

Contact:  
Stefan von Holtzbrinck  
Managing Director, Nature Publishing Group  
+44 011 171 843 4632  

QUOTE FROM JOHANNES VELTEROP, PUBLISHING DIRECTOR NATURE:  

"This is extremely good news for the active scientists and researchers all over the world. The interest of the users of scientific information is put at the centre of the stage again, and publishers clearly recognise the imperative of serving the research community in the very best way the new technology allows."  

 SPRINGER-VERLAG  
 https://web.archive.org/web/20000815203225/http://link.springer-ny.com/  
 http://link.springer.de/  

QUOTE FROM RÐDIGER GEBAUER, PRESIDENT AND CEO, SPRINGER-VERLAG-NEW YORK  

“Springer has been a long-time supporter of the DOI and reference linking. We recommend that others join this initiative to help facilitate the rapid dissemination of scientific information. Journal article reference linking is just the beginning. This may be the watershed initiative that promotes cross-publisher communication to ultimately enhance the online users experience.”  

 JOHN WILEY & SONS, INC.   
 www.wiley.com  

Contact:  
Susan Spilka  
Corporate Communications Director  
(212) 850-6147  
[sspilka@wiley.com](mailto:sspilka@wiley.com)  

QUOTE FROM ERIC A. SWANSON, SENIOR VICE PRESIDENT, STM PUBLISHING:  

"We strongly encourage broad-based participation in this initiative by primary journal publishers. Publishers need only have online journal content with metadata that can be tagged with DOIs. This reference linking service will enable us to serve our customers better."  



---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
