+++
title = "Crossref’s FundRef launches: Publishers and funders track scholarly output"
date = "2013-05-28"
parent = "News"
weight = 1
+++

*28 May 2013*

*28 May 2013, Lynnfield, MA** — FundRef, the funder identification service from Crossref, is now available for publishers to contribute funding data and for retrieval of that information. FundRef is the result of collaboration between funding agencies and publishers that correlates grants and other funding with the scholarly output of that support.     

Publishers participating in FundRef add funding data to the bibliographic metadata they already provide to Crossref for reference linking. FundRef data includes the name of the funder and a grant or award number. Manuscript tracking systems can incorporate a taxonomy of 4000 global funder names, which includes alternate names, aliases, and abbreviations enabling authors to choose from a standard list of funding names. Then the tagged funding data will travel through publishers’ production systems to be stored at Crossref.

Director of the Office of Science and Technical Information of the U.S. Department of Energy, (DOE/OSTI) Walter Warnick commented, “FundRef is a great way for publishers and funding agencies to work together. Research agencies like DOE are accountable for the results of their expenditures to Congress and to the public. Publications in the peer-reviewed literature represent an important output of those expenses, but they have been difficult to track and quantify until now. Tagging this data in a searchable, standard way is a key step forward.”

FundRef helps researchers comply with funder requirements to report the results of financial support they have received. The Crossmark service enables publishers to display FundRef data in a standardized way. FundRef data is also freely available to funding organizations, publishers, libraries, research institutions, scholars, authors, and the public through Crossref’s search services, application programming interfaces (APIs), XML queries, and data distribution channels. A beta version of FundRef Search allows anyone to search the data from the publishers and funders that participated in the pilot for funder names and abbreviations: http://search.crossref.org/funding. In addition, [Crossref Metadata Search](https://search.crossref.org) allows search by grant number.

Fred Dylla, Executive Director of the American Institute of Physics, noted, “FundRef is a remarkable collaboration. It shows how everyone benefits when multiple stakeholders in the scholarly communication process work together. FundRef is feasible and cost effective because it builds on an infrastructure that was already in place among scholarly publishers. The result is that funding agencies and others can have access to this valuable information at no cost.”

“Of all of Crossref’s recent initiatives,” Crossref Executive Director Ed Pentz commented, “response to FundRef has been the most marked. Since we announced the pilot project a year ago, publishers and funding agencies have lined up to participate. Crossref’s existing metadata collection and distribution infrastructure has been leveraged to provide a missing piece of the research funding puzzle. Readers will be able to view funding information in a standard way through Crossmark at participating publishers’ sites without having to hunt for it. ”

Publishers must be members of Crossref to participate in FundRef, which is an optional service. There is no cost to deposit funding data. Publishers who wish to display FundRef data in Crossmark will pay the established Crossmark fees*. Publishers may submit data for any content accepted by Crossref, whether current or previously published.

Crossref member publisher Elsevier donated the taxonomy of funder names for the Funder Registry. Suggested changes to the taxonomy can be submitted to Crossref for consideration for inclusion in the taxonomy.  

FundRef currently includes a limited amount of data from the following pilot publishers and funders:

* US Department of Energy, Office of Scientific and Technical Information (DOE/OSTI)
* US National Aeronautics and Space Administration (NASA)
* US National Science Foundation (NSF)
* Wellcome Trust
* AIP Publishing
* American Psychological Association
* Elsevier
* IEEE
* Nature Publishing Group
* Oxford University Press
* Wiley-Blackwell

Additional Crossref member publishers are expected to start adding FundRef metadata representing a broad array of funding bodies between now and the end of the year.
More information is available at [https://www.crossref.org/education/funder-registry/](/education/funder-registry/)

---
* Update: Extra fees for Crossmark removed 1st January 2020.

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
