+++
title = "Crossref™ Search Pilot Now Includes 29 Publishers, 3.4 Million Research Articles"
date = "2004-09-21"
parent = "News"
weight = 1
+++

*21 September 2004*

**LYNNFIELD, MA, September 21, 2004** -- Crossref, the reference-linking service for scholarly publishing, announced today that its pilot initiative in collaboration with Google has added 20 additional publishers during the past four months. There are now 29 publishers participating in the Crossref Search pilot that includes over 3.4 million scholarly research articles. Users can search the full text of high-quality, peer-reviewed journal articles, conference proceedings, monographs, and other resources covering the full spectrum of scholarly research.
“The involvement of so many publishers highlights the value of the Crossref Search pilot. The pilot provides targeted, interdisciplinary, cross-publisher search that makes it easier to find relevant research material,” said Ed Pentz, Executive Director of Crossref. “In addition to adding new publishers, Crossref has started to get very valuable feedback on the pilot service from researchers, librarians, and scientists.”
Crossref Search is available to all users, free of charge, on the websites of participating publishers, and encompasses current journal issues as well as back files. The results are delivered from the regular Google index but filter out everything except the participating publishers’ content, and links to the content on publishers’ websites via DOIs (Digital Object Identifiers) or regular URLs. Crossref itself doesn’t host any content or perform searches – Crossref works behind the scenes with Google to facilitate the crawling of content on publishers’ sites, and sets the policies and guidelines governing publisher participation in the initiative. As well as enabling Crossref Search, the partnership with Google means that full-text content from the publishers is also referenced by the main Google.com index in its more general searches. Participating publishers include:

American Physical Society   
Annual Reviews
Ashley Publications   
Association for Computing Machinery   
BioMed Central   
Blackwell Publishing
BMJ (British Medical Journal) Publishing Group  
Cambridge University Press   
Cold Spring Harbor Laboratory Press   
FASEB (Federation of American Societies for Experimental Biology)  
IEEE (Institute of Electrical and Electronics Engineers, Inc.)  
INFORMS (Information Management Specialists, Inc.)  
Institute of Physics Publishing   
International Union of Crystallography   
Investigative Ophthamology and Visual Science   
Journal of Clinical Oncology  
Lawrence Erlbaum Associates   
Medicine Publishing Group   
Nature Publishing Group   
Oldenbourg Wissenschaftsverlag   
Oxford University Press   
PNAS (Proceedings of the National Academy of Sciences)  
Royal College of Psychiatrists   
Springer-Verlag   
Taylor & Francis   
University of California Press   
University of Chicago Press   
Vathek Publishing   
John Wiley & Sons, Inc.   

Publishers participating in the Crossref Search pilot are surveying users of the tool on an on-going basis. “Initial responses are very positive and suggest that Crossref Search is meeting or exceeding user expectations. Some users have indicated a desire for more publishers’ scholarly material to be included—something we are working toward with the addition of 20 publishers,” added Mr. Pentz.  

The Crossref Search pilot began in January 2004 with nine publishers. It will run through the end of 2004 to evaluate functionality and to gather feedback from scientists, scholars and librarians for the purpose of fine-tuning the program. Participating publishers are also investigating how DOIs can be used to improve indexing of content and enable persistent links from search results to the full text of content at publishers’ sites. Crossref Search may work with other leading search technologies in the future.

---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
