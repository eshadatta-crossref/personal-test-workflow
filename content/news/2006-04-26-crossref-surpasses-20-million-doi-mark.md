+++
title = "Crossref Surpasses 20 Million DOI Mark"
date = "2006-04-26"
parent = "News"
weight = 1
+++

*26 April 2006*

**LYNNFIELD, MA, April 26th, 2006** -- Crossref, the citation linking service, announced today that over 20 million content entities had been registered in the Crossref system since its inception in early 2000. The majority of these Digital Object Identifiers (DOIs) are assigned to online journal articles. However, over 1.6 million DOIs are assigned to conference proceedings and books, at the chapter as well as title level.

Within the past year, Crossref began supporting assignment of DOIs to technical reports, working papers, dissertations, standards, and data elements. Among the DOIs most recently registered in Crossref are persistent identifiers for 38,000 protein description pages within the Protein Data Bank, and 27,000 working papers published by the Social Science Research Network.

JSTOR, the scholarly journal archive, has added as of today over 640,000 DOIs to Crossref. According Michael Spinella, JSTOR's Executive Director, “We are pleased to be working with Crossref to register DOIs on behalf of many of the publishers that contribute content to the JSTOR archive. Many of the journals included in JSTOR stretch back to the nineteenth century, and it is exciting to increase the pathways to this material through stable and persistent links. This begins the realization of a goal we have heard expressed by all the communities we work with: publishers, libraries, and scholars.”

The 20 millionth DOI, [http://dx.doi.org/10.1515/BC.2006.059](http://dx.doi.org/10.1515/BC.2006.059), was registered by de Gruyter publishing, for their journal Biological Chemistry.

Crossref hit the 10 million DOI mark back in January of 2004, after roughly four years in operation. Since then, the rate of growth in DOI creation across the scholarly publishing community has accelerated considerably, with the next 10 million DOIs being created and registered in just over two years.



---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
