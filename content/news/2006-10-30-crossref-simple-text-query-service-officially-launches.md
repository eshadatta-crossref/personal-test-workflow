+++
title = "Crossref Simple-Text Query Service Officially Launches"
date = "2006-10-30"
parent = "News"
weight = 1
+++

*30 October 2006*

**LYNNFIELD, MA – October 30, 2006** Crossref, the reference-linking network for scholarly and professional publishers, is pleased to announce the launch of its freely available Simple-Text Query service to facilitate DOI look-up for researchers and publishers. In partnership with Inera, Crossref has deployed a custom version of Inera’s eXtyles refXpress™ that parses unstructured, simple-text references into granular and valid XML and returns any matching DOIs for those references. The interface has been running on a more limited, trial basis since February of this year.

The link to the Simple-Text Query service is featured on Crossref’s homepage, www.crossref.org. A simple cut-and-paste form accepts references formatted in a wide range of bibliographic styles and returns the DOI for the publication if one is found in Crossref. One or more references may be pasted into the form on this page. For editorial purposes, users can easily check the accuracy of a reference by clicking on the returned DOI and viewing the bibliographic information available directly from the publisher. Bruce Rosenblum, CEO of Inera, commented, "We're delighted to partner with Crossref, providing technology that does not require structured references and produces immediate results."

In addition to serving researchers who wish to check references and add DOIs prior to publication, many smaller publishers experience this interface as a viable alternative to Crossref's XML-based batch query interface. According to Amy Brand, Crossref’s Director of Business and Product Development, “We have seen with the Simple-Text Query trial that more publishers are now able to implement reference linking. Crossref serves a very broad range of publishers, including many members with limited resources and technical expertise, and we are always striving to lower the technological barriers to full participation in Crossref for our members.” The member-only version of the service will soon include a file-upload capability as well.

About Inera Inc. - Since 1992, Inera (http://www.inera.com) has focused on supplying sophisticated editorial and production solutions to the publishing industry. Inera's eXtyles® product family provides integrated tools for Microsoft Word and content management systems that automate editorial, reference linking, and XML production processes. eXtyles products are used in the production of over five hundred prestigious journals worldwide.





---

 Please contact [our communications team](mailto:news@crossref.org) with any questions.
