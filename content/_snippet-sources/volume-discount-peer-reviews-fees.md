If you register multiple peer reviews for the same article, then volume discounts apply. The prices and discounts are different depending on whether you are the publisher of the article being reviewed.

Volume discounts don’t apply across reviews for different articles.


**Registered by title publisher**

|Total number of registered DOIs per article | Registration fee per record (current and backfile)|
| :------| :-----------:|
|First peer review against single article |USD 0.25|
|Second and all further peer reviews against same article|USD 0.00|

**Registered by not the title publisher**

|Total number of registered DOIs per article | Registration fee per record (current and backfile)|
| :------| :-----------:|
|First peer review against single article |USD 1.00|
|Second peer review against same article|USD 0.25|
|Third and all further peer review against same article|USD 0.00|
