

As a DOI is a persistent identifier, the DOI string can't be changed after it's been registered. It's therefore important that your DOI string is opaque and doesn't include any human-readable information. This means that the suffix should just be a random collection of characters. It should not include any information about the work that could be changed in the future,  to avoid a difference between the information in the DOI string, and the information in the metadata. 

For example, `10.5555/njevzkkwu4i7g` is opaque (and correct), but `10.5555/ogs.2016.59.1.1` is not opaque (and not correct); it encodes information about the publication name and date which may change in the future and become confusing or misleading. So don’t include information such as publication name initials, date, ISSN, issue, or page numbers in your suffix string.
