Each document run through Similarity Check is charged at a per-document-checking fee, and there are volume discounts. There is a separate invoice for Similarity Check document checking fees. This invoice is sent annually in January for the previous years' usage. 

|Number of documents checked per year | Price per document up to 25,000 words |
| :------| :-----------:|
|1 - 100| USD 0.00|
|101 - 2,000| USD 0.75|
|2,001 - 25,000| USD 0.65|
|25,001 - 50,000| USD 0.55|
|50,001 - 100,000| USD 0.45|
|100,001 - 200,000| USD 0.30|
| >200,001| 	USD 0.25|
