> #### The Funder Registry and associated funding metadata allows everyone to have transparency into research funding and its outcomes. It’s an open and unique registry of persistent identifiers for grant-giving organizations around the world.

It is good practice for authors to acknowledge support for and contributions to their research in their published articles. This support may be financial, such as a grant or salary award; or practical, such as the use or loan of specialist facilities and equipment. They do this by listing the funding agency and the grant number somewhere in their article - usually the first or last page, or in the acknowledgments or footnotes section. Members contribute by depositing the funding acknowledgements from their publications as part of their standard metadata, together with the unique funder IDs listed in our Funder Registry. The deposit should include funder names, funder IDs, and associated grant numbers.

This means that anyone can make connections, for example, to identify which funders invest in certain fields of research. Funding data is also used by funders to track the publications that result from their grants.

<figure><a href='https://search.crossref.org/funding'><img src='/images/documentation/Funder-Registry.png' alt='Crossref Funder Registry' title='' width='75%'></a></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image23">Show image</button>
<div id="image23" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content"  >              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Funder-Registry.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>


The Crossref Funder Registry is an open registry of grant-giving organization names and identifiers, which you use to find funder IDs and include them as part of your metadata deposits. It is a [freely-downloadable RDF file](https://gitlab.com/crossref/open_funder_registry). It is [CC0-licensed](https://creativecommons.org/choose/zero/) and available to integrate with your own systems. Funder names from acknowledgements should be matched with the corresponding unique funder ID from the Funder Registry.

You can search funding metadata manually using our [funding data search](https://search.crossref.org/funding), or programmatically via our [REST API](/documentation/retrieve-metadata/rest-api). This data not only clarifies the scholarly record, but makes life easier for researchers who may need to comply with requirements to make their published results publicly available.

Watch the introductory Funder Registry animation in your language:
            <td align="center"><script src="https://fast.wistia.com/embed/medias/xwwkkxf2wa.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_xwwkkxf2wa popover=true popoverContent=link" style="display:inline"><a href="#">français</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/o53zscn0br.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_o53zscn0br popover=true popoverContent=link" style="display:inline"><a href="#">español</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/n7oxyxtii0.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_n7oxyxtii0 popover=true popoverContent=link" style="display:inline"><a href="#">português do Brasil</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/bdf5a63fro.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_bdf5a63fro popover=true popoverContent=link" style="display:inline"><a href="#">简体中文</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/utpq61tm95.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_utpq61tm95 popover=true popoverContent=link" style="display:inline"><a href="#">日本語</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/yqhdzz6h0j.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_yqhdzz6h0j popover=true popoverContent=link" style="display:inline"><a href="#">한국어</a></span></td>;
<td align="center"><script src="https://fast.wistia.com/embed/medias/tva0hw0d06.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_tva0hw0d06 popover=true popoverContent=link" style="display:inline"><a href="#">العربية</a></span></td>;
<td align="center"><script src="https://fast.wistia.com/embed/medias/fzlqzkxzml.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js"async></script><span class="wistia_embed wistia_async_fzlqzkxzml popover=true popoverContent=link" style="display:inline"><a href="#">bahasa Indonesia</a></span></td>; or English below.

<script src="//fast.wistia.com/embed/medias/ea9qsekzri.jsonp" async></script><script src="//fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_ea9qsekzri popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;width:100%">&nbsp;</span></div></div>

## Benefits of the Funder Registry<a id='00281' href='#00281'><i class='fas fa-link'></i></a>

There are many benefits of clear, transparent, and measurable information on who funded research, and where it has been published. The Funder Registry facilitates accurate funding metadata, which in turn enables multiple parties to better understand the research funding landscape:

* Readers and researchers can read and assess literature in the context of knowing who funded it;
* Research institutions can monitor the published outputs of their researchers;
* Publishers can track who is funding their authors, and check if they’re meeting funding mandates;
* Service providers can offer integrated time-saving features to their users; and
* Funders can easily track the reach and return of the work they have supported.

## How the Funder Registry works<a id='00282' href='#00282'><i class='fas fa-link'></i></a>

Authors acknowledge the funding sources for their research in their publications. Using the Funder Registry, members can [find the unique IDs for these funders, standardize this metadata and send it to us](/documentation/content-registration/administrative-metadata#00566).

The Funder Registry is donated by Elsevier, and is updated around every 4-6 weeks with new and updated funders. Existing entries are also reviewed to make sure that they are accurate and up-to-date. We can then make it openly available through our [funding data search](https://search.crossref.org/funding) and our [API](http://api.crossref.org). If you spot anything that doesn’t look right, please [let us know](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001642691). You can also [download a .csv file](https://doi.crossref.org/funderNames?mode=list) of the Funder Registry.
Using the Funder Registry, members can find the unique IDs for these funders, standardize this metadata and send it to us.

## Obligations and fees for the Funder Registry<a id='00284' href='#00284'><i class='fas fa-link'></i></a>

The Funder Registry is open to everyone. There are no member fees for depositing funding data. [Funder Registry search](https://search.crossref.org/funding) and our [REST API](http://api.crossref.org) are freely available.

Members must include the Funder ID from the Registry for each funder if it is present in the Registry. If a funder is not in the Registry and does not have an ID, include the name of the funder.

## How to participate in the Funder Registry<a id='00283' href='#00283'><i class='fas fa-link'></i></a>

To access the Funder Registry, you do not need to be a member, but you need to be a member to include funder iDs in your Crossref metadata. Anyone who’s interested can simply enter an organization’s name into the [Funder Registry search](https://search.crossref.org/funding) to view content connected to funding sources. The metadata in the Funder Registry is also openly available via our REST API, and as a downloadable RDF file. Learn more about [accessing the Funder Registry](/documentation/funder-registry/accessing-the-funder-registry).

**Depositing metadata** (members): collect funder names and grant numbers from your authors through your manuscript tracking system (or extract them from acknowledgements sections) and match them with the corresponding Funder IDs from the Registry. Once this is done, it’s easy to add these three additional pieces of metadata - funder name, funder id, and grant number - as additional metadata in the regular Crossref content registration service. Learn more about [how to collect and register funding data](/documentation/content-registration/administrative-metadata#00566).

Whenever you register content with us, make sure you include funder names and grant numbers in the submission:

* If you are using a content registration helper tool - [Crossref XML plugin for OJS](/documentation/content-registration/ojs-plugin/) or, the [web deposit form](/documentation/member-setup/web-deposit-form) - simply enter funder names and grant numbers in the relevant fields. For OJS, you must be running at least OJS 3.1.2 and have the [Crossref funding plugin](https://github.com/ajnyga/funding) enabled.
* If you’re depositing XML with Crossref, include your funding data in your XML.

**Retrieving metadata:** you can view the content that has cited a particular funding source by entering the organization’s name into the [Funder Registry search](https://search.crossref.org/funding). If you prefer a machine-readable query, use our [REST API](/documentation/retrieve-metadata/rest-api). If you have questions about how your organization appears in the registry then please [get in touch](/contact/). Learn more about the funder registry and our other services on our [funder community page](/community/funders).

<figure><img src='/images/documentation/Infographic-Funder-Registry.png' alt='Funder Registry infographic' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image24">Show image</button>
<div id="image24" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content"  >              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Infographic-Funder-Registry.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

[Download the Funder Registry factsheet](/pdfs/about-funder-registry.pdf), and explore [factsheets for other Crossref services and in different languages](/services/#00252).
