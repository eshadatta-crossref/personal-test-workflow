
## What types of records can be registered with Crossref?

We are working to make our input schema more flexible so that almost any type of object can be registered and distributed openly through Crossref. At the moment, members tend to register the following:

* [Books, chapters, and reference works](/documentation/research-nexus/books-and-chapters/): includes book title and/or chapter-level records. Books can be registered as a monograph, series, or set.
* [Conference proceedings](/documentation/research-nexus/conference-proceedings/): information about a single conference and records for each conference paper/proceeding.
* [Datasets](/documentation/research-nexus/datasets/): includes database records or collections.
* [Dissertations](/documentation/research-nexus/dissertations/): includes single dissertations and theses, but not collections.
* [Grants](/documentation/research-nexus/grants/): includes both direct funding and other types of support such as the use of equipment and facilities.
* [Journals and articles](/documentation/research-nexus/journals-and-articles/): at the journal title and article level, and includes supplemental materials as components.
* [Peer reviews](/documentation/research-nexus/peer-reviews/): any number of reviews, reports, or comments attached to any other work that has been registered with Crossref.
* [Pending publications](/documentation/research-nexus/pending-publications/): a temporary placeholder record with minimal metadata, often used for embargoed work where a DOI needs to be shared before the full content is made available online.
* [Preprints and posted content](/documentation/research-nexus/posted-content-includes-preprints/): includes preprints, eprints, working papers, reports, and other types of content that has been posted but not formally published.
* [Reports and working papers](/documentation/research-nexus/reports-and-working-papers/): this includes content that is published and likely has an ISSN.
* [Standards](/documentation/research-nexus/standards/): includes publications from standards organizations.
* You can also [establish relationships between different research objects](/documentation/principles-practices/best-practices/relationships/) (such as preprints, translations, and datasets) in your metadata. 

Learn more about all the metadata that can be included in these records with our [schema library and markup guides](/documentation/schema-library/markup-guide-metadata-segments/).
