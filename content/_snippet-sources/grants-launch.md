Launched in 2019, our registration service for grants allows funders and the wider research community to connect support with outputs and activities.

Whilst the scholarly community has adopted standard persistent identifiers (PIDs)---for people (e.g. ORCID), content (e.g. [DOIs](http://doi.org), PMIDs), and now organizations ([ROR](http://ror.org/)) including funders (Open Funder Registry)---the record of the award was not captured in a consistent way across funders worldwide. These awards were not easily linked up with the literature or with researchers or with institutions.

With tens of thousands of funding organizations in the Open Funder Registry, we needed to find a way for all of them---small and large, private and government---to register their grants, whilst making it easy for researchers to include this information in their submissions to publishers and data repositories.
