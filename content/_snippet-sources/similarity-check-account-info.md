The *Account Information* section shows important information about your iThenticate account, including your account name, account ID, and user ID. Please ignore the iThenticate account expiry date - we’re working with iThenticate to have this removed. The iThenticate account expiry date is set to 1 June 2022 by default.

<figure><img src='/images/documentation/SC-account-info-profile.png' alt='My Profile' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image106">Show image</button>
<div id="image106" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content"  >              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-account-info-profile.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

From *Account Info*, then *My Profile*, you can:

* Update your profile: this form shows your current details. To make changes, enter your password in the *Current Password* field at the top of the form.
* Change the name attributed to your account: enter the first and last name in the relevant fields. These fields are required, you cannot leave them blank.
* Change your email address: enter your email into the *email* field. This email address is used to send you important account information, so please make sure it is valid. This field is required, you cannot leave it blank.
* Add a photo to your account: click *Choose File*, and select the image file you want to upload.
* Change your password: enter your current password in the *Current Password* field, enter your new password in the *Change Password* field, and enter it again in the *Confirm Password* field.
* Click *Update Profile* to save your changes.
