> #### Reference linking enables researchers to follow a link from the reference list to other full-text documents, helping them to make connections and discover new things.

To link references, you don’t need to be a Crossref member. Reference linking means including Crossref DOIs ([displayed as URLs](/display-guidelines)) when you create your citation list. This enables researchers to follow a link from a reference list to other full-text documents, helping them to make connections and discover new things. And because it’s a DOI rather than just a link, it will remain persistent.

Instead of:

> Soleimani N, Mohabati Mobarez A, Farhangi B. Cloning, expression and purification flagellar sheath adhesion of Helicobacter pylori in Escherichia coli host as a vaccination target. Clin Exp Vaccine Res. 2016 Jan;5(1):19-25.

Display the DOI link:

> Soleimani N, Mohabati Mobarez A, Farhangi B. Cloning, expression and purification flagellar sheath adhesion of Helicobacter pylori in Escherichia coli host as a vaccination target. Clin Exp Vaccine Res. 2016 Jan;5(1):19-25. [https://doi.org/10.7774/cevr.2016.5.1.19](https://doi.org/10.7774/cevr.2016.5.1.19)

Because Crossref is all about rallying the scholarly community to work together, reference linking is an obligation for all Crossref members and for all current journal content (published during this and the two previous years). It is encouraged for other content types (such as books and conference proceedings), and for backfiles (published longer ago).

Watch the introductory reference linking animation in your language:
            <td align="center"><script src="https://fast.wistia.com/embed/medias/207ayqcpbz.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_207ayqcpbz popover=true popoverContent=link" style="display:inline"><a href="#">français</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/hgt88iqzwc.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_hgt88iqzwc popover=true popoverContent=link" style="display:inline"><a href="#">español</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/8xb8wioqak.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_8xb8wioqak popover=true popoverContent=link" style="display:inline"><a href="#">português do Brasil</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/7hw2lvn4oa.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_7hw2lvn4oa popover=true popoverContent=link" style="display:inline"><a href="#">简体中文</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/tr201sp62n.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_tr201sp62n popover=true popoverContent=link" style="display:inline"><a href="#">日本語</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/r482wakyxd.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_r482wakyxd popover=true popoverContent=link" style="display:inline"><a href="#">한국어</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/b44qot3lwq.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_b44qot3lwq popover=true popoverContent=link" style="display:inline"><a href="#">العربية</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/qa41njcoql.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_qa41njcoql popover=true popoverContent=link" style="display:inline"><a href="#">bahasa Indonesia</a></span></td>; or English below.

<script src="//fast.wistia.com/embed/medias/o346senq5z.jsonp" async></script><script src="//fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_o346senq5z popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;width:100%">&nbsp;</span></div></div>

## Benefits of reference linking<a id='00253' href='#00253'><i class='fas fa-link'></i></a>

Persistent links enhance scholarly communications. Reference linking offers important benefits:

* **Reciprocity**: members’ content is linked together and more discoverable because all members link their references.
Crossref acts as a clearinghouse, negotiating reciprocal agreements among all its members so that individual members can avoid the inconvenience of signing bilateral agreements to link to persistent content on other platforms. The result is a scholarly communications infrastructure that enables the exchange of ideas and knowledge.
* **Discoverability**: research travels further when everyone links their references. Because DOIs don’t break if implemented correctly, they will always lead readers to the content they’re looking for, including yours. When the DOIs are displayed, anyone can copy and share them.
This will enable better tracking of where and when people are talking about and sharing scholarly content, including in social media.

## Obligations and fees for reference linking<a id='00256' href='#00256'><i class='fas fa-link'></i></a>

There’s no charge for reference linking but it is an [obligation of membership](/documentation/metadata-stewardship/understanding-your-member-obligations/). Reference linking is required for all Crossref members and for all current journal content. We’d encourage you to also add reference linking for backfile journals, and for other content types.

<figure><img src='/images/documentation/Infographic-Reference-linking.png' alt='Reference linking infographic' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image19">Show image</button>
<div id="image19" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content"  >              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Infographic-Reference-linking.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>


[Download the reference linking factsheet](/pdfs/about-reference-linking.pdf), and explore [factsheets for other Crossref services and in different languages](/services/#00252).

## How to participate in reference linking<a id='00255' href='#00255'><i class='fas fa-link'></i></a>

Note that reference linking is not the same as [registering references](/documentation/content-registration/descriptive-metadata/references/) - learn more about [the differences](/blog/linking-references-is-different-from-depositing-references).

To link references, you do not need to be a member, but reference linking is an obligation for Crossref members. When your organization becomes a Crossref member, [look up the DOIs for your references](/documentation/reference-linking/how-do-i-create-reference-links/), and add the DOI (as a URL) to reference lists for your content items.

## Best practice for reference linking<a id='00257' href='#00257'><i class='fas fa-link'></i></a>

* Start reference linking within 18 months of joining Crossref
* Link references for backfile as well as current journal content
* Link references in non-journal content types such as books, and conference proceedings.
* Make sure the links in your references conform to our [DOI display guidelines](/display-guidelines)
