> #### The collective power of our members’ metadata is available to use through a variety of tools and APIs—allowing anyone to search and reuse the metadata in sophisticated ways.

Members register content with us to let the world know it exists. They send us information called [metadata](/documentation/metadata) which we collect and store in a standard way. Metadata does not include the full-text of the content itself, just information about the content. The metadata includes a Digital Object Identifier (DOI) in each record, which links to the content even if it moves to a new website. We make this metadata openly available via our APIs, which means people and machines can incorporate it into their research tools and services. While we collect and distribute metadata, we do not change members' metadata. Learn more about the metadata each member is depositing with us using our [Participation Reports](/documentation/reports/participation-reports).

Manuscript tracking services, search services, bibliographic management software, library systems, author profiling tools, specialist subject databases, scholarly sharing networks - all of these (and more) incorporate scholarly metadata into their software and services. They use our free APIs to help them get the most complete, up-to-date set of metadata from all of our publisher members. And of course, members themselves are able to use our free APIs too.

<figure><img src='/images/documentation/metadata-users-uses.png' alt='Metadata users and uses: metadata from Crossref APIs is used for a variety of purposes by many tools and services' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image32">Show image</button>
<div id="image32" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content"  >              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/metadata-users-uses.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>
