> #### A service provided by Crossref and powered by iThenticate—Similarity Check provides editors with a user-friendly tool to help detect plagiarism.

Our Similarity Check service helps Crossref members prevent scholarly and professional plagiarism by providing immediate feedback regarding a manuscript’s similarity to other published academic and general web content, through reduced-rate access to the [iThenticate](https://www.ithenticate.com/) text comparison software from Turnitin.

Only Similarity Check members benefit from this tailored iThenticate experience that includes read-only access to the full text of articles in the Similarity Check database for comparison purposes, discounted checking fees, and unlimited user accounts per organization.

Watch the introductory Similarity Check animation in your language:
            <td align="center"><script src="https://fast.wistia.com/embed/medias/511nhamb4m.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_511nhamb4m popover=true popoverContent=link" style="display:inline"><a href="#">français</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/k0i9jhk3gg.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_k0i9jhk3gg popover=true popoverContent=link" style="display:inline"><a href="#">español</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/2uxa9v0cmc.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_2uxa9v0cmc popover=true popoverContent=link" style="display:inline"><a href="#">português do Brasil</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/rm5o8f3fvd.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_rm5o8f3fvd popover=true popoverContent=link" style="display:inline"><a href="#">简体中文</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/qwbtokm5dr.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_qwbtokm5dr popover=true popoverContent=link" style="display:inline"><a href="#">日本語</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/wbtfhw58zy.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_wbtfhw58zy popover=true popoverContent=link" style="display:inline"><a href="#">한국어</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/88790cssrp.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_88790cssrp popover=true popoverContent=link" style="display:inline"><a href="#">العربية</a></span></td>;
            <td align="center"><script src="https://fast.wistia.com/embed/medias/xhvmlp574f.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_xhvmlp574f popover=true popoverContent=link" style="display:inline"><a href="#">bahasa Indonesia</a></span></td>; or English below.

<script src="https://fast.wistia.com/embed/medias/mn2nrpbdzu.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_mn2nrpbdzu popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;width:100%">&nbsp;</span></div></div>

With editors under increased pressure to assess higher volumes of manuscript submissions each year, it’s important to find a fast, cost-effective solution that can be embedded into your publishing workflows. Similarity Check allows editors to upload a paper, and instantly produces a report highlighting potential matches and indicating if and how the paper overlaps with other work. This report enables editors to assess the originality of the work before they publish it, providing confidence for publishers and authors, and evidence of trust for readers. And as the iThenticate database contains over 78 million full-text scholarly content items, editors can be confident that Similarity Check will provide a comprehensive and reliable addition to their workflow.

Making sure only original research is published provides:

* peace of mind for publishers and authors that their content is identified and protected,
* a way for editors to educate their authors and ensure the reputation of their publication, and
* clarity for readers around who produced the work.

## Benefits of Similarity Check<a id='00310' href='#00310'><i class='fas fa-link'></i></a>

Similarity Check participants enjoy use of iThenticate at reduced cost because they contribute their own published content into Turnitin’s database of full-text literature. This means that as the number of participants grows, so too does the size of the database powering the service. More content in the database means greater peace of mind for editors looking to determine a manuscript’s originality.

If you participate in Similarity Check, not only do you get reduced rate access to iThenticate, but you also have the peace of mind of knowing that any similarity between your published content and manuscripts checked by other publishers will be flagged as a potential issue too.

As a Similarity Check user, you also see extra features in iThenticate, such as enhanced text-matches within the Document Viewer.

## How the Similarity Check service works<a id='00311' href='#00311'><i class='fas fa-link'></i></a>

To participate in Similarity Check, you need to be a member. Similarity Check subscribers allow Turnitin to index their full catalogue of current and archival published content into the iThenticate database. This means that the service is only available to members who are actively publishing DOI-assigned content and including in their metadata full-text URLs specifically for Similarity Check.

Turnitin indexes members’ content directly via its Content Intake System (CIS). Its CIS accesses our metadata daily to collect the full-text content links provided by our members within their metadata. Turnitin follows these URLs and indexes the content found at each location.

When you apply for the Similarity Check service, Turnitin will check that they can access your existing content via the full-text URLs in your Crossref metadata. Once confirmed, you’ll be provided with access to the iThenticate tool where you will be able to submit manuscripts to compare against the corpus of published academic and general web content in Turnitin’s database. You can do this in the iThenticate tool, or through your manuscript submission system using an API. iThenticate provides a Similarity Report containing a Similarity Score and a highlighted set of matches to similar text. Editors can then further review matches in order to make their own decision regarding a manuscript’s originality.

<figure><img src='/images/documentation/Infographic-Similarity-Check.png' alt='Similarity Check infographic' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image28">Show image</button>
<div id="image28" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content"  >              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Infographic-Similarity-Check.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

[Download the Similarity Check factsheet](/pdfs/about-similarity-check.pdf), and explore [factsheets for other Crossref services and in different languages](/services/#00252).

## Fees for Similarity Check<a id='00312' href='#00312'><i class='fas fa-link'></i></a>

[Similarity Check fees](/fees#similarity-check-fees) are in two parts: an annual service fee, and a per-document checking fee.

The [annual service fee](/fees/#similarity-check-annual-service-fee) is 20% of your Crossref annual membership fee and is included in the renewal invoices you receive each January. When you first join Similarity Check, you’ll receive a prorated invoice for the remainder of that calendar year.

[Per-document checking fees](/fees/#similarity-check-per-document-fees) are also paid annually in January. Volume discounts apply, and your first 100 documents are free of charge.
