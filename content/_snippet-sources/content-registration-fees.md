[Content Registration](/services/content-registration) (metadata deposit) fees are one-time fees for the initial registration of content with us. There are no fees for updating the metadata for registered content. These fees are usually billed quarterly in arrears. There are different fees for different content types and some fees are different depending on the publication date of the content. Some content types have volume discounts available.

If you are a member through a sponsor, your sponsor will pay content registration fees on your behalf (but they may also charge you for their services - [more here](/membership/about-sponsors)).

If you are eligible for the GEM program, you will not pay content registration fees - [more here](/gem).

#### Content registration fees by content type

|Content type|Registration fee per current record| Registration fee per backfile record| Volume discounts?
| :------| :-----------:|:-----------:|:-----------:|
Journal articles, book titles, conference proceedings and conference papers, technical reports and working papers, theses and dissertations|USD 1.00|USD 0.15|No
Peer Reviews (registered by the title owner) |USD 0.25 |USD 0.25|[Yes - see more](#volume-discounts-peer-reviews)
Peer Reviews (registered by not the title owner) |USD 1.00|USD 1.00| [Yes - see more](#volume-discounts-peer-reviews)  
Grants |USD 2.00 |USD 0.30|No
Preprints |USD 0.25|USD 0.15|[Yes - see more](#volume-discounts-preprints)
Book Chapters |USD 0.25|USD 0.15|[Yes - see more](#volume-discounts-chapters)
Standards |USD 0.15|USD 0.15|No
Databases and datasets |USD 0.06 |USD 0.06|[Yes - see more](#volume-discounts-datasets)
Components |USD 0.06 |USD 0.06|[Yes - see more](#volume-discounts-datasets)


*Note: "Current" content includes the current calendar year + the previous two calendar years, and "Backfile" is anything older than that.
So for content registered with Crossref in 2023, current content is anything registered with publication/award dates of 2023, 2022 or 2021. Backfile is anything registered registered with publication/award dates of 2020 or before.
For content registered back in 2022, current content was anything registered with publication/award dates of 2022, 2021, 2020, and backfile was any content publisher/awarded in 2019 or before.

---

<a id="volume-discounts-peer-reviews"></a>  
#### Volume discounts: peer reviews

If you register multiple peer reviews for the same article, then volume discounts apply. The prices and discounts are different depending on whether you are the publisher of the article being reviewed. Volume discounts don’t apply across reviews for different articles.


**Registered by the title publisher**

|Total number of registered DOIs per article | Registration fee per record (current and backfile)|
| :------| :-----------:|
|First peer review against single article |USD 0.25|
|Second and all further peer reviews against same article|USD 0.00|

**Registered by an organization who is not the publisher of the title being reviewed**

|Total number of registered DOIs per article | Registration fee per record (current and backfile)|
| :------| :-----------:|
|First peer review against single article |USD 1.00|
|Second peer review against same article|USD 0.25|
|Third and all further peer review against same article|USD 0.00|

<a id="volume-discounts-preprints"></a>  
#### Volume discounts: preprints and other posted content

|Total number of registered DOIs per quarter|Registration fee per record (current)| Registration fee per record (backfile)|
| :------| :-----------:|:-----------:|
|0-1000|USD 0.25|USD 0.15
|1,001 - 10,000 |USD 0.25 |USD 0.12
|10,001 - 100,000 |USD 0.25|USD 0.06
|100,001 and up |USD 0.25|USD 0.02

<a id="volume-discounts-chapters"></a>  
#### Volume discounts: book chapters and reference entries for a single title

If you're depositing a lot of chapters or reference works for the same title in the same quarter year, the following discounts apply:

|Total number of registered DOIs per title per quarter | Registration fee per record (current)| Registration fee per record (backfile)|
| :------| :-----------:|:-----------:|
|0-250|USD 0.25| USD 0.15|
|251-1,000|USD 0.15| USD 0.15|
|1,001-10,000|USD 0.12|USD 0.12|
|10,001-100,000|USD 0.06|USD 0.06|
|100,001 and up|USD 0.02|USD 0.02|


The higher tiers are for encyclopaedias, in case you're wondering how a single title could possibly have 100,000 chapters!

<a id="volume-discounts-datasets"></a>  
#### Volume discounts: datasets and components  

If you're registering a lot of components or datasets for a single title in the same quarter year, the following discounts apply. There is no difference in price between current and backfile content for datasets and components.

|Total number of registered DOIs per title per quarter | Registration fee per record (current and backfile)|
| :------| :-----------:|
|1-10,000|USD 0.06|
|10,001-100,000|USD 0.03|
|100,001-1,000,000|USD 0.02|
|1,000,001-10,000,000|USD 0.01|
|10,000,001 and up|USD 0.005|
