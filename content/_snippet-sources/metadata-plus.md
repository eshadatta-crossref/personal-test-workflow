> #### Metadata Plus gives you enhanced access to all our supported APIs, guarantees service levels and support, and additional features such as snapshots and priority service/rate limits.

To use Metadata Plus, an optional paid-for service, you do not need to be a member. In addition to enhanced access to all our supported APIs ([OAI-PMH and REST APIs](/documentation/retrieve-metadata/)) and metadata in XML and JSON, Metadata Plus provides you with:
* a service level agreement guaranteeing you extra service and support, giving you a consistent and predictable experience
* additional features such as snapshots and priority service/rate limits.

Metadata Plus users may use either or both of the included API interfaces:
* OAI-PMH: this API retrieves metadata in XML using the [Open Archives Initiative Protocol for Metadata Harvesting (OAI-PMH)](http://www.openarchives.org/OAI/openarchivesprotocol.html) version 2 repository framework
* REST API: this API searches and filters metadata, is generally RESTFUL, and returns results in JSON. Learn more in our [REST API documentation](https://github.com/CrossRef/rest-api-doc).

## Description of Service<a id='00343' href='#00343'><i class='fas fa-link'></i></a>

<table>
	<thead>
		<tr>
			<th width="33%">Feature</th>
			<th width="33%">Defined as</th>
			<th width="33%">Notes</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Snapshots</td>
			<td>User-requested full-file downloads.<br><br>Data may be requested in JSON and XML via an interface separate from each API.<br><br>Pre-generated reports available via machine request and human user interface.</td>
			<td>Filters based on commonly used data are in *under consideration*:<br>+ ISSN<br>+ Month and year<br>+ Publisher<br>+ Journal title</td>
		</tr>
		<tr>
			<td>Priority Service/Rate Limits</td>
			<td>Plus users will be prioritized via isolated resources.<br><br>Minimum number of queries per second per IP address.</td>
			<td>Rate limiting of the API is primarily on a per access token basis. If a method allows, for example, for 150 requests per rate limit window, then it allows 150 requests per window per access token. This number can depend on the system state and may need to change. If it does, Crossref will publish it in the response headers.</td>
		</tr>
	</tbody>
</table>

In the exceptional event that a release is not backwards-compatible, Crossref will provide extensive lead time to communicate the changes, as part of our proactive support for Plus users.

## How Metadata Plus works<a id='00341' href='#00341'><i class='fas fa-link'></i></a>

Start by [contacting us about Metadata Plus access](mailto:plus@crossref.org). Plus subscribers are provided with an access token to use either or both the OAI-PMH and the REST API. Learn more in our extensive documentation for both the [REST API](/documentation/retrieve-metadata/rest-api) and [OAI-PMH](/documentation/retrieve-metadata/oai-pmh) interfaces.

## Service level agreement (SLA)<a id='00342' href='#00342'><i class='fas fa-link'></i></a>

Crossref will maintain an aggregated, average uptime for all of the interfaces that together comprise the Crossref Metadata Service of 99.5%, reported on a monthly basis. Crossref will provide technical support to Subscriber through Crossref’s existing support channels as requested by Subscriber, and will provide a response within one (1) business day to support requests received during normal working hours in the United States and the United Kingdom. "Business days" do not include weekends or legal holidays in the United States and the United Kingdom. "Response" means that support requests will be acknowledged. The time required for resolution will depend upon the nature of the request.

## Agreement and fees for Metadata Plus<a id='00254' href='#00254'><i class='fas fa-link'></i></a>

Learn more about the [Metadata Plus service agreement](/services/metadata-retrieval/metadata-plus/terms/), and [fees and pricing tiers](/fees/#metadata-plus-subscriber-fees) for Metadata Plus.
