> #### Cited-by gives our members full access to citations, helping them to build a picture of how research has been received by the community.

Scholars use citations to critique and build on existing research, acknowledging the contributions of others. Members can include references in their metadata deposits which Crossref uses to create links between works that cite each other. The number of citations each work receives is visible to anyone through our public APIs. Through our Cited-by service, members who deposit reference metadata can retrieve everything they need to display citations on their website.

Members who use this service are helping readers to:

* easily navigate to related research,
* see how the work has been received by the wider community,
* explore how ideas evolve over time by highlighting connections between works.

Watch the introductory Cited-by animation in your language:
			<td align="center"><script src="https://fast.wistia.com/embed/medias/yocvjxar8l.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_yocvjxar8l popover=true popoverContent=link" style="display:inline"><a href="#">français</a></span></td>;
			<td align="center"><script src="https://fast.wistia.com/embed/medias/d8rkt2fuo4.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_d8rkt2fuo4 popover=true popoverContent=link" style="display:inline"><a href="#">español</a></span></td>;
			<td align="center"><script src="https://fast.wistia.com/embed/medias/efhkwsc29a.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_efhkwsc29a popover=true popoverContent=link" style="display:inline"><a href="#">português do Brasil</a></span></td>;
			<td align="center"><script src="https://fast.wistia.com/embed/medias/slzod0pvje.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_slzod0pvje popover=true popoverContent=link" style="display:inline"><a href="#">简体中文</a></span></td>;
			<td align="center"><script src="https://fast.wistia.com/embed/medias/7vv5ecmjjv.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_7vv5ecmjjv popover=true popoverContent=link" style="display:inline"><a href="#">日本語</a></span></td>;
			<td align="center"><script src="https://fast.wistia.com/embed/medias/vjfiqhs90q.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_vjfiqhs90q popover=true popoverContent=link" style="display:inline"><a href="#">한국어</a></span></td>; <td align="center"><script src="https://fast.wistia.com/embed/medias/j7sp7esumu.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_j7sp7esumu popover=true popoverContent=link" style="display:inline;position:relative"><a href="#">العربية</a></span></td>;
		<td align="center"><script src="https://fast.wistia.com/embed/medias/0o5jq79ezn.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_0o5jq79ezn popover=true popoverContent=link" style="display:inline;position:relative"><a href="#">bahasa Indonesia</a></span></td>; or in English below.

<script src="//fast.wistia.com/embed/medias/550k5c78e6.jsonp" async></script><script src="//fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_550k5c78e6 popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;width:100%">&nbsp;</span></div></div>

## How Cited-by works<a id='00265' href='#00265'><i class='fas fa-link'></i></a>

Cited-by begins with depositing references as part of the metadata records for your content. Learn more about [depositing references](/documentation/content-registration/descriptive-metadata/references/).

{{< figure-linked
	src="/images/documentation/Cited-by-how-it-works.png"
	large="/images/documentation/Cited-by-how-it-works.png"
	alt="How Cited-by works - text description below diagram"
	title="How Cited-by works"
	id="image127"
>}}

A member registers content for a work, the *citing paper*. This metadata deposit includes the reference list. Crossref automatically checks these references for matches to other registered content. If this is successful, a citation is created.

Crossref logs the citations and updates the citation counts for each work. You can [retrieve citation counts through our public APIs](#00266). Members who deposit references can sign up for the Cited-by service to retrieve the full list of *citing works* (not just the count), and can display them on their website.

Note that citations from Crossref may differ from those provided by other services because we only look for links between Crossref-registered works and don't share the same method to find matches.

## Obligations and fees for Cited-by<a id='00268' href='#00268'><i class='fas fa-link'></i></a>

* Participation in Cited-by is optional, but encouraged
* There is no charge for Cited-by
* You must include references when you register content in order to be eligible for Cited-by
* You only retrieve Cited-by metadata for your own content.

## Best practice for Cited-by<a id='00267' href='#00267'><i class='fas fa-link'></i></a>

Because citations can happen at any time, Cited-by links must be kept up-to-date. Members should either check regularly for new citations or (if [performing XML queries](/documentation/cited-by/retrieve-citations#00272)) set the alert attribute to *true*. This means the search will be saved in the system and you’ll get an alert when there is a new match.

<figure><img src='/images/documentation/Infographic-Cited-by.png' alt='Cited-by infographic' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image21">Show image</button>
<div id="image21" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content"  >              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Infographic-Cited-by.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

[Download the Cited-by factsheet](/pdfs/about-cited-by.pdf), and explore [factsheets for other Crossref services and in different languages](/services/#00252).

## How to access citation matches<a id='00266' href='#00266'><i class='fas fa-link'></i></a>

Members who deposit references allow them to be retrieved by anyone using our public APIs. For example:

```
http://api.crossref.org/works?filter=has-references:true
```

Also in the metadata is the number of citations a work has received, under the tag `"is-referenced-by-count"`.

However, to retrieve the full list of citations for your own works, you need to use Cited-by. While anyone can use an API query to see the number of citations a work has received, through Cited-by the member who deposited the work can retrieve a list of citing DOIs. Details of the citing works can be displayed on your website alongside the article.

In addition, Cited-by users can receive [callback notifications](/documentation/content-registration/verify-your-registration/notification-callback-service/) or emails informing them when one of their works has been cited.
