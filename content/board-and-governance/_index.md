+++
title = "Board & governance"
date = "2023-03-09"
author = "Ginny Hendricks"
draft = false
aliases = [
    "/01company/05board.html"
]
[menu.main]
rank = 3
parent = "About us"
weight = 5
+++

Crossref is registered as Publishers International Linking Association, Inc. (PILA) in New York, USA. You can view our [certificate of incorporation](/board-and-governance/incorporation-certificate) and [by laws](/board-and-governance/bylaws). We have tax exempt status in the US as a [501\(c\)6](https://www.irs.gov/charities-non-profits/other-non-profits/business-leagues) organization. Here is our [antitrust policy](/pdfs/antitrust.pdf) (pdf) and our [conflict of interest policy](/pdfs/coi.pdf) (pdf).

The board of directors governs Crossref. They meet three times a year and oversee the organization, set its strategic direction and make sure that Crossref fulfills its mission. A list of [motions](#motions) from every board meeting is available.

Our members elect the board. [Voting](/board-and-governance/elections) take place online and election results are announced at the annual business meeting during the Crossref LIVE conference each November. There is a [nominating committee](/committees/nominating) made up of three board members not up for election, and two non-board members. This committee puts forward a slate in September of each year for the entire membership to vote on. Please [contact us](mailto:feedback@crossref.org) if you would like to know more.

To ensure effective governance and get input from a wide range of members and other stakeholders in the scholarly communications community, we have [committees](/committees).

## Officers

* **Chair:** Lisa Schiff, California Digital Library (CDL)
* **Treasurer:** Rose L'Huillier, Elsevier
* **Secretary:** Secretary: Lucy Ofiesh, Crossref Director of Finance & Operations  
* **Assistant Secretary:** Ed Pentz, Crossref Executive Director

## Board members

|Board member|Representative|Alternate|Location |Org Type|Size (#items)|Term  |
|:--|:--|:--|:--|:--|--:|--:|
|AIP Publishing|Penelope Lewis|Dean Sanderson| USA |Society|881,000|2022-2025|
|APA|Aaron Wood|Jasper Simons| USA |Society |775,000|2022-2025|
|Beilstein-Institut|Wendy Patterson|Carsten Kettner| Germany |Nonprofit |6,500|2021-2024|
|California Digital Library (CDL)|Lisa Schiff|Catherine Mitchell| USA |Library |30,500|2022-2025|
|Clarivate|Christine Stohn|Francesca Buckland|USA|Company| 11,000 |2023-2026|  
|Center for Open Science|Nici Pfeiffer| Lisa Cuevas Shaw| USA |Researcher service|133,000| 2022-2025
|Elsevier BV|Rose L'Huillier|Alok Pendse |Netherlands|Company|20,600,000|2023-2026|
|Korean Council of Science Editors|Kihong Kim|Sue Kim | South Korea|Other nonprofit|325|2021-2024
|Melanoma Research Alliance|Marc Hurlbert|| USA|Research funder|425|2022-2025
|MIT Press| Nick Lindsay| Amy Brand | US |University Press |121,000|2023-2026|
|Open Edition|Marin Dacos|Marie Pellen| France |Other nonprofit|550,000| 2021-2024|
|Oxford University Press|James Phillpotts|John Campbell |UK| University press|4,650,000|2021-2024|
|Pan Africa Science Journal|Oscar Donde| |Kenya|Other nonprofit|15|2023-2026|
|SciELO|Abel Packer|Luis Gomez|Brazil|Other nonprofit|450,000|2021-2024|
|Springer Nature|Anjalie Nawarantne |Volker Boeing |UK|Company|16,200,000|2023-2026|
|Taylor & Francis|Liz Allen|Leon Heward Mills|UK|Company| 7,200,000|2021-2024|


<a id="motions"></a>
## 2022 Motions passed
### **November 2022 Board meeting**
1. To approve the agenda for the November 8-9, 2022 meeting of the Board of Directors.  (APA, Open Edition abstaining.)
2. To approve the Consent Agenda as set forth in the meeting materials. (APA, Open Edition abstaining.)
3. To approve Crossref’s FY 2023 budget as presented.  (APA abstaining.)  
4. To approve the proposal to evolve the fee assistance program into a more expansive Global Equitable Membership (GEM) Program involving: waiving annual membership fees as well as content registration fees; and offering this by default to all eligible countries irrespective of joining through any specific Sponsor, or independently, subject to annual review.

### **July 2022 Board meeting**
*All motions passed unanimously except as otherwise noted.*  
1. To approve the agenda for the July 12-13, 2022 meeting of the Board of Directors.
2. To approve the Consent Agenda as set forth in the meeting materials. (APA abstaining.)
3. To ratify the appointment of Aaron Wood to fill the American Psychological Association seat vacated by Jasper Simons, to serve out the remainder of the current term or until his respective successor is duly appointed and qualified. (APA abstaining.)
4. To approve the FY2021 audited financial statements, and to accept the recommendations from the Audit Committee.
5. Crossref commits to support, along with the California Digital Library and DataCite, the long term sustainability of ROR by funding a share of ROR operating costs through its normal expense budget on an ongoing basis, along with joint governance and sustainability oversight. Crossref will include support for ROR in the normal budget proposal presented to the board in November. (CDL abstaining.)


### **March 2022 Board meeting**
*All motions passed unanimously except as otherwise noted.*  

1. To elect Reshma Shaikh as Chair of the Board. _(Springer Nature abstaining)_
2. To elect Damian Pattison as Treasurer. _(eLife abstaining)_
3. To appoint each of Rose L’Huillier and Wendy Patterson to the Executive Committee. _(Elsevier and Beilstein Institut abstaining)_
4. To appoint Lucy Ofiesh as Secretary and Ed Pentz as Assistant Secretary of the Corporation.
5. To appoint Jasper Simons as Chair of the Audit Committee.  _(APA abstaining)_
6. To appoint Abel Packer as Chair of the Membership & Fees Committee. _(SciELO abstaining)_
7. To appoint Todd Toler as Chair of the Membership & Fees Committee.
8.	To ratify the appointment of Marc Hurlbert to fill the Melanoma Research Alliance seat vacated by Kristin Mueller, and of Damian Pattison to fill the eLife seat vacated by Melissa Harrison, in each case to serve out the remainder of the current term or until their respective successor is duly appointed and qualified.  _(eLife abstaining)_
9. To approve the agenda for the March 9-10, 2022 meeting of the Board of Directors.
10. To approve the Consent Agenda as set forth in the meeting materials.
11.	That, based on a technical assessment, Crossref will change its reference distribution policy so that all references registered with Crossref are treated the same as other metadata, following a planned transition.
12. To limit the 2022 Board slate to a number of nominees that is at least equal to, and exceeds by no more than one, the number of available seats in each of the Revenue Tier categories.  _(6 in favor; 5 opposed; Clarivate abstaining)_
13. To provide the following guidance to the Nominating Committee:
- To achieve balance between Revenue Tiers by proposing a 2022 slate consisting of Revenue Tier 1 seats and Revenue Tier 2 seats; thereby resulting in, as nearly as practicable, an equal balance between Board members representing Revenue Tier 1 and Revenue Tier 2 (as those terms are defined in Crossref’s Bylaws); and
- To provide the following further guidance to the Nominating Committee with respect to the choice of the slate of candidates for election to the Board at the 2022 annual meeting:
    - Construct a slate of nominees that is at least equal to, and exceeds by no more than one, the number of available seats in each of the Revenue Tier categories (as defined in Crossref’s Bylaws);
    - Prioritize maintaining representation of members having both commercial and non-commercial business models, in addition to continuing to seek balance across factors such as gender, ethnic and racial background, geography, and sector;
    - Work with staff to develop a call for interest that reflects those areas of skill that are functional priorities for the Board; and
    - Take into account any recommendations from the previous year’s Nominating Committee.
14. To appoint Liz Allen to the Executive Committee. _(Taylor & Francis abstaining)_

## 2021 Motions passed  

### **November 2021 Board meeting**
*All motions passed unanimously except as otherwise noted.*  
1. To approve the agenda for the November 9-10, 2021 meeting of the Board of Directors.
2. To approve the Consent Agenda as set forth in the meeting materials.
3. To adopt the proposed funds framework and investment policy changes as set forth in the meeting materials; and
4. To authorize (1) the Investment Committee to establish a set of high-level guidelines for ethical and sustainable investment practices, and (2) the Leadership Team and the Investment Committee to implement Crossref’s capital investments consistent with those guidelines.
5. To adopt Crossref’s proposed travel and events commitments, including budget levels, set forth in the meeting materials.
6. To authorize the Leadership Team, supported by counsel, to proceed with negotiations with Turnitin, LLC based on the recommendations set forth in the Board meeting materials, as well as to (1) further investigate options for image detection and (2) continue to promote and cultivate longer-term alternative solutions within the plagiarism detection market.
7. To approve Crossref’s FY 2022 budget as presented.

### **July 2021 Board meeting**
*All motions passed unanimously except as otherwise noted.*  
1. To approve the agenda for the July 13-14, 2021 meeting of the Board of Directors.
2. To approve the consent agenda as set forth in the meeting minutes.
3. To approve the FY2020 audited financial statements, and to accept the recommendations from the Audit Committee.
4. To ratify the Executive Committee’s termination of Graduate School of Economics and Management membership.
5. To terminate the subsequent membership of University of Economic and Management.
6. To disallow membership for any applications that the Leadership Team determines are the same entity or organization trying to regain membership.
7.To approve starting the recruitment and hiring for an additional infrastructure position as soon as possible.


### **March 2021 Board meeting:**
*All motions passed unanimously except as otherwise noted.*  

1. To elect Scott Delman as Chairman of the Board. (ACM abstaining)
2. To elect Catherine Mitchell as Treasurer. (CDL abstaining)
3. To appoint each of Melissa Harrison, Rose L’Huillier, and Reshma Shaikh to the Executive Committee. (eLife, Springer Nature, and Elsevier abstaining)
4. To appoint Lucy Ofiesh as Secretary and Ed Pentz as Assistant Secretary of the Corporation.
5. To appoint Jasper Simons as Chair of the Audit Committee. (APA abstaining)
6. To appoint Todd Toler as Chair of the Membership & Fees Committee. (Wiley abstaining)
7. To ratify the appointment of Dean Sanderson to fill the AIP seat vacated by Jason Wilde for the remainder of AIP’s term until March 2021. (AIP abstaining)
8. To approve the agenda for the March 9-10, 2021 meeting of the Board of Directors.
9. To approve the consent agenda as set forth in the meeting minutes.
10. To appoint Liz Allen as Chair of the Nominating Committee. (Taylor & Francis abstaining)
11. To instruct the Nominating Committee to (1) put forward a slate to fill three Tier 1 seats and two Tier 2 seats with one additional candidate per tier, for a total of four Tier 1 candidates and three Tier 2 candidates; and (2) to propose at least one name from a funder member for the current round of elections, with the Crossref Board to review this approach following the 2021 Board election.  (APA and AJOL abstaining.)


## 2020 Motions passed  

### **November 2020 Board meeting:**
*All motions passed unanimously except as otherwise noted.*  

1. To approve the agenda for the November 10-11, 2020 meeting of the Board of Directors.

2. To approve the Minutes of the July 2020 meeting of the Board of Directors.

3. To approve the Minutes of the October 2020 strategy session of the Board of Directors.

4. To hold the March 2021 Crossref Board meeting, in virtual format, on March 9-10, 2021.

5. To adopt the Audit Committee’s report as presented.

6. To endorse the Principles of Open Scholarly Infrastructure, as set forth in the meeting materials.  (APA voting against; Elsevier abstaining.)

7. To approve the 2020 budget as presented.

8. That the Crossref Board supports another organization’s taking ownership of the Distributed Usage Logging (“DUL”) initiative. Crossref will support the DUL proof of concept, as is, until the March 2021 Crossref Board meeting or until ownership has been transitioned, whichever is sooner.  Crossref will provide adequate transition support when the service migrates, and continue to support the registration of article-level DUL endpoint metadata if another organization takes over and maintains the service.


### **July 2020 Board meeting:**
*All motions passed unanimously except as otherwise noted.*  

1. To approve the agenda for the July 8-9, 2020 meeting of the Board of Directors.  

2. To approve the Minutes of the March 2020 meeting and May and June 2020 strategy sessions of the Board of Directors, with the revisions proposed by participants.   

3. To adopt the Minutes of the Crossref Executive Committee meetings of April 3, April 17, May 1, May 15, May 29, and June 12, 2020.   

4. To adopt the auditors’ recommendations with respect to further updating and documenting Crossref’s internal controls.  

5. To direct the Crossref leadership team to analyze the options with respect to type and frequency of regular audits of Crossref’s internal controls, and to present a proposal to the Board at its November 2020 meeting.  

6. To adopt the 2019 audited financial statements of the Company.  

7. To direct the organization to create a standing Investment Committee to provide guidance to the board and leadership team on managing Crossref’s financial assets, which committee shall (1) seek Board approval for any change in investment strategy and (2) work closely with the leadership team with respect to communications regarding Crossref’s asset management policies and related matters.  

8. To revise the Day 2 Board meeting agenda as proposed.  

9. That Crossref should proactively lead an effort to explore, with other infrastructure organizations and initiatives, how we can improve the scholarly research ecosystem. Crossref is committed to the collaborative development of open scholarly infrastructure for the benefit of our members and the wider research community.  _Abstaining: Open Edition, SciELO_  

10. That the exploration referenced in the foregoing resolution should consider a range of options looking at operational, governance, technical, and product and service issues and how the organizations could take advantage of synergies, efficiencies, and opportunities for the benefit of the wider research community by working more closely together.  

11. To establish an ad hoc Exploratory Committee to determine, within six weeks, concrete next steps in exploring broader scholarly infrastructure partnerships under the auspices of Crossref; said committee to include the Crossref leadership team and board members at large with relevant experience and no conflicts of interest.  

12. To constitute the ad hoc Exploratory Committee to consist of five members of the Crossref leadership team and four members of the Crossref Board.  _Abstaining: ACM, SciELO_  

13. That the Executive Committee’s May 29, 2020 termination of two sponsoring members, as more particularly set forth in the meeting materials, is hereby ratified, and staff directed to work with counsel to explore alternative modes of Crossref participation for similarly-situated members consistent with US and other applicable legal constraints.  

14. That the Executive Committee’s May 29, 2020 termination of certain Crossref members linked with former Crossref member OMICS is hereby ratified.

15. To accept with pleasure, subject to appropriate legal review and documentation, the rescission of Ed Pentz’s resignation as Executive Director of Crossref.   


### **March 2020 Board meeting:**
*All motions passed unanimously except as otherwise noted.*  

1. To approve the agenda for the March 11-12, 2020 meeting of the Board of Directors.  

2. To ratify the appointment of Rose L’Huillier to fill the Elsevier seat vacated by Chris Shillum, and of Andrew Smeall to fill the Hindawi seat vacated by Paul Peters, in each case to serve out the remainder of the current term or until his/her successor is duly appointed and qualified.  

3. To appoint Lucy Ofiesh as Secretary of the Corporation.    

4. To appoint each of Amy Brand, Catherine Mitchell, and Reshma Shaikh to the Executive Committee. _(MIT Press, CDL, and Springer Nature abstaining.)_

5. To appoint Andrew Smeall as Chair of the Audit Committee. _(Hindawi and Springer Nature abstaining.)_  

6. To adopt the recommendation of the Executive Committee with respect to acquisitions of Crossref members to provide that (1) any consolidated enterprise cannot occupy more than one Board seat, but can permit a subsidiary organization to occupy its Board seat; (2) wholly-owned subsidiaries should not be treated as separate Crossref members, but should be part of a single member whose fees are based on enterprise-level revenues; and (3) pursuant to the foregoing principles, Taylor &amp; Francis and F1000 Research will be treated as a single member following the acquisition of F1000 Research by Taylor &amp; Francis. _(AJOL, SciELO, and Taylor &amp; Francis abstaining.)_  

7. To approve the Minutes of the November 2019 meeting of the Board of Directors. _(AJOL, Springer Nature, Clarivate Analytics, Elsevier, Wiley, and Hindawi abstaining.)_  

8. To provide the following guidance to the Nominating Committee: To achieve balance between Revenue Tiers by proposing a 2020 slate consisting of four Revenue Tier 1 seats and two Revenue Tier 2 seats; thereby resulting in, as nearly as practicable, an equal balance between Board members representing Revenue Tier 1 and Revenue Tier 2 (as those terms are defined in Crossref’s Bylaws).  

9. To provide the following further guidance to the Nominating Committee with respect to the choice of the slate of candidates for election to the Board at the 2020 annual meeting:  
  *  Construct a slate of nominees that is at least equal to, and exceeds by no more than two, the number of available seats in each of the Revenue Tier categories (as defined in Crossref’s Bylaws);  
  * Prioritize maintaining representation of members having both commercial and non-commercial business models, in addition to continuing to seek balance across factors such as gender, ethnic and racial background,
geography, and sector; and  
  * Work with staff to develop a call for interest that reflects those areas of technical skill that are functional priorities for the Board.  

10. To adopt the proposed 2020 scope of work for the Membership &amp; Fees Committee as set forth in the meeting materials. (AJOL abstaining.)  

11. To retire Crossref’s Text and Data Mining Click Through Service due to lack of uptake, working in consultation with those Crossref members currently utilizing the service to accommodate their reasonable timing needs.  

12. That, with respect to the search process for Crossref’s next Executive Director, (1) Crossref’s Executive Committee, in consultation with Crossref’s staff directors, will conduct the search; (2) the Executive Committee will propose one candidate to the Board for ratification; and (3) in the event that the Executive Committee cannot agree on a single candidate to propose to the Board, it will seek broader assistance from the Board and senior staff, at the discretion of the Executive Committee, in order to arrive at a single candidate to propose for Board ratification.  

## 2019 Motions passed  

### **November 2019 Board meeting:**
*All motions passed unanimously except as otherwise noted.*  

1. To approve the agenda for the November 12-13, 2019 meeting of the Board of Directors.  
2. To approve the Minutes of the July 2019 meeting of the Board of Directors.  
3. To adopt the amended Whistleblower Policy.  
4. That staff, working with the ad hoc Strategic Working Group, will develop the framework for a discussion of key strategic questions, including alternatives to evaluate, to be held at the March 2020 Board meeting, with facilitation if appropriate.  
5. That any resource request associated with the Distributed Use Logging Initiative will be submitted to Crossref’s Executive Committee for review and approval.  
6. That the Membership & Fees Committee will prepare a revised M&F Committee Charter, to include elements addressing committee membership criteria and expectations for committee membership.  
7. To finalize the Board’s previous provisional decision to eliminate the Crossmark fee, in light of the presentation of Crossref’s proposed 2020 budget.  
8. To approve the 2020 budget as presented.  


### **July 2019 Board meeting:**
*All motions passed unanimously except as otherwise noted.*

1. To approve the agenda for the July 10-11, 2019 meeting of the Board of Directors.
2. To approve the minutes of the March 2019 meeting of the Board of Directors, as revised to reflect participant feedback.
3. To ratify the appointment of Melissa Harrison to fill the eLife seat vacated by Mark Patterson, to serve out the remainder of the current term until November 2019 or
her successor is duly appointed and qualified. _(eLife abstaining.)_
4. To institute a membership fee of $5 per Sponsored Member. *(AJOL abstaining.)*
5. To keep Event Data in maintenance mode and defer sustainability model work until
July 2020, and require staff to present a plan to the Board in July 2020 for Event
Data Plus.
6. To take the following actions:    
    (1) to implement a practice whereby, when a member registers a content item for which it pays the standard Content Registration fee, then no additional content fee will be assessed for a subsequent registration by the same member of a version of the original content, provided that the appropriate `isVersionof` designator is used in the metadata;    
    (2) to treat the `isTranslationOf` relationship type in the same fashion;    
    (3) to treat corrections and retractions in the same fashion when and to the extent technically feasible; and    
    (4) to direct the Membership & Fees Committee to examine Crossref’s other relationship types to determine which others, if any, should be treated in the same fashion.    
7. To approve the following Crossref Fee Principles:
    **Crossref’s fees should:**    
    (1) Enable us to fulfill our mission to make research outputs easy to find, cite, link, assess, and reuse.    
    (2) Encourage best practice and discourage bad practice, as Crossref policies and obligations advise.    
    (3) Be non-discriminatory, encouraging broad participation from organizations of all sizes and types.    
    (4) Support the long-term persistence of our services and infrastructure, so long as relevant and valuable to the community.    
    (5) Deliver value to our members.    
    (6) Be transparent and openly available, recommended by the Membership & Fees Committee and approved by the Board.    
    (7) Be the same for all, not discounted or negotiated individually, to ensure fairness.    
    (8) Be independent of our members’ own business models.    
    (9) Not always be necessary, e.g., new content types are not usually separate services.    
    (10) Be based on services not metadata.    
8. To approve the elimination of the Crossmark fee, subject to the 2020 budgeting process.
9. To adopt the 2018 audited financial statements of the Company.
10. To ratify the account termination, for cause, of OMICS Publishing Group (Member ID 2674); Ashdin Publishing (Member ID 2853); Scitechnol Biosoft Pvt. Ltd. (Member ID 9225); and Herbert Publications PVT LTD (Member ID 4912).
11. To amend Art. I Sec. 5 of Crossref’s Bylaws by replacing the second sentence thereto in its entirety with the following text: _“Suspension or expulsion shall be by
a vote of the Board (or by action of the Executive Committee, to take effect at the
time specified in such Executive Committee action and to be reviewed and ratified
by a vote of the Board at the next subsequent Board meeting), except where the
suspension or expulsion is the result of the non-payment of dues and fees, in which
event the Board may delegate such authority to the Executive Director.”_
12. To approve a policy of causing the DOIs of permanently terminated member
accounts to resolve first to an interstitial page with a message indicating the DOI
has resolved to content of a party that is no longer a member, and then to the
original version of the content (or, if no original, an archived version). *(AIP, eLife
abstaining.)*
13. To form a temporary ad hoc Strategic Review Working Group to examine
    (1) the feedback on strategic matters provided by the Board breakout groups at the July 2019 Board meeting; and
    (2) results of Crossref’s value research project once available.

### **March 2019 Board meeting:**
*All motions passed unanimously except as otherwise noted.*

1. To approve the agenda for the March 6-7, 2019 meeting of the Board of Directors.
2. To elect Paul Peters as Chairman of the Board. *(Hindawi abstaining.)*
3. To elect Scott Delman as Treasurer. *(ACM abstaining.)*
4. To appoint each of Amy Brand, Wim van der Stelt, and
5. Jason Wilde to the Executive Committee. *(MIT Press, Springer Nature, and AIP abstaining.)*
6. To appoint Chris Shillum as Chair of the Audit Committee. *(Elsevier abstaining.)*
7. To appoint Jasper Simons as Chair of the Nominating Committee. *(APA abstaining.)*
8. To appoint each of Graham McCann and Mark Patterson to the Audit Committee. *(IOP and eLife abstaining.)*
9. To appoint each of Scott Delman and Catherine Mitchell to
the Nominating Committee. *(ACM and CDL abstaining.)*
10. To appoint Lisa Hart as Secretary of the Corporation.
11. To approve the minutes of the November 2018 meeting of the Board of Directors.
12. To amend Art. VII, Sec. 2 of Crossref’s Bylaws by inserting the following language after the second sentence thereof: _Each such slate will be comprised such that, as nearly as practicable, one-half of the resulting Board shall be composed of Directors designated by Members then representing Revenue Tier 1; and one-half of the resulting Board shall be composed of Directors designated by Members then representing Revenue Tier 2. “Revenue Tier 1” means all consecutive membership dues categories, starting with the lowest dues category, that, when taken together, aggregate, as nearly as possible, to fifty percent (50%) of Crossref’s annual revenue. “Revenue Tier 2” means all membership dues categories above Revenue Tier 1._
13. To adopt the Crossref Board Election Campaign Policy as proposed in the Board materials.
14. To provide the following guidance to the Nominating Committee: _To achieve balance between Revenue Tiers by proposing a 2019 slate consisting of one Revenue Tier 1 seat and four Revenue Tier 2 seats, and a 2020 slate consisting of four Revenue Tier 1 seats and two Revenue Tier 2 seats; thereby resulting in, as nearly as practicable, an equal balance between board members representing Revenue Tier 1 and Revenue Tier 2 (as those terms are defined in Crossref’s Bylaws)._
15. To provide the following further guidance to the Nominating Committee with respect to the choice of the slate of candidates for election to the Board at the 2019 annual meeting:
  * Construct a slate of nominees that is at least equal to, and exceeds by no more than two, the number of available seats in each of the Revenue Tier categories(as defined in Crossref’s Bylaws);
  * Prioritize maintaining representation of members having both commercial and non-commercial business models, in addition to continuing to seek balance across factors such as gender, ethnic and racial background, geography, and sector; and
  * Work with staff to develop a call for interest that reflects those areas of technical skill that are functional priorities for the Board.
16. To authorize the Treasurer, Executive Director, and Secretary to open a new account at a mutually agreed-upon bank, and formally close Crossref’s Citizen’s Bank account.
17. To ratify the appointment of Ingrida Kasperaitienė to fill the VGTU seat vacated by Eleonora Dagiene, to serve out the remainder of their current term.

## 2018 Motions passed

### **November 2018 Board meeting:**
*All motions passed unanimously except as otherwise noted.*

1. To approve the agenda for the November 15, 2018 meeting of the Board of Directors.
2. To approve the minutes of the July 2018 meeting of the Board of Directors.
3. To approve the recommendation of the Membership & Fees Committee that Crossref begins admitting funders as members and register grant identifiers and to approve the fee structure proposed.
4. To approve the proposed Definitive Agreement with Turnitin, LLC, subject to (1) revisions to address Board member comments summarized in the Minutes and (2) further discussions to accommodate the content licensing concern expressed by Board meeting participants.
5. To approve the adoption of a governance structure pursuant to which Board seats will be, as nearly as practicable, designated by revenue tier, with two categories (large and small), defined so as to roughly reflect half of Crossref’s revenue and registered content items in each category. *(PASSED with one abstention (MIT Press).*
4. To approve the promulgation of a policy on campaigning in Board elections, with policy language to be developed by staff to reflect, at a minimum, a prohibition on negative campaigning and member expenditure of funds on campaigns and a distinction between passive versus active campaigning.
5. To approve the process proposed by the Governance Committee for nominations to each of the following positions: Chair, Treasurer, Executive Committee members, the Nominating Committee Chair, and the Audit Committee Chair.
5. To amend Art. VII of the Bylaws to remove Section 3 (Independent Nominations).
6. To amend Art. I Sections 2 and 3 of the Bylaws to reflect the current means and sequence of member acceptance, as more particularly set forth in the Board meeting materials under “Proposed Amendments to the Crossref Bylaws.”
7. To approve the 2019 budget as proposed, with the addition of up to $50,000 for the Distributed Usage Logging initiative.

### **July 2018 Board meeting:**
*All motions passed unanimously except as otherwise noted.*

1. To approve the agenda for the July 11-12, 2018 meeting of the Board of Directors.
2. To approve the minutes of the March 2018 meeting of the Board of Directors.
3. To adopt the 2017 audited financial statements of the Company.
4. To approve, with recommended revisions, the revised form of Crossref Membership Terms. There was general consensus to make three revisions to the draft terms: using the term “Crossref Infrastructure and Services” throughout the document; qualifying a member’s obligation to comply with the Crossref Display Guidelines; and clarifying the GDPR compliance language.
5. To approve the proposed Term Sheet with Turnitin, LLC, subject to revisions: (1) to expressly retain the parties’ existing terms with respect to full text use and reuse by Turnitin; and (2) to provide that the final agreement will include required milestones and deliverables coupled with express remedies, including termination of exclusivity, for certain milestone/deliverable failures.
6. To amend Art. I Sec. 1 of Crossref’s Bylaws by replacing the text of Art. I Sec. 1 in its entirety with the following text: “Membership in Crossref shall be open to any organization that publishes professional and scholarly materials and content and otherwise meets the terms and conditions of membership established from time to time by the Board of Directors, and to such other entities as the Board of Directors shall determine from time to time.”
7. To amend Art. V Sec. 4 to replace the phrase “on the day after” with the phrase “during the next calendar quarter immediately following”.
8. To promulgate a policy on board alternate participation, pursuant to which (1) alternates are encouraged and welcomed to attend each meeting of Crossref’s Board of Directors that is held concurrently with Crossref’s annual meeting; (2) only a director or their alternate, but not both, may attend any other meeting of the Crossref Board of Directors; and (3) Crossref’s need-based reimbursement policy will explicitly cover the participation of alternates.

### **March 2018 Board meeting:**
*All motions passed unanimously except as otherwise noted.*

1. To approve the agenda for the March 7-8, 2018 meeting of the Board of Directors.
2. To appreciate the contributions of Lois Wasoff to the Company.
3. To approve the minutes of the November 2017 Board meeting.
4. Board authorizes Crossref to make a proposal to DataCite and ORCID to proceed with an Org ID initiative on the following principles:
  * OrgID activities would be conducted through a new legal entity (or whatever structural approach is optimal from a legal standpoint), with a governing body consisting of Crossref, Date Cite, and ORCID, and potentially other nonprofit representative bodies.
  * Other interested parties who wish to contribute financially to the entity would be welcome to participate in a non-governance role.
  * Crossref is willing to commit $300,000 as follows: $30,000 in 2018; and $270,000 in additional startup funding, contingent on raising an additional $400,000 from other stakeholders by mid-October (so that results are available by Crossref’s November 2018 meeting).
  * Further funding beyond Year 1 will be contingent on a full business plan being developed and approved by the Crossref Board at its November 2018 meeting.
5. To give the Nominating Committee the following guidance with respect to the choice of the slate of candidates for election to the Board at the 2018 annual meeting:
  * Create a slate of nominees that encourages engagement in the election of directors through a contested election; maintain stability by constructing a slate that exceeds the number of available seats by no more than two.
  * Construct slate based on the quality of the expressions of interest from candidates and to maintain balance across organizational size, gender, and geography.
6. To create an ad hoc Finance Committee of the Board to project the financial profile of the Company on a two- to three-year prospective basis; and analyze and respond to the financial and revenue implications of the Company’s various strategic initiatives.

## 2017 Motions passed

### **November 2017 Board meeting:**
*All motions passed unanimously except as otherwise noted.*

1. To approve the agenda for the meeting.
2. To approve the minutes of the July 2017 Board Meeting.
3. To accept the minutes of the Executive Committee telephone meetings on October 6, October 27 and
November 7, 2017.
4. To elect Paul Peters as Chair of the Board of Directors, after a Board vote done through written ballots in
which Paul Peters and Chris Shillum were each nominated as Chair and a majority of the votes were cast for Paul.
5. To elect Scott Delman as Treasurer, Lisa Hart as Secretary, and Ed Pentz as Executive Director and Assistant
Secretary.
6. To elect Jason Wilde, Chris Shillum and John Shaw to the Executive Committee.
7. To elect Wim van der Stelt as Chair of the Audit Committee, and Duncan Campbell and Helen King as members of
the Audit Committee.
8. To elect Mark Patterson as the Chair of the Nominating Committee, and to defer the appointment of other
Nominating Committee members until the March Board meeting.
9. To elect Graham McCann as the Chair of the Membership & Fees Committee.
10. To authorize Crossref staff to reply to the Organization Identifier Working Group’s request for information (RFI) in accordance with the recommendations from the staff report, with specific guidance that the response should
state Crossref’s willingness to take a leading role in the development of an independent organization identifier
registry, that there is a strong preference that a new joint venture collaboration be formed and not a new
non‐profit organization and that Crossref can make resources available to support the joint venture collaboration
along with grants and funding from other organizations.
11. To approve the 2018 budget as proposed.
12. To create an ad hoc Governance Committee, to comprise Paul Peters, Mark Patterson, Chris Shillum, Ian
Bannerman, Wim van der Stelt and Lisa Hart Martin with Lois Wasoff as counsel, which will make specific
recommendations to the Board at the March meeting.
13. To create an ad hoc Technical Committee to look at infrastructure issues, the membership of which will be
determined by seeking expressions of interest from the members of the Board.

### **July 2017 Board meeting:**
*All motions passed unanimously except as otherwise noted.*

1. To approve the agenda for the meeting.
2. To approve the minutes of the March 2017 Board Meeting, as corrected.
3. To accept the minutes of the Executive Committee telephone meetings on April 21 and June 23, 2017.
4. To approve the creation of an ad hoc Finance Committee, the members of which will be appointed at the November Board meeting.
5. To formally recognize Bernie Rous’s contributions to Crossref and to scholarly publishing over his 40-year career.
6. To approve the recommendations of the Membership & Fees Committee with respect to volume discounts for current deposits of posted content. *(PASSED with AIP, IEEE, Elsevier opposed; and ACM abstaining.)*
7. To approve the recommendations of the Membership & Fees Committee with respect to the creation of “peer review” as a new content type, with specific metadata schema and a bundled fee of $1.25 to be charged, with the clarifications that (i) the original, unaccepted author manuscript is to be excluded; and (ii) although the number of peer reviews that can be registered at the bundled fee will be unlimited now, Crossref may make changes in future based on actual experience with the new content type.
8. To approve the recommendations of the Membership & Fees Committee with respect to updating the metadata delivery offering to have a single agreement that covers all metadata APIs/delivery routes, to adopt a single (updated) fee structure, and to remove case-by-case opt-outs for metadata.
9. To approve the audited financials.


### **March 2017 Board meeting:**
*All motions passed unanimously except as otherwise noted.*

1.	To approve the agenda for the meeting.
2.	To approve the minutes of the November 2016 Board Meeting, as corrected.
3.	To accept the minutes of the Executive Committee telephone meetings on January 24 and February 9, 2017.
4.	To appoint Eric Merkel-Sobotta to fill the vacancy on the Audit Committee.
5.	To approve the proposed charge to the Audit Committee, with the addition of language giving the Audit Committee the responsibility of overseeing technical and security audits of the company’s systems.

---

## 2016 Motions passed

### **November 2016 Board meeting:**
*All motions passed unanimously except as otherwise noted.*

1.	To approve the agenda for the meeting.
2.	To approve the minutes of the July 2016 Board Meeting.
3.	To accept the minutes of the Executive Committee telephone meetings on September 30 and October 21, 2016.
4.	To elect Bernard Rous as Chairman and President, Gerry Grenier as Treasurer and Vice Chairman, Ed Pentz as Executive Director and Assistant Secretary, and Lisa Hart as Secretary.
5.	To elect Ian Bannerman, Chris Shillum and Jason Wilde to serve on the Executive Committee along with Bernard Rous and Gerry Grenier.
6.	To appoint John Shaw as Chair, and Paul Peters, Reny Guida (IEEE) and Mark Patterson as members, of the Nominating Committee with authority to appoint two additional members who represent companies that are not on the Board.
7.	To appoint James Walker as Chair, and Wim van der Stelt and Jasper Wilde as members of the Audit Committee.
8.	To appoint Scott Delman as Chair of the Membership & Fees Committee.
9.	To approve the recommendation of the Membership & Fees Committee with respect to pricing for the registration of DOIs for preprints, with volume discounts to be applied only to backfile deposits.
10.	To approve ongoing participation in the working group being formed to look at Organization Identifiers and the expenditure of up to US$30,000 from capital reserves to support that participation.
11.	To approve the 2017 budget as proposed.


### **July 2016 Board meeting:**
*All motions passed unanimously except as otherwise noted.*

1.	To approve the agenda for the meeting.
2.	To approve the minutes of the March 2016 Board meeting.
3.	To accept the minutes of the Executive Committee telephone meetings on April 26 and June 24, 2016.
4.	To amend Section 2, Article VII of the bylaws to add the words “at least” before the words “equal in number” in the second sentence, so that the sentence will read “The Nominating Committee shall designate a slate of candidates for each election that is at least equal in number to the number of Directors to be elected at such election.” *(PASSED, with four opposing: Springer, Elsevier, IEEE and Sage; ACM abstaining.)*
5.	To adopt the revised financial policy.
6.	To accept the report from the auditors.
7.	To approve the business model for Event Data recommended by the Membership and Fees Committee subject to the clarification of the definition of “reseller” and to delegate approval of that clarification to the Executive Committee.

### **March 2016 Board meeting:**
*All motions passed unanimously except as otherwise noted.*

1.	To approve the agenda for the meeting.
2.	To approve the minutes of the November 2015 Board meeting.
3.	To accept the minutes of the Executive Committee telephone meeting on January 27, 2016, as corrected.
4.	To appoint Jasper Simons as the Chair of the Nominating Committee, to appoint Jason Wilde and Paul Peters as committee members, and to authorize the Nominating Committee to identify two additional committee members representing companies that are not on the Board.
5.	To delete the first sentence of Section 4a of PILA’s financial policy # 3 (Approval Authority), to eliminate the requirement that salaries and other compensation of all non-officer persons who report directly to the Executive Director must be jointly approved by the Executive Director, Treasurer and President, and to delegate the authority to set such compensation to the Executive Director. *(PASSED with one abstention (IOP).*
6.	To apportion 10% of the capital reserve fund to be invested in accordance with a different investment policy from the rest of the capital reserve fund, in a professionally managed portfolio consisting of dividend paying stocks and other instruments.
7.	To remove from the current investment policy the requirement that maturity of securities be targeted at January 31 of each year.
8.	To disband the Taxonomies Interest Group.

---

## 2015 Motions passed

### **November 2015 Board meeting:**
*All motions passed unanimously except as otherwise noted.*

1. To approve the agenda for the meeting.
2. To approve the minutes of the July 2015 Board Meeting as corrected.
3. To accept the minutes of the Executive Committee telephone meetings on September 25,2015.
4. To elect Bernard Rous as Chairman and President, Gerry Grenier as Treasurer and Vice Chairman, Ed Pentz as Executive Director and Assistant Secretary, and Lisa Hart as Secretary.
5. To elect Ian Bannerman, Kathleen Keane and Chris Shillum to serve on the Executive Committee along with Bernard Rous and Gerry Grenier.
6. To appoint Jasper Simons as Chair of the Nominating Committee.
7. To appoint James Walker as Chair of the Audit Committee and Carsten Buhr and Renny Guida as members of the Audit Committee.
8. To appoint Scott Delman as Chair of the Membership & Fees Committee.
6. To approve the recommendation of the Membership & Fees Committee with respect to pricing for registration of DOIs for standards *(PASSED with Elsevier abstaining)*.
7. To approve the language changes to the membership rules to cover registration of preprints as proposed by staff.
8. To approve the 2016 budget as proposed.

### **July 2015 Board meeting:**
*All motions passed unanimously except as otherwise noted.*

1. To approve the agenda for the meeting.
2. To approve the minutes of the March 2015 Board Meeting.
3. To accept the minutes of the Executive Committee telephone meetings on April 13 and June 12, 2015.
4. To accept the recommendation of the Membership & Fees Committee that the member fees be unchanged for 2016.
5. To approve the audited financials.
6. To approve the recommendation to create a new Director of Product Management position.
7. To designate the interest generated by the Capital Reserve Fund as part of the fund.
8. To change the minimum cash balance from 3 months operating expenses to 4 months operating expenses.
9. With respect to the proposed DOI Event Tracking (DET) service:
  * To support the launch of the DET service and to authorize staff to move forward with the development of a plan;
  * To ask the M&F Committee to review and refine the proposed sustainable revenue model; and
  * To establish a Crossref DET Committee to oversee and guide the ongoing development of the DET service and report back to the board.
10. To change current membership rules 12 and 13 to eliminate inconsistencies and reflect current member practice and allow assignment of DOIs to preprints in accordance with the procedures described in the duplicative works report submitted to the board. *(PASSED with PLOS, VGTU Press, Hindawi Limited, IOP, de Gruyter, Johns Hopkins, Springer, and IEEE voting in favor; ACM, AIP, Sage and APA voting against; and Elsevier abstaining).*

### **March 2015 Board meeting:**
*All motions passed unanimously except as otherwise noted.*

1. To approve the agenda for the meeting with the addition of the discussion of a possible acquisition to the second day’s agenda.
2. To approve the minutes of the November 2014 Board Meeting, as amended at the meeting.
3. To accept the minutes of the Executive Committee telephone meeting on February 13, 2015.
4. To give the Nominating Committee the following guidance with respect to the choice of the slate of candidates for election to the Board at the 2015 annual meeting:
  * In designating the slate of candidates, take into account issues of Board composition and balance, with the goal that the board fairly represent the membership;
	* Look at the balance between large, medium and small members, the balance between non-profit and commercial organizations and the geographic location of Board members;
  * Look at issues such as board meeting attendance, committee participation and serving as an officer when considering candidates for the slate;
	* Complete its work sufficiently in advance of the annual meeting to permit independent nominations.
5. To appoint Chris Shillum as a member of the Executive Committee to fill the vacancy created by Carol Richman’s retirement.
6. To adopt the whistle blower policy as presented to the board.

---

## Earlier motions

Here are the the motions passed by the board from 2010 to 2014.

* [Motions 2014](/pdfs/motions-2014.pdf) PDF
* [Motions 2013](/pdfs/motions-2013.pdf) PDF
* [Motions 2012](/pdfs/motions-2012.pdf) PDF
* [Motions 2011](/pdfs/motions-2011.pdf) PDF
* [Motions 2010](/pdfs/motions-2010.pdf) PDF

---

## Policy on term limits

The board adopted the following policy in November 2009:

1. Non-officer members of the Executive Committee (that is, members of the Executive Committee other than the Chairman and the Treasurer) may serve no more than three (3) consecutive one-year terms on the Executive Committee. After a break in service of at least one (1) year, the term-limited director shall again be eligible to serve on the Executive Committee. Years of service on the Executive Committee as an officer shall not be included in calculating the number of consecutive terms served.
2. A director may serve no more that three (3) consecutive one-year terms as Chair. After a break in service of at least one (1) year, the term-limited director shall again be eligible to serve as Chair.
3. A director may serve no more that three (3) consecutive one-year terms as Treasurer. After a break in service of at least one (1) year, the term-limited director shall again be eligible to serve as Treasurer.
4. The limitations set forth in this Board Policy apply to the directors as representatives of their member companies, and not as individuals. For avoidance of doubt, this means that if a member company designates a successor representative to serve on the Board, as set forth in the By-Laws, the years of service of that member company’s prior representative on the Executive Committee, as Chair or as Treasurer (as applicable) will be included in determining whether the newly appointed representative is term-limited under this policy.
5. This Board Policy is being implemented pursuant to resolutions adopted at the July 2009 meeting of the PILA Board of Directors and is in effect as of the date of that meeting. For purposes of determining whether a director is term-limited under this policy, each director will be deemed to have commenced service in the relevant capacity as of November, 2008.

---

Please contact our [operations director](mailto:feedback@crossref.org) with any questions about our governance.
