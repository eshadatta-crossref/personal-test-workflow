+++
title = "Board election 2021 candidates"
date = "2021-08-11"
draft = false
author = "Lucy Ofiesh"
rank = 2
parent = "Elections"
weight = 6
type = 'dontindexcontent'
+++

If you are a voting member of Crossref your ‘voting contact’ will receive an email in late September, please follow the instructions in that email which includes links to the relevant election process and policy information.

From 61 applications, our [Nominating Committee](/committees/nominating/) put forward the following eight candidates to fill the five seats open for election to the Crossref Board of Directors. Please read their candidate statements below.

### Tier 1, Small and mid-sized members  
| Member organization  | Candidate standing  | Title   | Org type  |  Country |
|---|---|---|---|---|
|[California Digital Library, University of California](#cdl)|Lisa Schiff | Associate Director, Publishing, Archives, and Digitization| Library | US|    
|[Center for Open Science](#cos)|Nici Pfeiffer | Chief Product Officer| Researcher service | US |    
|[Melanoma Research Alliance](#mra)|Kristen Mueller | Senior Director, Scientific Program| Research funder | US|    
|[Morressier](#morressier)|Sebastian Rose | Head of Data | Data repository | Germany |    
|[NISC](#nisc)|Mike Schramm | Managing Director| Publisher | South Africa |    

### Tier 2, Large members  
| Member organization  | Candidate standing  | Title   | Org type  |  Country |
|---|---|---|---|---|
|[AIP Publishing](#aip)|Penelope Lewis | Chief Publishing Officer | Society | US  |    
|[American Psychological Association (APA)](#apa)   |Jasper Simons| Chief Publishing Officer | Society | US  |   
|[Association for Computing Machinery](#acm)   |Scott Delman | Director of Publications | Society | US  |   


---

##  Tier 1, Small and mid-sized members (electing three seats)

<a id="cdl"></a>
## Lisa Schiff, University of California, USA<br>

<img src="/images/board-governance/lisa-schiff.jpg" alt="Lisa Schiff, University of California" width="225px" class="img-responsive" />  

{{% accordion %}}
{{% accordion-section "in English" %}}

### Organization statement
For almost twenty years, the California Digital Library (CDL) at the University of California (UC) has been a leader in library-based publishing, with now nearly 90 open access journals on eScholarship, our flagship open access publishing platform. Our continually expanding program supports subscription journals looking to flip to open access, emerging disciplines, scholars in fields with limited commercial markets, practitioners in applied fields, and graduate and undergraduate students.  In addition to this journals program, our services include: an institutional repository for a myriad of genres, from working papers to electronic theses and dissertations; a research information management system to implement UC’s expansive open access policies;  a preprint server for the global geoscience community; and a “Labs” program to experiment with new publishing approaches and tools.  This constellation of services situates CDL squarely within the needs and ambitions of our publishing partners--and as a technology center, CDL is attuned to the complexities of meeting those ambitions, including the challenges involved in gathering core metadata from users, integrating platforms, and providing optimal end user experiences.   

CDL is actively involved with organizations essential to the scholarly communications ecosystem, including, but not limited to: Crossref (organizational and board member);  Library Publishing Coalition (organizational and board member); Directory of Open Access Journals (advisory board member); Coalition of Open Access Policy Institutions (steering committee member); ORCID (member); and Open Access Scholarly Publishing Association (member).  CDL is also a Co-PI on the Next Generation Library Publishing initiative, which is building a community based, open source, library publishing platform that integrates and expands upon existing community solutions.  To each of these groups, CDL brings awareness of the requirements, goals, and impact of institutionally supported publishing activities, a dynamically growing area within scholarly communication that scholars increasingly turn to for trusted solutions.  Continuity of the library publisher voice on the board would further strengthen Crossref’s efforts to meet its core goals and its recently updated strategic agenda, especially those areas touching on broadening stakeholder and community engagement, and service and metadata improvement.  


### Personal statement <br>
As a long-standing member of the growing library publishing community, I am eager to help Crossref in its efforts to better support a broader stakeholder group by bringing my technical, programmatic, and organizational knowledge to a lynchpin entity in the scholarly communication space.   

As Associate Director of the Publishing, Archives, & Digitization group at the California Digital Library (CDL), I guide the strategic planning, development, and operations of services supporting publishing, research, and learning activities of the University of California’s academic community, including UC’s open access publishing platform (with nearly 90 scholarly journals), a research information management system undergirding UC’s open access policies, an institutional repository, and a preprint server for the global geoscience community. I am CDL’s Crossref board alternate, an advisory board member for the Directory of Open Access Journals, a co-chair of the Digital Library Federation Committee on Equity and Inclusion and vice-chair of its Gallery, Library, Archive, and Museum Diversity subgroup.  I co-chaired the ORCID Business Steering Group, was a founding editorial board member of the Journal of Librarianship and Scholarly Communication, and served on boards of U.S. educational non-profit organizations.  

My in depth understanding of the scholarly communication ecosystem and years implementing Crossref services for eScholarship’s publishing program give me a valuable perspective on Crossref’s specific role, including with regard to library publishers and community infrastructure developers. I understand, programmatically and technically, the benefits and challenges of PIDs and have strengths in organizational development, management, strategic planning, and assessment.  I am thus well positioned to support Crossref in its forward progress.   

I received my A.B. in Political Science from Bryn Mawr College, and my M.L.I.S. and Ph.D. in Library and Information Studies from the University of California, Berkeley. I propose Catherine Mitchell, Director of Publishing, Archives, & Digitization and current Crossref board member as CDL’s alternate.  


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Français" %}}

### Déclaration de l’organisation


Depuis près de vingt ans, la California Digital Library (CDL) de l’Université de Californie (UC) est un leader de la publication en bibliothèque, avec maintenant près de 90 revues en libre accès sur eScholarship, notre plateforme phare de publication en libre accès. Notre programme en constante expansion soutient les revues par abonnement qui cherchent à passer au libre accès, les disciplines émergentes, les chercheurs dans des domaines aux marchés commerciaux limités, les praticiens de domaines appliqués et les étudiants de deuxième et de troisième cycles.  En plus de ce programme de revues, nos services comprennent : un dépôt institutionnel pour une myriade de genres, des documents de travail aux thèses et mémoires électroniques ; un système de gestion de l’information de recherche pour mettre en œuvre les politiques d’accès ouvert étendues de l’UC ; un serveur de préimpression pour la communauté géoscientifique mondiale ; et un programme de « laboratoires (Labs) » conçu pour expérimenter avec de nouvelles approches et de nouveaux outils de publication.  Cette constellation de services place CDL directement au cœur des besoins et des ambitions de nos partenaires d’édition et, en tant que centre technologique, CDL a bien conscience de la complexité associée à ces ambitions, y compris des défis liés à la collecte de métadonnées de base auprès des utilisateurs, à l’intégration de plateformes et à la provision d’expériences optimales pour les utilisateurs finaux.   

CDL collabore activement avec les organisations essentielles à l’écosystème des communications universitaires, y compris, mais sans s’y limiter : Crossref (membre de l’organisation et du conseil d’administration), Library Publishing Coalition (membre de l’organisation et du conseil d’administration), Directory of Open Access Journals (membre du conseil consultatif), Coalition of Open Access Policy Institutions (membre du comité directeur), ORCID (membre), et Open Access Scholarly Publishing Association (membre).  CDL est également co-PI dans le cadre de l’initiative d’édition de bibliothèques de prochaine génération, qui met en place une plateforme communautaire d’édition de bibliothèques en open source dans le but d’intégrer et d’élargir les solutions communautaires existantes.  CDL présente à chacun de ces groupes les exigences, les objectifs et l’impact des activités d’édition soutenues par les institutions. Il s’agit d’un domaine en plein essor de la communication savante vers lequel les chercheurs se tournent de plus en plus pour trouver des solutions fiables.  La continuité de la voix des éditeurs de bibliothèques au sein du conseil d’administration renforcerait encore les efforts de Crossref pour atteindre ses objectifs fondamentaux et son programme stratégique récemment mis à jour, en particulier dans les domaines touchant à l’élargissement de la participation des parties prenantes et de la communauté, et à l’amélioration des services et des métadonnées.  


### Déclaration personnelle  

En tant que membre de longue date de la communauté grandissante de l’édition des bibliothèques, je suis impatiente d’aider Crossref dans ses efforts pour mieux soutenir un groupe plus large de parties prenantes en apportant mes connaissances techniques, programmatiques et organisationnelles à une entité charnière dans l’espace de la communication universitaire.   

En tant que directrice associée du groupe des publications, des archives et de la numérisation à la California Digital Library (CDL), je guide la planification stratégique, le développement et les opérations des services soutenant les activités d’édition, de recherche et d’apprentissage de la communauté universitaire de l’Université de Californie, y compris la plateforme de publication en libre accès d’UC (avec près de 90 revues savantes), un système de gestion de l’information de recherche sous-tendant les politiques d’accès libre d’UC, un référentiel institutionnel et un serveur de préimpression pour la communauté géoscientifique mondiale. Je suis suppléante de CDL au conseil d’administration de Crossref, membre du conseil consultatif du Directory of Open Access Journals (Répertoire des revues en libre accès), coprésidente du Comité sur l’équité et l’inclusion de la Fédération des bibliothèques numériques et vice-présidente de son sous-groupe Galerie, Bibliothèque, Archives et Diversité muséale.  J’ai coprésidé le Business Steering Group d’ORCID, j’ai été membre fondatrice du comité de rédaction du Journal of Librarianship and Scholarly Communication, et j’ai siégé au conseil d’administration d’organismes éducatifs à but non lucratif américains.  

Ma compréhension approfondie de l’écosystème de communication universitaire et mes années de mise en œuvre des services Crossref pour le programme d’édition d’eScholarship me donnent une perspective précieuse sur le rôle spécifique de Crossref, y compris en ce qui concerne les éditeurs de bibliothèques et les développeurs d’infrastructures communautaires. Je comprends, sur les plans programmatique et technique, les avantages et les défis des PID et je dispose d’atouts en matière de développement organisationnel, de gestion, de planification stratégique et d’évaluation.  Je suis donc bien placée pour soutenir Crossref dans ses progrès.   

J’ai obtenu ma licence en sciences politiques au Bryn Mawr College et ma maîtrise et mon doctorat en bibliothéconomie et en sciences de l’information à l’université de Californie à Berkeley. Je propose Catherine Mitchell, directrice de l’édition, des archives et de la numérisation et membre actuelle du conseil d’administration de Crossref comme remplaçante pour CDL.  


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "em Português" %}}

### Declaração da organização

Por quase vinte anos, a California Digital Library (CDL) da Universidade da Califórnia (UC) tem sido líder em publicações baseadas em bibliotecas, agora com quase 90 periódicos de acesso aberto na eScholarship, nossa principal plataforma de publicação de acesso aberto. Nosso programa em contínua expansão oferece suporte a periódicos por assinatura que queiram passar para o acesso aberto, disciplinas emergentes, acadêmicos em áreas com mercados comerciais limitados, profissionais em áreas de ciências aplicadas, alunos de graduação e pós-graduação.  Além desse programa de periódicos, nossos serviços incluem: um repositório institucional para uma ampla quantidade de gêneros, de documentos de trabalho a teses e dissertações eletrônicas; um sistema de gerenciamento de informações de pesquisa para implementar as políticas de acesso aberto expansivas da UC; um servidor de pré-impressão para a comunidade global de geociências; e um programa “Labs” para experimentar novas abordagens e ferramentas de publicação.  Esta constelação de serviços posiciona a CDL dentro das necessidades e ambições dos nossos parceiros de publicação; como um centro de tecnologia, a CDL está sintonizada com as complexidades de atender a essas ambições, incluindo os desafios envolvidos na coleta de metadados essenciais dos usuários, integração de plataformas e fornecer experiências ideais para o usuário final.   

A CDL está ativamente envolvida com organizações essenciais para o ecossistema de comunicação acadêmica, incluindo, mas não se limitando a: Crossref (membro organizacional e do conselho); Library Publishing Coalition (membro organizacional e do conselho); Directory of Open Access Journals (membro do conselho consultivo); Coalition of Open Access Policy Institutions (membro do comitê de direção); ORCID (membro); e Open Access Scholarly Publishing Association (membro).  A CDL também é um Co-PI na iniciativa Next Generation Library Publishing, que está construindo uma plataforma de publicação de biblioteca de código e aberto baseada na comunidade que integra e expande as soluções existentes da comunidade.  Para cada um desses grupos, a CDL traz a consciência dos requisitos, objetivos e impacto das atividades de publicação com suporte institucional, uma área em crescimento dinâmico dentro da comunicação científica, à qual os estudiosos recorrem cada vez mais em busca de soluções confiáveis.  A continuidade da voz do editor da biblioteca no conselho fortaleceria ainda mais os esforços da Crossref para cumprir seus principais objetivos e sua agenda estratégica recentemente atualizada, especialmente naquelas áreas relacionadas à ampliação do envolvimento das partes interessadas e da comunidade, e melhoria de serviços e metadados.  


### Declaração pessoal


Como membro de longa data da crescente comunidade de publicação de bibliotecas, estou ansiosa para ajudar a Crossref em seus esforços para melhor apoiar um grupo mais amplo de partes interessadas, trazendo meu conhecimento técnico, programático e organizacional para uma entidade que é um eixo central no espaço de comunicação acadêmica.   

Como Diretora Associada do Grupo de Publicação, Arquivos e Digitalização da California Digital Library (CDL), eu oriento o planejamento estratégico, o desenvolvimento e as operações de serviços de apoio à publicação, pesquisa e atividades de aprendizagem da comunidade acadêmica da Universidade da Califórnia, incluindo a plataforma de publicação de acesso aberto da UC (com quase 90 periódicos acadêmicos), um sistema de gerenciamento de informações de pesquisa que embasa as políticas de acesso aberto da UC, um repositório institucional e um servidor de pré-impressão para a comunidade global de geociências. Sou a suplente do conselho da Crossref na CDL, membro do conselho consultivo do Directory of Open Access Journals, copresidente do Comitê de Equidade e Inclusão da Digital Library Federation e vice-presidente do seu subgrupo Gallery, Library, Archive e Museum Diversity.  Eu copresidi o ORCID Business Steering Group, fui um dos membros fundadores do conselho editorial do Journal of Librarianship and Scholarly Communication e atuei em conselhos de organizações educacionais sem fins lucrativos dos EUA.  

Meu profundo entendimento do ecossistema de comunicação acadêmica e os anos de implementação de serviços da Crossref para o programa de publicação da eScholarship me dão uma perspectiva valiosa sobre o papel específico da Crossref, incluindo o que diz respeito a editores de bibliotecas e desenvolvedores de infraestrutura da comunidade. Entendo, de forma programática e técnica, os benefícios e desafios dos PIDs e tenho pontos fortes em desenvolvimento organizacional, gestão, planejamento estratégico e avaliação.  Estou, portanto, bem posicionada para apoiar a Crossref em seu progresso.   

Graduei-me no bacharelado em Ciência Política da Bryn Mawr College e recebi meu Master of Library and Information Science e Ph.D. em Biblioteconomia e Estudos da Informação pela Universidade da Califórnia, Berkeley. Proponho Catherine Mitchell, Diretora de Publicação, Arquivos e Digitalização e atual membro do conselho da Crossref como suplente da CDL.  


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Español" %}}

### Declaración de la organización

Durante casi veinte años, la Biblioteca Digital de California (CDL, por sus siglas en inglés) de la Universidad de California (UC), ha sido líder en la publicación basada en bibliotecas y, en la actualidad, cuenta con casi 90 revistas de acceso abierto en eScholarship, nuestra plataforma insignia de publicación de acceso abierto. Nuestro programa, que está en continua expansión, presta apoyo a las revistas por suscripción que quieran pasarse al acceso abierto, las disciplinas emergentes, los académicos en campos con mercados comerciales limitados, los profesionales de campos aplicados y los estudiantes de grado y posgrado.  Además de este programa de revistas, nuestros servicios incluyen: un repositorio institucional para infinidad de géneros, desde documentos de trabajo hasta tesis y trabajos finales en formato electrónico; un sistema de gestión de la información de investigación para implantar las amplias políticas de acceso abierto de la UC, un servidor de prepublicaciones para la comunidad de geociencia global; y un programa «Labs» para experimentar con los nuevos enfoques y herramientas de publicación.  Esta variedad de servicios sitúa a la CDL directamente en las necesidades y ambiciones de nuestros socios editoriales y, como centro tecnológico, la CDL se ajusta a las complejidades que implica el cumplimiento de tales ambiciones, incluidos los desafíos a la hora de recopilar metadatos de los usuarios, integrar plataformas y ofrecer experiencias óptimas al usuario final.   

La CDL colabora de forma activa con organizaciones clave para el ecosistema académico de comunicaciones, entre las que se incluyen: Crossref (miembro organizativo y de la Junta); Library Publishing Coalition (miembro organizativo y de la Junta); Directory of Open Access Journals (miembro del comité asesor); Coalition of Open Access Policy Institutions (miembro del comité directivo); ORCID (miembro); y Open Access Scholarly Publishing Association (miembro).  La CDL también es coinvestigadora de la iniciativa Next Generation Library Publishing, que está construyendo una plataforma de publicación bibliotecaria de acceso abierto basada en la comunidad, que integra y desarrolla las soluciones comunitarias existentes.  Para cada uno de estos grupos, la CDL sensibiliza sobre los requisitos, los objetivos y el impacto de las actividades de publicación con respaldo institucional, una dinámica área de crecimiento dentro de la comunicación académica a la que recurren cada vez más académicos para encontrar soluciones fiables.  La continuidad de la voz de los editores bibliotecarios en la Junta Directiva reforzaría más el trabajo de Crossref para cumplir sus objetivos principales y su programa estratégico actualizado recientemente, en especial, las áreas relativas a la ampliación de la participación de las partes interesadas y la comunidad, así como a la mejora de los servicios y los metadatos.  

### Declaración personal

Como miembro veterano de la creciente comunidad de publicaciones bibliotecarias, deseo ayudar a Crossref en su labor para prestar un mejor apoyo a un grupo de partes interesadas más amplio aportando mi conocimiento técnico, programático y organizativo a una entidad fundamental para el espacio de comunicación académica.   

Como directora adjunta del grupo de publicación, archivos y digitalización de la Biblioteca Digital de California, oriento la planificación estratégica, el desarrollo y las operaciones de servicios que permiten la publicación, investigación y las actividades didácticas de la comunidad académica de la Universidad de California, incluida la plataforma de publicación de acceso abierto de la UC (con casi 90 revistas académicas), un sistema de gestión de la información de investigación que sustenta las políticas de acceso abierto de la UC, un repositorio institucional y un servidor de prepublicaciones para la comunidad de geociencia global. Soy sustituta de la Junta de Crossref de la CDL, miembro del comité asesor del Directory of Open Access Journals, copresidenta del Digital Library Federation Committee on Equity and Inclusion (comité sobre equidad e inclusión de la Federación de Bibliotecas Digitales) y vicepresidenta de su subgrupo de diversidad de galerías, bibliotecas, archivos y museos.  He sido copresidenta del grupo directivo empresarial de ORCID y miembro fundador del consejo editorial del Journal of Librarianship and Scholarly Communication. Asimismo, he formado parte de juntas de instituciones educativas sin ánimo de lucro de EE. UU.  

Mi profundo conocimiento del ecosistema de comunicación académica y los años implementado los servicios de Crossref en el programa de publicación de eScholarship me aportan una valiosa perspectiva de la función específica de Crossref, también en lo que concierne a los editores bibliotecarios y los desarrolladores de infraestructuras comunitarias. Desde el punto de vista programático y técnico, comprendo los beneficios y desafíos de los PID y domino el desarrollo organizativo, la gestión, la planificación estratégica y la evaluación.  Así pues, reúno las condiciones para ayudar a Crossref en su progreso.   

Me licencié en Ciencias Políticas por la Bryn Mawr College y completé un máster en Bibliotecología y Ciencias de la Información y un doctorado en Bibliotecología y Estudios de la Información en la Universidad de California (Berkeley). Propongo como mi sustituta de CDL a Catherine Mitchell, directora de Edición, Archivos y Digitalización y actual miembro de la Junta de Crossref.  


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "한국어" %}}
### 단체소개서


약 20년 동안 캘리포니아 대학교(UC)에 위치한 캘리포니아 디지털 도서관(CDL)은 도서관 기반 출판에서 선두를 달리고 있고, 현재 저희의 플래그십 오픈 액세스 출판 플랫폼인 eScholarship에 90여 개의 오픈 액세스 저널을 보유하고 있습니다. 계속해서 확장하고 있는 저희의 프로그램은 오픈 액세스 저널로 전환하고자 하는 구독기반 저널, 신생 학문, 제한된 상업 시장을 갖는 분야의 학자들, 응용 분야의 실무자들, 대학생 및 대학원생을 지원합니다.  저희 서비스에는 이러한 저널 프로그램뿐만 아니라 작업 서류에서 전자형 학위 논문에 이르는 다양한 장르의 기관 리퍼지터리, UC의 광범위한 오픈 액세스 정책을 시행하기 위한 연구 정보 관리 시스템, 전 세계 지구과학 커뮤니티용 프리프린트 서버 및 새로운 출판 접근법 및 도구를 시험하기 위한 ‘실험실’ 프로그램이 포함됩니다.  CDL은 이러한 다양한 서비스를 제공하여 협력 출판사의 필요와 야망을 정확히 충족하고, 기술 센터로서 CDL은 그러한 야망을 충족하는 데 대한 복잡성에 맞추며, 이 복잡성에는 사용자로부터 핵심 메타데이터를 수집하고, 플랫폼을 통합하고, 최적의 최종 사용자 경험을 제공하는 데 관련된 어려움이 포함됩니다.   

CDL은 학술적 의사소통 생태계에 필수적인 단체와 적극적으로 연관을 맺고 있으며, 여기에는 다음의 단체들이 포함되나 이에 국한되지는 않습니다. CDL과 관련된 단체에는 Crossref(단체 및 이사회 회원), 도서관 출판 연합(단체 및 이사회 회원), 오픈 액세스 저널 디렉터리(자문 위원회 회원), 오픈 액세스 정책 기관 연합(운영 위원), ORCID(회원) 및 오픈액세스 학술출판 협회(회원)가 있습니다.  또한 CDL은 차세대 도서관 출판 이니셔티브의 Co-PI로, 이 이니셔티브는 커뮤니티 기반의 오픈 소스 도서관 출판 플랫폼을 구축하여 기존 커뮤니티 솔루션을 통합하고 확장합니다.  CDL은 각 단체가 기관에서 지원하는 출판 활동의 요구 사항, 목표 및 영향에 대한 인식을 갖도록 하며, 이 활동은 점점 더 많은 학자들이 신뢰할 수 있는 솔루션을 얻기 위해 참조하는 학술 커뮤니케이션 내에서 활발하게 성장하고 있는 영역입니다.  이사회에 도서관 출판사의 목소리를 지속해서 내면 Crossref는 단체의 핵심 목표와 최근에 업데이트된 전략적 의제를 충족하기 위해 더욱 노력할 것이고, 특히 이해당사자 및 커뮤니티 참여를 확대하고 서비스 및 메타데이터를 개선할 것입니다.  


## 자기소개서

성장하고 있는 도서관 출판 커뮤니티의 오랜 회원으로서 저는 제 기술, 프로그램, 조직에 대한 지식을 학술적 소통 공간의 린치핀 개체에 기여하여 Crossref가 더욱 폭넓은 이해관계자 단체에 더 나은 지원을 제공하도록 돕고 싶습니다.   

저는 캘리포니아 디지털 도서관(CDL) 내 출판, 아카이브 및 디지털화 그룹의 부책임자로서 전략적 계획, 개발과 출판, 연구, 학습 활동 출판 연구 및 캘리포니아 대학교 학문 커뮤니티의 학습 활동을 지원하는 서비스 운영을 감독합니다. 캘리포니아 대학교 학문 커뮤니티에는 UC의 오픈 액세스 출판 플랫폼(90여 개의 학술 저널), UC의 오픈 액세스 정책을 보강하는 연구 정보 관리 시스템, 기관 리포지토리 및 전 세계 지구과학 커뮤니티를 위한 프리프린트 서버가 포함됩니다. 저는 CDL의 Crossref 이사회의 대체 이사, 오픈 액세스 저널 디렉터리의 자문 위원회 회원, 공정성 및 포용에 대한 디지털 도서관 연맹 위원회의 공동 의장과 이 위원회 소속인 갤러리, 도서관, 아카이브 및 도서관 다양성 하부 단체의 부의장을 맡고 있습니다.  저는 ORCID 사업 운영 그룹의 공동 의장을 맡았고, Journal of Librarianship and Scholarly Communication의 창립 편집 이사회 회원이었고, 미국의 비영리 교육 조직에서 이사로 일했습니다.  

학술적 소통 생태계에 대한 깊은 이해와 eScholarship의 출판 프로그램에 대해 Crossref 서비스를 시행한 시간을 통해 도서관 출판사 및 커뮤니티 인프라 개발자와 관련한 역할을 포함하여 Crossref의 특정 역할에 대해 중요한 시각을 얻게 되었습니다. 저는 PID의 이점과 어려움을 프로그램과 기술적으로 이해하고 조직 개발, 운영, 전략적 계획 및 평가에 강점이 있습니다.  따라서 저는 Crossref의 진보를 잘 지원할 수 있습니다.   

저는 브린 모어 대학(Bryn Mawr College)에서 정치학 학위를 받았고, 캘리포니아 대학교(University of California, Berkeley)에서 도서관 및 정보학 석사와 박사 학위를 받았습니다. 저는 Catherine Mitchell을 출판, 아카이브 및 디지털화 책임자 및 CDL의 대안 이사로 현 Crossref 이사회 회원으로 추천합니다.  

{{% /accordion %}}
{{% /accordion-section %}}

***

<a id="cos"></a>
## Nici Pfeiffer, Center for Open Science, USA<br>
<img src="/images/board-governance/nici-pfeiffer.png" alt="Nici Pfeiffer, Center for Open Science" width="225px" class="img-responsive" />

{{% accordion %}}
{{% accordion-section "in English" %}}
### Organization statement

The Center for Open Science is a mission-driven nonprofit focused on enacting change in the culture and incentives that drive researcher behavior, the infrastructure that supports their research, and the business models that dominate scholarly communication. In becoming a member of the Crossref board, COS can contribute to the community through bringing a global perspective of infrastructure that supports the entire research lifecycle to make it easy and efficient for researcher producers to collaborate, manage, and share their research with increased rigor, transparency, and reproducibility. For example, our community-operated preprint servers now ask authors to assert their open data, preregistrations, and conflicts of interest in the submission workflow. An opportunity to collaborate with the Crossref strategy could be to formalize in the metadata open science practices related to connecting research findings with the data, preregistrations, code, and supplemental materials more formally. There is rich potential to collaborate and align the research outputs, their metadata, and improved research incentives and policies with advancing open science to change the research culture.  


### Personal statement<br>

In my role as Chief Product Officer at the Center for Open Science I have worked with and across many communities and stakeholders to understand through empathy, promote adoption through training and outreach, and implement infrastructure to support the practices of open, transparent, and rigorous in research workflows. Producing a variety of global connections and knowledge of research workflows, challenges, and opportunities to develop solutions to shift the culture to more open and accelerated sharing and discovery. The Crossref community is diverse and intelligent, and should have a vision to balance perspectives across the scholarly research ecosystem.  


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Français" %}}
### Déclaration de l’organisation

Le Center for Open Science est un organisme à but non lucratif axé sur sa mission, qui porte sur l’adoption de changements dans la culture et les incitations qui stimulent le comportement des chercheurs, l’infrastructure qui soutient leur recherche et les modèles d’affaires qui dominent la communication universitaire. En devenant membre du conseil d’administration de Crossref, COS peut contribuer à la communauté en apportant une perspective globale de l’infrastructure qui soutient l’ensemble du cycle de vie de la recherche afin de permettre aux producteurs de recherches de collaborer, de gérer et de partager leurs recherches avec plus de rigueur, de transparence et de reproductibilité. Par exemple, nos serveurs de préimpression gérés par la communauté demandent désormais aux auteurs de fournir leurs données ouvertes, leurs pré-enregistrements et leurs conflits d’intérêts dans le flux de travail de soumission. Une occasion de collaborer avec la stratégie Crossref pourrait consister à officialiser de manière plus formelle dans les métadonnées les pratiques scientifiques ouvertes liées à la mise en relation des résultats de la recherche avec les données, les pré-enregistrements, le code et les documents supplémentaires. Il existe un vaste potentiel de collaboration et d’harmonisation des résultats de la recherche, des métadonnées et d’amélioration des incitations et des politiques de recherche en faisant progresser la science ouverte pour changer la culture de recherche.  


### Déclaration personnelle

En tant que chef de produit au Center for Open Science, j’ai travaillé avec et au sein de nombreuses communautés et parties prenantes pour comprendre par l’empathie, promouvoir l’adoption par la formation et la sensibilisation, et mettre en œuvre une infrastructure pour soutenir les pratiques d’ouverture, de transparence et de rigueur dans les flux de travail de recherche. J’ai développé diverses connexions et connaissances mondiales sur les flux de travail de recherche, les défis et les opportunités pour développer des solutions afin de transformer la culture en faveur d’un partage et d’une découverte plus ouverts et accélérés. La communauté Crossref est diversifiée et intelligente, et devrait avoir pour vision d’équilibrer les perspectives dans l’écosystème de la recherche universitaire.  


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "em Português" %}}
### Declaração da organização

O Center for Open Science é uma organização sem fins lucrativos voltada para a missão, focada em promover mudanças na cultura e nos incentivos que conduzem o comportamento do pesquisador, a infraestrutura que apoia sua pesquisa e os modelos de negócios que dominam a comunicação acadêmica.  

 Ao se tornar um membro do conselho da Crossref, o COS pode contribuir com a comunidade, trazendo uma perspectiva global de infraestrutura, que oferece suporte a todo o ciclo de vida da pesquisa para facilitar e tornar mais eficiente para os produtores pesquisadores colaborarem, gerenciarem e compartilharem suas pesquisas com maior rigor, transparência e reprodutibilidade. Por exemplo, nossos servidores de pré-impressão operados pela comunidade agora pedem aos autores que declarem seus dados abertos, pré-registros e conflitos de interesse no fluxo de trabalho de envio. Uma oportunidade de colaborar com a estratégia da Crossref poderia ser formalizar nos metadados as práticas de ciência aberta relacionadas à conexão dos resultados da pesquisa com os dados, pré-registros, código e materiais complementares de forma mais formal. O avanço da ciência aberta para mudar a cultura da pesquisa traz um grande potencial para colaborar e alinhar os resultados da pesquisa, seus metadados e melhores incentivos e políticas de pesquisa.  

### Declaração pessoal

Em minha função como Diretora de Produto no Center for Open Science, trabalhei com e em muitas comunidades e partes interessadas para entender, pela empatia, promover a adoção por meio de capacitação e divulgação, e implementar infraestrutura para apoiar as práticas de abertura, transparência e rigor em fluxos de trabalho de pesquisa. Produzindo uma variedade de conexões globais e conhecimento de fluxos de trabalho de pesquisa, desafios e oportunidades de desenvolver soluções para mudar a cultura para um compartilhamento e descoberta mais abertos e acelerados. A comunidade da Crossref é diversificada e inteligente, e deve ter uma visão para equilibrar as perspectivas em todo o ecossistema de pesquisa acadêmica.  

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Español" %}}
### Declaración de la organización

El Center for Open Science es una organización sin ánimo de lucro impulsada por su misión que se centra en promulgar el cambio en la cultura y los incentivos que dirigen el comportamiento del investigador, la infraestructura en la que se basa su investigación y los modelos de negocio que prevalecen en la comunicación académica. Al ingresar como miembro de la Junta de Crossref, el COS puede contribuir a la comunidad aportando una perspectiva global de la infraestructura que sustenta todo el ciclo de vida de la investigación para que a los productores de la investigación les resulte fácil y eficiente colaborar, gestionar y compartir su investigación con rigor, transparencia y capacidad de reproducción. Por ejemplo, nuestros servidores de prepublicación, gestionados por la comunidad, ahora piden a los autores que declaren sus datos abiertos, prerregistros y conflictos de intereses en el flujo de trabajo de las propuestas. Una oportunidad de colaborar con la estrategia de Crossref sería formalizar en los metadatos las prácticas de ciencia abierta relacionadas con la vinculación de los resultados de la investigación con los datos, los prerregistros, el código y los materiales complementarios de manera más formal. Hay un gran potencial para colaborar y ajustarse a los resultados de la investigación, sus metadatos e incentivos y políticas de investigación mejorados con el avance de la ciencia abierta para cambiar la cultura de investigación.  


### Declaración personal

En mi función como responsable de Producto en el Center for Open Science, he trabajado con muchas comunidades y partes interesadas para comprender a través de la empatía, fomentar la adopción a través de la formación y la divulgación y poner en marcha la infraestructura para apoyar las prácticas dirigidas a unos flujos de trabajo de investigación abiertos, transparentes y rigurosos. De esta forma, se producen diversas conexiones globales y el conocimiento de los flujos de trabajo, desafíos y oportunidades de la investigación para desarrollar soluciones que cambien la cultura hacia un intercambio y descubrimiento más abierto y acelerado. La comunidad Crossref es diversa e inteligente, y debería tener una visión para equilibrar las perspectivas dentro del ecosistema de investigación académica.  


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "한국어" %}}
### 단체소개서

오픈 사이언스 센터는 사명을 중심으로 하는 비영리 단체로, 연구원의 행동을 이끄는 문화와 동기, 그들의 연구를 지원하는 인프라, 그리고 학술적 소통을 주도하는 사업 모델의 변화를 일으키는 데 중점을 둡니다. COS가 Crossref 이사회의 회원이 된다면 인프라에 대한 세계적인 시각을 가져와서 커뮤니티에 기여할 수 있습니다. 이 인프라는 전체 연구 라이프사이클을 지원하여 연구 생산자가 더욱 엄격하고, 투명하고, 재현성 있는 연구를 쉽고 효율적으로 협업, 관리 및 공유할 수 있도록 합니다. 예를 들어, 현재 커뮤니티가 운영하는 저희의 프리프린트 서버를 통해 저자가 작업 흐름 제출에서 그들의 공개 데이터, 사전 등록 및 이해 충돌에 대해 주장하라고 요청합니다. Crossref 전략과의 협업 기회는 메타데이터 오픈 사이언스 프랙티스에서 연구 결과를 데이터, 사전 등록, 코드 및 보조 자료와 더 형식적으로 연결하는 것과 관련하여 공식화하는 것일 수 있습니다. 연구 결과물과 이것의 메타데이터, 발전된 연구 인센티브와 정책을 진보한 오픈 사이언스에 맞춰 협력하고 조정하여 연구 문화를 바꿀 수 있는 풍부한 잠재력이 있습니다.  

### 자기소개서

오픈 사이언스 센터에서 수석 상품 책임자를 맡으며 많은 커뮤니티 및 주주와 협력하여 공감대를 통해 이해하고, 훈련과 봉사 활동을 통해 채용을 촉진하고, 인프라를 구현하여 연구 작업 흐름에서 개방적이고 투명하며 엄격한 프랙티스를 지원하였습니다. 연구 작업 흐름, 과제 및 솔루션을 개발할 기회에 대해 다양하고 세계적인 연결과 지식을 생산하여 더 개방적이고 공유 및 발견을 촉진하는 문화로 바꿉니다. Crossref 커뮤니티는 다양하고 지적이며, 이 커뮤니티는 사명을 가져 학문적 연구 생태계 전반에 걸친 균형 잡힌 시각을 가져야 합니다.  

{{% /accordion %}}
{{% /accordion-section %}}

***

<a id="mra"></a>
## Kristen Mueller, Melanoma Research Alliance, USA<br>
<img src="/images/board-governance/kristen-mueller.jpg" alt="Kristen Mueller, Melanoma Research Alliance" width="225px" class="img-responsive" />   

{{% accordion %}}
{{% accordion-section "in English" %}}

### Organization statement
The Melanoma Research Alliance (MRA) is the largest non-profit funder of melanoma research worldwide. MRA’s mission is to end suffering and death due to melanoma by collaborating with all stakeholders to accelerate powerful research, advance cures for all patients, and prevent more melanomas. Since its founding in 2007, MRA has committed $131 million in funding to 380 projects at 158 research institutions in 19 countries in the areas of melanoma prevention, diagnosis, and treatment.   

MRA-supported projects are selected by a world-class Grant Review Committee. The NIH recognizes MRA as a Cancer Center Support Grants funder, which means that MRA meets specific minimum levels of peer-reviewed, funded research projects.   

MRA recognizes the critical importance of persistent object identifiers (PIDs), like DOIs and ORCID iDs, to the research community. In April 2021, MRA became the first organization in Proposal Central to successfully register grant DOIs for all of our active awards, and as of June 2021 all MRA grants have a registered DOI. By registering DOIs for all awards, requiring ORCID iDs for all applicants (as of 2017), and joining the US ORCID Community in 2020, MRA seeks to reduce the administrative burden on applicants and awardees, increase discoverability of our research grants, and make research outcomes from the grants we fund accurately identifiable. Use of PIDs will also allow MRA to capture more complete, timely, and accurate data to inform our scientific strategy and report MRA-funded advancements to our key stakeholders.  


### Personal statement<br>

I am a Ph.D. immunologist by training and have nearly 20 years of experience in grants funding, scientific publishing, and biomedical research, providing me with a unique perspective that I am excited to bring to the Crossref Board of Directors.  

In 2017, I joined the Melanoma Research Alliance (MRA) as the Senior Director, Scientific Program. I oversee MRA’s grants program, including the solicitation, peer review, and oversight of $10-$13 million/ year in funded programs. I have led MRA’s efforts to bring PIDs to the funding sector by requiring ORCID iDs for all applicants, joining the US ORCID Community, and registering DOIs for all of MRA’s awards through Proposal Central/Altum’s partnership with Crossref. In doing so, MRA aims to make what we fund more transparent, reduce administrative burden, and improve the analysis and evaluation of our research portfolio.  

Prior to joining MRA, I was a Senior Editor at the journal Science. My eight years at Science introduced me to the importance of PIDs and the potential of the metadata encoded to create transparent and discoverable connections between articles, other research outputs, grants, researchers, and institutions.  

In 2020, I joined the Board of Directors of the Health Research Alliance (HRA), a collaborative member organization of nonprofit biomedical research funders. Through HRA, I regularly communicate with colleagues from > 80 funding institutions about issues including grants administration and evaluation, open science, and workforce development.  

If elected to the Board, I would use these experiences to help strengthen relationships between funders and Crossref, and help educate and communicate to the funding community the benefits of PIDs such as DOIs. More broadly, I would help Crossref realize the goals set out in their 2021 strategic plan. I applaud Crossref’s commitment to further engage the funding community and am excited to help them realize this goal.  


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Français" %}}
### Déclaration de l’organisation


La Melanoma Research Alliance (MRA) est le plus important financeur à but non lucratif de la recherche sur le mélanome dans le monde. La mission de la MRA est de mettre fin à la souffrance et à la mort dues au mélanome en collaborant avec toutes les parties prenantes pour accélérer une recherche performante, faire progresser les traitements pour tous les patients et prévenir plus de mélanomes. Depuis sa création en 2007, la MRA a engagé 131 millions de dollars pour financer 380 projets dans 158 établissements de recherche de 19 pays dans les domaines de la prévention, du diagnostic et du traitement du mélanome.   

Les projets appuyés par la MRA sont sélectionnés par un comité d’examen des subventions de calibre mondial. Le NIH reconnaît la MRA en tant que donateur de subventions de soutien aux centres de cancérologie, ce qui signifie que la MRA répond à des niveaux minimums spécifiques de projets de recherche financés et évalués par les pairs.   

La MRA reconnaît l’importance critique des identificateurs d’objets persistants (PID), comme les DOI et les ID ORCID, dans le milieu de la recherche. En avril 2021, la MRA est devenue la première organisation de Proposal Central à enregistrer avec succès les DOI de subventions pour toutes nos bourses actives, et depuis juin 2021, toutes les subventions de la MRA ont un DOI enregistré. En enregistrant des DOI pour toutes les bourses, en exigeant des ID ORCID pour tous les candidats (à partir de 2017) et en rejoignant la communauté ORCID des États-Unis en 2020, la MRA cherche à réduire le fardeau administratif des candidats et des boursiers, à faciliter la découverte de nos subventions de recherche et à rendre les résultats de recherche découlant des subventions que nous finançons identifiables avec précision. L’utilisation des PID permettra également à la MRA de saisir des données plus complètes, plus opportunes et plus précises pour éclairer notre stratégie scientifique et de présenter les progrès financés par la MRA à nos principales parties prenantes.  


### Déclaration personnelle

Je suis docteur en immunologie de formation et j’ai près de 20 ans d’expérience dans le financement de subventions, l’édition scientifique et la recherche biomédicale, ce qui me donne une perspective unique que je suis heureuse d’apporter au conseil d’administration de Crossref.  

En 2017, j’ai rejoint la Melanoma Research Alliance (MRA) en tant que directrice principale du programme scientifique. Je supervise le programme de subventions de la MRA, y compris la sollicitation, l’examen par les pairs et la surveillance de 10 à 13 millions de dollars de programmes financés par an. J’ai dirigé les efforts de la MRA pour amener les PID dans le secteur du financement en exigeant des ID ORCID pour tous les candidats, en rejoignant la communauté ORCID des États-Unis et en enregistrant les DOI pour tous les prix financés par la MRA par le biais du partenariat de Proposal Central/Altum avec Crossref. Ce faisant, la MRA vise à rendre les projets que nous finançons plus transparents, à réduire le fardeau administratif et à améliorer l’analyse et l’évaluation de notre portefeuille de recherche.  

Avant de me joindre à la MRA, j’étais rédactrice principale de la revue Science. Mes huit années auprès de Science m’ont initiée à l’importance des PID et au potentiel des métadonnées encodées pour créer des liens transparents et découvrables entre les articles, les autres résultats de recherche, les subventions, les chercheurs et les institutions.  

En 2020, j’ai rejoint le conseil d’administration de la Health Research Alliance (HRA), une organisation collaborative à but non lucratif dont les membres sont des financeurs de la recherche biomédicale. Par l’entremise de HRA, je communique régulièrement avec des collègues de plus de 80 établissements de financement sur des questions telles que l’administration et l’évaluation des subventions, la science ouverte et la formation continue du personnel.  

Si je suis élue au conseil d’administration, j’utiliserai ces expériences pour aider à renforcer les relations entre les financeurs et Crossref, et pour aider à éduquer et à communiquer à la communauté de financement les avantages des PID tels que les DOI. Plus largement, j’aiderai Crossref à réaliser les objectifs énoncés dans son plan stratégique de 2021. Je salue l’engagement de Crossref à mobiliser davantage la communauté de financement et je suis enthousiaste à l’idée de l’aider à atteindre cet objectif.  

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "em Português" %}}

### Declaração da organização


A Melanoma Research Alliance (MRA) é a maior financiadora sem fins lucrativos de pesquisas sobre melanoma em todo o mundo. A missão da MRA é acabar com o sofrimento e a morte devidos ao melanoma, colaborando com todas as partes interessadas para acelerar pesquisas poderosas, promover curas para todos os pacientes e prevenir mais melanomas. Desde sua fundação, em 2007, a MRA dedicou US$ 131 milhões ao financiamento de 380 projetos de 158 instituições de pesquisa, em 19 países, nas áreas de prevenção, diagnóstico e tratamento de melanoma.   

Os projetos apoiados pela MRA são selecionados por um Comitê de Revisão de Subsídios de nível mundial. O NIH reconhece a MRA como financiadora do Cancer Center Support Grants, isso significa que a MRA atende a níveis mínimos específicos de projetos de pesquisa financiados com revisão por pares.   

A MRA reconhece a importância crucial de identificadores de objeto persistentes (PIDs), como DOIs e IDs de ORCID, para a comunidade de pesquisa. Em abril de 2021, a MRA se tornou a primeira organização na Proposal Central a registrar, com sucesso, DOI de concessões para todos os nossos prêmios ativos e, em junho de 2021, todas as concessões da MRA tinham um DOI registrado. Ao registrar DOIs para todos os prêmios, exigindo IDs de ORCID para todos os candidatos (a partir de 2017) e ingressando na Comunidade ORCID dos EUA em 2020, a MRA busca reduzir a carga administrativa sobre os candidatos e premiados, aumentar a descoberta das nossas bolsas de pesquisa e tornar os resultados da pesquisa das bolsas que financiamos identificáveis com precisão. O uso de PIDs também permitirá que a MRA capture dados mais completos, oportunos e precisos para informar nossa estratégia científica e relatar os avanços financiados pela MRA aos nossos principais interessados.  

### Declaração pessoal

Eu sou imunologista Ph.D. de formação e tenho quase 20 anos de experiência em financiamento de bolsas, publicação científica e pesquisa biomédica, o que me proporciona uma perspectiva única que estou animada para apresentar ao Conselho Diretor da Crossref.  

Em 2017, entrei para a Melanoma Research Alliance (MRA) como Diretora Sênior do Programa Científico. Eu supervisiono o programa de subsídios da MRA, incluindo a solicitação, revisão por pares e supervisão de US$ 10 a 13 milhões/ano em programas financiados. Liderei os esforços da MRA para trazer PIDs para o setor de financiamento, exigindo IDs de ORCID para todos os candidatos, entrando para a comunidade ORCID dos EUA e registrando DOIs para todos os prêmios da MRA por meio da parceria da Proposal Central/Altum com a Crossref. Ao fazer isso, a MRA visa dar mais transparência ao que financiamos, reduzir a carga administrativa e melhorar a análise e avaliação do nosso portfólio de pesquisa.  

Antes de ingressar na MRA, fui Editora Sênior da revista Science. Meus oito anos na Ciência me mostraram a importância dos PIDs e o potencial dos metadados codificados para criar conexões transparentes e detectáveis entre artigos, outros resultados de pesquisa, bolsas, pesquisadores e instituições.  

Em 2020, entrei para o Conselho Diretor da Health Research Alliance (HRA), uma organização-membro colaborativa de financiadores de pesquisas biomédicas sem fins lucrativos. Por meio da HRA, eu me comunico regularmente com colegas de mais de 80 instituições de financiamento sobre questões, tais como administração e avaliação de subsídios, ciência aberta e desenvolvimento da força de trabalho.  

Se eleita para o Conselho, eu usaria essas experiências para ajudar a fortalecer as relações entre os financiadores e a Crossref, além de ajudar a educar e comunicar à comunidade financiadora os benefícios dos PIDs, como os DOIs. De forma mais ampla, eu ajudaria a Crossref a alcançar as metas estabelecidas em seu plano estratégico para 2021. Aplaudo o compromisso da Crossref em envolver ainda mais a comunidade de financiamento e estou animada para ajudá-la a alcançar esse objetivo.  

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Español" %}}

### Declaración de la organización

La Melanoma Research Alliance (MRA) es la mayor organización sin ánimo de lucro que financia la investigación del melanoma en todo el mundo. La misión de la MRA es poner fin al sufrimiento y el fallecimiento causados por el melanoma y colabora con todas las partes interesadas para acelerar una investigación potente, avanzar en las curas para todos los pacientes y prevenir más melanomas. Desde su fundación en 2007, la MRA ha destinado 131 millones de dólares a la financiación de 380 proyectos en 158 instituciones de investigación de 19 países en las áreas prevención, diagnóstico y tratamiento del melanoma.   

Un comité de revisión de subvenciones selecciona los proyectos que apoya la MRA. El NIH reconoce a la MRA como patrocinadora de las subvenciones de apoyo a centros de cáncer, lo que significa que la MRA cumple unos niveles mínimos específicos de proyectos de investigación financiados y arbitrados.   

La MRA reconoce la importancia fundamental de los identificadores persistentes de objetos (PID, por sus siglas en inglés), como los identificadores DOI y ORCID, para la comunidad de investigación. En abril de 2021, la MRA se convirtió en la primera organización de Proposal Central que registró adecuadamente los DOI de las subvenciones para todas nuestras adjudicaciones activas y, desde junio de 2021, todas las subvenciones de la MRA tienen un DOI registrado. Al registrar DOI para todas las adjudicaciones, solicitar identificadores ORCID a todos los candidatos (desde 2017) y unirse a la comunidad ORCID de EE. UU. en 2020, la MRA pretende aliviar el peso administrativo de los candidatos y beneficiarios, aumentar la capacidad de encontrar nuestras subvenciones de investigación y permitir que los resultados de investigación de las subvenciones que financiamos se pueden identificar de forma precisa. El uso de los PID también permitirá a la MRA recopilar datos más completos, oportunos y precisos para fundamentar nuestra estrategia científica e informar sobre los avances que financia la MRA a nuestras partes interesadas.  

### Declaración personal


De formación, soy doctora en inmunología y cuento con casi 20 años de experiencia en la financiación de subvenciones, las publicaciones científicas y la investigación en biomedicina, lo que me proporciona una perspectiva única que estoy encantada de poner a disposición a la Junta Directiva de Crossref.  

En 2017, me uní a la Melanoma Research Alliance (MRA) como directora sénior del Programa Científico. Superviso el programa de subvenciones de la MRA, lo que incluye las solicitudes, la revisión de expertos y el control de entre 10 y 13 millones de dólares al año en programas financiados. He liderado los esfuerzos de la MRA para llevar los PID al sector de la financiación solicitando identificadores ORCID a todos los candidatos, incorporándonos a la comunidad ORCID de EE. UU. y registrando DOI para todas las adjudicaciones de la MRA a través de la colaboración de Proposal Central/Altum con Crossref. En su labor, la MRA tiene como objetivo que lo que financiamos sea más transparente, reducir el peso administrativo y mejorar el análisis y la evaluación de nuestra cartera de investigación.  

Antes de formar parte de la MRA, era editora sénior en la revista Science. Mis ocho años en Science me enseñaron la importancia de los PID y el potencial de los metadatos codificados para crear conexiones transparentes y fáciles de encontrar entre los artículos, otros resultados de la investigación, subvenciones, investigadores e instituciones.  

En 2020, pasé a formar parte de la Junta Directiva de la Health Research Alliance (HRA), una colaborativa organización afiliada de patrocinadores de investigación en biomedicina sin ánimo de lucro. A través de la HRA, estoy en continuo contacto con compañeros de más de 80 instituciones financieras sobre cuestiones que abordan la administración y evaluación de subvenciones, la ciencia abierta y el desarrollo de la mano de obra.  

Si soy elegida para la Junta Directiva, utilizaría estas experiencias para ayudar a fortalecer las relaciones entre los patrocinadores y Crossref y contribuir en la formación y comunicación de los beneficios de los DPI y DOI para la comunidad de financiación. En términos más amplios, ayudaría a Crossref a lograr los objetivos fijados en su plan estratégico para 2021. Aplaudo el compromiso de Crossref para seguir haciendo partícipe a la comunidad de financiación y será un placer ayudar a hacer realidad este objetivo.  

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "한국어" %}}

### 단체소개서
흑색종 연구 연맹(MRA)은 전 세계에서 흑색종을 연구하는 가장 큰 비영리 기금입니다. MRA의 사명은 연구를 강력하게 촉진하고, 모든 환자를 위한 치료를 진전시키며 흑색종을 더욱 예방하기 위해 모든 주주들과 협업하여 흑색종으로 인한 고통과 죽음을 종식하는 것입니다. 2007년에 설립된 이후로 MRA는 흑색종 예방, 진단 및 치료 분야에 대해 19개국 158개 연구 기관의 380개 프로젝트에 1억 3천 백만 달러의 자금을 지원했습니다.   

MRA의 지원을 받는 프로젝트는 세계적 수준의 보조금 심의 위원회에 의해 선정되었습니다. NIH는 MRA를 암센터 지원 보조금 기금으로 인정했고, 이는 MRA가 동료 평가와 자금 지원을 받는 연구 프로젝트에 대한 특정 최소 수준을 충족함을 의미합니다.   

MRA는 DOI 및 ORCID iD와 같이 지속적인 객체 식별자(PIDs, persistent object identifiers)가 연구 커뮤니티에 매우 중요함을 알고 있습니다. 2021년 4월에 MRA는 Proposal Central의 첫 번째 단체가 되어 진행 중인 모든 상에 대한 보조금 DOI를 성공적으로 등록했고, 2021년 6월에는 MRA의 모든 보조금에 대해 DOI를 등록했습니다. MRA는 모든 상에 대해 DOI를 등록하고, 모든 지원자에게 ORCID iD를 요구하고(2017년 기준), 2020년에 미국 ORCID 커뮤니티에 가입함으로써 지원자 및 수상자를 관리하는 부담을 줄이고, 연구 보조금에 대한 검색 용이성을 높이고, 자금을 지원하는 보조금의 연구 결과를 정확하게 식별할 수 있도록 합니다. PID를 사용하면 MRA는 더 완전하고 시기적절하며 정확한 데이터를 발견하여 우리의 과학 전략을 알리고 MRA가 자금 지원을 하는 발전을 주요 주주에 보고할 수도 있을 것입니다.  


### 자기소개서

저는 면역학자로 교육을 받아 박사 학위를 받았으며 보조금 자금 지원, 과학적 출판 및 생명공학 연구에 대해 약 20년 동안 경험을 쌓았고, 이를 통해 갖게 된 독특한 시각을 Crossref 이사회에 기여할 수 있게 되어 기쁘게 생각합니다.  

저는 2017년 흑색종 연구 연맹(Melanoma Research Alliance)에 과학적 프로그램의 수석 이사로 합류했습니다. 저는 MRA의 보조금 프로그램을 감독합니다. 이 프로그램에는 광고, 동료 검토 및 연간 1,000만~1,300만 달러 규모의 자금 지원 프로그램을 감독하는 일이 포함됩니다. 저는 모든 지원자에게 ORCID iD를 요구하고, 미국 ORCID 커뮤니티에 가입하며, Crossref와 Proposal Central/Altum의 파트너십을 통해 MRA의 모든 수상에 대해 DOI를 등록함으로써 펀딩 부문에 PID를 도입하기 위한 MRA의 노력을 이끌었습니다. 그렇게 함으로써 MRA는 우리가 자금을 더 투명하게 지원하고, 관리 부담을 줄이고, 연구 포트폴리오 분석 및 평가를 개선하는 것을 목표로 합니다.  

저는 MRA에 합류하기 전에 journal Science에서 수석 편집자로 있었습니다. Science에서 8년간 근무하며 PID의 중요성과 논문, 기타 연구 결과물, 보조금, 연구자 및 기관 사이의 투명하고 발견 가능한 연결을 생성하기 위해 부호화된 메타데이터의 가능성을 알게 되었습니다.  

2020년에 저는 보건 연구 연합(Health Research Alliance, HRA)에 합류하였습니다. HRA는 비영리 생명공학 연구 기금의 협력적 구성 단체입니다. 저는 HRA를 통해 80개가 넘는 자금 기관의 동료들과 보조금 운용 및 평가, 오픈 사이언스, 인력 개발 등의 문제에 대해 정기적으로 소통합니다.  

제가 이사회에 선출된다면 이 경험을 통해 기금과 Crossref 간의 관계를 강화하고, 기금 커뮤니티에 DOI와 같은 PID의 이점을 교육하고 전달하겠습니다. 더 나아가, Crossref를 도와 2021년 전략 계획에서 제시한 목표를 이루도록 하겠습니다. 기금 커뮤니티를 더욱 모으기 위한 Crossref 위원회의 헌신에 찬사를 보내며 그들이 이 목표를 이루는 데 도움을 줄 수 있어 기쁩니다.  


{{% /accordion %}}
{{% /accordion-section %}}

***

<a id="morressier"></a>
## Sebastian Rose, Morressier, Germany<br>
<img src="/images/board-governance/sebastian-rose.png" alt="Sebastian Rose, Morressier" width="225px" class="img-responsive" />   

{{% accordion %}}
{{% accordion-section "in English" %}}
### Organization statement

As the leading publisher of conference content, Morressier provides valuable insights into the traditionally hidden and offline world of academic conferences by shining a light onto the abstracts, posters, presentations, and datasets that are shared at these events. Morressier disseminates conference content to a global audience, democratizing access to valuable knowledge for the entire scientific community - including those who may not have the funding or seniority to be able to attend an event in-person. With seven years experience working with over 200 of the world’s leading academic societies, associations, institutions, and organizations, Morressier can provide the Crossref board with a unique set of insights into the previously untapped world of conference content.  

Morressier champions the standardization of conference content as part of its goal to organize and bring more legitimacy to this information to increase its usefulness. Already, Morressier assigns DOIs to all conference submissions, extracts metadata from each document, provides ORCID iD sign in options, and powers a search engine to ensure documents are easy to find, cite, link, assess, and reuse. This crucial step perfectly aligns with Crossref’s own mission to put scholarly content in context and make scholarly communications better, broadening its scope to include content outside of the well-established journal article format. As a next step, the Morressier team is working to connect relevant findings to further support collaboration and the discovery of conference research while integrating it into the entire research lifecycle.   


### Personal statement <br>

In my role as Morressier’s Head of Data, I am responsible for managing the data and search services on the platform to ensure all research documents are citable and discoverable. In addition, I oversee our analytics services, which provide data and insights into research reach and conference trends, helping authors understand the impact of their work and conference organizers identify important scientific topics from their event. Having worked with Crossref’s funding registry, content registration, and reference linking for the Morressier platform, I am well acquainted with these services.  

Scholarly communications has historically been slow to change, with underlying systems, formats, and workflows evolving incrementally over time. However, given the events of the last 18 months and the clear need for a more transparent, accessible, and comprehensive research ecosystem, we may be at an inflection point, with more rapid change ahead. Sharing early-stage research findings and promoting discourse around initial experimentation, far before a published article, can be critical to supporting research progress. My vision for the Crossref community, and for the scholarly ecosystem as a whole, is to promote the ability to follow research throughout its entire lifecycle - from a first conference submission all the way to a published paper. All steps taken along the way should be connected and new research formats, including datasets and negative results, standardized to increase the efficiency of the research process and, ultimately, accelerate scientific breakthroughs.  


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Français" %}}

### Déclaration de l’organisation

En tant que principal éditeur de contenu de conférence, Morressier fournit des informations précieuses sur le monde traditionnellement caché et hors ligne des conférences académiques en mettant en lumière les résumés, les affiches, les présentations et les ensembles de données qui sont partagés lors de ces événements. Morressier diffuse le contenu des conférences à un public mondial, démocratisant l’accès à des connaissances précieuses pour l’ensemble de la communauté scientifique, et notamment auprès des personnes qui ne disposent peut-être pas du financement ou de l’ancienneté nécessaires pour pouvoir assister à un événement en personne. Fort de sept années d’expérience auprès de plus de 200 des plus grandes sociétés, associations, institutions et organisations académiques au monde, Morressier peut fournir au conseil d’administration de Crossref un ensemble unique d’informations sur le monde du contenu de conférences, inexploité jusqu’à maintenant.  

Morressier défend la normalisation du contenu des conférences dans le cadre de son objectif visant à organiser et à apporter plus de légitimité à cette information pour en accroître l’utilité. Morressier attribue déjà des DOI à toutes les soumissions de conférence, extrait des métadonnées de chaque document, fournit des options de connexion ID ORCID et alimente un moteur de recherche pour s’assurer que les documents sont faciles à trouver, citer, lier, évaluer et réutiliser. Cette étape cruciale s’inscrit parfaitement dans la mission de Crossref consistant à replacer le contenu scientifique dans son contexte et à améliorer les communications universitaires, tout en élargissant son champ d’application pour inclure le contenu en dehors du format bien établi des articles de revue. Dans un deuxième temps, l’équipe de Morressier travaille à relier les résultats pertinents afin de soutenir davantage la collaboration et la découverte de recherches présentées lors de conférences tout en les intégrant à l’ensemble du cycle de vie de la recherche.   


### Déclaration personnelle

En tant que directeur des données de Morressier, je suis responsable de la gestion des données et des services de recherche sur la plateforme pour m’assurer que tous les documents de recherche sont fiables et identifiables. En outre, je supervise nos services d’analyse, qui fournissent des données et des informations sur la portée de la recherche et les tendances des conférences, aidant les auteurs à comprendre l’impact de leurs travaux et les organisateurs des conférences à identifier les sujets scientifiques importants de leur événement. Ayant travaillé avec le registre de financement de Crossref, l’enregistrement de contenu et la création de liens de référence pour la plateforme de Morressier, je connais déjà bien ces services.   

Les communications universitaires ont toujours changé très lentement, les systèmes, les formats et les flux de travail sous-jacents évoluant progressivement au fil du temps. Cependant, étant donnés les événements des 18 derniers mois et le besoin évident d’un écosystème de recherche plus transparent, accessible et complet, nous nous trouvons sans doute à un point d’inflexion, susceptible de déboucher sur des changements plus rapides à venir. Le partage des premiers résultats de la recherche et la promotion d’une discussion portant sur l’expérimentation initiale, bien avant la publication d’un article, peuvent être essentiels pour soutenir les progrès de la recherche. Ma vision pour la communauté Crossref, et pour l’écosystème scientifique dans son ensemble, est de promouvoir la capacité de suivi de la recherche tout au long de son cycle de vie, depuis sa première présentation lors d’une conférence jusqu’à la publication d’un article. Toutes les mesures prises en cours de route devraient être reliées et de nouveaux formats de recherche, et notamment des ensembles de données et des résultats négatifs, devraient être normalisés pour accroître l’efficacité du processus de recherche et, en fin de compte, accélérer les percées scientifiques.   


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "em Português" %}}

### Declaração da organização

Como editora líder de conteúdo de conferências, a Morressier fornece informações valiosas sobre o mundo tradicionalmente oculto e off-line das conferências acadêmicas, trazendo à luz resumos, pôsteres, apresentações e conjuntos de dados que são compartilhados nesses eventos. A Morressier dissemina o conteúdo de conferências para um público global, democratizando o acesso a um conhecimento valioso para toda a comunidade científica – incluindo aqueles que podem não ter o financiamento ou a senioridade para participar de um evento pessoalmente. Com sete anos de experiência trabalhando com mais de 200 das principais sociedades, associações, instituições e organizações acadêmicas do mundo, a Morressier pode fornecer ao conselho da Crossref um conjunto exclusivo de insights sobre o mundo anteriormente inexplorado do conteúdo de conferências.   

A Morressier defende a padronização do conteúdo de conferências como parte de seu objetivo de organizar e trazer mais legitimidade a essas informações, aumentando sua utilidade. A Morressier já atribui DOIs a todos os envios de conferências, extrai metadados de todos os documentos, fornece opções de login de ORCID iD e capacita um mecanismo de busca para garantir que os documentos sejam fáceis de encontrar, citar, vincular, avaliar e reutilizar. Esta etapa crucial se alinha perfeitamente à própria missão da Crossref de colocar o conteúdo acadêmico em contexto e melhorar as comunicações acadêmicas, ampliando seu escopo para incluir conteúdo fora do bem estabelecido formato de artigo de periódico. Como uma próxima etapa, a equipe da Morressier está trabalhando para conectar descobertas relevantes e apoiar ainda mais a colaboração e a descoberta de pesquisas em conferências, integrando-as a todo o ciclo de vida da pesquisa.    

### Declaração pessoal

Em minha função como chefe de dados da Morressier, sou responsável por gerenciar os dados e serviços de pesquisa na plataforma para garantir que todos os documentos de pesquisa possam ser citados e descobertos. Além disso, supervisiono nossos serviços de análise, que fornecem dados e insights sobre o alcance da pesquisa e tendências de conferências, ajudando os autores a entender o impacto do seu trabalho e ajudando os organizadores de conferências a identificar tópicos científicos importantes dos seus eventos. Tendo trabalhado com o registro de financiamento, registro de conteúdo e links de referência da Crossref para a plataforma Morressier, estou bastante familiarizado com esses serviços.   

Historicamente, as mudanças nas comunicações acadêmicas têm sido lentas, com sistemas, formatos e fluxos de trabalho subjacentes evoluindo gradativamente ao longo do tempo. No entanto, considerando os eventos dos últimos 18 meses e a clara necessidade de um ecossistema de pesquisa mais transparente, acessível e abrangente, podemos ter chegado a um ponto de inflexão, com mudanças mais rápidas à frente. Compartilhar os resultados da pesquisa em estágio inicial e promover o discurso em torno da experimentação inicial, muito antes de um artigo publicado, pode ser fundamental para apoiar o progresso da pesquisa. Minha visão para a comunidade da Crossref, e para o ecossistema acadêmico como um todo, é promover a capacidade de acompanhar a pesquisa ao longo de todo o seu ciclo de vida, desde a apresentação de uma primeira conferência até um artigo publicado. Todas as etapas realizadas ao longo do caminho devem ser conectadas e novos formatos de pesquisa, incluindo conjuntos de dados e resultados negativos, padronizados para aumentar a eficiência do processo de pesquisa e, em última análise, acelerar avanços científicos.   

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Español" %}}

### Declaración de la organización


Como editorial líder en contenido de las conferencias, Morressier ofrece valiosos aportes sobre el mundo tradicionalmente oculto y fuera de internet de las conferencias académicas arrojando luz sobre los resúmenes, pósteres, presentaciones y conjuntos de datos que se comparten en estos eventos. Morressier divulga contenido de las conferencias a un público mundial y democratiza el acceso a un valioso conocimiento para la comunidad científica en su conjunto, incluidas aquellas personas que no cuenten con la financiación o antigüedad suficientes para poder asistir a evento presenciales. Tras siete años de experiencia trabajando con más de 200 agrupaciones académicas, asociaciones, instituciones y organizaciones líderes en el mundo, Morressier puede ofrecer a la Junta de Crossref aportes únicos sobre el contenido del mundo de las conferencias que antes estaba por explorar.  

Morressier defiende la estandarización del contenido de las conferencias como parte de su objetivo para organizar y aportar una mayor legitimidad a esta información y aumentar su utilidad. A día de hoy, Morressier asigna DOI a todas las propuestas para las conferencias, extrae metadatos de cada documento, proporciona opciones de registro de identificadores ORCID y mantiene un motor de búsqueda para garantizar que los documentos sean fáciles de encontrar, citar, vincular, evaluar y reutilizar. Este paso fundamental está en perfecta consonancia con la propia misión de Crossref de poner en contexto el contenido académico y mejorar las comunicaciones académicas, de forma que se amplíe su alcance para incluir el contenido externo al formato del artículo de revista bien configurado. El siguiente paso del equipo de Morressier es trabajar para conectar los hallazgos importantes con el objetivo de seguir apoyando la colaboración y el descubrimiento de la investigación de conferencias al tiempo que se integra en todo el ciclo de vida de la investigación.   

### Declaración personal

En mi función como Director de Datos de Morressier, me ocupo de gestionar los datos y servicios de búsqueda en la plataforma para garantizar que todos los documentos de investigación se puedan citar y encontrar. Además, superviso nuestros servicios de análisis, que proporcionan datos e información al alcance de la investigación y las tendencias de las conferencias, lo que ayuda a los autores a comprender la repercusión de su trabajo y a los organizadores de las conferencias a identificar temas científicos importantes para su evento. Gracias a mi trabajo en el registro de financiación de Crossref, el registro del contenido y la vinculación de referencias para la plataforma de Morressier, estoy muy familiarizado con estos servicios.  

Históricamente, las comunicaciones académicas tardan en cambiar y los sistemas, formatos y flujos de trabajo que subyacen van evolucionando de forma gradual a lo largo del tiempo. No obstante, debido a los eventos de los últimos 18 meses y la evidente necesidad de un ecosistema de investigación más exhaustivo, accesible y transparente, puede que nos encontremos en un punto de inflexión con un futuro cambio que será más rápido. Compartir las conclusiones de investigación iniciales y fomentar la divulgación en torno a la experimentación inicial, mucho antes de la publicación de un artículo, puede ser clave para apoyar el progreso de la investigación. Mi visión de la comunidad de Crossref y del ecosistema académico en su totalidad consiste en fomentar la capacidad de seguir la investigación a lo largo de todo su ciclo de vida, desde la primera propuesta para la conferencia hasta el artículo publicado. Todos los pasos que se tomen por el camino deben estar conectados y los nuevos formatos de investigación, incluidos los conjuntos de datos y los resultados negativos, deben estandarizarse para aumentar la eficiencia del proceso de investigación y, en última instancia, acelerar los avances científicos.  

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "한국어" %}}

### 단체소개서

선도적인 콘퍼런스 콘텐츠 출판사로서 Morressier는 본래 잘 알려지지 않았던 오프라인 학술 콘퍼런스에서 공유되는 개요, 포스터, 프레젠테이션 및 데이터를 조명하여 오프라인 학술 콘퍼런스 세계에 유용한 통찰을 제공합니다. Morressier는 전 세계 청중에게 콘퍼런스 콘텐츠를 알려서 이 행사에 직접 참석할 수 없는 사람들을 포함한 과학계 전체에 귀중한 지식에 대한 접근을 대중화합니다. 7년 동안 200개 넘는 세계 최고의 학술 학회, 협회, 기관 및 단체와의 협업 경험을 통해 Morressier는 Crossref 이사회에 이전에 뛰어들지 않았던 콘퍼런스 콘텐츠 세계에 대한 고유한 통찰력을 제공할 수 있습니다.  

Morressier는 학술 콘텐츠의 유용성을 증진하기 위해 이 정보를 체계화하고 정통성을 더욱더 높이고자 하는 목표의 일환으로 콘퍼런스 콘텐츠의 표준화를 주도합니다. Morressier 이미 콘퍼런스 개진에 관한 모든 것을 DOI에 할당하고, 각 문서에서 메타데이터를 추출하고, ORCID iD 로그인 옵션을 제공하고, 문서 검색, 인용, 링크, 평가 및 재사용을 쉽게 할 수 있도록 서치 엔진에 힘을 주고 있습니다. 이 중요한 단계는 학술 콘텐츠를 맥락에 놓고 학술적 소통을 개진함으로써 범위를 넓혀 잘 구축된 저널 논문 형식 외의 콘텐츠를 포함하려는 Crossref의 사명과 완벽하게 일치합니다. Morressier 팀은 다음 단계로 관련 결과를 연결하여 협업과 콘퍼런스 연구 발견을 더욱더 지원하는 동시에 이 연구 발견을 전체 연구 라이프사이클에 통합하기 위해 노력하고 있습니다.   


### 자기소개서

저는 Morressier의 데이터 책임자로서 데이터 및 플랫폼상의 검색 서비스를 관리하여 모든 연구 자료가 인용할 수 있고 검색이 쉽도록 하는 일을 담당합니다. 또한 연구 자료에 대한 데이터 및 인사이트와 콘퍼런스 트렌드를 제공하는 저희의 분석 서비스를 감독하여 저자가 그들의 작업이 끼치는 영향을 이해하고 콘퍼런스 주최자가 그들이 주최한 행사에서 중요한 과학 주제를 발견하는 데 도움을 줍니다. Crossref의 펀딩 레지스트리, 콘텐츠 등록, 그리고 Morressier 플랫폼용 참조 연계와의 협업 경험을 바탕으로 이러한 서비스에 대해 잘 알고 있습니다.  

학술적 소통은 시간이 지남에 따라 끊임없이 진화하는 근본적 시스템, 형식 및 작업 흐름과 함께 역사적으로 느리게 바뀌어 왔습니다. 그러나 지난 18개월 동안의 사건과 더 투명하고 접근 가능하며 종합적인 연구 생태계에 대한 명확한 수요를 고려할 때, 우리는 변곡점에 있으면서 더 빠른 변화를 앞에 둔 것일지도 모릅니다. 논문을 출판하기 훨씬 전에 초기 단계의 연구 결과를 공유하고 초기 실험에 대한 담론을 장려하는 것은 연구 진보를 지원하는 데 중요할 수 있습니다. Crossref 커뮤니티와 학술적 생태계 전반에 대한 저의 비전은 능력을 촉진하여 연구의 첫 번째 콘퍼런스 제출부터 논문으로 출판하기까지의 모든 라이프사이클 내내 연구를 추적할 수 있도록 하는 것입니다. 그 과정에서의 모든 단계는 연결되어야 하고 데이터 세트와 부정적 결과를 포함한 새로운 연구 형식을 표준화하여 연구 진행의 효율성을 높이고, 궁극적으로 과학 발전을 촉진해야 합니다.  

{{% /accordion %}}
{{% /accordion-section %}}

***

<a id="nisc"></a>
## Mike Schramm, NISC, South Africa<br>

<img src="/images/board-governance/mike-schramm.jpg" alt="Mike Schramm, NISC" width="225px" class="img-responsive" />  

{{% accordion %}}
{{% accordion-section "in English" %}}

### Organization statement
NISC (Pty) Ltd is the publisher of a range of premier African research products that promote African scholarship to a global audience. We publish high-quality research on behalf of institutions, societies, and associations in Africa. The business started with building, licensing and publishing bibliographic databases, targeting research institutions around the world. Database development remains a key activity at NISC, but we now publish academic journals on behalf of local scholarly societies and research institutes and have many of Africa's leading peer-reviewed academic journals in our stable. NISC also publishes scholarly books, primarily monographs.  

In the past we were able to keep pace with the big publishers with respect to the developments in production standards and online hosting – indeed NISC has been an early adopter of new technologies gaining us multiple Top 100 Technology Company awards in South Africa. However, in common with other small publishers we faced challenges in competing with bigger enterprises able to offer large and diverse title bundles in country or consortium deals. As a remedy, we have secured international publishing partnerships, working with companies who share our ethos of shining a light on the scholarship of Africa. From our base in a small university town in the Eastern Cape province of South Africa, these partnerships have put the best of African research into the hands of scholars across the world.  

Our engagement with Crossref, from its early days, was in pursuit of best publishing practice and particularly improving discoverability. Alongside our co-publishing partners, we have continued to include these tools and services in all our publishing workflow. We have a good understanding of the challenges that the small regional scholarly publishing enterprise faces in raising visibility and discoverability of the work of their authors and believe that we are well positioned to represent this sector.   


### Personal statement <br>

As Managing Director of NISC, I provide the strategic direction for the company. My academic background is in the biological sciences with wide-ranging published research interests. Over the past 25 years, I have held management positions in both the not-for-profit and commercial publishing sectors in South Africa, moving to NISC in 2008 where I am also a shareholder.   

Our own experience at NISC and the day-to-day engagement with journal editors and the societies and institutions whose titles we publish have given me a good understanding of the challenges faced by the small, regional publisher. These challenges go beyond the financial and relate to impediments many journals experience in implementing best practices that will get the work of their authors noticed and cited. A consequence of these difficulties is that too much important, good quality research published on the African continent does not get the exposure it deserves. I would like to see an improved understanding amongst the often overlooked regional, small publishers regarding the ways Crossref tools and services can help to shine a light on hitherto poorly exposed scholarship.  

I have a particular passion for working with early career scholars and to this end have involved NISC in a number of externally funded programmes involving mentor-driven article writing courses, collaborations with new journal editors and publishing of monographs of young humanities scholars. These initiatives have engaged participants from across the African-continent and given opportunities for new voices to be heard.  

I am a non-executive director of African Journals Online (AJOL), a widely respected non-profit organisation that shares many of the goals of NISC to improve access to the scholarship of Africa, by users both within the continent and to the North.  

Lester Isaacs, NISC sales and marketing manager, would be our alternate in the event of my unavailability.  


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Français" %}}

### Déclaration de l’organisation

NISC (Pty) Ltd est l’éditeur d’une gamme de produits de recherche africains de premier plan qui font la promotion de la recherche africaine auprès d’un public mondial. Nous publions des recherches de haute qualité pour le compte d’institutions, de sociétés et d’associations en Afrique. L’entreprise a commencé par construire, concéder des licences et publier des bases de données bibliographiques en ciblant les institutions de recherche du monde entier. Le développement de bases de données reste une activité clé du NISC, mais nous publions maintenant des revues académiques pour le compte de sociétés universitaires et d’instituts de recherche locaux et nous comptons dans nos rangs bon nombre des principales revues académiques africaines évaluées par les pairs. Le NISC publie également des ouvrages universitaires, principalement des monographies.  

Par le passé, nous pouvions suivre le rythme des grands éditeurs en ce qui concerne l’évolution des normes de production et de l’hébergement en ligne ; en effet, le NISC a été l’un des premiers à adopter de nouvelles technologies, ce qui nous a valu plusieurs prix Top 100 Technology Company en Afrique du Sud. Cependant, comme d’autres petits éditeurs, nous avons eu du mal à rivaliser avec les grandes entreprises capables d’offrir des ensembles de titres importants et diversifiés dans le cadre d’accords de pays ou de consortiums. Pour remédier à ce problème, nous avons établi des partenariats internationaux en matière d’édition, en travaillant avec des entreprises qui partagent notre philosophie de faire la lumière sur l’érudition de l’Afrique. Depuis notre base, située dans une petite ville universitaire de la province du Cap-Oriental en Afrique du Sud, ces partenariats ont mis le meilleur de la recherche africaine entre les mains d’universitaires du monde entier.  

Notre engagement auprès de Crossref, depuis ses débuts, a toujours eu pour but la poursuite des meilleures pratiques d’édition et en particulier l’amélioration de la découvrabilité. En plus de nos partenaires de coédition, nous avons continué à inclure ces outils et services dans tous nos flux de travail de publication. Nous comprenons bien les défis auxquels toute petite entreprise régionale d’édition universitaire est confrontée pour accroître la visibilité et la découvrabilité des travaux de ses auteurs et nous pensons être particulièrement bien placés pour représenter ce secteur.   


### Déclaration personnelle

En tant que directeur général de NISC, je fournis l’orientation stratégique de l’entreprise. Dans le cadre de mon parcours académique, j’ai eu l’occasion de me pencher sur divers projets de recherches et de publier de nombreux articles. Au cours des 25 dernières années, j’ai occupé des postes de direction dans les secteurs de l’édition commerciale et sans but lucratif en Afrique du Sud, avant d’intégrer NISC, dont je suis également actionnaire, en 2008.   

Notre propre expérience au NISC et notre relation quotidienne avec les éditeurs de revues et les sociétés et institutions dont nous publions les titres m’ont permis de bien comprendre les défis auxquels sont confrontés les petits éditeurs régionaux. Ces défis dépassent le cadre financier et sont liés aux obstacles rencontrés par de nombreuses revues dans la mise en œuvre de pratiques exemplaires qui feront remarquer et citer le travail de leurs auteurs. Une conséquence de ces difficultés est que trop de recherches importantes et de bonne qualité publiées sur le continent africain n’obtiennent pas l’exposition qu’elles méritent. J’aimerais que les petits éditeurs régionaux, souvent négligés, comprennent mieux comment les outils et les services de Crossref peuvent contribuer à mettre en lumière les bourses d’études jusqu’à présent peu exposées.   

J’ai une passion particulière pour la collaboration avec les chercheurs en début de carrière et, à cette fin, j’ai impliqué le NISC dans un certain nombre de programmes financés de l’extérieur, y compris des formations sur la rédaction d’articles animées par des mentors, des collaborations avec de nouveaux éditeurs de revues et la publication de monographies de jeunes chercheurs en sciences humaines. Ces initiatives ont mobilisé des participants de tout le continent africain et donné l’occasion à de nouvelles voix de se faire entendre.  

Je suis directeur non exécutif de l’African Journals Online (AJOL), une organisation à but non lucratif très respectée qui partage bon nombre des objectifs du NISC visant à améliorer l’accès des utilisateurs à l’érudition de l’Afrique, aussi bien sur le continent que plus au nord.  

Lester Isaacs, directeur des ventes et du marketing du NISC, serait notre remplaçant en cas d’indisponibilité.  


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "em Português" %}}

### Declaração da organização

A NISC (Pty) Ltd é a editora de uma série de produtos de pesquisa africanos de primeira linha que promovem bolsas de estudo africanas para um público global. Publicamos pesquisas de alta qualidade em nome de instituições, sociedades e associações na África. A empresa começou com a construção, licenciamento e publicação de bases de dados bibliográficos, visando instituições de pesquisa em todo o mundo. O desenvolvimento de banco de dados continua sendo uma atividade-chave da NISC, mas agora publicamos periódicos acadêmicos em nome de sociedades acadêmicas e institutos de pesquisa locais e temos em nosso portfólio muitos dos principais periódicos acadêmicos revisados​por pares da África. A NISC também publica livros acadêmicos, principalmente monografias.  

No passado, pudemos acompanhar as grandes editoras no que diz respeito aos desenvolvimentos em padrões de produção e hospedagem on-line, na verdade, a NISC foi uma das primeiras a adotar novas tecnologias, ganhando vários prêmios Top 100 Technology Company na África do Sul. No entanto, como acontece com outras editoras pequenas, enfrentamos desafios ao competir com empresas maiores, capazes de oferecer pacotes de títulos grandes e diversificados em acordos nacionais ou consórcio. Como solução, garantimos parcerias internacionais de publicação, trabalhando com empresas que compartilham nosso ethos de lançar uma luz sobre as bolsas de estudos da África. A partir da nossa base, em uma pequena cidade universitária na província de Cabo Oriental, na África do Sul, essas parcerias colocaram o melhor da pesquisa africana nas mãos de acadêmicos de todo o mundo.   

Nosso envolvimento com a Crossref, desde seus primórdios, foi em busca das melhores práticas de publicação e, particularmente, de melhorar a capacidade de descoberta. Juntamente com nossos parceiros de copublicação, continuamos incluindo essas ferramentas e serviços em todo o nosso fluxo de trabalho de publicação. Temos um bom entendimento dos desafios que uma pequena empresa regional de publicações acadêmicas enfrenta para aumentar a visibilidade e a capacidade de descoberta do trabalho de seus autores e acreditamos que estamos bem posicionados para representar esse setor.   

### Declaração pessoal

Como Diretor Executivo da NISC, forneço a direção estratégica para a empresa. Minha formação acadêmica é em Ciências Biológicas, com amplos interesses de pesquisa publicados. Nos últimos 25 anos, ocupei cargos de gestão nos setores de publicação comercial e sem fins lucrativos na África do Sul, juntando-me à NISC em 2008, da qual também sou acionista.   

Nossa própria experiência na NISC e o envolvimento diário com editores de periódicos, sociedades e instituições cujos títulos publicamos me deram um bom entendimento dos desafios enfrentados por uma pequena editora regional. Esses desafios vão além do financeiro e estão relacionados aos impedimentos que muitos periódicos enfrentam na implementação das melhores práticas que farão com que o trabalho dos seus autores seja notado e citado. Uma consequência dessas dificuldades é que muitas pesquisas importantes e de boa qualidade publicadas no continente africano não têm a exposição que merecem. Eu gostaria de ver um melhor entendimento entre os pequenos editores regionais frequentemente esquecidos sobre as maneiras como as ferramentas e serviços da Crossref podem ajudar a trazer à luz estudos até então mal expostos.   

Tenho uma paixão particular por trabalhar com acadêmicos em início de carreira e, para tanto, engajei a NISC em uma série de programas financiados externamente envolvendo cursos de redação de artigos orientados por mentores, colaborações com novos editores de periódicos e publicação de monografias de jovens acadêmicos da área de Humanas. Essas iniciativas envolveram participantes de todo o continente africano e criaram oportunidades para que novas vozes fossem ouvidas.  

Sou um diretor não executivo da African Journals Online (AJOL), uma organização sem fins lucrativos amplamente respeitada que compartilha muitos dos objetivos da NISC em melhorar o acesso a bolsas de estudos da África por usuários tanto dentro do continente, como no Norte.  

Lester Isaacs, gerente de vendas e marketing da NISC, seria nosso suplente no caso de minha indisponibilidade.  

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Español" %}}

### Declaración de la organización


NISC (Pty) Ltd es la editorial de diversos productos de investigación africanos de primer nivel que fomentan la actividad académica africana para un público de todo el mundo. Publicamos investigaciones de gran calidad en nombre de instituciones, agrupaciones y asociaciones de África. La actividad comenzó con la creación, concesión de licencias y publicación de bases de datos bibliográficas dirigidas a instituciones de investigación de todo el mundo. El desarrollo de la base de datos sigue siendo una tarea principal para NISC, pero, ahora, publicamos revistas académicas en nombre de agrupaciones académicas e institutos de investigación locales y contamos con muchas de las revistas académicas arbitradas más importantes de África. NISC también publica libros académicos, principalmente, estudios monográficos.  

Antes, éramos capaces de seguir el ritmo de las grandes editoriales en lo que respecta a los desarrollos en las normas de producción y el alojamiento en línea. De hecho, NISC fue una de las primeras que adoptó las nuevas tecnologías, lo que nos ha valido varios premios a las 100 mejores empresas tecnológicas de Sudáfrica. Sin embargo, al igual que otras editoriales pequeñas, nos enfrentamos a desafíos al competir con grandes empresas capaces de ofrecer grandes y diversos paquetes de títulos en los acuerdos con países o consorcios. Para remediarlo, hemos establecido acuerdos editoriales internacionales y trabajamos con empresas que comparten nuestra filosofía de arrojar luz sobre la actividad académica de África. Desde nuestra sede ubicada una pequeña ciudad universitaria de la provincia Cabo Oriental de Sudáfrica, estos acuerdos han puesto lo mejor de la investigación africana en las manos de académicos de todo el mundo.  

Nuestra implicación con Crossref, desde el principio, se basaba en la búsqueda de la mejor práctica editorial y, en particular, en la mejora de la capacidad de encontrar contenido. Junto a nuestros socios de coedición, hemos seguido incluyendo estas herramientas y servicios en todo nuestro flujo de trabajo de edición. Conocemos bien los retos a los que se enfrentan las pequeñas empresas de publicación académica a la hora de aumentar la visibilidad y la capacidad de encontrar el trabajo de sus autores y consideramos que estamos en una buena situación para representar a este sector.   

### Declaración personal

Como director general de NISC, mi misión es la dirección estratégica a la empresa. Mi formación académica se centra en la biología y cuento con un amplio abanico de intereses de investigación publicados. Durante los últimos 25 años, he ocupado puestos de gestión en el sector de la edición comercial y sin ánimo de lucro en Sudáfrica y empecé en NISC, de la que también soy accionista, en 2008.   

Nuestra propia experiencia en NISC y la continua colaboración con editores de revistas y las agrupaciones e instituciones cuyos títulos publicamos me han aportado una buena noción de los desafíos a los que se enfrenta una pequeña editorial regional. Estas dificultades van más allá de los aspectos económicos y tienen que ver con los impedimentos que tienen muchas revistas cuando se trata de poner en marcha las mejores prácticas para que el trabajo de sus autores se encuentre y se cite. Una consecuencia de estas dificultades es que gran parte de la investigación de buena calidad y de gran relevancia que se publica en el continente africano no recibe la visibilidad que se merece. Me gustaría observar un mejor conocimiento de los pequeños editores regionales, a menudo ignorados, con respecto a las formas en las que las herramientas y los servicios de Crossref pueden ayudar a poner el foco en esta actividad académica que, hasta el momento, no goza de la suficiente visibilidad.   

Especialmente, me apasiona trabajar con académicos que están iniciando su carrera y, para ello, he involucrado a NISC en una serie de programas de financiación externa que incluyen cursos de redacción de artículos dirigidos por mentores, colaboraciones con nuevos editores de revistas y la publicación de estudios monográficos de jóvenes académicos de humanidades. Estas iniciativas han involucrado a participantes de todo el continente africano y han dado la oportunidad de escuchar nuevas voces.  

Soy director no ejecutivo de African Journals Online (AJOL), una prestigiosa organización sin ánimo de lucro que comparte muchos de los objetivos de NISC para mejorar el acceso a la actividad académica en África por parte de usuarios, tanto del continente como del norte.  

Lester Isaacs, responsable de Ventas y Marketing de NISC me sustituirá en el caso de que yo no esté disponible.  

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "한국어" %}}

### 단체소개서

NISC(현지 법인) 유한회사는 아프리카의 다양한 최고의 지식 정보를 출판하는 회사이고, 이 지식 정보는 아프리카의 학문을 전 세계 청중에게 홍보합니다. 저희는 아프리카의 기관, 학회 및 협회를 위해 고품질 연구를 출판합니다. 이 사업은 도서 목록 데이터베이스를 구축하고, 라이선스를 발급하고, 출판하면서 시작했고, 그 대상은 전 세계의 연구 기관이었습니다. 데이터베이스 개발은 여전히 NISC의 주요 활동이지만, 저희는 현재 현지 학술 학회 및 연구 기관을 위해 학술 저널을 출판하고 있고 아프리카를 선도하는 다량의 동료 검토 학술 저널을 보유하고 있습니다. 또한 NISC는 학술 서적과 그중에서도 주로 모노그래프를 출판합니다.  

과거에 저희는 생산 표준 개발 및 온라인 호스팅에 있어서 대형 출판사에 뒤처지지 않고 따라갈 수 있었고, 오히려 저희는 신기술을 초기에 수용해오며 남아프리카의 기술 회사 톱 100을 여러 번 수상했습니다. 그러나 다른 소규모 출판사와 마찬가지로 국가나 협회 단위의 거래에서 규모가 크고 다양한 출판물을 제공할 수 있는 대기업과 경쟁하면서 어려움에 직면했습니다. 저희는 해결책으로 국제 출판 파트너십을 맺어서 아프리카 학문에 불빛이 되고자 하는 저희의 정신을 공유하는 회사와 협력했습니다. 저희의 기반인 남아프리카 이스턴 케이프 지방의 작은 대학 마을에서 이러한 파트너십을 통해 전 세계 학자들이 아프리카 최고의 연구를 접할 수 있도록 했습니다.  

저희는 최고의 출판 운영 방법을 추구하고 특히 검색 용이성을 개선하기 위해 Crossref의 초기 시절부터 함께했습니다. 공동 출판 파트너와 함께 계속해서 이러한 툴과 서비스를 저희의 모든 출판 작업 흐름에 포함했습니다. 저희는 소규모 지역 학술 출판사가 저자의 작업물에 대한 가시성과 검색 용이성을 높이는 데 겪는 어려움을 잘 이해하고 있으며 이 부문을 대변하기에 좋은 위치에 있다고 생각합니다.   

### 자기소개서

저는 NISC의 상무 이사로서 회사를 위한 전략적 방향성을 제공합니다. 제 학문적 배경은 생명과학에 있으며, 넓은 범위를 다루는 출판에 관심이 있습니다. 지난 25년 동안 남아프리카의 비영리 및 상업 출판 부문 모두에서 경영직을 맡아왔고, 2008년에 NISC로 자리를 옮겨 주주를 겸하고 있습니다.   

NISC에서의 경험과 저희가 내는 출판물의 저널 편집자, 협회 및 기관과 매일 협력하며 저는 소규모 지역 출판사가 직면하는 어려움을 잘 이해할 수 있었습니다. 이 어려움은 재정 문제를 넘어 많은 저널을 출판할 때 저자의 작업물이 주목받고 인용되도록 수행하는 모범 경영에서 겪는 장애물과 관련이 있습니다. 이러한 어려움의 결과는 아프리카 대륙에서 출판된 너무 중요한 양질의 연구는 그 중요성과 품질에 맞는 만큼 노출되지 않는다는 것입니다. 저는 Crossref 툴과 서비스가 지금까지 잘 알려지지 않은 학문이 빛을 볼 수 있도록 도움을 줄 방법과 관련하여 종종 간과되는 지역 단위의 소규모 출판사에 대한 이해가 향상되길 바랍니다.   

저는 특히 초기 경력 단계의 학자들과 협업하는 데 열정을 가지고 있으며, NISC는 초기 경력 단계의 학자들을 위해 멘토 중심의 논문 작성 코스, 새로운 저널 편집자들과의 협업 및 젊은 인문 학자들의 모노그래프 출판 등 외부에서 자금 지원을 받는 다수의 프로그램에 참여했습니다. 이러한 이니셔티브는 아프리카 대륙 전반에 걸쳐 참가자를 참여시켰고 새로운 목소리를 들을 수 있는 기회를 주었습니다.  

저는 아프리카 학술지 온라인(African Journals Online, AJOL)의 비상임 이사입니다. AJOL은 널리 존경받는 비영리 단체로, 아프리카와 북쪽에서 AJOL과 NISC를 모두 사용하는 사람들을 통해 아프리카 학문에 대한 접근을 개선하고자 하는 NISC의 목표 중 많은 부분을 공유합니다.  

제가 이 직을 맡게 될 수 없으면 대체자는 NISC의 영업 및 마케팅 담당자인 Lester Isaacs가 될 것입니다.  

{{% /accordion %}}
{{% /accordion-section %}}

## Tier 2, Large members (electing two seats)

<a id="aip"></a>
## Penelope Lewis, AIP Publishing, USA<br>
<img src="/images/board-governance/penelope-lewis.JPG" alt="Penelope Lewis, AIP Publishing" width="225px" class="img-responsive" />  

{{% accordion %}}
{{% accordion-section "in English" %}}
### Organization statement

AIP Publishing’s mission is to advance, promote, and serve the physical sciences for the benefit of humanity by breaking barriers to open, equitable research communication and empowering researchers to accelerate global progress. We are a subsidiary of the American Institute of Physics and support the mission and activities of AIP through publication of scholarly communication in physical science. We are a mid-size not-for-profit publisher, advancing our mission through 33 self-published and partner journals.    

AIP Publishing has been an active supporter and Board member of Crossref for many years. We support its aim to make scholarly communications better through our participation in industry organizations that advance meaningful initiatives through partnership. AIP Publishing is an active member and holds leadership roles in scholarly organizations including CHORUS, STM, and the Society Publishers’ Coalition. Partnership and collaboration in scholarly publishing is vital, particularly for publishers representing smaller-to-mid size organizations, not-for-profits, and societies. AIP Publishing would continue to bring this perspective to the Crossref Board and ensure diverse representation to enable the community-linking solutions that Crossref provides.    


### Personal statement <br>

I have represented AIP Publishing on Crossref’s Board since February 2021, when I joined AIP Publishing as Chief Publishing Officer.  I am responsible for the strategic growth of AIP Publishing’s diverse portfolio of journals, books, and other products aimed at advancing the global physical science community.     

Crossref provides an invaluable service to publishers, service providers, funders, researchers, and the broader scholarly research community. As the scientific research landscape becomes more open and prolific, the role that Crossref plays in providing infrastructure that connects, preserves, and registers this research output becomes increasingly important. The diversity in research output formats and the ecosystem itself pose an interesting challenge for Crossref and its members. I would be honored to continue to serve a full term on Crossref’s Board and to dedicate my energy to providing input into these challenges.    

I have worked in scholarly publishing for 15 years, having held senior-level roles at the American Chemical Society Publications Division as Chief Scientist, Strategic Planning & Analysis and as its Editorial Director prior to joining AIP Publishing. In addition to the Crossref Board and its Audit Committee, I am a member of the Society Publishers’ Coalition and have served on the AAP/PSP Executive Council and on STM and SSP committees. Aside from my experience in scholarly publishing, I have a deep appreciation of the scientific research landscape, receiving a Ph.D. in Physical Chemistry from The Pennsylvania State University and performing postdoctoral research at Columbia University, where I studied nanotechnology and surface science.    


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Français" %}}
### Déclaration de l’organisation

La mission d’AIP Publishing est de faire progresser, de promouvoir et de servir les sciences physiques au profit de l’humanité en brisant les obstacles à une communication ouverte et équitable en matière de recherche et en donnant aux chercheurs les moyens d’accélérer les progrès mondiaux. Nous sommes une filiale de l’American Institute of Physics et nous appuyons la mission et les activités de l’AIP en publiant des communications universitaires dans le domaine des sciences physiques. Nous sommes un éditeur à but non lucratif de taille moyenne, faisant progresser notre mission à travers 33 revues auto-publiées et partenaires.  

Depuis de nombreuses années, AIP Publishing soutient activement Crossref et est membre de son conseil d’administration. Nous appuyons son objectif visant à améliorer les communications universitaires grâce à notre participation à des organisations sectorielles qui font progresser des initiatives significatives par le biais de partenariats. AIP Publishing est un membre actif et joue divers rôles de leadership au sein d’organisations universitaires, y compris CHORUS, STM et la Society Publishers’ Coalition. Le partenariat et la collaboration en matière d’édition universitaire sont essentiels, en particulier pour les éditeurs représentant des organisations de petite et moyenne taille, des organismes sans but lucratif et des sociétés. AIP Publishing continuerait d’apporter cette perspective au conseil d’administration de Crossref et d’assurer une représentation diversifiée pour assurer la pérennité des solutions de liaison avec la communauté fournies par Crossref.  

### Déclaration personnelle

Je représente AIP Publishing au sein du conseil d’administration de Crossref depuis février 2021, date à laquelle j’ai rejoint AIP Publishing en tant que directrice de l’édition.  Je suis responsable de la croissance stratégique du portefeuille diversifié de revues, de livres et d’autres produits d’AIP Publishing visant à faire progresser la communauté mondiale des sciences physiques.    

Crossref offre un service inestimable aux éditeurs, aux prestataires de services, aux financeurs, aux chercheurs et au milieu de la recherche universitaire en général. À mesure que le paysage de la recherche scientifique devient plus ouvert et plus prolifique, le rôle que joue Crossref dans la fourniture d’infrastructures qui connectent, préservent et enregistrent ces résultats de recherche gagne en importance. La diversité des formats de sortie de la recherche et l’écosystème en lui-même représentent un défi intéressant pour Crossref et ses membres. Je serais honorée de continuer à siéger au conseil d’administration de Crossref pour un mandat complet et de consacrer mon énergie à apporter ma contribution à ces défis.    

Je travaille dans l’édition universitaire depuis 15 ans, et, avant de rejoindre AIP Publishing, j’ai occupé des postes de haut niveau à la division des publications de l’American Chemical Society en tant qu’experte scientifique en chef pour la planification stratégique et l’analyse, ainsi qu’en tant que directrice éditoriale. Outre le comité d’administration de Crossref et son comité d’audit, je suis membre de la Society Publishers’ Coalition et j’ai siégé au conseil exécutif de l’AAP/PSP et aux comités de la STM et de la SSP. Outre mon expérience dans l’édition universitaire, j’ai une profonde appréciation du paysage de la recherche scientifique. Je suis titulaire d’un doctorat en chimie physique de la Pennsylvania State University et j’ai effectué des recherches postdoctorales à l’Université de Columbia, où j’ai étudié la nanotechnologie et la science des surfaces.    


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "em Português" %}}

### Declaração da organização

A missão da AIP Publishing é desenvolver, promover e trabalhar para as ciências físicas para o benefício da humanidade, rompendo barreiras para a comunicação de pesquisa aberta e equitativa, e capacitando pesquisadores para acelerar o progresso global. Somos uma subsidiária do Instituto Americano de Física (AIP) e apoiamos sua missão e atividades por meio da publicação de comunicações acadêmicas em ciências físicas. Somos uma editora de médio porte sem fins lucrativos, que promove nossa missão com 33 periódicos autopublicados e de parceiros.    

A AIP Publishing tem sido apoiadora ativa e membro do Conselho da Crossref por muitos anos. Apoiamos seu objetivo de melhorar as comunicações acadêmicas, participando em organizações do setor que promovem iniciativas significativas por meio de parcerias. A AIP Publishing é um membro ativo e ocupa funções de liderança em organizações acadêmicas, incluindo CHORUS, STM e Society Publishers’ Coalition. Parceria e colaboração na publicação acadêmica são vitais, especialmente para editores que representam organizações de pequeno a médio porte, sem fins lucrativos e sociedades. A AIP Publishing continuaria trazendo essa perspectiva para o Conselho da Crossref e garantindo uma representação diversificada para possibilitar as soluções de vínculo com a comunidade que a Crossref oferece.    

### Declaração pessoal

Eu represento a AIP Publishing no Conselho da Crossref desde fevereiro de 2021, quando entrei para a AIP Publishing como Diretora de Publicação.  Sou responsável pelo crescimento estratégico do diversificado portfólio de periódicos, livros e outros produtos da AIP Publishing, que visam o avanço da comunidade de ciências físicas global.   

A Crossref fornece um serviço inestimável para editores, provedores de serviços, financiadores, pesquisadores e para a comunidade de pesquisa acadêmica mais ampla. À medida que o panorama da pesquisa científica se torna mais aberto e prolífico, o papel que a Crossref desempenha no fornecimento de infraestrutura que conecta, preserva e registra essa produção de pesquisa torna-se cada vez mais importante. A diversidade nos formatos de produção de pesquisa e o próprio ecossistema representam um desafio interessante para a Crossref e seus membros. Eu ficaria honrada em continuar meu trabalho por um mandato completo no Conselho da Crossref e em dedicar minha energia para contribuir com esses desafios.  

Trabalhei em publicações acadêmicas por 15 anos, tendo ocupado cargos de nível sênior na American Chemical Society Publications Division como Cientista Chefe, Planejamento Estratégico e Análise, além de ter sido sua Diretora Editorial antes de ingressar na AIP Publishing. Além do Conselho da Crossref e do Comitê de Auditoria, sou membro da Society Publishers’ Coalition, atuei no Conselho Executivo da AAP/PSP e nos comitês da STM e SSP. Além de minha experiência em publicações acadêmicas, tenho um profundo apreço pelo panorama da pesquisa científica, recebendo um Ph.D. em Físico-Química pela Universidade Estadual da Pensilvânia e realizando pesquisa de pós-doutorado na Universidade Columbia, onde estudei Nanotecnologia e Ciências de Superfície.  


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Español" %}}
### Declaración de la organización


La misión de AIP Publishing es promover, impulsar y servir a las ciencias físicas en favor de la humanidad rompiendo barreras para una comunicación de la investigación abierta, equitativa y que empodere a los investigadores para acelerar el progreso global. Somos una filial del American Institute of Physics y apoyamos la misión y las actividades de AIP a través de la publicación de comunicación académica en el campo de las ciencias físicas. Somos una editorial sin ánimo de lucro de tamaño mediano y fomentamos nuestra misión a través de 33 revistas con socios y autopublicadas.    

Desde hace años, AIP Publishing apoya activamente a Crossref y es miembro de la Junta. Respaldamos su objetivo de mejorar las comunicaciones académicas mediante nuestra participación en organizaciones del sector que fomenten iniciativas significativas a través de la asociación. AIP Publishing es miembro activo y ejerce liderazgo en organizaciones académicas como CHORUS, STM, y la Society Publishers’ Coalition. La asociación y la colaboración en el sector editorial académico es vital, sobre todo, para los editores que representan a pequeñas y medianas organizaciones, organizaciones sin ánimo de lucro y agrupaciones. AIP Publishing seguiría aportando su perspectiva a la Junta de Crossref y garantizaría una representación diversa para permitir las soluciones de vinculación de la comunidad que ofrece Crossref.   

### Declaración personal

Represento a AIP Publishing en la Junta de Crossref desde febrero de 2021, cuando me uní a AIP Publishing como editora jefa.  Soy responsable del crecimiento estratégico de la amplia cartera de revistas, libros y otros productos de AIP Publishing destinada a fomentar la comunidad internacional de ciencias físicas.   

Crossref ofrece un servicio inestimable a los editores, proveedores de servicios, patrocinadores, investigadores y la comunidad de investigación académica en su conjunto. A medida que el panorama de investigación científica prolifera y se va abriendo, el papel que juega Crossref en la provisión de una infraestructura que conecta, preserva y registra sus resultados de investigación es cada vez más importante. La diversidad de los formatos de resultados de investigación y su propio ecosistema suponen un interesante desafío para Crossref y sus miembros. Sería un honor seguir en la Junta de Crossref durante un mandato completo y dedicar mi energía a contribuir en estos retos.   

Trabajo en el sector editorial académico desde hace 15 años y he ocupado puestos de categoría superior en la American Chemical Society Publications Division como jefa de Planificación y Análisis Científico y Estratégico y directora editorial antes de unirme a AIP Publishing. Además de la Junta de Crossref y su comité de auditoría, soy miembro de la Society Publishers’ Coalition y he formado parte del consejo ejecutivo de AAP/PSP y en los comités de STM y SSP. Al margen de mi experiencia en el sector editorial académico, tengo un profundo conocimiento del panorama de investigación científica. Soy doctora en Química Física por la Universidad Estatal de Pensilvania y estoy desarrollando una investigación posdoctoral en la Universidad de Columbia, donde estudié nanotecnología y ciencia de superficies.  

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "한국어" %}}

### 단체소개서

AIP 출판사의 사명은 개방적이고 공정한 연구 장벽을 허물고 연구자가 국제적 진전을 촉진할 수 있도록 힘을 실어줌으로써 인류를 위해 물리과학을 진전 및 활성화하고 이에 기여하는 것입니다. 저희는 미국 물리학회(American Institute of Physics)의 자회사로 자연과학의 학술적 소통을 출판물로 발간하여 AIP의 사명과 활동을 지원합니다. 중간 규모의 비영리 출판사로서 33개의 자비 출판 및 제휴 저널을 통해 저희의 사명을 추진합니다.    

AIP 출판사는 수년 동안 Crossref의 이사회 회원으로서 활발히 지원해왔습니다. 저희는 파트너십을 통해 의미 있는 이니셔티브를 추진하는 업계 단체에 참여함으로써 학술적 소통을 개선하고자 하는 Crossref의 목표를 지원합니다. AIP 출판사는 CHORUS, STM 및 출판사 연합 협회를 포함한 학술적 조직에서 활동 중이고 지도적 역할을 맡고 있습니다. 학술적 소통에서 파트너십 및 협업은 중요하며, 특히 중소 규모 단체, 비영리 단체 및 협회를 대표하는 출판사들에 필수적입니다. AIP 출판사는 계속해서 Crossref 이사회에 이러한 시각을 가져오고 Crossref가 다양한 목소리를 대변하여 커뮤니티를 잇는 솔루션을 제공하도록 합니다.   

### 자기소개서
저는 2021년 2월부터 Croosref 이사회에서 AIP 출판사를 대표했고 AIP 출판사에 수석 출판 책임자로 합류했습니다.  저는 저널, 서적 및 기타 상품으로 이루어진 AIP 출판사의 다양한 포트폴리오를 전략적으로 키우는 일을 담당하고 있고, 이 포트폴리오는 전 세계 자연 과학계의 발전을 목표로 합니다.     

Crossref는 출판사, 서비스 제공자, 기금, 연구자 및 더욱 넓은 학술 연구 커뮤니티에 매우 귀중한 서비스를 제공합니다. 과학 연구 전망이 더 개방적이고 활발해지면서, Crossref가 이러한 연구 결과를 연결하고, 관리하고, 등록하는 인프라를 제공하는 데 수행하던 역할의 중요성이 점점 더 커지고 있습니다. 연구 결과 형식의 다양성과 이것의 생태계는 Crossref와 소속 회원에게 흥미로운 과제를 던집니다. 제가 Crossref 이사회의 임기를 계속해서 다 채우고 이 과제에 인풋을 제공하는 데 전념할 수 있게 된다면 영광일 것입니다.    

저는 학술 출판 분야에서 15년 동안 일하며 AIP 출판사에 합류하기 전에 미국 화학회(American Chemical Society, ACS) 출판부에서 수석 과학자, 전략 기획 및 분석 책임자, 편집 책임자로서 고위직을 맡아 근무했습니다. Crossref 이사회 및 이사회의 감사위원회 외에 저는 학회 출판사 연합(the Society Publsihers’ Coalition)의 회원이고 AAP/PSP 집행위원회, STM 및 SSP 위원회에서 활동했습니다. 학술 출판 경험 외에 저는 과학 연구 전망에 대해 깊은 이해를 하고, 펜실베이니아 주립대학교(The Pennsylvania State University)에서 물리 화학 박사 학위를 받았으며, 컬럼비아 대학교(Columbia University)에서 나노 기술과 표면 과학을 공부하고 박사 후 연구를 수행했습니다.    


{{% /accordion %}}
{{% /accordion-section %}}

---


<a id="apa"></a>
## Jasper Simons, American Psychological Association (APA), USA<br>
<img src="/images/board-governance/jasper-simons.png" alt="Jasper Simons, American Psychological Association (APA)" width="200px" class="img-responsive" />  

{{% accordion %}}
{{% accordion-section "in English" %}}
### Organization statement

The American Psychological Association (APA) is the leading scientific and professional organization representing psychology in the United States, with more than 122,000 members. It is APA’s mission to advance the creation, communication, and application of psychological knowledge to benefit society and improve people's lives.  

It is essential that the Crossref Board has representation from a trusted publisher in the social and behavioral sciences. The needs of social and behavioral scientists must be represented. With APA on the Crossref Board, we can ensure that Crossref considers the needs of this thriving community of scholars.  

For decades, the APA has been a trusted voice in the field of psychology and a leader in developing research solutions for scholars across the globe. APA publishes ~100 scholarly journals. APA’s flagship discovery platform PsycInfo® has fostered the digital exchange of high-quality peer-reviewed research since 1967. PsycInfo has grown to become the most trusted and comprehensive library of psychological science in the world.   

APA values close collaborations with content experts and industry partners to improve research outcomes. APA has been a member of Crossref and on the Crossref Board since its early days. Together with fellow publishers, exceptional Crossref staff and industry partners, APA has helped to structure, process, and share metadata to reveal relationships between research outputs across the world. APA is ambitious about the future and excited about the role Crossref can play to make research outputs easy to find, cite, link, and assess.   

### Personal statement <br>
As Chief Publishing Officer at the American Psychological Association (APA), Jasper oversees a vast portfolio of psychological-related publishing products including journals, books, library databases and digital learning solutions. He is responsible for driving strategy, establishing editorial policies, producing content, developing products and overseeing the related sales and marketing services. Jasper has more than 20 years of experience in scholarly publishing, working at leading publishing organizations such as Elsevier, SAGE Publications and Thomson Reuters. At APA, Jasper manages a team of almost 200 staff members. In recent years, he and his team have developed innovative new product lines that will ensure the future relevance of the association.   

In 2017, Jasper lead the collaboration with the Center for Open Science to support Open Science and Reproducibility in psychology. The collaboration seeks to advance the integration between the content in the Open Science Framework and the peer-reviewed content of the APA. The APA Journals program launched the new Open Access Platform, APA Open, at the end of 2019 with the new journal Technology, Mind, and Behavior. And in 2019, Jasper represented APA in an industry coalition that worked directly with the White House Office of Science and Technology Policy on the issue of open access as it relates to federally funded research.  

Jasper is a current member of the Crossref Board of Directors. He served as chairman of the Crossref Board during the 2020/2021 term, and he is the current chair of the Crossref Audit Committee. He also serves as a member of the Board of Directors for the Association of American Publishers (AAP). He was the keynote speaker at STM’s Society Day in 2020.  

(Alternate) Tony Habash, Chief Information Officer  

Tony F. Habash, DSc, is the chief business integration officer and the chief information officer of the APA. Tony oversees digital services and user experience across APA, seeks to integrate the operational processes of APA through process enhancements, oversees APA data initiatives and business intelligence and drives innovation in products and services to serve APA’s mission. As CIO, he is responsible for APA's information technology strategy and operations, including all core business systems development, Web platform and strategy, APA publishing systems, and solutions and technology enablement across APA.   

Before joining APA in 2007, Tony spent 15 years with AARP as its director of information technology strategy and planning. He was responsible for directing IT strategy and implementation across the organization.  


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Français" %}}
### Déclaration de l’organisation


L’American Psychological Association (APA) est la principale organisation scientifique et professionnelle représentant la psychologie aux États-Unis. Elle compte plus de 122 000 membres. La mission de l’APA est de faire progresser la création, la communication et l’application des connaissances psychologiques au profit de la société et d’améliorer la vie des gens.  

Il est essentiel que le conseil d’administration de Crossref soit représenté par un éditeur de confiance dans les sciences sociales et comportementales. Les besoins des spécialistes des sciences sociales et du comportement doivent être représentés. Avec l’APA au sein du conseil d’administration de Crossref, nous pouvons faire en sorte que Crossref tienne compte des besoins de cette communauté de chercheurs florissante.  

Depuis des décennies, l’APA est une voix de confiance dans le domaine de la psychologie et un chef de file dans le développement de solutions de recherche pour les chercheurs du monde entier. L’APA publie environ 100 revues universitaires. La plateforme de découverte phare d’APA, PsycInfo®, favorise depuis 1967 l’échange numérique de recherches de haute qualité évaluées par les pairs. PsycInfo est devenue la bibliothèque de sciences psychologiques la plus fiable et la plus complète au monde.   

L’APA accorde beaucoup d’importance aux collaborations étroites avec les experts en contenu et les partenaires de l’industrie pour améliorer les résultats de la recherche. L’APA est membre de Crossref et du conseil d’administration depuis ses premiers jours. Associé à d’autres éditeurs, au personnel exceptionnel de Crossref et à des partenaires du secteur, l’APA a contribué à structurer, traiter et partager des métadonnées pour révéler les liens entre les résultats de recherche dans le monde entier. L’APA fait preuve d’ambition quant au futur et est ravi du rôle que Crossref peut jouer pour rendre les résultats de recherche plus facile à trouver, à citer, à lier et à évaluer.   


### Déclaration personnelle

En tant que directeur de la publication pour l’American Psychological Association (APA), Jasper supervise un vaste portefeuille de produits d’édition liés à la psychologie, notamment des revues, des livres, des bases de données de bibliothèques et des solutions d’apprentissage numérique. Il est responsable de l’application de la stratégie, de l’établissement des politiques éditoriales, de la production de contenu, du développement des produits et de la supervision des ventes connexes et des services marketing. Jasper compte plus de 20 ans d’expérience dans le domaine de l’édition universitaire et il a collaboré avec des entreprises d’éditions de pointe comme Elsevier, SAGE Publications et Thomson Reuters. Au sein de l’APA, Jasper gère une équipe de près de 200 employés. Ces dernières années, il a développé avec son équipe de nouvelles gammes de produits innovants qui assureront la pertinence future de l’association.   

En 2017, Jasper a dirigé la collaboration avec le Center for Open Science pour soutenir la science ouverte et la reproductibilité en psychologie. Cette collaboration cherche à faire avancer l’intégration entre le contenu de l’Open Science Framework et le contenu évalué par les pairs de l’APA. À la fin de l’année 2019, le programme APA Journals a lancé la nouvelle plateforme de libre accès, APA Open, avec la nouvelle revue Technology, Mind, and Behavior. En 2019, Jasper a représenté l’APA au sein d’une coalition industrielle qui a travaillé directement avec le Bureau des politiques scientifiques et technologiques de la Maison Blanche sur la question du libre accès au niveau de la recherche financée par le gouvernement fédéral.   

Jasper est actuellement membre du conseil d’administration de Crossref. Il a été président du conseil d’administration de Crossref en 2020/2021, et il est actuellement président du comité d’audit de l’organisation. Il est également membre du conseil d’administration de l’Association of American Publishers (AAP). Il a été le conférencier d’honneur de la Journée des sociétés de la STM en 2020.    

(Remplaçant) Tony Habash, Directeur de l’information  

Tony F. Habash, DSc, est directeur de l’intégration des affaires et directeur de l’information de l’APA. Tony supervise les services numériques et l’expérience utilisateur dans l’ensemble de l’APA, cherche à intégrer les processus opérationnels de l’APA grâce à des améliorations des processus, supervise les initiatives de l’APA en matière de données et de veille stratégique et encourage l’innovation au niveau des produits et services afin de remplir la mission de l’APA. En tant que directeur de l’information, il est responsable de la stratégie et des opérations informatiques d’APA, y compris le développement de tous les systèmes d’affaires de base, de la plateforme et de la stratégie Web, des systèmes de publication d’APA et de l’activation des solutions et technologies à l’échelle de l’APA.   

Avant de se joindre à l’APA en 2007, Tony a passé 15 ans au sein de l’AARP en tant que directeur de la stratégie et de la planification des technologies de l’information. Il était responsable de la direction de la stratégie informatique et de sa mise en œuvre dans l’ensemble de l’organisation.  


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "em Português" %}}
### Declaração da organização


American Psychological Association (APA) é a principal organização científica e profissional representando a Psicologia nos Estados Unidos, com mais de 122.000 membros. É missão da APA avançar na criação, comunicação e aplicação de conhecimento psicológico para beneficiar a sociedade e melhorar a vida das pessoas.  

Éssencial que o Conselho da Crossref tenha representação de um editor confiável nas ciências sociais e comportamentais. As necessidades dos cientistas sociais e comportamentais devem ser representadas. Com a APA no Conselho da Crossref, poderemos garantir que a Crossref considere as necessidades desta próspera comunidade de acadêmicos.

Por décadas, a APA tem sido uma voz confiável no ramo da Psicologia e a líder no desenvolvimento de soluções de pesquisa para acadêmicos em todo o mundo. A APA publica cerca de 100 periódicos acadêmicos. Desde 1967, a PsycInfo®, principal plataforma de descoberta da APA, promove a troca digital de pesquisas revisadas por pares de alta qualidade. A PsycInfo cresceu e se tornou a biblioteca de ciência psicológica mais confiável e abrangente do mundo.   

A APA valoriza colaborações próximas com especialistas em conteúdo e parceiros da indústria para melhorar os resultados da pesquisa. A APA é membro da Crossref e do Conselho da Crossref desde seus primórdios. Junto com outras editoras, da excepcional equipe da Crossref e de parceiros do setor, a APA ajudou a estruturar, processar e compartilhar metadados para revelar as relações entre os resultados da pesquisa em todo o mundo.  A APA é ambiciosa em relação ao futuro e está motivada com o papel que a Crossref pode desempenhar para fazer com que os resultados de pesquisas sejam fáceis de serem encontrados, citados, vinculados e acessados.   


### Declaração pessoal

Como Diretor de Publicações da American Psychological Association (APA), Jasper supervisiona um vasto portfólio de produtos de publicação relacionados à Psicologia, incluindo periódicos, livros, bancos de dados de bibliotecas e soluções de aprendizagem digital. Ele é responsável por conduzir a estratégia, estabelecer políticas editoriais, produzir conteúdo, desenvolver produtos e supervisionar as vendas e os serviços de marketing relacionados. Jasper tem mais de 20 anos de experiência em publicação acadêmica, trabalhando em organizações de publicação líderes, tais como Elsevier, SAGE Publications e Thomson Reuters. Na APA, Jasper gerencia uma equipe de quase 200 funcionários. Nos últimos anos, ele e sua equipe desenvolveram novas linhas de produtos inovadores que garantirão a relevância futura da associação.   

Em 2017, Jasper liderou a colaboração com o Center for Open Science para apoiar a ciência aberta e a reprodutibilidade em Psicologia. A colaboração visa promover a integração entre o conteúdo no Open Science Framework e o conteúdo revisado por pares da APA. No final de 2019, o programa APA Journals lançou a nova plataforma de acesso aberto, a APA Open, com a nova revista Technology, Mind, and Behavior. E, em 2019, Jasper representou a APA em uma coalizão do setor que trabalhou diretamente com o Escritório de Política de Ciência e Tecnologia da Casa Branca na questão do acesso aberto, no que se refere à pesquisa financiada pelo governo federal.
Jasper é um atual membro do Conselho Diretor da Crossref. Ele atuou como presidente do Conselho da Crossref durante o mandato de 2020/2021 e é o atual presidente do Comitê de Auditoria da Crossref. Ele também atua como membro do Conselho Diretor da Association of American Publishers (AAP). Ele foi o orador principal no Dia da Sociedade do STM, em 2020.  
(Suplente) Tony Habash, Diretor de TI

Tony F. Habash, DSc, é o diretor de integração de negócios e o diretor de TI da APA. Tony supervisiona os serviços digitais e a experiência do usuário na APA, ele busca integrar os processos operacionais da APA por meio de melhorias de processo, supervisiona as iniciativas de dados e a inteligência comercial da APA, e impulsiona a inovação em produtos e serviços para atender à missão da APA. Como Diretor de TI, ele é responsável pela estratégia e pelas operações de Tecnologia da Informação da APA, incluindo todo o desenvolvimento dos sistemas comerciais essenciais, plataforma e estratégia Web, sistemas de publicação da APA e soluções e capacitação em tecnologia na APA.

Antes de ingressar na APA, em 2007, Tony foi diretor de estratégia e planejamento de Tecnologia da Informação da AARP por 15 anos. Ele foi responsável por direcionar a estratégia e implementação de TI em toda a organização.

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Español" %}}
### Declaración de la organización

La American Psychological Association (APA) es la principal organización profesional y científica que representa al sector de la psicología en Estados Unidos. Tiene más de 122 000 miembros. La misión de la APA es avanzar en la creación, comunicación y aplicación del conocimiento psicológico en favor de la sociedad y mejorar las vidas de las personas.
Es esencial que la Junta de Crossref incluya la representación de una editorial de confianza en las ciencias sociales y del comportamiento. Las necesidades de los científicos sociales y del comportamiento deben estar representadas. Con la APA en la Junta de Crossref, podemos asegurarnos de que Crossref tenga en cuenta las necesidades de esta comunidad en pleno crecimiento.  

Durante décadas, la APA ha sido una voz fiable en el campo de la psicología y líder en el desarrollo de soluciones de investigación para académicos de todo el planeta. La APA publica unas 100 revistas académicas. La plataforma insignia de búsqueda de referencias PsycInfo® fomenta el intercambio digital de investigación arbitrada de alta calidad desde 1967. PsycInfo se ha convertido en la biblioteca más fiable y exhaustiva de la ciencia de la psicología en el mundo.   

La APA valora las estrechas colaboraciones con expertos en contenido y socios del sector para mejorar los resultados de la investigación. La APA ha sido miembro de Crossref y de la Junta de Crossref desde sus orígenes. Junto con otras editoriales, el extraordinario personal de Crossref y otros socios del sector, la APA ha ayudado a estructurar, procesar y compartir metadatos para identificar relaciones entre los resultados de investigaciones de todo el mundo. La APA muestra ambición por el futuro y entusiasmo por el papel que puede desempeñar Crossref en lo relativo a simplificar la búsqueda, la citación, la vinculación y la evaluación de los resultados de las investigaciones.   

### Declaración personal

Como editor jefe de la American Psychological Association (APA), Jasper supervisa una amplia cartera de productos editoriales del campo de la psicología, entre los que se incluyen revistas, libros, bases de datos bibliográficas y soluciones de aprendizaje digital. Es responsable de impulsar estrategias, crear políticas editoriales, producir contenido, desarrollar productos y supervisar los servicios de ventas y marketing asociados. Jasper cuenta con más de 20 años de experiencia en el mundo de la edición académica y ha trabajado en importantes organizaciones editoriales, como Elsevier, SAGE Publications y Thomson Reuters. En la APA, Jasper dirige un equipo de casi 200 empleados. En los últimos años, él y su equipo han desarrollado nuevas e innovadoras líneas de productos que garantizarán la importancia de la asociación en el futuro.   

En 2017, Jasper dirigió la colaboración con el Center for Open Science para apoyar la ciencia abierta y la capacidad de reproducción en el ámbito de la psicología. La colaboración pretende agilizar la integración entre el contenido del marco para la ciencia abierta y el contenido de revistas arbitradas de la APA. El programa de revistas de la APA puso en marcha la nueva plataforma de acceso abierto, APA Open, a finales de 2019 con la nueva revista Technology, Mind, and Behavior. Además, en 2019, Jasper representó a la APA en una coalición del sector que trabajaba directamente con la Oficina de la Casa Blanca para Políticas de Ciencia y Tecnología sobre la cuestión del acceso abierto, puesto que está vinculada a la investigación financiada por el gobierno federal.
Actualmente, Jasper es miembro de la Junta Directiva de Crossref. Ha sido presidente de la Junta de Crossref durante el período comprendido entre 2020 y 2021 y es el actual presidente del comité de auditoría de Crossref. También es miembro de la Junta Directiva de la Association of American Publishers (AAP). Fue el ponente principal del Society Day de STM en 2020.  

(Sustituto) Tony Habash, director de Sistemas de Información  

Tony F. Habash, doctor en Ciencias, es director de Integración empresarial y director de Sistemas de Información de la APA. Tony supervisa servicios digitales y experiencia de usuario en la APA, se ocupa de integrar los procesos operativos de la APA mediante mejoras en los procesos, supervisa las iniciativas de datos y la inteligencia empresarial de la APA e impulsa la innovación en sus productos y servicios para contribuir a la misión de la APA. Como director de Sistemas de Información, es responsable de la estrategia y las operaciones de tecnologías de la información de la APA, incluido el desarrollo de todos los sistemas de las actividades principales, plataforma web y estrategia, sistemas editoriales de la APA y soluciones y tecnología.   

Antes de unirse a la APA en 2007, Tony trabajó 15 años en AARP como director de la estrategia y planificación de la tecnología de la información. Era responsable de dirigir la estrategia de TI y su implementación en toda la organización.  


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "한국어" %}}
### 단체소개서

미국 심리 학회(APA)는 선도적인 과학적, 전문적 기관으로 미국의 심리학을 대표하며, 120,000명 이상의 회원이 있습니다. APA의 사명은 사회에 이익을 주고 사람들의 삶을 개선하기 위해 심리 지식의 창작, 의사소통 및 적용을 진전시키는 것입니다.
Crossref 이사회는 사회과학 및 행동과학 분야에서 신뢰받는 출판사를 대표해야 합니다. 사회과학 및 행동 과학자들의 필요에 대한 목소리는 대변되어야 합니다. Crossref 이사회가 APA와 함께한다면 우리는 Crossref가 이 번영하고 있는 학자 커뮤니티의 수요를 고려하게 할 수 있습니다.  

수십 년간 APA가 내는 목소리는 심리학 분야에서 신뢰를 받아왔고 전 세계 학자를 위해 연구 솔루션을 개발하는 일을 선도해왔습니다. AOA 출판사~100개의 학술지 APA의 플래그십 발견 플랫폼인 PsycInfo®은 1967년부터 고품질의 동료 검토 연구를 온라인에서 교환하는 일을 발전시켜왔습니다. PsycInfo는 성장하여 세계에서 가장 신뢰받고 광범위한 심리과학 도서관이 되었습니다.   

APA는 연구 결과를 개선하기 위해 콘텐츠 전문가 및 업계 파트너와의 긴밀한 협업을 중시합니다. APA는 Crossref의 회원이며 초기부터 Crossref 이사회 회원이었습니다. APA는 동료 출판사, 특출한 Crossref 직원 및 업계 파트너와 함께 전 세계 연구 결과물 간의 관계를 밝히기 위해 메타데이터를 구조화, 처리 및 공유하는 데 도움을 주었습니다. APA는 미래에 대해 야망을 품고 있으며 Crossref가 연구 결과물을 찾고, 인용하고, 링크하고, 평가하는 일을 쉽게 하는 역할을 할 수 있다는 사실에 아주 만족하고 있습니다.   


### 자기소개서

미국 심리 학회(APA)의 수석 출판 책임자인 Jasper는 정기 간행물, 책, 도서관 데이터베이스 및 디지털 러닝 솔루션을 포함한 심리학 관련 출판 상품의 방대한 포트폴리오를 감독합니다. Jasper는 전략 추진, 편집 정책 수립, 콘텐츠 제작, 제품 개발과 관련 판매 및 마케팅 서비스의 관리 감독을 총괄하고 있습니다. Jasper는 Elsevier, SAGE Publications 및 Thomson Reuters 등의 선도적인 출판 기관에서 근무하면서 20년 이상 학술 출판 경험을 쌓았습니다. 그는 APA에서 200여 명의 직원으로 구성된 팀을 관리하고 있습니다. 최근 몇 년 동안 Jasper와 그의 팀은 협회의 향후 관련성을 확보할 혁신적인 새로운 제품 라인을 개발했습니다.   

2017년에는 심리학에서 열린 과학과 재현성을 지원하기 위해 Center for Open Science와의 협업을 주도했습니다. 이 협업을 통해 Open Science Framework의 콘텐츠와 APA의 동료 검토 콘텐츠 간의 통합을 향상하려 하고 있습니다. APA 저널 프로그램은 2019년 말에 새로운 오픈 액세스 플랫폼인 APA Open을 새로운 저널인 ‘Technology, Mind, and Behavior’와 함께 출시하였습니다. 그리고 2019년에 Jasper는 업계 연합에서 APA를 대표하였고, 이 업계 연합은 오픈 액세스가 연방정부의 자금 지원을 받는 연구와 관련이 있기 때문에 이 문제에 대해 백악관 과학기술정책실과 직접 협업하였습니다.
그는 현재 Crossref 이사회에 속해 있습니다. 2020/2021 임기 동안 Crossref 이사회의 의장이었고, 현재 Crossref 감사위원회(Audit Committee) 의장입니다. 또한 미국출판사협회(Association of American Publishers, AAP)의 회원입니다. Jasper는 2020년 STM의 Society Day에서 키노트 연설을 했습니다.  

(대체 이사)Tony Habash, 최고 정보 책임자  

DSc의 Tony F. Habash는 최고 비즈니스 통합 책임자이자 APA의 최고 정보 책임자입니다. Tony는 APA 전반에 걸친 디지털 서비스와 사용자 경험을 감독하고, 프로세스 개선을 통해 APA의 운영 프로세스를 통합하고, APA 데이터 이니셔티브 및 기업 정보를 감독하고, APA의 사명을 위해 상품 및 서비스 혁신을 추진합니다. 그는 CIO로서 모든 핵심 사업 시스템 개발, 웹 플랫폼 및 전략, APA 출판 시스템, APA 전반에 걸친 솔루션 및 기술 활성화를 포함한 APA의 정보 기술 전략과 운영을 담당하고 있습니다.  

그는 2007년 APA 합류 이전에 AARP에서 15년 동안 정보 기술 전략 및 계획 담당자로 있었습니다. 그는 조직 전반의 IT 전략 및 실행 감독을 담당했습니다.   

{{% /accordion %}}
{{% /accordion-section %}}

---

<a id="acm"></a>
## Scott Delman, Association for Computing Machinery (ACM), USA<br>

<img src="/images/board-governance/scott-delman.jpeg" alt="Scott Delman, Association for Computing Machinery (ACM)" width="150px" class="img-responsive" />

{{% accordion %}}
{{% accordion-section "in English" %}}

### Organization statement

ACM is the world’s largest Society dedicated to advancing the art, science, engineering, and application of Computing-related technologies by fostering the open exchange of information and by promoting the highest professional and ethical standards. ACM’s Core Values are Technical Excellence, Education & Technical Advancement, Ethical Computing and Technology for Positive Impact, and Diversity and Inclusion. These Core Values drive our long-term vision and our short and medium-term strategy as a Society and Publisher.  

ACM’s governance is composed entirely of Computing professionals, not career Publishing professionals or Financial Investment professionals, as is often the case with many of the large commercial Publishers. This has given and continues to give ACM a unique perspective and approach to many of the major challenges and disruptive changes happening in the scholarly communications industry, such as the wide-scale transition to Open Access, in that every decision we make is driven primarily by their short and long term impact on researchers, educators, students, and practitioners, and not by how our decisions will impact generating and growing revenue and profitability. As such, ACM’s Publications program operates only as a means to fulfilling our mission, and not as an end in itself, since ACM’s publications program funds a wide variety of educational and good works programs around the world, such as public policy, education, and diversity & inclusion initiatives.   

ACM is also one of the few remaining large societies and medium sized publishers that continues to manage all major aspects of our Publications program, and because of that we rely heavily on third party technology services like those offered by Crossref. For this reason and because ACM cares deeply about advancing science and education, ACM has supported and played an active leadership role in Crossref’s governance since Crossref’s founding in 1999, and we are committed to supporting and actively contributing to the organization’s sustainability and continued relevance, just as ACM has done with other non-profits over the past three decades, such as ORCID, Portico, arXiv, Plan S, and CHORUS.    

Crossref is at a critical moment in its history and now, perhaps more than any other time in the past, requires visionary, strategic, and experienced leadership on its Board of Directors to partner and help guide Crossref’s highly skilled and dedicated professional staff, who are constantly looking at the scholarly publishing landscape and developing new ways for the organization to remain relevant and play a leading role that supports not just the scholarly publishing community, but the broader scientific community, such as Similarity Check, ROR, and Funder IDs. In recent years, Crossref has been rapidly expanding its range of services to both the publishing community and research institutions and funders to drive growth for the organization. At the same time, key technologies, such as Machine Learning, have the potential to impact Crossref’s core DOI registration service, on which the organization relies so heavily.   

The decisions Crossref takes over the next few years, in terms of its own growth, new services offered, how it chooses to address new financial and operational challenges caused by the global Pandemic, how Crossref deploys and focuses staff and resources, and more generally how it responds to the changing technology landscape, will continue to have a transformational impact on the organization, and the scholarly publication landscape.


### Personal statement <br>


For nearly 30 years, I have served in a wide variety of roles in scholarly publishing, including editorial, strategy, licensing, open access evangelist, and senior management roles, including Kluwer Academic Publishers’ Vice President of Publishing, Springer’s Vice President of Business Development, and for the 14 years as a Director at ACM.     

As ACM’s Director of Publications, I am responsible for developing and implementing ACM’s overall publications strategy, overseeing editorial operations, managing ACM’s publication sales and licensing activities, and developing and implementing ACM’s Open Access strategy. Volunteering and helping to drive positive change in the industry has led me to become active as part of the governance structure for several other leading technology-driven non-profit organizations, including Crossref, arXiv, Portico, and CHORUS.  

For the past year, I have been honored to serve as Crossref’s Chair. During this time, I’ve primarily focused on supporting the organization’s leadership team through a period of extreme uncertainty, providing counsel and leadership through discussions with Ed and his team, as well as working closely with the organization’s Executive Committee and Board to ensure that Crossref has the resources it needs to continue providing valuable services to its membership and the scholarly community. Several of the initiatives we’ve focused on this past year are (1) to adopt and start implementing the Principles of Open Scholarly Infrastructure (POSI), which over the coming months and years will start to lead the organization down some new pathways in support of the industry’s transition to Open Access publication (2) the creation of a Crossref Investment Committee to oversee and steer the organization’s growing pool of financial resources in a way that is consistent with the organization’s mission-based, ethical, and non-profit status, and (3) to start establishing a set of new priorities for the organization, the most important one being that Crossref will start to play a more significant leadership role in developing and supporting technical infrastructure tools and solutions aimed at strengthening the community’s trust in the scholarly literature by focusing more on services like Similarity Check (i.e.- plagiarism detection), a possible new service identifying Conflicts of Interest with scholarly publications, and Research Organization Identifier (ROR). I’m proud of the work that’s been done to keep Crossref strong during the pandemic and start paving the way for the future, but there’s so much left to do and a year in this role simply isn’t enough, so I’m asking for your support to continue this good work. Thank you.  


{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Français" %}}

### Déclaration de l’organisation

ACM est la plus grande société au monde dédiée à l’avancement de l’art, de la science, de l’ingénierie et de l’application des technologies liées à l’informatique en favorisant l’échange ouvert d’informations et en promouvant les normes professionnelles et éthiques les plus élevées. Les valeurs fondamentales d’ACM sont l’excellence technique, l’éducation et le progrès technique, l’informatique éthique et la technologie pour un impact positif, ainsi que la diversité et l’inclusion. Ces valeurs fondamentales sous-tendent notre vision à long terme et notre stratégie à court et à moyen terme en tant que société et éditeur.  

La gouvernance d’ACM est entièrement composée de professionnels de l’informatique, et non de professionnels s’étant forgé une carrière dans l’édition ou de professionnels de l’investissement financier, comme c’est souvent le cas parmi de nombreux grands éditeurs commerciaux. Cela a conféré et continue d’offrir à ACM une perspective et une approche uniques à de nombreux défis majeurs et changements perturbateurs qui se produisent dans l’industrie des communications universitaires, comme la transition à grande échelle vers le libre accès, car chaque décision que nous prenons est principalement motivée par son impact à court et à long terme sur les chercheurs, les enseignants, les étudiants et les praticiens, et non par la façon dont nos décisions auront un impact sur la génération et la croissance des revenus et de la rentabilité. En tant que tel, le programme de publications d’ACM ne fonctionne que comme un vecteur nous permettant de remplir notre mission, et non comme une fin en soi, puisque le programme de publications d’ACM finance un large éventail de programmes éducatifs et de bonnes œuvres dans le monde, notamment dans le domaine des politiques publiques, de l’éducation et des initiatives de diversité et d’inclusion.   

ACM est également l’un des rares grands groupes et éditeurs de taille moyenne qui continuent de gérer tous les aspects majeurs de notre programme de publications, et pour cette raison, nous dépendons fortement des services technologiques tiers comme ceux proposés par Crossref. Pour cette raison et parce qu’ACM se soucie profondément de faire progresser la science et l’éducation, ACM a soutenu et joué un rôle de leadership actif dans la gouvernance de Crossref depuis sa fondation en 1999, et nous nous engageons à soutenir et à contribuer activement à la durabilité et à la pertinence continue de l’organisation, tout comme ACM l’a fait avec d’autres organisations à but non lucratif au cours des trois dernières décennies (comme ORCID, Portico, arXiv, Plan S et CHORUS).  

Crossref est à un moment critique de son histoire et nécessite maintenant, peut-être plus qu’à tout autre moment de son histoire, un leadership visionnaire, stratégique et expérimenté au sein de son conseil d’administration pour établir des partenariats et aider à guider le personnel professionnel hautement qualifié et dévoué de Crossref, qui examine constamment le paysage de l’édition universitaire et développe de nouvelles façons pour l’organisation de rester pertinente et de jouer un rôle de premier plan afin de soutenir non seulement la communauté de l’édition universitaire, mais aussi la communauté scientifique plus large, par le biais de contrôles de plagiat, d’un registre des organismes de recherche et de l’identification des financeurs. Ces dernières années, Crossref a rapidement élargi sa gamme de services auprès de la communauté de l’édition et des institutions de recherche, mais aussi des financeurs, en vue de stimuler la croissance de l’organisation. Dans le même temps, les technologies clés, comme l’apprentissage machine, ont un impact potentiel sur le service principal d’enregistrement de DOI de Crossref, dont l’organisation est particulièrement tributaire.   

Les décisions que Crossref prendra au cours des prochaines années, en termes de sa propre croissance, de nouveaux services offerts, de la façon dont elle choisit de relever les nouveaux défis financiers et opérationnels causés par la pandémie mondiale, de la manière dont Crossref déploie et concentre son personnel et ses ressources, et plus généralement de sa réponse à l’évolution du paysage technologique, continueront d’avoir un impact transformationnel sur l’organisation et le paysage des publications universitaires.   

### Déclaration personnelle

Pendant près de 30 ans, j’ai occupé un large éventail de rôles dans l’édition universitaire, y compris dans les domaines de l’édition, de la stratégie, des licences, de la promotion du libre accès, ainsi que des postes de direction, y compris ceux de vice-président de l’édition chez Kluwer Academic Publishers, de vice-président du développement des affaires pour Springer, et, depuis 14 ans déjà, en qualité de directeur d’ACM.   

En tant que directeur des publications d’ACM, je suis responsable de l’élaboration et de la mise en œuvre de la stratégie globale d’ACM en matière de publications, de la supervision des opérations éditoriales, de la gestion des activités de vente et de licence des publications d’ACM, ainsi que de l’élaboration et de la mise en œuvre de la stratégie de libre accès d’ACM. Le bénévolat et l’aide au changement positif dans l’industrie m’ont amené à devenir actif dans le cadre de la structure de gouvernance de plusieurs autres organisations à but non lucratif de premier plan axées sur la technologie, y compris Crossref, arXiv, Portico et CHORUS.  

Au cours de l’année écoulée, j’ai eu l’honneur d’assumer la présidence de Crossref. Au cours de cette période, je me suis principalement concentré sur le soutien de l’équipe de direction de l’organisation à travers une période d’incertitude extrême, en fournissant des conseils et un leadership par le biais de discussions avec Ed et son équipe, ainsi qu’en travaillant en étroite collaboration avec le comité exécutif et le conseil d’administration de l’organisation pour veiller à ce que Crossref dispose des ressources dont elle a besoin pour continuer à fournir des services précieux à ses membres et à la communauté universitaire. Voici plusieurs des initiatives sur lesquelles nous nous sommes concentrés au cours de la dernière année : (1) adopter et commencer à mettre en œuvre les principes de l’infrastructure scientifique ouverte (POSI), qui, au cours des prochains mois et des prochaines années, commenceront à guider l’organisation vers de nouvelles voies à l’appui de la transition de l’industrie vers la publication en libre accès ; (2) créer un comité d’investissement de Crossref pour superviser et diriger le bassin croissant de ressources financières de l’organisation d’une manière compatible avec la mission et le caractère éthique et sans but lucratif de l’organisation ; et (3) commencer à établir un ensemble de nouvelles priorités pour l’organisation, la plus importante étant que Crossref commencera à jouer un rôle de leadership plus important dans le développement et le soutien d’outils et de solutions d’infrastructure technique visant à renforcer la confiance de la communauté vis-à-vis de la littérature scientifique en se concentrant davantage sur des services tels que la vérification des similitudes (c.-à-d. la détection du plagiat), un nouveau service potentiel identifiant les conflits d’intérêts avec les publications universitaires, et l’identificateur des organismes de recherche (ROR). Je suis fier du travail effectué afin d’assurer la stabilité de Crossref pendant la pandémie et de commencer à préparer le terrain pour l’avenir, mais il reste tellement à faire et une année à ce poste ne suffit pas. Je vous demande donc votre soutien pour poursuivre cet excellent travail. Merci.  

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "em Português" %}}

### Declaração da organização

A ACM é a maior sociedade do mundo dedicada ao avanço da arte, ciência, engenharia e aplicação de tecnologias relacionadas à computação, incentivando a troca aberta de informações e promovendo os mais altos padrões profissionais e éticos. Os principais valores da ACM são Excelência Técnica, Educação e Avanço Técnico, Computação Ética, Tecnologia para Impacto Positivo e Diversidade e Inclusão. Esses valores essenciais orientam nossa visão de longo prazo e nossa estratégia de curto e médio prazo como sociedade e editora.  

A governança da ACM é composta inteiramente por profissionais de computação, profissionais de publicação ou profissionais de investimento financeiro que não são de carreira, como costuma ser o caso em muitas das grandes editoras comerciais. Isso deu e continua a dar à ACM uma perspectiva e abordagem únicas para muitos dos principais desafios e mudanças inovadoras que acontecem na indústria de comunicações acadêmicas, como a transição em larga escala para o Acesso Aberto, em que cada decisão que tomamos é impulsionada principalmente por seu impacto de curto e longo prazo sobre pesquisadores, educadores, alunos e profissionais, e não pelo impacto das nossas decisões na geração e no aumento de receita e lucratividade. Assim sendo, o programa de Publicações da ACM opera apenas como um meio de cumprir nossa missão, e não como um fim em si mesmo, uma vez que o programa de publicações da ACM financia uma ampla variedade de programas educacionais e de boas obras em todo o mundo, como políticas públicas, educação e iniciativas de diversidade e inclusão.   

A ACM também é uma das poucas grandes sociedades e editoras de médio porte remanescentes que continua a gerenciar todos os principais aspectos do nosso programa de publicações e, por isso, dependemos muito de serviços de tecnologia de terceiros, como os que são oferecidos pela Crossref. Por esta razão, e por se preocupar profundamente com o avanço da ciência e da educação, a ACM tem apoiado e desempenhado um papel de liderança ativa na governança da Crossref desde a sua fundação em 1999. Estamos comprometidos a apoiar e contribuir ativamente para a sustentabilidade e relevância contínua da organização. Justamente como a ACM tem feito com outras organizações sem fins lucrativos nas últimas três décadas, como ORCID, Portico, arXiv, Plan S e CHORUS.  

A Crossref está em um momento crítico de sua história e agora, talvez mais do que em qualquer outro momento no passado, exige liderança visionária, estratégica e experiente em seu Conselho Diretor para formar parceria e ajudar a orientar a equipe profissional altamente qualificada e dedicada da Crossref, que está constantemente olhando para o panorama da publicação científica e desenvolvendo novas maneiras para a organização manter-se relevante e desempenhar um papel de liderança, que apoia não apenas a comunidade editorial acadêmica, mas também a comunidade científica mais ampla, como a verificação de semelhança, ROR e IDs de Financiador. Nos últimos anos, a Crossref tem expandido rapidamente sua gama de serviços para a comunidade editorial, instituições de pesquisa e financiadores para impulsionar o crescimento da organização. Ao mesmo tempo, tecnologias-chave, como aprendizado de máquina, têm o potencial de impactar o serviço de registro DOI principal da Crossref, do qual a organização depende tanto.   

As decisões que a Crossref tomar nos próximos anos, em termos de seu próprio crescimento, novos serviços oferecidos, como ela escolhe enfrentar os novos desafios financeiros e operacionais causados pela pandemia global, como ela implanta e concentra pessoal e recursos e, de maneira mais geral, como ela responde às mudanças no cenário da tecnologia, continuarão a ter um impacto transformador na organização e no cenário das publicações acadêmicas.


### Declaração pessoal

Por quase 30 anos, desempenhei várias funções na publicação acadêmica, incluindo editorial, estratégia, licenciamento, propagação do acesso aberto, além de cargos de seniores de gestão, incluindo Vice-presidente de Publicação da Kluwer Academic Publishers, Vice-presidente de Desenvolvimento de Negócios da Springer, Diretor da ACM por 14 anos.   

Como Diretor de Publicações da ACM, sou responsável por desenvolver e implementar a estratégia geral de publicações da empresa, supervisionando as operações editoriais, gerenciando as vendas de publicações e atividades de licenciamento da ACM, e desenvolvendo e implementando a estratégia de Acesso Aberto da ACM. Ser voluntário e ajudar a impulsionar mudanças positivas no setor me levaram a ser ativo como parte da estrutura de governança de várias outras organizações sem fins lucrativos líderes de tecnologia, incluindo Crossref, arXiv, Portico e CHORUS.  

No ano passado, tive a honra de trabalhar como presidente da Crossref. Durante esse tempo, concentrei-me principalmente em apoiar a equipe de liderança da organização em um período de extrema incerteza, fornecendo conselho e liderança durante conversas com Ed e sua equipe, bem como trabalhando em estreita colaboração com o Comitê Executivo e o Conselho da organização para garantir que a Crossref tivesse os recursos de que precisa para continuar a fornecer serviços valiosos aos seus membros e à comunidade acadêmica. Muitas das iniciativas nas quais focamos no ano passado são: (1) adotar e começar a implementar os Princípios de Infraestrutura Escolar Aberta (POSI), que nos próximos meses e anos começarão a conduzir a organização por novos caminhos em apoio à transição do setor para publicação de Acesso Aberto, (2) a criação de um Comitê de Investimento da Crossref, para supervisionar e orientar o crescente conjunto de recursos financeiros da organização, de forma consistente com seu status, baseado na missão e na ética e sem fins lucrativos, e (3) começar a estabelecer um conjunto de novas prioridades para a organização, sendo a mais importante delas a Crossref começar a desempenhar um papel de liderança mais significativo no desenvolvimento e suporte de ferramentas e soluções de infraestrutura técnica destinadas a fortalecer a confiança da comunidade na literatura acadêmica, concentrando-se mais em serviços como a verificação de semelhança (ou seja, detecção de plágio), um possível novo serviço de identificação de conflitos de interesse com publicações acadêmicas, e Identificador de Organização de Pesquisa (ROR). Estou orgulhoso do trabalho que foi feito para manter a Crossref sólida durante a pandemia e começar a preparar o caminho para o futuro, mas ainda há muito a ser feito e um ano nesta função simplesmente não é suficiente, por isso, estou pedindo o seu apoio para continuar esse bom trabalho. Obrigado.  

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "en Español" %}}

### Declaración de la organización

ACM es la agrupación más grande del mundo dedicada al progreso del arte, la ciencia, la ingeniería y la aplicación de tecnologías relacionadas con la computación a través del fomento del intercambio abierto de información y la promoción de las más elevadas normas éticas y profesionales. Los valores fundamentales de ACM son la excelencia técnica, el progreso educativo y técnico, la computación ética y la tecnología dirigida a un impacto positivo, así como la diversidad y la inclusión. Estos valores fundamentales impulsan nuestra visión a largo plazo y nuestra estrategia a corto y medio plazo como agrupación y editorial.  

La gobernanza de ACM está compuesta exclusivamente por profesionales de la computación, no profesionales del sector editorial o de la inversión financiera, como suele ser el caso de muchas de las grandes editoriales comerciales. Esto le ha proporcionado a ACM una perspectiva y un enfoque únicos sobre cualquiera de los principales desafíos y cambios disruptivos que se están produciendo en el sector de las comunicaciones académicas, como la transición a gran escala al acceso abierto, en la que cada decisión que tomamos se rige por su impacto a corto y largo plazo en los investigadores, educadores, estudiantes y profesionales, y no por cómo nuestras decisiones afectarían a la generación y el aumento de los ingresos y la rentabilidad. Como tal, el programa de publicaciones de ACM desarrolla su actividad solo como un medio para lograr nuestra misión y no como un fin en sí mismo, ya que el programa de publicaciones de ACM financia un amplio abanico de programas educativos y de buenas obras en todo el mundo, tales como políticas públicas, educación e iniciativas de diversidad e inclusión.   

ACM también es una de las pocas agrupaciones grandes y de las editoriales de tamaño medio que quedan y siguen gestionando todos los principales aspectos de nuestro programa de publicaciones y, por ese motivo, nos valemos, en gran medida, de servicios como los que ofrece Crossref. Por esta razón, y porque ACM se interesa profundamente por el progreso de la ciencia y la educación, ACM ha apoyado y desempeñado un liderazgo activo en la gobernanza de Crossref desde su fundación en 1999, y nos comprometemos a respaldar y contribuir activamente a la sostenibilidad y continua relevancia de la organización, tal y como ha hecho ACM con otras organizaciones sin ánimo de lucro durante las últimas tres décadas, como ORCID, Portico, arXiv, Plan S, y CHORUS.  

Crossref se encuentra en un momento decisivo de su historia y, quizás ahora más que nunca, necesita un liderazgo visionario, estratégico y experimentado en su Junta Directiva para asociarse y ayudar a guiar al especializado y altamente cualificado equipo de profesionales de Crossref, que siempre tiene la mirada puesta en el panorama de las publicaciones académicas y desarrolla constantemente nuevas formas para que la organización preserve su importancia y desempeñe un papel que ayude, no solo a la comunidad de las publicaciones académicas, sino a la comunidad científica en su conjunto, como Similarity Check, ROR e identificadores de patrocinadores. En los últimos años, Crossref ha ampliado rápidamente su gama de servicios, tanto a la comunidad editorial como a las instituciones de investigación y los patrocinadores para impulsar el crecimiento de la organización. Al mismo tiempo, las tecnologías clave, como el aprendizaje automático, tienen el potencial de influir en el servicio principal de registro DOI de Crossref, del que depende en gran medida la organización.   

Las decisiones que tome Crossref en los próximos años en lo que respecta a su propio crecimiento, los nuevos servicios que ofrece, cómo decide abordar los nuevos retos financieros y operacionales que ha causado la pandemia global, cómo Crossref destina y concentra personal y recursos y, en términos más generales, cómo responde ante el cambiante panorama tecnológico, seguirán influyendo en la transformación de la organización y en el ámbito de las publicaciones académicas.

### Declaración personal


Durante casi 30 años, he desempeñado una gran variedad de funciones en el ámbito de las publicaciones académicas, entre las que se incluyen funciones editoriales, de estrategia, concesión de licencias, defensa del acceso abierto y de alta dirección, incluida la vicepresidencia de Publicaciones de Kluwer Academic Publishers, la vicepresidencia de Desarrollo de negocio de Springer y la dirección de ACM durante 14 años.   

Como Director de Publicaciones de ACM, soy responsable del desarrollo y la implementación de la estrategia general de publicaciones de ACM, superviso las operaciones editoriales, gestiono las ventas de publicaciones de ACM y me ocupo de la concesión de licencias a actividades y el desarrollo y puesta en marcha de la estrategia de acceso abierto de ACM. Trabajar como voluntario y ayudar a impulsar un cambio positivo en el sector me han convertido en una parte activa de la estructura de gobernanza de varias organizaciones sin ánimo de lucro dirigidas por la tecnología, entre las que se incluyen Crossref, arXiv, Portico y CHORUS.  

El pasado año, tuve el honor de asumir la presidencia de Crossref. Durante este tiempo, me he centrado, principalmente, en apoyar al equipo de liderazgo de la organización durante una etapa de máxima incertidumbre, he prestado asesoramiento y liderazgo a través de debates con Ed y su equipo y he trabajado estrechamente con el comité ejecutivo y la Junta de la organización para garantizar que Crossref tenga los recursos que necesita para seguir proporcionando servicios de valor a sus afiliados y a la comunidad académica. Varias de las iniciativas a las que hemos prestado especial atención durante el pasado año son: 1) adoptar y empezar a implantar los principios de la infraestructura académica abierta (POSI, por sus siglas en inglés), que a lo largo de los siguientes meses y años empezarán a llevar a la organización por nuevas vías para apoyar la transición del sector a la publicación de acceso abierto; 2) la creación de un comité de inversión de Crossref para supervisar y dirigir el creciente fondo de recursos financieros de forma coherente con la condición de la organización: sin ánimo de lucro, ética y basada en la misión; y 3) empezar a fijar una serie de nuevas prioridades para la organización, la de más peso, que Crossref empiece a desempeñar un liderazgo más importante en el desarrollo y el apoyo a herramientas e infraestructuras técnicas dirigidas a la reforzar la confianza de la comunidad en la literatura académica, todo ello prestando más atención a servicios como Similarity Check (es decir, detección del plagio), la posibilidad de un nuevo servicio que detecte conflictos de interés con las publicaciones académicas y un identificador de organizaciones de investigación (ROR, por sus siglas en inglés). Estoy orgulloso del trabajo que se ha hecho para que Crossref se mantuviera fuerte durante la pandemia y empezara a preparar el terreno para el futuro, pero todavía queda mucho por hacer y un año en este puesto no es suficiente, por lo que os pido vuestro apoyo para seguir con el buen trabajo.   Gracias.

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "한국어" %}}

### 단체소개서


ACM은 예술, 과학, 엔지니어링 및 컴퓨터 관련 기술 적용에 전념하는 세계에서 가장 큰 학회로, 정보의 개방적 교환을 조성하고 최고의 전문적, 윤리적 표준을 촉진합니다. ACM의 핵심 가치는 기술적 우수성, 교육 및 기술 진보, 긍정적 영향을 위한 윤리적 컴퓨터 사용 및 기술, 그리고 다양성 및 포용입니다. 이러한 핵심 가치는 학회 및 출판사로서 우리의 장기 비전과 중단기 전략을 추진합니다.

ACM의 거버넌스는 대부분의 대형 상업 출판사의 경우와 같이 직업 출판 전문가나 금융 투자 전문가가 아닌 컴퓨팅 전문가로 전부 구성됩니다. 이는 ACM에게 학술적 소통 업계에서 일어나는 많은 주요 과제 및 획기적 변화에 대해 고유한 시각과 접근을 계속해서 제시해 왔고, 그 예시로 오픈 액세스로의 광범위한 전환을 들 수 있습니다. 이 경우에 우리가 내리는 모든 결정은 주로 연구자, 교육자, 학생 및 실무자에 대한 그들의 장단기 영향에 의해 결정되며 우리의 결정이 이익 및 수익성을 창출하고 증가하는 데 영향을 어떻게 미치는지는 고려되지 않습니다. 이처럼 ACM의 출판 프로그램은 사명을 이행하기 위한 수단으로서만 운영되며, 프로그램 그 자체를 목적으로 하지 않습니다. ACM의 출판 프로그램은 공공 정책, 교육, 다양성 및 포용성 이니셔티브와 같이 전 세계의 다양하고 폭넓은 교육 및 자선 행위 프로그램에 자금을 지원하기 때문입니다.   

ACM은 출판 프로그램의 모든 주요 측면을 계속해서 관리하는 소수의 대규모 학회 및 중견 규모 출판사 중 하나이기도 합니다. 이로 인해 우리는 Crossref와 같은 제3자가 제공하는 기술 서비스에 크게 의존합니다. 이러한 이유와 과학 및 교육 발전에 대한 깊은 걱정 때문에 ACM은 1999년 Crossref이 창설된 이후로 Crossref의 거버넌스를 지원하고 지도적 역할을 활발히 수행했습니다. 그리고 우리는 ACM이 지난 30년 동안 기타 비영리 단체와 했던 것처럼 조직의 지속 가능성과 계속된 관련성을 지원하고 이것에 활발히 기여하도록 전념하고 있으며, 그러한 비영리 단체로는 ORCID, Portico, arXiv, Plan S 및 CHORUS 등이 있습니다.  

Crossref는 단체의 역사에 있어서 중요한 순간에 있고, 현재 이사회에는 아마 과거 그 어느 때보다도 확실한 비전을 가진 전략적이고 경험 있는 리더십이 필요하며, Crossref의 고도로 숙련된 헌신적인 전문 인력을 감독하는 데 파트너가 되고 도움을 주어야 합니다. 이 인력들은 학술 출판 전망을 끊임없이 살피고, 단체를 위한 새로운 방법을 개발하여 관련성을 유지하고 학술 출판 업계뿐만 아니라 유사도 검사(Similarity Check), ROR 및 기금 ID과 같이 더 넓은 범위에서 과학계를 지원하는 선도적인 역할을 수행하도록 합니다. 최근 몇 년 동안 Crosref는 출판 업계, 연구 기관 및 기금 모두로 서비스 범위를 빠르게 확장하여 단체의 성장을 이끌어 왔습니다. 동시에, 기계 학습(Machine Learning)과 같은 핵심 기술은 서비스는 Crossref가 크게 의존하고 있는 핵심 디지털 객체 식별(DOI) 등록 서비스에 영향을 줄 가능성이 있습니다.   

Crossref가 단체의 성장, 새롭게 제공하는 서비스, 글로벌 팬데믹으로 인한 새로운 재정 및 운영 과제를 해결하는 방법, 단체가 인력과 자원을 효율적으로 배치하고 집중하는 방법, 그리고 더 나아가 변화하는 기술 전망에 대응할 방법 면에서 향후 몇 년 동안 내릴 결정은 계속해서 조직과 학술 출판 업계 전망에 변혁적으로 영향을 미칠 것입니다.


### 자기소개서

저는 30여 년 동안 학술 출판 업계에서 편집, 전략, 라이선스 부여, 오픈 액세스 에반젤리스트 등 다양한 역할을 수행했으며, Kluwer Academic Publisher에서 출판 부사장, Springer에서 사업 개발 부사장, ACM에서 14년 동안 이사로 재직하며 고위 경영직을 맡았습니다.   

저는 ACM의 출판 책임자로서 전반적인 출판 전략 개발 및 실행, 편집 운영 감독, 출판 영업 및 라이선스 부여 활동 관리, 그리고 오픈 액세스 전략 개발 및 실행을 담당합니다. 자원 봉사와 업계에 긍정적 변화를 이끄는 도움을 주면서 기타 여러 선도적인 기술 주도 비영리 단체의 거버넌스 구조 일부로 활발히 활동하게 되었고, 이러한 비영리 단체로는 Crossref, arXiv, Portico, CHORUS가 있습니다.  

저는 지난 한 해 동안 영광스럽게도 Crossref의 의장직을 맡았습니다. 의장직을 지내는 동안 매우 불확실한 시기를 겪으며 조직의 지도부를 지원하고, Ed와 그의 팀을 자문과 리더십을 제공하고, 조직의 집행위원회(Executive Committee) 및 이사회와 긴밀히 협력하여 Crossref가 회원과 학계에 귀중한 서비스를 계속해서 제공하는 데 필요한 자원을 확보하는 일에 주력했습니다. 작년 한 해 동안 집중했던 몇몇 이니셔티브는 다음과 같습니다. (1) POSI(the Principles of Open Scholarly Infrastructure)를 채택하고 시행하기 시작하고, 향후 몇 달, 몇 년 동안 조직을 새로운 길로 이끌기 시작하여 업계가 오픈 액세스 출판으로 전환하도록 지원하고, (2) Crossref 투자위원회를 창설하여 조직의 사명을 기반으로 하는 윤리적이고 비영리적인 위상에 부합하는 방향으로 성장하고 있는 재정 자원 풀을 감독하고 이끌고, (3) 조직의 새로운 우선순위 설정 구축을 시작하기 위해 가장 중요한 것은 Crossref가 기술적 인프라 툴과 솔루션을 개발 및 지원하는 데 있어 보다 중요한 선도 역할을 하기 시작할 것이라는 점입니다. 이는 유사도 검사(예를 들어 표절 검사), 학술 출판에서의 이해 충돌을 식별할 수 있는 새로운 서비스, 그리고 ROR(Research Organization Identifier)을 통해 학술 문헌에서 커뮤니티의 신뢰를 강화하는 걸 목표로 합니다. 저는 팬데믹 기간 동안 Crossref를 견고히 지키고 미래를 위한 길을 마련하기 위해 수행한 일에 부심을 느낍니다. 하지만 아직 해야 할 일이 많이 남아있으며 이 역할을 수행하기에 1년은 충분하지 않기에 제가 이러한 좋은 일을 계속할 수 있도록 여러분의 지지를 부탁드립니다. 감사합니다.  


{{% /accordion %}}
{{% /accordion-section %}}
