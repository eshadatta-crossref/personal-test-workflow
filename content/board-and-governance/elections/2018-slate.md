+++
title = "Board election 2018 candidates"
date = "2018-08-15"
draft = false
author = "Lisa Hart Martin"
rank = 2
parent = "Elections"
weight = 6
+++

If you are a voting member of Crossref your ‘voting contact’ will receive an email in late September, please follow the instructions in that email which includes links to the relevant election process and policy information.

From 26 applications, our [Nominating Committee](/committees/nominating/) proposed the following seven candidates to fill the five seats open for election to the Crossref Board of Directors.

| Member organization | Candidate standing     | Country      |
|:--------------------|:-----------------------|:-------------|
|[African Journals Online](#AJOL)|Susan Murray |South Africa  |
|[American Psychological Association](#APA)|Jasper Simons|USA |
|[Association for Computing Machinery](#ACM)|Scott Delman |USA|
|[California Digital Library](#CDL)|Catherine Mitchell|USA    |
|[Hindawi](#HW)       |Paul Peters             |UK            |
|[SAGE](#SG)          |Richard Fidczuk         |USA           |
|[Wiley](#WL)         |Duncan Campbell         |USA           |
<br><br>

{{% imagewrap right %}} <img src="/images/blog/susan-murray.png" alt="Susan Murray, AJOL" width="250px" /> {{% /imagewrap %}}

<a id="AJOL"></a>
## Susan Murray, African Journals Online (AJOL), South Africa<br>
Unlike in Europe and North America with the giant commercial academic publishers there, journals publishing from developing countries are usually stand-alone journals run by subject-specific experts on a Non Profit basis. This is why hosting platforms like African Journals Online (AJOL) in Africa, other (INASP supported) country-level Journal Online (JOL) platforms around the world, and our “big sister” SciELO in Latin America have emerged… to provide technical support, quality improvements, and increased visibility of peer-reviewed Southern-published research content on a Non Profit basis.

Many journals don’t have the resources or the IT capacity to get online in isolation, and a small journal website is not very visible to search engines, so AJOL provides free highly visible aggregated hosting, software updates, high machine-readability of metadata, and various other free services like monthly usage statistics and free Digital Object Identifiers (DOIs), as a Sponsoring Member of Crossref. We were the first Non Profit Organisation to have successfully negotiated a discounted rate from Crossref for DOIs for journals publishing from Low and Lower Middle Income Countries, and we absorb the costs of membership and DOI assignation on behalf of our journal partners.

We are familiar with Crossref and have had a long-standing collaboration with your organisation. Additionally, AJOL supports the subject experts running the journals to become more aware of the quality norms and standards of robust research publishing, and assist them to implement these. Hence the development of the Journal Publishing Practices & Standards (JPPS) framework and assessment process. This work is important so that Southern research journals can be credible and trusted by authors and readers.

Based in South Africa, AJOL hosts 521 journals based in 32 African countries from all academic disciplines on its platform and over 150,000 full text articles available to download, more than half of which are free to read and download. There are 350 more journals who’ve applied for inclusion and are waiting to be assessed, but this has been put on hold while a new journal publishing practices framework and assessment has been developed by AJOL and INASP (www.journalquality.info). Once all current journal partners are assessed, we will start working through new applicants’ assessments again.

There is a lot of demand for AJOL’s services by journals who’ve heard from and observed the benefits of AJOL inclusion, and in the long term, we hope to include all qualifying peer-reviewed, African-published journals on the continent.

AJOL is a long-standing Non Profit Organisation (started in 1998 and run independently from South Africa since 2005). AJOL's strategy, implementation and finances are overseen by a Board drawn from AJOL's main stakeholder groups, we are annually audited, and we are a well-known and trusted platform internationally. AJOL's users have been tracked with Google Analytics. The primary users 10 years ago were European and North American, but user numbers from Africa have increased substantially over the years to the point that in 2017 there were nearly a quarter of a million repeat African-based users of our platform. This is a vital achievement for us… making African research output available and accessed/used by African researchers! Globally, there were over 8.5 million full-text article downloads from AJOL during 2017, so we know that AJOL is accomplishing the goal of increased accessibility of the research outputs on our website. AJOL also offers support of journal via email, as well as in-person training workshops, which have been held in countries all over the continent. We at AJOL understand the contexts, challenges, and needs of the small, usually stand-alone or scholarly association-based journals in developing countries, as well as the intricacies of implementing DOIs for hundreds of journals as a Crossref Sponsoring Member.

### Personal statement<br>
For the past 11 years, I have been the Executive Director of African Journals OnLine (www.ajol.info). AJOL is a South African Non Profit Organisation working toward increased visibility and quality of African-published research journals. AJOL hosts the world’s largest online collection of peer-reviewed, African-published scholarly journals and is a sponsoring member of Crossref. I am also a member of the Advisory Committee of the Directory of Open Access Journals (DOAJ www.doaj.org), a member of the Advisory Board for the Public Knowledge Project’s current study on Open Access Publishing Cooperatives, and member of the Advisory Board of CODESRIA’s new African Citation Index initiative.

I have an academic background in Development Economics and have an abiding interest in the role that increased access to research outputs can play in low income and emerging economies, as well as the practicalities of attaining this. AJOL hosts 521 Journals of which 440 have active DOIs assigned. At last count there were more than 84 000 articles with DOI's on AJOL: including roughly 56 000 with DOIs assigned by AJOL (prefix 10.4314), and 28 000 with DOIs assigned by the Journal itself or relevant publisher. As AJOL has no control over the DOIs assigned by entities other than ourselves, we have found in the past that conflicts can arise when changes take place at Journals' publishers and ownership of DOIs. Hence we run a periodic verification process in order to ascertain that the DOIs we display on www.ajol.info are accurate, during which we fetch the latest valid DOI for each article directly from Crossref, regardless of publisher/owner. In order to avoid conflicts, any DOIs added in the interim period to the site do not get displayed until the next verification process. This causes delays in the display of DOIs on AJOL. The verification process happens approximately every 2 months. Even though the process itself is automated, it takes about two days, during which it requires manual monitoring to ensure all goes smoothly, and therefore we are not always able to execute it more often.

We would very much like to discuss a different pricing model for the Similarity Checker aspect of Crossref's services for developing country Sponsoring Members to make this essential service more practicable for developing countries, and I believe that our deep knowledge of the (sometimes heterogeneous needs) of our journal partners in Africa can contribute to a more balanced decision-making process of the Crossref Board as regards working towards offsetting the inequalities of scholarly research outputs between the Global North and Global South, helping boost the verified credibility, sharing and preservation of quality research from developing countries.

My colleague at AJOL, Kate Snow, who is the Content & Communications Manager, would likely be the person to attend Crossref meetings if I could not. Kate deals with implementing DOIs together with our partner journals every day, and has an intimate knowledge of the specifics of that work, as well as an overall understanding of AJOL's work, organisational ethos and the nature and needs of our partner journals.

***

{{% imagewrap right %}} <img src="/images/blog/jasper-simons.png" alt="Jasper Simons, APA" width="225px" class="img-responsive" /> {{% /imagewrap %}}

<a id="APA"></a>
## Jasper Simons, American Psychological Association (APA), USA<br>
With more than 115,000 members, it is APA’s mission to advance the creation, communication and application of psychological knowledge to benefit society and improve people's lives. An important part of achieving this mission is our vibrant publishing program, which includes a portfolio of 80+ journals published on behalf of both communities within APA and related scholarly societies worldwide, a scholarly books program, a children’s book imprint, digital learning solutions and a suite of databases, including the discovery platform PsycINFO®, the most trusted and comprehensive library of psychological science in the world.

For decades, APA Publishing has been a leader in developing knowledge solutions and setting standards for scholars in social and behavioral sciences. The APA Style and our new Journal Article Reporting Standards (JARS) for quantitative and qualitative research impact the reporting of scholarly work well beyond APA’s journals or the field of psychology. APA’s partnership with the Center for Open Science further demonstrates our commitment to collaborating across the industry to improve research outcomes.

APA has been a member of Crossref and on the Crossref board since its early days. Together with fellow publishers, exceptional Crossref staff and industry partners, APA has helped to structure, process, and share metadata to reveal relationships among research outputs across the world. APA is ambitious about the future and excited about the role Crossref can play to make research outputs easy to find, cite, link, assess, and reuse. It is important that the Crossref board has representation from a trusted Society publisher in the social and behavioral sciences so that the needs of this vibrant community are reflected in the organization’s important work.

### Personal statement<br>
As the Chief Publishing Officer of APA, Jasper is responsible for driving strategy, establishing editorial policies, producing content, developing products and overseeing the related sales and marketing services. He manages a team of almost 200 expert staff in APA’s Washington, DC office. In 2017, he led the collaboration with the Center for Open Science to support Open Science and Reproducibility in psychology. The collaboration advances the integration between the content in the Open Science Framework and the peer-reviewed content of the APA.

Jasper served on the Crossref board from 2015 through 2017 and was the Chair of Crossref’s Nominating Committee in 2015-2016. He also served on the Executive Committee of the AAP/PSP board, and is a member of the Standards & Technology Committee of the STM Association. Jasper has 20 years of experience in scholarly publishing, working at leading publishing organizations such as Elsevier, SAGE Publications and Thomson Reuters.

(Alternate) Tony Habash, Chief Information Officer: Tony F. Habash, DSc, has been the Chief Business Integration Officer and the Chief Information Officer of APA since 2007.

***

{{% imagewrap right %}} <img src="/images/blog/scott-delman.png" alt="Scott Delman, ACM" width="225px" class="img-responsive" /> {{% /imagewrap %}}

<a id="ACM"></a>
## Scott Delman, Association for Computing Machinery (ACM), USA<br>
ACM is a scientific society devoted to furthering computing research and education and supporting the professional needs of its 110,000 members around the world. As a non-profit publisher who’s governance is composed primarily of members of the scientific community, ACM has a unique perspective on many of the changes we are experiencing in the scholarly publishing industry, in that every decision we make needs to consider both the short and long term effects on computer scientists, students, and educators as its primary concern and generating income as an important but secondary consideration.

ACM is by most standards an incredibly lean and efficiently managed organization. As a society publisher, our publication’s program supports the research community through its various high impact publications, but our publications also fund a wide variety of educational and practitioner-oriented good works programs around the world, as well underwrite public policy initiatives that provide a leading independent and non-partisan voice on U.S. and European public policy issues relating to computing and information technology, such as innovation, artificial intelligence, privacy, big data and analytics, security, accessibility, intellectual property, and technology law. As a standalone organization, ACM is neither big, nor small, in comparison to many of our peers and service to both the computer science community and the scholarly publishing community is embedded in our DNA.

For the past two decades, ACM has been committed to serving the interests of the scholarly publishing community as a founding and active board member of many of the industry’s most important publishing technology initiatives, including Crossref, CHORUS, ORCID, and Portico. ACM has served on the Board of Directors of Crossref since inception, and I currently serve on Crossref’s Executive Committee as Board Treasurer.

I believe Crossref is at a critical moment in its history and now, perhaps more than any other time in the past, requires strategic and disciplined leadership from its Board of Directors, and leadership that can help to provide an important balance to the strong and sometimes dominant voices and special interests of the large commercial publishers’ on the organization’s Board and to Crossref’s highly skilled and professional staff, who are constantly looking at the scholarly publishing landscape and proposing new ways for Crossref to play a role. As a result, Crossref is rapidly expanding its menu of services to the publishing community and is for the first time considering expanding its membership beyond publishers to research institutions and funders to drive growth for the organization. At the same time, key technologies, such as Machine Learning, have the potential to impact Crossref’s core DOI registration service, which the organization relies so heavily on.

The decisions Crossref takes over the next few years, in terms of its own growth and where it chooses to invest its staff and financial resources, and more generally how it responds to the changing technology landscape, will have a transformational impact on the organization. As a technology-focused organization that faces many of the same challenges that Crossref is facing, ACM, as represented by its Director of Publications, is in a good position to offer sound, consistent, and strategic leadership on the Board.

### Personal Statement<br>
During my tenure on the Board, I have served as both a board alternate and member, chaired and served on the Nominations Committee, chaired and served on the Membership & Fees Committee, and currently serve on Crossref’s Executive Committee as Board Treasurer. If elected to another term on the Board, I am committed to working closely with Crossref’s excellent staff and leadership to ensure Crossref’s future success, but also to ensure that Crossref’s growth is managed carefully and in a way that supports both the scholarly publishing community and the scientific community.

My alternate is Bernadette Shade, Print Production Manager, ACM.

***

{{% imagewrap right %}} <img src="/images/blog/catherine-mitchell.png" alt="Catherine Mitchell, CDL" width="250px" /> {{% /imagewrap %}}

<a id="CDL"></a>
## Catherine Mitchell, California Digital Library (CDL), USA<br>
Since its founding in 1997, the California Digital Library (CDL) at the University of California has been engaged in building a library-based publishing program that supports emerging disciplines and scholars, explores new publishing models, and seeks to reach professionals in applied fields beyond academia.

CDL provides open access publishing services for UC-affiliated departments, research units, publishing programs, and individual scholars who seek to publish or edit original work - as well as comprehensive repository services for a wide range of scholarship including working papers, conference proceedings, electronic theses and dissertations, and data sets.

Now with over 80 journals published on its eScholarship platform, CDL is an established library publisher with robust infrastructure, deep knowledge of the myriad publishing practices and needs across the academy, and a strong interest in the discoverability challenges faced, in particular, by open access materials; the organization regularly grapples with the complexity of rationalizing metadata across distinct research outputs, identifying new mechanisms to ensure global access to this research, and encouraging authors to license their work in ways that support sharing and reuse while at the same time retaining their own copyright.

As a major library publisher and a member of the Library Publishing Coalition, CDL would help broaden Crossref’s understanding of the unique needs and opportunities within this burgeoning space, particularly in the context of DOI registration, metadata requirements associated with new research output formats, aggregated metrics, and the Event Data initiative. As the consortial digital library for the ten University of California campuses, CDL would represent a public institution of great breadth and ambition that has declared its commitment to open scholarship and continues to seek new ways to make good on that promise. And, as a current member of Crossref and ORCID, as well as a founding member of DataCite, CDL would bring its dedication to supporting community-led efforts to enhance the discoverability, interconnectedness and broader contextualization of scholarly communication around the world.

### Personal statement<br>
As the Director of Publishing & Special Collections at the California Digital Library, I am responsible for overseeing the strategic planning, development, and operational management of CDL’s suite of library publishing services for the ten University of California campuses, including an open access publishing platform (80+ active journals), a research information management system, and an institutional repository. I am also Operations Director of UC’s Office of Scholarly Communication and, in this capacity, am particularly engaged in questions of use, value, authorship, and professional legitimacy. And finally, I have served as the President of the Board for the Library Publishing Coalition for the past two years (June 2016 - June 2018), and will remain on the Board this year as Immediate Past-President.

As someone with over a decade of leadership experience within scholarly communication and, in particular, library publishing, I am well positioned to bring a new perspective to the Crossref Board that reflects a growing community of academic publishers who operate outside the commercial and university press world but are, nonetheless, responsible for the production and distribution of a significant and growing number of scholarly research publications.

I would contribute to Crossref as a board member by representing the substantial commitment of libraries as partners in establishing an open scholarly communication environment and the unique needs of and challenges faced by library publishers worldwide in ensuring appropriate visibility and “credit” for open access publications. As a community, we have worked with DOIs for articles but are keen to see the standard evolve in ways that can better accommodate the full complement of scholarly research output, from data sets and pre-prints to blog posts and micro-publications. Crossref is uniquely positioned, as a globally engaged organization, to solve these problems and help create a standards-driven, hyper-connected scholarly communication environment that transcends siloed publishing systems and national boundaries to maximize the availability of contextualized and relevant information.

Prior to joining CDL, I earned an AB in English from the University of Chicago and a PhD in English Literature from the University of California, Berkeley.

I propose John Kunze, Identifier Systems Architect at CDL, as our alternate.

***

{{% imagewrap right %}} <img src="/images/blog/paul-peters.png" alt="Paul Peters, Hindawi" width="300px" /> {{% /imagewrap %}}

<a id="HW"></a>
## Paul Peters, Hindawi, UK<br>
As one of the leading independent publishers of Open Access journals, Hindawi has played an important role in representing the interests of OA publishers on the Crossref Board for the past 9 years. Hindawi's commitment to develop open infrastructure to support scholarly communications is highly aligned with Crossref's mission, as are Hindawi's efforts to support universities and funding agencies in their efforts to become more closely involved in the scholarly communications system.

In addition to the contributions that Hindawi has made to Crossref, it has also been an important contributor to OASPA, I4OC, JATS4R, and many similar initiatives. Hindawi has also built strong partnerships with some of the most well-established scholarly publishers, including Wiley and AAAS, in order to work together in expanding the open access publishing activities of these organizations.

### Personal statement<br>
In my 9 years serving on the Crossref Board I have contributed to a number of important initiatives within Crossref, served on a number of Crossref's working groups and committees, and most recently served as the Chair of Crossref's Board. I have a very detailed understanding of Crossref's existing services, as well as the projects that are currently under development.

In terms of my vision for the Crossref community, I believe that one of the greatest opportunities and challenges for Crossref in the years ahead will be to expand the organization's current membership to include new categories of members while continuing to serve the needs of the existing membership. I also believe that Crossref has a great opportunity to expand the constituencies it serves by building closer relationships with other organizations who share Crossref's mission but cater to constituencies that are not currently represented in Crossref's membership.

If Hindawi is re-elected to the Crossref Board, Craig Raybould (Hindawi's Director of Operations) would be Hindawi's alternate Board Member. In addition to currently serving as a member of Crossref's Board, Craig is also involved in several closely-aligned initiatives including JATS4R and Metadata 2020.

***

{{% imagewrap right %}} <img src="/images/blog/richard-fidczuk.png" alt="Richard Fidczuk, Sage" width="300px" /> {{% /imagewrap %}}

<a id="SG"></a>
## Richard Fidczuk, SAGE, UK<br>
Founded in 1965, SAGE is a leading independent, academic and professional publisher of innovative, high-quality content. SAGE publishes journals, and books, digital publications and online resources, across the STEM and HSS fields, and has been heavily involved with Crossref since its inception, with members on the Executive, Nominating and Finance Committees in recent years.

We are uniquely positioned because of our size and independent status to bridge the gap between the large commercial publishers and smaller publishers and to represent and understand the interests of both in a way no other organization can. SAGE publishes in partnership with a large number of society partners and so is intimately connected with the academic community and its changing needs, which SAGE can feed into its work with Crossref.

Because of the great breadth of its publishing, including journals (STM, HSS, and Open Access), college textbook publishing, library products and data, SAGE is intimately familiar with the large range of the requirements and challenges faced by Crossref and therefore well situated to ensure that it remains at the forefront of developments, whether it is in the use and deployment of new services, identifiers, metadata, or new forms of content. The breadth of SAGE’s use of DOIs and the spectrum of coverage is vast when thinking about the use and place of Crossref services and the importance of quality identifiers and metadata services in our industry.

SAGE can also call on a wide range of experts within the organization beyond the proposed Board members if necessary to assist in projects: SAGE staff also participate already in a number of Crossref ad hoc technical working groups.

### Personal statement<br>
I am Global Journals Production Director at SAGE Publishing, where I am responsible currently for the production of over 1000 journals, both traditional and open access. I have worked previously at IOP Publishing, Prentice Hall, and Blackwell Publishing and I’ve been in the publishing industry for over 30 years, managing production operations of both books and journals. I was involved right at the start of the publication of online journals in the 1990s.

I have served as SAGE representative for NISO (the National Standards Information Organization) and on the ALPSP (Association of Learned and Professional Society Publishers) Board, where I was Chair of their Research Committee. I am also a Chartered Director and Fellow of the UK Institute of Directors.

As a result of this wide experience I have a deep comprehension of the crucial role that Crossref plays as the underpinning of so much of the infrastructure of scholarly communications today. It’s important that Crossref continues to innovate as the needs of the scholarly community continue to evolve, and to aid this I can contribute and advise in both the technical aspects required to understand and to set Crossref’s vision and future strategy, and the commercial aspects needed to ensure that Crossref continues to be successful and to thrive into the future.

The alternate for me on the Board of Crossref would be John Shaw, Vice President, Publishing Technologies. This would reverse the current situation where John is the main Board member and I am his alternate.

***

{{% imagewrap right %}} <img src="/images/blog/duncan-campbell.png" alt="Duncan Campbell, Wiley" width="250px" class="img-responsive" /> {{% /imagewrap %}}

<a id="WL"></a>
## Duncan Campbell, Wiley, USA<br>
Founded in 1807, John Wiley and Sons (Wiley) publishes on behalf of more academic, professional and scholarly societies and associations than any other publisher.

Wiley was a founding member of Crossref, as was Blackwell, which merged with Wiley in 2007. We see Crossref as a core component of the research publishing ecosystem and are committed to helping Crossref as an organization meet its goals and objectives. Overall, Wiley strives to take a leadership role in scholarly publishing, and through initiatives such as CHORUS and ORCID (among others) we work with stakeholders across the industry to help develop infrastructure that supports researchers and scholarly publishing globally.

We are a global company, with major publishing centres in the United States, the United Kingdom, Germany, Australia, China, and Japan. Wiley colleagues are actively engaged in numerous industry membership associations, such as the International Association of Scientific, Technical and Medical Publishers (STM), the Association of Learned and Professional Society Publishers (ALPSP), the Publishers Association (PA) and the American Association of Publishers (AAP). Wiley is also a co-chair of Project DARE, which is working to address skills gaps in the Asia-Pacific region.

We recognize that the market for scholarly publishing is changing rapidly, creating opportunities to develop new ways to describe, share, and disseminate research advances; we are committed to supporting the transition to Open Science in all manifestations, and to managing the transition in a sustainable manner for all stakeholders.

### Personal statement<br>
I am currently Director, Global Sales Partnerships, for Wiley’s Research business, where I am responsible for licensing, agent relations and copyright & permissions for Wiley’s academic journal and database content. In addition, I am also engaged in developing Wiley’s strategies and policies in areas such as government affairs, content sharing/syndication and text & data mining.  I am co-chair of the CLOCKSS digital archive, a not-for-profit joint venture between the world's leading academic publishers and research libraries, and am also a member of the International Publishers’ Rights Organization (IPRO) board, and a non-executive director of Seren Books, a non-profit literary publisher based in Wales.

I have represented Wiley on the Crossref board since 2015, and am currently a member of the Audit Committee and the Membership & Fees Committee. Crossref is a key component of the scholarly publishing ecosystem, and plays a major role in the development of the standards and infrastructure that we all depend on for our day-to-day publishing activities. If we want our industry to survive (and thrive) in the future, we need to work together as Crossref members and stakeholders to build a robust and sustainable open infrastructure for scholarly publishing that supports the continuing development of innovative products and services, irrespective of business models.

Wiley’s alternate is Sophia Joyce, Vice President, Content Strategy & Management.

***
