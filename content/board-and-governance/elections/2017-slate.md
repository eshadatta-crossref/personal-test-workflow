+++
title = "Board election 2017 candidates"
date = "2017-09-29"
draft = false
author = "Ginny Hendricks"
parent = "Elections"
rank = 2
weight = 6
+++

If you are a voting member of Crossref your 'voting contact' will have received an email on September 28, 2017. If you are the designated voting contact please follow the instructions in that email which includes links to the relevant [election process and policy information](/board-and-governance/elections).

From 25 applications, our [Nominating Committee](/committees/nominating/) proposed the following nine candidates to fill the six seats open for election to the Crossref Board of Directors.  For the first time, we have more candidates than available seats:

| Member organization                                          | Candidate standing     | Country      |
|:-------------------------------------------------------------|:-----------------------|:-------------|
|[American Institute of Physics](#AIP)                         |Jason Wilde             |USA           |
|[F1000 Research](#F1000Research)                              |Liz Allen               |UK            |
|[Institute of Electronic and Electrical Engineers](#IEEE)     |Gerry Grenier           |USA           |
|[The Institution of Engineering and Technology](#IET)         |Vincent Cassidy         |UK            |
|[Massachusetts Institute of Technology Press](#MIT)           |Amy Brand               |USA           |
|[OpenEdition](#OE)                                            |Marin Dacos             |France        |
|[SciELO](#Sci)                                                |Abel Packer             |Brazil        |
|[SPIE](#SPIE)                                                 |Eric Pepper             |USA           |
|[Vilnius Gediminas Technical University Press](#VGTU)         |Eleonora Dagiene        |Lithuania     |
<br><br>

{{% imagewrap right %}} <img src="/images/blog/Jason1.jpg" alt="Jason Wilde, AIP" width="300px" /> {{% /imagewrap %}}

<a id="AIP"></a>
## Jason Wilde, American Institute of Physics (AIP), USA<br>
For more than 80 years, AIP Publishing has championed the needs of the global research community as a wholly owned, not-for-profit, subsidiary of the American Institute of Physics (AIP). Our mission is to support the charitable, scientific and educational purposes of AIP through scholarly publishing activities across the physical sciences.

From being an instigator in the development of CHORUS to taking a lead role in enabling public access to research content AIP Publishing has played a pivotal role in shaping our industry. Through our role on the boards of key industry bodies, Crossref, International Association of Scientific, Technical and Medical Publishers (STM), CHORUS, and Professional and Scholarly Publishing (PSP) Executive Council, AIP Publishing continues to ensure that the voice of small, scholarly, publishers is clearly heard.

At a time when a small number of large commercial publishers are taking dominant roles in scientific publishing, there is an ongoing need for strong representation of self-published not-for-profit societies, such as AIP Publishing, across our industry and, most importantly, on the Crossref board.

### Personal statement<br>
As the Chief Publishing Officer (CPO) for AIP Publishing, Jason is responsible for the leadership, vision, and growth of the publishing program.  Jason currently serves as a board member for Crossref (2014–2017) and previously from 2007–2013 when he was with Nature. Jason was a member of the Crossref Nominating Committee in 2011, 2012, and 2015, and in 2017 was elected to Crossref’s Executive Committee.

With 20 years’ experience in both large for-profit and smaller not-for-profit organizations and significant involvement in the Crossref organization, Jason has a unique understanding of our industry and the vital role that Crossref plays. Crossref is not only the core infrastructure that links research and aids discoverability, but it provides the framework and data on which many other industry tools and services are built. As publishing continues to evolve, there is opportunity for Crossref to increasingly serve as a focal point for the industry, fostering collaboration among publishers, and developing solutions that serve the needs of our customers – researchers, librarians, funders and governments.

Before joining AIP Publishing, Jason was Business Development Director for Nature Publishing Group (NPG). During his twelve years with NPG, Jason oversaw the creation of the Nature-branded physical science research journals and NPG’s Open Access publications Nature Communications, Scientific Reports and Scientific Data. Prior to his career in publishing, Jason earned degrees from Dundee University (BEng in Electrical and Electronic Engineering), Cambridge University (PGCE in Balanced Science and Physics) and Durham University (PhD in Molecular Electronics).

Jason also serves on the board of the International Association of Scientific, Technical and Medical Publishers (STM).<br>The alternate for Jason on the board of Crossref would be Dr. John Haynes, CEO AIP Publishing.

***

{{% imagewrap right %}} <img src="/images/blog/Liz1.jpg" alt="Liz Allen, F1000" width="300px" /> {{% /imagewrap %}}

<a id="F1000Research"></a>
## Liz Allen, F1000, UK<br>
F1000 Research would be an important and strategic addition to the Crossref Board at this time. Faculty of 1000 is a long-standing open research innovator and since 2013 has been working with a range of partners across the world to provide online, open research publishing solutions, to accelerate access to research findings outside of traditional publishing outlets, and to support the associated use and potential impact of research (e.g. Wellcome Open Research; Gates Open Research; F1000Research).

Ultimately Crossref exists to build an infrastructure to bring about efficiencies in how we deliver, discover and use knowledge and scholarly insights. F1000 is therefore well placed to specifically advise and play an active role in Crossref Board and strategic discussions about how best to ensure that newer research publishing models and outlets are considered in specific initiatives and broader strategy and projects (e.g. Event Data; Funding Registry; Crossmark). In addition, through its direct
work with funding agencies, research institutions and Learned Societies, F1000 would bring a broad, cross-sector perspective and experience to the Crossref Board as it seeks to evolve its scholarly infrastructure and metadata requirements and services.

### Personal Statement<br>
I am currently Director of Strategic Initiatives at F1000, and involved in seeking opportunities and shaping new initiatives particularly to promote and foster open research. I have spent much of my career thus far involved in projects and initiatives that aim to improve the understanding of how science progresses and how knowledge can be used - essentially to accelerate access to and the potential impact of research. In 2015 I became a Visiting Senior Research Fellow in the Policy Institute at King’s College London and continue to advise on academic projects that seek to understand research impact.

Prior to joining F1000 in 2015, I spent over a decade as Head of Evaluation at the Wellcome Trust (a major biomedical research funding agency), with a particular specialism in impact assessment and the development of science-related indicators, serving as an adviser on the 2015 UK government commissioned Independent review of the role of research metrics in research assessment
https://www.hefce.ac.uk/rsrch/metrics/. I understand the vital importance of building a data infrastructure to connect science, scientists and associated research outputs. I was a Board Director of [ORCID](https://orcid.org/) between 2010-2015 and helped to mandate the adoption of ORCID for all Wellcome grantees. While at Wellcome I also co-led the development of project CRediT (Contributor Roles Taxonomy - http://www.casrai.org/CRediT) and continue to serve on the CASRAI CRediT committee.

During my time at Wellcome I played an active role in helping Crossref to shape what is now the Funding Registry and worked with funding agencies to encourage the adoption of consistent ways to classify and capture research-related data – though there is lots more to be done to consolidate and develop this valuable information.

I am very supportive of Crossref’s metadata drive and think that my knowledge and cross-sector experience and networks, would make me a valuable addition to the Crossref Board at this time. If I were appointed to the Crossref Board, my alternate would be Michaela Torkar, PhD, Publishing Director at F1000.

***

{{% imagewrap right %}} <img src="/images/blog/Gerry1.jpg" alt="Gerry Grenier, IEEE" width="300px" /> {{% /imagewrap %}}

<a id="IEEE"></a>
## Gerry Grenier, Institute of Electronic and Electrical Engineers (IEEE), USA<br>
The Institute of Electrical and Electronics Engineers (IEEE) is the world’s leading technical
membership organization, with over 420,000 members and a mission to advance technology for
the benefit of society. It inspires a global community of scholars and industry professionals to
innovate for a better tomorrow through highly-cited publications, conferences, technology
standards, and professional education services.

A member and supporter of Crossref since its inception, IEEE sees its contribution to the
organization as an opportunity to influence the future of scholarly publishing through continuing
development of innovative and shared collaborative services for the industry. IEEE as a
publisher and a not-for- profit organization is at the forefront of numerous initiatives to serve
authors, reviewers, readers, and customers and brings a community-centric approach to its work
with Crossref. It will seek to balance the needs of Crossref’s global consumers and its diverse
member base, advocating sustainable, fiscally sound management of the organization.

### Personal statement<br>
Gerry is Senior Director of Publishing Technologies & Content Management, a role he has held since joining IEEE in 1999. He manages the development and operation of the IEEE’s publishing
ecosystem, including the systems used to create, enhance, distribute, and archive IEEE’s intellectual
property. He has been a member of the Crossref Board since 2004, a member of its Executive Committee since 2013, and its Treasurer since 2016.

Gerry’s career in the STM business began in 1980 with editorial and production roles for life sciences
publisher Alan R. Liss, Inc. He joined John Wiley &  Sons with its acquisition of Liss in 1987 and served in editorial, production, and technology roles -- culminating in his role as Director of Publishing
Technology. He was a key member of the team that developed Wiley’s initial online digital library, Wiley
Interscience.

He is currently serving on the NISO Board of Directors, is a past Board Member of International STM,
Past Chair of the New Jersey Chapter of the American Society for Information Science and is a member
of the Association for Computing Machinery.

(Alternate) Michael Forster, Managing Director, Publications<br>
Michael is Managing Director for Publications at IEEE, a position he has held since late 2015, and has
more than two decades of experience in educational, professional, and research publishing. Working for
both Elsevier and Wiley prior to IEEE, he has served in senior management positions in the UK, the US,
and in Germany, with expertise in strategic planning, M&A and portfolio analysis, product management, and content development.

As a leader within Wiley for nearly a decade, he was responsible for journal, book, magazine, database,
and scientific workflow product portfolios, led development of new products in researcher workflow, and introduced the first semantic enrichment products to Wiley’s online platform. Michael began his career with publisher Butterworth-Heinemann, then worked as Publishing Director for Engineering and
Computer Science in Elsevier’s Science and Technology Books business where he led innovative projects in online learning and eBook products and to transform business models for the textbook market.

Michael received his B.A. (Hons) and M.Eng. in Engineering Science from the University of Oxford.

***

{{% imagewrap right %}} <img src="/images/blog/vincent1.jpg" alt="Vincent Cassidy, IET" width="300px" /> {{% /imagewrap %}}

<a id="IET"></a>

## Vincent Cassidy, Institution of Engineering and Technology (IET), UK<br>
The Institution of Engineering and Technology will contribute to the Crossref board in two ways. Firstly, as a learned and professional society publishing a broad range of content (journals, books, standards, multi-media) we will represent the interests of a significant section of Crossref’s core membership. Learned membership bodies are developing their own distinct approaches to the open science and open data debate and, being well networked within the world of learned societies, we will be able to reflect the role of the learned society in the changing landscape of scholarly discourse.

In addition, through the process of semantically enriching Inspec, our flagship A&I service, we appreciate the opportunities and concerns inherent in managing distributed big-data assets and the evolution of new service models and relationships in the scholarly network. Inspec supports a metadata service business and gives the IET an interesting perspective on the challenges and opportunities facing Crossref.

### Personal statement<br>
I have 30 years experience in scholarly and professional publishing and having held leadership positions at Academic Press, Thomson Reuters, Elsevier Health Sciences, British Standards (BSI) and the IET I have experience of many information markets and sectors across Crossref’s membership base. I have worked closely with Crossref and DOIs from the birth of the organisation (in my time working with Academic Press's Ideal project) and have continued to support and promote Crossref through my career, including leading the adoption of DOIs by the standards community during my period at British Standards.

I understand the key role of standards in our industry and Crossref’s value and potential in the networked information economy. It is easy to take Crossref's central role in facilitating scholarly discourse for granted, and it is instructive to remember the high degree of competition between the major commercial publishers that was the original spur for Crossref's creation. Crossref has facilitated the development of a system of networked content that has improved the process of scholarly communication.

My vision is that our content centric industry is evolving new service dimensions that will enhance, not only the content output, but the process of research and development itself. Crossref can continue to add value by providing the standards and the institution to connect this increasingly complex stakeholder group.

I propose Sara Killingworth (Head of Marketing, IET) as our alternate.

***

{{% imagewrap right %}} <img src="/images/blog/Amy1.jpg" alt="Amy Brand, MIT" width="300px" /> {{% /imagewrap %}}

<a id="MIT"></a>
## Amy Brand, Massachusetts Institute of Technology Press (MIT Press), USA<br>
One of the largest and most distinguished university presses in the world, the MIT Press is known
for bold content and design, creative technology, and its commitment to re-imagining university
based publishing. By electing the MIT Press to the Crossref Board, the Crossref community will
have the representation not only of a leading university press, but also of a technically progressive press that spans STEM and HSS fields within its book and journal programs. The MIT Press is resolutely focused on pushing the boundaries of digital publication across the board, and is committed to integrating DOIs into all aspects of digital publication. Core to our vision is enriching the metadata environment for academic research, and interlinking as extensively as possible with text, data and nontraditional objects, and with persistently identified organizations, stakeholders, and roles that comprise a more articulated scholarly communication infrastructure. With a seat on the Crossref Board, the MIT Press will be able to help grow adoption of DOIs for books and other digital objects within the university press community, many members of which have only recently embarked on digital publishing.

### Personal statement<br>
Amy Brand worked at Crossref for several years in the early days of the organization, serving as
Director of Business and Product Development from 2001-2007. Hence, she should would bring
to the Crossref Board a uniquely deep perspective on how the organization functions, how
members of our community work together to move a shared agenda forward, and what the
opportunities are for continuing to develop and improve Crossref services.

Because her career has afforded her experience in several regions of the scholarly communication landscape — academic research, university administration, open access, analytics, entrepreneurship, in
addition to book and journal publishing — Amy brings valuable insight into diverse perspectives.

She is unusually well qualified to help build the consensus that makes Crossref work so well. In
recent years, she helped launch the CRediT contributor role taxonomy initiative, and she’s
currently involved in a new community-wide project to grow the adoption of peer review badges. Both of these projects produce metadata that can and should be integrated into the Crossref system. Amy was also a founding member of the ORCID Board of Directors and currently serves on the National Academies Board of Research Data and Information, the Duraspace Board of Directors, and the advisory board for altmetic.com.

The MIT Press Alternate to the Crossref board, should we be elected, would be our Journals Director, Nick Lindsay. Nick has successfully run the MITP journals program since 2009, having brought forward several significant technology and business model changes that positively transformed the division. Prior to his move to the MIT Press, Nick worked at the University of California Press in their journals
department. He has served on several committees within the Society for Scholarly Publishing and
the Association of American University Presses, and chaired the scholarly journals committee for
AAUP for two years.

***

{{% imagewrap right %}} <img src="/images/blog/Marin1.jpg" alt="Marin Dacos, OpenEdition" width="300px" /> {{% /imagewrap %}}

<a id="OE"></a>
## Marin Dacos, OpenEdition, France<br>
OpenEdition is a major European platform for HSS in Europe, which provides publishing services for 450 journals, 4000 books and more than 2000 academic blogs. We are based in Marseille and Paris. We are pursuing long-term projects in Italy (OpenEdition Italia), Portugal (LusOpenEdition), Germany (de.hypotheses.org), Spain (es.hypotheses.org) and the Netherlands (OpenEdition is the co-founder of the DOAB Foundation, with the OAPEN Foundation). Our content is published in five European languages: French, English, German, Spanish and Italian. We are in the process of establishing a European network of all players involved in open digital publishing. This is OPERAS <http://operas.hypotheses.org>, which brings together 20 partners, 9 countries and 2 lots of H2020 funding. Together, those involved have published 800,000 documents and 250,000 authors.

OpenEdition’s presence on the board will show that disciplines whose scholarly communication is plurilingual have a place within the organization. It will also underline the inclusion of non-STM platforms and publishers as integral parts of the digital scholarly publishing sector. This will help Crossref to embrace the cultural and organizational diversity of its communities.

### Personal statement<br>
With 20 years of experience as the founding director of OpenEdition, I am very familiar with the diversity of the HSS publishing ecosystem in Europe. I have been at the forefront of the digitalization of scholarly communication, supporting editors, publishers and academics to shift from paper to electronic formats. This ongoing revolution has deeply transformed the HSS publishing industry, with some embracing it and inventing new formats and economic models, and others remaining reluctant and seeing it as only a secondary market and communication channel.

In the face of this diversity, I have developed tools and adapted standards in order to include as many publishing actors as possible and to drive them to the most open solutions. As DOIs and Crossref have become key services for open science, it is necessary to take into account small publishers and platforms, in order to provide a comprehensive service for the whole industry, and not only to major players and international initiatives. To do so, the diversity of languages and disciplines for future developments of Crossref services should be acknowledged.

I have been using Crossref’s services as a publisher for almost a decade and know them fairly well. I hope that these services will expand even more substantially in the future, particularly in terms of covering the minor forms of scientific communication that are blogs, as well as by offering functionalities specific to fine-grained forms of publishing (particularly critical editions of historical sources).

Proposed alternate : Pierre Mounier (EHESS, OpenEdition).

***

{{% imagewrap right %}} <img src="/images/blog/Abel1.jpg" alt="Abel Packer, SciELO" width="300px" /> {{% /imagewrap %}}

<a id="Sci"></a>
## Abel Packer, SciELO, Brazil<br>
My organization is the Scientific Electronic Library Online (SciELO) Program which is an international collaboration on scholarly communication aiming at improving quality, visibility and credibility of nationally edited journals and the research they communicate.

SciELO was launched in 1998 in Brazil and it is implemented through the SciELO Network composed by 15 national collections of selected open access peer reviewed journals from Latin American countries, Portugal, Spain and South Africa. The network is fully decentralized using the same principles and methodology and publishes about 1 thousand journals from all disciplines, 50 thousand new articles per year and an accumulated repository of over 700 thousand articles. The network serves over one million downloads per day according COUNTER methodology.

SciELO is probably the most important international cooperation program on scientific communication among developing countries.

### Personal statement<br>
With more than 20 years of experience on scientific information management in Latin America, my application envisages to enrich the Crossref Board with a voice and demands of Latin American and other developing regions on the adoption of Crossref products and services.

My alternate will be Fabio Batalha, SciELO leader on systems development and information technology infrastructures. Fabio has an extensive experience on the management of Crossref services.

***

{{% imagewrap right %}} <img src="/images/blog/Eric1.jpg" alt="Eric Pepper, SPIE" width="300px" /> {{% /imagewrap %}}

<a id="SPIE"></a>

## Eric Pepper, SPIE, USA<br>
I have worked in publishing at SPIE, the international society for optics and photonics, for thirty-five
years, the past twenty-four of those as Director of Publications, with responsibilities encompassing
editorial, production, technology, and business aspects of all our publishing activities. In this time I have seen and been directly involved in enormous changes in our industry and in how we think about,
produce, and disseminate scholarly information.

Crossref is of course an important outcome of this transformation. SPIE’s first connection to Crossref dates back to 2000 when our journals were hosted on the AIP platform and we participated in Crossref via an agency agreement with AIP. SPIE joined as an independent member a few years after that.

### Personal statement<br>
I have been SPIE’s liaison to Crossref during this entire period and in addition to basic DOI registration and linking was instrumental in adding Similarity Check, Funder Registry, and now Cited-by to the suite of Crossref services we use. SPIE greatly values these Crossref services and the organization that developed and enables them and will appreciate an opportunity to have a voice in Crossref’s governance and strategic direction.

As our representative, I believe that my experience helping to develop and manage a diverse portfolio of scholarly publications in a variety of print and digital formats has given me the perspective needed to understand the needs of publishers as well as the research and educational communities that provide and utilize the content we publish. I have worked with many other organizations, vendors, and publishing partners over the years and through that have developed an understanding of broader industry needs and priorities, which which I can bring to my role in Crossref leadership should I have the opportunity.

***

{{% imagewrap right %}} <img src="/images/blog/Eleonora1.jpg" alt="Eleonora Dagiene, VGTU" width="300px" /> {{% /imagewrap %}}

<a id="VGTU"></a>

## Eleonora Dagiene, VGTU Press, Lithuania<br>
In recent years, many small publishers have joined Crossref. VGTU Press has been representing this particular and growing group of members on the Board for three years. We are an innovative small publisher functioning within a university structure, which means we are close to the academic community and aware of everyday issues faced by researchers, librarians, and administrators.

The services offered by Crossref enable small publishers to keep up with the rapidly changing scholarly communication trends. However, it happens that many small publishers find it difficult to implement some of the services due to having fewer staff with limited technical expertise. The young, promising, and curious team of VGTU Press helps Crossref understand these challenges, and we share with staff and the rest of the Board the reasons and causes of such obstacles. VGTU Press seeks to implement as many Crossref innovations as possible so we have firsthand experience with almost every initiative that Crossref is involved in, actively working with staff to  share information and help with improvements.

As a small publisher supporting Crossref initiatives, VGTU is a good example for other small publishers, showing the feasibility of implementing not only cornerstone services, such as DOI registration or the use of Similarity Check, but also other services offered by Crossref. As the academic community continues to seek new forms and models of publishing, VGTU Press, as one of the most advanced non-profit academic publishers in the Baltics, would be an excellent choice as a representative of small publishers, an area of the world never represented previously on the Crossref Board which is heavily monopolized by large US and UK publishers.

### Personal statement<br>
As someone in the modern academic publishing industry with over ten years’ experience, and for whom work in scholarly communication is a personal passion, I would be a great candidate to re-elect to Crossref's board. Serving on the Board for the term 2015–2017 was not only a privilege and an excellent driver of my professional development but also a great pleasure. It also means I can offer some continuity as I fully understand the key issues that currently face Crossref and the Board.  As Director of VGTU Press and President of the Association of Lithuanian Serials, I have gained a considerable expertise related to scholarly publishing, especially familiarity of the challenges faced by small publishers. I have learned from my experience that a person running a small press and turning it successful and contemporary, should have knowledge of different areas and the ability to think creatively.

To manage processes efficiently, it is important to gain a thorough understanding not only of everyday activities in a press but also to take an interest in innovations which are usually related to technology. Especially when the descriptions of any current innovation can sound as if they were magic words, demanding more time to gain a required level of understanding.

Liaising with researchers/authors, journal editors, and university employees is a part of my daily agenda, which helps me to keep my finger on the pulse of needs and expectations arising from the academic community. Besides, I am also a researcher, author and peer-reviewer, working in the field of scholarly communication.

The three years of experience as a member of Crossref Board provided me with an excellent opportunity to enhance the understanding of Crossref services. Moreover, I have valuable knowledge of various innovations and it is clear for me what problems current scholarly communication faces.

Furthermore, I firmly believe that many publishing-related improvements would have been impossible without the services provided by Crossref. It would be an honour to be selected for an additional term. In any case, I would like to take this opportunity to say that I feel inspired by Crossref activities
and will continue to promote them actively.
