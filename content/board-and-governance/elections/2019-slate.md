+++
title = "Board election 2019 candidates"
date = "2019-08-23"
draft = false
author = "Lisa Hart Martin"
rank = 2
parent = "Elections"
weight = 6
+++

If you are a voting member of Crossref your ‘voting contact’ will receive an email in late September, please follow the instructions in that email which includes links to the relevant election process and policy information.

From 52 applications, our [Nominating Committee](/committees/nominating/) proposed the following seven candidates to fill the five seats open for election to the Crossref Board of Directors.

| Member organization | Candidate standing     | Country      |
|:--------------------|:-----------------------|:-------------|
|[Clarivate Analytics](#clarivate)|Nandita Quaderi|USA |
|[eLife](#elife)|Melissa Harrison |UK  |
|[Elsevier](#elsevier)       |Chris Shillum    |Netherlands   |
|[IOP Publshing](#iop)|Graham McCann |UK|
|[Springer Nature](#springer)|Reshma Shaikh|UK    |
|[The Royal Society](#royal)|Stuart Taylor|UK |
|[Wiley](#wiley)       |Todd Toler             |USA    |

---

{{% imagewrap right %}} <img src="/images/blog/nandita.jpg" alt="Nandita Quaderi, Clarivate Analytics" width="225px" class="img-responsive" /> {{% /imagewrap %}}

<a id="clarivate"></a>
## Nandita Quaderi, Clarivate Analytics, USA<br>
The Web of Science Group provides the tools and resources researchers, publishers, institutions and research funders need to monitor, measure, and make an impact in the world of research. Like Crossref, we want to “make scholarly communications better.”  

Guided by the legacy of Dr Eugene Garfield, inventor of the world’s first citation index, our goal is to provide guaranteed quality, impact and neutrality through world-class research literature and meticulously captured metadata and citation connections. We devised and curate the largest, most authoritative citation database available, with over 1 billion cited reference connections indexed from high quality peer reviewed journals, books and proceedings from 1900 to the present.  

Our products include ScholarOne, which provides comprehensive workflow management systems for scholarly journals, books and conferences; Publons, the home of the peer review community; and Kopernio, a free web browser plug-in which enables one-click access to full text PDFs. We too “make tools and services – all to help put scholarly content in context.”  

The existing Crossref Board is comprised of publishers, libraries, non-profit organisations looking to increase access, and technology providers. As an entirely publisher-independent organisation, we would complement the current membership, and as a hugely diverse organisation ourselves, we would add to the diverse nature of the board. Our presence on the Crossref board would enhance our organisations’ shared goals of revealing meaningful connections among research, collaborators, and data as we strive to enable researchers to accelerate discovery, evaluate impact, and benefit society worldwide.  

### Personal statement <br>
Firstly, as a former researcher myself, I would bring a deep understanding of the challenges academics and institutions face in the research communication process. In my post-academia career, I have consistently championed excellence in diversity, accessibility, and open science.  

After receiving my PhD in Molecular Genetics, and completing a post-doctoral fellowship in Italy, I ran a Wellcome Trust-funded developmental biology lab at Kings College London. This experience provided me with a nuanced understanding of the technological infrastructures supporting research, peer review, and publishing.  

Crossref’s commitment to a culture of openness and transparency mirrors my own post-academic pursuit of universally accessible research, making sure that our industry provides places where people can come to find trusted, high-quality content. I managed the Open Access journal portfolios at Nature Research and BMC. I have experience successfully transitioning journals from hybrid to OA models, leading new OA journals from conception to launch, and promoting open research among my teams and the wider community.  

In my current role as Editor in Chief for the Web of Science, I have overall editorial responsibility for the content on Web of Science and oversee the team of in-house editors that select content for the Web of Science Core Collection. My team’s principles are objectivity, selectivity and collection dynamics; we look to promote and protect the Web of Science’s reputation for excellence, while remaining publisher-independent and innovating to meet the needs of the community.  

As a woman of Bangladeshi heritage in STEM, in all my roles I have worked to build environments where diversity, equality, and gender parity are championed and valued. At the Web of Science Group, I have been honoured to facilitate insightful discussions for International Women’s Day, taking parts in events to encourage women in STEM. I am part of the Women@Clarivate group, an initiative to help women across our parent company grow in their careers.  

I love metadata, love data, and love science. If I were elected to the Crossref board, I would help to increase the impact of existing Crossref initiatives. I aim to bring fresh ideas, based on the diversity and experience of the Web of Science Group to the board. I would promote diversity, open science and excellence in research.  

***

{{% imagewrap right %}} <img src="/images/blog/melissa.jpg" alt="Melissa Harrison, eLife" width="250px" /> {{% /imagewrap %}}

<a id="elife"></a>
## Melissa Harrison, eLife, UK<br>
eLife is an open-access publishing initiative launched by three major biomedical funders, the Howard Hughes Medical Institute, the Max Planck Society and the Wellcome Trust (and joined more recently by the Wallenberg Foundation). eLife publishes a single open-access journal that covers important new findings in the life and biomedical sciences, designs and builds new open-source products and infrastructure to support publishing, and works with the scientific community to advocate for improvement in the way that science is evaluated and communicated. An important aspect of the work at eLife is our commitment to sharing our findings and resources as effectively as possible, so that others can reuse, build on and improve on our work. eLife is often early to engage or propose new ideas related to openness and improving scientific communication and several eLife staff are involved in community initiatives including OASPA, FORCE11, DORA and JATS4R as well as open-source technology collaborations such as COKO and The Substance Consortium.  

eLife has served one term on the Board of Crossref and we would be delighted to continue to serve on the board because of Crossref’s role in providing sustainable and shared infrastructure supporting critical aspects of research communication.  

Melissa Harrison (http://orcid.org/0000-0003-3523-4408)  

### Personal statement<br>
I am Head of Production Operations at eLife, where I manage the production process as well as content and metadata delivery to other services. I have served as eLife’s alternate representative on the Crossref Board for the past three years.  

I have worked in publishing for over 20 years in a variety of organisations:,  beginning with small independent publishers, followed by the Royal College of Psychiatrists and then the  BMJ Group. I also worked for an Indian production services vendor before joining eLife in 2012. I, therefore, have substantial and in-depth knowledge of publishing and the importance of high-quality metadata. I chair JATS4R and actively serve on subgroups in the Metadata2020 and FORCE11 communities. As a member of multiple communities, I have a broad perspective on the opportunities for improvement in general publishing infrastructure and processes. Like Crossref, eLife is also committed to open source technologies and re-use. I initiated the development of an open source tool to convert JATS XML to Crossref and PubMed XML and eLife is working collaboratively on community open source tools to support the complete publishing process.  

The services and infrastructure that Crossref provide are of great value to the publishing community and there are many opportunities to develop them. There is also an expanding range of organisations, including institutions and funders, that Crossref has the potential to serve. With my broad experience in publishing and community engagement, I can offer understanding and insight into the opportunities and challenges faced by Crossref and would relish the chance to continue to represent eLife as part of the Crossref Board.  

Alternate statement: Mark Patterson (orcid.org/0000-0001-7237-0797) is the Executive Director of eLife. Previously, Mark was the Director of Publishing at PLOS where he worked for 9 years and helped to launch several of the PLOS Journals including PLOS Biology and PLOS ONE.  Mark began his career as a researcher in yeast and human genetics before moving into scientific publishing in 1994 first as the Editor of Trends in Genetics and later as one of the launch editors for NPG’s Nature Reviews Journals. Mark has served as the representative for eLife on the Crossref Board for the past three years (in which role he has chaired the Nominations Committee for two years and currently serves on the Audit Committee), and is one of the founding directors of the Open Access Scholarly Publishers Association.  

***

{{% imagewrap right %}} <img src="/images/blog/shillum.JPG" alt="Chris Shillum, Elsevier" width="225px" class="img-responsive" /> {{% /imagewrap %}}

<a id="elsevier"></a>
## Chris Shillum, Elsevier, Netherlands<br>
At Elsevier, we believe that to meet the ever-growing needs of researchers and scholars, the information system supporting research must be founded upon the principles of source neutrality, interoperability and transparency, placing researchers under full control of their information. As the fastest-growing open access publisher, we provide authors with a choice of publication models, and are investing in tools to support Open Science such as SSRN and Mendeley Data.

Today, a vibrant array of companies, non-profits, universities, and researchers provide scholarly tools and information resources. Common standards and shared infrastructure play a vital role enabling innovation and fostering competition towards finding ever better ways for researchers and their institutions to manage the unprecedented amount of knowledge and data that is available to today. As an industry leader, Elsevier has a strong track record of supporting many of the standards and infrastructure services we rely on today: We were charter members of the [International DOI Foundation](http://www.doi.org), have been involved with Crossref since its start, and are founding members of [ORCID](http://www.orcid.org) and [CHORUS](http://www.chorusaccess.org).

As the largest financial contributor to Crossref, we are proud of the role we play in sustaining this vital shared resource, and as a large organization, we are also able to commit significant staff time to the development of new initiatives such as Funding Data, Access and Licensing Indicators and Distributed Usage Logging. We continue to support the maintenance and enhancement of the Crossref Funder Registry as a free service to the community.

### Personal statement<br>
I am an industry veteran, having spent 25 years in scholarly publishing, starting my career during the exciting first wave of the move to online publishing. As well as my day job, where I currently look after Identity and Platform Strategy for Elsevier, I am fortunate to have served in leadership positions at the International DOI Foundation, Crossref, ORCID and NISO. Over the years, I’ve gained experience in most aspects of scholarly communication technology, as well as expertise in strategic management, financial planning and industry collaboration. I am passionate about the role that industry organizations such as Crossref play in enabling innovation and accelerating the development of new ways to help researchers and scholars; I consider it both a privilege and a responsibility to use my skills to help develop, lead and guide such organizations. Philippe Terheggen, Elsevier’s designated alternate, is Manager Director of our STM Journals publishing group.

Crossref is at a pivotal moment in its development, as it seeks to expand its constituency, develop new services and embrace the opportunities of new technology such as AI, whilst at the same time updating its aging core infrastructure. It’s also vital that Crossref maintains the support of its members by delivering value for money and operating its services as efficiently as possible. Crossref will need experienced, disciplined leadership with representation from all sectors of the community to successfully navigate the complex challenges ahead. Should I be fortunate enough to be re-elected to the board, I believe I have the right combination of experience, technical, and strategic skills to help Crossref make the best decisions for its members, its future, and for the community of researchers and scholars that we all serve.

***

{{% imagewrap right %}} <img src="/images/blog/graham.jpeg" alt="Graham McCann, IOP Publishing, UK" width="250px" /> {{% /imagewrap %}}

<a id="iop"></a>
## Graham McCann, IOP Publishing, UK<br>
IOP Publishing is one of the world’s leading scholarly publishers. It has partnered with Crossref since it was founded and shares its mission to make research outputs easy to find, cite, link to and assess.IOP Publishing is a subsidiary of the Institute of Physics, a leading scientific society promoting physics for the benefit of all. Through its worldwide membership of more than 50,000, the Institute works to advance physics research, application and education, and engages with policy makers and the public to develop awareness and understanding of physics. The revenue generated by IOP Publishing is used by the Institute to support science and scientists in both the developed and developing world.IOP Publishing would be proud to continue representing publishers on the Board of Crossref and further its work to solve industry-wide problems and help shape the future of research.

### Personal statement <br>
Graham McCann is currently Chair of Crossref’s Membership and Fees Committee, which has responsibility for reviewing the value that members receive from Crossref services.  He has more than 25 years’ experience in STM publishing, initially in editorial development and then in product management. Currently, Graham manages IOP Publishing’s electronic content and customer-facing platforms for journals and books. Graham is involved in a number of new industry initiatives and integrations. As a continuing member of the Board, he would be particularly keen to support a refresh of Crossref’s core DOI registration technologies. With his mix of publishing and technical experience and expertise, Graham is well placed to help advise Crossref on development of their services and he remains committed to Crossref’s goal of supporting both researchers and science as a whole.

***

{{% imagewrap right %}} <img src="/images/blog/reshma.JPG" alt="Reshma Shaikh, Springer Nature" width="300px" /> {{% /imagewrap %}}

<a id="springer"></a>
## Reshma Shaikh, Springer Nature, UK<br>
Springer Nature is one of the founding members of Crossref and has been a very active and loyal board member since. We understand that Crossref needs tangible support from the membership and as a large organization we take the responsibility to deliver such support. We admire the role Crossref plays in both operating the basic infrastructure of content linking for academic communities while also playing a leading role in the development of new identifier and metadata initiatives. Supporting innovative projects to (further) develop open science is a major reason for Springer Nature to be active in Crossref. Springer Nature is committed to work together with Crossref and its membership to implement new processes and functionalities.  

As a network of 12,667 members from 118 countries, Crossref represents a truly global movement to ensure the world’s knowledge is understood and consumed within the context it was intended. The organisation’s success and increasing impact derive from its commitment to openness and transparency while staying true to its original mission of metadata at the center of all it does.  

The world of academic publishing is currently going through the largest disruption since the invention of the Open Access business model. The use of technology and the ubiquity of startups in this space will continue to challenge the status quo for the foreseeable future. In these times, organisations such as Crossref, with an established reputation as one of the original pan publishing organisations and one that is digital by birth, have a responsibility to help shepherd the industry as it goes through its transformation, while also ensuring it keeps itself relevant. The vision of Metadata 2020 and the desire to ensure Funders are able to really understand the impact of their funding, are important goals that require strong execution and oversight.  

As a technology transformation specialist, I have used Agile and Lean principles, combined with bundles of creativity to persuade companies to try things differently and embrace Digital. While at Springer Nature, I have directed large automation programmes, championed innovation and led departments of 170+ people over three different locations, set up digital and data capabilities and reinvented an innovative global content delivery platform.  Having worked in North America, Europe and Asia and with an Asian background, I believe that I can offer the board a diversity of background, knowledge and experience.  

***

{{% imagewrap right %}} <img src="/images/blog/stuart.jpg" alt="Stuart Taylor, The Royal Society" width="250px" class="img-responsive" /> {{% /imagewrap %}}

<a id="royal"></a>
## Stuart Taylor, The Royal Society, UK<br>
The Royal Society is the national academy of science for the UK. We provide science policy advice to government, we recognise excellence with Fellowships, medals and awards and we fund research. We are also a publisher of journals and although modest in scale (ten journals) we launched the world’s first science journal in 1665.   Our publishing is financially strong and we are entirely independent of any commercial publishing organisation. We are very active in the learned society publishing sector and have a seat at the table at many of the key groups and committees. We are in close contact with many of the learned society publishers and we understand and share the issues and challenges they see in the evolving publishing system. Most recently we have been instrumental in setting up the Society Publishers’ Coalition in response to Plan S. Our journal publishing workflow is based on full text JATS XML, we use a continuous publication model and were the first publisher to make ORCID iD mandatory for submitting authors.   We seek to adapt and innovate constantly particularly in terms of open science. Two of our ten journals are fully open access with CC-BY licence, we operate open peer review on four journals, we have mandatory open data on all journals, we permit text and data mining for both commercial and non-commercial purposes and we support zero embargo green open access. We have been strongly supportive of Crossref from the outset and we use Crossmark, funder registry IDs and Crossref similarity checking. We are also participants in the i4OC open references project. We also support preprints by encouraging our authors to share early versions of their work and by appointing a dedicated preprints editor for our flagship biology journal.

### Personal statement <br>

Dr Stuart Taylor is the Publishing Director at the Royal Society. He has responsibility for the Royal Society’s publishing operation which consists of a staff of 30 who publish the Society’s ten journals. He joined the Society in 2006 after working as a Publisher at Blackwell Science (now Wiley) in Oxford where he was responsible for postgraduate book and journal acquisitions in clinical medicine. He is a keen advocate of open science and believes that the scholarly communication system should genuinely serve science and do so far more effectively and efficiently than it does at present. He is a member of FORCE11, is also on the Board of Directors of the Open Access Scholarly Publishers’ Association (OASPA) and works in several other open science groups. He is a strong supporter of cross-stakeholder collaborative solutions in general and Crossref in particular as the single most ambitious and successful of them. He would welcome the opportunity to input into Crossref’s strategy and decision making. He feels it is essential that the voice of the smaller and scholar-led publishers is represented as they face some very distinct challenges as the publishing landscape continues to evolve. Crossref is in a unique position with the trust, breadth of influence and technical competence to continue be a key actor at the centre of the rapidly developing scholarly communication system.

He has an MA in chemistry and a DPhil in psychopharmacology from the University of Oxford, and has published 25 peer reviewed scientific papers in neuroscience. He has also published a number of articles on journal publishing and scholarly communication. In 2015, he organised a four day conference as part of the Royal Society’s celebration of 350 years of science publishing entitled The Future of Scholarly Scientific Communication. He has served on the Crossref Board previously (2010 - 2013).

ORCID iD: http://orcid.org/0000-0003-0862-163X

***

{{% imagewrap right %}} <img src="/images/blog/todd.jpg" alt="Todd Toler, Wiley" width="200px" /> {{% /imagewrap %}}

<a id="wiley"></a>
## Todd Toler, Wiley, USA<br>
Founded in 1807, John Wiley and Sons (Wiley) publishes on behalf of more academic, professional and scholarly societies and associations than any other publisher.  

Wiley was a founding member of Crossref, as was Blackwell, which merged with Wiley in 2007. We see Crossref as a core component of the research publishing ecosystem and are committed to helping Crossref as an organization meet its goals and objectives. Overall, Wiley strives to take a leadership role in scholarly publishing, and through initiatives such as CHORUS and ORCID (among others) we work with stakeholders across the industry to help develop infrastructure that supports researchers and scholarly publishing globally.  

We are a global company, with major publishing centres in in the United States, the United Kingdom, Germany, Australia, China, and Japan. Wiley colleagues are actively engaged in numerous industry membership associations, such as the International Association of Scientific, Technical and Medical Publishers (STM), the Association of Learned and Professional Society Publishers (ALPSP), the Publishers Association (PA) and the American Association of Publishers (AAP). Wiley is also a co-chair of Project DARE, which is working to address skills gaps in the Asia-Pacific region.  

We recognize that the market for scholarly publishing is changing rapidly, creating opportunities to develop new ways to describe, share, and disseminate research advances; we are committed to supporting the transition to Open Science in all manifestations, and to managing the transition in a sustainable manner for all stakeholders.  

### Personal statement <br>
I am currently Vice President, Product Strategy & Partnerships, for Wiley’s Research business, where I work across business units to help develop the strategic direction for Wiley’s products and services for researchers.  In addition, I also represent Wiley in strategic external relations with industry and trade groups, government organizations and scientific communities.  

I am currently Wiley’s alternate on the STM board, am a founding member of RA-21 (working towards standards of seamless, secure access to scholarly content) and frequently meet with industry peers on a wide variety of matters related to improving scholarly communications standards and infrastructure.   

I have been at Wiley for 12 years, originally joining the company as its first ever Director of User Experience in 2007.  I’ve also held the titles of Director & Publisher, Wiley Online Library and Vice President of Digital Product Management.   My background and expertise is in the field of interaction design, instructional design, product management, and digital strategy.  I love working in the field of research communication and feel we are only at the beginning of what’s possible for the field.   I am particularly passionate about the future of linked data and reproducible science, and am currently working on a next-generation journal workflow for Wiley that is committed to open data, open web standards, and open annotation.  

Crossref is the most successful example in our industry of what’s possible through collaboration and infrastructure sharing.   It provides vital technology and serves as an important forum to navigate a rapidly evolving technology landscape for content providers and other stakeholders across a wide variety of business models.  I would be honored to serve on the board, and my alternate would be Duncan Campbell, Senior Director, Global Sales Partnerships, who has previously sat on the Crossref board representing Wiley since 2015.  


***
