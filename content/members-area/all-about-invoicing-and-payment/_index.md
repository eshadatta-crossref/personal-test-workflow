+++
title = "All about invoicing and payment"
date = "2021-05-13"
draft = false
author = "Amanda Bartell"
identifier = "members-area/all-about-invoicing-and-payment"
rank = 4
weight = 140401
aliases = [
    "/education/become-a-member/how-invoicing-works/",
		"/education/become-a-member/how-invoicing-works",
    "/education/metadata-stewardship/maintaining-your-crossref-membership/all-about-invoicing-and-payment",
    "/education/metadata-stewardship/maintaining-your-crossref-membership/all-about-invoicing-and-payment/",
    "/membership/billing/",
    "/membership/billing/",
    "/billing-faqs/",
    "/billing-faqs",
    "/documentation/metadata-stewardship/maintaining-your-crossref-membership/all-about-invoicing-and-payment",
    "/documentation/metadata-stewardship/maintaining-your-crossref-membership/all-about-invoicing-and-payment/",
    "/documentation/metadata-stewardship/maintaining-your-crossref-membership/payment_portal",
"/documentation/metadata-stewardship/maintaining-your-crossref-membership/payment_portal/",
"/members-area/payment_portal/",
"/members-area/payment_portal/"
]
+++
As a not-for-profit membership organization, we are sustained by [fees](/fees/). We have a duty to remain sustainable and manage our finances in a responsible way. Financial sustainability means we can keep the organization afloat and keep our dedicated service to scholarly communications running.

* As a member (or a Sponsor who represents members), you'll receive your annual membership fee invoice each January. If you participate in Similarity Check, this invoice will contain your Similarity Check annual service charge, and you'll also receive a separate Similarity Check document-checking invoice for the documents you've checked in the previous year. We invoice for content registration on a quarterly basis.
* If you are a member through a sponsor, all Crossref billing goes to your Sponsor.
* If you are a service provider or use a paid-for metadata retrieval service, you'll receive your annual invoice in January too.

We send out invoices by email to the billing contact on your account - please do update us immediately if you need to [change your billing contact](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001618152). Invoices have a due date of net 45 days, and you can always view both paid and unpaid invoices in our payment portal. You'll need login credentials for the payment portal - these are different from the account credentials that you use to register your content with us. The billing contact for each member account is automatically sent credentials for the portal, and you can [request payment portal login credentials](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360003343631) for others at your organization too.

When you receive invoices from us you will see a *pay now* link in the body of the email. This link takes you to our payment portal where you can pay using a credit/debit card or by ACH. You'll also be able to see any outstanding invoices in one central place. If you aren't able to pay using the payment portal, you have the option to pay by wire or check from a US bank.

An important part of our accounting process is the automated invoice reminder schedule. We send out automated reminders to the billing contact 7 days before the due date, and then 15 days past the invoice due date. If you still have unpaid invoices after this, we'll send a further email to all the contacts we hold on your account (Billing, Primary, Voting, Technical and Metadata Quality) to notify you that your service is at risk of suspension. If your invoices remain unpaid after this, we suspend your account and remove your access to register content.

If an account becomes suspended for non-payment, then your membership of Crossref becomes at risk of being ‘terminated’. If your membership is terminated, you need to contact our membership specialist to discuss whether you can rejoin Crossref. You would need to pay any outstanding invoices before you can re-apply.

We understand there are many factors that can make prompt payment a challenge for some people: international transfer delays or fees, funding for your publishing operations may end, change of contacts, problems receiving our emails, etc. We really don't want to see you go, so our billing team works closely with members to make sure they can pay their invoices promptly. We send numerous notifications/reminders before suspension or termination takes place, and we can always be reached at [billing@crossref.org](mailto:billing@crossref.org) for any invoice inquiries you may have - please include your account name, prefix, and invoice number.


## Tips for smoother payments<a id='00545' href='#00545'><i class='fas fa-link'></i></a>

Here are some things you can do to help speed up, or simplify payments:

* Pay with a credit card, using our online payment portal. This is fast, convenient, and lower in fees.
* Always reference an invoice number on the payment to ensure that it’s applied to your account efficiently.
* Be sure to make `billing@crossref.org` a ‘safe’ email address, so that you receive our invoices and reminders.
* Always keep us up-to-date with any contact changes at your organization, to ensure that we have accurate information for invoicing and other communication.
* We recommend giving us a generic email address for the billing contact on your account (such as `accounts@publisher.com`) rather than the email address for one person. This means that if one person leaves your billing team, invoices can still get through to your organization.

## Billing FAQs<a id='00462' href='#00462'><i class='fas fa-link'></i></a>

Here are the answers to some frequently asked questions about:

* [Initial subscription order for your first year of membership](#00537)
* [General billing processes](#00622)
* [The payment portal](#10123)
* [Content registration invoices](#00661)
* [Similarity Check invoices](#00542)
* [Unpaid invoices and suspensions](#00465)
* [The Global Equitable Membership Program](#12345)


### Initial subscription order for your first year of membership<a id='00537' href='#00537'></i></a>

{{% accordion %}}
{{% accordion-section "The subscription order is for less than I expected - is this a mistake?" %}}

Don’t worry, this isn’t a mistake.

When you first apply to join Crossref, you’ll receive a pro-rated Subscription Order for the remainder of that calendar year. So depending on when you join, you’ll only pay for the remaining months of that year.

The calculation will also reflect whether you apply in the first or second half of the month. For example, if you join before the middle of July (15th of the month), your membership order will be for six months. If you join after the middle of July, your membership order will be for five months.

Then, in the following January, you’ll receive an invoice for the whole of that calendar year, and will continue to receive invoices every subsequent January.
{{% /accordion-section %}}

{{% accordion-section "Can you change 'subscription order' on the document to 'subscription invoice'?" %}}

Unfortunately, no, we cannot change the document type. We have hundreds of organizations that apply for membership with good intentions, but then decide that timing, or other factors, delay them from completing the joining process. For this reason, we issue a *Subscription Order* instead of a *Subscription Invoice*, as an *order* more accurately reflects the status of the joining process in our accounting system.
{{% /accordion-section %}}
{{% /accordion %}}

### General billing processes<a id='00622' href='#00622'></i></a>

{{% accordion %}}
{{% accordion-section "When will I be billed?" %}}
#### Members

There are two different types of invoice that all members receive from us - your annual membership fee invoice, and your content registration fee invoices. If you participate in Similarity Check, there's a third invoice you'll receive - your Similarity Check document checking fees invoice.

If you are a member of Crossref through a Sponsor, your Sponsor will pay these invoices on your behalf. They may charge you for their services, so you need to discuss their invoicing schedule with them.


**Your annual membership fee invoice**
This allows you to remain a member of our organization and take advantage of our services and the reciprocal relationship with other members. Members receive this invoice in January each year to cover their membership for that year - so in January 2021 you'll receive a membership invoice for 2021. If you participate in Similarity Check, your annual service fee for Similarity Check will also be included in this invoice.

**Content registration invoices**
There's a charge for each item you register with Crossref, and we invoice for this in arrears - this means that we send you the invoice after you've registered the content, so we know exactly how much to charge.

These invoices are usually sent out on a quarterly basis and cover the deposit fees for the content you registered with us during the previous quarter:

* In April, you’ll receive an invoice for the content you registered in the first quarter of the year (January - March)
* In July, you’ll receive an invoice for the content you registered in the second quarter of the year (April - June)
* In October, you’ll receive an invoice for the content you registered in the third quarter of the year (July - September)
* In January you’ll receive an invoice for the content you registered in the fourth quarter of the previous year (October - December)

However, you may not receive an invoice every single quarter. If your content registration charges are below USD 100 for a quarter, those charges will roll forward to the next quarter. This is to avoid members having to pay lots of smaller invoices which may incur international charges.

These charges don't roll on past a full year though - so even if your total content registration fees haven't hit USD 100 by the end of the year, you'll receive a content registration invoice in January to cover all your content registration fees for the previous year.

To put it another way - you’ll be invoiced when your total charges exceed USD 100, or in the last quarter of the year, whichever occurs first.

**Similarity Check document checking invoices**
If you participate in the Similarity Check service, you'll receive an extra invoice each January to cover the fees for all the documents you've checked in the previous year. Your first 100 documents are free though, so if you check fewer than 100 documents, you won't receive an invoice.

#### Metadata subscribers
You'll receive your annual subscription invoice each January.

#### Service providers
You'll receive your annual invoice each January.
{{% /accordion-section %}}
{{% accordion-section "What are the payment terms - how long do I have to pay?" %}}

Payment is due 45 days after the date of the invoice.
{{% /accordion-section %}}
{{% accordion-section "What are your current fees?" %}}

Our current fees are always available on our [fees page](/fees).
{{% /accordion-section %}}
{{% accordion-section "How do I pay - what are the payment methods?" "payment-methods" %}}

{{< snippet "/_snippet-sources/how-to-pay-us.md" >}}

{{% /accordion-section %}}
{{% accordion-section "Can you make a change to my invoice after I’ve received it?" %}}

**What we can change**
If the invoice hasn’t yet been paid, we can make the following changes:
* We can update your organization name or address if this has changed.
* We can update the detail if there’s an error on the invoice. For example, if you’ve been charged for current content when you should have been charged for backfile content (due to an error in registering the publication date), we can amend the invoice once you’ve updated your metadata.

**What we can’t change**
* We can’t change dates and due dates, so please pay the invoices as soon as you receive them.
* We can’t add wire fees into the invoice as they aren't a standard charge for everyone - only for those who use wire transfer as a payment method. Wire fees are USD 35, so you’ll need to add this to your total if you’re paying by wire transfer.
{{% /accordion-section %}}
{{% accordion-section "Will any tax be added to my invoice?" %}}

No tax will be added to your invoice - there's no tax on membership fees or any of the services we offer.
{{% /accordion-section %}}
{{% /accordion %}}

### The payment portal <a id='10123' href='#10123'></i></a>
The payment portal gives members a way to view, download and pay their invoices.

{{% accordion %}}
{{% accordion-section "How do I get access to the payment portal?" %}}
The billing contact on each member account is automatically set up with access to our payment portal. They are sent an email with a link to set their password when their organization joins. Once this password is set, the billing contact will login to the portal using their email address as their username.

(Please note - this email address and password for the payment portal is separate and different from the credentials you'll use to access our other systems to register your content).

You can request that others at your organization have access to the payment portal too by [contacting our billing team](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360003343631). This request will need to come from one of the key contacts that we hold for the account. These other contacts will then be sent their own email to set their password, and once this is set, they will also access the portal using their own email address and the password - different contacts at your organization will have their own separate set of credentials for the payment portal.

{{% /accordion-section %}}
{{% accordion-section "Where do I find my invoices in the payment portal?" %}}
On the menu on the left hand side of the portal you'll see the following:

- Open invoices - these are your unpaid invoices
- Paid invoices - these are the invoices that you've paid in the past

{{% /accordion-section %}}
{{% accordion-section "Can I find the invoices that I've already paid in the portal?" %}}
Yes you can. In the menu on the left hand side of the portal you will have an option that says "Paid invoices". Click here to see the invoices that you have paid in the past.

{{% /accordion-section %}}
{{% accordion-section "How do I pay for invoices in the portal?" %}}
You can use the portal to pay using your credit card or most major debit cards. If you are located in the US, you can also set up an ACH in the portal.

{{% /accordion-section %}}
{{% accordion-section "How do I reset my password in the payment portal?" %}}
If you've forgotten your password or you need to reset it in future, you can do this by clicking on the "forgot password" link on the [portal homepage](https://portal.tpro3.com/CrossRef/Customer_Portal). This will send an email to you with a link to reset your password.

{{% /accordion-section %}}
{{% accordion-section "I've received a link to reset my password, but it only lasts for 4 hours and it's expired" %}}

Don't worry - you can just request another link by clicking on the "forgot password" link on the [portal homepage](https://portal.tpro3.com/CrossRef/Customer_Portal). This will send another email to you with a link to reset your password.

{{% /accordion-section %}}
{{% accordion-section "I thought I used account ID for my username, not my email address?" %}}

In our old payment portal, everyone at a member organization shared one set of credentials, and the username  was the account ID.

However, in our new payment portal, each person at each member organization will access the portal using different credentials based on their personal email address. This will keep things secure, and if you forget your password, you can request that a reset link is sent to your inbox using the portal - you don't have to ask our billing team to send one to you. This should make things much faster for you.

{{% /accordion-section %}}

{{% accordion-section "Can I update our organization billing address in the portal?" %}}

No. In the portal, you can only change details related to your payment card. If you want to update the billing address that appears on your invoice, you'll need to [contact our membership team](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001618152).

{{% /accordion-section %}}

{{% /accordion %}}

### Content registration invoices <a id='00661' href='#00661'></i></a>

{{% accordion %}}
{{% accordion-section "Can you explain my content registration invoice to me?" %}}

There’s a charge for each item you register with Crossref and we invoice for this in arrears - this means you receive the invoice after you’ve registered the content so we know exactly how much to charge.

These invoices are usually sent out on a quarterly basis, and cover the deposit fees for the content you’ve registered with us during the previous quarter. However, we do sometimes roll smaller charges on to the next quarter, so you may not receive an invoice every single quarter - and the next quarter you might find charges in your invoice from previous quarters.

The information on your Content Registration invoice will look something like this:

| Item | Description | Unit | Quantity | Unit price | Amount |
|--- |--- |--- |--- |--- |--- |
| 40801 | CY Journal <br> 08/2020: 10.5555 : CY Journal article (users: aelt) | EA | 30 | $1.00 | $30.00 |
| 40802 | BY Journal <br> 08/2020: 10.5555: BY Journal article (users: aelt) | EA | 10 | $0.15 | $1.50 |
| 40816 | CY Book Titles <br> 08/2020: 10.5555: CY Book title (users: aelt) | EA | 12 | $1.00 | $12.00 |
| 40801 | CY Journal <br> 09/2020: 10.5555 : CY Journal article (users: aelt) | EA | 27 | $1.00 | $27.00 |
| 40802 | BY Journal <br> 09/2020: 10.5555: BY Journal article (users: aelt, fort) | EA | 30 | $0.15 | $4.50 |
|  |  |  |  | Sub total | $75.00 |

You’ll see there are different lines on the invoice, and a total at the end.

Your content registration fees are split out onto separate lines on your invoice by:
* Prefix
* Month the content was registered
* Content type
* Whether the content is current (CY) or backfile (BY).

This is because there are different charges for different content types, and different charges depending on whether the publication date of the content is current or backfile. Learn more about [content registration fees](/fees/#content-registration-fees).

You can also see on the invoice which role or roles were used to register the content you’re being charged for.

Here's a bit more information about each section of the invoice.

{{< figure-linked
	src="/images/documentation/invoice-contentreg-1.png"
	large="/images/documentation/invoice-contentreg-1.png"
	alt="This part of the invoice shows the type of content that this charge relates to, and whether the content is current or backfile. In this example, the charge is for current year (CY) journal articles."
	title="Content registration invoice detail 1"
	id="image120"
>}}

This part of the invoice shows the type of content that this charge relates to, and whether the content is current or backfile. In the example above, the charge is for current year (CY) journal articles.

{{< figure-linked
	src="/images/documentation/invoice-contentreg-2.png"
	large="/images/documentation/invoice-contentreg-2.png"
	alt="This part of the invoice shows the month that this content was registered. In this example, this content was registered in August 2020."
	title="Content registration invoice detail 2"
	id="image121"
>}}

This part of the invoice shows the month that this content was registered. In the example above, this content was registered in August 2020.

{{< figure-linked
	src="/images/documentation/invoice-contentreg-3.png"
	large="/images/documentation/invoice-contentreg-3.png"
	alt="This part of the invoice shows which prefix the content was registered with. In this example, the content was registered under the prefix 10.5555."
	title="Content registration invoice detail 3"
	id="image122"
>}}

This part of the invoice shows which prefix the content was registered with. In the example above, the content was registered under the prefix 10.5555.

{{< figure-linked
	src="/images/documentation/invoice-contentreg-4.png"
	large="/images/documentation/invoice-contentreg-4.png"
	alt="This part of the invoice shows which role was used to register the content. In this example, the role was 'aelt'."
	title="Content registration invoice detail 4"
	id="image123"
>}}

This part of the invoice shows which role was used to register the content. In the example above, the role was *aelt*.

{{< figure-linked
	src="/images/documentation/invoice-contentreg-5.png"
	large="/images/documentation/invoice-contentreg-5.png"
	alt="Sometimes more than one role has been used to register content. In this example, both 'aelt' and 'fort' have been used."
	title="Content registration invoice detail 5"
	id="image124"
>}}

Sometimes more than one role has been used to register content. In the example above, both *aelt* and *fort* have been used.

{{< figure-linked
	src="/images/documentation/invoice-contentreg-6.png"
	large="/images/documentation/invoice-contentreg-6.png"
	alt="All prices are in USD, and we can only accept payment in USD."
	title="Content registration invoice detail 6"
	id="image126"
>}}

All prices are in USD, and we can only accept payment in USD.
{{% /accordion-section %}}
{{% accordion-section "Why haven’t I received a content registration invoice this quarter?" %}}

We send invoices for the metadata you register with us on a quarterly basis. However, if the amount comes to less than USD 100, we roll it on to the next quarter. If you haven’t reached USD 100 in fees by the last quarter of the year, we send out an invoice anyway.

This is to avoid members having to pay lots of ‘small’ invoices, which may incur international charges.
{{% /accordion-section %}}
{{% accordion-section "What do the *CY* and *BY* stand for on my content registration invoice?" %}}

*CY* stands for current content (Current Year), and *BY* stands for backfile content (Back Year). You’re charged a different amount depending on the content type you’re registering, and also whether the content is current (CY) or backfile (BY).

*Current content* is anything registered with us with a publication date in the current year, or up to two years previously. For example, in 2022, current content is anything with a publication date in 2022, 2021 or 2020. In 2023, this will change to anything with a publication date in 2023, 2022 or 2021.

Backfile content is anything registered with us with a publication date older than this. So in 2022, backfile content is anything published in 2019 or earlier. In 2023 this will become anything published in 2020 or earlier.
{{% /accordion-section %}}
{{% accordion-section "Why was I charged the CY fee for BY articles?" %}}

[Content Registration fees](/fees#content-registration-fees) differ according to whether the content you register is *current* (published during this year or the previous two years) or *backfile* (older than that).

A record is determined to be either *backfile* or *current* based on the publication date in your metadata. If you have different dates for print and online (for example, if you're registering archival content), then we look at the print date.

If you use our web deposit form, the system looks at the information you’ve entered into the *publication date* field. If you deposit XML directly with us, the system looks at the date in the `<publication_date>` element. And we look at each individual item separately—so even if you’ve put a publication date at the journal level, you still need to put it at the journal article level too.

If you’ve been charged 'current' fees for content that is actually backfile, it’s probably because the wrong date was put in the publication date field. We have had instances where members have accidentally put the date they registered the content into that field, rather than the date of publication.

You can fix this by [updating your metadata](/documentation/register-maintain-records/maintaining-your-metadata/updating-your-metadata) with the correct publication dates. [Please let us know](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001642691) as soon as you’ve done this so we can provide you with an amended invoice.
{{% /accordion-section %}}


{{% accordion-section "It's January, and I've just received a content registration invoice. I haven't received one before - why have I received one now?" %}}
There are two sets of fees associated with Crossref membership - the annual membership fee (which covers your membership) and the content registration fees (which are a one-off fee for each item you register with us). The membership invoice is sent out at the beginning of each year to cover the forthcoming year, and the content registration invoices are sent out quarterly in arrears.

However, you won't necessarily receive a content registration invoice every single quarter. If the amount of content you register in a quarter comes to less than USD 100, we roll it on to the next quarter. This is to avoid members having to pay lots of ‘small’ invoices, which may incur international charges. But even if you haven’t reached USD 100 in fees by the last quarter of the year, we send out an invoice anyway.  This means that if you've only registered a small number of DOIs in your first year, you won't receive an invoice until right at the end of the year, in your Q4 invoice which is sent out the following January. So you might go a full year before you receive a content registration invoice from us.

There are also a very small number of members who might not receive a content registration invoice from us for a few years. If you've only registered a couple of DOIs, we won't send you an invoice even in Q4. So if you've had a few years of registering just a tiny handful of DOIs, you might not receive a content registration invoice for a few years.  

{{% /accordion-section %}}
{{% /accordion %}}

### Similarity Check invoices <a id='00542' href='#00542'></i></a>

{{% accordion %}}
{{% accordion-section "When will I receive my Similarity Check invoices?" %}}
There are two sets of fees for the Similarity Check service - the annual service fee, and document checking fees.

**Annual service fee**<br/>
This will be included in the annual membership invoice you receive each January.

**Document checking fees**<br/>
You will be sent this invoice in January each year.

{{% /accordion-section %}}
{{% accordion-section "Why doesn't my Similarity Check document checking invoice exactly match the number of submissions/documents I've checked in iThenticate?" %}}

Users of our Similarity Check service receive an invoice each January for the documents they've checked in the previous year. The Similarity Check administrator for each organization can monitor their spend throughout the year by checking the reports section of the iThenticate platform (under the [*Manage Users*](/documentation/similarity-check/ithenticate-account-setup/manage-users#00577) tab).

However, sometimes you may see a difference between the number of documents that you're invoiced for and the number that the report in iThenticate tells you that you've checked. There are a few possible reasons for this.

**Documents above a certain size are considered more than one document**

For billing purposes, a single document is considered anything of 25,000 words or fewer.

So if you check a document of 25,001-50,000 words, it will be considered 2 document checks. If you check a document of 50,001-75,000 words, it will be considered 3 document checks. And so on.

Check the 'documents' column in the iThenticate report and not the 'submissions' column as you are invoiced for the number of documents checked and not files submitted.

**No charge for accidental duplicates**

If you accidentally check the same document several times, we treat this as a duplicate and don't charge you for it. This includes any documents with exactly the same filename and exactly the same Similarity Score that are submitted within the same 24 hour period. The 'Manage Users' report in iThenticate isn't able to detect this but these are detected and removed from your invoice before we send it.

This means that you may see slightly fewer document checks on your invoice than you see in your iThenticate report.

**Your first 100 documents are free of charge**

Your first 100 documents are free of charge, so you'll see 100 fewer document checks on your invoice than you see in your iThenticate report.
{{% /accordion-section %}}
{{% /accordion %}}

### Unpaid invoices and suspensions <a id='00465' href='#00465'></i></a>

{{% accordion %}}
{{% accordion-section "My service has been suspended due to unpaid invoices - can you extend the payment deadline?" %}}

Unfortunately not. A suspension is not a termination of your membership, it just temporarily suspends your ability to register content with us. As soon as payment for past due balances is received, your service will be restored and you will be able to register content again.
{{% /accordion-section %}}
{{% /accordion %}}

### The Global Equitable Membership Program (GEM)<a id='12345' href='#00537'></i></a>
{{% accordion %}}
{{% accordion-section "What is the Global Equitable Membership Program?" %}}
The Global Equitable Membership (GEM) program offers relief from membership and content registration fees for members in the least economically-advantaged countries in the world. [Find out more.](/gem)
{{% /accordion-section %}}

{{% accordion-section "When does the GEM Program begin?" %}}
The GEM Program begins on 1st January 2023. Existing members who are eligible for the GEM program will not be sent a membership fee invoice for 2023, and will not be charged for content registered after 1st January 2023. They will still be responsible for any fees incurred before 1st January 2023.
{{% /accordion-section %}}

{{% accordion-section "Does the GEM Program cover all Crossref fees?" %}}
Not all. The annual membership fee and content registration fees are waived for GEM-eligible members from 1st January 2023. But participation in other paid services, such as Similarity Check and Metadata Plus, will be charged at the usual fees.
{{% /accordion-section %}}
{{% /accordion %}}

[Back to members area](/members-area)
