+++
title = "Webinars and events"
date = "2021-10-01"
author = "Rosa Clark"
image = "/images/blog/grass.jpg"
maskcolor = "crossref-yellow"
draft = false
toc = false
aliases = [
    "/01company/11conferences.html",
    "/crossmark/AboutEvents.htm",
    "/industry-events",
    "/10meetings/landing_page.html",
    "/annualmeeting/agenda.html",
    "/annualmeeting/index.html",
    "/crossref-live",
    "/webinars",
    "/webinars/",
    "/01company/webinar.html",
    "/01company/webinars.html",
    "/events/archive",
    "/events/archive/",
    "/industry-events/",
    "/conferences/",
    "/conferences",
    "/industry-events"
]
[menu.main]
parent = "Get involved"
weight = 20
rank = 3
+++

<a id="top"></a>  


We attend, speak at, and participate in a number of community events held by organizations in the scholarly community each year––held online and in person––such as conferences, workshops, hack days, and seminars   You can also attend our regular online events which are a great way to find out more about the various Crossref services.   

{{% divwrap align-right %}}<img src="/images/community-images/crossref-live.png" alt="Crossref LIVE logo" width="150px" />{{% /divwrap %}}    

We also host our events under a program known as "Crossref LIVE."  We've recently changed our emphasis from broadcast/informational to learning/ listening to our membership community.  Crossref LIVEs are free to attend and open to all who want to learn more about our services, with a program tailored to each country or segment of the community. At these events –– held online and in-person –– we also build time to have conversations with attendees and get feedback on what we're doing right and what could be better.     

---

## Upcoming events (in person/online)

<br>

Events with a {{% logo-inline %}} are wholly or partly hosted by Crossref (listed in order of event date)  

**2023**  

[UKSG](https://www.uksg.org/event/annualconference2023) - April 13 - 15 - Glasgow, UK  
[FORCE2023](https://force11.org/force2023/) - April 18 - 20   
[csv,conf,v7 2023 Conference](https://csvconf.com/) - April 19-20  
{{% logo-inline %}} [Crossref Community update:](https://outreach.crossref.org/acton/fs/blocks/showLandingPage/a/16781/p/p-006c/t/page/fm/0?utm_source=landing+page&utm_medium=website&utm_campaign=community-call-may-2023) - May 3   
[Society for Scholarly Publishing (SSP) 2023 Conference](https://customer.sspnet.org/SSP/ssp/AM23/Home.aspx?hkey=22abbe1c-7a5d-45c5-9a39-183b3535d1b6) - May 31 - June 2 - Portland, OR    
[EASE Conference 2023](https://ease.org.uk/ease-events/17th-ease-general-assembly-and-conference/) - June 1 - 3  - Instanbul  
[SSP Virtual 5K Run, Walk, and Roll Returns for Second Year #SSP5K](https://www.sspnet.org/community/news/ssp-virtual-5k-run-walk-and-roll-2023/?utm_campaign=coschedule&utm_source=twitter&utm_medium=ScholarlyPub&utm_content=SSP%20Virtual%205K%20Run%2C%20Walk%2C%20and%20Roll%20Returns%20for%20Second%20Year) - June 10  
{{% logo-inline %}} Crossref Annual Meeting and Board Election #CRLIVE23 - October 31  
[Feria Internacional del Libro de Guadalajara (Guadalajara International Book Fair)](https://www.fil.com.mx/ingles/info/info_fil.asp) - November 25 - December 3  

  
## Got a suggestion for an event (online/in-person) ?

We'd like to hear from you! You are welcome to [suggest topics](https://forms.gle/GQstGLabVPyt7TU77) we should explain/collaborate on/demonstrate that you think would be helpful as a Crossref member or to the community.  

### The annual meeting archive

Find out more about [our annual meeting](/crossref-live-annual/) or browse our [archive of annual meetings](/crossref-live-annual/archive) from 2001 through to last year. Its a real trip down memory lane!


{{< figure src="/images/community-images/break.png" width="100%">}}

## Past recordings and events  

<br>

{{% row %}}
{{% column %}}

{{% accordion %}}
{{% accordion-section "Events in Arabic" %}}

Metadata webinar series with Knowledge E - [Recordings playlist](https://www.youtube.com/playlist?list=PLe_-TawAqQj3gyf6xxlssPfxdcKfAjnUo)  
#1) [Slides](https://doi.org/10.13003/STFJ4776) - September 21, 2022    
#2) [Slides](https://doi.org/10.13003/IZLL7725) - October 5, 2022    
#3) [Slides](https://doi.org/10.13003/CNDK8684) - October 19, 2022         

- ندوه عن كيفية استخدام كروس مارك باللغة العربية | Crossmark How-To Arabic webinar - September 15, 2020 [Slides](https://www.slideshare.net/CrossRef/crossmark-howto-arabic-webinar), [Recording](https://youtu.be/sZME-mbmig4)  
- Crossref LIVE Arabic - May 11, 2020 - [Recording](https://youtu.be/McDIrEpWph4) - [Slides](https://www.slideshare.net/CrossRef/crossref-live-arabic)  
- Reference Linking & Cited-By - November 8, 2018 -  [Slides](https://www.slideshare.net/CrossRef/reference-linking-citedby-arabic-webinar), [Recording](https://youtu.be/dfNG57jkIdw)  
- Getting Started with Content Registration - September 17, 2018 [Slides](https://www.slideshare.net/CrossRef/getting-started-with-content-registration-arabic-webinar), [Recording](https://youtu.be/zavLltyFRYs)  
- Introduction to Crossref - August 6, 2018 [Recording](https://youtu.be/NGY3Ypyf1Jc)  


{{% /accordion-section %}}
{{% /accordion %}}

{{% accordion %}}
{{% accordion-section "Events in Chinese" %}}

- Crossref Live Chinese网络研讨会——Crossref简介 - October 14, 2021 - [Slides](https://www.slideshare.net/CrossRef/crossref-live-chinesecrossref-14-oct-2021), [Recording](https://youtu.be/2Uvd0pOgXXk)  

{{% /accordion-section %}}
{{% /accordion %}}


{{% accordion %}}
{{% accordion-section "Events in Indonesian" %}}

- Crossref LIVE Indonesia webinar series: Introduction to Crossref, Content Registration, The Value of Crossref metadata - July 13 - 15 - [Online Recordings](https://www.youtube.com/playlist?list=PLe_-TawAqQj0H-vrtBLT_y0BOgYqtBILm)  

{{% /accordion-section %}}
{{% /accordion %}}

{{% accordion %}}
{{% accordion-section "Events in Korean" %}}

Crossref LIVE Korea - June 17 2020 - Online (in Korean) - [Recording](https://youtu.be/pYSzwjFgHrA) - [Slides](https://www.slideshare.net/CrossRef/crossref-live-korea-17-june-2020)      

{{% /accordion-section %}}
{{% /accordion %}}

{{% accordion %}}
{{% accordion-section "Events in Portuguese" %}}

- Mantenha seus DOIs atualizados: A importância dos metadados (em Português) - March 17, 2022, [Recording and Slides](https://zenodo.org/record/6366369)  
- Introdução ao registro de livros no Crossref (Introduction to Registering Book Content Webinar, Brazilian Portuguese) - November 17, 2021 - [Recording and Slides](https://zenodo.org/record/5718731#.YmmarPNueug)  
- Introduction to Crossmark/Crossmark: O que é e como usar - October 19, 2020 [Recording](https://youtu.be/FEvilhMnCuU)  
- Melhores Práticas para Registro de Conteúdo/Crossref Content Registration - October 7, 2020 [Slides](https://www.slideshare.net/CrossRef/crossref-content-registration-in-brazilian-portuguese-pt1), [Recording](https://youtu.be/Zn-0Mmykc-U)  
Crossref LIVE Brazil - June 25 2020 - Online (in Portuguese) -  [Recording](https://youtu.be/Uf5iLyd7Yd4) - [Slides](https://www.slideshare.net/CrossRef/crossref-live-brazil-2020-236362696)  
- Introduction to Participation Reports webinar  - October 30, 2019 - [Slides](https://www.slideshare.net/CrossRef/crossref-participation-reports-webinar-slides-in-brazilian-portuguese), [Recording](https://youtu.be/-HM3N8CWlIo)    
- Reference Linking & Cited By webinar - April 16, 2019 [Recording](https://youtu.be/JWwvzKSHvDI)  
- Registering content and adding to your Crossref metadata - November 26, 2018 [Slides](https://www.slideshare.net/CrossRef/registering-and-adding-to-your-metadata-at-crossref-in-portuguese), [Recording](https://youtu.be/Yg5a5GsANzw)  
- Introduction to Similarity Check webinar - April 10, 2018 [Slides](https://www.slideshare.net/CrossRef/similarity-check-portuguese), [Q&A](http://bit.ly/simcheck-portuguese), [Recording](https://youtu.be/458YidNCJk4)  
- Funder Registry - in Portuguese - September 26, 2016 [Slides](http://www.slideshare.net/Crossref/funding-data-orcid-webinar-in-portuguese)  
- Getting started with Content Registration: Portuguese Webinar - September 5, 2017 - [Slides](https://www.slideshare.net/CrossRef/getting-started-with-content-registration-portuguese-webinar-introduo-ao-registro-de-contedo-webinar-em-portugus), [Recording](https://youtu.be/fY5ZxJHd17E)  


{{% /accordion-section %}}
{{% /accordion %}}

{{% accordion %}}
{{% accordion-section "Events in Russian" %}}

- Understanding Crossref reports - in Russian - April 18, 2019 [Slides](https://www.slideshare.net/CrossRef/crossref-reports-in-russian), [Recording](https://youtu.be/3HOUJPyyl6M)  
- Introduction to Crossmark - in Russian - December 6, 2018 [Slides](https://www.slideshare.net/CrossRef/introduction-to-crossmark-russian-webinar), [Recording](https://www.youtube.com/watch?v=2PRYbK0t-SY)   
- Crossref and OJS - in Russian - November 22, 2018 [Recording](https://youtu.be/VKgZIoIBTPs)  


{{% /accordion-section %}}
{{% /accordion %}}

{{% accordion %}}
{{% accordion-section "Events in Spanish" %}}

- Mantener sus DOI actualizados: la importancia de los metadatos (en español) - April 6, 2022 - [Recording and Slides](https://doi.org/10.13003/DJQZOhtWjJ)    
- Crossmark (en español) -  September 30, 2021 - [Recording](https://youtu.be/9oEwaLkuTDk)   
- Registro y actualización de contenido en Crossref / Register and update content in Crossref - October 1, 2020 [Slides](https://www.slideshare.net/CrossRef/register-and-update-content-in-crossref-in-spanish), [Recording](https://youtu.be/bzuKf4EoeH4)  
- Crossref LIVE Spanish - May 19, 2020 - [Recording](https://youtu.be/kQNwWzcWeH8), [Slides](https://www.slideshare.net/CrossRef/crossref-live-spanish-19th-may-2020)  
- Reference linking and Cited-by - in Spanish - November 7, 2018 [Slides](https://www.slideshare.net/CrossRef/reference-linking-and-cited-by-in-spanish), [Recording](https://youtu.be/eDRDglEBybM)  
- Introduction to Crossref and Content Registration - in Spanish - October 24, 2018 [Slides](https://www.slideshare.net/CrossRef/introduction-to-crossref-and-content-registration-in-spanish), [Recording](https://youtu.be/6qiIVQRxv2w)  
<a id="crossmarkspanish2021"></a>   


{{% /accordion-section %}}
{{% /accordion %}}

{{% accordion %}}
{{% accordion-section "Events in Turkish" %}}

- Crossref LIVE Turkey - February 18, 2020 - Online (in Turkish) - [Recording](https://youtu.be/D6GhGntyACg)   
- Introduction to Crossref: Turkish Seminar  November 2, 2017 [Recording](https://youtu.be/gc_MUolaBKc)   

{{% /accordion-section %}}
{{% /accordion %}}

{{% accordion %}}
{{% accordion-section "Events in Ukrainian" %}}

- [Participation Reports webinar in Ukrainian](https://www.crossref.org/webinars/#prepukrainian)

{{% /accordion-section %}}
{{% /accordion %}}

### Learn more
{{% accordion %}}

{{% accordion-section "Getting started" %}}

- Getting started as a new Crossref member - May 24, 2018 [Slides](https://www.slideshare.net/CrossRef/new-member-webinar-052418), [Recording](https://youtu.be/GAUEXizVcHw)  
- Get started with Reference Linking - May 23, 2018 [Slides](https://www.slideshare.net/CrossRef/getting-started-with-reference-linking), [Recording](https://youtu.be/WvPhmRMCICg)
- Getting started with looking up metadata - March 8, 2018 [Slides](https://www.slideshare.net/CrossRef/getting-started-with-looking-up-metadata), [Recording](https://youtu.be/9_RrWFRHEbI)  
- OpenCon Oxford 2020 [Recording](https://youtu.be/Y8nEPMUo9R0)  
- Get started with Content Registration - October 17, 2017 [Slides](https://www.slideshare.net/CrossRef/getting-started-with-content-registration), [Recording](https://youtu.be/G9mIUvXrTLc)    

{{% /accordion-section %}}
{{% /accordion %}}

{{% accordion %}}
{{% accordion-section "How-to" %}}

- Participation Reports - October 7, 2020 - [Slides](https://www.slideshare.net/CrossRef/participation-reports-webinar-october-2020-238797935), [Recording](https://youtu.be/U8VpRTBSnQ4)  
- The ins and outs of our Admin Tool - March 5, 2020 - [Recording](https://youtu.be/Csh6GRegWxw)  
- Crossmark how-to - May 15, 2018 [Slides](https://www.slideshare.net/CrossRef/cross-mark-webinar-how-to), [Recording](https://youtu.be/em0IVJf-UNo)  
- Crossref Cited-by how-to - June 13, 2017 - [Slides](https://www.slideshare.net/CrossRef/cited-by-howto), [Recording](https://youtu.be/tY7W2IWt7tE)  

{{% /accordion-section %}}

{{% /accordion %}}

{{% accordion %}}

{{% accordion-section "About our services" %}}

- Participation Reports and Reference Metadata (Indonesian time zone) - October 12 [Recording](https://youtu.be/RrxnpC-3AXo)  
- Similarity Check iThenicate v2 - October 6, 2022 [Recording](https://youtu.be/3SgoRhGSc50)    
- Introduction to Similarity Check - April 30, 2020 [Slides](https://www.slideshare.net/CrossRef/introducing-crossref-similarity-check), [Recording](https://youtu.be/7GV95FzbjmM)  
- Research infrastructure with and for funders - September 6, 2019 [Recording](https://youtu.be/va9J3XVYFfw)  
- Crossref Cited-by - June 8, 2017 [Slides](https://www.slideshare.net/CrossRef/crossref-cited-by), [Recording](https://youtu.be/31u6Iz_ENC8)  
- Introduction to Crossmark - November 21, 2017 [Slides](https://www.slideshare.net/CrossRef/introductiontocrossmarklastest), [Recording](https://youtu.be/mHMoQWp_cM4)  
- Content Registration maintaining metadata - May 17, 2017 [Slides](https://www.slideshare.net/CrossRef/content-registration-maintaining-metadata), [Recording](https://youtu.be/xwbghcX7wW0)  
- Funding data and the Funder Registry - April 4, 2017 [Slides](https://www.slideshare.net/CrossRef/funding-data-and-funder-registry), [Recording](https://youtu.be/NyBufFcN2nA)  
- Similarity Check Members update -  March 02, 2017 [Recording (must register first to view)](https://event.on24.com/eventRegistration/EventLobbyServlet?target=reg20.jsp&referrer=&eventid=1371000&sessionid=1&key=B41D99989415D914443947C706FFED1F®Tag=&sourcepage=register)  
- Crossmark update - February 23, 2017 [Slides](https://www.slideshare.net/Crossref/crossmark-update-webinar), [Recording](https://youtu.be/qkwN49lgH9k)  
- Similarity Check update - September 20, 2016 [Slides](http://www.slideshare.net/Crossref/crossref-similarity-check-update-webinar-sept-2016)  

{{% /accordion-section %}}
{{% /accordion %}}

{{% accordion %}}

{{% accordion-section "Other" %}}

- Making the world a PIDder place: it’s up to all of us! (Co-hosted by DataCite, Crossref, ORCID & ROR)  - Sep 22, 2021 [Recording](https://www.youtube.com/watch?v=D9Mtqb64OEk), [Slides](https://www.slideshare.net/CrossRef/working-with-ror-as-a-crossref-member-what-you-need-to-know)    
- How to manage your metadata with Crossref - November 18, 2020 [Slides](https://www.slideshare.net/CrossRef/how-to-manage-your-metadata-with-crossref), [Recording](https://youtu.be/Muy-vrupsnY)   
- Participation Reports webinar - October 7, 2020 [Slides](https://www.slideshare.net/CrossRef/participation-reports-webinar-october-2020-238797935), [Recording](https://youtu.be/U8VpRTBSnQ4)  
- Finding your way with Crossref: getting started & additional services -  September 2, 2020 [Webinar slides](https://www.slideshare.net/CrossRef/finding-your-way-with-crossref-getting-started-additional-services), [Recording](https://youtu.be/KooOKVfsyPI)   
- Not just identifiers: why Crossref DOIs are important - September 2, 2020 [Slides](https://www.slideshare.net/CrossRef/not-just-identifiers-why-crossref-dois-are-important), [Recording](https://youtu.be/TVWbzb6zJV0)  
- Getting started with books at Crossref - July 22, 2020 [Slides](https://www.slideshare.net/CrossRef/getting-started-with-books-at-crossref), [Recordibng](https://youtu.be/pQPKlhJfzIM)  
- Introduction to ROR - April 29, 2020 [Recording](https://youtu.be/W61JMsC3Dho)  
- Proposed schema changes - have your say - January 2, 2020 [Slides](https://www.slideshare.net/CrossRef/proposed-schema-changes-have-your-say), [Recofding](https://youtu.be/ytBA2Ygq48I)  
- Using ORCID in publishing workflows - September 16, 2019 [Recording](https://youtu.be/Um0sc06OR-c)  
- Crossref and DataCite joint data citation webinar - February 4, 2019 [Slides](https://www.slideshare.net/CrossRef/crossref-datacite-joint-data-citation-webinar), [Recording](https://youtu.be/U-k6cuNvLms)  
- Where does publisher metadata go and how is it used? - September 11, 2018 [Webinar Slides: Laura Wilinson](https://www.slideshare.net/CrossRef/crossref-webinar-laura-wilkinson-where-does-publisher-metadata-go-and-how-is-it-used-110918), [Webinar Slides: Anna Tolwinska](https://www.slideshare.net/CrossRef/crossref-webinar-anna-tolwinska-crossref-participation-reports-metadata-091118), [Webinar Slides: Stephanie Dawson](https://www.slideshare.net/CrossRef/crossref-webinar-stephanie-dawson-sciencopen-metadata-091118/CrossRef/crossref-webinar-stephanie-dawson-sciencopen-metadata-091118), [Webinar Slides: Pierre Mounier](https://www.slideshare.net/CrossRef/crossref-webinar-pierre-mounier-where-does-publisher-metadata-go-and-how-is-it-used-91118), [Recording](https://youtu.be/RJhDHWhFFAs)  
- Maintaining your metadata - April 24, 2018 [Slides](https://www.slideshare.net/CrossRef/maintaining-your-metadata-95497150), [Wistia Recording](https://crossref.wistia.com/medias/bl0vdzzes1), [Recording](https://youtu.be/oR4d5YMPEFM)  
- Preprints and scholarly infrastructure - January 30, 2017 [Slides](http://www.slideshare.net/Crossref/preprints-scholarly-infrastructure-013017), [Recording](https://youtu.be/cFpuQ3lwpME)  
- Beyond OpenURL: Making the most of Crossref metadata - July 12, 2017 [Slides](https://www.slideshare.net/CrossRef/beyond-openurl), [Recording](https://youtu.be/DmrFnLFu6iU)  

{{% /accordion-section %}}
{{% /accordion %}}


{{% /column %}}
{{% column %}}

{{% accordion %}}
{{% accordion-section "Past LIVEs" %}}

{{% logo-inline %}} Crossref Annual Meeting & Board Election #CRLIVE22 - October 26, 2022 -  [Recording](https://www.youtube.com/watch_popup?v=csy4mf8QNtY), [Wistia Recording](https://crossref.wistia.com/medias/alpbj23mry?wtime=0s),  [Slides](https://docs.google.com/presentation/d/1CWoueRlXDBr764zDxw3docHDWosD8UCqlXzE9zZxyRI/preview?slide=id.gf67b34ab3e_0_8), [Slides PDF](https://www.crossref.org/pdfs/crossref-annual-meeting-live22-presentation-shared.pdf), [Recording transcript](https://docs.google.com/document/d/1h2Pc0orQqnMJoNGx4E6W_Yhr1fiZaeZRqF5ItcjEjkc/preview), [Posters from community guest speakers](https://community.crossref.org/c/live22-presentations/36), [Zoom Q&A transcript](https://docs.google.com/spreadsheets/d/1sPlXW8rg3jMN2IiBbihdAuDc_zacUvIR8vXSiHEbNN0/preview#gid=0)  
{{% logo-inline %}} [Crossref community call: the Research Nexus](https://outreach.crossref.org/acton/ct/16781/e-06ed-2206/Bct/l-tst/l-tst:1/ct1_0/1/lu?sid=TV2%3Ah33hCJvAB), [Google slides](https://docs.google.com/presentation/d/1oPGBfT_gJ-bcUOXYdRcM89iFZpzB0PTxkErsbOL0uvg/present?slide=id.g12c048515d9_2_13), [pdf slides](https://zenodo.org/record/6671346) - June 14, 2022    
{{% logo-inline %}} Crossref LIVE21 Annual Meeting & Board Election #CRLIVE21 - November 9 - Online [Recording](https://youtu.be/ca7YAXIPI70), [Slides]()  
The Benefits of Open Infrastructure (APAC time zones) - October 29, 2021 [Recording](https://youtu.be/X5TjsQ0gYok), [Slides](https://www.slideshare.net/CrossRef/crossref-live-the-benefits-of-open-infrastructure-apac-time-zones-29th-oct-2021)  
SE Asia webinar series - September 2 - 5, 2020 [Recording](https://www.youtube.com/playlist?list=PLe_-TawAqQj3n4B-xa9kgeTV4qX0O7aKt)  
Crossref LIVE UK/EU - October 8 2020 - Online - [Recording](https://youtu.be/t_UZOoA22oM) - [Slides](https://www.slideshare.net/CrossRef/crossref-live-uk-online)    
Crossref LIVE US/Canada - October 6 2020 - Online - [Recording](https://youtu.be/nnpVjSkaHm4) - [Slides](https://www.slideshare.net/CrossRef/crossref-live-us-online)  
Crossref LIVE Brazil - June 25 2020 - Online (in Portuguese) -  [Recording](https://youtu.be/Uf5iLyd7Yd4) - [Slides](https://www.slideshare.net/CrossRef/crossref-live-brazil-2020-236362696)  
Crossref LIVE Korea - June 17 2020 - Online (in Korean) - [Recording](https://youtu.be/pYSzwjFgHrA) - [Slides](https://www.slideshare.net/CrossRef/crossref-live-korea-17-june-2020)      
Crossref LIVE Arabic - May 11 2020 - Online (in Arabic)  - [Recording](https://youtu.be/McDIrEpWph4) - [Slides](https://www.slideshare.net/CrossRef/crossref-live-arabic)  
Crossref LIVE Spanish - May 19 2020 - Online (in Spanish) - [Recording](https://youtu.be/kQNwWzcWeH8) - [Slides](https://www.slideshare.net/CrossRef/crossref-live-spanish-19th-may-2020)  
Crossref LIVE CA - September 2019 - Oakland, CA          
Crossref LIVE Kuala Lumpur- July 8 2019 - Kuala Lumpur, Malaysia      
Crossref LIVE Bangkok- July 10 2019 - Bangkok, Thailand     
Crossref LIVE Bogota - June 4 2019 - Bogota, Colombia     
Crossref LIVE Kyiv - March 26 2019 - Kyiv, Ukraine        
Publisher workshop: metadata, Open Access and more in collaboration with the British Library - February 5 2019 - London, UK         
Crossref LIVE Mumbai - December 5 2018 - Mumbai, India            
OpenCon satellite event Oxford in collaboration with the Bodleian Library and Centre for Digital       Scholarship - November 30 2018 - Oxford, UK        
Crossref LIVE Goiania - September 18 2018 - Goiania, Brazil         
Crossref LIVE Fortaleza - September 20 2018 - Fortaleza, Brazil       
Crossref LIVE Hannover - June 27 2018 - Hannover, Germany           
Crossref LIVE Ulyanovsk - June 18 2018 - Ulyanovsk, Russia            
Crossref LIVE Cape Town - April 19 2018 - Cape Town, South Africa               
Crossref LIVE Pretoria - April 17 2018 - Pretoria, South Africa         
Crossref LIVE Tokyo - February 14 2018 - Tokyo, Japan           
OpenCon satellite event Oxford in collaboration with the Bodleian Library and Centre for Digital Scholarship - December 1 2017 - Oxford, UK         
Crossref LIVE Yogyakarta - November 20 2017 - Yogyakarta, Indonesia         
Crossref LIVE Turkey - October 26 2017 - Online event         
Crossref LIVE London - September 26 2017 - London, UK         
Joint Global Infrastructure Conference (ORCID/Crossref/DataCite) - June 15 2017 - Seoul, South Korea          
Crossref LIVE Seoul - June 12 2017 - Seoul, South Korea         
Crossref LIVE Boston - May 30 2017 - Boston, MA, USA        
Crossref/THOR Outreach Meeting - April 24 2017 - Warsaw, Poland       
Crossref LIVE Beijing - March 30 2017 - Beijing, China        
Crossref LIVE Brazil - December 16 2016 - São Paulo, Brazil       
Asia Pacific community webinar - December 14, 2016 [Slides](http://www.slideshare.net/Crossref/crossref-community-webinar-asia-pacific-12142016)    
Crossref LIVE Brazil - December 13 2016 - Campinas, Brazil        
Crossref LIVE DC - June 16 2016 - Washington DC, USA        
Crossref South Africa Workshop on Good Practice Publishing - September 1 2015 - Pretoria, South Africa          
Crossref International Workshop - July 7 2015 - Amsterdam, The Netherlands          
Crossref International Workshop - June 11 2015 - Vilnius, Lithuania         
Crossref International Workshop - April 29 2015 - Shanghai, China         
Crossref International Workshop - March 3 2014 - Barcelona, Spain         

{{% /accordion-section %}}

{{% /accordion %}}

### Past community events   

{{% accordion %}}

{{% accordion-section "2023 events" %}}

[APE 2023](https://berlinstitute.org/ape2023/) - January 10 - 11 - Berlin   
[NISO Plus 2023](https://niso.plus/np23schedule/) - February 14 - 16   
{{% /accordion-section %}}

{{% /accordion %}}


{{% accordion %}}

{{% accordion-section "2022 events" %}}

[Munin Conference Panel on Open Identifiers](https://site.uit.no/muninconf/program-2/)  
Better Together: Complete Metadata as Robust Infrastructure (APAC region) - [Recording](https://youtu.be/vS808JSSCjA) -  November 28   
[Feria Internacional del Libro de Guadalajara (
Guadalajara International Book Fair)](https://www.fil.com.mx/) - November 28-30   
[ISMTE 2022 Global Event](https://www.ismte.org/events/EventDetails.aspx?id=1630533&group=) - November 1  
[2022 Charleston Conference](https://www.charleston-hub.com/the-charleston-conference/) - November 2 - 5   
[2022 SSP Regional Meetup](https://customer.sspnet.org/ssp/EventDisplayNPGF.aspx?WebsiteKey=d1424957-06c0-458c-aa62-316197b4295d&hkey=b98b9701-0def-45d5-8f29-d85f8a3fd729&EventKey=REUK221027) - October 27 - Oxford, UK   
[Frankfurt Book Fair 2022, Stand M5, Hall 4.2](https://www.buchmesse.de/en) - October 17 - 21   
Better Together: Facilitating FAIR Research Output Sharing (APAC time zones) (with ORCID, Crossref, and DataCite) [Recording](https://crossref.wistia.com/medias/toq4ynsw0h), [Slide deck](https://zenodo.org/record/7105340)    
[ABEC (Associação Brasileira de Editores Científicos)](https://www.abecbrasil.org.br/novo/) Annual Meeting - Oct 4     
[Pubmet](https://pubmet2022.unizd.hr/programme/) - Sept 14 - 16 - Online  
[ALPSP Annual Conference and Awards 2022](https://www.alpsp.org/Conference) - September 14 - 16 - In person  
[OASPA Online Conference on Open Access Scholarly Publishing](https://oaspa.org/conference/) - September 20 - 22 - Online  
[DataCite Annual Member Meeting](https://datacite.org/member-meeting-2022.html) - September 22 - Online  
[Europe PMC AGM](https://europepmc.org/) - September 22 - London, UK  
[Plagiarism detection in the evolving publishing landscape: Best practices for journals](https://lp.scholasticahq.com/webinar-plagiarism-detection-journals/) - September 22 - Online  
[Better Together: Open new possibilities with Open Infrastructure (APAC time zones)(with ORCID, Crossref, and DataCite)](https://crossref.zoom.us/webinar/register/9016540028489/WN_b4fgKvczRueOjsg9IrFFXA) - June 27, 2022      
[Data Policy IG: Exploring features and improving standards for data availability statements](https://www.rd-alliance.org/plenaries/rda-19th-plenary-meeting-part-international-data-week-20%E2%80%9323-june-2022-seoul-south-korea-10) - June 21, 2022    
Crossref community call: the Research Nexus, June 14, 2022 - [Recording: Western time zone](https://youtu.be/wPzQtYjVFBo), [Recording: Eastern time zone](https://youtu.be/2Vrw-E8cCcw), [Wistia recording](https://crossref.wistia.com/medias/alpbj23mry?wtime=0s), [Recording transcript](https://docs.google.com/document/d/1h2Pc0orQqnMJoNGx4E6W_Yhr1fiZaeZRqF5ItcjEjkc/preview), [Zoom Q&A transcript](https://docs.google.com/spreadsheets/d/1sPlXW8rg3jMN2IiBbihdAuDc_zacUvIR8vXSiHEbNN0/preview#gid=0), [Google slides](https://docs.google.com/presentation/d/1CWoueRlXDBr764zDxw3docHDWosD8UCqlXzE9zZxyRI/preview?slide=id.gf67b34ab3e_0_8), [PDF](https://www.crossref.org/pdfs/crossref-annual-meeting-live22-presentation-shared.pdf), [Posters from community guest speakers](https://community.crossref.org/c/live22-presentations/36)  
[Working with Scholarly APIs: A NISO Training Series](http://www.niso.org/events/working-scholarly-apis-niso-training-series) - May 12 - Online  
[ALPSP Climate change: Practical steps to take action](https://www.alpsp.org/events-training) - March 16 - Online  
[NISO Plus 2022: Global Conversations: Global Connections](https://niso.plus/) - February 15 - 17 - Online  
[Paris Open Science European Conference](https://www.ouvrirlascience.fr/paris-open-science-european-conference-osec/) - February 4 - 6 - Online   


{{% /accordion-section %}}
{{% /accordion %}}



{{% accordion %}}
{{% accordion-section "2021 events" %}}

Academic Publishing in Europe Nr. 16 (APE)](https://www.ape2021.eu/) - January 12 - 13, 2021 - Online  
[PIDapalooza](https://www.pidapalooza.org/) - January 28, 2021 - Online   
NAS Journal Summit - March 22, 2021  
[Establishing Open & FAIR Research Data: Initiatives and Coordination](https://www.eventbrite.nl/e/establishing-open-fair-research-data-initiatives-and-coordination-tickets-103804895236)  - March 22, 2021 - Online  
[Mozfest 2021](https://www.mozillafestival.org/en/) - March 16, 2021 - Online  
STM Research Data - March 22, 2021 - Online  
[NORF Open Research in Ireland webinar: Infrastructures for Open Research](https://norf.ie/index.php/2021/03/10/norf-open-research-in-ireland-infrastructures/) - March 30, 2021 - Online    
[UKSG](https://www.uksg.org/event/uksgconference2021) - April 12 - 14, 2021 - Online   
[EARMA 2021](https://earma.org/conferences) - April 14 - 20, 2021 - Online  
[RDA 17th Plenary Meeting](https://www.rd-alliance.org/plenaries/rda-17th-plenary-meeting-edinburgh-virtual/promoting-data-citation-adoption-scholix) - April 20 - 23, 2021 - Online  
[JATS-con 2021](https://jats.nlm.nih.gov/jats-con/upcoming.html) - April 27 – 29, 2021 - Online  
[Los Metadatos Para la Comunidad de Investigacion](http://www.revistas.ucn.cl/2021/04/19/04demayo/) - May 4, 2021 - Online - [Recording](https://youtu.be/tUeKOQoM5eE) - [Slides](https://www.slideshare.net/CrossRef/los-metadatos-para-la-comunidad-de-investigacion)  
[LIS-Bibliometrics 2021](https://thebibliomagician.wordpress.com/2021/03/25/lis-bibliometrics-conference-2021/) - May 5, 2021 - Online   
[EARMA Digital Event - Global grant identifiers: building a richer picture of research support](https://earma.wildapricot.org/event-4235140)  - May 6, 2021 - Online   
[Library Publishing Forum](https://librarypublishing.org/forum/) - May 10 - 14, 2021 - Online  
UNAM webinar: Infraestructura Académica Abierta: uso y explotación de metadatos - May 13, 2021 (Online)   
[SSP 2021 Virtual Meeting](https://customer.sspnet.org/ssp/AM21/Home) - May 24 - 27, 2021 - Online  
{{% logo-inline %}} Crossref update: The Road Ahead (Western timezones timezones) - June 8, 2021- Online [Recording](https://youtu.be/Vj20ySUG1Ms), [Slides](https://docs.google.com/presentation/d/1dHCy_RIbFeyfjt34TU2FUtLHDsJ1LCjVnBGjv9tVJ44/present#slide=id.gd8afd7ca38_1_14)  
{{% logo-inline %}} Crossref update: The Road Ahead (Eastern timezones)  - June 9, 2021 - Online [Recording](https://youtu.be/NorhCsl9BfM), [Slides](https://docs.google.com/presentation/d/1dHCy_RIbFeyfjt34TU2FUtLHDsJ1LCjVnBGjv9tVJ44/present#slide=id.gd8afd7ca38_1_14)    
[EOSC Symposium 2021 Programme](https://www.eoscsecretariat.eu/eosc-symposium-2021-programme) - June 15, 2021 - Online   
[Japan Open Science Summit 2021](https://joss.rcos.nii.ac.jp/) - June 15, 2021 - Online   
[PKP Annual Meeting 2021](https://pkp.sfu.ca/2021/06/02/you-are-invited-pkp-2021-agm/) - June 18, 2021 - Online    
Crossref LIVE Indonesia webinar series - July 13 - 15, 2021 - Online - [Recordings](https://www.youtube.com/playlist?list=PLe_-TawAqQj0H-vrtBLT_y0BOgYqtBILm)  
[ASAPBio #feedbackASAP](https://asapbio.org/feedbackASAP) - July 21, 2021 - Online   
[The Geneva Workshop on Innovations in Scholarly Communication](https://oai.events) - September 6 - 10, 2021 - Online  
[OAI12](https://oai.events/oai12/) - September 6 - 10, 2021 - Online  
[Korean Council of Science Editors 10th anniversary conference](https://www.kcse.org/bbs/event.php?sid=121&year=2021) - September 8, 2021 - Online  
[STI 2021 Conference](http://sti2020.org/) - September 13 - 17, 2021 - Online    
[The 25th International Conference on Science, Technology and Innovation Indicators, STI 2021](https://www.ubivent.com/register/STI-2021) - September 15, 2021 - Online   
[OASPA conference 2021](https://oaspa.org/conference/ ) - September 21, 2021 - Online  
[Peer Review Week 2021](https://peerreviewweek.wordpress.com/peer-review-week-2021/) - September 20 - 24, 2021 - Online  
[RORing-at-Crossref community webinar](/blog/some-rip-roring-news-for-affiliation-metadata/) - September 29, 2021 - Online  
[Beilstein Open Science Symposium 2021](https://www.beilstein-institut.de/en/symposia/open-science/) - October 5 - 7, 2021  - Rüdesheim, Germany    
[ARMA Annual Conference 2021](https://armaconference.com/2021) - October 6 - 7, 2021 - Online    
[SSP New Directions Seminar](https://customer.sspnet.org/ssp/Events/ssp/EventDisplayNPGF.aspx?EventKey=SEM21FALL) - October 6 - 7, 2021 - Online  
[ISMTE 2021 Global Virtual Event](https://www.ismte.org/page/Conferences) - October 11 - 14, 2021 - Online     
[KnowledgeE OA week -  Towards a more Knowledgeable world: Open Access research in MENA](https://knowledgee.com/virtual-symposium-oa-research-in-MENA) - October 28 -  Online    
{{% logo-inline %}} [Crossref LIVE21](/crossref-live-annual/) - November 9 - Online   
**December**  
 [FORCE2021](https://www.force11.org/meetings/force2021) - December 7 - 9 - Online  


 {{% /accordion-section %}}
 {{% /accordion %}}

{{% accordion %}}
{{% accordion-section "2020 events" %}}

{{% logo-inline %}} [OpenCon Oxford 2020](https://youtu.be/Y8nEPMUo9R0) in in collaboration with the Bodleian Library - December 4, 2020  - Online    
{{% logo-inline %}} [Crossref LIVE20](/crossref-live-annual/) - November 10, 2020  - Online   
[Scholix Working Group: stakeholder uptake and next steps for article/data linking](https://www.rd-alliance.org/plenaries/rda-16th-plenary-meeting-costa-rica-virtual/scholix-working-group-stakeholder-uptake-and) - November 12, 2020  - Online  
[Frankfurt Book Fair 2020](/blog/crossref-at-the-frankfurt-digital-book-fair/) - October 14 - 18, 2020  - Online   
[OASPA 2020 Conference](https://oaspa.org/oaspa-2020-conference-program/) - September 21-24, 2020  - Online  
[Platform Strategies 2020](https://www.silverchair.com/community/platform-strategies/) - September 23, 2020  - 24 -  New York, NY  
[ABEC Annual Meeting 2020](https://meeting20.abecbrasil.org.br/) - September 22-25, 2020    - Online  
{{% logo-inline %}} [Crossref Live ](https://outreach.crossref.org/acton/fs/blocks/showLandingPage/a/16781/p/p-0055/t/page/fm/0) (US timezones) -  October 6, 2020  - Online  
[Workshop on Open Citations and Open Scholarly Metadata 2020](https://workshop-oc.github.io/) - September 9, 2020  - Online  
[PUBMET2020](http://pubmet.unizd.hr/pubmet2020/) - Septelmber 16-18, 2020  - Online  
[PIDapalooza](https://www.eventbrite.com/e/pidapalooza-2020-registration-60971406117) - January 29-30, 2020  2020 - Lisbon, Portugal   
[ROR Community Meeting](https://www.eventbrite.com/e/the-ror-community-meeting-lisbon-registration-82814758171) - January 28, 2020  - Lisbon, Portugal             
[ASAPbio January 2020 workshop: A Roadmap for Transparent and FAIR Preprints in Biology and Medicine](https://asapbio.org/meetings/preprints-roadmap-2020) - January 20, 2020  - Hinxton, UK
[Academic Publishing in Europe (APE)](https://web.archive.org/web/20200125135749/https://www.ape2020.eu/) - January 13-14, 2020 - Berlin, Germany    
[NISO Plus](https://www.niso.org/events/2020/02/niso-plus-conference) - February 23 -25, 2020  - Baltimore, USA     
[SocietyStreet (virtual)](https://www.societystreet.org/) - March 26, 2020  - Washington, DC   
[ER&L](https://2020erl.sched.com/event/XVhQ/w03-using-the-crossref-api-pulling-and-working-with-crossref-publication-and-funder-metadata-in-r) - March 8 - 11, 2020  - Austin, TX       
[CASE (webinar)](https://www.asianeditor.org/event/2020/index.php) - August 21, 2020 - Online  


{{% /accordion-section %}}
{{% /accordion %}}

{{% accordion %}}
{{% accordion-section "2019 events" %}}


[OpenCon Oxford Satellite](https://www.eventbrite.com/e/opencon-oxford-2019-tickets-74992752341)  -  December 6, 2019 - Oxford, UK    
[SPARC Africa Open Access Symposium 2019](http://www.sparcafricasymp.uct.ac.za/sparc/programme) - December 4-7, 2019 - Cape Town, South Africa     
[STM Week](https://www.stm-assoc.org/events/stm-week-2019/) - December 3-4, 2019 - London, UK  
[Munin Conference on Scholarly Publishing](https://site.uit.no/muninconf/) - November 27-28, 2019 - Tromsø, Norway          
[SpotOn London](https://www.eventbrite.co.uk/e/spoton-london-2019-communicating-research-for-societal-impact-tickets-67002112121) - November 21, 2019 - London, UK      
[euroCRIS Strategic Membership Meeting](https://eurocris.uni-muenster.de/#section-schedule) - November 18-20, 2019 - Münster, Germany    
[7th Annual PKP Conference](https://conference.pkp.sfu.ca/index.php/pkp2019/pkp2019) - November 18-20, 2019 - Barcelona, Spain    
[Crossref Annual Community Meeting #CRLIVE19](/crossref-live-annual/) - November 13-14, 2019 - Amsterdam, Netherlands    
[2LATmetrics conference](https://www.altmetric.com/events/latmetrics-2/) - November 4-6, 2019 - Cusco, Perú    
[Charleston Library Conference](https://charlestonlibraryconference.com/full-schedule/) - November 4-8, 2019 - Charleston, SC    
[RDA](https://www.rd-alliance.org/rda-14th-plenary-helsinki-espoo) - October 23-25, 2019 Helsinki, Finland    
[FORCE2019](https://www.force11.org/meetings/force2019) - October 16-17, 2019 - Edinburgh, UK    
[Frankfurt Book Fair 2019](https://www.buchmesse.de/en) - October 16-20, 2019 - Frankfurt, Germany     
[6:AM Altmetrics Conference](http://www.altmetricsconference.com/) - October 8-11, 2019 - Stirling, UK     
[ISMTE Europe](https://www.ismte.org/page/Conferences) - October 3, 2019 - Oxford, UK        
[Transforming Research](http://www.transformingresearch.org/) - September 26-27, 2019 - Washington, DC     
[Silverchair Platform Strategies](https://www.silverchair.com/community/platform-strategies/) - September 25-26, 2019 - New York, NY      
[European Research Innovation Days](https://ec.europa.eu/info/research-and-innovation/events/upcoming-events/european-research-and-innovation-days_en) - September 24-26, 2019 - Brussels, Belgium     
[PubMet](http://pubmet.unizd.hr/pubmet2019) - September 18-20, 2019 - Zadar, Croatia     
[ARMS 2019](http://www.arms2019.org.au/) - September 17-20, 2019 - Adelaide, South Australia     
[iPres](https://ipres2019.org) - September 16-20, 2019 - Amsterdam, Netherlands       
[ALPSP 2019](https://www.alpsp.org/Conference) - September 11-13, 2019 - Berkshire, UK     
[17th International Conference on Scientometrics & Infometrics (ISSI2019)](https://web.archive.org/web/20191105203524/https://www.issi2019.org/) - September 2-5, 2019 - Rome, Italy       
[RDA UK Workshop](https://www.rd-alliance.org/group/rda-united-kingdom/post/second-rda-uk-workshop-16-july-save-date) - July 16, 2019 - London, UK     
[OAI11](https://indico.cern.ch/event/786048/) - June 19-21, 2019 - Geneva, Switzerland      
[ARMA conference, 2019](https://www.arma.org/events/event_list.asp?start=11%2F10%2F2019&end=11%2F16%2F2019&view=week&group=&cid=) - June 17-18, 2019 - Belfast, Northern Ireland       
[4th Regional Meeting of Academic Journal Editors](https://jasolutions.com.co/4to-encuentro-editores-revistas/) - June 5-7, 2019 - Medellín, Columbia     
[International Conference on Electronic Publishing (ElPub) 2019](https://elpub2019.sciencesconf.org/) - June 2-4 - Marseille, France     
[CALJ Annual Conference 2019](https://calj-acrs.ca/) - June 1-2, 2019 - Vancouver, BC     
[SSP 41st Annual Meeting](https://customer.sspnet.org/SSP/AM-Archive/2019-Annual-Meeting/ssp/AM19/Home.aspx?hkey=621797eb-e077-4f96-87f3-9ce7a40cd6ac) - May 29-31, 2019 - San Diego, CA      
[iAnnotate 2019](https://iannotate.org/) - May 22-23, 2019 - Washington, DC      
[JATS-Con 2019](https://www.ncbi.nlm.nih.gov/books/NBK540820/) - May 21, 2019 - Cambridge, UK      
[Library Publishing Forum](https://librarypublishing.org/library-publishing-forum/) - May 8-10, 2019 - Vancouver, BC      
[8th International Scientific and Practical Conference](http://conf.rasep.ru/WCSP/WCSP2019 ) - April 23-26, 2019 - Moscow, Russia     
[STM US Annual Conference 2019](https://www.stm-assoc.org/events/stm-us-2019/) - April 11-12, 2019  - Washington, DC          
[UKSG 42nd Annual Conference and Exhibition](https://www.uksg.org/event/conference19) - April 8-10, 2019 - Telford, UK     
[Metrics in Transition Workshop 2019](https://metrics-project.net/en/events/workshop2019/) -  March 27-28, 2019 - Göttingen, Germany   
[The London Book Fair 2019](https://www.londonbookfair.co.uk/) - March 12-14, 2019 - London, UK    
[IFLA SIG on Library Publishing 2019 Midterm Meeting](https://librarypublishing.org/cfp-ifla-2019-midterm-meeting/) - February 28 - March 1, 2019 - Dublin, Ireland     
[AAP/PSP 2019 Annual Conference](https://www.aap2019pspconference.com/) - February 6-8, 2019 - Washington, DC     
[Publisher workshop: metadata, Open Access and more](https://www.eventbrite.com/e/publisher-workshop-metadata-open-access-and-more-tickets-52858612533) - February 5, 2019 - London, UK     
[PIDapalooza 2019](https://www.eventbrite.com/e/pidapalooza-2019-registration-49295286529) - January 23-24, 2019 - Dublin, Ireland     
[APE 2019](https://ape-archiv.eu/ape2019//) - January 16, 2019 - Berlin, Germany     
[SSP Pre-Conference](https://www.sspnet.org/events/past-events/ssp-pre-conference-in-conjunction-with-ape-2018-ramping-up-relevance/) - January 15, 2019 - Berlin, Germany    

{{% /accordion-section %}}
{{% /accordion %}}


{{% accordion %}}

{{% accordion-section "2018 events" %}}

[Coko London](https://coko.foundation/community/) - December 5 - London, UK  
[STM Week 2018 Tools and Standards](https://www.stm-assoc.org/events/day-1-stm-week-2018-tools-standards/) - Decemer 4 - London, UK  
[OpenCon Oxford](https://www.opencon2018.org/) - November 30 - Oxford, UK  
[Munin Conference on Scholarly Publishing](http://site.uit.no/muninconf/) - November 28 - 29 - Tromsø, Norway  
[Crossref LIVE18 #CRLIVE18](/crossref-live-annual/) - November 13 -14 - Toronto, Canada  
[SpotOn London](https://www.biomedcentral.com/p/spoton-2018) - November 3 - London, UK  
[Workshop on Research Objects](http://www.researchobject.org/ro2018/) - October 29 - Amsterdam, Netherlands  
[RDA 12th Plenary Meeting](https://www.rd-alliance.org/plenaries/rda-twelfth-plenary-meeting-part-international-data-week-2018-gaborone-botswana) - October 26 - Gaborone, Botswana  
[SciDataCon 2018](http://www.scidatacon.org/) - October 22 - 26 - Gaborone, Botswana  
[Frankfurt Book Fair](https://www.buchmesse.de/en) - October 10 - 12 - Frankfurt, Germany  
[FORCE2018](https://force2018.sched.com/) - October 10 - 12 - Montreal, Canada  
[Transforming Research](http://www.transformingresearch.org/) - October 3 - 4 - Providence, RI  
[SciELO 20 Years](https://www.scielo20.org/en/) - September 26 - 28 - Sao Paulo, Brazil  
[5AM London 2018](http://www.altmetricsconference.com/) - September 25-28 - London, UK  
[COASP 2018 Conference](https://oaspa.org/conference/coasp-2018-program/) - September 19 - Vienna, Austria  
[Dublin Core Metadata Initiative](http://dublincore.org/) - September 10 - 13 - Porto, Portugal  
[OpenCitations workshop](https://workshop-oc.github.io/#about) - September 3 - 5 - Bologna, Italy  
[European Association of Science Editors (EASE)](http://www.ease.org.uk/ease-events/14th-ease-conference-bucharest-2018/programme-2018/) - June 7 - 10 - Bucharest, Romania  
[INORMS Conference](http://www.inorms2018.org/) - Jun 6 - 8 - Edinburgh, UK  
[SSP 40th Annual Meeting, Booth #212A](https://customer.sspnet.org/SSP/AM-Archive/2018-Meeting/ssp/AM18/Home.aspx?hkey=621797eb-e077-4f96-87f3-9ce7a40cd6ac) - May 30 - June 1 - Chicago, IL, USA  
[Library Publishing Forum](https://librarypublishing.org/library-publishing-forum/) - May 21 - 23 - Minneapolis, MN, USA  
[3er Congreso Internacional de Editores Redalyc](https://web.archive.org/web/20200316022408/http://congreso.redalyc.org/ocs/public/congresoEditores/index.html) - May 16 - 18 - Trujillo, Peru  
[CSE 2018 Conference](https://www.councilscienceeditors.org/events/previous-annual-meetings/cse-2018-annual-meeting/) -  May 5 - 8 - New Orleans, LA, USA  
[MIT Better Science Ideathon](https://betterscience.mit.edu/) - April 23 - Cambridge, MA, USA  
[Computers in Libraries 2018](http://computersinlibraries.infotoday.com/Speakers/Patricia-Feeney.aspx) - April 17 - 19 - Arlington, VA, USA  
[EARMA Conference 2018](http://www.earmaconference.com/) - April 16 - 18 - Brussels, Belgium  
[International Publishing Symposium](https://publishing.brookes.ac.uk/conference/2018_international_publishing_symposium/) - April 12 - 13 - Oxford, UK  
[UKSG 2018](https://www.uksg.org/event/conference18) - April 9 - 11 - Glasgow, Scotland  
[ISMTE 2018 Asia Pacific Conference](https://www.ismte.org/page/PastConferences) - March 27 - 28 - Singapore  
[NFAIS 2018 Annual Conference](http://www.nfais.org/2018-conference) - February 28 - March 2 - Alexandrea, VA, USA  
[Researcher to Reader](https://r2rconf.com) -  February 26 - 27 - London, UK  
[ASAPBio Meeting](http://asapbio.org/peer-review) - February 7 - 9 - Chevy Chase, Maryland, USA  
[LIS Bibliometrics](https://www.eventbrite.com.au/e/responsible-use-of-bibliometrics-in-practice-tickets-39687409109) - January 30 - London, UK  
Peer Review Transparency Workshop - January 24 - Cambridge, MA, USA  
[PIDapalooza](https://pidapalooza.org/) - January 23 - 24 - Girona, Catalonia, Spain  

{{% /accordion-section %}}

{{% /accordion %}}


{{% accordion %}}

{{% accordion-section "2017 events" %}}

[AGU](https://fallmeeting.agu.org/2017/) - December 14 - 15 - New Orleans, LA, USA  
[STM Innovations Seminar 2017](http://www.stm-assoc.org/events/innovations-seminar-2017/) - December 6 - London, UK  
[Crossref #LIVE17](/crossref-live-annual/) - November 14 - 15 - Singapore  
[XUG eXtyles User Group Meeting](http://www.inera.com/customers/eXtyles-user-group-meeting) - November 2-3 - Cambridge, MA, USA  
[Dublin Core 2017](http://dcevents.dublincore.org/IntConf/dc-2017) - October 26-29 - Washington, DC, USA  
[FORCE 2017](https://www.force11.org/event/force2017-berlin-oct-25-27) - October 25-27 - Berlin, Germany  
[Frankfurt Book Fair, #FBM17](https://www.buchmesse.de/en) - October 11-15 - Frankfurt, Germany  
[Altmetrics Conference 4:AM](http://www.altmetricsconferencearchive.com/) -  September 26 - 29 -  Toronto, Canada  
[COASP 2017](http://oaspa.org/coasp-2017-save-date/) - September 20 - 21 - Lisbon, Portugal  
[ALPSP Conference](https://www.alpsp.org/conference) - September 13 - 15 - Netherlands  
[ISMTE North American Conference](http://www.ismte.org/page/Conferences) - August 10-11 - Denver, CO, USA  
[LIBER 2017 Conference](https://libereurope.eu/article/conference-highlights-from-liber-2017-in-patras-greece/) - July 5-7 - Patras, Greece  
[ALA Annual Conference (ALCTS CRS)](https://www.eventscribe.com/2017/ala-annual/fsPopup.asp?Mode=presInfo&PresentationID=266284) - June 25 - Chicago, IL, USA  
[EMUG 2017](http://www.cvent.com/events/emug-2017/event-summary-e99312253d7f45b2b3b6a0f4dfdb7900.aspx) - June 22-23 - Boston, MA, USA  
[ORCID Identifiers and Intellectual Property Workshop](https://orcid.org/content/identifiers-and-intellectual-property-workshop) - June 22 - Paris, France  
[32nd Annual NASIG Conference 2017](http://www.nasig.org/) - June 8 -11 - Indianapolis, IN, USA  
[SSP 39th Annual Meeting - Booth 500A](https://www.sspnet.org/events/past-events/annual-meeting-2017/event-home/) - May 31-June 2 - Boston, MA, USA  
[5th World Conference on Research Integrity](https://web.archive.org/web/20170614193134/http://wcri2017.org/) - May 28 - 31 - Amsterdam, The Netherlands  
[WikiCite 2017](https://meta.wikimedia.org/wiki/WikiCite_2017) - May 23-25 - Vienna, Austria  
[CSE 2017 Annual Meeting](http://www.resourcenter.net/Scripts/4Disapi07.dll/4DCGI/events/2017/634-Events.html?Action=Conference_Detail&ConfID_W=634&ConfID_W=634) - May 20-23 - San Diego, CA, USA  
[2017 ScholarONE User Conference](https://clarivate.com/blog/scholarone-manuscripts-2017-key-highlights/) - May 3-4 - Madrid, Spain  
[UKSG 2017 Annual Conference](http://www.uksg.org/event/conference17) - April 10-12 - Harrogate, UK  
[Highwire Spring Publishers Meeting](http://events.r20.constantcontact.com/register/event?llr=q9cys5lab&oeidk=a07eds09n4bb350024e) - April 4-6 - Stanford, CA, USA  
[CNI Spring 2017 Membership Meeting](https://www.cni.org/events/membership-meetings/upcoming-meeting/spring-2017) - April 3-4 - Albuquerque, NM  
[ISMTE 2017 Asian-Pacific Conference](http://www.ismte.org/event/Beijing2017) - March 27-28 - Beijing, China  
[COPE China Seminar 2017](http://publicationethics.org/events/cope-china-seminar-2017) - March 26 - Beijing, China  
[ACRL 2017 Conference](http://conference.acrl.org/) - March 22-25 - Baltimore, MD, USA  
[#FuturePub 10 - New Developments in Scientific Collaboration Tech](https://www.eventbrite.com/e/futurepub-10-new-developments-in-scientific-collaboration-tech-tickets-32012536358) - March 13 - London, UK  
[Research Libraries UK (RLUK)](http://rlukconference.com/) - March 8-10 - London, UK  
PSP 2017 Annual Conference - February 1-3 - Washington, DC, USA  

{{% /accordion-section %}}

{{% /accordion %}}

{{% accordion %}}

{{% accordion-section "2016 events" %}}

[STM Digital Publishing Conference](http://www.stm-assoc.org/events/stm-digital-publishing-2016/) – December 6-8 – London, UK  
[OpenCon 2016](http://www.opencon2016.org/updates) – November 12-14 – Washington DC, USA  
[PIDapalooza](http://pidapalooza.org/) – November 9-10 – Reykjavik, Iceland  
[Frankfurt Book Fair](https://www.buchmesse.de/en) – October 19-23 – Frankfurt, Germany (Hall 4.2, Stand 4.2 M 85)  
[ORCID Outreach Conference](https://orcid.org/about/events/) – October 5-6 – Washington DC, USA  
[3:AM Conference](http://altmetricsconference.com/) – September 26 – 28 – Bucharest, Romania  
[OASPA](http://oaspa.org/conference/) – September 21-22 – Arlington VA, USA  
[ALPSP](https://www.alpsp.org/Conference) – September 14-16 – London, UK  
[SciDataCon](http://www.scidatacon.org/2016/) – September 11-17 – Denver CO, USA  
[Vivo 2016 Conference](http://vivoconference.org/) – August 17-19 – Denver CO, USA  
ACSE Annual Meeting 2016 – August 10-11 - Dubai, UAE  
[CASE 2016 Conference](http://asianeditor.org/event/2016/index.php) – July 20-22 - Seoul, South Korea  
[SHARE Community Meeting](http://www.share-research.org/2016/04/share-2016-community-meeting/) - July 11-14 - Charlottesville, VA, USA  

{{% /accordion-section %}}
{{% /accordion %}}

{{% /row %}}
{{% /column %}}



{{< icon name="fa-star" color="crossref-blue" size="small" >}} See also the archive of all our [annual meetings](/crossref-live-annual/archive/) and if you’re interested in viewing recordings about our services and more, check out our [YouTube channel](https://www.youtube.com/c/CrossrefVideos).  

<br/><br/>
<a href="#top">Back to top of the page</a>  

---

Contact our [outreach team](mailto:feedback@crossref.org) if you'd like us to participate in an event or meet us at one of the above.
