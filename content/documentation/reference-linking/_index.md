+++
title = "Reference Linking"
date = "2020-04-08"
draft = false
author = "Anna Tolwinska"
type = "documentation"
layout = "documentation_single"
documentation_section = ["other-services", "reference-linking"]
aliases = [
  "/education/reference-linking",
  "/education/reference-linking/"
]
[menu.documentation]
identifier = "documentation/reference-linking"
parent = "Documentation"
rank = 4
weight = 200000
+++

{{< snippet "/_snippet-sources/reference-linking.md" >}}
