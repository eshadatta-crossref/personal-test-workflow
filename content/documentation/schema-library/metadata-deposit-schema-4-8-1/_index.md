+++
title = "Metadata deposit schema 4.8.1"
date = "2021-10-05"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["schema-library", "metadata-deposit-schema-4-8-1"]
identifier = "documentation/schema-library/metadata-deposit-schema-4-8-1"
rank = 4
weight = 60407
aliases = [
    "/education/content-registration/metadata-deposit-schema/metadata-deposit-schema-4-8-1",
    "/education/content-registration/metadata-deposit-schema/metadata-deposit-schema-4-8-1/",
    "/documentation/content-registration/metadata-deposit-schema/metadata-deposit-schema-4-8-1/",
    "/documentation/content-registration/metadata-deposit-schema/metadata-deposit-schema-4-8-1"
]
+++

Beginning with deposit schema version `4.4.2`, all Crossref schema releases are available in our GitLab schema repository as a bundle. [Bundle 0.3.1](https://gitlab.com/crossref/schema/-/releases/0.3.1) contains schema version `4.8.1` and associated files.

Schema: [crossref4.8.1.xsd](https://data.crossref.org/schemas/crossref4.8.1.xsd)
Full documentation: [4.8.1](https://data.crossref.org/reports/help/schema_doc/4.8.1/index.html)

Crossref included schema:

* [common4.8.1.xsd](https://data.crossref.org/schemas/common4.8.1.xsd)
* [fundref.xsd](https://data.crossref.org/schemas/fundref.xsd)
* [AccessIndicators.xsd](https://data.crossref.org/schemas/AccessIndicators.xsd)
* [clinicaltrials.xsd](https://data.crossref.org/schemas/clinicaltrials.xsd)
* [relations.xsd](https://data.crossref.org/schemas/relations.xsd)

External imported schema:

* [MathML](http://www.w3.org/Math/XMLSchema/mathml3/mathml3.xsd)
* [JATS](https://jats.nlm.nih.gov/publishing/1.2/xsd/JATS-journalpublishing1.xsd)

Changes from `4.4.2`

* refactoring of schema
* relax regex rules for email addresses
* allow ISBN beginning with 979
* update imported JATS schema to v. 1.3
* relax regex rules for `<given_name>` element\
