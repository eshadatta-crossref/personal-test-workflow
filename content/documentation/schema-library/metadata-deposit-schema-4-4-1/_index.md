+++
title = "Metadata deposit schema 4.4.1"
date = "2020-04-08"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["schema-library", "metadata-deposit-schema-4-4-1"]
identifier = "documentation/schema-library/metadata-deposit-schema-4-4-1"
rank = 4
weight = 60410
aliases = [
    "/education/content-registration/metadata-deposit-schema/metadata-deposit-schema-4-4-1",
    "/education/content-registration/metadata-deposit-schema/metadata-deposit-schema-4-4-1/",
    "/documentation/content-registration/metadata-deposit-schema/metadata-deposit-schema-4-4-1",
    "/documentation/content-registration/metadata-deposit-schema/metadata-deposit-schema-4-4-1/"
]
+++

Schema: [crossref4.4.1.xsd](https://data.crossref.org/schemas/crossref4.4.1.xsd)
Full documentation: [4.4.1](https://data.crossref.org/reports/help/schema_doc/4.4.1/index.html)

Crossref included schema:

* [common4.4.1.xsd](https://data.crossref.org/schemas/common4.4.1.xsd)
* [fundref.xsd](https://data.crossref.org/schemas/fundref.xsd)
* [AccessIndicators.xsd](https://data.crossref.org/schemas/AccessIndicators.xsd)
* [clinicaltrials.xsd](https://data.crossref.org/schemas/clinicaltrials.xsd)
* [relations.xsd](https://data.crossref.org/schemas/relations.xsd)
* [common4.3.5.xsd](https://data.crossref.org/schemas/common4.3.5.xsd)

External imported schema:

* [MathML](http://www.w3.org/Math/XMLSchema/mathml3/mathml3.xsd)
* [JATS](https://jats.nlm.nih.gov/publishing/1.1/xsd/JATS-journalpublishing1.xsd)

Changes from `4.4.0`

* adds support for peer reviews
