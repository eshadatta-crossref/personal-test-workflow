+++
title = "Resource-only deposit schema 4.3.6"
date = "2020-04-08"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["schema-library", "resource-only-deposit-schema-4-3-6"]
identifier = "documentation/schema-library/resource-only-deposit-schema-4-3-6"
rank = 4
weight = 60413

aliases = [
    "/education/content-registration/metadata-deposit-schema/resource-deposit-schema-4-3-6/",  
    "/education/content-registration/metadata-deposit-schema/resource-only-deposit-schema-4-3-6",
    "/education/content-registration/metadata-deposit-schema/resource-only-deposit-schema-4-3-6/",
    "/documentation/content-registration/metadata-deposit-schema/resource-only-deposit-schema-4-3-6/",
    "/documentation/content-registration/metadata-deposit-schema/resource-only-deposit-schema-4-3-6/"
]
+++

Schema: [doi_resources4.3.6.xsd](https://data.crossref.org/schemas/doi_resources4.3.6.xsd)
Full documentation: [doi_resource4.3.6](https://data.crossref.org/reports/help/schema_doc/doi_resources4.3.6/doi_resources4.3.6.html)

Crossref included schema:

* [common4.3.7.xsd](https://data.crossref.org/schemas/common4.3.7.xsd)
* [fundref.xsd](https://data.crossref.org/schemas/fundref.xsd)
* [AccessIndicators.xsd](https://data.crossref.org/schemas/AccessIndicators.xsd)
* [clinicaltrials.xsd](https://data.crossref.org/schemas/clinicaltrials.xsd)
* [relations.xsd](https://data.crossref.org/schemas/relations.xsd)
* [common4.3.5.xsd](https://data.crossref.org/schemas/common4.3.5.xsd)
