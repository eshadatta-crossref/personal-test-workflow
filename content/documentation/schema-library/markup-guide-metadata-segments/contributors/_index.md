+++
title = "Contributors"
date = "2022-01-19"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["schema-library", "markup-guide-metadata-segment", "contributors"]
identifier = "documentation/schema-library/markup-guide-metadata-segment/contributors"
rank = 4
weight = 60610
aliases = [
  "/documentation/content-registration/descriptive-metadata/contributors",
  "/documentation/content-registration/descriptive-metadata/contributors/",
    "/education/content-registration/descriptive-metadata/contributors",
    "/education/content-registration/descriptive-metadata/contributors/"
]
+++
This guide gives markup examples of contributor metadata for members registering content by direct deposit of XML. A *contributor* is a person or organization that is considered the author of a work. A contributor may be a person or a group author (`organization` in our XML). Contributor metadata also includes affiliations, which [have their own guide](documentation/content-registration/descriptive-metadata/contributor).

### ORCID iDs<a id='00008' href='#00008'><i class='fas fa-link'></i></a>

An author's [ORCID iD](https://orcid.org) should be included whenever possible. Providing an ORCID iD in a metadata record allows the author's ORCID record to be automatically updated via our [auto-update process](/community/orcid/). OJS users who have [upgraded](https://docs.pkp.sfu.ca/admin-guide/en/managing-the-environment#upgrading) to version 3.1.2 or later can request authenticated iDs from both contributing authors and co-authors - learn more about the [OJS-ORCID plugin](https://orcid.org/blog/2019/03/12/introducing-new-ojs-orcid-plugin).

### Contributor roles

We currently support and require a single contributor role per contributor. Supported values are:

* author
* editor
* chair
* reviewer
* review-assistant
* stats-reviewer
* reviewer-external
* reader
* translator

We intend to allow multiple roles per contributor and as expand our list of supported contributor roles in a future update.

### Contributor order<a id='00009' href='#00009'><i class='fas fa-link'></i></a>

The `<person_name>` and `<organization>` elements both include required contributor `role` and `sequence` attributes. An author may be *first* or *additional*. Specific sequence numbering is not allowed, but many systems using our metadata assume that the order of authors as present in the metadata is the appropriate order for metadata display. * If a contributor has just one name, put it under the `<surname>` field



Note that the data supplied in the `<given_name>` and `<surname>` fields is used for display and query matching and must be as accurate as possible.

``` XML
<person_name sequence="first" contributor_role="author">
  <given_name>Minerva</given_name>
  <surname>Nipperson</surname>
  <ORCID authenticated="true">https\://orcid.org/0000-0002-4011-3590</ORCID>
</person_name>
```



### Contributor example<a id='00010' href='#00010'><i class='fas fa-link'></i></a>

``` XML
<contributors>
  <person_name sequence="first" contributor_role="author">
    <given_name>Minerva</given_name>
    <surname>Nipperson</surname>
    <suffix>III</suffix>
    <affiliations>
      <institution>
        <institution_id type="ror">https://ror.org/01bj3aw27</institution_id>
        <institution_department>Office of Environmental Management</institution_department>
      </institution>
    </affiliations>
    <institution>
        <institution_name>Tinker Fan Club</institution_name>
        <institution_acronym>TinFC</institution_acronym>
        <institution_place>Boston, MA</institution_place>
        <institution_department>Office of Environmental Management</institution_department>
    </institution>
    <ORCID authenticated="true">http\://orcid.org/0000-0002-4011-3590</ORCID>
  </person_name>
  <person_name sequence="additional" contributor_role="author">
    <given_name>Christopher </given_name>
    <surname>Fielding</surname>
    <affiliations>
      <institution>
        <institution_id type="ror">https://ror.org/01bj3aw27</institution_id>
        <institution_department>Office of Environmental Management</institution_department>
      </institution>
    </affiliations>
  </person_name>
  <person_name sequence="additional" contributor_role="author">
    <given_name>Katharine </given_name>
    <surname>Mech</surname>
  </person_name>
</contributors>
```
