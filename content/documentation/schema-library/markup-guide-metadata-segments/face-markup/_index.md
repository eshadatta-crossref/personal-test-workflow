+++
title = "Face markup"
date = "2020-04-08"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["schema-library", "markup-guide-metadata-segment", "face-markup"]
identifier = "documentation/schema-library/markup-guide-metadata-segment/face-markup"
rank = 4
weight = 60612
aliases = [
    "/documentation/schema-library/face-markup",
    "/documentation/schema-library/face-markup/",
    "/education/content-registration/metadata-deposit-schema/face-markup",
    "/education/content-registration/metadata-deposit-schema/face-markup/",
    "/documentation/content-registration/metadata-deposit-schema/face-markup/",
    "/documentation/content-registration/metadata-deposit-schema/face-markup"
]
+++

Our schema supports minimal face markup in order to avoid ambiguity in certain disciplines, such as genetics, where the same text may be a gene (when italicized) or a protein (when not italicized).

Face markup that appears in the `title`, `subtitle`, `original_language_title`, and `unstructured_citation` elements should be retained when depositing metadata. Face markup in other elements (such as small caps in author names) must be dropped. Face markup support includes bold (b), italic (i), underline (u), over-line (ovl), superscript (sup), subscript (sub), small caps (scp), and typewriter text (tt).

Examples where inclusion of face markup is especially important include:

1. *Italics* in titles for terms such as species names or genes
2. Superscript and subscript in titles as part of chemical names (for example, H<sub>2</sub>0)
3. Superscript and subscript in simple inline mathematics (for example, x<sup>2</sup> + y<sup>2</sup> = z<sup>2</sup>)

The schema supports nested face markup (for example: This text is <b><i>bold and italic</b></i>), which would be tagged as:

``` XML
This text is <b><i>bold and italic</i></b>
```

Correspondingly, superscript and subscript may be nested for correct representation of xyz. This expression should be tagged as:

``` XML
x<sup>y<sup>z</sup></sup>
```

We also support [MathML markup](documentation/schema-library/markup-guide/including-mathml-in-deposits) in title elements.
