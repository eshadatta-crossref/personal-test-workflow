+++
title = "Translated and multi-language materials"
date = "2023-01-18"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["schema-library", "markup-guide-metadata-segment", "translations"]
identifier = "documentation/schema-library/markup-guide-metadata-segment/translations"
rank = 4
weight = 60624
aliases = [

]
+++

Much of the content in Crossref is English language, but we encourage members to register content in the appropriate language for the content being registered.  We support UTF-8 encoded character sets and in many cases you will be able to supply multiple versions of titles, abstracts, and other metadata.

## Multi-language content
We currently provide limited support for multi-language content. If you consider your content to be multi-language and not a translation (meaning it will be cited as a single item) register one DOI for the item, and include titles and abstracts in multiple languages in your metadata record as allowed (Note that support for this currently varies by content type).
Order is important in input metadata -  if the English title is provided as the first title in your metadata, then the English title will be displayed in citations generated from our metadata.

## Translated content

If your content is translated register separate DOIs for each translation, and connect translations with [relationship metadata](/documentation/schema-library/markup-guide/relationships) using the relationship `hasTranslation`.
This is essential if translate items have differing metadata such as article IDs or page numbers.  
If the translations are registered and connected via a relationships, it is not necessary to include titles and other metadata in multiple languages.
Note that for items with separate DOIs, we do not aggregate cited-by matches or search results.

``` XML
<titles>
<title>When your best metadata isn't good enough: working with an imperfect specification</title>
<original_language_title language="fr">Quand vos meilleures métadonnées ne	suffisent pas: travailler avec une spécification imparfaite</original_language_title>
</titles>
```
