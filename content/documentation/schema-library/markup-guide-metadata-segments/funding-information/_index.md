+++
title = "Funding information"
date = "2020-04-08"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["schema-library", "markup-guide-metadata-segment", "funding information"]
identifier = "documentation/schema-library/markup-guide-metadata-segment/funding-information"
rank = 4
weight = 60616
aliases = [
    "/education/content-registration/administrative-metadata/funding-information",
    "/education/content-registration/administrative-metadata/funding-information/",
    "documentation/content-registration/administrative-metadata/funding-information",
    "documentation/content-registration/administrative-metadata/funding-information/",
]
+++

Add funder information, including the funder’s unique identifier from the [Funder Registry](/documentation/funder-registry/), and help build connections between funders and research outputs.

### Linking research funding and published outcomes<a id='00565' href='#00565'><i class='fas fa-link'></i></a>

Funding data is used by funders to track the publications that result from their [grants](/documentation/content-registration/content-types-intro/grants/), including use of facilities, equipment, salary awards, and so on.

Publishers can contribute by depositing the funding acknowledgements from their publications as part of their standard metadata. The deposit should include funder names, funder IDs, and associated grant numbers.

Funding data can be searched using our [interfaces for people](/documentation/retrieve-metadata#00358) or our [APIs for machines](/documentation/retrieve-metadata#00359). This data clarifies the scholarly record, and makes life easier for researchers who may need to comply with requirements to make their published results publicly available.

### How to collect and register funding data<a id='00566' href='#00566'><i class='fas fa-link'></i></a>

* Ask authors to submit the names of their funder(s) and grant numbers when they submit their manuscript, or extract funding information from the text of accepted manuscripts
* Match funder names to their corresponding Funder ID in the [Funder Registry](/services/funder-registry)
* Deposit with funder name(s), Crossref funder ID(s) and Crossref [grant ID(s)](/documentation/content-registration/content-types-intro/grants/) for each DOI.
  * You can register funding data as a [stand-alone deposit](/documentation/funder-registry/funding-data-deposits#00298) (useful for backfiles) or as part of your standard metadata deposit (for current content)
* Make use of our [metadata retrieval tools](/documentation/retrieve-metadata/) to check the metadata we hold for your publications (and to retrieve metadata for your own analysis)
* Check your progress using [Participation Reports](/documentation/reports/participation-reports/) to see the percentage of your deposits that have funding data (and other key metadata elements) registered.
