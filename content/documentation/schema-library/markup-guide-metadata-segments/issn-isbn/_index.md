+++
title = "ISSNs and ISBNs"
date = "2020-04-08"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["content-registration", "markup-guide-metadata-segment", "issn-isbn"]
identifier = "documentation/content-registration/markup-guide-metadata-segment/issn-isbn"
rank = 4
weight = 60618
aliases = [
    "/education/content-registration/descriptive-metadata/issns-isbns",
    "/education/content-registration/descriptive-metadata/issns-isbns/"
]
+++

An International Standard Serial Number (ISSN) or International Standard Book Number (ISBN) is a number used to uniquely identify a serial or book publication. To obtain an ISSN, you need to register with the [ISSN International Centre](https://www.issn.org/); and for an ISBN, with your [national ISBN agency](https://www.isbn-international.org/content/how-get-isbn).

ISSNs/ISBNs are useful in distinguishing between serials or books with the same title. If a publication with the same content is published in more than one format, a different identifier is assigned to each media type. For example, a journal may have a print ISSN and an electronic ISSN, and print and ebooks have different ISBNs.

* Include the title and ISSN/ISBN when you first deposit metadata for a content item in our system (if applicable)
* Include both print and electronic ISSNs/ISBNs (if applicable)

If the journal does not have an ISSN at the time of registering content for it, include a [title-level DOI for the journal](/documentation/content-registration/descriptive-metadata/journal-title-management#00016). Once the ISSN is known, deposits should include both the ISSN and the journal-level DOI. Ideally, you would also [update the metadata](/documentation/register-maintain-records/maintaining-your-metadata/updating-your-metadata) for all the previously registered content to include the ISSN. If you have any queries, please [contact us](/contact/).

We do not verify your title and ISSN combination with an external agency, but we carry out a check digit validation on every ISSN deposited. Once a title or ISSN is deposited, a new publication with the same title or ISSN can't be created. If you try to make another deposit using a title and ISSN combination that does not match the combination in our system, the deposit will not work. Learn more about [updating title records, including ISSNs/ISBNs](/documentation/register-maintain-records/maintaining-your-metadata/updating-your-metadata#00167).
