+++
title = "Grants markup guide"
date = "2021-08-16"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["schema-library", "markup-guide-record-types", "grants"]
identifier = "documentation/schema-library/markup-guide-record-types/grants"
rank = 4
weight = 60614
aliases = [
    "/documentation/content-registration/content-type-markup-guide/grants/",
    "/documentation/content-registration/content-type-markup-guide/grants",
    "/education/content-registration/content-type-markup-guide/grants",
    "/education/content-registration/content-type-markup-guide/grants/"
]
+++

All metadata records and identifiers registered with Crossref are submitted as XML formatted using our metadata input schema. Unlike other objects registered with Crossref, grants have their own grant-specific input schema. Version 0.1.1 of our Grants schema is available in our [GitLab schema repository](https://gitlab.com/crossref/schema/), as is a [complete XML example](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/grants-0.1.1.xml).

Please note: as of version 0.1.1 a version attribute is required in the <doi_batch> schema declaration, for example:

``` XML
<doi_batch xmlns="http://www.crossref.org/grant_id/0.1.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.crossref.org/grant_id/0.1.1 http://www.crossref.org/schemas/grant_id0.1.1.xsd" version="0.1.1">
```

Members currently using version 0.0.1 do not need to provide a version number.

## Grant  and project metadata
Grant metadata by default includes a single or multiple projects. Multiple projects may be applied to a single grant but the DOI registered is applied at the grant level. Multiple grants may be included in a single XML file.

The metadata within each project includes basics like titles, descriptions, and investigator information (including affiliations), funding information such as funder names and identifiers from the [Funder Registry](/services/funder-registry/), as well as information about funding types and amounts.

When registering a grant:
* you **must** include required project information (a project title, a funder name and identifier, and a funding type) as well as your internal grant or award number
* you **should** include a project description, language information, investigator details including ORCID IDs, ROR IDs within affiliations, and investigator country code; award amounts/currency, and project start and end dates and/or an award date (note that  project or grant start/end dates are used to calculate [current vs. backfile content registration fees](/fees/#content-registration-fees)).
* you **may** include multiple titles and descriptions as well as language information; a funding scheme, and planned project start and end dates.

### Project metadata

**Project:** Project metadata includes titles and descriptions (abstracts). Both can be supplied multiple times to capture information in different languages.

| Element / attribute | Description | Limits |
|---|---|---|
| project | Container for project information. Multiple projects may be assigned to a single Grant ID. | required; multiple allowed
| project-title | Title of a project funded by the grant being registered | required; multiple allowed
| description | Used to capture an abstract or description of a project. | optional; multiple allowed
| @xml:lang | Use @xml:lang to identify language for each project-title or description. This allows you to provide multiple titles in different languages. | optional |

**Investigators:** Investigators are not required, but all applicable investigators should be included. Optional start and end dates may be used to capture investigators whose involvement is limited to a specific timeframe.

| Element / attribute | Description | Limits |
|---|---|---|
| investigators | container for investigator information | optional |
| person | container for individual investigator details | at least 1 required, multiple allowed (unbounded) |
| @role | available roles are lead_investigator, co-lead_investigator, investigator | required |
| @start-date | Date an investigator began work with the project | optional |
| @end-date | Date an investigator ended work with the project | optional |
| givenName | given or first name | optional |
| familyName | family or surname | optional |
| alternateName | alias or nickname used by the Investigator | optional |
| affiliation | container for affiliation information | optional, multiple allowed |
| institution | institution an investigator is affiliated with when associated with the project being defined. Multiple affiliations should be supplied where applicable | 1 allowed, use multiple affiliation groups for investigators with multiple affiliations |
| @country | ISO 3166-1 alpha 2-letter country code, captures location (country) of affiliation | optional |
| ROR | A [ROR ID](https://ror.org/) may be supplied to disambiguate affiliation information. | optional |
| ORCID | [ORCID ID](https://orcid.org) of the investigator, expressed as a URL | optional |

**Funding details:**  Funding details include award amount, currency, funder details, and funding type.  

| Element / attribute | Description | Limits |
|---|---|---|
| award-amount | total overall amount awarded to project | optional |
| @currency | ISO 4217 currency for value provided in award-amount | required |
| funding | container for funding information. Use multiple funding sections as needed to specify different funding types | required, multiple allowed |
| @amount | amount of funding provided by funder | optional |
| @currency | ISO 4217 currency for value provided in @amount | optional |
| @funding-percentage | percentage of overall funding | optional |
| @funding-type | type of funding provided, values are limited | required |
| @null-amount | supply for projects where an award amount is missing or can’t be disclosed - allowed values are: unknown, undisclosed, not-applicable other | optional |
| funder-name | name of the funder | required |
| funder-id | funder identifier from our [Funder Registry](/services/funder-registry/) | required
| funding-scheme | scheme for grant or award as provided by the funder | optional

**Award dates:** Dates can be applied at the project level (via award-date). An award-start-date may also be applied to the grant / award as a whole.

| Element / attribute | Description | Limits |
|---|---|---|
| award-dates | container for date information | optional |
| @start-date | actual start date of award | optional |
| @end-date | actual end date of the award | optional |
| @planned-start-date | planned start date of award | optional |
| @planned-end-date | planned end date of award | optional |

**Funding types:** Types of funding are limited to the following values:
* **award:** a prize, award, or other type of general funding
* **contract:** agreement involving payment
* **crowdfunding:** funding raised via multiple sources, typically small amounts raised online
* **endowment:** gift of money that will provide an income
* **equipment:** use of or gift of equipment
* **facilities:** use of location, equipment, or other resources
* **fellowship:** grant given for research or study
* **grant:** a monetary award
* **loan:** money or other resource given in anticipation of repayment
* **other:** award of undefined type
* **prize:** an award given for achievement
* **salary-award:** an award given as salary, includes intramural research funding
* **secondment:** detachment of a person or resource for temporary assignment elsewhere
* **seed-funding:** an investor invests capital in exchange for equity
* **training-grant:** grant given for training

### Grant metadata
We collect grant-specific metadata that is separate from the project information. This includes the funder-specific award identifier (grant number), the (optional) start date of the grant, related items, and the DOI and URL being registered.

| Element / attribute | Description | Limits |
|---|---|---|
| award-number | funder-supplied award ID /grant number | required |
| award-start-date | start date of grant funding | optional |
| relation (as rel:program) | relationship metadata connecting grant to other items (other grants, funded research outputs) | optional |
| DOI | DOI being registered | required |
| resource | URL of grant landing page | required |
