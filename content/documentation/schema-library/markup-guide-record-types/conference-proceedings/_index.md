+++
title = "Conference proceedings markup guide"
date = "2020-04-08"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["schema-library", "markup-guide-record-types", "conference-proceedings"]
identifier = "documentation/schema-library/markup-guide-record-types/conference-proceedings"
rank = 4
weight = 60606
aliases = [
    "/documentation/content-registration/content-type-markup-guide/conference-proceedings",
    "/documentation/content-registration/content-type-markup-guide/conference-proceedings/",
    "/education/content-registration/content-type-markup-guide/conference-proceedings",
    "/education/content-registration/content-type-markup-guide/conference-proceedings/"
]
+++

This guide gives markup examples for members registering conference proceedings by direct deposit of XML. You can also register the conference proceedings content type using one of our helper tools: [web deposit form](/documentation/content-registration/web-deposit-form/).

The *conference proceedings* content type captures metadata about a single conference, such as date, acronym, and location. DOIs should be assigned to all papers associated with the conference, and a DOI may be assigned to the conference itself. Ongoing conferences published with an ISSN may be deposited as a series.

`<conference>` is the container for all information about a single conference as well as the individual conference papers you are depositing for the conference. If you need to register articles for more than one conference, you must use multiple instances of `<conference>`.

Conference deposits require metadata about the event (captured in `<event_metadata>`) such as conference name (required) and theme, acronym, sponsor, location, and date (all optional), and proceedings-specific metadata such as a proceedings title, publisher, and date (all required) and subject (optional).

A conference may be deposited as a single conference or as a conference series. A conference series requires an ISSN and series-level title.

## Conference papers<a id='00062' href='#00062'><i class='fas fa-link'></i></a>

Conference paper metadata is captured in `<conference_paper>`. Contributor(s), title, and DOI are required. Abstracts, page numbers, publication date, citations, funding, license, and relationship metadata are optional but encouraged.

## Example of a single conference<a id='00063' href='#00063'><i class='fas fa-link'></i></a>

Review the sample below or [download an XML file](/xml-samples/conf.xml).

``` XML
<doi_batch xmlns="http://www.crossref.org/schema/4.3.7" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.crossref.org/schema/4.3.7 http://www.crossref.org/schemas/crossref4.3.7.xsd" version="4.3.7">
<head>
<doi_batch_id>1234</doi_batch_id>
<timestamp>20010910040315</timestamp>
<depositor>
<depositor_name>Sample Master</depositor_name>
<email_address>support@crossref.org</email_address>
</depositor>
<registrant>Association of Computing Machinery</registrant>
</head>
<body>
<conference>
<contributors>
<person_name sequence="first" contributor_role="chair">
<given_name>Peter</given_name>
<surname>Lee</surname>
</person_name>
<person_name sequence="additional" contributor_role="chair">
<given_name>Fritz</given_name>
<surname>Henglein</surname>
</person_name>
<person_name sequence="additional" contributor_role="chair">
<given_name>Neil D.</given_name>
<surname>Jones</surname>
</person_name>
</contributors>
<event_metadata>
<conference_name>24th ACM SIGPLAN-SIGACT symposium</conference_name>
<conference_theme>Algorithms & Computation Theory</conference_theme>
<conference_acronym>POPL '97</conference_acronym>
<conference_sponsor>L'École des Mines de Paris</conference_sponsor>
<conference_number>24</conference_number>
<conference_location>Paris, France</conference_location>
<conference_date start_month="01" start_year="1997" start_day="15" end_year="1997" end_month="01" end_day="17"/>
</event_metadata>
<proceedings_metadata language="en">
<proceedings_title>
Proceedings of the 24th ACM SIGPLAN-SIGACT symposium on Principles of programming languages - POPL '97
</proceedings_title>
<proceedings_subject>Principles of programming languages</proceedings_subject>
<publisher>
<publisher_name>ACM Press</publisher_name>
<publisher_place>New York</publisher_place>
</publisher>
<publication_date>
<year>1997</year>
</publication_date>
<isbn>0-89791-853-3</isbn>
<doi_data>
<doi>10.1145/263699</doi>
<timestamp>20010910040315</timestamp>
<resource>http://portal.acm.org/citation.cfm?doid=263699</resource>
</doi_data>
</proceedings_metadata>
<conference_paper publication_type="full_text">
<contributors>
<person_name sequence="first" contributor_role="author">
<given_name>Marc</given_name>
<surname>Shapiro</surname>
</person_name>
<person_name sequence="additional" contributor_role="author">
<given_name>Susan</given_name>
<surname>Horwitz</surname>
</person_name>
</contributors>
<titles>
<title>
Fast and accurate flow-insensitive points-to analysis
</title>
</titles>
<publication_date>
<year>1997</year>
</publication_date>
<pages>
<first_page>1</first_page>
<last_page>14</last_page>
</pages>
<doi_data>
<doi>10.1145/263699.263703</doi>
<resource>
http://portal.acm.org/citation.cfm?doid=263699.263703
</resource>
</doi_data>
</conference_paper>
<conference_paper publication_type="full_text">
<contributors>
<person_name sequence="first" contributor_role="author">
<given_name>Erik</given_name>
<surname>Ruf</surname>
</person_name>
</contributors>
<titles>
<title>Partitioning dataflow analyses using types</title>
</titles>
<publication_date>
<year>1997</year>
</publication_date>
<pages>
<first_page>15</first_page>
<last_page>26</last_page>
</pages>
<doi_data>
<doi>10.1145/263699.263705</doi>
<resource>
http://portal.acm.org/citation.cfm?doid=263699.263705
</resource>
</doi_data>
</conference_paper>
<conference_paper publication_type="full_text">
<contributors>
<person_name sequence="first" contributor_role="author">
<given_name>Pascal</given_name>
<surname>Fradet</surname>
</person_name>
<person_name sequence="additional" contributor_role="author">
<given_name>Daniel</given_name>
<surname>Le M&étayer</surname>
</person_name>
</contributors>
<titles>
<title>Shape types</title>
</titles>
<publication_date>
<year>1997</year>
</publication_date>
<pages>
<first_page>27</first_page>
<last_page>39</last_page>
</pages>
<doi_data>
<doi>10.1145/263699.263706</doi>
<resource>
http://portal.acm.org/citation.cfm?doid=263699.263706
</resource>
</doi_data>
</conference_paper>
</conference>
</body>
</doi_batch>
```

## Example of a conference that is part of a series<a id='00064' href='#00064'><i class='fas fa-link'></i></a>

Review the sample below or [download an XML file](/xml-samples/conf_series.xml).

``` XML
<doi_batch xmlns="http://www.crossref.org/schema/4.3.7" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.crossref.org/schema/4.3.7 http://www.crossref.org/schemas/crossref4.3.7.xsd" version="4.3.7">
<head>
<doi_batch_id>111111</doi_batch_id>
<timestamp>200503161011</timestamp>
<depositor>
<depositor_name>Sample Master</depositor_name>
<email_address>support@crossref.org</email_address>
</depositor>
<registrant>CrossRef</registrant>
</head>
<body>
<conference>
<event_metadata>
<conference_name>Crossref Annual Meeting</conference_name>
<conference_acronym>cam2005</conference_acronym>
<conference_date>Nov. 13, 2005</conference_date>
</event_metadata>
<proceedings_series_metadata>
<series_metadata>
<titles>
<title>CrossRef Annual Meeting Dummy Proceedings</title>
</titles>
<issn>5555-5555</issn>
</series_metadata>
<proceedings_title>Annual Meeting of PILA</proceedings_title>
<volume>3</volume>
<publisher>
<publisher_name>Crossref</publisher_name>
</publisher>
<publication_date media_type="print">
<month>11</month>
<day>23</day>
<year>2004</year>
</publication_date>
<isbn>5555555555</isbn>
<doi_data>
<doi>10.5555/cram05</doi>
<resource>http://www.crossref.org/cram05</resource>
</doi_data>
</proceedings_series_metadata>
<!-- ============== -->
<conference_paper>
<contributors>
<person_name sequence="first" contributor_role="author">
<given_name>Jim</given_name>
<surname>Jones</surname>
</person_name>
</contributors>
<titles>
<title>Crossref Sample Title</title>
</titles>
<publication_date media_type="print">
<month>12</month>
<day>31</day>
<year>2003</year>
</publication_date>
<pages>
<first_page>3</first_page>
<last_page>4</last_page>
</pages>
<doi_data>
<doi>10.5555/cdp0001</doi>
<resource>http://www.crossref.org/pubs/cdp0001</resource>
</doi_data>
</conference_paper>
</conference>
</body>
</doi_batch>
```
