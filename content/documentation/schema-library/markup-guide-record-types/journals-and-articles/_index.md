+++
title = "Journals and articles markup guide"
date = "2020-04-08"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["schema-library", "markup-guide-record-types", "journals-and-articles"]
identifier = "documentation/schema-library/markup-guide-record-types/journals-and-articles"
rank = 4
weight = 60616
aliases = [
    "/education/content-registration/content-type-markup-guide/journals-and-articles",
    "/education/content-registration/content-type-markup-guide/journals-and-articles/",
    "/education/content-registration/recommendations-by-content-type/journals-and-articles/",
    "/education/content-registration/recommendations-by-content-type/journals-and-articles",
    "/documentation/content-registration/content-type-markup-guide/journals-and-articles/",   
    "/documentation/content-registration/content-type-markup-guide/journals-and-articles"
]
+++

This guide gives markup examples for members registering journals and articles by direct deposit of XML. You can also register the journals and articles content type using one of our helper tools: [web deposit form](/documentation/content-registration/web-deposit-form/) or third party  [Crossref XML plugin for OJS](/documentation/content-registration/ojs-plugin/).

DOIs may be assigned to journal titles, volumes, issues, and (of course) journal articles. A title-level DOI is encouraged for all journals.

Assign DOIs to supplemental materials associated with journal articles using our [component](/documentation/content-registration/structural-metadata/components/) content type.

## Creating journal deposits<a id='00070' href='#00070'><i class='fas fa-link'></i></a>

`<journal>` is the container for all information about a single journal and the articles you are depositing for the journal. Within a single `<journal>` instance you may register articles for a single issue. If you need to register articles for more than one issue, you must use multiple instances of `<journal>`. These may be included within the same deposit file.

If you have articles that have not been assigned to an issue (or you do not use issue numbering) you may register them within a single journal instance. In this case, do not include `<journal_issue>` metadata.

If you publish in volumes only you must include `<journal_issue>` and the child element `<journal_volume>` but omit the `<issue>` number.

## Examples

A journal may be created with an ISSN or without. A DOI is not required but is strongly recommended and should remain consistent for all articles registered for the journal.
 Example title-only deposit files are available here: [with ISSN](https://www.crossref.org/xml-samples/journaltitle.xml) | [without ISSN](https://www.crossref.org/xml-samples/journaltitle4.3.7.xml).

### Example of a journal deposit containing several articles and issues<a id='00071' href='#00071'><i class='fas fa-link'></i></a>

Review the sample below or [download an XML file](/xml-samples/journal.xml).

``` XML
<doi_batch xmlns="http://www.crossref.org/schema/4.4.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="4.4.0" xsi:schemaLocation="http://www.crossref.org/schema/4.4.0 http://www.crossref.org/schemas/crossref4.4.0.xsd">
<head> / contains information related to submission file
 <doi_batch_id>123456</doi_batch_id> / required, used to track submissions
 <timestamp>19990628123304</timestamp> / required, must be incremented with each metadata record update
 <depositor>
  <depositor_name>Bob Surname</depositor_name> / person or entity submitting deposit
  <email_address>someone@crossref.org</email_address> / we'll send a submission log to this address
 </depositor>
 <registrant>CrossRef</registrant> / entity responsible for content being registered
</head>
<body>
 <journal>
  <journal_metadata language="en"> / captures journal-level metadata
   <full_title>Applied Physics Letters</full_title> / required, full title of the journal
   <abbrev_title>Appl. Phys. Lett.</abbrev_title> / abbreviated or alternate title
   <issn media_type="print">0003-6951</issn> / multiple ISSN may be supplied
   <coden>applab</coden> / optional
  </journal_metadata>
  <journal_issue> / captures volume and/or issue level metadata
   <publication_date media_type="print"> / publication year is required, month and day are encouraged
	<year>1999</year>
   </publication_date>
   <journal_volume>
	<volume>74</volume> / volume number
   </journal_volume>
	<issue>16</issue> / issue number
  </journal_issue>
  <journal_article publication_type="full_text">
   <titles>
 	<title>
Sub-5-fs visible pulse generation by pulse-front-matched noncollinear optical parametric amplification
 	</title> / article title
   </titles>
   <contributors> / author names
	<person_name sequence="first" contributor_role="author">
 	<given_name>Ann P.</given_name>
 	<surname>Shirakawa</surname>
	</person_name>
	<organization sequence="additional" contributor_role="author">Sample Organization</organization> / an organization that has authored the article
   </contributors>
   <publication_date media_type="print">
	<year>1999</year>
   </publication_date>
   <pages>
	<first_page>2268</first_page>
   </pages>
   <publisher_item>
	<identifier id_type="pii">S000369519903216</identifier> / optional, internal identifier
   </publisher_item>
   <doi_data>
	<doi>10.9876/S000369519903216</doi>
	<resource>http://ojps.crossref.org/link/?apl/74/2268/ab</resource> / the URL of your DOI landing page
   </doi_data>
  </journal_article>
  <journal_article publication_type="full_text"> / a second journal article in the same issue
   <titles>
	<title>Ultrafast (GaIn)(NAs)/GaAs vertical-cavity surface-emitting laser</title>
   </titles>
   <contributors>
	<person_name sequence="first" contributor_role="author">
 	<given_name>M</given_name>
 	<surname>van Exter</surname>
 	<suffix>III</suffix>
   </person_name>
   </contributors>
   <publication_date media_type="print">
	<year>1999</year>
   </publication_date>
   <pages>
	<first_page>2274</first_page>
   </pages>
   <publisher_item>
	<identifier id_type="pii">S0003695199038164</identifier>
   </publisher_item>
   <doi_data>
	<doi>10.9876/S0003695199034166</doi>
	<resource>http://ojps.aip.org/link/?apl/74/2271/ab</resource>
   </doi_data>
   <citation_list> / a citation list for the content being registered
	<citation key="key-10.9876/S0003695199034166-1">
  	<issn>0027-8424</issn>
  	<journal_title>Proc. Natl. Acad. Sci. U.S.A.</journal_title>
  	<author>West</author>
  	<volume>98</volume>
  	<issue>20</issue>
  	<first_page>11024</first_page>
  	<cYear>2001</cYear>
	</citation>
	<citation key="key-10.9876/S0003695199034166-2">
 	<journal_title>Space Sci. Rev.</journal_title>
 	<author>Heber</author>
 	<volume>97</volume>
 	<first_page>309</first_page>
 	<cYear>2001</cYear>
	</citation>
	<citation key="key-10.9876/S0003695199034166-3">
 	<doi>10.1029/2002GL014944</doi>
	</citation>
	<citation key="key-10.9876/S0003695199034166-4">
 	<journal_title>Dev. Dyn.</journal_title>
 	<author>Tufan</author>
 	<cYear>2002</cYear>
	</citation>
	<citation key="key-10.9876/S0003695199034166-5">
 	<journal_title>J. Plankton Res.</journal_title>
 	<author>Bocher</author>
	</citation>
	<citation key="key-10.9876/S0003695199034166-6">
 	<issn>0169-6009</issn>
 	<journal_title>Bone. Miner.</journal_title>
 	<author>
 	Proyecto Multicéntrico de Investigación de Osteoporosis
 	</author>
 	<volume>17</volume>
 	<first_page>133</first_page>
 	<cYear>1992</cYear>
	</citation>
	<citation key="key-10.9876/S0003695199034166-7">
 	<volume_title>Neuropharmacological Basis of Reward</volume_title>
 	<author>Carr</author>
 	<first_page>265</first_page>
 	<cYear>1989</cYear>
	</citation>
	<citation key="key-10.9876/S0003695199034166-8">
 	<volume_title>The Metabolic and Molecular Bases of Inherited Disease
 	</volume_title>
 	<author>Mahley</author>
 	<edition_number>7</edition_number>
 	<first_page>1953</first_page>
 	<cYear>1995</cYear>
	</citation>
	<citation key="key-10.9876/S0003695199034166-9">
 	<series_title>Genome Analysis</series_title>
 	<volume_title>Genetic and Physical Mapping</volume_title>
 	<author>Rinchik</author>
 	<volume>1</volume>
 	<first_page>121</first_page>
 	<cYear>1990</cYear>
	</citation>
	<citation key="key-10.9876/S0003695199034166-10">
 	<volume_title>Molecular Cloning: A Laboratory Manual</volume_title>
 	<author>Sambrook</author>
 	<cYear>1989</cYear>
	</citation>
	<citation key="key-10.9876/S0003695199034166-11">
 	<volume_title>Immunocytochemistry: Theory and Practice</volume_title>
 	<author>Larsson</author>
 	<first_page>41</first_page>
 	<cYear>1988</cYear>
 	<component_number>2</component_number>
	</citation>
 	<citation key="key-10.9876/S0003695199034166-12">
 	<volume_title>
 	Proceedings of the XXVth International Conference on Animal Genetics
 	</volume_title>
 	<author>Roenen</author>
 	<first_page>105</first_page>
 	<cYear>1996</cYear>
	</citation>
	<citation key="key-10.9876/S0003695199034166-13">
 	<volume_title>
 	Proceedings of the Corn and Sorghum Industry Research Conference
 	</volume_title>
 	<author>Beavis</author>
 	<first_page>250</first_page>
 	<cYear>1994</cYear>
	</citation>
	<citation key="key-10.9876/S0003695199034166-14">
 	<unstructured_citation>L. Reynolds, Three dimensional reflection and transmission equations for optical diffusion in blood, MS Thesis. Seattle, Washington: University of Washington (1970).</unstructured_citation>
	</citation>
	<citation key="key-10.9876/S0003695199034166-15">
 	<unstructured_citation>T. J. Dresse, U.S. Patent 308, 389 [<i>CA 82</i>, 73022 (1975)].</unstructured_citation>
	</citation>
   </citation_list>
  </journal_article>
 </journal>
 <journal> / start of a new journal submission
  <journal_metadata language="en">
   <full_title>Applied Physics Letters</full_title>
   <abbrev_title>Appl. Phys. Lett.</abbrev_title>
   <issn media_type="print">0003-6951</issn>
   <coden>applab</coden>
  </journal_metadata>
  <journal_issue>
   <publication_date media_type="print">
	<year>1999</year>
   </publication_date>
   <journal_volume>
	<volume>74</volume>
   </journal_volume>
   <issue>1</issue>
  </journal_issue>
 <journal_article publication_type="full_text">
  <titles>
   <title>Working with metadata</title>
   </titles>
  <contributors>
  <person_name sequence="first" contributor_role="author">
   <given_name>D L</given_name>
   <surname>Peng</surname>
  </person_name>
  <person_name sequence="additional" contributor_role="author">
   <given_name>K</given_name>
   <surname>Sumiyama</surname>
  </person_name>
  <person_name sequence="additional" contributor_role="author">
   <given_name>S</given_name>
   <surname>Sumiyama</surname>
  </person_name>
  <person_name sequence="additional" contributor_role="author">
   <given_name>T</given_name>
   <surname>Hihara</surname>
  </person_name>
  <person_name sequence="additional" contributor_role="author">
   <given_name>T J</given_name>
   <surname>Konno</surname>
  </person_name>
 </contributors>
 <publication_date media_type="print">
  <year>1999</year>
 </publication_date>
 <pages>
  <first_page>76</first_page>
 </pages>
 <doi_data>
  <doi>10.9876/S0003695199019014</doi>
  <resource>http://ojps.crossref.org:18000/link/?apl/74/1/76/ab</resource>
 </doi_data>
 <component_list> / component (supplemental material) registration records
  <component parent_relation="isPartOf">
   <description>Figure 1: This is the caption of the first figure...</description>
   <format mime_type="image/jpeg">Web resolution image</format>
  <doi_data>
   <doi>10.9876/S0003695199019014/f1</doi>
   <resource>http://ojps.crossref.org:18000/link/?apl/74/1/76/f1</resource>
  </doi_data>
 </component>
 <component parent_relation="isReferencedBy">
  <description>Video 1: This is a description of the video...</description>
  <format mime_type="video/mpeg"/>
  <doi_data>
   <doi>10.9876/S0003695199019014/video1</doi>
   <resource>http://ojps.crossref.org:18000/link/?apl/74/1/76/video1</resource>
  </doi_data>
 </component>
</component_list>
</journal_article>
</journal>
</body>
</doi_batch>
```
