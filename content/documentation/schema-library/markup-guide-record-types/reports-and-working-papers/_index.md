+++
title = "Reports and working papers markup guide"
date = "2020-04-08"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["schema-library", "markup-guide-record-types", "reports-and-working-papers"]
identifier = "documentation/schema-library/markup-guide-record-types/reports-and-working-papers"
rank = 5
weight = 60626
aliases = [
    "/education/content-registration/content-type-markup-guide/reports-and-working-papers",
    "/education/content-registration/content-type-markup-guide/reports-and-working-papers/",
    "/education/content-registration/recommendations-by-content-type/reports-and-working-papers",
    "/education/content-registration/recommendations-by-content-type/reports-and-working-papers/",
    "/documentation/content-registration/content-type-markup-guide/reports-and-working-papers",
    "/documentation/content-registration/content-type-markup-guide/reports-and-working-papers/"
]
+++

This page gives markup examples for members registering reports and working papers by direct deposit of XML. You can also register the reports and working papers content type using one of our helper tools: [web deposit form](/documentation/content-registration/web-deposit-form/).

`<report-paper>` is the container for all information about a single report or working paper. If you need to register articles for more than one report, you must use multiple instances of `<report-paper>`. These may be included within the same deposit file.

Technical reports and working papers are typically assigned a single identifier, but identifiers may also be assigned to sub-sections of the report (such as chapters) as needed using the `<content_item>` element. Report registration files may include a publisher name (within `<publisher>`) and/or institution name (within `<institution>` depending on the organization issuing the report.

Reports/working papers may also be deposited as a series.

## Example of a single report<a id='00095' href='#00095'><i class='fas fa-link'></i></a>

Review the sample below or [download an XML file](/xml-samples/report.xml).

``` XML
<doi_batch xmlns="http://www.crossref.org/schema/4.3.7" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="4.3.7" xsi:schemaLocation="http://www.crossref.org/schema/4.3.7 http://www.crossref.org/schemas/crossref4.3.7.xsd">
<head>
<doi_batch_id>123456</doi_batch_id>
<timestamp>20050606110604</timestamp>
<depositor>
<depositor_name>Sample Master</depositor_name>
<email_address>support@crossref.org</email_address>
</depositor>
<registrant>CrossRef</registrant>
</head>
<body>
<report-paper>
<report-paper_metadata language="en">
<contributors>
<person_name sequence="first" contributor_role="author">
<given_name>D.S.</given_name>
<surname>McShane</surname>
</person_name>
</contributors>
<titles>
<title>
Title Sludge Handling System Conceptual Design Document
</title>
</titles>
<edition_number>0</edition_number>
<publication_date media_type="online">
<month>03</month>
<day>03</day>
<year>2001</year>
</publication_date>
<publisher>
<publisher_name>Office of Scientific and Technical Information</publisher_name>
<publisher_place>Washington, DC</publisher_place>
</publisher>
<institution>
<institution_name>United States Department of Energy</institution_name>
<institution_acronym>USDOE(EM)</institution_acronym>
<institution_place>Washington, DC</institution_place>
<institution_department>Office of Environmental Management</institution_department>
</institution>
<institution>
<institution_name>Fluor Daniel Northwest</institution_name>
<institution_acronym>FDNW</institution_acronym>
<institution_place>Aliso Viejo, CA</institution_place>
</institution>
<publisher_item>
<identifier id_type="report-number">abc123</identifier>
</publisher_item>
<contract_number>AC06-96RL13200</contract_number>
<doi_data>
<doi>10.9999/osti-806888</doi>
<resource>
http://198.232.211.23/pdwdocs/fsd0001/osti/2001/I0004856.pdf
</resource>
</doi_data>
</report-paper_metadata>
</report-paper>
</body>
</doi_batch>
```

## Example of a report with ‘chapters’<a id='00096' href='#00096'><i class='fas fa-link'></i></a>

Review the sample below or [download an XML file](/xml-samples/report.chapters.xml).

``` XML
<?xml version="1.0" encoding="UTF-8"?>
<doi_batch version="4.3.7" xmlns="http://www.crossref.org/schema/4.3.7" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.crossref.org/schema/4.3.7
http://www.crossref.org/schemas/crossref4.3.7.xsd">
<head>
<doi_batch_id>123456</doi_batch_id>
<timestamp>20050606110604</timestamp>
<depositor>
<depositor_name>Sample Master</depositor_name>
<email_address>support@crossref.org</email_address>
</depositor>
<registrant>CrossRef</registrant>
</head>
<body>
<report-paper>
<report-paper_metadata language = "en">
<contributors>
<person_name sequence = "first" contributor_role = "author">
<given_name>D.S.</given_name>
<surname>McShane</surname>
</person_name>
</contributors>
<titles>
<title>Title Sludge Handling System Conceptual Design Document</title>
</titles>
<edition_number>0</edition_number>
<publication_date media_type = "online">
<month>03</month>
<day>03</day>
<year>2001</year>
</publication_date>
<publisher>
<publisher_name>Office of Scientific and Technical Information</publisher_name>
<publisher_place>Washington, DC</publisher_place>
</publisher>
<institution>
<institution_name>United States Department of Energy</institution_name>
<institution_acronym>USDOE(EM)</institution_acronym>
<institution_place>Washington, DC</institution_place>
<institution_department>Office of Environmental Management</institution_department>
</institution>
<institution>
<institution_name>Fluor Daniel Northwest</institution_name>
<institution_acronym>FDNW</institution_acronym>
<institution_place>Aliso Viejo, CA</institution_place>
</institution>
<publisher_item>
<identifier id_type="report-number">abc123</identifier>
</publisher_item>
<contract_number>AC06-96RL13200</contract_number>
<doi_data>
<doi>10.9999/abcd-806888</doi>
<resource>http://www.crossref.org/pdwdocs/fsd0001/osti/2001/I0004856.pdf</resource>
</doi_data>
</report-paper_metadata>
<content_item component_type="part">
<titles><title>Introduction</title></titles>
<pages><first_page>3</first_page></pages>
<doi_data>
<doi>10.9999/abcd-806888.p1</doi>
<resource>http://www.crossref.org/pdwdocs/fsd0001/osti/2001/p1</resource>
</doi_data>
</content_item>
<content_item component_type="part">
<titles><title>A chapter title</title></titles>
<pages><first_page>17</first_page></pages>
<doi_data>
<doi>10.9999/abcd-806888.p2</doi>
<resource>http://www.crossref.org/pdwdocs/fsd0001/osti/2001/p2</resource>
</doi_data>
</content_item>
<content_item component_type="part">
<titles><title>Appendix</title></titles>
<pages><first_page>32</first_page></pages>
<doi_data>
<doi>10.9999/abcd-806888.p3</doi>
<resource>http://www.crossref.org/pdwdocs/fsd0001/osti/2001/p3</resource>
</doi_data>
</content_item>
</report-paper>
</body>
</doi_batch>
```

## Example of a report series<a id='00097' href='#00097'><i class='fas fa-link'></i></a>

Review the sample below or [download an XML file](/xml-samples/report_series.xml).

``` XML
<doi_batch xmlns="http://www.crossref.org/schema/4.3.7" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="4.3.7" xsi:schemaLocation="http://www.crossref.org/schema/4.3.7 http://www.crossref.org/schemas/crossref4.3.7.xsd">
<head>
<doi_batch_id>123456</doi_batch_id>
<timestamp>20050606110604</timestamp>
<depositor>
<depositor_name>CrossRef</depositor_name>
<email_address>support@crossref.org</email_address>
</depositor>
<registrant>CrossRef</registrant>
</head>
<body>
<report-paper>
<report-paper_series_metadata language="en">
<series_metadata>
<titles>
<title>CrossRef Report Series</title>
</titles>
<issn>5555-5555</issn>
</series_metadata>
<contributors>
<person_name sequence="first" contributor_role="author">
<given_name>Bob</given_name>
<surname>Surname</surname>
</person_name>
</contributors>
<titles>
<title>Depositing report series with CrossRef</title>
</titles>
<edition_number>0</edition_number>
<publication_date media_type="online">
<month>03</month>
<day>03</day>
<year>2009</year>
</publication_date>
<publisher>
<publisher_name>Publishers International Linking Association</publisher_name>
<publisher_place>Lynnfield, MA</publisher_place>
</publisher>
<institution>
<institution_name>CrossRef</institution_name>
<institution_acronym>CR</institution_acronym>
<institution_place>Lynnfield, MA</institution_place>
<institution_department>Metadata Quality</institution_department>
</institution>
<publisher_item>
<item_number item_number_type="Report Number">IMA-RPT</item_number>
</publisher_item>
<contract_number>AC06-96RL13200</contract_number>
<doi_data>
<doi>10.5555/sampledoi</doi>
<resource>http://www.crossref.org/report/</resource>
</doi_data>
</report-paper_series_metadata>
</report-paper>
</body>
</doi_batch>
```
