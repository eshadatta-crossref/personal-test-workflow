+++
title = "Standards markup guide"
date = "2022-05-31"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["schema-library", "markup-guide-record-types", "standards"]
identifier = "documentation/schema-library/markup-guide-record-types/standards"
rank = 4
weight = 60630
aliases = [
    "/education/content-registration/content-type-markup-guide/standards",
    "/education/content-registration/content-type-markup-guide/standards/",
    "/documentation/content-registration/content-type-markup-guide/standards/",
    "/documentation/content-registration/content-type-markup-guide/standards"
]
+++

This page gives markup examples for members registering standards by direct deposit of XML. It is not currently possible to register the standards content type using one of our helper tools.

`<standard>` is the top-level element for deposit of metadata about standards developed by Standards Development Organizations (SDOs) or consortia. Standards are assigned a DOI at the title level and may also have DOIs assigned to lower level `content-items`. Standards deposits contain several pieces of standard-specific metadata, including standard designators and standards body information. Standards may only be deposited with schema version 4.3.6 and above.

## Designator types<a id='00098' href='#00098'><i class='fas fa-link'></i></a>

All standards have a designator that is used as a primary identifier. [crossref4.3.6.xsd](https://data.crossref.org/reports/help/schema_doc/4.3.6/4.3.6.html) allows for a number of designator types to be applied to a DOI. A primary designator must be included in each metadata deposit. One of the following designator types must be supplied:

* **As-published:** captured in `<std_designator>` (child of `<std_as_published>`), designator for the standard being deposited. This is an item-level designator. Typically includes the year of initial publication plus additional information such as amendment number and/or revision/reaffirmation year, for example: ASTM D6/D6M-95(2011)e1 is a designator for [an ASTM standard](http://doi.org/10.1520/D0006_D0006M-95R11E01). Any undated, family, and set designators related to the designator supplied in `<std_designator>` may be recorded in the corresponding attributes, for example:

```
<std_designator undated="ASTM D6/D6M-95"> ASTM D6/D6M-95(2011)</std_designator>
```

* *Optional*: an alternative as-published designator may be recorded in `<std_alt_as_published>`.This is intended to accommodate minor changes to a standard that do not merit assigning a new DOI. A variant form designator (see below) may also be supplied to accommodate differing forms of a designator
* **Undated**: captured in `<std_undated_designator>`. An undated designator removes the year component that specifies a particular revision. Undated designators refer to a single document series. For example: ASTM C90, IEC 60601-2-11, ISO/IEC 19757-2
* **Family**: captured in `<std_family_designator>`, a collection of standards which are conceptually grouped together where that grouping is not necessarily reflected in the designator in an obvious way.For example, the ISO 9000 family includes ISO 9001, ISO 9004, ISO 19011
* **Set**: captured in `<std_set_designator>`. A set, also referred to as truncated form, is composed of several parts (standards that are divided into separate documents). For example, ISO 19757 is a standard in 11 parts, with the individual documents known as ISO 19757-2 where the "-" denotes a part.

### Optional designators<a id='00099' href='#00099'><i class='fas fa-link'></i></a>

Some optional designators may be supplied in deposits *in addition to* a required dated, undated, family, or set designator. They are supplied to accommodate query matching but are not considered title-level designators. These are:

* **Variant form**: Alternative versions of a designator may be supplied in `<std_variant_form>`. Variant form captures stylized forms that don’t accurately reflect the true standard designator but are needed due to business practices (for example, IEEE formal designators have "std" while the display of them does not). Variant forms may be applied to `<std_alt_as_published>`, `<std_as_published>`, `<std_set_designator>` and `<std_undated_designator>`
* **Alternative script**: captured in `<std_alt_script>`, accommodates designators that are published using multiple character sets
* **Supersedes**: captured in `<std_supersedes>`. Designator for standard being replaced by the standard being deposited.
* **Adopted from**: captured in `<std_adopted_from>`. Designator for standard from which the current deposit is adopted.
* **Revision of**: captured in `<std_revision_of>`. Designator for the previous revision of the standard being deposited.

## Standards body information<a id='00100' href='#00100'><i class='fas fa-link'></i></a>

The `<standards_body>` wrapper element has two children:

* `<standards_body_name>`: the full name of the standards body
* `<standards_body_acronym>`: acronym of the standards body

Both child elements are required and should reflect the standards body name and acronym used when the standard was published. Changes in standards body names and acronyms over time will be accounted for within Crossref’s query mechanism.

## History<a id='00101' href='#00101'><i class='fas fa-link'></i></a>

Crossref began accepting metadata deposits for standards in 2005. The schema was modified significantly for standards with the inception of the Standards Technical Working Group. Significant changes to the deposit and indexing of designators were made with schema version 4.3.6, as a result standards may only be deposited schema versions 4.3.6 and above.

## Example of a standard deposit<a id='00102' href='#00102'><i class='fas fa-link'></i></a>

Review the sample below or [download an XML file](/xml-samples/standard.xml).

``` XML
<doi_batch xmlns="http://www.crossref.org/schema/4.3.7" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="4.3.7" xsi:schemaLocation="http://www.crossref.org/schema/4.3.7 http://www.crossref.org/schemas/crossref4.3.7.xsd">
<head>
<doi_batch_id>123456</doi_batch_id>
<timestamp>20050606110604</timestamp>
<depositor>
<depositor_name>CrossRef</depositor_name>
<email_address>pfeeney@crossref.org</email_address>
</depositor>
<registrant>CrossRef</registrant>
</head>
<body>
<standard>
<standard_metadata language="en">
<contributors>
<organization sequence="first" contributor_role="author">CrossRef Standards TWG</organization>
</contributors>
<titles>
<title>Fun with standards</title>
</titles>
<designators>
<std_as_published>
<std_designator>CR 1234</std_designator>
</std_as_published>
</designators>
<approval_date>
<month>04</month>
<day>17</day>
<year>1995</year>
</approval_date>
<publisher>
<publisher_name>CrossRef</publisher_name>
<publisher_place>Bethesda, MD</publisher_place>
</publisher>
<standards_body>
<standards_body_name>CrossRef</standards_body_name>
<standards_body_acronym>CRABC</standards_body_acronym>
</standards_body>
<publisher_item>
<item_number item_number_type="designation">12083</item_number>
</publisher_item>
<doi_data>
<doi>10.5555/standard1</doi>
<resource>http://www.crossref.org/abc</resource>
</doi_data>
</standard_metadata>
</standard>
</body>
</doi_batch>
```
