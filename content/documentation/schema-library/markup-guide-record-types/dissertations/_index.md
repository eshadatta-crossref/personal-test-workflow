+++
title = "Dissertations markup guide"
date = "2020-04-08"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["schema-library", "markup-guide-record-types", "dissertations"]
identifier = "documentation/schema-library/markup-guide-record-types/dissertations"
rank = 4
weight = 60612
aliases = [
    "/documentation/content-registration/content-type-markup-guide/dissertations/",
    "/documentation/content-registration/content-type-markup-guide/dissertations",
    "/education/content-registration/content-type-markup-guide/dissertations",
    "/education/content-registration/content-type-markup-guide/dissertations/",
    "/education/content-registration/recommendations-by-content-type/dissertations/",
    "/education/content-registration/recommendations-by-content-type/dissertations"
]
+++

This guide gives markup examples for members registering dissertations by direct deposit of XML. You can also register the dissertations content type using one of our helper tools: [web deposit form](/documentation/content-registration/web-deposit-form/).

Dissertation records may be deposited for a single dissertation or thesis. DOIs are not assigned to collections of dissertations. The dissertation type should be used for content items which have not been published in books or journals. If a dissertation is published as a book or within a serial, it should be deposited with the appropriate content type.

## Constructing dissertation deposits<a id='00068' href='#00068'><i class='fas fa-link'></i></a>

A dissertation deposit requires a title, a single author (deposited as `<person_name>`), institution, and approval date. Degree, ISBN, and record number information may also be included.

If a Dissertation Abstracts International (DAI) number has been assigned, it should be deposited in the identifier element with the `id_type` attribute set to `dai`. If an institution has its own numbering system, it should be deposited in `<item_number>`, and the `item_number_type` should be set to `institution`.

See also our [dissertation example XML file](https://gitlab.com/crossref/schema/-/blob/master/best-practice-examples/dissertation.5.3.0.xml).
