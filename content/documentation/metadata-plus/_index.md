+++
title = "Metadata Plus"
date = "2020-04-08"
draft = false
author = "Jennifer Kemp"
type = "documentation"
layout = "documentation_single"
documentation_section = ["metadata-plus"]
aliases = [
  "/education/metadata-plus",
  "/education/metadata-plus/"

]
[menu.documentation]
identifier = "documentation/metadata-plus"
parent = "Documentation"
rank = 4
weight = 500000
+++

{{< snippet "/_snippet-sources/metadata-plus.md" >}}
