+++
title = "Updating metadata for inherited DOIs after a title transfer"
date = "2020-10-06"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["register-maintain-records", "maintaining-your-metadata", "updating-after-title-transfer"]
identifier = "documentation/register-maintain-records/maintaining-your-metadata/updating-after-title-transfer"
rank = 4
weight = 140105
aliases =[
  "/education/metadata-stewardship/maintaining-your-metadata/updating-after-title-transfer",
  "/education/metadata-stewardship/maintaining-your-metadata/updating-after-title-transfer/",
  "/documentation/metadata-stewardship/maintaining-your-metadata/updating-after-title-transfer/",
  "/documentation/metadata-stewardship/maintaining-your-metadata/updating-after-title-transfer"
]
+++

If you’ve acquired titles from another publisher, you may have also acquired existing DOIs and metadata that were previously registered. Although these DOIs won't be on your prefix, these DOIs will now be your responsibility and you'll be able to update the metadata associated with them.

Don't try to register new DOIs on your prefix for content that already has a DOI. Instead, you should just update the metadata for these DOIs if you want to change something.

## Confirming your acquired DOIs<a id='00170' href='#00170'><i class='fas fa-link'></i></a>

If you aren't sure which DOIs have already been registered for a particular title, look at the [depositor report](/documentation/reports/depositor-report/) for the title. Please note: the depositor report updates at 0100 UTC each day, so a new publisher may need to wait until the next day to see its newly acquired titles.

Go to the depositor report and look for your organization name. You might need to wait a while for the page to load properly - it's a bit slow. Click on your name and you'll see the list of titles associated with your organization. Click on the recently acquired title and you'll see all the DOIs that are currently registered for it.

## Updating the existing metadata for acquired titles<a id='00623' href='#00623'><i class='fas fa-link'></i></a>

When you acquire a title from another publisher and we perform a title transfer for you, the publisher name will update in the metadata automatically.

However, there are likely to be some elements that you need to update yourself. For example, you may need to change the [resolution URLs](/documentation/content-registration/administrative-metadata/resource-resolution-url/). And you may also need to change the [full-text URLs for text and data mining](/documentation/retrieve-metadata/rest-api/text-and-data-mining-for-members/) or [Similarity Check URLs](/documentation/similarity-check/participate#00040) if the previous publisher has submitted them. Or you may need to change a [license URL](/documentation/content-registration/administrative-metadata/license-information/) that the previous publisher has submitted.

To add, change, or remove information from your metadata records, you generally need to resubmit your complete metadata record with the changes included.

However, there are a few exceptions to this, and changes to resolution URLs is an important one - you may need our help here. Learn more about [updating your resolution URLs](/documentation/register-maintain-records/maintaining-your-metadata/updating-your-metadata#00171).

## Finding the existing XML for acquired DOIs<a id='00175' href='#00175'><i class='fas fa-link'></i></a>

If you wish to see what is in the metadata that you have inherited, you can retrieve the metadata as an XML file using either the [deposit harvester](/documentation/retrieve-metadata/deposit-harvester), or one of these [REST API](/documentation/retrieve-metadata/rest-api) queries. If you plan to use a REST API query, we suggest installing a JSON formatter in your browser.

* To retrieve all items by ISSN, use this API query: `http://api.crossref.org/works?filter=issn:2090-8091&rows=1000` and replace *2090-8091* with the ISSN for your title
* To retrieve all items by title, use this API query: `http://api.crossref.org/works?query.container-title=Connections` and replace *Connections* with your title
* You can adjust the API query to retrieve just one element per DOI, such as full-text links (including for Similarity Check) - replace *2090-8091* with the ISSN for your title: `http://api.crossref.org/works?filter=issn:2090-8091&rows=1000&select=DOI,link`
* To transform the JSON to XML for individual records, append your API call with `.xml`, like this: (unsupported, so please do not rely on this feature)
  * [https://api.crossref.org/works/10.12794/journals.ntjur.v1i1.68](https://api.crossref.org/works/10.12794/journals.ntjur.v1i1.68) (JSON)
  * [https://api.crossref.org/works/10.12794/journals.ntjur.v1i1.68.xml](https://api.crossref.org/works/10.12794/journals.ntjur.v1i1.68.xml) (XML)

If you register your content with us by sending us XML files, you can just edit this XML to remove or replace metadata, and then redeposit the XML.

Note: for most metadata elements, you can just update the XML record and resubmit to delete elements. However, there are some non-bibliographic metadata elements where you have to go through a two-step process -  firstly send us a submission to delete this element, and then send us a further submission to add in the replacement data. Learn more about [updating your metadata](/documentation/register-maintain-records/maintaining-your-metadata/updating-your-metadata/).
