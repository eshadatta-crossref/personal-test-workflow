+++
title = "XML deposit using HTTPS POST"
date = "2022-10-28"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["register-maintain-records", "direct-deposit-xml", "https-post"]
identifier = "documentation/register-maintain-records/direct-deposit-xml/https-post"
rank = 4
weight = 50603
aliases = [
	"/education/member-setup/direct-deposit-xml/https-post",
	"/education/member-setup/direct-deposit-xml/https-post/",
	"/education/member-setup/xml-deposit-via-https-post/",
	"/education/member-setup/xml-deposit-via-https-post",
	"/documentation/member-setup/direct-deposit-xml/https-post",
	"/documentation/member-setup/direct-deposit-xml/https-post/",
	"/documentation/content-registration/direct-deposit-xml/https-post/",
	"/documentation/content-registration/direct-deposit-xml/https-post"
]
+++

XML files can be POSTed to our system where they are added to the submission queue to await processing. You can do this yourself or make use of our [java program](/documentation/member-setup/direct-deposit-xml/https-post-using-java-program).

On this page, find out more about:
* [Submitting files - an overview](#00494)
* [Sample transactions](#00230)
* [Submission limits](#00493)

## Submitting files - an overview<a id='00494' href='#00494'><i class='fas fa-link'></i></a>

Uploading files (for deposit or for bulk queries) are submitted using HTTPS POST with the `encType: multipart/form-data`. The URL for all submissions is [https://doi.crossref.org/servlet/deposit](https://doi.crossref.org/servlet/deposit). You may also POST submissions to our test system using [https://test.crossref.org/servlet/deposit](https://test.crossref.org/servlet/deposit). Learn more about our [test system](/documentation/member-setup/direct-deposit-xml/testing-your-xml/).

The following parameters are supported:

|Form field|Description|Possible values|Mandatory?|Default|
|--- |--- |--- |--- |--- |
|operation|Depends on submission type|**doMDUpload**: For [metadata (XSD)](/documentation/content-registration/metadata-deposit-schema/) submissions (or, (full XML) metadata upload) </br> **doDOICitUpload**: For DOI citations or resources submissions (or, (resource-only) DOI resources) </br> **doQueryUpload**: For query submissions </br> **doDOIQueryUpload**: For DOI-to-metadata query submissions </br> **doTransferDOIsUpload**: For submissions to update resource resolution URLs |No|doMDUpload|
|subType|subtype for metadata submissions|**cm**: for [conflict management](/documentation/reports/conflict-report) submissions|No|Not applicable|
|login_id|[Crossref account credentials](/documentation/member-setup/account-credentials/) username|If using shared role credentials, add the role. If using personal user credentials, add your email address and the role in this format email@address.com/role|Yes|Not applicable|
|login_passwd|[Crossref account credentials](/documentation/member-setup/account-credentials/) password|Not applicable|Yes|Not applicable|
|**Content parts**|
|fname|Submission contents|Not applicable|Yes|Not applicable|

## Sample transactions<a id='00230' href='#00230'><i class='fas fa-link'></i></a>

### Sample transaction using curl

```
curl -F 'operation=doQueryUpload' -F 'login_id=USERNAME' -F 'login_passwd=PASSWORD' -F 'fname=@FILENAME' https:// doi.crossref.org/servlet/deposit
```
Note: several members have contacted us about use of the @ in the above sample transaction; it should be retained prior to the FILENAME

### Sample https transaction

```
POST https://doi-crossref-org/servlet/deposit?operation=doMDUpload&login_id=USER&login_passwd=PSWD HTTP/1.1
Accept: image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, */*
Accept-Language: en-us
Content-Type: multipart/form-data; boundary=---------------------------7d22911b10028e
User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; Q312461)
Host: Myhost
Content-length: 1304 Pragma: no-cache -----------------------------7d22911b10028e
Content-Disposition: form-data; name="fname"; filename="crossref_query.xml"
<?xml version="1.0" encoding="UTF-8"?>
<doi_batch version="4.3.0" xmlns="http://www.crossref.org/schema/4.3.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.crossref.org/schema/4.3.0 http://www.crossref.org/schemas/crossref4.3.0.xsd">
<head>
...
</head>
<body>
<journal>
....
</journal>
</body>
</doi_batch>
-----------------------------7d22911b10028e--
```
For backward compatibility, we also accept the login_id, login_passwd, operation, and area parameters in a multi-part request:

```
-----------------------------7d22911b10028e
Content-Disposition: form-data; name="login_id"
atypon
-----------------------------7d22911b10028e
Content-Disposition: form-data; name="login_passwd"
_atypon_
-----------------------------7d22911b10028e
Content-Disposition: form-data; name="fname"; filename="hisham.xml"

... file contents ...

```

## Submission limits<a id='00493' href='#00493'><i class='fas fa-link'></i></a>

We have a limit of 10,000 pending submissions per user. If there are 10,000 submissions in our queue for a given user, subsequent uploads will fail with a 429 error. You may resume POSTing when pending submissions are below 10,000.
