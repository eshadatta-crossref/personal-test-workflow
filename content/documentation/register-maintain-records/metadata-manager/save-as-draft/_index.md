+++
title = "Save as draft"
date = "2022-07-22"
draft = false
author = "Sara Bowman"
type = "documentation"
layout = "documentation_single"
documentation_section = ["register-maintain-records", "metadata-manager", "save-as-draft"]
identifier = "documentation/register-maintain-records/metadata-manager/save-as-draft"
rank = 4
weight = 50503
aliases = [
	"/education/member-setup/metadata-manager/save-as-draft",
	"/education/member-setup/metadata-manager/save-as-draft/"
]
+++

{{< snippet "/_snippet-sources/metadata-manager-deprecated.md" >}}

If the record you’re working on is not yet complete, you can choose to "save as draft". Your record will be saved within Metadata Manager for you to work on later, but it won’t yet be submitted to our system. This is true whether your record is completely new, or you’re using Metadata Manager to update the metadata record for a DOI you’ve already registered with Crossref. When you want to submit the changes to Crossref, you must choose *add to deposit cart* and [resubmit your deposit](/documentation/register-maintain-records/metadata-manager/review-and-submit/).

To save your record for later, go to *Continue*, and click *Save as draft*.

{{< figure-linked
	src="/images/documentation/MM-save-as-draft.png"
	large="/images/documentation/MM-save-as-draft.png"
	alt="Metadata Manager save as draft function"
	title="Metadata Manager save as draft function.png"
	id="image129"
>}}

When you are ready to continue editing the record or submit it, you can find it again under your workspace.
