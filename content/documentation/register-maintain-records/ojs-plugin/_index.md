+++
title = "Crossref XML plugin for OJS"
date = "2022-11-04"
draft = false
author = "Anna Tolwinska"
type = "documentation"
layout = "documentation_single"
documentation_section = ["register-maintain-records", "ojs-plugin"]
identifier = "documentation/register-maintain-records/ojs-plugin"
rank = 4
weight = 50300
aliases = [
	"/education/member-setup/ojs-plugin",
	"/education/member-setup/ojs-plugin/",
	"/documentation/member-setup/ojs-plugin/",
	"/documentation/member-setup/ojs-plugin",
	"/documentation/content-registration/ojs-plugin/",
	"/documentation/content-registration/ojs-plugin"
]
+++

If you use the [Open Journal Systems (OJS)](https://pkp.sfu.ca/ojs/) platform from PKP, you should use [PKP’s OJS Crossref XML plugin](https://docs.pkp.sfu.ca/crossref-ojs-manual/en/config) to register your content with us. To make best use of the plugin, make sure you’re using OJS version 3 or higher.

You can set up the plugin to work in one of two ways:  

1. Register your content with us automatically using the OJS plugin
2. Have the plugin create an XML file that you can then
[upload to our admin tool](/documentation/member-setup/direct-deposit-xml/admin-tool/).

## Setting up the plugin and credentials <a id='00800' href='#00800'><i class='fas fa-link'></i></a>    

If you plan to set up the plugin to register content with Crossref automatically, you'll need to add your [Crossref account credentials](/documentation/member-setup/account-credentials/) into the username and password field in the plugin.


<figure><img src='/images/documentation/OJS-DOIs-Crossref-settings.png' alt='OJS Crossref plugin settings' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image41">Show image</button>
<div id="image41" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/OJS-DOIs-Crossref-settings.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

1. Depositor name - Name of the organization registering the DOIs (note: this field is not authenticated with Crossref)
2. Depositor email - email address of the individual responsible for registering content with Crossref (note: this field is not authenticated with Crossref)
3. Username - the Crossref username that will be passed to us to authenticate your submission(s). This might also be an email address - more on role versus user credentials below.
4. Password - the password associated with your Crossref credentials

Note: if the combination of username and password is incorrect, OJS will return a 401 unauthorized status code error at the time of registration. This error indicates that the username and password are incorrectly entered. That is, they do not match the username and/or password set with Crossref.

* If you are using organization-wide, shared [**role credentials**](/documentation/member-setup/account-credentials/#00376), you can simply add in your shared username and password.
* If you are using personal [**user credentials**](/documentation/member-setup/account-credentials/#00368) that are unique to you, you'll need to add your email address *and* your role into the username field, and your personal password into the password field. Here's an example of what this will look like:

Username: email@address.com/role <br/>
Password: your Password

## Additional OJS plugins for Crossref
In addition to the Crossref XML plugin for OJS, there are also other important plugins that can be enabled in OJS to enrich your metadata records:

1. [Reference linking and deposit plugin](https://docs.pkp.sfu.ca/crossref-ojs-manual/en/references) - As of OJS 3.1.2, it is possible to enable a reference linking plugin for Crossref. The plugin will use the Crossref API to check against plain text references and locate possible DOIs for articles. The plugin will also allow the display of reference lists on the article landing page in OJS and deposit them as part of your metadata deposit. Linking references is a requirement of Crossref membership.
2. [Cited-by (OJS Scopus/Crossref plugin)](https://docs.pkp.sfu.ca/crossref-ojs-manual/en/citationsplugin) - As of OJS 3.2, this [third-party plugin](https://github.com/RBoelter/citations) allows journals to display citations and citation counts (using article DOIs) from Scopus and/or Crossref.
3. [Crossmark plugin](https://docs.pkp.sfu.ca/crossref-ojs-manual/en/crossmark) - OJS 3.2 includes support for [Crossmark](https://www.crossref.org/services/crossmark/), which gives readers quick and easy access to the current status of an item of content, including any corrections, retractions, or updates to that record.
4. [Funding Metadata plugin](https://docs.pkp.sfu.ca/crossref-ojs-manual/en/funding) - As of OJS 3.1.2, it is possible to enable a funding registry plugin for submitting [funding information](/documentation/principles-practices/best-practices/funding/) to Crossref. The plugin will use the [Open Funder Registry](/documentation/funder-registry/accessing-the-funder-registry/) to check against existing funding agencies. The plugin will include funding information in your Crossref DOI deposits.
5. [Similarity Check plugin](https://docs.pkp.sfu.ca/crossref-ojs-manual/en/simCheck) - if you are using OJS 3.1.2 or above, you are able to use the Similarity Check plugin. This will enable you to automatically send manuscripts to your iThenticate account to check their similarity to already published content. You will need to be subscribed to Crossref's [Similarity Check service](https://www.crossref.org/services/similarity-check/) for this to work.

## Getting help with OJS plugins
The team at Crossref didn't create these plugins - they were either created by the team at PKP, or by third party developers. Because of this, we aren't able to give in-depth help or troubleshooting on problems with these plugins.

If you need more help, you can learn more at [PKP’s Crossref OJS Manual](https://docs.pkp.sfu.ca/crossref-ojs-manual/en/), plus there's a very active [PKP Community Forum](https://forum.pkp.sfu.ca/) which has more information on how to modify your OJS instance to submit metadata and register DOIs with Crossref.

Alternatively you can [contact the Support team at PKP](https://pkp.sfu.ca/contact-us/).
