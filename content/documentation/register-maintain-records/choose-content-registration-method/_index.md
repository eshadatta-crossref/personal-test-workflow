+++
title = "Choosing a content registration method"
date = "2022-12-07"
draft = false
author = "Amanda Bartell"
type = "documentation"
layout = "documentation_single"
documentation_section = ["register-maintain-records", "choose-content-registration-method"]
identifier = "documentation/content-registration/choose-content-registration-method"
rank = 4
weight = 50100
aliases = [
  "/education/member-setup/choose-content-registration-method",
  "/education/member-setup/choose-content-registration-method/",
  "/documentation/member-setup/choose-content-registration-method",
  "/documentation/member-setup/choose-content-registration-method/",
  "/documentation/content-registration/choose-content-registration-method",
  "/documentation/content-registration/choose-content-registration-method/"
]
+++

{{< snippet "/_snippet-sources/content-reg.md" >}}

### Helper tools<a id='00479' href='#00479'><i class='fas fa-link'></i></a>

* [Crossref XML plugin for OJS](/documentation/content-registration/ojs-plugin/) (Open Journal Systems) - you can use this helper tool if you’re using the Open Journal Systems publishing platform.
* [Web deposit form](/documentation/content-registration/web-deposit-form/) - you can use this form to register metadata for journals, books, conference proceedings, reports, and dissertations.
* [Grant registration form](/documentation/register-maintain-records/grant-registration-form) - you can use this form to register metadata for grants


### Direct deposit of XML<a id='00492' href='#00492'><i class='fas fa-link'></i></a>

* [Upload JATS XML using the web deposit form](/documentation/content-registration/web-deposit-form/)
* [Upload XML files using our admin tool](/documentation/member-setup/direct-deposit-xml/admin-tool/)
* [XML deposit using HTTPS POST](/documentation/member-setup/direct-deposit-xml/https-post/)

## Quick guide to choosing your content registration method<a id='00046' href='#00046'><i class='fas fa-link'></i></a>

Use this chart to choose the best option for you:

<figure><img src='/images/documentation/decision-tree-worflow.png' alt='Quick guide to choosing your content registration method illustration of the Helper tools and Direct deposit of XML options above' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image108">Show image</button>
<div id="image108" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/decision-tree-worflow.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

{{< snippet "/_snippet-sources/content-type-deposit-method.md" >}}

## Which deposit methods can I use to register pending publications?<a id='00383' href='#00383'><i class='fas fa-link'></i></a>

||Crossref XML plugin for OJS | Web deposit form  | XML (via the web deposit form, admin tool, or HTTPS POST) |
|--- |--- |--- |--- |
| [Pending publication](/documentation/schema-library/markup-guide-record-types/pending-publications/) | No | No  | Yes |
