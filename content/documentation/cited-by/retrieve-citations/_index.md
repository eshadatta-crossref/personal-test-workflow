+++
title = "Retrieve citations using Cited-by"
date = "2022-11-23"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["cited-by", "retrieve-citations"]
identifier = "documentation/cited-by/retrieve-citations"
rank = 4
weight = 80200
aliases = [
    "/education/cited-by/retrieving-cited-by-matches",
    "/education/cited-by/retrieving-cited-by-matches/",
    "/education/cited-by/retrieve-citations",
    "/education/cited-by/retrieve-citations/"
]
+++

There are a number of methods through which to retrieve citations:

| Input | Query for | Output |
|---|---|---|
| [Admin tool](https://doi.crossref.org/servlet/submissionAdmin?sf=citedByLinks) | DOI | List of citing DOIs |
| HTTPS doi.crossref.org | DOI, prefix | [XML](https://data.crossref.org/reports/help/schema_doc/crossref_query_output2.0/query_output2.0.html) |
| [XML](/documentation/retrieve-metadata/xml-api/) | DOI | [XML](https://data.crossref.org/reports/help/schema_doc/crossref_query_output2.0/query_output2.0.html) |
| OAI-PMH oai.crossref.org | prefix, title | [XML](https://www.crossref.org/schemas/crossref_citations_1.0.0.xsd) |

There are two additional options to receive a "push" notification any time one of your deposited works is cited:

| Input | Output |
|---|---|
| Email notification | Email sent to an address provided by the member |
| [Notification callback](/documentation/content-registration/verify-your-registration/notification-callback-service/) | Sent to an HTTP(S) URL endpoint hosted by the member |

Note that we don’t provide a plugin to directly display Cited-by results on a publisher website. The data is delivered in XML format only and needs to be correctly reformatted.

On this page, learn more about:

* Retrieving citation matches using:
  * [HTTPS POST](#00270)
  * [an XML query](#00272)
  * [the admin tool](#00273)
  * [OJS Plugin](#00276)
  * [OAI-PMH](#00271)
* [Citation notifications](#00274)
* [Troubleshooting Cited-by queries](#00275)

## Retrieve citation matches using HTTPS POST<a id='00270' href='#00270'><i class='fas fa-link'></i></a>

Using a URL, you can retrieve all citations for a single DOI or prefix within a date range. You will need to provide your [Crossref account credentials](/documentation/member-setup/account-credentials/) in the query.

If you use shared, organization-wide **role credentials**, queries have the following format:

```
https://doi.crossref.org/servlet/getForwardLinks?usr=username&pwd=password&doi=doi&startDate=YYYY-MM-DD&endDate=YYYY-MM-DD
```

where:
* **username** is the shared role and **password** is the shared password for the prefix or title being retrieved;
* **doi** can be a full DOI or a prefix.

If you use personal, individual **user credentials**, queries have the following format:
```
https://doi.crossref.org/servlet/getForwardLinks?usr=email@address.com/role&pwd=password&doi=doi&startDate=YYYY-MM-DD&endDate=YYYY-MM-DD
```

where:
* **email@address.com** is your user credential email address
* **role** is the role corresponding to the prefix or title being retrieved;
* **password** is your user credential password
* **doi** can be a full DOI or a prefix

On both versions of the query, **date range** is optional. Dates in the query refer to when the citation match was made (usually shortly after the DOI of the citing article was registered), not the publication date of the articles being queried for: all citations found in the given period will be returned, regardless of when the cited articles were originally deposited. Queries can also be made for a single day, in which case use the following format:

```
https://doi.crossref.org/servlet/getForwardLinks?usr=role&pwd=password&doi=prefix&date=YYYY-MM-DD
```

By default, citations from posted content (including preprints) are not included. To retrieve them as well, include `&include_postedcontent=true` in the query URL:

```
https://doi.crossref.org/servlet/getForwardLinks?usr=role&pwd=password&doi=prefix&date=YYYY-MM-DD&include_postedcontent=true
```

Output is XML formatted according to Crossref’s [query schema](https://data.crossref.org/reports/help/schema_doc/crossref_query_output2.0/query_output2.0.html).

If the query times out, we recommend using a smaller query, for example by using a narrower date range or splitting prefixes into individual DOIs. This is unlikely to affect most users, however if you frequently experience very large queries please [contact us](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001642691).

Here is some example output:

```XML
<crossref_result xmlns="http://www.crossref.org/qrschema/2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="2.0" xsi:schemaLocation="http://www.crossref.org/qrschema/2.0 http://www.crossref.org/qrschema/crossref_query_output2.0.xsd">
<query_result>
<head>
<email_address>none</email_address>
<doi_batch_id>none</doi_batch_id>
</head>
<body>
</forward_link>
<forward_link doi="10.1021/jacs.9b09811">
<journal_cite fl_count="0">
<issn type="print">2161-1653</issn>
<issn type="electronic">2161-1653</issn>
<journal_title>ACS Macro Letters</journal_title>
<journal_abbreviation>ACS Macro Lett.</journal_abbreviation>
<article_title>Critical Role of Ion Exchange Conditions on the Properties of Network Ionic Polymers</article_title>
<contributors>
<contributor first-author="true" sequence="first" contributor_role="author">
<given_name>Naisong</given_name>
<surname>Shan</surname>
</contributor>
<contributor first-author="false" sequence="additional" contributor_role="author">
<given_name>Chengtian</given_name>
<surname>Shen</surname>
</contributor>
<contributor first-author="false" sequence="additional" contributor_role="author">
<given_name>Christopher M.</given_name>
<surname>Evans</surname>
</contributor>
</contributors>
<volume>9</volume>
<issue>12</issue>
<first_page>1718</first_page>
<year>2020</year>
<publication_type>full_text</publication_type>
<doi type="journal_article">10.1021/acsmacrolett.0c00678</doi>
</journal_cite>
</forward_link>
</body>
</query_result>
</crossref_result>
```

Note that the `fl_count` property gives the number of times the citing article has itself been cited.



## Retrieve citation matches using an XML query<a id='00272' href='#00272'><i class='fas fa-link'></i></a>

Citations can also be retrieved through an [XML query](/documentation/retrieve-metadata/xml-api/using-https-to-query/#00418). The query contains only the DOI of the cited article stored in the *fl_query* element. Each XML file must contain *only* a single DOI.

If you submit a batch query submission with more than one DOI per query, the remaining DOIs in that query will return the message "exceeded limit of forward link queries per submission." Thus, the DOIs in excess of the first will not have alerts enabled.

Setting the *alert* attribute to “true” instructs the system to remember this query and to send new Cited-by link results to the specified email address when they occur. Note that an email address cannot be unset from receiving notifications, so only use this option for email addresses that will continue to receive notifications on a long-term basis.

Here is an example XML query:

```XML
https://doi.crossref.org/servlet/query?usr=ROLE&pwd=PASSWORD&qdata=<?xml version="1.0"?>
<query_batch version="2.0" xmlns = "http://www.crossref.org/qschema/2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.crossref.org/qschema/2.0 http://www.crossref.org/qschema/crossref_query_input2.0.xsd">
    <head>
        <email_address>EMAIL</email_address>
        <doi_batch_id>fl_001</doi_batch_id>
    </head>
    <body>
        <fl_query alert="false">
            <doi>10.1021/acs.joc.7b01326</doi>
        </fl_query>
    </body>
</query_batch>
```

By default, citations from posted content (including preprints) are not included. To retrieve them as well, use `<fl_query include_postedcontent="true">` in the body of the query.

Here is an example of the output XML:

```XML
<crossref_result xmlns="http://www.crossref.org/qrschema/2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="2.0" xsi:schemaLocation="http://www.crossref.org/qrschema/2.0 http://www.crossref.org/qrschema/crossref_query_output2.0.xsd">
  <query_result>
    <head>
      <email_address>{email}</email_address>
      <doi_batch_id>fl_001</doi_batch_id>
    </head>
    <body>
      <forward_link doi="10.5555/ums71316">
        <journal_cite fl_count="0">
          <issn type="print">1070-3632</issn>
          <issn type="electronic">1608-3350</issn>
          <journal_title>Russian Journal of General Chemistry</journal_title>
          <journal_abbreviation>Russ J Gen Chem</journal_abbreviation>
          <article_title>Simultaneous Formation of Cage and Spirane Pentaalkoxyphosphoranes in Reaction of 5,5-Dimethyl-2-(2-oxo-1,2-diphenylethoxy)-1,3,2-dioxaphosphorinane with Hexafluoroacetone</article_title>
          <contributors>
            <contributor first-author="true" sequence="first" contributor_role="author">
              <given_name>V. F.</given_name>
              <surname>Mironov</surname>
            </contributor>
            <contributor first-author="false" sequence="additional" contributor_role="author">
              <given_name>M. N.</given_name>
              <surname>Dimukhametov</surname>
            </contributor>
            <contributor first-author="false" sequence="additional" contributor_role="author">
              <given_name>Ya. S.</given_name>
              <surname>Blinova</surname>
            </contributor>
            <contributor first-author="false" sequence="additional" contributor_role="author">
              <given_name>F. Kh.</given_name>
              <surname>Karataeva</surname>
            </contributor>
          </contributors>
          <volume>90</volume>
          <issue>11</issue>
          <first_page>2080</first_page>
          <year>2020</year>
          <publication_type>full_text</publication_type>
          <doi type="journal_article">10.1134/S1070363220110109</doi>
        </journal_cite>
      </forward_link>
    </body>
  </query_result>
</crossref_result>
```

## Retrieve citation matches using the admin tool<a id='00273' href='#00273'><i class='fas fa-link'></i></a>

Once you have Cited-by enabled, you can find citations to single DOIs using our [admin tool](https://doi.crossref.org/). Log in using your [Crossref account credentials](/documentation/member-setup/account-credentials/), click on the *Queries* tab, then *Cited By Links*. This returns a list of DOIs:

{{< figure-linked
	src="/images/documentation/Cited-by-admin-view.png"
	large="/images/documentation/Cited-by-admin-view.png"
	alt="Cited-by view in the admin tool"
	title="Cited-by view in the admin tool"
	id="image128"
>}}

## Retrieve citation matches using the OJS Cited-by plugin<a id='00276' href='#00276'><i class='fas fa-link'></i></a>

For members who manage their journal using [OJS](https://pkp.sfu.ca/ojs/) v3.1.2.4 or later, you can install [a Cited-by plugin](https://github.com/RBoelter/citations) from the plugin gallery. It pulls data from the Cited-by API and can display it directly on article webpages. Note that in order to use the plugin you still need to go through the steps of depositing your references (there is a plugin for that too) and contacting us to enable Cited-by.

If you are not using OJS but use another third party software to manage your journal there is a good chance that there is also a plugin available. We don't maintain a comprehensive list of Cited-by plugins, but you can contact the software provider for details.

## Retrieve citation matches using OAI-PMH<a id='00271' href='#00271'><i class='fas fa-link'></i></a>

Note that the OAI-PMH API returns matches for the following article types: Journals, Books, Book Series, and Components. Other types are not included. To get complete results, we recommend using the HTTPS POST or an XML query (see the two sections above) for retrieving Cited-by matches rather than OAI-PMH.

This format retrieves Cited-by matches established within a date range for a prefix or title. Queries have the following format:

```
https://oai.crossref.org/OAIHandler?verb=ListRecords&usr=role&pwd=password&set=J:prefix:pubID&from=YYYY-MM-DD&until=YYYY-MM-DD&metadataPrefix=cr_citedby&include_postedcontent=false
```

where:
* **role and password** are the role credentials for the prefix or title being retrieved;
* **content type** is *J* for journal; *B* for books, conference proceedings, datasets, reports, standards, or dissertations; and *S* for series;
* **prefix** is the owning prefix of the title being retrieved;
* **pubID** is the [publication identification number](/documentation/retrieve-metadata/xml-api/retrieving-publication-ids/) of the title. This is optional: to query for all titles related to a prefix, simply omit the pubID;
* **metadataPrefix=cr_citedby** indicates that the results should include Cited-by matches rather than item metadata.

A **date range** is optional. Dates used refer to when the citing articles were last updated, not the publication date of the articles being queried for. All citations found in the given period will be returned, regardless of when the cited articles were originally deposited.

By default, citations from posted content (including preprints) are not included. To retrieve them as well, add `&include_postedcontent=true` to the query URL.

Output is XML formatted according to our [query schema](https://www.crossref.org/schemas/crossref_citations_1.0.0.xsd) and contains a list of the DOIs that cited the specified article or prefix.

Some OAI-PMH requests are too big to be retrieved in a single transaction. If a given response contains a resumption token, the user must make an additional request to retrieve the rest of the data. Learn more about [resumption tokens](/documentation/retrieve-metadata/oai-pmh#00398), and [OAI-PMH requests](/documentation//cited-by/retrieve-citations/#00271).

An example OAI-PMH query response is as follows:

```XML
<?xml version="1.0" encoding="UTF-8"?>
<OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/ http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd">
  <responseDate>2020-12-21T10:38:26</responseDate>
  <request verb="ListRecords" from="2020-01-01" until="2020-01-02" set="J:10.1021" metadataPrefix="CR_CITEDBY" resumptionToken="78da6164be33c5fb" >http://oai.crossref.org/oai</request>
  <!-- recipient 1234 abc -->
  <ListRecords>
    <record>
      <header>
        <identifier>info:doi/10.1016/1044-0305(94)80016-2</identifier>
        <datestamp>2020-12-18</datestamp> <setSpec>J:10.1021</setSpec>
      </header>
      <metadata>
          <citations>
           <citation xmlns="http://www.crossref.org/schema/crossref_citations_1.0.0">
          <doi>10.1023/1051-030580416</doi>
            <citations-cited-by>
              <doi type="journal-article">10.1021/acs.jproteome.0c00464</doi>
              <doi type="conference-paper">10.1007/978-1-0716-0943-9_16</doi>
              <doi type="journal-article">10.1038/s41598-020-78800-6</doi>
              <doi type="posted-content">10.1101/2020.12.01.407270</doi>
              <doi type="journal-article">10.1007/s11120-020-00803-1</doi>
          <citations-cited-by>
          </citation>
        </citations>
      </metadata>
    </record>
    <resumptionToken >78da6164be33c5fb</resumptionToken>
  </ListRecords>
</OAI-PMH>
```

OAI-PMH queries return the DOI of each citation. You can use our [REST API](/documentation/retrieve-metadata/rest-api/)  or [XML API](/documentation/retrieve-metadata/xml-api/) to retrieve the full bibliographic data for each citation.

## Citation notifications<a id='00274' href='#00274'><i class='fas fa-link'></i></a>

You can receive citation notifications by email or an [endpoint notification](/documentation/content-registration/verify-your-registration/notification-callback-service/). In both cases the text of the message is the same: it contains the same output as an XML query, containing details of the citing and cited works.

To select an email address for Cited-by notifications, see the [XML query](#00272) section.

## Troubleshooting Cited-by queries<a id='00275' href='#00275'><i class='fas fa-link'></i></a>

Sometimes citations don’t show up in Cited-by when you would expect them. There could be several reasons for this:

* The references haven’t been included in the metadata. We don’t use article PDFs or crawl websites to retrieve references, we rely on them being deposited as metadata by our members. You can use our APIs to check whether references are present.
* The DOI of the cited work wasn’t included in the reference and there was either an error in the metadata or insufficient information for us to make a reliable match. In this case, check the metadata for any errors and contact the owner of the citing work to redeposit the references.
* If the citing article was registered very recently it can take time to update the cited article’s metadata. If this happens, wait for a few days before trying again.

Note that citations are only retrieved from works with a Crossref DOI and will differ from citation counts provided by other services. Not all scholarly publications are registered with us and not all publishers opt to deposit references, so we can't claim that citation counts are comprehensive.

If you can’t access citation matches for a journal you own:

* Check that you have contacted us to enable Cited-by for the journal’s prefix as part of [the steps for participation in Cited-by](/documentation/cited-by/cited-by-participation/).
* Use the [admin tool](https://doi.crossref.org/) to make sure the journal is associated with your account.
* [Run a search in the admin tool](#00273) before using one of the URL-based methods.
