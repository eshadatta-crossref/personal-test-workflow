+++
title = "OpenURL"
date = "2023-01-27"
draft = false
author = "Jennifer Kemp"
type = "documentation"
layout = "documentation_single"
documentation_section = ["retrieve-metadata", "openurl"]
identifier = "documentation/retrieve-metadata/openurl"
rank = 4
weight = 30200
aliases = [
  "/education/retrieve-metadata/openurl",
  "/education/retrieve-metadata/openurl/"
]
+++

Our OpenURL service is used primarily by library link resolvers but can also be used to look up metadata records. Please note that OpenURL retrieval includes only [bibliographic metadata](/documentation/content-registration/descriptive-metadata/).

On this page, learn more about:

{{% row %}}
{{% column %}}

* [How to access OpenURL](#00375)
  * [Option one: access using an email address](#00377)
  * [Option two: access via a library link resolver using Crossref credentials](#00642)
* [OpenURL metadata queries](#00378)
  * [Citation metadata parameters](#00379)
  * [Other parameters](#00380)

{{% /column %}}
{{% column %}}

* [OpenURL results](#00381)
  * [Retrieving metadata](#00382)
  * [Example metadata queries](#00384)
  * [Example DOI queries](#00385)
* [NISO 0.1 or 1.0 URLs](#00388)
  * [Simple reverse lookup](#00389)
  * [Retrieve metadata for a DOI](#00390)
  * [A journal article lookup](#00391)

{{% /column %}}
{{% /row %}}

## How to access OpenURL<a id='00375' href='#00375'><i class='fas fa-link'></i></a>

Access to the OpenURL service is free, but does ask that you identify yourself. There are two different options for how to do this.

* Option one: Use your email address to identify yourself. You do not need to register your email address with us in advance, but you do need to include your email address in your query. [Find out more](#00377).
* Option 2: if you are a librarian and you need to use OpenURL with your library link resolver, and if that system requires a username and password, you will need a set of Crossref account credentials. [Find out more.](#00642)

### Option one: OpenURL access using an email address<a id='00377' href='#00377'><i class='fas fa-link'></i></a>

You need to include your email address in the *pid* parameter of the OpenURL request. Note that *pid* is not the same as *PID* persistent identifier, such as a DOI or an ORCID iD).

For interfaces that require a key, your email address is your key.

Use the format below but include your own email address instead of name\@someplace.com:

```
https://doi.crossref.org/openurl?pid=name@someplace.com&aulast=Maas%20LRM&title= JOURNAL%20OF%20PHYSICAL%20OCEANOGRAPHY&volume=32&issue=3&spage=870&date=2002
```

### Option two: Access to OpenURL via a library link resolver using Crossref credentials<a id='00642' href='#00642'><i class='fas fa-link'></i></a>

* To request your Crossref credentials, [read through the terms of use](/community/librarians/), and then request your credentials. There's a link to request your credentials after the terms of use.
* Once we receive your request, we will set up your credentials, and send you an email with a link where you can set your password.
* Once you have your username and password, login to the library link resolver using your new credentials.

## OpenURL metadata queries<a id='00378' href='#00378'><i class='fas fa-link'></i></a>

The OpenURL query interface uses metadata to identify a matching DOI, and redirects the user to the target of the DOI.

For example, this query contains an author name, a journal title, volume, issue, first page, and publication year:

```
https://doi.crossref.org/openurl?pid=email@address.com&aulast=Maas%20LRM&title= JOURNAL%20OF%20PHYSICAL%20OCEANOGRAPHY&volume=32&issue=3&spage=870&date=2002
```

The OpenURL query interface matches the query with a metadata record and redirects the user to the relevant persistent identifier landing page at [https://doi.org/10.1175/1520-0485(2002)032<0870:CT>2.0.CO;2](https://doi.org/10.1175/1520-0485(2002)032<0870:CT>2.0.CO;2).

The OpenURL Query Interface can accept these parameters:

### Citation metadata parameters<a id='00379' href='#00379'><i class='fas fa-link'></i></a>
* *issn*
* *title* (journal title)
* *aulast* (family name, preferably of first author)
* *volume*
* *issue*
* *spage* (first page)
* *date* (publication year YYYY)
* *stitle* (short title, which may be supplied as an alternative to *title*)

### Other parameters<a id='00380' href='#00380'><i class='fas fa-link'></i></a>
* *pid* (your email address). Note: pid (personal id) is different from PID (persistent identifier)
* *redirect* (set to *false* to return the DOI in XML format instead of redirecting to the target URL. The default is *true*)
* *multihit* (set to *true* to return DOIs for more than one content item if our system does not find an exact match. The default is *false*)
* *format* (set to *unixref* to return metadata in UNIXREF format)

## OpenURL results<a id='00381' href='#00381'><i class='fas fa-link'></i></a>

By default, an OpenURL match will direct the user to the landing page registered for the matched metadata record.

In most instances, only a single identifier will be returned. If more than one identifier is returned, the user will be directed to a list of all available DOIs. For example, the query:

```
https://www.crossref.org/openurl?pid=email@address.com&title=Science&aulast=Fernández&date=2009
```

will return multiple results.

### Retrieving metadata<a id='00382' href='#00382'><i class='fas fa-link'></i></a>

OpenURL may be used to retrieve metadata records by setting the *redirect* parameter to "false". By default an OpenURL response uses the XSD XML format. The UNIXREF format may be requested by setting the *format* parameter to "unixref".

### Example metadata queries<a id='00384' href='#00384'><i class='fas fa-link'></i></a>

This query will return an XSD-formatted XML metadata record:

```
https://doi.crossref.org/openurl?issn=03770273&aulast=Walker&volume=54&spage=117&date=1983**&redirect=false**&pid=email@address.com
```

There are multiple matches for this query, when multihit=*true* the metadata record is returned for all results:

```
https://doi.crossref.org/openurl?issn=03603016&volume=54&issue=2&spage=215&date=2002&multihit=true&pid=email@address.com
```

Setting multihit=*exact* will return no matches:

```
https://doi.crossref.org/openurl?issn=03603016&volume=54&issue=2&spage=215&date=2002&multihit=exact&pid=support@crossref.org
```

### Example DOI queries<a id='00385' href='#00385'><i class='fas fa-link'></i></a>

We support DOI queries formatted as OpenURL version 0.1 requests:

#### Open URL query<a id='00386' href='#00386'><i class='fas fa-link'></i></a>
```
https://doi.crossref.org/openurl/?pid=email@address.com&id=doi:10.1103/PhysRev.47.777&noredirect=true
```

#### Crossref query<a id='00387' href='#00387'><i class='fas fa-link'></i></a>
```
https://doi.crossref.org/servlet/query?pid=email@address.com&id=10.1006/jmbi.2000.4282
```

Like metadata queries, DOI query results are returned in XML format.

## NISO 0.1 or 1.0 URLs<a id='00388' href='#00388'><i class='fas fa-link'></i></a>

We also support NISO 0.1 and 1.0 URLs as well as some common deviations. In general it supports the San Antonio Profile \#1, including in-line, by-value, and by-reference. In the presence of a `url_ver= Z39.88-2004` parameter this service will operate on a `info:ofi/fmt:kev:mtx:ctx` context format with referent formats `info:ofi/fmt:kev:mtx:journal` or `info:ofi/fmt:kev:mtx:book`.

### Simple reverse lookup<a id='00389' href='#00389'><i class='fas fa-link'></i></a>
```
https://doi.crossref.org/openurl?pid=email@address.com&url_ver=Z39.88-2004&rft_id=info:doi/10.1103/PhysRev.47.777
```

### Retrieve metadata for a DOI<a id='00390' href='#00390'><i class='fas fa-link'></i></a>
```
https://doi.crossref.org/openurl?pid=email@address.com&url_ver=Z39.88-2004&rft_id=info:doi/10.1361/15477020418786&noredirect=true
```

### A journal article lookup<a id='00391' href='#00391'><i class='fas fa-link'></i></a>
```
https://doi.crossref.org/openurl?pid=email@address.com&url_ver=Z39.88-2004&rft_val_fmt=info:ofi/fmt:kev:mtx:journal&rft.atitle=Isolation of a common receptor for coxsackie B&rft.jtitle=Science&rft.aulast=Bergelson&rft.auinit=J&rft.date=1997&rft.volume=275&rft.spage=1320&rft.epage=1323
```
