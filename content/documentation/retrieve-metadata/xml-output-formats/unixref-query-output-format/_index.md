+++
title = "UNIXREF query output format"
date = "2020-04-08"
draft = false
author = "Jennifer Kemp"
type = "documentation"
layout = "documentation_single"
documentation_section = ["retrieve-metadata", "xml-output-formats", "unixref-query-output-format"]
identifier = "documentation/retrieve-metadata/xml-output-formats/unixref-query-output-format"
rank = 4
weight = 31102
aliases = [
  "/education/retrieve-metadata/xml-output-formats/unixref-query-output-format",
  "/education/retrieve-metadata/xml-output-formats/unixref-query-output-format/"
]
+++

Our *unified XML* (UNIXREF) format returns all metadata submitted by the member responsible for a DOI. The UNIXREF data does not include namespaces or namespace prefixes, which are used extensively for non-bibliographic metadata.

The UNIXREF format will return deposited citations from other members. Citations will also be returned to members querying for their own deposited data.

## UNIXREF schema<a id='00455' href='#00455'><i class='fas fa-link'></i></a>

The majority of UNIXREF results use the `unixref1.1.xsd` schema ([schema](https://www.crossref.org/schema/unixref1.1.xsd) | [documentation](https://data.crossref.org/reports/help/schema_doc/unixref1.1/unixref1.1.html)). Some results involving book or conference proceeding data deposited prior to a deposit schema change use `unixref1.0.xsd` ([schema](https://www.crossref.org/schema/unixref1.0.xsd) | [documentation](https://data.crossref.org/reports/help/schema_doc/unixref1.0/unixref.html)).

### Example UNIXREF result<a id='00456' href='#00456'><i class='fas fa-link'></i></a>

```
<doi_records>
  <doi_record owner="10.1353" timestamp="2007-02-13 15:56:13">
   <crossref>
 	<journal>
    	<journal_metadata language="en">
     	<full_title>American Quarterly</full_title>
     	<abbrev_title>American Quarterly</abbrev_title>
     	<issn media_type=l"electronic">1080-6490</issn>
    	</journal_metadata>
    	<journal_issue>
     	<publication_date media_type="print">
      	<year>1998</year>
     	</publication_date>
     	<journal_volume>
      	<volume>50</volume>
     	</journal_volume>
     	<issue>1</issue>
    	</journal_issue>
    	<journal_article publication_type="full_text">
     	<titles>
      	<title>&quot;Disturbing the Peace: What Happens to American Studies If You Put African American Studies at the Center?&quot;: Presidential Address to the American Studies Association, October 29, 1997</title>
     	</titles>
     	<contributors>
      	<person_name sequence="first" contributor_role="author">
       	<given_name>Mary Helen.</given_name>
       	<surname>Washington</surname>
      	</person_name>
     	</contributors>
     	<publication_date media_type="print">
      	<year>1998</year>
     	</publication_date>
     	<pages>
      	<first_page>1</first_page>
      	<last_page>23</last_page>
     	</pages>
     	<doi_data>
      	<doi>10.1353/aq.1998.0005</doi>
       	<timestamp>20070206205234</timestamp>
       	<resource>
http://muse.jhu.edu/content/crossref/journals/american_quarterly/v050/50.1washington.html</resource>
     	</doi_data>
    	</journal_article>
   	</journal>
  	</crossref>
 	</doi_record>
   </doi_records>
   ```
