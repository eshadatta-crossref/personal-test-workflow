+++
title = "Tips for working with Crossref public data files and Plus snapshots"
date = "2022-02-01"
draft = false
author = "Geoffrey Bilder"
type = "documentation"
layout = "documentation_single"
documentation_section = ["retrieve-metadata", "rest-api", "tips-for-using-the-crossref-rest-api"]
rank = 4
weight = 30101
aliases = [

]
+++


## What is this?

About once a year Crossref releases a Public metadata file that includes all of Crossref's public metadata. We typically release this as a `tar` file and distribute it via [Academic Torrents](https://academictorrents.com/browse.php?search=crossref).

Users of Crossref's [Plus service](https://www.crossref.org/services/metadata-retrieval/metadata-plus/) can access similar data snapshots that we updated monthly. These are also `tar` files, but we distribute them via the Plus service API, and you need a Plus API token to access them.

In either case, these files are large and unwieldy. This document provides you with tips that should make your life easier when handling Crossref public metadata files and Plus snapshots.


### Handling tar files

**Q:** The `tar` file contains many files that, in turn, contain the individual DOI records. Some of these files are very large and hard to process. Could you break them out into separate files per DOI instead?

**A:** Yes, we could. But that creates its own set of problems. Standard filesystems on Linux/macOS/Windows really, really don't like you to create hundreds of millions of small files on them. Even standard command-line tools like `ls` choke on directories with more than a few thousand files in them. Unless you are using a specialized filesystem, formatted with custom inode settings optimized for hundreds of millions of files- saving each DOI as an individual record will bring you a [world of hurt](https://www.urbandictionary.com/define.php?term=world%20of%20hurt).

**Q:** Gah! The `tar` file is large and uncompressing it takes up a ton of room and generates a huge number of files. What can we do to make this easier? Can you split the tar file so we can manage it in batches?

**A:** Don't uncompress or extract the tar file. You can read the files straight from the compressed tar file.

**Q:**  But won't reading files straight from the tar file be slow?

We did three tests- all done on the same machine using the same `tar` file, which, at the time of this writing, contained 42,210 files which, in turn, contained records for `127,574,634` DOIs.

**Test 1:** Decompressing and untarring the file took about 82 minutes.

On the other hand...

**Test 2:** A python script iterating over each filename in the `tar` file (without extracting and reading the file into memory) was completed in just 29 minutes.

**Test 3:** A python script iterating over each filename in the `tar` file *and* extracting and reading the file into memory completed in just 61 minutes.

Both of the above scripts worked in a single process. However, you could almost certainly further optimize by parallelizing reading the files from the `tar` file.

In short - the `tar` file is a lot easier to handle if you don't decompress and/or extract it. Instead, it is easiest to read directly from the compressed `tar` file.


### Downloading and using Plus snapshots

**Q:** How should I best use the snapshots? Can we get them more frequently than each month?

**A:** The monthly snapshots include all public Crossref metadata up to and including data for the month before they were released. We make them available to *seed* and occasionally *refresh* a local copy of the Crossref database in any system you are developing that requires Crossref metadata. In most cases, you should just keep this data current by using the Crossref REST API to retrieve new or modified records. Typically, only a small percentage of the snapshot changes from month to month. So if you are downloading it repeatedly, you are just downloading the same unchanged records time and time again. Occasionally, there will be a large number of changes in a month. This typically happens when:\\
\
- A large Crossref member adds or updates a lot of records at once.\
- We add a new metadata element to the schema.\
- We change the way we caluclate something (e.g. citation counts) and that effects a lot of records.\
\
In these cases, it makes sense to refresh your metadata from the newly downloaded snapshot instead of using the API.
\
In short, if you are downloading the snapshot more than a few times a year- you are probably doing something very inefficient.

**Q:** The snapshot is large and difficult to download. I keep having it fail and have to start the download again. Can you split the snapshot so that I can download smaller parts instead?

**A:** If your download gets interrupted, you can resume the download from the point it got interrupted instead of starting over. This is easiest to do using something like `wget.`

But you can also do it with `curl.` You can try it yourself:

```
> export TOKEN='<insert-your-token-here>'
> curl -o "all.json.tar.gz" --progress-bar -L -X GET  https://api.crossref.org/snapshots/monthly/latest/all.json.tar.gz -H "Crossref-Plus-API-Token: ${TOKEN}"
```

Wait a few minutes, then execute `ctrl-c` to interrupt the download.

Then to resume it from where it left-off, include the switch ```-C -```:

```
curl -o "all.json.tar.gz" --progress-bar -L -X GET  https://api.crossref.org/snapshots/monthly/latest/all.json.tar.gz -H "Crossref-Plus-API-Token: ${TOKEN} -C -"
```

Then the `curl` command will calculate the byte offset from where it left off and continue the download from there.