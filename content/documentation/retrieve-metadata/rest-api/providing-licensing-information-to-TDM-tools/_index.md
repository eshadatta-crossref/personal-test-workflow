+++
title = "Providing licensing information to TDM tools"
date = "2020-04-08"
draft = false
author = "Jennifer Kemp"
type = "documentation"
layout = "documentation_single"
documentation_section = ["retrieve-metadata", "rest-api", "providing-licensing-information-to-TDM-tools"]
identifier = "documentation/retrieve-metadata/rest-api/providing-licensing-information-to-TDM-tools"
rank = 4
weight = 30105
aliases = [
  "/education/retrieve-metadata/rest-api/providing-licensing-information-to-TDM-tools",
  "/education/retrieve-metadata/rest-api/providing-licensing-information-to-TDM-tools/"
]
+++

Providing the researcher with a link to full-text of scholarly content is of limited value if the researcher has no means of automatically determining what they are allowed to do with it. Members registering licensing information in their metadata (using the `<license_ref>` element) lets researchers know when they can perform TDM and under what conditions. This license could be proprietary, or an open license such as [Creative Commons](https://creativecommons.org/share-your-work/).

```
<program name="AccessIndicators">
     <license_ref>
          http://creativecommons.org/licenses/by/3.0/deed.en_US
     </license_ref>
</program>
```

In the `<license_ref>` element, you can include simple embargo information by registering licenses with different start dates. For example, *Annals of Psychoceramics B* could show that an article was embargoed for a year by listing a proprietary license with a start date of the publication date and a Creative Commons license with a start date at the end of the embargo:

```
<program name="AccessIndicators">
   <license_ref start_date="2013-02-03">http://annalsofpsychoceramics.org/proprietary_license.html</license_ref>
   <license_ref start_date="2014-02-03">
      http://creativecommons.org/licenses/by/3.0/deed.en_US
   </license_ref>
</program>
```
