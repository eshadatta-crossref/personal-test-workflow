+++
title = "REST API Versions "
date = "2022-12-01"
draft = false
author = "Geoffrey Bilder"
type = "documentation"
layout = "documentation_single"
documentation_section = ["retrieve-metadata", "rest-api", "rest-api-version"]
rank = 4
weight = 30101
aliases = [

]
+++

## REST API Versions

The [Crossref REST API](https://api.crossref.org) is versioned. You should always use the API version in your REST API requests.


### Breaking changes

Any breaking changes will be released in a new API version. Breaking changes are changes that can potentially break an integration. Breaking changes include:

- removing an entire operation
- removing or renaming a parameter
- removing or renaming a response field
- adding a new required parameter
- making a previously optional parameter required
- changing the type of a parameter or response field
- removing enum values
- adding a new validation rule to an existing parameter
- changing authentication or authorization requirements

### Non-breaking changes

Any additive (non-breaking) changes will be available in all supported API versions. Additive changes are changes that should not break an integration. Additive changes include:

- adding an operation
- adding an optional parameter
- adding an optional request header
- adding a response field
- adding a response header
- adding enum values

### Legacy version support

When a new REST API version is released, the previous API version will be supported for 24 more months following the release of the new API version. In exceptional circumstances, we may decide to extend this.

### Specifying an API version

To be safe, You should always specify an API version in your requests.

For example:

[https://api.crossref.org/v1/works?rows=0](https://api.crossref.org/v1/works?rows=0)

However, as long as v1 of the API exists, requests that do not contain an API version in the request will default to v1.

Eventually, if you specify an API version that is no longer supported, you will receive a 400 error.

So, if version 1 of the API is ever retired, then requests to the API that do not contain a version number will fail with a 400 error.

Again, to be safe, you should include the API version in your requests.


### Upgrading to a new API version

Before upgrading to a new REST API version, you should read the changelog of breaking changes for the new API version to understand what breaking changes are included and to learn more about how to upgrade to that specific API version. For more information, see "Breaking changes."

### Supported API versions

The following REST API versions are currently supported:

- V1

Credits: This policy is  adapted from the very short, clear and reasonable [Github API policy](https://docs.github.com/en/rest/overview/api-versions?apiVersion=2022-11-28)