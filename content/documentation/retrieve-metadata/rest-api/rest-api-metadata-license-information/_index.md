+++
title = "REST API metadata license information"
date = "2022-02-01"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["retrieve-metadata", "rest-api", "tips-for-using-the-crossref-rest-api"]
identifier = "documentation/retrieve-metadata/rest-api/rest-api-metadata-license-information"
rank = 4
weight = 30101
aliases = [

]
+++


## REST API metadata license information

### TL;DR

You can use and redistribute any metadata you retrieve with the Crossref REST API or that is included in Crossref snapshots and public data files. Have fun.

### Details

What we colloquially call “Crossref metadata” is actually a mix of elements, some of which come from our members, some of which come from third parties, and some of which come from Crossref itself. These elements, in turn, each have different copyright implications.

On top of this, Crossref has terms and conditions for its members and terms and conditions for specific services. These grant Crossref the right to do things with some classes of metadata and not do things with other classes of metadata - regardless of copyright.

Since 2000 Crossref has stated that it considers basic bibliographic metadata to be “facts.” And under US law (Crossref is registered in the US) these facts are not subject to copyright at all.  Note also that, given that this data is not subject to copyright at all, there is no way Crossref can “waive the copyright” under [CC0](https://creativecommons.org/share-your-work/public-domain/cc0/). In short, this metadata has no restrictions on reuse.

More recently, some of our members have been submitting abstracts to Crossref. These are copyrighted. In the case of subscription publishers, the copyright usually belongs to the publisher. In the case of open access publishers, the copyright most often belongs to the authors. In both cases, Crossref cannot waive copyright under CC0 because the copyright is not ours to waive. However, we are allowed to redistribute the abstracts with our metadata because that is part of the terms and conditions we have with our members.

We also collect [Event Data](/documentation/event-data/), which contains mentions of research works from across the internet. We can make the majority of this data openly available under CC0, however for a few of the sources there are additional restrictions. These might affect you if you're planning to republish results of event data queries or store them for a long time. You can find more details on the [Event Data terms of use](/services/event-data/terms/) page.

We also have some data that we've always released under CC0. This includes the Open Funder Registry and Event Data. This data is currently available through separate APIs, but will eventually be made available via the REST API as well.

And this leaves us with data that is created by Crossref itself as a byproduct of our services. This data includes things like participation reports, conflict reports, member IDs, and Cited-by counts, and any aggregations of our otherwise uncopyrighted data that might, by aggregating it, be subject to sui generis database rights. We also make this latter class of data available CC0.

To summarize:

| Data  |Licence   |
|---|---|
| Bibliographic metadata, including references  | Facts, not subject to copyright   |
| Crossref-generated data, and any aggregations of our otherwise uncopyrighted data that might, by aggregating it, be subject to sui generis database rights.   | CC0  |
| Open Funder Registry, Event Data | CC0 |
| Abstracts  | Copyright held by publisher or author, but redistributable as per Crossref membership terms.   |
