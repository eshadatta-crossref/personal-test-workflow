+++
title = "Retrieving publication IDs"
date = "2020-04-08"
draft = false
author = "Jennifer Kemp"
type = "documentation"
layout = "documentation_single"
documentation_section = ["retrieve-metadata", "xml-api", "retrieving-publication-ids"]
identifier = "documentation/retrieve-metadata/xml-api/retrieving-publication-ids"
rank = 4
weight = 31015
aliases = [
  "/education/retrieve-metadata/xml-api/retrieving-publication-ids",
  "/education/retrieve-metadata/xml-api/retrieving-publication-ids/"
]
+++

Every top-level publication in Crossref is assigned a unique publication ID. Publication IDs are mainly used for internal purposes, but may be useful when [retrieving data using OAI-PMH](/documentation/retrieve-metadata/oai-pmh), or identifying a specific title. For most purposes, publication IDs are always preceded by the publication type (*J*, *B*, or *S*). *B* indicates non-journal publications including books, conference proceedings, standards, reports, dissertations, posted content, and datasets.

Publication IDs may be retrieved via the following:

* OAI-PMH: an OAI-PMH ListSets request will return titles and publication IDs for journals, books, conference proceedings, and series-level data:

```
https://oai.crossref.org/OAIHandler?verb=ListSets
```
```
https://oai.crossref.org/OAIHandler?verb=ListSets&set=B
```

*J* (journal) is the default set, set=*B* must be specified to retrieve book or conference proceeding titles, and *S* for series-level titles. Sets may be further limited by member prefix

* The publication ID is listed within the `<setspec>` element, after the set and member prefix. For example, within the following set, `24` is the publication ID for *Journal of Clinical Psychology*:

```
<set>
<setSpec>J:10.1002:24</setSpec>
<setName>Journal of Clinical Psychology</setName>
</set>
```

* [Browsable title list](https://www.crossref.org/titleList/): this list includes the publication ID next to each title in the search results. Click <img src="/images/documentation/Icon-ID.png" alt="id icon" height="23" > to reveal the ID
* [UNIXSD](/documentation/retrieve-metadata/xml-output-formats/unixsd-query-output-format) metadata: the UNIXSD output format includes the `journal-id` (*J*) or `book-id` (*B*).
