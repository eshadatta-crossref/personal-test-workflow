+++
title = "Using the match attribute"
date = "2020-04-08"
draft = false
author = "Jennifer Kemp"
type = "documentation"
layout = "documentation_single"
documentation_section = ["retrieve-metadata", "xml-api", "using-the-match-attribute"]
identifier = "documentation/retrieve-metadata/xml-api/using-the-match-attribute"
rank = 4
weight = 31010
aliases = [
  "/education/retrieve-metadata/xml-api/using-the-match-attribute",
  "/education/retrieve-metadata/xml-api/using-the-match-attribute/"
]
+++

Our query engine operates by evaluating several logic rules in order. Each rule focuses on certain fields in the query, with the first rule processing the entire query. If any rule returns a single DOI as its output, this DOI is taken as the result for the query, and rule processing terminates.

In an XML query you can exercise some control over how the query engine works by using the `match` attribute that is available on many query elements.

* **fuzzy**: Fuzzy matching is allowed but the field is not optional, it must be matched.
* **optional**: In most cases, *optional* is a default property. Using a value that excludes optional (for example, `match=""`) tells the query engine that this field is not optional and must be present, and matched, for a DOI match to occur.
* **exact**: Instructs the query engine to not apply any of its fuzzy comparison logic. This is the same as `match=""` (which means not optional and not fuzzy). Using `exact` is not compatible with `optional`, or `fuzzy`.
* **null**: Instructs the query engine to match on the query element if it is not present in the metadata. When using this option it makes no sense to provide an element value (for example, the correct style would be `<author match="null"/>`

Multiple values may be assigned (as in `match="optional fuzzy"`). Typical uses include:

* `match="fuzzy"` - instructs the query engine to apply its fuzzy comparison logic for this field
* `match="optional fuzzy"` - a rule may drop the field altogether or use fuzzy matching
* `match="exact"` - field must be matched and no fuzzy matching is allowed
* `match=""` - same as `match="exact"`
