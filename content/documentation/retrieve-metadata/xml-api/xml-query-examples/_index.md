+++
title = "XML query examples"
date = "2020-04-08"
draft = false
author = "Jennifer Kemp"
type = "documentation"
layout = "documentation_single"
documentation_section = ["retrieve-metadata", "xml-api", "xml-query-examples"]
identifier = "documentation/retrieve-metadata/xml-api/xml-query-examples"
rank = 4
weight = 31003
aliases = [
  "/education/retrieve-metadata/xml-api/xml-query-examples",
  "/education/retrieve-metadata/xml-api/xml-query-examples/"
]
+++

This section includes examples for:

{{% row %}}
{{% column %}}

* [a strict journal article query](#00430)
* [a less strict journal article query](#00431)
* [an unstructured citation](#00432)
* [a book chapter query](#00433)
* [a book title query](#00434)

{{% /column %}}
{{% column %}}

* [a book title query without an author](#00435)
* [controlling query result XML](#00436)
* [searching all authors](#00439)
* [DOI-to-metadata query](#00440) (retrieves metadata for a DOI)

{{% /column %}}
{{% /row %}}

## A strict journal article query<a id='00430' href='#00430'><i class='fas fa-link'></i></a>

This query is fairly strict - it is requesting a single match for the given metadata. The ISSN is provided but does not need to be used for matching (`match="optional"`). The journal title needs to match exactly (`match="exact"`), no fuzzy matching will be applied. Fuzzy matching is applied to the author (`match="fuzzy"`) but only the first author will be matched.

```
<query key="1178517" enable-multiple-hits="false">
<issn match="optional">15360075<issn>
<journal_title match="exact">American Journal of Bioethics</journal_title>
<author match="fuzzy" search-all-authors="false">Agich</author>
<volume match="fuzzy">1</volume>
<issue>1</issue>
<first_page>50</first_page>
<year>2001</year>
<article_title>The Salience of Narrative for Bioethics</article_title>
</query>
```

## A less strict journal article query<a id='00431' href='#00431'><i class='fas fa-link'></i></a>

The query below will return multiple matches (`enable-multiple-hits="true"`) and fuzzy match the author against all deposited authors, and will do an author/article title query if the full metadata query does not produce a match.

```
<query key="1178517" enable-multiple-hits="true" secondary-query="author-title">
  <journal_title match="fuzzy">American Journal of Bioethics</journal_title>
  <author match="fuzzy" search-all-authors="true">Agich</author>
  <volume match="fuzzy">1</volume>
  <issue>1</issue>
  <first_page>50</first_page>
  <year>2001</year>
  <article_title>The Salience of Narrative for Bioethics</article_title>
</query>
```

## An unstructured citation<a id='00432' href='#00432'><i class='fas fa-link'></i></a>

This citation has not been marked up into separate elements. We'll try to break it up, but the results may not be as accurate.

```
<query key="q1" enable-multiple-hits="true">
  <unstructured_citation>Hungate, B. A., &amp; Hampton, H. M. (2012). Ecosystem services: Valuing ecosystems for climate. Nature Climate Change, 2(3), 151-152.
  </unstructured_citation>
</query>
```

## Book chapter query<a id='00433' href='#00433'><i class='fas fa-link'></i></a>

A query which will return the DOI for a single chapter in the specific title

```
<query key="MyKey1" enable-multiple-hits="false">
  <author>Casteilla</author>
  <volume>155</volume>
  <first_page>1</first_page>
  <year>2001</year>
  <isbn>1-59259-231-7</isbn>
  <volume_title>Adipose Tissue Protocol</volume_title>
</query>
```

Searching for individual chapters within a book may also be done by using just the author name and chapter title (author name is optional, but should be included for better results):

```
<query key="MyKey1" enable-multiple-hits="false">
  <author>Casteilla</author>
  <article_title>Choosing an Adipose Tissue Depot for Sampling </article_title>
</query>
```

## Book title query<a id='00434' href='#00434'><i class='fas fa-link'></i></a>

Book title queries should include the book title (as `volume_title`) and author (or editor) when available.

```
<query key="MyKey1" enable-multiple-hits="false">
  <author> Ailhaud</author>
  <volume>155</volume>
  <first_page>1</first_page>
  <year>2001</year>
  <isbn>1-59259-231-7</isbn>
  <volume_title>Adipose Tissue Protocol</volume_title>
</query>
```

## Book title query without an author<a id='00435' href='#00435'><i class='fas fa-link'></i></a>

Some title-level book DOIs lack author information. If you do not have author information to include in your query or you are querying for an authorless book, for best results your query should instruct the system to ignore author by setting the `author match` attribute to *null*.

```
<query key="555-555">
  <isbn>9780387791456</isbn>
  <volume_title>Ordinary and Partial Differential Equations</volume_title>
  <year>2009</year>
  <author match="null"/>
</query>
```

## Controlling query result XML<a id='00436' href='#00436'><i class='fas fa-link'></i></a>

Crossref query results can be retrieved in several formats. By default the *XSD_XML* format will only contain basic bibliographic metadata. Setting expanded-results to *TRUE* will also return the article title.

This example shows use of `expanded-results=true` along with `enable-multiple-hits=true`:

### Query<a id='00437' href='#00437'><i class='fas fa-link'></i></a>
```
<?xml version = "1.0" encoding="UTF-8"?>
<query_batch version="2.0" xmlns = "https://www.crossref.org/qschema/2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <head>
    <email_address>someone@crossref.org</email_address>
    <doi_batch_id>Sample multi resolve</doi_batch_id>
  </head>
  <body>
    <query key="mutliResolve1" enable-multiple-hits="true" expanded-results="true">
      <issn>0360-3016</issn>
      <volume>54</volume>
      <issue>2</issue>
      <first_page>215</first_page>
      <year>2002</year>
    </query>
  </body>
</query_batch>
```

### Query results<a id='00438' href='#00438'><i class='fas fa-link'></i></a>
```
<crossref_result version="2.0" xsi:schemaLocation="https://www.crossref.org/qrschema/2.0 https://www.crossref.org/qrschema/crossref_query_output2.0.xsd">
  <query_result>
	<head>
  	<email_address>hisham@atypon.com</email_address>
  	<doi_batch_id>Sample multi resolve</doi_batch_id>
	</head>
	<body>
  	<query key="mutliResolve1" status="multiresolved" fl_count="0">
    	<doi type="journal_article">10.1016/S0360-3016(02)03429-6</doi>
    	<issn type="print">03603016</issn>
    	<journal_title> International Journal of Radiation Oncology*Biology*Physics</journal_title>
    	<author>KIM</author>
    	<volume>54</volume>
    	<issue>2</issue>
    	<first_page>215</first_page>
    	<year>2002</year>
    	<publication_type>full_text</publication_type>
    	<article_title> Potential radiation sensitizing effect of SU5416 by down-regulating the COX-2 expression in human lung cancer cells </article_title>
  	</query>
  	<query key="mutliResolve1" status="multiresolved" fl_count="0">
    	<doi type="journal_article">10.1016/S0360-3016(02)03428-4</doi>
    	<issn type="print">03603016</issn>
    	<journal_title> International Journal of Radiation Oncology*Biology*Physics </journal_title>
    	<author>WILLIAMS</author>
    	<volume>54</volume>
    	<issue>2</issue>
    	<first_page>215</first_page>
    	<year>2002</year>
    	<publication_type>full_text</publication_type>
    	<article_title> Effect of the administration of lovastatin on the development of pulmonary fibrosis following whole lung irradiation in a mouse model </article_title>
  	</query>
	</body>
  </query_result>
</crossref_result>
```

The system will return no DOIs if an ambiguity exists. Setting `enable-multiple-hits` to *true* instructs the system to return the list of DOIs.

## Searching all authors<a id='00439' href='#00439'><i class='fas fa-link'></i></a>

Normally the author name supplied in a query must be that of the article's first author. First author is an optional designation made by the member when depositing a DOI's metadata. Articles registered without a first author designation handicap queries that depend on author (for example, those which do not supply a page number). In an XML query there is a property called *search-all-authors* which forces the process to examine all authors associated with the article.

This example shows a query that would not return any results if this feature were not used:

```
<?xml version = "1.0" encoding="UTF-8"?>
<query_batch version="2.0" xmlns = "https://www.crossref.org/qschema/2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<head>
<email_address>support@crossref.com</email_address>
<doi_batch_id>Sample multi resolve</doi_batch_id>
</head>
<body>
<query key="mutliResolve1" enable-multiple-hits="false">
<journal_title>Advances in Applied Probability</journal_title>
<author search-all-authors="true">Weil</author>
<volume>33</volume>
<year>2001</year>
</query>
</body>
</query_batch>
```

## DOI-to-metadata query (retrieves metadata for a DOI)<a id='00440' href='#00440'><i class='fas fa-link'></i></a>
```
<?xml version = "1.0" encoding="UTF-8"?>
<query_batch version="2.0" xmlns = "https://www.crossref.org/qschema/2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <head>
      <email_address>hisham@atypon.com</email_address>
      <doi_batch_id>Sample multi resolve</doi_batch_id>
  </head>
  <body>
   	<query key="mykey" expanded-results="true">
   	    <doi>10.1006/jmbi.2000.4282</doi>
       </query>
   </body>
</query_batch>
```

### DOI-to-metadata query results<a id='00441' href='#00441'><i class='fas fa-link'></i></a>
```
<crossref_result version="2.0" xsi:schemaLocation="https://www.crossref.org/qrschema/2.0 https://www.crossref.org/qrschema/crossref_query_output2.0.xsd">
<query_result>
<head>
<email_address>hisham@atypon.com</email_address>
<doi_batch_id>Sample multi resolve</doi_batch_id>
</head>
  <body>
 	<query key="mykey" status="resolved" fl_count="1">
 	<doi type="journal_article">10.1006/jmbi.2000.4282</doi>
 	<issn type="print">00222836</issn>
 	<issn type="electronic">10898638</issn>
 	<journal_title>Journal of Molecular Biology</journal_title>
  	<contributors>
  	<contributor first-author="true">
   	<given_name>Y</given_name>
   	<surname>Jiang</surname>
 	</contributor>
 	</contributors>
 	<volume>305</volume>
 	<issue>3</issue>
 	<first_page>377</first_page>
 	<year>2001</year>
 	<publication_type>full_text</publication_type>
 	<article_title> Specific interaction between anticodon nuclease and the tRNALys wobble base </article_title>
</query>
  </body>
</query_result>
</crossref_result>
```
