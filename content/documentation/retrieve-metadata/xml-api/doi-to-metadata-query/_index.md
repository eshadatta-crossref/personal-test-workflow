+++
title = "DOI-to-metadata query"
date = "2020-04-08"
draft = false
author = "Jennifer Kemp"
type = "documentation"
layout = "documentation_single"
documentation_section = ["retrieve-metadata", "xml-api", "doi-to-metadata-query"]
identifier = "documentation/retrieve-metadata/xml-api/doi-to-metadata-query"
rank = 4
weight = 31002
aliases = [
  "/education/retrieve-metadata/xml-api/doi-to-metadata-query",
  "/education/retrieve-metadata/xml-api/doi-to-metadata-query/"
]
+++

Metadata may be retrieved in JSON format using our [REST API](https://api.crossref.org), or in XML format as documented below.

Most DOI-to-metadata queries are done via HTTPS using synchronous [HTTPS queries](/documentation/retrieve-metadata/xml-api/using-https-to-query), but may also be submitted as asynchronous batch queries.

## Example HTTPS query<a id='00424' href='#00424'><i class='fas fa-link'></i></a>

```
https://doi.crossref.org/servlet/query?pid={myemail@crossref.org}&format=unixref&id=DOI
```

## OpenURL query<a id='00425' href='#00425'><i class='fas fa-link'></i></a>

We support DOI queries formatted as [OpenURL](/documentation/retrieve-metadata/openurl) version 0.1 requests. For complete metadata ([UNIXREF](/documentation/retrieve-metadata/xml-output-formats/unixref-query-output-format)) include the `format="unixref"` parameter.
```
https://www.crossref.org/openurl/?pid={myemail@crossref.org}&format=unixref&id=doi:10.1577/H02-043&noredirect=true
```

## Query results: xsd_xml format (default)<a id='00426' href='#00426'><i class='fas fa-link'></i></a>
```
<crossref_result version="2.0" xsi:schemaLocation="https://www.crossref.org/qrschema/2.0 https://www.crossref.org/schema/queryResultSchema/crossref_query_output2.0.xsd" xmlns="https://www.crossref.org/qrschema/2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"> <query_result>
<head>
<doi_batch_id>none</doi_batch_id>
</head>
<body>
<query status="resolved" fl_count="6">
<doi type="journal_article">10.1577/H02-043</doi>
<issn type="print">0899-7659</issn>
<issn type="electronic">1548-8667</issn>
<journal_title>Journal of Aquatic Animal Health</journal_title>
<contributors>
<contributor sequence="first" contributor_role="author">
<given_name>John P.</given_name>
<surname>Hawke</surname>
</contributor>
<contributor sequence="additional" contributor_role="author">
<given_name>Ronald L.</given_name>
<surname>Thune</surname>
</contributor>
<contributor sequence="additional" contributor_role="author">
<given_name>Richard K.</given_name>
<surname>Cooper</surname>
</contributor>
<contributor sequence="additional" contributor_role="author">
<given_name>Erika</given_name>
<surname>Judice</surname>
</contributor>
<contributor sequence="additional" contributor_role="author">
<given_name>Maria</given_name>
<surname>Kelly-Smith</surname>
</contributor>
</contributors>
<volume>15</volume>
<issue>3</issue>
<first_page>189</first_page>
<last_page>201</last_page>
<year media_type="print">2003</year>
<publication_type>full_text</publication_type>
<article_title>Molecular and Phenotypic Characterization of Strains ofPhotobacterium damselaesubsp.piscicidaIsolated from Hybrid Striped Bass Cultured in Louisiana, USA</article_title>
</query>
</body>
</query_result>
</crossref_result>
```

## Query results: UNIXREF format<a id='00427' href='#00427'><i class='fas fa-link'></i></a>
```
<doi_records>
<doi_record owner="10.1080" timestamp="2011-12-08 13:28:43">
<crossref>
<journal>
<journal_metadata language="en">
<full_title>Journal of Aquatic Animal Health</full_title>
<abbrev_title>Journal of Aquatic Animal Health</abbrev_title>
<issn media_type="print">0899-7659</issn>
<issn media_type="electronic">1548-8667</issn>
</journal_metadata>
<journal_issue>
<publication_date media_type="print">
<month>09</month>
<year>2003</year>
</publication_date>
<journal_volume>
<volume>15</volume>
</journal_volume>
<issue>3</issue>
</journal_issue>
<journal_article publication_type="full_text">
<titles>
<title>Molecular and Phenotypic Characterization of Strains of <i>Photobacterium damselae</i>subsp. <i>piscicida</i> Isolated from Hybrid Striped Bass Cultured in Louisiana, USA</title>
</titles>
<contributors>
<person_name sequence="first" contributor_role="author">
<given_name>John P.</given_name>
<surname>Hawke</surname>
</person_name>
<person_name sequence="additional" contributor_role="author">
<given_name>Ronald L.</given_name>
<surname>Thune</surname>
</person_name>
<person_name sequence="additional" contributor_role="author">
<given_name>Richard K.</given_name>
<surname>Cooper</surname>
</person_name>
<person_name sequence="additional" contributor_role="author">
<given_name>Erika</given_name>
<surname>Judice</surname>
</person_name>
<person_name sequence="additional" contributor_role="author">
<given_name>Maria</given_name>
<surname>Kelly-Smith</surname>
</person_name>
</contributors>
<publication_date media_type="print">
<month>09</month>
<year>2003</year>
</publication_date>
<pages>
<first_page>189</first_page>
<last_page>201</last_page>
</pages>
<publisher_item>
<item_number item_number_type="sequence-number">1</item_number>
<identifier id_type="doi">10.1577/H02-043</identifier>
</publisher_item>
<doi_data>
<doi>10.1577/H02-043</doi>
<resource>http://www.tandfonline.com/doi/abs/10.1577/H02-043</resource>
</doi_data>
</journal_article>
</journal>
</crossref>
</doi_record>
</doi_records>
```

## DOI-to-metadata XML batch query<a id='00428' href='#00428'><i class='fas fa-link'></i></a>

DOI-to-metadata queries can also be [performed using XML](/documentation/retrieve-metadata/xml-api/using-https-to-query), allowing the use of control features available only with XML queries.

### Example XML query<a id='00429' href='#00429'><i class='fas fa-link'></i></a>
```
<?xml version = "1.0" encoding="UTF-8"?>
<query_batch version="2.0" xmlns = "https://www.crossref.org/qschema/2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <head>
      <email_address>support@crossref.org</email_address>
      <doi_batch_id>Sample multi resolve</doi_batch_id>
  </head>
  <body>
       <query key="mykey" expanded-results="true">
           <doi>10.1006/jmbi.2000.4282</doi>
       </query>
   </body>
</query_batch>
```
