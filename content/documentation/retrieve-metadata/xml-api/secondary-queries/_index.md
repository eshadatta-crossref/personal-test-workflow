+++
title = "Secondary queries"
date = "2020-04-08"
draft = false
author = "Jennifer Kemp"
type = "documentation"
layout = "documentation_single"
documentation_section = ["retrieve-metadata", "xml-api", "secondary-queries"]
identifier = "documentation/retrieve-metadata/xml-api/secondary-queries"
rank = 4
weight = 31009
aliases = [
  "/education/retrieve-metadata/xml-api/secondary-queries",
  "/education/retrieve-metadata/xml-api/secondary-queries/"
]
+++

Our query engine operates by processing many different rules. By default, all metadata provided in a query is used to generate a match, and a result is only returned if exactly one DOI match is found. Enabling a secondary query instructs the query engine to perform specific searches when the initial search fails to find a match.

## Author/article title secondary query<a id='00447' href='#00447'><i class='fas fa-link'></i></a>

Queries by default use all metadata present in the request. Requests with only an author and title are treated as [author/article title queries](/documentation/retrieve-metadata/xml-api/author-article-title-query/). When other fields are present, the request is submitted as a metadata search.

Setting the *secondary-query* attribute to *author-title* causes an author/title search to be performed when the initial metadata search fails to find a match.

```
<query key="cit-3" enable-multiple-hits="true" secondary-query="author-title">
<issn>0360-3016</issn>
<volume>54</volume>
<issue>2</issue>
<first_page>215</first_page>
<year>2002</year>
<author>Kim</author>
<article_title match="fuzzy">Potential radiation sensitizing effect of SU5416 by down-regulating the COX-2 expression in human lung cancer cells</article_title>
</query>
```

*Year* is included in the author/article title secondary query when present. An author/article title query will only be performed if both author and article title are included in the query.

## Multiple hits secondary query<a id='00448' href='#00448'><i class='fas fa-link'></i></a>

By default, the query engine will only return a result if a single DOI is found. Queries returning multiple results (or hits) are unresolved. Setting the *secondary-query* attribute to "multi-hit" instructs the query engine to return all available results.

```
<query key="cit-3" secondary-query="multiple-hits">
<issn>0360-3016</issn>
<volume>54</volume>
<issue>2</issue>
<first_page>215</first_page>
<year>2002</year>
<author>Kim</author>
<article_title match="fuzzy">Potential radiation sensitizing effect of SU5416 by down-regulating the COX-2 expression in human lung cancer cells</article_title>
</query>
```

This option can also be performed by setting attribute `enable-multiple-hits` ([learn more about allowing multiple hits](/documentation/retrieve-metadata/xml-api/allowing-multiple-hits)) to *true*.

## Author/title multiple hits secondary query<a id='00449' href='#00449'><i class='fas fa-link'></i></a>

Setting the `secondary-query` attribute to "author-title-multiple-hits" instructs the query engine to perform an author/title query and return multiple hits if the initial search fails.

```
<query key="cit-3" enable-multiple-hits="true" secondary-query="author-title-multiple-hits">
<issn>0360-3016</issn>
<volume>54</volume>
<issue>2</issue>
<first_page>215</first_page>
<year>2002</year>
<author>Kim</author>
<article_title match="fuzzy">Potential radiation sensitizing effect of SU5416 by down-regulating the COX-2 expression in human lung cancer cells</article_title>
</query>
```

## Secondary query results<a id='00450' href='#00450'><i class='fas fa-link'></i></a>

Query results will specify which method was used to return the result. Successful metadata searches will specify `metadata`:
```
<query key="cit-3" status="multiresolved" fl_count="0" query_mode="metadata">
```

Queries resolved by the secondary author/title query will specify `author-title`:
```
<query status="resolved" fl_count="0" query_mode="author-title">
```

Queries with multiple hits will have a status of `multiresolved`
```
<query status="multiresolved" query_mode="metadata">
```
