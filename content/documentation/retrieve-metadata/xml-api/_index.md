+++
title = "XML API"
date = "2020-04-08"
draft = false
author = "Jennifer Kemp"
type = "documentation"
layout = "documentation_single"
documentation_section = ["retrieve-metadata", "xml-api"]
identifier = "documentation/retrieve-metadata/xml-api"
rank = 4
weight = 31000
aliases = [
  "/education/retrieve-metadata/xml-api",
  "/education/retrieve-metadata/xml-api/"
]
+++

The XML API supports XML-formatted querying. XML queries give you significant control over the DOI matching process. The XML API is designed to (typically) return only one DOI, the one that best fits the metadata supplied in the query, and is therefore suitable for automated matching. Query results are returned in XML, and will contain a full or abbreviated metadata record for matched items, depending on the request. The query input schema file is [crossref_query_input2.0.xsd](https://data.crossref.org/schemas/crossref_query_input2.0.xsd) - learn more in our [schema documentation](https://data.crossref.org/reports/help/schema_doc/query_input2.0/query_input.html).

The XML API also supports [DOI-to-metadata queries](/documentation/retrieve-metadata/xml-api/doi-to-metadata-query).
