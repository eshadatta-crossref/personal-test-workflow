+++
title = "Using HTTPS to query"
date = "2020-04-08"
draft = false
author = "Jennifer Kemp"
type = "documentation"
layout = "documentation_single"
documentation_section = ["retrieve-metadata", "xml-api", "using-https-to-query"]
identifier = "documentation/retrieve-metadata/xml-api/using-https-to-query"
rank = 4
weight = 31001
aliases = [
  "/education/retrieve-metadata/xml-api/using-https-to-query",
  "/education/retrieve-metadata/xml-api/using-https-to-query/"
]
+++

Most queries sent to our XML API will use HTTPS. HTTPS querying requires either a member account or that you supply your email address in the query. Use a DOI-to-metadata query when you know the DOI and want to find its associated metadata.

## HTTPS query examples<a id='00414' href='#00414'><i class='fas fa-link'></i></a>

### DOI-to-metadata query example<a id='00415' href='#00415'><i class='fas fa-link'></i></a>
```
https://doi.crossref.org/servlet/query?pid={email@address.com}&id=10.1577/H02-043
```

### DOI-to-metadata query requesting UNIXSD results<a id='00416' href='#00416'><i class='fas fa-link'></i></a>
```
https://doi.crossref.org/servlet/query?pid={email@address.com}&format=unixsd**&id=10.1577/H02-043
```

### DOI-to-metadata queries may also be supplied using this format<a id='00417' href='#00417'><i class='fas fa-link'></i></a>
```
https://doi.crossref.org/search/doi?pid={email@address.com}&doi=10.1577/H02-043
```

### XML query<a id='00418' href='#00418'><i class='fas fa-link'></i></a>
```
https://doi.crossref.org/servlet/query?usr={email@address.com}&format=unixref&qdata=<?xml version="1.0"?><query_batch version="2.0"...
```

where:
* *usr* is your email address
* *format* is the desired [results format](/documentation/retrieve-metadata/xml-output-formats/)
* *qdata* is the query data (XML is the recommended format)
* *id* is a Crossref DOI

## XML querying<a id='00419' href='#00419'><i class='fas fa-link'></i></a>

An XML query must contain complete and valid [query XML](/documentation/retrieve-metadata/xml-api/xml-query-examples). Multiple queries may be included in a single XML file, but the `<query>` element is repeatable. For best results, do not exceed 5 queries per HTTPS XML query request.

### Multiple queries in a single request<a id='00420' href='#00420'><i class='fas fa-link'></i></a>
```
https://doi.crossref.org/servlet/query?usr=username&pwd=password&qdata=<?xml version = "1.0" encoding="UTF-8"?>  
<query_batch xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="2.0" xmlns="http://www.crossref.org/qschema/2.0"  xsi:schemaLocation="http://www.crossref.org/qschema/2.0 http://www.crossref.org/qschema/crossref_query_input2.0.xsd"><head><email_address>support@crossref.org</email_address><doi_batch_id>ABC_123_fff</doi_batch_id> </head> <body> <query key="query1" enable-multiple-hits="true" forward-match="false"><issn match="optional">15360075</issn><journal_title match="exact">American Journal of Bioethics</journal_title><author match="fuzzy" search-all-authors="false">Agich</author><volume match="fuzzy">1</volume><issue>1</issue><first_page>50</first_page><year>2001</year><article_title>The Salience of Narrative for Bioethics</article_title></query><query key="query2" enable-multiple-hits="true"><unstructured_citation>Hungate, B. A., &amp; Hampton, H. M. (2012). Ecosystem services: Valuing ecosystems for climate. Nature Climate Change, 2(3), 151-152.</unstructured_citation></query></body></query_batch>
```

### HTTPS XML query example<a id='00421' href='#00421'><i class='fas fa-link'></i></a>
```
https://doi.crossref.org/servlet/query?usr=email@address.com&qdata=<?xml version = "1.0" encoding="UTF-8"?>  
<query_batch xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="2.0" xmlns="http://www.crossref.org/qschema/2.0"  xsi:schemaLocation="http://www.crossref.org/qschema/2.0 http://www.crossref.org/qschema/crossref_query_input2.0.xsd"><head><email_address>support@crossref.org</email_address><doi_batch_id>ABC_123_fff</doi_batch_id> </head> <body> <query key="1178517" enable-multiple-hits="false" forward-match="false"><issn match="optional">15360075</issn><journal_title match="exact">American Journal of Bioethics</journal_title><author match="fuzzy" search-all-authors="false">Agich</author><volume match="fuzzy">1</volume><issue>1</issue><first_page>50</first_page><year>2001</year><article_title>The Salience of Narrative for Bioethics</article_title></query></body></query_batch>
```

### HTTPS XML query with an unstructured (formatted) citation<a id='00422' href='#00422'><i class='fas fa-link'></i></a>

Learn more about [querying with an unstructured (formatted) citation](/documentation/retrieve-metadata/xml-api/querying-with-formatted-citations)

```
https://doi.crossref.org/servlet/query?usr=email@address.com&format=unixref&qdata=<?xml version = "1.0" encoding="UTF-8"?><query_batch xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="2.0" xmlns="http://www.crossref.org/qschema/2.0" xsi:schemaLocation="http://www.crossref.org/qschema/2.0 http://www.crossref.org/qschema/crossref_query_input2.0.xsd"><head><email_address>your@email.org</email_address><doi_batch_id>01032012</doi_batch_id></head><body><query key="q1" enable-multiple-hits="true"><unstructured_citation>Hungate, B. A., &amp; Hampton, H. M. (2012). Ecosystem services: Valuing ecosystems for climate. Nature Climate Change, 2(3), 151-152.</unstructured_citation></query></body></query_batch>
```

### HTTPS XML query with URL encoding<a id='00423' href='#00423'><i class='fas fa-link'></i></a>
```
https://doi.crossref.org/servlet/query?usr=email@address.com&qdata=%3C?xml%20version=%221.0%22?%3E%3Cquery_batch%20version=%222.0%22%20xsi:schemaLocation=%22https://www.crossref.org/qschema/2.0%20https://www.crossref.org/qschema/crossref_query_input2.0.xsd%22%20xmlns=%22https://www.crossref.org/qschema/2.0%22%20xmlns:xsi=%22http://www.w3.org/2001/XMLSchema-instance%22%3E%3Chead%3E%3Cemail_address%3Esupport@crossref.org%3C/email_address%3E%3Cdoi_batch_id%3EABC_123_fff%3C/doi_batch_id%3E%20%3C/head%3E%20%3Cbody%3E%20%3Cquery%20key=%221178517%22%20enable-multiple-hits=%22false%22%20forward-match=%22false%22%3E%3Cissn%20match=%22optional%22%3E15360075%3Cissn%3Cjournal_title%20match=%22exact%22%3EAmerican%20Journal%20of%20Bioethics%3C/journal_title%3E%3Cauthor%20match=%22fuzzy%22%20search-all-authors=%22false%22%3EAgich%3C/author%3E%3Cvolume%20match=%22fuzzy%22%3E1%3C/volume%3E%3Cissue%3E1%3C/issue%3E%3Cfirst_page%3E50%3C/first_page%3E%3Cyear%3E2001%3C/year%3E%3Carticle_title%3EThe%20Salience%20of%20Narrative%20for%20Bioethics%3C/article_title%3E%3C/query%3E%3C/body%3E%3C/query_batch%
```

Note that some characters must be URL-encoded:

|Character|Name|URL code|
|--- |--- |--- |
|;|Semi-colon|%3B|
|/|Slash, virgule, separatrix, or solidus|%2F|
|?|Question mark|%3F|
|:|Colon|%3A|
|@|At sign, arobase|%40|
|=|Equals sign|%3D|
|&|Ampersand|%26|
|lf|line feed|%0A|
