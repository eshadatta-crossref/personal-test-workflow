+++
title = "Private repository"
date = "2022-07-15"
draft = false
author = "Kathleen Luschek"
type = "documentation"
layout = "documentation_single"
documentation_section = ["similarity-check", "ithenticatev2-MTS-account-setup", "private-repository"]
identifier = "documentation/similarity-check/ithenticatev2-mts-account-setup/private-repository"
rank = 4
weight = 100280

+++

This section is for Similarity Check account administrators who are integrating iThenticate v2 with their Manuscript Submission System (MTS).

* Using iThenticate v1 instead? Go to the [v1 account administrators section](/documentation/similarity-check/ithenticate-account-setup/settings).
* Using iThenticate v2 through a browser instead? Go to [setting up iThenticate v2 for browser users](/documentation/similarity-check/ithenticatev2-account-setup/private-repository).
* Not sure if you're using iThenticate v1 or iThenticate v2? [More here](/documentation/similarity-check/upgrading/v1-or-v2).
* Not sure whether you're an account administrator? [Find out here](/documentation/similarity-check/ithenticatev2-account-setup/).

## Private Repository - ScholarOne only

The Submitted Works repository (or Private Repository) is a new feature in iThenticate v2. The only MTS that currently integrates with this feature is ScholarOne. This feature allows users to find similarity not just across Turnitin’s extensive Content Database but also across all previous manuscripts submitted to your iThenticate account for all the journals you work on. This would allow you to find collusion between authors or potential cases of duplicate submissions.

## How does this work?

You have received a manuscript from Author 1 and have decided to index this manuscript into your Submitted Works repository. At some point later you receive a new manuscript from Author 2. When generating your Similarity Report, you have decided to check against your Submitted Works repository. There is a paragraph in the manuscript from Author 2 which matches a paragraph in the manuscript from Author 1. This would be highlighted within your Similarity Report as a match against your Submitted Works repository.

<figure><img src='/images/documentation/SC-v2-submitted-works-author-match.png' alt='v2 submitted works author match' title='' width='100%'></figure>

By clicking on this match you can see the full text of the submission you’ve matched against:

<figure><img src='/images/documentation/SC-v2-submitted-works-match-clicking.png' alt='v2 submitted works match clicking' title='' width='100%'></figure>

And details about the submission, such as the name and email address of the user who submitted it, the date it was submitted and the title of the submission:

<figure><img src='/images/documentation/SC-v2-submitted-works-match-details.png' alt='v2 submitted works match details' title='' width='100%'></figure>

The ability to see the full source text and the details can both be switched off individually.

<figure><img src='/images/documentation/SC-v2-submitted-works-switch.png' alt='v2 submitted works match switch' title='' width='100%'></figure>

## Setting up the Submitted Works repository

If you are using a third party integration then you should have options inside your MTS when setting up your configuration with iThenticate to decide whether submissions will be indexed to the Submitted Works repository and whether generated Similarity Reports will match against the Submitted Works.

## Important: This feature means that sensitive data could be shared between different journals using your iThenticate account

The Submitted Works repository is shared across your entire iThenticate account. This means regardless of whether a submission was made natively from the iThenticate website or through an integration, all Similarity Reports which match against the Submitted Works repository will potentially match against any submissions which were indexed within it.  This means that an editor working on one journal may be able to view submissions for another journal. If you are worried about giving your users access to sensitive data, we recommend switching this functionality off.

## Submitted Works repository FAQs

#### Q. How much does this feature cost to use?<br>
This feature comes free with every v2 account.

#### Q. How many submissions can I index to my private repository?<br>
There is no limit to the number of submissions you can index.

#### Q. Can I delete submissions from my private repository?<br>
Yes. An Administrator can find and delete a submission using the Paper Lookup Tool. Go to [Turnitin's help documentation](https://help.turnitin.com/crossref-similarity-check/administrator/paper-lookup.htm) for more information.
