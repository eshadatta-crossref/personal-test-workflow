+++
title = "Upgrade FAQs"
date = "2022-07-15"
draft = false
author = "Amanda Bartell"
toc = true
type = "documentation"
layout = "documentation_single"
documentation_section = ["similarity-check", "upgrading", "FAQs" ]
identifier = "documentation/similarity-check/upgrading/faqs"
rank = 4
weight = 100503
+++

### Q. What are the big differences between iThenticate v1 and v2 for all users?

As well as the faster, more user-friendly and responsive interface there are a few new features in iThenticate v2 which everyone can benefit from.

#### The Submitted Works repository (or Private Repository)

The Submitted Works Repository (or Private Repository) is a new feature in iThenticate v2 which is now available to Crossref members. This feature allows users to find similarity not just across Turnitin’s extensive Content Database but also across all previous manuscripts submitted to your iThenticate account for all the journals you work on. This would allow you to find collusion between authors or potential cases of duplicate submissions.

* Using iThenticate v2 in the browser? Find out more about the [Submitted Works Repository](/documentation/similarity-check/ithenticatev2-account-setup/private-repository/).<br>
* Using iThenticate v2 through an integration with your MTS? This feature is currently only available to ScholarOne Manuscripts users. Find out more about the [Submitted Works Repository](/documentation/similarity-check/ithenticatev2-mts-account-setup/private-repository/).

#### Better identification of preprints

* Using iThenticate v2 in the browser? Find out more about [Preprint labels and exclusions](/documentation/similarity-check/ithenticatev2-account-setup/exclusions/).<br>
* Using iThenticate v2 through an integration with your MTS? Find out more about [Preprint labels and exclusions](/documentation/similarity-check/ithenticatev2-mts-account-setup/exclusions/).

#### Identification of hidden text or suspicious character replacement

The new “red flag” feature signals the detection of hidden text such as text/quotation marks in white font, or suspicious character replacement. Find out more about the [flags](https://help.turnitin.com/crossref-similarity-check/user/the-similarity-report/flags.htm)

#### No need to manually exclude bibliography or citations

In iThenticate v2, there is no need to choose to exclude the bibliography or citations in papers written in English, as Turnitin will automatically exclude these from your Similarity Report. For more information, please see [Turnitin's documentation here](https://help.turnitin.com/crossref-similarity-check/user/the-similarity-report/bibliography-and-quote-exclusion-definitions.htm).

Please note, if you using an MTS integration and you find that the bibliography or citations are not being excluded, please reach out to your MTS, as they will need to adjust the settings on their end to ensure these exclusions are in place.

### Q. Are there other differences between v1 and v2 that those who integrate with an MTS will also notice?

Yes, there are some other big improvements to iThenticate for members who integrate iThenticate with their MTS.

#### View your Similarity Reports within your MTS

You can now both submit your manuscripts AND view your Similarity Report from within your MTS.

#### No folders if you integrate with an MTS

If you are using iThenticate through an integration then there is no need to set up any folders within your iThenticate account. Unlike in iThenticate v1, integrations will not send submissions to a folder which is accessible by logging into iThenticate through your browser. Submissions created by your MTS will only be accessible through your MTS.

#### No individual users in iThenticate if you integrate with an MTS

If you are using iThenticate through an integration with an MTS, then you do not need to set up any new users on your new iThenticate account. This is because all the submissions from your MTS will be made by the API key you’ve set, rather than individual users.

### Q. Can I move my existing users over from iThenticate v1 to iThenticate v2?

If you are integrating iThenticate v2 with your MTS, you don't need to move or add your existing users. [More here](/documentation/similarity-check/ithenticatev2-mts-account-setup/manage-users/).

If you are using iThenticate v2 in the browser, you can import your users from v1 in bulk by following the steps below:

1. Export users from v1: Log into your iThenticate v1 account and go to Manage Users and press the download xls link at the top of the Profile page. This will give you a complete list of users from your account.

2. Import users to v2: You can find instructions on how to import users to your v2 account [here](https://help.turnitin.com/crossref-similarity-check/administrator/users/adding-a-list-of-users.htm). Using the file you downloaded from v1 you should be able to copy and paste the required data into the template provided.

### Q. When I move to iThenticate v2, will I immediately lose access to iThenticate v1?

Once you have access to iThenticate v2, you may need some time to transition all your journals over from iThenticate v1.

#### Using iThenticate directly in the browser

You will be able to continue to use your iThenticate v1 account for several months so you have time to move everyone over. After a few months you will lose your ability to submit *new* manuscripts through your iThenticate v1 account, but you will still be able to access your old reports. We will email your Similarity Check editorial contact with several months notice before we remove your ability to submit new manuscripts.

#### Using iThenticate through an integration with an MTS

As soon as you update your integration to work with your iThenticate v2 account, you will lose your ability to submit new manuscripts to v1 through that MTS. However, you will still be able to access your past reports.

If you use more than one MTS, you can use iThenticate v2 with one MTS, but continue to use your iThenticate v1 account with other MTSs that aren't yet ready to integrate with iThenticate v2. We won't remove your ability to check new documents in iThenticate v1 until all the MTSs that you work with are able to work with v2, and you have integrated all of them with v2.

### Q. Can I still view my previous submissions from iThenticate v1 in iThenticate v2?

The submissions you made through iThenticate v1 will not be accessible directly through iThenticate v2. However, you will still be able to access your historical reports.

#### If you are using an integration with an MTS

If you were using iThenticate v1 through an integration which is now using iThenticate v2, then for most manuscript tracking systems we have asked them to continue to support access to your v1 reports. This means that when you attempt to access a historical report it will open in the old document viewer. Each MTS is only intending to support access to v1 historical reports for a limited period of time. Please check with your MTS for details as to when they will no longer support this functionality.

Once access to historical reports through your MTS is no longer possible you will still be able to access your historical reports by logging into your old iThenticate v1 account directly in the browser. You will need go to ithenticate.com and log in using the original administrator credentials for iThenticate v1. Once logged in, you can run reports to find historical reports. There are instructions for how to do this on [Turnitin's website](https://help.turnitin.com/crossref-similarity-check/v1/administrator/reports/usage-reports.htm).

At some point in the future iThenticate v1 will be sunset and you will no longer be able to access your old account. Before this happens a solution will be in place to ensure you can still access your historical reports.

#### If you use iThenticate through the browser

Once you have moved to iThenticate v2 you will still be able to access your historical reports through your old iThenticate v1 account. You will need to log into this by going to ithenticate.com and logging in using the administrator credentials previously used. Once logged in, you can run reports to find historical reports. There are instructions for how to do this on [Turnitin's website](https://help.turnitin.com/crossref-similarity-check/v1/administrator/reports/usage-reports.htm).

At some point in the future iThenticate v1 will be sunset and you will no longer be able to access your old account. Before this happens a solution will be in place to ensure you can still access

### Q. Is there an extra charge for using iThenticate v2?

There is no charge to upgrade to iThenticate v2, and the costs to check your manuscripts are the same as in iThenticate v1. If you are using iThenticate v2 for some of your journals, but continue to use iThenticate v1 for some of your other journals, your volume discounts will apply across both instances.

However, it's important to note that we can't tell if you check the same document in both iThenticate v1 and iThenticate v2. If you do this, you will be charged twice.

### Q. My organization uses more than one Manuscript Tracking System. One of my MTSs is able to integrate with iThenticate v2 already, but the other one can't yet integrate with v2. Should I upgrade or not?

If we have contacted you about upgrading to iThenticate v2, this will give you access to a v2 account, and you will be able to link this with the MTS that's ready. You can continue using iThenticate v1 with the other MTS while you wait for them to be able to integrate with v2.

### Q. If I use iThenticate v2 with one of my MTSs, and iThenticate v1 with another (as the other MTS can't yet integrate with v2), what happens to my volume discounts?

Your volume discounts will apply against the total number of manuscripts that you've checked across both instances.

Please note though - we can't tell if you've checked the same document in both v1 and v2. If you do this, you will be charged twice, so it's important to keep them separate.  
