+++
title = "v1 or v2?"
date = "2022-07-15"
draft = false
author = "Amanda Bartell"
type = "documentation"
layout = "documentation_single"
documentation_section = ["similarity-check", "upgrading", "v1-or-v2" ]
identifier = "documentation/similarity-check/upgrading/v1-or-v2"
rank = 4
weight = 100502
+++

Not sure if you are currently using iThenticate v1 or iThenticate v2?

## iThenticate v1

iThenticate v1 looks like this:

<figure><img src='/images/documentation/SC-find-your-way-around.png' alt='Find your way around for users' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image65">Show image</button>
<div id="image65" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-find-your-way-around.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

If you access iThenticate through the browser, you will use the address ithenticate.com.

## iThenticate v2

iThenticate v2 looks like this:

<figure><img src='/images/documentation/SC-v2-welcome-screen.png' alt='v2 welcome screen' title='' width='75%'></figure>

If you access iThenticate through the browser, you will use a bespoke URL: https://crossref-[your member ID].turnitin.com
