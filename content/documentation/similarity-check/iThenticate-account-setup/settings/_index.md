+++
title = "Admin settings"
date = "2020-05-19"
draft = false
author = "Kathleen Luschek"
type = "documentation"
layout = "documentation_single"
documentation_section = ["similarity-check", "ithenticate-account-setup", "settings"]
identifier = "documentation/similarity-check/ithenticate-account-setup/settings"
rank = 4
weight = 100203
aliases = [
  "/education/similarity-check/ithenticate-account-setup/settings",
  "/education/similarity-check/ithenticate-account-setup/settings/"
]

+++
This section shows Similarity Check account administrators using iThenticate v1 how to update their account admin settings. You need to follow the steps in this section before you start to set up your users and share the account with your colleagues.

If you are using iThenticate v2 rather than iThenticate v1, there are separate instructions for you.
* Using iThenticate v2 directly in the browser - go to [setting up iThenticate v2](/documentation/similarity-check/ithenticatev2-account-setup/settings).
* Integrating iThenticate v2 with your MTS - go to [setting up your MTS integration](/documentation/similarity-check/ithenticatev2-mts-account-setup/).

Not sure if you're using iThenticate v1 or iThenticate v2? [More here](/documentation/similarity-check/upgrading/v1-or-v2).

Not sure whether you're an account administrator? [Check here](/documentation/similarity-check/ithenticate-account-setup/admin-account).

{{< snippet "/_snippet-sources/similarity-check-settings.md" >}}
