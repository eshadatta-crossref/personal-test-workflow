+++
title = "Administrator checklist"
date = "2020-05-19"
draft = false
author = "Kathleen Luschek"
type = "documentation"
layout = "documentation_single"
documentation_section = ["similarity-check", "ithenticate-account-setup", "similarity-check-administrator-checklist"]
identifier = "documentation/similarity-check/ithenticate-account-setup/administrator-checklist"
rank = 4
weight = 100202

+++

This section is for Similarity Check account administrators using iThenticate v1.

If you are using iThenticate v2 rather than iThenticate v1, there are separate instructions for you.
* Using iThenticate v2 directly in the browser - go to [setting up iThenticate v2](/documentation/similarity-check/ithenticatev2-account-setup/administrator-checklist).
* Integrating iThenticate v2 with your MTS - go to [setting up your MTS integration](/documentation/similarity-check/ithenticatev2-mts-account-setup/administrator-checklist).

Not sure if you're using iThenticate v1 or iThenticate v2? [More here](/documentation/similarity-check/upgrading/v1-or-v2).

Not sure whether you're an account administrator? [Check here](/documentation/similarity-check/ithenticate-account-setup/admin-account).

### Administrator Checklist for iThenticate v1<a id='00624' href='#00624'><i class='fas fa-link'></i></a>

As an administrator, you create and manage the users on your account, and you decide how your organization uses the iThenticate tool. You’ll find the system easier to use if you set it up correctly to start with, so do read through the checklist below carefully and make sure you've set up your account how you want it before inviting any users to your account.

1. [How do you want to manage users?](#00614)
2. [How will you use the folders in iThenticate?](#00615)
3. [How will you use the exclusions functionality?](#00616)
4. [Which iThenticate repositories will you want to check your manuscripts against?](#00617)
5. [How will you budget for your document checking fees?](#00618)

### 1. How do you want to manage users? (v1)<a id='00614' href='#00614'><i class='fas fa-link'></i></a>

You can set up users individually, or put them into groups and manage their settings at group level.

* How many users do you need to set up?
* Do you need them to have different permissions or access to different folders? Do you want users to be able to see each other’s folders?
* Do you want to set up groups to manage the users?
* Do you want to be the only account administrator, or do you want to add other administrators?

Learn more about [how to manage users](/documentation/similarity-check/ithenticate-account-setup/manage-users/).

### 2. How will you use the folders in iThenticate? (v1)<a id='00615' href='#00615'><i class='fas fa-link'></i></a>

If you set up different folders in iThenticate to manage the manuscripts you’re checking, you’ll be able to:

* Assign different users or groups to each folder
* Set up different exclusions on each folder
* Choose which repositories to include in each folder
* Report separately on different folders.

You may choose to set up different folders for different titles or years of publication, for example.

Learn more about how to [manage folders](/documentation/similarity-check/ithenticate-account-use/folders/).

### 3. How will you use the exclusions functionality? (v1)<a id='00616' href='#00616'><i class='fas fa-link'></i></a>

Exclusions allow you to set iThenticate to ignore particular phrases, document sections, common words, and URLs, so that they are not flagged in your account’s Similarity Reports.

We recommend starting without any exclusions to avoid excluding anything important. Once your users are experienced enough to identify words and phrases that appear frequently but are not potentially problematic matches (and can therefore be ignored) in a Similarity Report, you can start carefully making use of this feature.

* At account level, administrators can set [phrase exclusions](/documentation/similarity-check/ithenticate-account-setup/account-info#00573) and [URL filters](/documentation/similarity-check/ithenticate-account-setup/account-info#00572).
* At folder level, administrators can exclude quotes, bibliography, phrases, small matches, small sources, abstracts, and methods and materials. Users can also [edit filters and exclusions for existing folders](/documentation/similarity-check/ithenticate-account-use/folders#00586).
* Users can [set exclusions when setting up a new folder](/documentation/similarity-check/ithenticate-account-use/folders#00586) and [adjust some settings at Similarity Report level](/documentation/similarity-check/ithenticate-account-use/similarity-report-use#00604).

Set clear guidelines for your users so they understand the settings you have already applied, and can make skilful use of the options they can choose for themselves at report level.

### 4. Which iThenticate repositories will you want to check your manuscripts against? (v1)<a id='00617' href='#00617'><i class='fas fa-link'></i></a>

iThenticate has a number of content repositories, grouped by the type of content they contain, including: Crossref, Crossref posted content, Internet, Publications, Your Indexed Documents.

You can choose which of iThenticate’s repositories you’re checking your manuscripts against. We recommend including them all to start with.

* The person (whether an administrator or a user) who sets up a folder selects the repositories to check against for that folder. When the folder is shared, other users cannot adjust the repositories selected.

Learn more about [choosing which repositories to search against](/documentation/similarity-check/ithenticate-account-use/folders#00587).

### 5. How will you budget for your document checking fees? (v1)<a id='00618' href='#00618'><i class='fas fa-link'></i></a>

There’s a [charge for each document checked](/fees#similarity-check-per-document-fees), and you’ll receive an invoice in January each year for the documents you’ve checked in the previous year. If you're a member of Crossref through a Sponsor, your Sponsor will receive this invoice.

As well as setting a Similarity Check document fees budget for your account each year, it’s useful to monitor document checking and see if you’re on track. You can monitor your usage in the reports section of the iThenticate platform. Ask yourself:

* How many documents do you plan to check?
* How often do you want to monitor usage? Set yourself a reminder to check your usage reports periodically.
* How do you want to segment your report? You can report separately by groups of users, so think about what types of groups would make sense for your circumstances.

Learn more about how [usage reports](/documentation/similarity-check/ithenticate-account-setup/manage-users#00577) can help you monitor the number of documents checked on your account.

It’s a good idea to come back to these questions periodically, consider how your use of the tool is evolving, and make changes accordingly.
