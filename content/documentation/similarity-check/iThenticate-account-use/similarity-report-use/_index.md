+++
title = "Working with your Similarity Report"
date = "2020-05-19"
draft = false
author = "Kathleen Luschek"
type = "documentation"
layout = "documentation_single"
documentation_section = ["similarity-check", "iThenticate-account-use", "similarity-report-use"]
identifier = "documentation/similarity-check/ithenticate-account-use/similarity-report-use"
rank = 4
weight = 100305
aliases = [
  "/education/similarity-check/ithenticate-account-use/similarity-report-use",
  "/education/similarity-check/ithenticate-account-use/similarity-report-use/"
]
+++

{{< snippet "/_snippet-sources/iThenticateversion.md" >}}

**[v2 Working with your Similarity Report](https://help.turnitin.com/crossref-similarity-check/user.htm#TheSimilarityReport)**

**v1 Working with your Similarity Report, keep reading:**


Once you have [accessed your Similarity Report](/documentation/similarity-check/ithenticate-account-use/similarity-report-create/), you can:

* [Download Similarity Report PDF](#00603)
* Apply [filters and exclusions in individual Similarity Reports](#00604)
  * [Exclude a match](#00605)
  * [Excluded sources list](#00606)
* Access [the text-only report](#00607)

## Download Similarity Report PDF (v1)<a id='00603' href='#00603'><i class='fas fa-link'></i></a>

To download a *Similarity Report* as a print-friendly .pdf document, click the *print* icon at the bottom left of the *Document Viewer*.

<figure><img src='/images/documentation/SC-Similarity-Report-download-pdf.png' alt='Download Similarity Report in PDF' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image90">Show image</button>
<div id="image90" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-Similarity-Report-download-pdf.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

The .pdf created is based on the current view of the Similarity Report, so a version created while in *Match Overview* will create a .pdf with color-coded highlights.

## Filters and exclusions in individual Similarity Reports (v1)<a id='00604' href='#00604'><i class='fas fa-link'></i></a>

You can use filters and exclusions to remove certain elements from being checked for similarity, and help you focus on more significant matches. The functions for excluding material are approximate - they are not perfectly accurate. Take care when choosing what to exclude, as you may miss important matches. At folder level, all users can [set filters and exclusions](/documentation/similarity-check/ithenticate-account-use/folders#00586), and administrators can also set [URL filters](/documentation/similarity-check/ithenticate-account-setup/account-info#00572) and [phrase exclusions](/documentation/similarity-check/ithenticate-account-setup/account-info#00573). These settings will apply to any documents within the folder. But you can also set filters and exclusions on an individual document, so they only apply to the Similarity Report for that specific document.

Start from the *Document Viewer*, and click the *filters* icon at the bottom of the sidebar to see the *Filters & Settings* menu.

<figure><img src='/images/documentation/SC-Similarity-Report-filters-settings.png' alt='Filters and settings' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image91">Show image</button>
<div id="image91" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-Similarity-Report-filters-settings.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

The filters and exclusions options are:

* **Exclude quoted or bibliographic material**: Click the check-box next to *Exclude Quotes* or *Exclude Bibliography*, then click *Apply Changes* at the bottom of the *Filter & Settings* sidebar.
* **Exclude small sources**: Click the check-box for excluding by words or %, and enter a numerical value for sources to be excluded from this Similarity Report. To turn off excluding small sources, select *Don’t exclude by size*. Click *Apply Changes* at the bottom of the *Filter & Settings* sidebar. This setting will affect the *All Sources* view of the side panel.
* **Exclude small matches**: Under *Exclude matches that are less than*, choose *words*, and enter the numerical value for match instances to be excluded from this Similarity Report. To turn off excluding small matches, select *Don’t exclude*. Click *Apply Changes* at the bottom of the *Filter & Settings* sidebar. This setting will affect the *Match Overview* view of the side panel.
* **Exclude sections**: Under *Exclude Sections*, choose the sections you would like to exclude:
  * abstract
  * methods and materials (including variations)
iThenticate will exclude sections of the submitted document with headers containing the excluded words: 'abstract', 'method and materials', 'methods', 'method', 'materials', and 'materials and methods'.

### Exclude a match (v1)<a id='00605' href='#00605'><i class='fas fa-link'></i></a>

If you decide that a match does not need to be flagged, you can exclude the source from the Similarity Report through *Match Breakdown* or *All Sources*. The Similarity Score will be recalculated, and may change the current percentage of the Similarity Report.

To access *Match Breakdown* from *Match Overview*, hover over the match for which you would like to view the underlying sources, and click the *arrow* icon.

<figure><img src='/images/documentation/SC-Similarity-Report-view-match-breakdown.png' alt='View match breakdown' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image92">Show image</button>
<div id="image92" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-Similarity-Report-view-match-breakdown.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

In *Match Breakdown*, click *Exclude Sources*, and select the sources you would like to remove by selecting the check-box next to each, then click the *Exclude* button.

<figure><img src='/images/documentation/SC-Similarity-Report-exclude-sources.png' alt='Exclude sources' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image93">Show image</button>
<div id="image93" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-Similarity-Report-exclude-sources.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

To exclude an entire source match from *All Sources*, select *Exclude Sources*, select the sources you would like to remove by selecting the check-box next to each, then click the *Exclude* button.

### Excluded sources lis (v1)<a id='00606' href='#00606'><i class='fas fa-link'></i></a>

The *excluded sources* list shows all sources excluded from the Similarity Report. To see the excluded sources list, click the *excluded sources* icon at the bottom of the sidebar.

<figure><img src='/images/documentation/SC-Similarity-Report-excluded-sources-icon.png' alt='Excluded sources icon' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image94">Show image</button>
<div id="image94" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-Similarity-Report-excluded-sources-icon.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

Click the check-box next to any source you would like to re-include in the Similarity Report, and click the *Restore* button to include the source in the Similarity Report. To restore all of the sources that were excluded from the report, click the *Restore All* button. The Similarity Score will be recalculated.

<figure><img src='/images/documentation/SC-Similarity-Report-excluded-sources.png' alt='Excluded sources' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image95">Show image</button>
<div id="image95" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-Similarity-Report-excluded-sources.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

## The text-only report (v1)<a id='00607' href='#00607'><i class='fas fa-link'></i></a>

Start in the *Document Viewer*, and click the *Text-Only Report* button at the bottom right to see the Similarity Report without document formatting. The report will stay in text-only view mode (even if you close and reopen it) until you click *Document Viewer* to return to that mode.

<figure><img src='/images/documentation/SC-Similarity-Report-text-only-report.png' alt='Text-only report' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image96">Show image</button>
<div id="image96" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-Similarity-Report-text-only-report.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

Along the top of the screen, the *document information* bar shows important details about the submitted document (including the date the report was processed, word count, the folder the document was submitted from, the number of matching documents found in the selected databases and the similarity index), and a menu bar with various options. Use the information bar drop-down to switch between uploaded documents in the same folder.

<figure><img src='/images/documentation/SC-Similarity-Report-text-only-report-menu-bar.png' alt='Text-only report menu bar' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image97">Show image</button>
<div id="image97" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-Similarity-Report-text-only-report-menu-bar.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

The *menu bar* beneath the information bar has a *mode selection* drop-down menu, options to exclude quotes, bibliography, small sources, and small matches, as well as options to print and download.

Choose a viewing mode from the mode drop-down menu:

* **Similarity Report** (default) - this mode has a similar layout to the Document Viewer. You will see the document's text on the left of the screen, with similarities highlighted. On the right are the sources, color-coded and listed from highest to lowest percentage of matching words. Only the top or best matches are shown - choose Content Tracking mode to see all underlying matches.
* **Content tracking** mode lists all the matches between the submitted document and the databases. Regular updates means that there may be many matches from the same source, some of which may be partially or completely hidden due to the content appearing in a higher matched source. The sources that are the same will specify from where they were taken and when.
* **Summary report** mode offers a simple, printable list of the matches found followed by the paper with the matching areas highlighted. It shows the sources first, with the document text below.
* **Largest matches** mode shows the percentage of words that are a part of a matching text string (with some limited flexibility). In some cases, strings from the same source may overlap, in which case, the longer string in the largest match view will be displayed.

You have options to filter and exclude:

* Exclude **quoted** or **bibliographic** material - click *Exclude Quotes* or *Exclude Bibliography* from the menu bar.
* Exclude **phrases** - click *enable this setting* for a folder means that any submission made to that folder will exclude the phrases specified in the folder settings. If you would like to include these phrases in the report, click *Do not Exclude Phrases* in the menu bar.
* Exclude **a match** - use this to exclude a source from the Similarity Report in either the Similarity Report or largest matches viewing modes. To exclude a match, view the report in *Similarity Report* or *largest matches* mode. Each source listed has an *X* icon to its right - click this to exclude the source. Any underlying source, if present, will replace the excluded source. Once a source has been excluded it can be re-included in the Similarity Report through the content tracking mode, which lists all sources with content matching that of the submission. In this view mode, excluded sources have a *+* icon to the right of their name - click this to re-include the source in the Similarity Report.
* Exclude **small sources and matches** - click Exclude small sources or Exclude small matches in the menu bar.
* Exclude **small sources** - To exclude a small source, enter a value into the word count or percentage field to set an exclusion threshold. Any source below the word court or match percentage threshold will be excluded from the record. Click Update to save the exclusion setting.
* Exclude **small matches** - To exclude a small match, enter a value into the word count field to set an exclusion threshold. Any match below that threshold will be excluded from the report. Click Update to save the exclusion setting.

Making these changes may change the percentage of matching text found within the submission. Deselect an option to include it again.

Learn more about exclusion settings when [setting up a new folder](/documentation/similarity-check/ithenticate-account-use/folders#00586), [editing filters and exclusions in existing folders](/documentation/similarity-check/ithenticate-account-use/folders#00586), [filters and exclusions within the Similarity Report](/documentation/similarity-check/ithenticate-account-use/similarity-report-use#00604), and [URL filters](/documentation/similarity-check/ithenticate-account-setup/account-info#00572) and [phrase exclusions](/documentation/similarity-check/ithenticate-account-setup/account-info#00573) for account administrators.
