+++
title = "Using your iThenticate account"
date = "2020-05-19"
draft = false
author = "Kathleen Luschek"
type = "documentation"
layout = "documentation_single"
documentation_section = ["similarity-check", "iThenticate-account-use"]
identifier = "documentation/similarity-check/ithenticate-account-use"
rank = 4
weight = 100300
aliases = [
  "/education/similarity-check/ithenticate-account-use",
  "/education/similarity-check/ithenticate-account-use/",
  "/pdfs/similarity-check-user-guide-aug19.pdf"
]
+++

{{< snippet "/_snippet-sources/iThenticateversion.md" >}}

**[v2 using your iThenticate account](https://help.turnitin.com/crossref-similarity-check/user.htm)**

**v1 using your iThenticate account, keep reading:**


Welcome to your Similarity Check user account!

When your organization signs up for Similarity Check, a central contact at your organization will become your Similarity Check account administrator. They will set up all the users on your account.

When your administrator adds you as a user, you’ll receive an email from noreply@ithenticate.com with the subject line “Account Created” containing a username and a single-use password. You may only log in once with the single-use password, and you must change it the first time you log in.

## Log in to your user account (v1)<a id='00580' href='#00580'><i class='fas fa-link'></i></a>

1. Start from the link in the invitation email from noreply@ithenticate.com with the subject line “Account Created” and click *Login*
2. Enter your username and single-use password
3. Click to agree to the terms of the end-user license agreement. These terms govern your personal use of the service. They’re separate from the central Similarity Check service agreement that your organization has agreed to.
4. You will be prompted to choose a new password
5. Click ​*Change Password​* to save.

## Your user account profile (v1)<a id='00581' href='#00581'><i class='fas fa-link'></i></a>

Manage your user account using the *Account Info* tab. Learn more about [your Similarity Check user account](/documentation/similarity-check/ithenticate-account-use/account-info/).

### Reset your password (v1)<a id='00582' href='#00582'><i class='fas fa-link'></i></a>

1. Start from [iThenticate](https://www.ithenticate.com)
2. Click *Login*, then click *Forgot Password*. Enter your email address and click *Submit*
3. You'll receive a password reset link by email. Click the link in the email, choose a new password, and click *Reset Password*.

### Change your email address or password (v1)<a id='00583' href='#00583'><i class='fas fa-link'></i></a>

1. Start from [iThenticate](https://www.ithenticate.com)
2. Enter your username and password
3. Go to *Profile*.
4. To change your email address, remove your current address from the ​*Email​* field, enter your new email in the same field, and click ​*Update Profile​* to save.
5. To change your password, enter a new password in the *Change Password* field, repeat it in the *Confirm Password* field, and click ​*Update Profile*​ to save.

## Find your way around for users (v1)<a id='00584' href='#00584'><i class='fas fa-link'></i></a>

In the main navigation bar at the top of the screen, you will see several tabs:

* [Folders](/documentation/similarity-check/ithenticate-account-use/folders/): this is the main area of iThenticate, where you upload, manage, and view documents for checking
* [Settings](/documentation/similarity-check/ithenticate-account-setup/settings/): configure the iThenticate interface
* [Account Info](/documentation/similarity-check/ithenticate-account-setup/account-info/): manage your account, including your user profile and account usage

<figure><img src='/images/documentation/SC-find-your-way-around.png' alt='Find your way around for users' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image65">Show image</button>
<div id="image65" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">              
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/SC-find-your-way-around.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>
