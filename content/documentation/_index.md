+++
title = "Documentation"
date = "2021-04-21"
draft = false
author = "Amanda Bartell"
image = "/images/banner-images/documentation-pipes.jpg"
maskcolor = "crossref-blue"
type = "documentation"
educationfront = true
aliases =[
    "/education",
    "/education/",
    "/help",
    "/help/",
    "/docs",
    "/docs/",
    "/support",
    "/support/"
    ]
[menu.main]
weight = 3
+++

Take a look at the topics on the right to browse and page through our documentation, or search using the box above the topic list {{< icon name="fa-arrow-right" color="crossref-darkgrey" size="small" >}}

---

## Common questions; how do I...?

#### {{< icon name="fas fa-link" color="crossref-yellow" size="small" >}}&nbsp;[Construct DOI suffixes](/documentation/member-setup/constructing-your-dois)

#### {{< icon name="fas fa-check" color="crossref-blue" size="small" >}}&nbsp;[Verify a metadata registration](/documentation/content-registration/verify-your-registration)

#### {{< icon name="fas fa-pencil" color="crossref-lightgrey" size="small" >}}&nbsp;[Update an existing metadata record](/documentation/register-maintain-records/maintaining-your-metadata/updating-your-metadata/)

#### {{< icon name="fas fa-align-left" color="crossref-darkgrey" size="small" >}}&nbsp;[Interpret and act on reports](/documentation/reports/)

#### {{< icon name="fas fa-exchange" color="crossref-red" size="small" >}}&nbsp;[Query the API to retrieve metadata](/documentation/retrieve-metadata/rest-api/a-non-technical-introduction-to-our-api/)

#### {{< icon name="fas fa-exclamation-circle" color="crossref-green" size="small" >}}&nbsp;[See system status and maintenance](https://status.crossref.org)

<br>

<script src="https://cpl595mrvlzp.statuspage.io/embed/script.js"></script>

---
If you have questions please consult other users on our forum at [community.crossref.org](https://community.crossref.org) or [open a ticket with our technical support team](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001642691) where we'll reply within a few days. Please also visit our [status page](https://status.crossref.org/) to find out about scheduled (and unscheduled) maintenance and subscribe to updates.
