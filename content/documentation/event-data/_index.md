+++
title = "Event Data"
date = "2020-10-06"
draft = false
author = "Martyn Rittman"
type = "documentation"
layout = "documentation_single"
documentation_section = ["event-data"]
aliases = [
	"/education/event-data",
	"/education/event-data/"
]
[menu.documentation]
identifier = "documentation/event-data"
parent = "Documentation"
rank = 4
weight = 550000
+++

{{< snippet "/_snippet-sources/event-data.md" >}}
