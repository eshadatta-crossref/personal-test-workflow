+++
title = "Contributing to Event Data"
date = "2020-10-06"
draft = false
author = "Martyn Rittman"
type = "documentation"
layout = "documentation_single"
documentation_section = ["event-data", "contribute"]
identifier = "documentation/event-data/contribute"
rank = 4
weight = 130300
aliases = [
	"/education/event-data/contribute",
	"/education/event-data/contribute/"
]
+++

There are a number of ways that you can contribute to Event Data. On this page, we show you how to make sure an event is picked up by Event Data, and other ways to get more involved.

## How can I create an event?<a id='00633' href='#00633'><i class='fas fa-link'></i></a>

The easiest way to create an event is to take an action that our agents are tracking. For example, an event will be created if you post a tweet, publish a dataset that is cited by or cites a research article, or add a citation to a Wikipedia article.

Make sure that when you cite a Crossref article you are as specific as possible: including the DOI link, such as `https://doi.org/10.1016/j.jmb.2007.03.025` or, where this isn't possible, a link to the article. An event will not be created if you only use details such as the title and authors without including a link. We attempt to track publisher website URLs but they can change and we may not be successful in creating a link based on the website if there is no DOI.

For data citations to be picked up by Event Data, these citations must be [included in reference lists deposited with Crossref](/documentation/register-maintain-records/maintaining-your-metadata/add-references/), and must be in a structured format. A reference that the publisher formats using the “unstructured reference” type will not lead to an event.

If you run a website or service that generates a large number of events, please [get in touch](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001642691/) about delivering them to us directly. Similarly, if you run a source that we already cover, we would welcome a discussion about the most efficient way to transfer data.

## There is something missing from Crossref Event Data, what can I do?<a id='00155' href='#00155'><i class='fas fa-link'></i></a>

If you see events online that you think should have been included in Event Data, first check our [status page](https://status.crossref.org/) to see whether there are any current issues. If not, create a ticket on our [Gitlab pages](https://gitlab.com/crossref/event_data_enquiries). You can also create a ticket to let us know if you see items missing from our [artifacts](/documentation/event-data/transparency#00629).

If you have a new source of events that you would like to add, please [get in touch](/contact/) to discuss it with us.

## Can I contribute to the code base?<a id='00160' href='#00160'><i class='fas fa-link'></i></a>

Yes, you can! Crossref runs Event Data for the benefit of the scholarly community and we welcome collaborations to develop and improve our code. All of the code is open source and a summary of the elements is available in our [Knowledge Database](https://crossref.gitlab.io/knowledge_base/products/event-data/).

Please [contact us](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001642691/) if you would like to collaborate with us to improve Event Data.
