+++
title = "Missed conflict report"
date = "2020-04-08"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["reports", "missed-conflict-report"]
identifier = "documentation/reports/missed-conflict-report"
rank = 4
weight = 140207
aliases = [
  "/education/metadata-stewardship/reports/missed-conflict-report",
  "/education/metadata-stewardship/reports/missed-conflict-report/",
  "/education/reports/missed-conflict-report/",
  "/education/reports/missed-conflict-report"

]
+++

Learn more about conflicts and the [conflict report](/documentation/reports/conflict-report). Conflicts are usually flagged upon deposit, but sometimes this doesn't happen, creating a *missed conflict*.

A missed conflict may occur for several reasons:

* Two DOIs are deposited for the same item, but the metadata is slightly different (DOI A deposited with an online publication date of 2011, DOI B deposited with a print publication date of 1972)
* DOIs were deposited with a unique item number. Before 2008, DOIs containing unique item numbers (supplied in the *<publisher_item>* element) were not checked for conflicts.

The missed conflict report compares article titles across data for a specified journal or journals. To retrieve a missed conflict report for a title:

1. Start from the [browsable title list](https://www.crossref.org/titleList/) and search or browse for the title
2. Click the <img src="/images/documentation/Icon-blue-chevrons-right.png" alt="Blue chevrons right icon" height="23" > icon at the far right of the title
3. The missed conflict interface will pop up in a second window. Enter your email address in the appropriate field. Multiple title IDs can be included in a single request if needed

A report will be emailed to the email address you provided. This report lists all DOIs with identical article titles that have not been flagged as conflicts.
