+++
title = "Reports"
date = "2021-10-17"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["reports"]
aliases = [
    "/education/metadata-stewardship/reports",
    "/education/metadata-stewardship/reports/",
    "/education/reports",
    "/education/reports/",
    "/reports",
    "/reports/",
    "/education/metadata-stewardship/reports/reports-available-on-our-website",
    "/education/metadata-stewardship/reports/reports-available-on-our-website/",
    "/education/metadata-stewardship/reports/reports-by-email",
    "/education/metadata-stewardship/reports/reports-by-email/",
    "/faqs/reports/",
    "/faqs/reports"
]
[menu.main]
parent = "Documentation"
weight = 200000
[menu.documentation]
identifier = "documentation/reports"
parent = "Documentation"
rank = 5
weight = 170000

+++

Our reports are tools to help you evaluate and improve your metadata. The [dashboard](/dashboard/) gives an overview of our ever-growing corpus of metadata. How good is your metadata? Find out using [Participation Reports](/documentation/reports/participation-reports) and other reports to evaluate your metadata records, check for any issues, and learn how to resolve them.  <br/>


Some reports are [sent to you by email](#00612), and some are [available on our website](#00613).

## Reports by email<a id='00612' href='#00612'><i class='fas fa-link'></i></a>

We send reports by email from reports@crossref.org to [specific contacts on your account](/documentation/member-setup/). Do add this address to your email contacts list or safe senders list to ensure that you receive them. These reports are intended to help you keep your metadata records up-to-date, and include:

* [Conflict report](/documentation/reports/conflict-report) - this report shows where two (or more) DOIs have been submitted with the same metadata, indicating that you may have duplicate DOIs. You’ll start receiving conflict reports if you have at least one conflict. These reports are sent out on a monthly basis, or more frequently if your number of conflicts peaks by over 500, and it is sent to the main Technical contact on your account.
* [DOI error report](/documentation/reports/doi-error-report) - a DOI error report is sent immediately when a user informs us that they’ve seen a DOI somewhere which doesn't resolve to a website, and it is sent to the main Technical contact on your account.
* [Resolution report](/documentation/reports/resolution-report) - this monthly report shows the number of successful and failed DOI resolutions for the previous month, and it is sent to the Primary contact on your account (please note - this contact used to be known as the Business contact).
* [Schematron report](/documentation/reports/schematron-report) - the main Technical contact on your account may also receive periodic Schematron reports if there’s a metadata quality issue with your records.

If you aren’t receiving reports, please check the emails aren’t being caught by your spam filter. It might also be because we our contact information for your organization is not current, or you aren’t the designated reports person in our database. Please [contact us](/contact/) and we’ll sort it out for you.

If you are not the appropriate person to receive reports, we can send reports to a different email address. If you don’t find our reports useful, please [contact us](/contact/) and we’ll see what we can do.

If you need information about something that’s not covered by your reports, please explore all the information you can access through our [REST API](/documentation/retrieve-metadata/rest-api), or [contact us](/contact/) for help.

## Reports available on our website<a id='00613' href='#00613'><i class='fas fa-link'></i></a>

These reports are available on our website:

* [Browsable title list](/documentation/reports/browsable-title-list)
* [Conflict report](/documentation/reports/conflict-report)
* [Depositor report](/documentation/reports/depositor-report)
* [DOI crawler report](/documentation/reports/doi-crawler-report)
* [Field or missing metadata report](/documentation/reports/field-or-missing-metadata-report)
* [Missed conflict report](/documentation/reports/missed-conflict-report)
* [Schematron report](/documentation/reports/schematron-report)
* [XML journal list](/documentation/reports/xml-journal-list)
