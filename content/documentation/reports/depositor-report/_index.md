+++
title = "Depositor report"
date = "2020-04-08"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["reports", "depositor-report"]
identifier = "documentation/reports/depositor-report"
rank = 4
weight = 140203
aliases = [
  "/education/metadata-stewardship/reports/depositor-report",
  "/education/metadata-stewardship/reports/depositor-report/",
  "/education/reports/depositor-report/",
  "/education/reports/depositor-report"
]
+++

The depositor report is used for checking basic info about your DOI registrations.

Depositor reports list all DOIs by member and title for [journals](https://www.crossref.org/06members/51depositor.html), [books](http://www.crossref.org/06members/51depositorB.html), and [conference proceedings](http://www.crossref.org/06members/51depositorCP.html). We currently have depositor reports for journals, books, and conference proceedings (but not for other content types). The index page is updated weekly. Title-level reports are updated as your metadata is updated with us.

Each title-level report lists all DOIs registered for the title as well as (for each DOI) the owning prefix, the deposit timestamp, the date the record was last updated, and the number of Cited-by matches. To view each title-level report, select the member name then the appropriate title.

* **Field/missing metadata report**: You can also see what basic bibliographic metadata fields are populated for your journal articles - click on the green triangle to the right of each member name to view a field / missing metadata report.
* **DOI crawler**: We crawl a broad sample of journal DOIs to make sure the DOIs are resolving to the appropriate page. For each journal crawled, a sample of DOIs that equals 5% of the total DOIs for the journal up to a maximum of 50 DOIs is selected. You can access the crawler details for a given journal by selecting the linked date in the ‘last crawl date’ column.

Click on a member name in the report, and you will see a list of that member’s titles below the name. Click on any publication title to open a text file which list all DOIs for that title.

The initial view shows:

* Name: name of the member. Members with more than one prefix will appear multiple times
* Journal/Book/Conf Proc count: number of journal, book, or conference proceeding titles associated with the member
* Total DOIs: total number of DOIs deposited for the selected title
* [Field report](/documentation/reports/field-or-missing-metadata-report): shows missing metadata fields for each member, select the <img src="/images/documentation/Icon-green-arrow-right.png" alt="Green arrow right icon" height="23" > icon to view

The expanded view shows:

* Name of each journal, book, or conference proceeding with DOI names deposited by the member
* DOIs: Total number of DOIs registered for each journal, book, or conference proceeding deposited by the member
* Last crawl date: date of last [crawler report](/documentation/reports/doi-crawler-report) (if available)

<figure><img src='/images/documentation/Depositor-report-expanded-view.png' alt='Depositor report expanded view' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image9">Show image</button>
<div id="image9" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Depositor-report-expanded-view.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>


## Depositor report title view<a id='00258' href='#00258'><i class='fas fa-link'></i></a>

Select a journal, book, or conference proceeding title to retrieve a list of DOIs for the title (DOI), the owner prefix of the DOI (OWNER), the timestamp value for the DOI (DEPOSIT-TIMESTAMP) the date the DOI was last updated (LAST-UPDATED), and the number of Cited-by matches for the DOI:

<figure><img src='/images/documentation/Depositor-report-title-view.png' alt='Depositor report title view' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image10">Show image</button>
<div id="image10" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/Depositor-report-title-view.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>


Title-level depositor report data may also be retrieved using `format=doilist` - learn more about [retrieving DOIs by title](/documentation/retrieve-metadata/xml-api/retrieving-dois-by-title).
