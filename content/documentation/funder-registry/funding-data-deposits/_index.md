+++
title = "Funding data deposits"
date = "2020-04-08"
draft = false
author = "Rachael Lammey"
type = "documentation"
layout = "documentation_single"
documentation_section = ["funder-registry", "funding-data-deposits"]
identifier = "documentation/funder-registry/funding-data-deposits"
rank = 4
weight = 90300
aliases = [
  "/education/funder-registry/funding-data-deposits",
  "/education/funder-registry/funding-data-deposits/"
]
+++

Funding metadata can be deposited with Crossref in two ways:

1. In a stand-alone deposit where just the funding metadata is provided.
2. As part of the full set of metadata for an article.

When funding data is successfully deposited, an inserted identifier will appear as a message (`<msg>`) within the submission log:

```
 <record_diagnostic status="Success">
      <doi>10.1016/j.apcatb.2018.04.081</doi>
      <msg>Inserted identifier: 501100001809 for name: "the National Natural Science Foundation of China"
</msg>
   </record_diagnostic>
```

Learn more about the detailed [rules for depositing funding metadata](/documentation/funder-registry/funding-data-overview#00287).

## Stand-alone deposit<a id='00298' href='#00298'><i class='fas fa-link'></i></a>

Stand-alone deposits are intended as a convenience for depositing funding data to existing DOIs without having to repeat the existing metadata. The deposit XML file contains just the DOI of the article and the specific FundRef data. Please note the following:

* If the DOI currently has any funding data it will be replaced by the stand-alone deposited data.
* If the DOI currently has any Crossmark data, the stand-alone deposited funding data will be inserted within the existing (previously deposited) Crossmark data.
* When uploading stand-alone [XML deposits to Crossref via HTTPS POST](/documentation/content-registration/verify-your-registration#00527), the operation must be *doDOICitUpload*.
* When uploading using the [admin tool](https://doi.crossref.org), the file type must be *DOI References/Resources*.

Review this sample or [download an XML file](https://gitlab.com/crossref/schema/raw/master/examples/resource_funding_4.4.2.xml).

```
<?xml version="1.0" encoding="UTF-8"?>
<doi_batch version="4.4.2" xmlns="http://www.crossref.org/doi_resources_schema/4.4.2"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.crossref.org/doi_resources_schema/4.4.2 doi_resources4.4.2.xsd">
    <head>
        <doi_batch_id>arg_123_954</doi_batch_id>
        <depositor>
            <depositor_name>Crossref</depositor_name>
            <email_address>pfeeney@crossref.org</email_address>
        </depositor>
    </head>
    <body>
        <fundref_data>
            <doi>10.32013/12345678</doi>
            <program xmlns="http://www.crossref.org/fundref.xsd">
                <assertion name="fundgroup">
                    <assertion name="funder_name"> National Science Foundation
                        <assertion name="funder_identifier">http://dx.doi.org/10.13039/100000001</assertion>
                    </assertion>
                    <assertion name="award_number">CHE-1152342</assertion>
                </assertion>
            </program>
        </fundref_data>
        <fundref_data>
            <doi>10.32013/879fk3</doi>
            <program xmlns="http://www.crossref.org/fundref.xsd">
                <assertion name="fundgroup">
                    <assertion name="funder_name">National Science Foundation
                        <assertion name="funder_identifier">http://dx.doi.org/10.13039/100000001</assertion>
                    </assertion>
                    <assertion name="award_number">CHE-1152342</assertion>
                </assertion>
            </program>
        </fundref_data>
    </body>
</doi_batch>
```

## Full metadata deposit<a id='00299' href='#00299'><i class='fas fa-link'></i></a>

Funding data may be deposited as part of a normal 'full' metadata XML deposit for a DOI.

```
<?xml version="1.0" encoding="UTF-8"?>
  <doi_batch version="4.4.0" xmlns="http://www.crossref.org/schema/4.4.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.crossref.org/schema/4.4.0 http://www.crossref.org/schema/deposit/crossref4.4.0.xsd" xmlns:fr="http://www.crossref.org/fundref.xsd">
  <head>
	<doi_batch_id>7ce0adc7155c63a5e2b-3ebc</doi_batch_id>
	<timestamp>201610241300</timestamp>
	<depositor>
 	<depositor_name>Crossref Support</depositor_name>
 	<email_address>support@crossref.org</email_address>
	</depositor>
	<registrant>Crossref</registrant>
   </head>
  <body>
   <journal>
	<journal_metadata language="en">
  	<full_title>Applied Physics Letters</full_title>
  	<abbrev_title>Appl. Phys. Lett.</abbrev_title>
  	<issn media_type="print">00036951</issn>
  	<coden>APPLAB</coden>
	</journal_metadata>
	<journal_issue>
  	<publication_date media_type="print">
   	<month>09</month>
   	<day>10</day>
   	<year>2012</year>
  	</publication_date>
  	<journal_volume>
   	<volume>101</volume>
  	</journal_volume>
  	<issue>11</issue>
	</journal_issue>
   <journal_article publication_type="full_text">
	<titles>
  	<title>Total energy loss to fast ablator-ions and target capacitance of direct-drive implosions on OMEGA</title>
  	</titles>
  	<contributors>
  	<person_name sequence="first" contributor_role="author">
    	<given_name>N.</given_name>
    	<surname>Sinenian</surname>
  	</person_name>
  	<person_name sequence="additional" contributor_role="author">
    	<given_name>A. B.</given_name>
    	<surname>Zylstra</surname>
  	</person_name>
  	<person_name sequence="additional" contributor_role="author">
    	<given_name>M. J.-E.</given_name>
    	<surname>Manuel</surname>
  	</person_name>
  	<person_name sequence="additional" contributor_role="author">
    	<given_name>H. G.</given_name>
    	<surname>Rinderknecht</surname>
  	</person_name>
  	<person_name sequence="additional" contributor_role="author">
    	<given_name>J. A.</given_name>
    	<surname>Frenje</surname>
  	</person_name>
  	<person_name sequence="additional" contributor_role="author">
    	<given_name>F. H.</given_name>
    	<surname>Séguin</surname>
  	</person_name>
  	<person_name sequence="additional" contributor_role="author">
    	<given_name>C. K.</given_name>
    	<surname>Li</surname>
  	</person_name>
  	<person_name sequence="additional" contributor_role="author">
    	<given_name>R. D.</given_name>
    	<surname>Petrasso</surname>
  	</person_name>
  	<person_name sequence="additional" contributor_role="author">
    	<given_name>V.</given_name>
    	<surname>Goncharov</surname>
  	</person_name>
  	<person_name sequence="additional" contributor_role="author">
    	<given_name>J.</given_name>
    	<surname>Delettrez</surname>
  	</person_name>
  	<person_name sequence="additional" contributor_role="author">
    	<given_name>I. V.</given_name>
    	<surname>Igumenshchev</surname>
  	</person_name>
  	<person_name sequence="additional" contributor_role="author">
    	<given_name>D. H.</given_name>
    	<surname>Froula</surname>
  	</person_name>
  	<person_name sequence="additional" contributor_role="author">
    	<given_name>C.</given_name>
    	<surname>Stoeckl</surname>
  	</person_name>
  	<person_name sequence="additional" contributor_role="author">
    	<given_name>T. C.</given_name>
    	<surname>Sangster</surname>
  	</person_name>
  	<person_name sequence="additional" contributor_role="author">
    	<given_name>D. D.</given_name>
    	<surname>Meyerhofer</surname>
  	</person_name>
  	<person_name sequence="additional" contributor_role="author">
    	<given_name>J. A.</given_name>
    	<surname>Cobble</surname>
  	</person_name>
  	<person_name sequence="additional" contributor_role="author">
    	<given_name>D. G.</given_name>
    	<surname>Hicks</surname>
  	</person_name>
 	</contributors>
 	<publication_date media_type="online">
  	<month>09</month>
  	<day>10</day>
  	<year>2012</year>
  	</publication_date>
 	<pages>
  	<first_page>114102</first_page>
 	</pages>
 	<crossmark>
  	<crossmark_version>1</crossmark_version>
  	<crossmark_policy>10.1063/aip-crossmark-policy-page</crossmark_policy>
  	<crossmark_domains>
    	<crossmark_domain>
     	<domain>aip.org</domain>
    	</crossmark_domain>
  	</crossmark_domains>
  	<crossmark_domain_exclusive>true</crossmark_domain_exclusive>
  	<custom_metadata>
    	<assertion name="received" label="Received" group_name="publication_history" group_label="Publication History" order="0">2012-07-31</assertion>
    	<assertion name="accepted" label="Accepted" group_name="publication_history" group_label="Publication History" order="1">2012-08-28</assertion>
    	<assertion name="published" label="Published" group_name="publication_history" group_label="Publication History" order="2">2012-09-10</assertion>
    	<fr:program name="fundref">
     	<fr:assertion name="fundgroup">
      	<fr:assertion name="funder_name">
       	U.S. Department of Energy
       	<fr:assertion name="funder_identifier">http://dx.doi.org/10.13039/100000015</fr:assertion>
      	</fr:assertion>
     	<fr:assertion name="award_number">DE-FG03-03SF22691</fr:assertion>
     	<fr:assertion name="award_number">DE-AC52-06NA27279</fr:assertion>
    	</fr:assertion>
  	</fr:program>
  	</custom_metadata>
	</crossmark>
  <doi_data>
 	<doi>10.1063/1.4752012</doi>
 	<timestamp>20130806074500</timestamp>
 	<resource>
http://scitation.aip.org/content/aip/journal/apl/101/11/10.1063/1.4752012
 	</resource>
   </doi_data>
  </journal_article>
 </journal>
</body>
</doi_batch>
```
