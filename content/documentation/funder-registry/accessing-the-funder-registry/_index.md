+++
title = "Accessing the Funder Registry"
date = "2021-04-09"
draft = false
author = "Rachael Lammey"
type = "documentation"
layout = "documentation_single"
documentation_section = ["funder-registry", "accessing-the-funder-registry"]
identifier = "documentation/funder-registry/accessing-the-funder-registry"
rank = 4
weight = 90100
aliases = [
	"/education/funder-registry/accessing-the-funder-registry",
	"/education/funder-registry/accessing-the-funder-registry/"
]
+++

The list is available to download as an RDF file from the [Open Funder Registry GitLab repository](https://gitlab.com/crossref/open_funder_registry), and is freely available under a [CC0 license](https://creativecommons.org/publicdomain/zero/1.0/). The RDF file provides the funder preferred name, alternate name(s), country, type (government, private), subtype, and any relationship to another entity. Private funding subtypes include: academic, corporate, foundation, international, other non-profit (private), professional associations and societies. Government funding subtypes include: federal (national government), government non-federal (state/provincial government). You can therefore easily build funding metadata into your own tools such as manuscript tracking systems, or analytics services.

You may also [download a .csv file](https://doi.crossref.org/funderNames?mode=list) of the funder names and identifiers in the Open Funder Registry, and [download a list of funders in JSON format](https://api.crossref.org/funders).

## Funder Registry updates<a id='00285' href='#00285'><i class='fas fa-link'></i></a>

The current and previous versions of the Registry are available from the [Open Funder Registry GitLab repository](https://gitlab.com/crossref/open_funder_registry). The registry is usually updated monthly.

You can request to have a missing funder added to the Funder Registry by using our [contact form](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001642691). Please include the name of the funder, its website address, country, and if possible any DOIs of research articles that already acknowledge this funder.
