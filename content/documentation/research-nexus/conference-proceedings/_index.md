+++
title = "Introduction to conference proceedings"
date = "2022-05-06"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["content-registration", "content-types-intro", "conference-proceedings"]
identifier = "documentation/content-registration/content-types-intro/conference-proceedings"
rank = 4
weight = 60502
aliases = [
    "/education/content-registration/recommendations-by-content-type/conference-proceedings",
    "/education/content-registration/recommendations-by-content-type/conference-proceedings/"
]
+++

### Why register conference proceedings
Conference proceedings are often one of the first ways that researchers communicate new, innovative, and emergent research to their peers and the scholarly community. While this content type can be a precursor to a more formal peer-reviewed journal article, conference proceedings are critical to communicating new concepts that further enrich the [research nexus](/documentation/research-nexus/).


### Obligations and limitations<a id='00080' href='#00080'><i class='fas fa-link'></i></a>
1. Follow the conference proceedings [metadata best practices.](/documentation/principles-practices/conference-proceedings)
2. The conference proceedings content type captures metadata about a single conference, such as date, acronym, and location. DOIs should be assigned to all papers associated with the conference, and a DOI may be assigned to the conference itself. Ongoing conferences published with an ISSN may be deposited as a series.
3. You can register conference proceedings using our [web deposit form](/documentation/content-registration/web-deposit-form/) or via [direct deposit of XML](/documentation/content-registration/direct-deposit-xml/).

### Fees<a id='00081' href='#00081'><i class='fas fa-link'></i></a>

In addition to annual membership dues, all records attract a one-time registration fee. [Read about the fees](/fees/#content-registration-fees).

### History
We've been supporting conference proceedings since 2003.

### Key links
* Conference proceedings [metadata best practice](/documentation/principles-practices/conference-proceedings)
* How to register content via [direct deposit of XML](/documentation/member-setup/direct-deposit-xml/) or the [web deposit form](/documentation/content-registration/web-deposit-form/).
* [Conference proceeding markup guide](/documentation/schema-library/markup-guide-record-types/conference-proceedings/)
