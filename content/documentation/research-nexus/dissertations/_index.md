+++
title = "Introduction to dissertations"
date = "2022-05-31"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["content-registration", "content-types-intro", "dissertations"]
identifier = "documentation/content-registration/content-types-intro/dissertations"
rank = 4
weight = 60504
+++

### Why register dissertations
Like conference proceedings, dissertations are excellent sources of emergent research findings and innovations in the scholarly community. Dissertations are often robust works that serve to expand the [research nexus](/documentation/research-nexus/).

### Obligations and limitations<a id='00080' href='#00080'><i class='fas fa-link'></i></a>
1. Follow the dissertations [metadata best practices.](/documentation/principles-practices/dissertations).
2. You can register dissertations using our [web deposit form](/documentation/content-registration/web-deposit-form/) or via [direct deposit of XML](/documentation/content-registration/direct-deposit-xml/).


### Fees<a id='00081' href='#00081'><i class='fas fa-link'></i></a>
In addition to annual membership dues, all records attract a one-time registration fee. [Read about the fees](/fees/#content-registration-fees).

### History
Dissertations have been supported since 2005.

### Key links
* [Dissertations content metadata best practice](/documentation/principles-practices/dissertations)
* How to register content using [direct deposit of XML](/documentation/content-registration/direct-deposit-xml/)
* [Dissertation markup guide](/documentation/schema-library/markup-guide-record-types/dissertations/)
