+++
title = "Introduction to reports and working papers"
date = "2022-04-11"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["content-registration", "content-types-intro", "reports-and-working-papers"]
identifier = "documentation/content-registration/content-types-intro/reports-and-working-papers"
rank = 5
weight = 60511
+++

### Why register reports and working papers

Researchers communicate with each other through increasing diverse channels, and our community sees more and more citations and links to reports and working papers such as white papers as an important part of the scholarly record. When these records are registered with Crossref, research can be traced from origin to practical implementation.

### Obligations and limitations

1. Follow the reports and working papers [metadata best practices.](/documentation/principles-practices/reports)
2. Technical reports and working papers are typically assigned a single identifier, but identifiers may also be assigned to sub-sections of the report (such as chapters) as needed using the `content_item` element.

### Fees
In additional to annual membership dues, all records attract a one-time registration fee. Fees for reports and working papers are different depending on whether the content registered is current or backfile. [Read about the fees.](/fees/#content-registration-fees)

### History

We've been supporting reports and working papers since 2005.

### Key links
* [Reports and working papers metadata best practices](/documentation/principles-practices/reports)
* How to register content using [direct deposit of XML](/documentation/member-setup/direct-deposit-xml/)
* [Reports and working paper markup guide](/documentation/schema-library/markup-guide-record-types/reports-and-working-papers/)
