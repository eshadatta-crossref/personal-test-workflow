+++
title = "Introduction to peer reviews"
date = "2022-02-04"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["research-nexus", "peer-reviews"]
identifier = "documentation/content-types-intro/peer-reviews"
rank = 4
weight = 60507
aliases = [
    "/education/content-registration/content-types-intro/peer-reviews",
    "/education/content-registration/content-types-intro/peer-reviews/",
    "/services/content-registration/peer-reviews/",
    "/services/content-registration/peer-reviews",
    "/documentation/content-registration/content-types-intro/peer-reviews/",
    "/documentation/content-registration/content-types-intro/peer-reviews/",
    "/services/content-registration/peer-reviews",
    "/services/content-registration/peer-reviews/"
]
+++

### Why register peer reviews

More of our members are keen to expose evidence of the integrity of the editorial process, such as peer review. Registering peer reviews means:

* The metadata can provide relevant information about the reviews such as whether they were part of peer review or post-publication.
* There is evidence of the contribution from reviewers.
* These reviews are connected to the original work and other related objects.
* Links to these documents persist over time for future generations.

This metadata may also support enrichment of scholarly discussion, reviewer accountability, publishing transparency, and analysis or research on peer reviews.

### Obligations and limitations <a id='00073' href='#00073'><i class='fas fa-link'></i></a>

1. Members need to follow the peer review [metadata best practices](/documentation/principles-practices/peer-review).
2. All peer reviews must include relationship metadata linking the review with the item being reviewed.
3. The item being reviewed must have a Crossref DOI.
4. You can't add components to peer review records.
5. Crossmark is not currently supported for peer review records.
6. You can only register peer reviews by direct deposit of XML, our helper tools do not currently support this content type.

### Fees<a id='00074' href='#00074'><i class='fas fa-link'></i></a>

In addition to annual membership dues, all records attract a one-time registration fee. For peer reviews, the fees are different if they are reviews of your own records, or for another member's records. [Read about the fees](/fees/#volume-discounts-peer-reviews).

### History

Our members asked for the flexibility to register content for the reviews and discussions of scholarly content which they publish, so we've extended our infrastructure to support members who post them. We support a whole host of outputs made publicly available from the peer review history, as they vary greatly based on journal. This may include referee reports, decision letter, and author response. The overall set may include outputs from the initial submission only or those from all subsequent rounds of revisions. We also allow members to register content made up of discussions surrounding a journal article after it was published (e.g. post-publication reviews).

The following organizations consulted with us on the design and/or development of the peer review service:

- Publons
- PeerJ
- F1000 Research
- eLife
- BioMedCentral
- BMJ
- Copernicus
- EMBO
- Nature Communications

### Key links
- [Peer review metadata best practices](/documentation/principles-practices/peer-review)
- [Direct deposit of XML](/documentation/member-setup/direct-deposit-xml/)
- [Peer Review markup guide](/documentation/schema-library/markup-guide-record-types/peer-reviews/)

For full instructions and XML examples please visit our [support article](/education/content-registration/content-types-intro/peer-reviews/) where you can also raise a ticket for any questions.

If you have questions please consult other users on our forum at [community.crossref.org](https://community.crossref.org/) or [open a ticket with our technical support team](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001642691) where we’ll reply within a few days.
