+++
title = "Introduction to journals and articles"
date = "2022-04-01"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["content-registration", "content-types-intro", "journals-and-articles"]
identifier = "documentation/content-registration/content-types-intro/journals-and-articles"
rank = 4
weight = 60506
+++

### Why register journals and articles
Journals and articles remain the starting point for researchers exploring previous research, and provide a great way to discover links and relationships with other parts of the [research nexus](/documentation/research-nexus/). Registering your journals and articles with us ensures that metadata about them are shared across the scholarly ecosystem with library discovery systems, scholarly sharing networks, specialist databases, metrics and analytics tools and much more.


### Obligations and limitations

1. Follow the journals [metadata best practices.](/documentation/principles-practices/journals)
2. You can register journals and articles using all our content registration tools.

### Fees

In addition to annual membership dues, most records attract a one-time registration fee. Journal title records are free of charge, but there are fees for each journal articles registered. These fees are different depending on whether the article registered is current or backfile. [Read about the fees.](/fees/#content-registration-fees)

### History

Crossref has supported journal and journal article registration since we started registering content in 2000.

### Key links

* [Journals metadata best practices](/documentation/principles-practices/journals)
* How to register content - using [Crossref XML plugin for OJS](/documentation/content-registration/ojs-plugin/), our [web deposit form](/documentation/content-registration/web-deposit-form/) or [direct deposit or XML](/documentation/content-registration/direct-deposit-xml/).
* [Journals and articles markup guide](/documentation/schema-library/markup-guide-record-types/journals-and-articles/)
