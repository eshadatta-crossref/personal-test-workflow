+++
title = "Introduction to books and chapters"
date = "2022-05-31"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["content-registration", "content-types-intro", "books-and-chapters"]
identifier = "documentation/content-registration/content-types-intro/books-and-chapters"
rank = 4
weight = 60501
aliases = [
    "/documentation/content-registration/structural-metadata/structures-book",
    "/documentation/content-registration/structural-metadata/structures-book/"
]
+++

### Why register books and chapters
Registration of books and chapters maximizes reference linking between books and other content types including journals and conference proceedings, and ensures that we collect and distribute persistent identifiers and authoritative metadata for online books. There are seven benefits for our members to register book- and chapter-level metadata:

1. increased discoverability
2. increased usage
3. matching author expectations
4. author exposure
5. usage and citations reporting
6. supporting authors with funding compliance and reporting
7. understanding the hot topics within your books

You can read about each of these in more detail on [our blog](/blog/crossing-the-rubicon-the-case-for-making-chapters-visible/).

### Obligations and limitations<a id='00080' href='#00080'><i class='fas fa-link'></i></a>
1. Follow the books [metadata best practices.](/documentation/principles-practices/books-and-chapters)
2. You can register books and chapters using our [web deposit form](/documentation/content-registration/web-deposit-form/) or via [direct deposit of XML](/documentation/content-registration/direct-deposit-xml/).

### Fees<a id='00081' href='#00081'><i class='fas fa-link'></i></a>

In addition to annual membership dues, all records attract a one-time registration fee. There are volume discounts available for registration of book chapters and reference entries for a single title. [Read about the fees.](/fees/#volume-discounts-book-chapters-and-reference-entries-for-a-single-title)

### History

Books have been supported since 2003. The [Books Interest Group](/working-groups/books/) provides guidance and advice for developing the books metadata we collect.

### Key links
* [Books and chapters content metadata best practice](/documentation/principles-practices/books-and-chapters)
* How to register content via [direct deposit of XML](/documentation/member-setup/direct-deposit-xml/) or the [web deposit form](/documentation/content-registration/web-deposit-form/).
* [Books and chapters markup guide](/documentation/schema-library/markup-guide-record-types/books-and-chapters/)
