+++
title = "Introduction to datasets"
date = "2022-05-31"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["content-registration", "content-types-intro", "datasets"]
identifier = "documentation/content-registration/content-types-intro/datasets"
rank = 4
weight = 60503
aliases = [
    "/education/content-registration/recommendations-by-content-type/datasets/",
    "/education/content-registration/recommendations-by-content-type/datasets"
]
+++

### Why register datasets
Datasets are important research outputs themselves and increasingly seen as a first-class object Registration helps ensure reproducibility and further solidifies the [research nexus](/documentation/research-nexus/).

### Obligations and limitations<a id='00080' href='#00080'><i class='fas fa-link'></i></a>
1. Follow the datasets [metadata best practices.](/documentation/principles-practices/datasets)
2. You can register datasets via [direct deposit of XML](https://www.crossref.org/documentation/member-setup/direct-deposit-xml/).

### Fees<a id='00081' href='#00081'><i class='fas fa-link'></i></a>

In addition to annual membership dues, all records attract a one-time registration fee. [Read about the fees for registering datasets](/fees/#content-registration-fees).

### History
Datasets have been supported since 2006.

### Key links
* [Datasets content metadata best practice](/documentation/principles-practices/datasets)
* How to register content via [direct deposit of XML](/documentation/member-setup/direct-deposit-xml/)
* [Dataset markup guide](/documentation/schema-library/markup-guide-record-types/datasets/)
