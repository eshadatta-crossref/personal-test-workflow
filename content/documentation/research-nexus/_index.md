+++
title = "The research nexus"
date = "2021-12-11"
draft = false
author = "Ginny Hendricks"
type = "documentation"
layout = "documentation_single"
documentation_section = ["research-nexus"]
identifier = "documentation/research-nexus"
rank = 4
weight = 60512
aliases = [
    "/services/content-registration/content-types",
    "/02publishers/dois_for_books.html",
    "/education/content-registration/content-types-intro",
    "/education/content-registration/content-types-intro/",
    "/documentation/content-registration/content-types-intro",
    "/documentation/content-registration/content-types-intro/",
    "/documentation/metadata",
    "/documentation/metadata/",
    "/documentation/metadata/#00232",
    "/documentation/metadata/#00232/",
    "/education/content-registration/structural-metadata/relationships",
    "/education/content-registration/structural-metadata/relationships/",
    "/documentation/content-registration/structural-metadata/relationships/",
    "/documentation/content-registration/structural-metadata/relationships"
]
[menu.main]
parent = "Documentation"
identifier = "documentation/research-nexus"
weight = 100005
[menu.documentation]
parent = "Documentation"
identifier = "documentation/research-nexus"
weight = 100005
rank = 4
+++

The 'research nexus' is the vision to which we aspire:

> A rich and reusable open network of relationships connecting research organizations, people, things, and actions; a scholarly record that the global community can build on forever, for the benefit of society.

The research nexus goes beyond the basic idea of just having persistent identifiers for content. Objects and entities such as journal articles, book chapters, grants, preprints, data, software, statements, dissertations, protocols, affiliations, contributors, etc. should all be identified and that is still an important part of the picture. But what is most important is how they relate to each other and the context in which they make up the whole research ecosystem.

The foundation of the research nexus is metadata; the richer and more comprehensive the metadata in Crossref records, the more value there is for our members and for others, including for future generations.

{{< figure src="/images/documentation/research-nexus-wb-2021.png" caption="Crossref Research Nexus Vision" width="80%">}}

Metadata and relationships between research objects and entities can support the whole scholarly research ecosystem in many ways, including:

- **Research integrity**: helping to provide signals about the trustworthiness of the work including provenance information such as who funded it (when and for how much), which organizations and people contributed what, whether something was updated or corrected, and whether it was checked for originality. All of these signals can be expressed through Crossref metadata.

- **Reproducibility**: helping others to reproduce outcomes by adding relationships between literature, data, software, protocols and methods, and more. All of these relationships can be asserted through members' ongoing stewardship of their Crossref metadata records.

- **Reporting and assessment**: helping organizations such as universities, funders, governments, to track and demonstrate the outcomes of investment; provide benchmarking information; show compliance with funder mandates; and decide what other research to fund. This kind of information can be included in Crossref metadata.

- **Discoverability**: helping people and systems identify work through multiple angles. Registering content with Crossref makes it possible for work to be found and used. Thousands of systems use Crossref metadata, therefore the richer the records are, the more visibility there is likely to be of your work. Including metadata like abstracts and references are very simple ways to increase the visibility of your records.

### The importance of relationships

A big part of the research nexus is establishing connections between and among different research objects which establishes provenance over time. Adding relationships to your metadata records can convey much richer and more nuanced connections beyond traditional references.

These relationships may consist of versions, corrections, translations, data, formats, supplements, and components. There are no extra fees for including relationships in your metadata.

Currently relationships can only be established by [direct deposit of XML](/documentation/content-registration/direct-deposit-xml/). Read our [relationships metadata best practice](/documentation/principles-practices/best-practices/relationships) page and our [relationships markup guide](/documentation/schema-library/markup-guide-metadata-segments/relationships/).

{{< snippet "/_snippet-sources/content-types.md" >}}
