+++
title = "Linked clinical trials"
date = "2020-04-08"
draft = false
author = "Martyn Rittman"
type = "documentation"
layout = "documentation_single"
documentation_section = ["crossmark", "linked-clinical-trials"]
identifier = "documentation/crossmark/linked-clinical-trials"
rank = 4
weight = 110500
aliases = [
	"/education/crossmark/linked-clinical-trials",
	"/education/crossmark/linked-clinical-trials/"
]
+++

Crossmark can be used to display the relationships between different publications that report on a common clinical trial. This section describes the steps a member needs to take to participate in this initiative. Learn more about [the background to the project](/blog/linking-clinical-trials-enriched-metadata-and-increased-transparency/).

Clinical trial numbers should be extracted from the paper by the publisher or supplied by the author on submission.

There are three elements to the metadata that members need to deposit to participate in linked clinical trials:

1. The registry in which the clinical trial has been registered (required)
Clinical trials should be registered with one of the WHO-approved national trial registries or with [ClinicalTrials.gov](https://clinicaltrials.gov/). Crossref maintains a list of these [approved registries](https://doi.org/10.18810/registries), and has assigned a DOI to each one. This ID should be deposited. The registry ID is used in combination with the trial number to identify trials correctly.
2. The registered clinical trial number (required)
The trial number, including its prefix, for example, ISRCTN00757750
3. The relationship of the publication to the clinical trial (optional)
This field is optional but encouraged. The three allowed elements are "pre-results", "results" and "post-results", indicating which stage of the trial the publication is reporting on.

These fields should be included within the custom metadata section of the Crossmark deposit. When clinical trial metadata is deposited, the *Clinical Trials* section of the Crossmark box will automatically appear and be populated.

## Example deposit for linked clinical trials<a id='00523' href='#00523'><i class='fas fa-link'></i></a>

```
<clinicaltrial_data>
<doi>10.5555/12345678</doi>
<ct:program>
<ct:clinical-trial-number registry="10.18810/isrctn" type="results">ISRCTN1234</ct:clinical-trial-number>
<ct:clinical-trial-number registry="10.18810/isrctn" type="results">ISRCTN9999</ct:clinical-trial-number>
</ct:program>
</clinicaltrial_data>
```
