+++
title = "Crossmark policy page"
date = "2020-04-08"
draft = false
author = "Martyn Rittman"
type = "documentation"
layout = "documentation_single"
documentation_section = ["crossmark", "crossmark-policy-page"]
identifier = "documentation/crossmark/crossmark-policy-page"
rank = 4
weight = 110300
aliases = [
	"/education/crossmark/crossmark-policy-page",
	"/education/crossmark/crossmark-policy-page/"
]
+++

To participate in Crossmark, you must create a Crossmark policy page on your own website. This page should be assigned a DOI and registered with us.

You may choose to have one Crossmark policy page for all of your titles, or separate policy pages for each title if your policies and guidelines vary from title to title.

Here are some guidelines for what should appear on a Crossmark policy page. The sample introductory text may be reused, but all other information will be member-specific.

## Example introductory text<a id='00331' href='#00331'><i class='fas fa-link'></i></a>

[Crossmark](/documentation/crossmark/) is an initiative to provide a standard way for readers to locate the current version of a piece of content. By applying the Crossmark button, [add member name] is committing to maintaining the content it publishes, and to alerting readers to changes if and when they occur.

Clicking on the Crossmark button will tell you the current status of a document, and may also give you additional publication record information about the document.

## Retraction and correction policies<a id='00332' href='#00332'><i class='fas fa-link'></i></a>

You should include links to retractions and corrections policies found elsewhere on your site.

If you do not have specific policy pages to link to, you should describe those policies instead. For example, if you issue corrigenda while leaving original uncorrected articles in place, you should explain that. If you replace original articles with corrected versions, you should state this.

You may also include links to other policy information, such as guidelines for authors.

## Depositing Crossmark policy page(s)<a id='00333' href='#00333'><i class='fas fa-link'></i></a>

Crossmark policy pages should be deposited as datasets with a "database" called "PublisherName Crossmark Policy Statement". If you have multiple policy pages (for example, different policy pages for different journals) you should include them in the database deposit as multiple datasets.

See an example of [a member’s Crossmark policy page](https://f1000research.com/about/policies) (section *9 Permanency of content*).
