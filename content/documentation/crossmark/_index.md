+++
title = "Crossmark"
date = "2020-04-08"
draft = false
author = "Martyn Rittman"
type = "documentation"
layout = "documentation_single"
documentation_section = ["crossmark"]
aliases = [
    "/education/crossmark",
    "/education/crossmark/",
    "/education/content-registration/administrative-metadata/update-or-retraction",
    "/education/content-registration/administrative-metadata/update-or-retraction/"
]
[menu.main]
parent = "Documentation"
identifier = "documentation/crossmark"
weight = 250000
[menu.documentation]
parent = "Documentation"
identifier = "documentation/crossmark"
rank = 4
weight = 250000
+++

{{< snippet "/_snippet-sources/crossmark.md" >}}
