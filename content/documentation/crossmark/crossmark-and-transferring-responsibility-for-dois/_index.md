+++
title = "Crossmark and transferring responsibility for DOIs"
date = "2020-04-08"
draft = false
author = "Martyn Rittman"
type = "documentation"
layout = "documentation_single"
documentation_section = ["crossmark", "crossmark-and-transferring-responsibility-for-dois"]
identifier = "documentation/crossmark/crossmark-and-transferring-responsibility-for-dois"
rank = 4
weight = 110700
aliases = [
	"/education/crossmark/crossmark-and-transferring-responsibility-for-dois",
	"/education/crossmark/crossmark-and-transferring-responsibility-for-dois/"
]
+++

If content moves from a member which participates in Crossmark to one which does not, the Crossmark button would need to be removed from that content. Although the button can be removed, the Crossmark metadata will remain in the system to enable simple reactivation if the new hosting member chooses to participate in Crossmark.

It is likely that Crossmark-associated content will continue to exist, for example, on readers’ local drives. Clicking on these Crossmark buttons will show a message stating that the content is no longer being tracked in Crossmark, and the current status of the content is unknown.

Learn more about [transferring responsibility for DOIs](/documentation/content-registration/creating-and-managing-dois/transferring-responsibility-for-dois).
