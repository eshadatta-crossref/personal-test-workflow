+++
title = "Journals and articles"
date = "2022-09-30"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["principles-practices", "journals"]
identifier = "documentation/principles-practices/journals"
rank = 4
weight = 10108
aliases = [
  "/documentation/content-registration/descriptive-metadata/journal-title",
  "/documentation/content-registration/descriptive-metadata/journal-title/",
  "/education/content-registration/descriptive-metadata/journal-title-management",
  "/education/content-registration/descriptive-metadata/journal-title-management/",
  "/documentation/content-registration/descriptive-metadata/journal-title-management",
  "/documentation/content-registration/descriptive-metadata/journal-title-management/"

]
+++

Our journals model supports registration of records for journal titles and articles, as well for individual volumes and issues of a journal. We recommend registering DOI records for journal titles and articles, and optionally for volumes and issues.

**Journal titles**

Do:  
* be consistent - journal title records are created from the metadata submitted when you first register your journal and articles. You determine the exact title and ISSN included in the deposit, and we record that title and ISSN in a title record in our database. The title, ISSN, and title-level persistent identifier supplied in your content registration files must be consistent across submissions.
* register a [title-level DOI](/documentation/member-setup/constructing-your-dois/the-structure-of-a-doi/#00470) for your journal
* include all registered ISSN for your journal - the ISSN is crucial for identifying a serial. If you are supplying us with data for older titles that predate ISSN assignment, you should request ISSNs from your ISSN agency as they can be assigned retroactively. This isn’t only for our convenience - libraries, database providers, and other organizations using your data will welcome (and often require) an ISSN for anything defined as a journal.
* supply distinct ISSN and / or title DOI for each distinct version of a title. If a title changes significantly the publisher should obtain new ISSNs (both print and online). This rule is established by the [ISSN International Centre](http://issn.org/), not us, but we support and enforce it. Minor title changes (such as changing ‘and’ to ‘&’) don’t require a new ISSN.
* supply all commonly used title abbreviations within the repeatable `abbrev` element
* supply a journal language using the `language` attribute

Do not:
* register issues and articles published under a past title under the current title - this makes it hard to match DOIs to citations and accurately identify items published over time. ome publishers consolidate all versions of a title under the most recent title. This isn’t recommended practice as it causes a lot of linking and citing confusion – you’ve essentially created two (or more) versions of a title. This is particularly confusing when volume and issue numbers overlap between title iterations.
* Journal titles should reflect the **journal title at the time of publication**, and should not be updated if the journal title changes later on.
* vary your journal title without obtaining a new ISSN

For recommendations on displaying information about journals, see the [ISSN Manual](http://www.issn.org/2-23364-ISSN-Manual.php), and NISO's recommended practice on [Presentation & Identification of E-Journals](https://www.niso.org/standards-committees/pie-j).

**Journal issue and volume metadata**
You can register DOIs for volumes and issues of journals if you want to make them citeable and linked persistently. If you do not opt to register DOIs for volumes and issues you should still provide cleam and complete metadata as it is needed for the article records within each issue.

Do:
* supply accurate issue and volume numbers
* for special issues, include additional metadata to make the issue identifiable and citable - this includes:
** editors in the `contributors` section
** issue title (as `title`)
** any special issue numbering, including text (as `special_numbering`)

Do not:
* include non-essential text in the `volume` or `issue` elements

**Journal articles**
You should register all research articles published by your journal, as well as other cite-able content (book reviews, case studies, editorials).

Do:
* pay close attention to [title](https://www.crossref.org/documentation/schema-library/markup-guide-metadata-segments/contributors/) best practice - journal article metadata is used for display and discoverability, so it is vital that article titles are accurate.
* include all [contributors](/documentation/principles-practices/best-practices/#contributors) and (again) pay close attention to the metadata you are sending us.
* include affiliation info for each contributor
* include abstracts (recommended for all types of content, but particularly useful for journal articles)
* include all relevant [funding](/documentation/principles-practices/best-practices/funding/), [license](/documentation/principles-practices/best-practices/license/), and [relationship](/documentation/principles-practices/best-practices/relationships/) metadata
* include [full text URLs](/documentation/retrieve-metadata/rest-api/providing-full-text-links-to-tdm-tools/) to facilitate text and data mining
* include information on updates, corrections, withdrawals, and retractions via [Crossmark](/services/crossmark/)
* include [references](/documentation/principles-practices/best-practices/#references)
* include a language (using the language attribute on the journal_article element)

Do not:
* register records for items you do not intend to maintain long-term, such as advertisements
* supply titles in all caps - titles in article metadata are used for display and citation formatting, and if it supplied in all caps, it will appear in all caps wherever our metadata is used.

Review our [Journals and Articles Markup Guide](/documentation/schema-library/markup-guide-record-types/journals-and-articles) for XML and metadata help.   
