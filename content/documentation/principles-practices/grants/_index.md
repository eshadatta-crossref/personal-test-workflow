+++
title = "Grants"
date = "2021-11-10"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["principles-practices", "grants"]
identifier = "documentation/principles-practices/grants"
rank = 4
weight = 10107
aliases = [

]
+++

Grants may be registered using our grants model.

When registering a grant, DO:

* include required project information (a project title, a funder name and identifier, and a funding type) as well as your internal grant or award number
* include information describing grant-funded projects such as project description, language information, investigator details (including ORCID IDs and ROR IDs within affiliations)
* include award amounts and currency,
* and project start and end dates and/or an award date
* include multiple titles and descriptions as well as language information;
* include a funding scheme, and planned project start and end dates when relevant

Review our [Grants Markup Guide](/documentation/schema-library/markup-guide-record-types/grants/) for XML and metadata help.
