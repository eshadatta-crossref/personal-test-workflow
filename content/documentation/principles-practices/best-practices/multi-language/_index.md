+++
title = "Multi-language material and translations"
date = "2023-01-18"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["principles-practices", "best-practices", "multi-language"]
identifier = "documentation/principles-practices/best-practices/multi-language"
rank = 4
weight = 10110
aliases = [

]
+++
Much of the content in Crossref is English language, but we encourage members to register content in the appropriate language for the content being registered.  We support UTF-8 encoded character sets and in many cases you will be able to supply multiple versions of titles, abstracts, and other metadata.

If you consider your content to be *multi-language* and not a translation (meaning it will be cited as a single item)
Do:
* register one DOI for the item
* include titles, abstracts, and other metadata in multiple languages in your metadata record (support for this varies by content type)
* pay attention to order in your input XML - if the English title is provided as the primary title in your metadata, then the English title will be displayed in citations generated from our metadata

If your content is *translated* -

Do:
* register separate DOIs for each translation
* connect each registered record with relationship metadata (`hasTranslation`)
* include language metadata wherever possible

Do not:
* cram multiple titles in multiple languages in one element - this impacts discoverability and citation formatting

Review our [Translated and multi-language materials Markup Guide](/documentation/schema-library/markup-guide-metadata-segments/multi-language/) for XML and metadata help.
