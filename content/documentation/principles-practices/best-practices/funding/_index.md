+++
title = "Funding metadata"
date = "2022-06-01"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["principles-practices", "best-practices", "funding-metadata"]
identifier = "documentation/principles-practices/best-practices/funding-metadata"
rank = 4
weight = 10106
aliases = [

]
+++
Funding metadata may be supplied for most record models, and helps link funding to research.

When registering funding metadata, DO:

* include the name of the funding organization and the funder identifier from the [Open Funder Registry](/documentation/funder-registry/accessing-the-funder-registry/)
* include an award/grant number whenever possible
* include a funder name if a funder identifier is not available - funding data that is not paired with a funder identifier is not available via our APIs, but we may be able to match the name supplied with an identifier and make the data available
* pay close attention to the structure of your metadata - correct nesting of funder names and identifiers is essential as it significantly impacts how funders, funder identifiers, and award numbers are related to each other

Do not:
* Include incomplete funder names or acronyms as a `funder_name`, particularly if you have not supplied an accompanying funder identifier.

Some additional best practices for extracting data and working with vendors to supply funding data are available in this [Best Practices for depositing funding data]()/blog/best-practices-for-depositing-funding-data/) blog post, and review our [Funding Markup Guide](/documentation/schema-library/markup-guide-metadata-segments/funding-information/) for XML and metadata help.
