+++
title = "Bibliographic metadata"
date = "2022-05-31"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["principles-practices", "best-practices", "bibliographic-metadata"]
identifier = "documentation/principles-practices/best-practices/bibliographic-metadata"
rank = 4
weight = 10104
aliases = [

]
+++
The bibliographic (descriptive) metadata you send us is used to display citations, match DOIs to citations, and to enhance discovery services.  It is essential that this metadata is clean, complete, and accurate.

Do:
* provide all contributors, titles, dates, and identifiers associated with the item you are registering
* make sure the contributors, titles, dates, and identifiers are accurate
* update and correct metadata as needed

Do not:
* supply titles, names, or other metadata in all caps, even if that is how you display and store them - it makes it difficult for others to use your metadata to format citations (and link to your content)
* omit article identifiers, page numbers, or author names - omissions will make your metadata less or undiscoverable
* force metadata into fields that aren’t a good match - it’s often better just to leave it out. For example, putting subject keywords into a title

### Titles

Your metadata should include the title used for the content when it was first published. For most types of content alternate titles and subtitles can be provided as well (see each [record type markup guide](/documentation/schema-library/markup-guide-record-types/) for details).  You are also able in most cases to provide titles in multiple languages (see [translated and multi-language materials](/documentation/schema-library/markup-guide-metadata-segments/multi-language/)).

Do:
* use subtitles - subtitles are supported in a distinct subtitle element, and allow an item to be discoverable using the main title, subtitle, or both combined.
* supply alternate titles, abbreviated titles, and translated titles if you use them in citation recommendations.
* use [face markup](/documentation/schema-library/markup-guide-metadata-segments/face-markup/) and/or [MathML](/documentation/schema-library/markup-guide-metadata-segments/mathml/) in titles when it impacts the meaning of the text.

Do not:
* include non-title metadata such as author, price, or volume numbers in a title field - this is a common error that significantly impacts discoverability and display.
* cram multiple titles in multiple languages in one element - see [translated and multi-language materials](/documentation/schema-library/markup-guide-metadata-segments/multi-language/) for guidance.
* supply titles in ALL CAPS - our metadata is often used for display and citation formatting.

Additional best practices may apply for the content you are registering, see specific content type guides for details.

### Contributors

Contributor metadata is expressed consistently across content types (excluding Grants), and includes contributor names, roles, identifiers, alternate names, and affiliation information.  A contributor is a single person or a group of people/organization that has contributed in some way to the content being registered.  

Do include:
* correct names, so authors and other contributors can be matched to citations
* a complete contributor list so that contributors can receive credit for their work, and to help make your content more discoverable
* Contributor role(s) - at least one for each contributor, but supply as many as apply
* ORCID iDs, so that authors can be disambiguated and connected to the research they write and support
* Affiliations and [ROR IDs](/documentation/schema-library/markup-guide-metadata-segments/affiliations/) so that contributor institutions can be identified and research outputs can be traced by institution

Do not:
* include suffixes such as *Jr*, *Sr*, *IV* in the family name field - use the `suffix` element
<!-- * Alternate names, for contributors whose names may be represented in multiple ways (nicknames, aliases, or different character sets) - this helps with discovery -->

Guidance on constructing XML for contributors can be found in our [Contributors Markup Guide](/documentation/schema-library/markup-guide-metadata-segments/contributors/).  

### Dates (publication and other)

Do:
* supply the entire date whenever possible - for most dates supplied within our metadata we allow you to supply just a year, with month and day being optional, but we encourage you to supply full dates whenever possible, particularly for online content.  
* supply all relevant date types - for most items a publication date is required and other dates are optional but we encourage you to supply all dates that apply to the content you are registering. This includes acceptance dates for most content, and approval and posted dates for others.
* Include the correct date at both the parent and child level (journal issue / article, book title / chapter)
* Include both online and print publication dates (if applicable)

Do not:
* supply only the most recent publication date - this is inaccurate and may impact your registration fees, as back year rates are calculated based on the publication year provided in your registration metadata.

### Page numbers and article identifiers
Correct page number and article identifier (aka e-location ID) metadata is essential for many discovery systems.

Do:
* be careful with your pages - be sure each page element contains only the page number itself, not a range.  This means capture the first page in first_page, the last page in last_page, and any additional page information in other_pages.  If your content has pages, first_page is essential.
* if you use article numbers /e-location IDs, supply them as described in the [markup guide](/documentation/schema-library/markup-guide-metadata-segments/article-ids/)

Do not:
* include an entire page range in first_page - this is incorrect, and will throw off many matching processes (in Crossref and beyond) and cause your metadata to be displayed incorrectly wherever it is used.
* include extraneous text in the page field - just the page please, no ‘page 1’ or ‘1st pg’

Guidance on constructing XML for article IDs and page ranges can be found in our [Article ID Markup Guide](/documentation/schema-library/markup-guide-metadata-segments/article-ids/).  
