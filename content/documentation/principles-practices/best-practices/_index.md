+++
title = "Metadata best practices"
date = "2021-10-21"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["principles-practices", "best-practices"]
identifier = "documentation/principles-practices/best-practices"
rank = 4
weight = 10100
aliases = [
  "/education/metadata/metadata-for-different-purposes",
  "/education/metadata/metadata-for-different-purposes/",
  "/documentation/content-registration/descriptive-metadata/title",
  "/documentation/content-registration/descriptive-metadata/title/",
    "/education/content-registration/descriptive-metadata/title",
    "/education/content-registration/descriptive-metadata/title/",
    "/education/content-registration/descriptive-metadata/numbers",
    "/education/content-registration/descriptive-metadata/numbers/",
    "/documentation/content-registration/descriptive-metadata/dates",
    "/documentation/content-registration/descriptive-metadata/dates/",
    "/education/content-registration/descriptive-metadata/dates",
    "/education/content-registration/descriptive-metadata/dates/",
    "/documentation/content-registration/administrative-metadata/",
    "/documentation/content-registration/administrative-metadata",
    "/documentation/content-registration/descriptive-metadata/",
    "/documentation/content-registration/descriptive-metadata",
    "/documentation/content-registration/structural-metadata/",
    "/documentation/content-registration/structural-metadata"
]
+++

Best practices, like principles, are aspirational for our members, but we’ll do our best to help you meet them. Our systems, schema, and practices have evolved over time and, as with many organizations, we need to balance the decisions of the past with the needs of the future. When a best practice is not met, we try to assess that honestly with a goal of meeting the best practice in the future.

Crossref metadata requirements for content registration are minimal but meeting minimum requirements only means that you have succeeded in registering your content and DOIs.  Most of the optional metadata we collect is recommended to improve discoverability and connect content persistently to the scholarly record.  There are nuances and best practices for both different types of content and different types of metadata.

Best practices are available for the following:

{{% row %}}
{{% column %}}
##### Best practices for key metadata elements
* [Abstracts](/documentation/principles-practices/best-practices/abstracts)
* [Bibliographic metadata](/documentation/principles-practices/best-practices/bibliographic)
  * [Titles](/documentation/principles-practices/best-practices/bibliographic#titles)
  * [Contributors](/documentation/principles-practices/best-practices/bibliographic#contributors)
  * [Dates](/documentation/principles-practices/best-practices/bibliographic#dates)
  * [Page numbers and article IDs](/documentation/principles-practices/best-practices/bibliographic#page-numbers-and-article-identifiers)
* [Funding](/documentation/principles-practices/best-practices/funding)
* [License](/documentation/principles-practices/best-practices/license)
* [Multi-language and translated content](/documentation/principles-practices/best-practices/multi-language)
* [References](/documentation/principles-practices/best-practices/references)
* [Relationships](/documentation/principles-practices/best-practices/relationships)
{{% /column %}}

{{% column %}}
##### Best practices for key content types  
* [Books and chapters](/documentation/principles-practices/books-and-chapters)
* [Conference proceedings and papers](/documentation/principles-practices/conference-proceedings/)
* [Datasets](/documentation/principles-practices/datasets/)
* [Dissertations](/documentation/principles-practices/dissertations/)
* [Grants](/documentation/principles-practices/grants/)
* [Peer review](/documentation/principles-practices/peer-review/)
* [Pending publication](/documentation/schema-library/markup-guide-record-types/pending-publications/)
* [Posted content](/documentation/principles-practices/posted-content/)
* [Reports and working papers](/documentation/principles-practices/reports/)
* [Standards](/documentation/principles-practices/standards)
{{% /column %}}
{{% /row %}}
