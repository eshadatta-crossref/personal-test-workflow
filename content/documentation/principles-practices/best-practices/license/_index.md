+++
title = "License metadata"
date = "2022-06-01"
draft = false
author = "Patricia Feeney"
type = "documentation"
layout = "documentation_single"
documentation_section = ["principles-practices", "best-practices", "license"]
identifier = "documentation/principles-practices/best-practices/license"
rank = 4
weight = 10109
aliases = [

]
+++
Members registering licensing information in their metadata let researchers know when they can perform [TDM](/documentation/retrieve-metadata/rest-api/text-and-data-mining/) and under what conditions. This license could be proprietary, or an open license such as Creative Commons.

When supplying license metadata, DO:
* provide URLs for proprietary or open licenses for your content
* supply dates that apply to your licenses (to support embargos, for example) and KEEP THEM UP TO DATE!
* make sure the URLs resolve to an active license
* review any metadata records you acquire from other members to make sure the license data supplied is accurate and up to date

Do not:
* supply unverified license URLs - we make sure the URLs provided are URLs, but don't verify that they resolve to an active license

Review our [License information Markup Guide](/documentation/schema-library/markup-guide-metadata-segments/license-information/) for XML and metadata help.
