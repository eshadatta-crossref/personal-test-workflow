+++
title = "Creating a landing page"
date = "2021-11-08"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["member-setup", "creating-a-landing-page"]
identifier = "documentation/member-setup/creating-a-landing-page"
rank = 4
weight = 50800
aliases = [
	"/education/member-setup/creating-a-landing-page",
	"/education/member-setup/creating-a-landing-page/"
]
+++

As soon your content is registered with Crossref, users will be able to retrieve identifiers and create links with them. Crossref DOIs must resolve to a landing (or response) page that you maintain.

A landing page is a web page that provides further information to someone who has clicked on a DOI link to help them confirm that they are in the right place.

### Landing pages for published content

The landing page for published content should contain:

* **Full bibliographic information**: so that the user can verify they have been delivered to the correct item
* **The DOI displayed as a URL**: so that if a reader wishes to cite this item, they can just copy and paste the DOI link (learn more about our [DOI display guidelines](/display-guidelines))
* **A way to access the full-text of the content**: It's acceptable for the full text to be behind a login or paywall - this is fine as long as the landing page is accessible to everyone. A DOI can resolve to the HTML full-text of the content, and if this page includes the criteria above, a separate landing page is not necessary. It's not good practice to link directly to a PDF however, as it will start downloading when the DOI is clicked.

Here are some examples of landing pages for published content:
* [https://doi.org/10.33545/26175754.2021.v4.i2a.110](https://doi.org/10.33545/26175754.2021.v4.i2a.110)
* [https://doi.org/10.11646/zootaxa.5164.1.1](https://doi.org/10.11646/zootaxa.5164.1.1)
* [https://doi.org/10.54957/jolas.v2i2.182](https://doi.org/10.54957/jolas.v2i2.182)
* [https://doi.org/10.1364/JOSAB.1.000354](https://doi.org/10.1364/JOSAB.1.000354)

Many Crossref journal DOIs resolve to an abstract.

#### And a little more for preprints
As well as the criteria above, a preprint landing page should also prominently identify the content as a preprint and include a link to any AAM or VOR. This information should be above the fold.

### Landing pages for grants

The landing pages for grants should contain:
* Information about the grant so the user can verify they've been delivered to the correct item
* The DOI displayed as a URL - learn more about our [DOI display guidelines](/display-guidelines).

Here are two example landing pages for grants: [https://doi.org/10.37717/220020589](https://doi.org/10.37717/220020589) and [https://doi.org/10.35802/107769](https://doi.org/10.35802/107769).
