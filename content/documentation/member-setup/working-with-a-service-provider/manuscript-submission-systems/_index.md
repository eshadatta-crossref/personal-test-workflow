+++
title = "Manuscript submission systems"
date = "2020-04-08"
draft = false
author = "Amanda Bartell"
type = "documentation"
layout = "documentation_single"
documentation_section = ["member-setup", "working-with-a-service-provider", "manuscript-submission-systems"]
identifier = "documentation/member-setup/working-with-a-service-provider/manuscript-submission-systems"
rank = 4
weight = 50902
aliases = [
	"/education/member-setup/working-with-a-service-provider/manuscript-submission-systems",
	"/education/member-setup/working-with-a-service-provider/manuscript-submission-systems/",
	"/service-providers/submission-systems/",
	"/service-providers/submission-systems"
]
+++

Metadata first takes shape with author manuscripts.

## Discoverability starts here<a id='00507' href='#00507'><i class='fas fa-link'></i></a>

Manuscript submission systems, and the authors who use them, start metadata on its long lifecycle. Publishers’ instructions to authors and author-created metadata (such as keywords) are first captured by manuscript submission systems.

Along with other publishing service organizations such as typesetters, submission systems are a kind of service provider, and are the first step in getting [content registered](/services/content-registration/), often by yet another kind of service provider, [hosting platforms](/documentation/member-setup/working-with-a-service-provider/hosting-platforms).

## Beyond bibliographic<a id='00508' href='#00508'><i class='fas fa-link'></i></a>

Learn about the importance of depositing [different types of metadata](/services/content-registration/).

We encourage the fullest metadata possible and recognize that manuscript submission systems are among those that make this possible!
