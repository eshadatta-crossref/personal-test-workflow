+++
title = "Working with a service provider"
date = "2020-04-08"
draft = false
author = "Amanda Bartell"
type = "documentation"
layout = "documentation_single"
documentation_section = ["member-setup", "working-with-a-service-provider"]
identifier = "documentation/member-setup/working-with-a-service-provider"
rank = 4
weight = 50900
aliases = [
	"/education/member-setup/working-with-a-service-provider",
	"/education/member-setup/working-with-a-service-provider/",
	"/service-providers/",
	"/service-providers"
]
+++

Service providers such as hosting platforms and XML providers may [provide or deposit metadata](/services/content-registration/) with Crossref on behalf of members.

These organizations provide key services to our shared community, by formatting metadata and providing visibility to registered content on hosting platforms. Working closely with service providers as our own services develop is critical to improving research communications at all points in the supply chain.

If you're working with a service provider, check that they’re helping you fulfil your [member obligations](/membership#member-obligations), and look at your [Participation Report](https://www.crossref.org/members/prep/) to see how your metadata is growing. Learn more about [metadata stewardship](/documentation/metadata-stewardship/), and how [our reports help you evaluate and improve your metadata](/documentation/reports/). If you would like your reports to go directly to a contact at your service provider, please [contact us](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001618152/) so we can add their details.

Even if you are not moving platforms, you might find our [checklist for platform migration](/documentation/member-setup/working-with-a-service-provider/checklist-for-platform-migration) helpful in discussing aspects of metadata deposit with your service provider.

If you are looking to access rather than deposit Crossref metadata, learn more about [metadata retrieval](/documentation/retrieve-metadata/) and choose the best service for your needs.
