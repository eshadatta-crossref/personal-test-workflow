+++
title = "Constructing your DOIs"
date = "2021-07-09"
draft = false
author = "Isaac Farley"
type = "documentation"
layout = "documentation_single"
documentation_section = ["member-setup", "constructing-your-dois"]
identifier = "documentation/member-setup/constructing-your-dois"
rank = 4
weight = 50700
aliases = [
  "/education/member-setup/constructing-your-dois",
  "/education/member-setup/constructing-your-dois/",
  "/education/member-setup/constructing-your-dois/tips-for-creating-a-doi-suffix/",
  "/education/member-setup/constructing-your-dois/tips-for-creating-a-doi-suffix",
  "/education/member-setup/constructing-your-dois/example-doi-suffix-patterns/",
  "/education/member-setup/constructing-your-dois/example-doi-suffix-patterns"
]
+++

Crossref allows citation linking using [Digital Object Identifiers (DOIs)](#00232) between research produced by different organizations (without the need for individual agreements between them). This ensures that citation links are persistent - that they work over long periods of time. However, there is no purely technical solution to the problem of broken links on the web; Crossref members have to keep these links updated, along with rich metadata that everyone in the scholarly ecosystem relies on.

At Crossref, every metadata record that our members register for their content needs to have a unique DOI attached to it, both as a container for that record and as a locator for others to use. A DOI does not signify any value or accuracy of the thing it locates; the value lies in the record's metadata which gives context about the object (such as contributors, funding bodies, abstract/summary) and enables connections with other entities (such as people (e.g. ORCID) or organizations (e.g. ROR)).

DOIs include 3 parts:

<figure><img src='/images/documentation/DOI-structure.png' alt='The structure of a DOI: resolver, prefix, suffix' title='' width='75%'></figure>
<button id="show-img" type="button" class="btn btn-default" data-toggle="modal" data-target="#image110">Show image</button>
<div id="image110" class="modal fade" aria-labelledby="my-modalLabel" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" data-dismiss="modal">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img src="/images/documentation/DOI-structure.png" class="img-responsive" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

Of these three parts of the DOI, members (or their service providers) create the last part, the suffix. Because DOIs must be unique and persistent, members need a reasonable way to create and manage their suffixes, which should be [opaque](#whyopaque).

Here, we share the [rules](#afewrules), guidelines and some examples to help you decide how to approach your suffixes. You can also go straight to our [suffix generator](/documentation/member-setup/constructing-your-dois/suggested-doi-registration-workflow-including-suffix-generator/).

Note, if you’re using the Crossref XML plugin for OJS, you don’t need to create your suffixes as [the plugin will generate them for you automatically](https://docs.pkp.sfu.ca/crossref-ojs-manual/en/gettingStarted#configuring-the-doi-plugin-for-ojs).

We’ve also got some [advice for DOIs and DSpace repositories](/documentation/member-setup/constructing-your-dois/dois-and-dspace-repositories).

## First, a few rules
<a id="afewrules"></a>
Rules are shared by all DOI registration agencies.

* Each DOI must be unique
* Only use approved characters: DOI suffixes can be any alphanumeric string that includes combinations the following approved characters:
    - Letters of the Roman alphabet, A-Z (see below on case insensitivity)
    - Numbers, 0-9
    - -._;()/ (hyphen, period, underscore, semicolon, parentheses, forward slash). Note that em and en dashes are not approved characters.
        - Note, some older (pre-2008) DOIs which contain other characters. Learn more about [suffixes containing special characters](/documentation/member-setup/constructing-your-dois/suffixes-containing-special-characters).
* Suffixes are case insensitive, so `10.1006/abc` is the same in the system as `10.1006/ABC`. Note that using lowercase is better for accessibility.  

## Guidelines for creating a DOI suffix <a id='00007' href='#00007'><i class='fas fa-link'></i></a>
In part because there are few rules, it can be helpful to have some guidance in how to approach suffixes. This advice applies to [DOIs at all levels](https://www.crossref.org/education/metadata/persistent-identifiers/the-structure-of-a-doi#00469), whether at journal or book level (_a title-level DOI_), or volume, issue, article, or chapter level.

The most important part of creating your DOIs is to understand that because DOIs are unique, persistent and '[dumb](#whyopaque)', once they are created, they will always work. There is never a need to delete or update existing DOIs.

**Best practices for DOI suffixes:**
Suffixes are best when they include short strings that are easily displayed and typed but are ‘dumb’ - meaning, the suffixes contains no readable information, including metadata.

* Use a random approach: this ensures the DOIs are opaque or ‘dumb’ and minimizes attempts at interpretation or prediction (more on [opaque suffixes below](#whyopaque)). Try our suggested [DOI registration workflow](/documentation/member-setup/constructing-your-dois/suggested-doi-registration-workflow-including-suffix-generator), including our suffix generator. Any random generator will also work. Note, if you’re using the Crossref XML plugin for OJS,you don’t need to create your suffixes as [the plugin will generate them for you automatically](https://docs.pkp.sfu.ca/crossref-ojs-manual/en/gettingStarted#configuring-the-doi-plugin-for-ojs).
* Keep suffixes short. This makes them easier to read and to re-type. Remember, DOIs will appear online and in print.

**Best practice DOI example:** `10.3390/s18020479` This example appears to be opaque because it includes no obvious information.

**Avoid the following in DOI suffixes:**
The function of suffixes is technical in nature so they are most problematic when they are treated as information to be read, interpreted and/or predicted. _Remember, DOIs are persistent and not subject to correction or deletion._

* While it may be tempting, using a pattern, such as a sequence, can cause problems. Services and tools that use DOIs may, for example, try to predict future DOIs that are not registered and may never be (more on [opaque suffixes](#whyopaque) below).
* Don’t include information like journal title (or initials), page number or date. This kind of information should be included in the metadata but can cause problems when included in suffixes for 2 main reasons:
  - Information in the suffix that conflicts with information in the metadata is confusing.
  - Information like journal title (or initials) may change or be found to be incorrect, as with dates, but DOIs are persistent, cannot be deleted and are not subject to correction. See more on [opaque suffixes](#whyopaque) below.  

**Example problematic DOI suffix:** `10.5555/2014-04-01` This example is not opaque because it includes a date, which should be included in the metadata instead of in the suffix.

**Proceed with caution in DOI suffixes:**
Determining how to create suffixes and manage the over time can be a challenge. We recognize that some systems have requirements that don’t follow this advice and that human readability is helpful in managing DOIs.
* If you must use a suffix with meaning, internal system identifiers can work, with careful management. Because things like ISBNs are themselves metadata, we don’t recommend using them in suffixes.

Just remember that while you and readers may recognize an ISBN, for example, the DOI system itself doesn’t and DOIs are not subject to correction or deletion.

No matter your approach, it’s worth taking some time to understand the emphasis on [opaque suffixes](#whyopaque).

Once a DOI has been registered with us, it should always be used for the same content. Even if the content moves to a new website or a new owner, the same DOI should continue to be used. Though the DOI never changes, its associated metadata is [kept up-to-date](/documentation/register-maintain-records/maintaining-your-metadata/) by the relevant Crossref member.

## What if your content already has a DOI?

Sometimes members may acquire a journal that already has DOIs registered for some articles. It's important to keep and continue to use the DOIs that have already been registered and not change them - DOIs need to be persistent.

It doesn't matter if the prefix on the existing DOI is different from the prefix belonging to the acquiring member. As content can move between members, the owner of a DOI is not necessarily the same as the owner of the prefix. Read more about [transferring responsibility for DOIs](/documentation/content-registration/creating-and-managing-dois/transferring-responsibility-for-dois).

## The importance of opaque identifiers
<a id="whyopaque"></a>

**What are opaque suffixes & why they are important**

Suffixes are ‘dumb numbers.’ They are essentially meaningless on their own and meant to be that way--opaque. One good reason for that is because when something is meaningless, it doesn’t need to be corrected.

DOIs should not include information that can be understood, interpreted or predicted, especially information that may change. Page numbers and dates are examples of information that shouldn’t be included in suffixes. It is particularly problematic if the suffix includes information that conflicts with the metadata associated with the DOI.

We’ve referred to creating ‘suffix patterns’ in the past but information that includes or implies a pattern is also problematic. A sequence of numbers, for example, lends itself to the assumption that future DOIs can be predicted.

Scraping for DOIs - or what appear to be DOIs--is common, as is the likelihood that what is--or appears to be--a pattern will be treated as such. Just as the timing of DOI registration is important, in order to avoid unregistered DOIs, their construction is critical to avoiding interpretation.

## More information on creating DOIs
Here are a few other resources that discuss creating DOIs and the importance of using opaque suffixes.

* https://blog.datacite.org/cool-dois/
* https://www.crossref.org/blog/dois-and-matching-regular-expressions/
* https://en.wikipedia.org/wiki/Handle_System  
