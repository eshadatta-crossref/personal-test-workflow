+++
title = "Thank you for contacting us"
date = "2022-08-04"
+++

## Thank you

Thanks for getting in touch. We will reply by email as soon as we can. In the meantime, please check out our [forum](https://community.crossref.org) where other community members may be able to help sooner.