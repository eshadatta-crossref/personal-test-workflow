---
title: Citation Formatting Service
author: Crossref
date: 2013-01-22

---

{{% divwrap blue-highlight %}}

**Note:** This experiment has graduated. This description has been kept for reference, but many of the links and/or services that appear below no longer work.

Its functionality is now a standard feature of  DOI content negotiation.

{{% /divwrap %}}

{{% labsection %}}
Crossref labs has added two new content types to dx.doi.org resolution for Crossref DOIs. These allow anyone to retrieve DOI bibliographic metadata as formatted bibliographic entries. To perform the formatting we&#8217;re using the citation style language processor, citeproc-js which supports a shed load of citation styles and locales. In fact, all the styles and locales found in the CSL repositories, including many common styles such as bibtex, apa, ieee, harvard, vancouver and chicago are supported.

First off, if you&#8217;d like to try citation formatting without using content negotiation, <a href="http://crosscite.org/citeproc/" target="_blank" rel="external">there&#8217;s a simple web UI</a> that allows input of a DOI, style and locale selection.

If you&#8217;re more into accessing the web via your favorite programming language, have a look at these content negotiation curl examples. To make a request for the new &#8220;text/bibliography&#8221; content type:

```
$ curl -LH "Accept: text/bibliography; style=bibtex" http://dx.doi.org/10.1038/nrd842
```

And get:

```
@article{Atkins_Gershell_2002, title={From the analyst's couch: Selective anticancer drugs}, volume={1}, DOI={10.1038/nrd842}, number={7}, journal={Nature Reviews Drug Discovery}, author={Atkins, Joshua H. and Gershell, Leland J.}, year={2002}, month={Jul}, pages={491-492}}
```

A locale can be specified with the &#8220;locale&#8221; content type parameter, like this:

```
$ curl -LH "Accept: text/bibliography; style=mla; locale=fr-FR" http://dx.doi.org/10.1038/nrd842
```

Which gets you:

```
Atkins, Joshua H., et Leland J. Gershell. « From the analyst's couch: Selective anticancer drugs ». Nature Reviews Drug Discovery 1.7 (2002): 491-492.
```

You may want to process metadata through CSL yourself. For this use case, there&#8217;s another new content type, &#8220;application/citeproc+json&#8221; that returns metadata in a citeproc-friendly JSON form:

```
$ curl -LH "Accept: application/citeproc+json" http://dx.doi.org/10.1038/nrd842
```

Returns the following


``` JS
{
	"volume":"1",
	"issue":"7",
	"DOI":"10.1038/nrd842",
	"title":"From the analyst's couch: Selective anticancer drugs",
	"container-title":"Nature Reviews Drug Discovery",
	"issued":{"date-parts":[[2002,7]]},
	"author":[{"family":"Atkins","given":"Joshua H."},{"family":"Gershell","given":"Leland J."}],
	"page":"491-492",
	"type":"article-journal"
}
```

Finally, to retrieve lists of supported styles and locales, either hit these URLs:

  * http://data.crossref.org/styles
  * http://data.crossref.org/locales

or check out the CSL style and locale repositories.

There&#8217;s one big caveat to all this. The CSL processor will do its best with Crossref metadata which can unfortunately be quite patchy at times. There may be pieces of metadata missing, inaccurate metadata or even metadata items stored under the wrong field, all resulting in odd-looking formatted citations. Most of the time, though, it works.

If you have any comments, suggestions or bug reports please fee free to send them to us here at:

<img class="alignnone size-full wp-image-75" src="/images/labs/labs-email.png" alt="labs_email" width="233" height="42" />


{{% /labsection %}}
