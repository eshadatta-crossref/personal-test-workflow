---
title: Randoim
author: Crossref
date: 2013-01-22

---

{{% divwrap blue-highlight %}}

**Note:** This experiment has graduated. This description has been kept for reference, but many of the links and/or services that appear below no longer work.

Equivalent functionality can now be found using the `sample` function in the Crossref REST API.

{{% /divwrap %}}

{{% labsection %}}
Randoim is a simple API that will return you a…

(drum roll)

Random Crossref DOI.

Um, have we gone nuts?

Well, possibly, but this is actually a useful tool for Crossref internally as it allows us to more easily test our various new APIs and tools against representative samples of Crossref metadata.

It might also be useful to anybody doing research on scholarly publications.

The API, of course, also supports limiting your random DOI selection to particular subsets of Crossref metadata. So, for example, you can select a random DOI from a particular date range, ISSN, etc. Documentation for the APIas well as an explanation of how we “randomly” select DOIs can be found on the site itself.

Also note that, as with all things on the labs site, this tool is only guaranteed to break and make your life even more miserable and pathetic than it already is.

Send complaints, hate mail and snide remarks to:

&nbsp;

<img class="alignnone size-full wp-image-75" src="/images/labs/labs-email.png" alt="labs_email" width="233" height="42" />


 [1]: https://api.crossref.org

{{% /labsection %}}
