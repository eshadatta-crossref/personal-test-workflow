---
title: Family Name Detector
author: Crossref
date: 2013-01-22

---
{{% divwrap blue-highlight %}}

**Note:** This experiment has been retired. This description has been kept for reference, but many of the links and/or services that appear below no longer work.

{{% /divwrap %}}


{{% labsection %}}
**Overview**
  
Crossref Labs has created a <a href="" target="_blank" rel="external">small web API</a> that wraps a family name database here at Crossref R&D. The database, built from Crossref&#8217;s metadata, lists all unique family names that appear as contributors to articles, books, datasets and so on that are known to Crossref. As such the database likely accounts for the majority of family names represented in the scholarly record.

The web API comes with two services: a family name detector that will pick out potential family names from chunks of text and a family name autocompletion system.

Very brief documentation can be found <a style="line-height: 1.714285714; font-size: 1rem;" href="" target="_blank" rel="external">here</a> along with a jQuery example of autocompletion.

**The Weasel Speaks&#8230;**

The database is still in development so there may be some oddities and inaccuracies in there. Right now one obvious omission from the name list that I hope to address soon are double-worded names such as &#8220;von Neumann&#8221;. We&#8217;re not proposing this database as an authority but rather something that backs a practical service for family name detection and autocompletion.

If you have any comments, suggestions, bug reports or (hint, hint) patches, please fee free to send them to us here at:

<img class="alignnone size-full wp-image-75" src="/images/labs/labs-email.png" alt="labs_email" width="233" height="42" />

Or just snipe about it on Google+.


{{% /labsection %}}