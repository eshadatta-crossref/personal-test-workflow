+++
date = "2023-02-20"
lastmod = "2023-02-20"
draft = false
title = "Labs"
name = "Crossref Labs"
author = "A Crossref Labs creature"

rank = 1
weight = 5
+++

{{% labsection %}}

Crossref Labs is the dedicated R&D arm of Crossref. In late 2021 we announced that we are [re-energizing Labs](/blog/time-to-put-the-r-back-in-rd/) after a period of working mostly on development tasks.

### What's our focus?

The division between what the R&D group does at the group-level, and what the wider organisation does will always be more of a gradient than a line. But at the highest level we'd say that R&D will focus on projects that:

* Address new constituencies.
* Involve fundamentally new approaches (technology or process or both).
* Are exploratory with no clear product application yet.

And that a "strategic initiative" (as opposed to a new feature, service, or product) is something that:

* Involves something we've never done before.
* Involves potential changes to our membership model and fees.
* Would require a large investment of resources outside of normal budget.

We’re certainly not the only group at Crossref who experiment, build proof of concepts (POCs), and do research, but we hope to support other groups who do - both inside our organization and in the wider research ecosystem.

Sound right up your street? Interested in collaborating on something you’re working on? Let us know.

<img class="alignnone size-full wp-image-75" src="/images/labs/labs-email.png" alt="labs_email" width="233" height="42" />

### What are we working on now?

All the projects we are working on can be browsed [here](https://crossref.atlassian.net/jira/dashboards/10010). We also have a list of [Labs ideas](https://crossref.atlassian.net/jira/dashboards/10012).

A few current highlights are:


* Measuring how good our community is at preservation.  
* [Playing around with how we might evolve our Participation Reports](https://prep.labs.crossref.org).
* Looking at running a "labs" version of our API where we can experiment with exposing new functionality and metadata in order to get quick feedback from the community.
* Conversations about how we might crowdsource retraction information.
* Looking at how we can extend the classification information we currently make available via our API across more journal titles.
* Exploring how we currently do citation matching with a view to evolving this approach i.e. making it better, more transparent and open to community contributions.
* Creating a sampling framework that can be used to extract samples of works and make them publicly available.
* Building POC tools to help our members and support team more easily accomplish common tasks.


{{% /labsection %}}
{{% labsection %}}

## A flavor of Labs research and experiments

- [Matching Grants registered with Crossref to other research outputs](/blog/follow-the-money-or-how-to-link-grants-to-research-outputs/): read Dominika's blog to explain the methodology we used to match grants to other works registered in Crossref, using grant identifier metadata.
- [Setting up our test journal on Open Journal Systems (OJS)](https://sandbox.publicknowledgeproject.org/index.php/jpc/issue/view/49): Many of our members use OJS so having a test site helps us help them with queries and our own testing.
- [DOI Popup Proof of Concept](http://crossref.github.io/doi-popup/): what would it look like if we displayed Crossref metadata in a handy work-level popup?
- [DOI Chronograph](http://chronograph.labs.crossref.org/): which websites do people come from when they click on a DOI?
- [Live DOI event stream](http://live.eventdata.crossref.org/live.html): Hypnotising. A a demo designed to exhibit the stream of data that is flowing through Crossref Event Data at any given time.
- [Funder Registry reconciliation service](/labs/fundref-reconciliation-service/): designed to help members (or anybody) more easily clean-up their funder data and map it to the [Funder Registry](/services/funder-registry/).
- [pdfmark](/labs/pdfmark/) and [pdfstamp](/labs/pdfstamp/): Open source command line tools to add Crossref metadata to a PDF (pdfmark) and to automate the application of linked images to PDFs (pdfstamp). These never graduated from Labs so we aren't supporting their active maintenance and development. If you're a keen user however, [let us know](mailto:labs@crossref.org).
- Open source search-based reference matchers [Java version](https://github.com/CrossRef/search-based-reference-matcher)/[Python version](https://github.com/CrossRef/reference-matching-evaluation).
- Continuing adventures in reference matching:
  - [Comparing approaches](/blog/matchmaker-matchmaker-make-me-a-match/)
  - [Adding unstructured reference strings](/blog/reference-matching-for-real-this-time/)
  - [And structured references](/blog/what-if-i-told-you-that-bibliographic-references-can-be-structured/)
  - [Automatic citation style classifier](/blog/whats-your-citations-style/)
- [Detective work on duplicate DOIs](/blog/double-trouble-with-dois/)

{{% /labsection %}}
{{% labsection %}}

## Documents

- [Recommendations on RSS Feeds for Scholarly Publishers](/wp/labs/whitepapers/rss-best-practice)
- [Unixref Reference (the schema used in Crossref OpenURL results)](https://data.crossref.org/reports/help/schema_doc/4.4.2/index.html)
- [Disambiguation without de-duplication: Modeling authority and trust in the ORCID system](/wp/labs/whitepapers/disambiguation-deduplication-wp-v4.pdf)

{{% /labsection %}}
{{% labsection %}}

## Graduated to production services (or became part of them)

- [Crossref Metadata Search](http://search.crossref.org)
- [PatentCite](/labs/patentcite/)
- [LicenseRef](https://www.niso.org/schemas/ali/1.0)  
- [DOI content negotiation](http://www.crosscite.org/)
- [Text and data mining services](/education/retrieve-metadata/rest-api/text-and-data-mining/)
- [Citation formatting](/labs/citation-formatting-service/)
- [Randoim](/labs/randoim/)
- [TOI DOI](/labs/toi-doi-i-e-short-dois/)
- [Crossref Contributor ID](/labs/contributor-id/)
- [Reverse domain lookup](/labs/reverse-domain-lookup/)
- [Funder Registry widget](/labs/fundref-widget/)
- [Linking data and publications](http://www.scholix.org/)
- [Crossref REST API](https://api.crossref.org)  
- [Member participation browser](http://browse.crossref.org/members/)
- Lots of tools and demos that became Event Data. Read on...

Once upon a time, [PLOS](https://plos.org/) ran a project called [Article Level Metrics (ALM)](https://theplosblog.plos.org/2021/03/a-farewell-to-alm-but-not-to-article-level-metrics/). It worked well for them internally and had garnered interest and support from other organizations. It had enough potential that we decided to pick it up as a Labs project. The idea was to collect information from various online sources mentioning their DOIs, a version of altmetrics.

The project had a couple of aims:
* Scale effectively beyond one publisher to all Crossref DOIs.
* Work out what would be needed to create a production version of such a tool.  

The output was intended to be open data to provide context around outputs, while avoiding creating yet another set of metrics. If successful, it had the potential to centralise collection of this kind of data, providing significant efficiencies to publisher and sources. Read more about the early stages of the project [here](/blog/many-metrics-such-data-wow/).

The initial experimenting proved successful and by Spring 2014 we were in a position to run a pilot with the cooperation of a number of organisations. The project had become the DOI Event Tracker (DET), which built on Lagotto, the successor of ALM. Read about the DET pilot [here](/blog/det-poised-for-launch/).

DET was now ready to be passed over to the production team and in 2017 entered a [beta phase](/blog/event-data-enters-beta/).

It has continued as [Crossref Event Data](/services/event-data/), with over 800 million events collected (up until October 2021) and available via a public API.

{{% /labsection %}}
{{% labsection %}}

## Retired

- [QR Code generator](/labs/qr-code-generator/)
- [WordPress & Moveable Type plugins](/labs/wordpress-moveable-type-plugins/)
- [Ubiquity plugin](http://labs.crossref.org/ubiquity-plugin/)
- [Linked periodical data](/labs/linked-periodical-data/)
- [InChI lookup](/labs/inchi-lookup/)
- [Family name detector](/labs/family-name-detector/)
- [pmid2doi](/labs/pmid2doi/)
- [PDF-extract](/labs/pdfextract/)
- [Taxonomy interest group](http://taxonomies.labs.crossref.org/)
- [URL to DOI](https://github.com/CrossRef/event-data-reverse)

{{% /labsection %}}

{{% labsection %}}

## Labs Group <a id="strategic-initiatives-group">

- [Esha Datta](/people/esha-datta/)
- [Paul Davis](/people/paul-davis/)
- [Martin Eve](/people/martin-eve/)
- [Dominika Tkaczyk](/people/dominika-tkaczyk/)

## Alumni

- [Rachael Lammey](/people/rachael-lammey/)
- Karl Ward
- [Joe Wass](/people/joe-wass/)


{{% /labsection %}}
