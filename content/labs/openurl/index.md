---
title: OpenURL
author: Crossref
date: 2013-01-22

---

{{% labsection %}}
First, [Get an API Key][1]. It takes no time at all. Just fill in the form and we will activate a key for you. Note that we do this because occasionally we need to get in touch with somebody if their scripts start misbehaving and are hammering our systems. The alternative is to simply block the script- and we wouldn’t want to do that, right?

In the following examples you need to substitute API_KEY with the key that you get from us.

Often you just want to take an existing Crossref DOI and lookup the metadata for it. For that you want to use our OpenURL query. So, if you have the Crossref DOI:

```
10.3998/3336451.0009.101
```

You can simply construct a URL like this:

```
http://www.crossref.org/openurl/?id=doi:10.3998/3336451.0009.101&noredirect=true&pid=API_KEY&format=unixref
```

And you will get an XML representation of the metadata. Unless, of course, you forgot to substitute the API_KEY in the URL above with the one that you activated in the first step&#8230; 😉

The OpenURL query interface to Crossref is capable of much more than just DOI lookups. You can visit [our documentation][2] to get more detailed examples of fielded searches, etc.

 [1]: https://apps.crossref.org/requestaccount/
 [2]: /education/retrieve-metadata/openurl/

{{% /labsection %}}
