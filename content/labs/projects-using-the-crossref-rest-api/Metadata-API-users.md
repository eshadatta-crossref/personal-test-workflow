+++
title = "REST API users"
date = "2014-11-03"
draft = false
author = "Karl Ward"
Weight = 3
+++

{{% labsection %}}
## Open Tree of Life (NSF)

  * <http://blog.opentreeoflife.org/>
  * Maintained by: Jim Allman @jimallman
  * Using: Work queries
  * https://pbs.twimg.com/media/B2bYcKWIUAETSf2.png
{{% /labsection %}}
{{% labsection %}}
## Enriched Biodiversity

  * <http://bdj.pensoft.net/articles.php?id=1125>
  * <span class="attachment_author_subname">@rmounce</span>
{{% /labsection %}}
{{% labsection %}}
## Journalhub showcase journal

  * <http://journalhub.io/journals/acta-dermatovenerol-apa>
  * Maintained by: Jure
  * Using: Citation linking and json DOI query
  * Also elife lens and metypeset
{{% /labsection %}}
{{% labsection %}}
## PLOS<span style="font-size: 1.285714286rem; line-height: 1.6;"> Extensions to Wikimedia Visual Editor</span>

  * <http://cdn.substance.io/ve/>
  * ****Maintained by:**** <http://www.adamhyde.net/plos/>
  * ****Using:**** Work queries
{{% /labsection %}}
{{% labsection %}}
## Kudos

  * <http://growkudos.com>
  * ****Maintained by:**** Lou Peck, David Sommer, Leigh Dodds
  * **Using:** Work queries
{{% /labsection %}}
{{% labsection %}}
## doimgr

  * <https://github.com/dotcs/doimgr>
  * ****Maintained by:**** dotcs
  * ****Using:**** Work queries, work filtering
{{% /labsection %}}
{{% labsection %}}
## ****bibby****

  * <https://github.com/jdherman/bibby>
  * ****Maintained by:**** Jon Herman, Cornell
  * ****Using:**** Conneg, work queries
{{% /labsection %}}
{{% labsection %}}
## CHORUS Search

  * <http://search.chorusaccess.org>
  * ****Maintained by:**** propulsion.io
  * **Using:** Funder work queries, filters, faceting
{{% /labsection %}}
{{% labsection %}}
## CHORUS Dashboards

  * <http://dashboard.chorusaccess.org>
  * ****Maintained by:**** propulsion.io
  * **Using:** Funder work queries, filters
{{% /labsection %}}
{{% labsection %}}
## Crossref Search

  * [http://search.crossref.org][1]
  * ****Maintained by:**** Karl Ward
{{% /labsection %}}
{{% labsection %}}
## Crossref FundRef Search

  * <http://search.crossref.org/funding>
  * **Maintained by:** Karl Ward
{{% /labsection %}}
{{% labsection %}}
## PLoS Enhanced Citations Experiment

  * ****Maintained by:**** Adam Becker, PLoS
  * **Using:** Work queries and filters
{{% /labsection %}}
{{% labsection %}}
## PLoS ALM / Crossref DET****

****

  * <http://det.labs.crossref.org>
  * ****Maintained by:**** Martin Fenner
  * **Using:** Work queries, update and publication date filters
{{% /labsection %}}
{{% labsection %}}
## PKP OJS Crossref Plugin

  * ****Maintained by:**** Juan, James, Bozana, PKP
  * **Using:** deposits (xml deposits)
{{% /labsection %}}
{{% labsection %}}
## Crossref Crossmark Statistics

  * ****Maintained by:**** Joe Wass
  * **Using:** Work queries, date filters, significant update filters, update policy filter
{{% /labsection %}}
{{% labsection %}}
##  ****Crossref Metadata Participation****

  * ****Maintained by:**** Joe Wass
  * **Using:** Publisher routes, publisher feature coverage values
{{% /labsection %}}
{{% labsection %}}
## ****pdfextract****

  * ****Maintained by:**** Karl Ward
  * **Using:** Work queries, work metadata transforms
{{% /labsection %}}
{{% labsection %}}
## ****Cambia patent to scholarly literature citation matching****

  * ****Maintained by:**** Doug Ashton
  * **Using:** Work queries, deposits (patent citation deposits)

&nbsp;

&nbsp;

&nbsp;
{{% /labsection %}}
{{% labsection %}}
 [1]: http://dashboard.chorusaccess.org

{{% /labsection %}}
