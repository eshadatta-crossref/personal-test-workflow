+++
title = "Metadata 2020"
date = "2017-01-28"
draft = true
author = "Ginny Hendricks"
rank = 4
parent = "Get involved"
weight = 9
+++

## Connect the dots

Metadata 2020 is an advocacy campaign led by several thought leaders in the scholarly community. It aims to motivate publishers to supply richer metadata so that scholarly content is even more discoverable. Better metadata is a shared endeavor, and the campaign goal is to educate all partners in publishing, from journals to technology providers, and preliminary research indicates widespread support for an initiative of this kind. Now is the time to translate this "joined-up thinking" into action. The goals of Metadata 2020 are to:

- Raise awareness of the importance of richer metadata.
- Educate the community on the role of metadata in making scholarly content discoverable.
- Encourage publishers to make a public commitment to submit all of the metadata relevant to their content.
- Equip all stakeholders with the tools they need to comply with agreed thresholds.

Join the metadata movement at the [Metadata 2020](http://www.metadata2020.org/) site.
