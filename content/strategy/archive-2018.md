+++
title = "Strategic roadmap 2018-2020"
date = "2019-10-31"
draft = false
author = "Ginny Hendricks"
parent = "Strategic agenda"
weight = 4
rank = 5
+++

{{% divwrap darkgrey-highlight %}}
### This is our strategic agenda from 2018-2020 and it's now archived, please visit the main [strategy page](/strategy) for the most up-to-date version.
{{% /divwrap %}}

<br>

Crossref makes research objects easy to find, cite, link, assess, and reuse.

We’re a not-for-profit membership organization that exists to make scholarly communications better.  We **rally** the community; **tag** and share metadata; **run** an open infrastructure; **play** with technology; and **make** tools and services---all to help
put scholarly content in context.

It’s as simple---and as complicated---as that.


{{% keyword "rally" %}}
{{% keyword "tag" %}}
{{% keyword "run" %}}
{{% keyword "play" %}}
{{% keyword "make" %}}

&nbsp;

{{% strategy-section wrapper %}}

{{% strategy-section %}}

## The strategic landscape

Scholarly communications is changing, and putting research outputs into context is becoming more complicated. Our membership is part of a community that values and exchanges metadata between themselves as well as with a broader community.

Some of our existing members no longer classify themselves as “publishers”, and some of our newer members have never classified themselves as “publishers”. Governments, funders, institutions, and researchers—parties who once had tangential involvement in scholarly publishing—are taking a more direct role in shaping how research is registered, certified and disseminated. Additionally, low income (but emerging) countries increasingly see it as a strategic imperative that they own and manage a research communication system that reflects their regional research priorities.

Researchers are increasingly insisting that new kinds of research outputs, like data, software, preprints, and peer reviews form a critical part of the scholarly record. New players (e.g. sharing networks, alt-metrics services, and Current Research Information Systems) are becoming critical elements of the research landscape. New technologies like ML and AI promise to change the way in which research is produced, assessed, and consumed.

For Crossref and its membership to stay sustainable in this new environment, we need to adapt, do and encourage new things. But we have limited resources. So in order to adapt and do new things, we also need to also make sure that we are currently doing the right things efficiently. Hence, our strategic roadmap is a combination of consolidation and expansion:


{{% strategy-table table landscape %}}

{{% strategy-table row %}}

{{% strategy-table cell %}}

### [Simplify and enrich existing services](#simplify-and-enrich-existing-services)

{{% /strategy-table %}}

{{% strategy-table cell %}}

### [Adapt to expanding constituencies](#adapt-to-expanding-constituencies)

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% strategy-table row %}}

{{% strategy-table cell %}}

### [Improve our metadata](#improve-our-metadata)

{{% /strategy-table %}}

{{% strategy-table cell %}}

### [Collaborate and partner](#collaborate-and-partner)

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% /strategy-section %}}


{{% strategy-section style="simplify" %}}

{{% divwrap service-yellow %}}
##  Simplify and enrich existing services
{{% /divwrap %}}

The characteristics of our members and users continue to diversify—to scholar publishers, library publishers, and other emerging organizations. Furthermore, the use of our APIs has grown significantly in recent years as Crossref becomes better known as a source of metadata. Users are therefore asking for a more predictable service-based option in addition to the public options. We have and will continue to develop service-level guarantees in order to meet this growing demand, which will strengthen Crossref's position as a way for the wider community to centrally access information from 10,000+ publishers.

A focus on user experience will allow us to make it easier for all of them to participate in Crossref as fully as possible, irrespective of their depth of need or their level of technical skill.

We are also focusing our efforts on ensuring there is broad support for systems in accessing Crossref metadata so that reuse reaches its fullest potential across the entire research ecosystem. This necessary evolution of Crossref services will ensure that we can support the changing needs and priorities of all involved in research.

We do not want to add resources infinitum so we must make sure that we are performing our existing functions efficiently. To this end, we are streamlining processes to improve member experience, modernize infrastructure, and upgrade tools and data provision capabilities. These activities will achieve efficiencies for members, metadata users, as well as staff.

{{% strategy-table table %}}

{{% strategy-table row %}}

{{% strategy-table cell %}}

### Recently completed
* Similarity Check service transition
* Metadata Manager for journal articles
* Reference-matching improvements (phase 1)
* Transition from GitHub and Jira to GitLab


{{% /strategy-table %}}

{{% strategy-table cell %}}

### In focus
* Pending publication (in Beta)
* Event Data
* Incident response process refinements
* Automated monitoring & status updates
* Support documentation re-write and migration to website
* API ElasticSearch migration
* Enhanced JATS support
* DevOps automation
* Research Organizations Registry (ROR)

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% strategy-table row %}}

{{% strategy-table cell %}}

### Scheduled

#### 2020
* REST API improvements
* Similarity Check v2
* Address technical debt

#### Pending
* Metadata Plus sync

* Cloud migration for Content Registration infrastructure
* Crossmark reports
* Consolidated Member Center
* Self-repairing DOIs
* Joint DataCite & Crossref Search (with FREYA)
* Standard Crossref DOI display/status widget

{{% /strategy-table %}}

{{% strategy-table cell %}}

### R&D
* Image manipulation detection
* Auto-classification of journal types
* Citation classification

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% /strategy-section %}}


{{% strategy-section style="improve" %}}

{{% divwrap service-red %}}
## Improve our metadata
{{% /divwrap %}}

Metadata provided by our members is the foundation of all our services. Crossref membership is a collective benefit. The more metadata a member is able to put in—and the greater adherence to best practice—the easier it is for other members and community users downstream to find, cite, link, assess, and reuse their content. Furthermore, the more discoverable and more trusted is the content. Better quality metadata improves the system for each member and all of Crossref's other members and stakeholders.

Existing Crossref members may have joined Crossref when only providing minimal bibliographic metadata was required for reference linking. But, increasingly, Crossref is becoming a hub which the community relies on to get both complete bibliographic metadata and non-bibliographic metadata (e.g. funding information, license information, clinical trial information, etc.) We need to help our existing members meet the new metadata expectations. Our objectives are to better communicate what metadata best practice is, equip members with all the data and tools they need to meet best practice and achieve closer cooperation from service providers.

We will focus on expanding the links between scholarly objects to all their associated research outputs. We will also expand support for new content types to ensure that they integrated into the scholarly record and can be discovered. At the other extreme, some new Crossref members have little technical infrastructure for creating and maintaining quality metadata. We need to help provide them with tools to ensure that we don’t dilute the Crossref system with substandard and/or incomplete metadata.

But metadata quality is a strategic focus across the entire Crossref membership. While we improve this across our entire membership by implementing stronger validation measures internally in our deposit processes, we will also employ mechanisms that engage the broader community to fill in gaps and correct metadata with a clear provenance trail of every metadata assertion in the Crossref system.


{{% strategy-table table %}}

{{% strategy-table row %}}

{{% strategy-table cell %}}

### Recently completed
* Metadata Manager for journal articles
* Reference-matching improvements (phase 1)
* Improvements to OJS integration (with PKP)
* Research [grants](/education/content-registration/content-types-intro/grants/) deposit

{{% /strategy-table %}}

{{% strategy-table cell %}}

### In focus
* Metadata 'health checks'
* Support documentation re-write and migration to website
* Research Organizations Registry (ROR) support
* Data citations
* Improving JATS support
* Research [grants](/education/content-registration/content-types-intro/grants/) retrieval
* Conference IDs
* Metadata Practitioners Interest Group


{{% /strategy-table %}}

{{% /strategy-table %}}

{{% strategy-table row %}}

{{% strategy-table cell %}}

### Scheduled

#### 2020
* Metadata schema enhancements
* Multiple resolution improvements (& decommission co-access)

#### Pending
* Metadata Principles and Best Practices
* New Service Providers program
* Emerging Publisher Education Coalition
* Crossmark reports
* Revised relations taxonomy
* Improvement for bulk updates of metadata
* Standard Crossref DOI display/status widget

{{% /strategy-table %}}

{{% strategy-table cell %}}

### R&D
* Participation reports (phase 2)
* Automating metadata extraction, preflight checking
* Metadata profiling
* Public feedback channel for metadata quality issues

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% /strategy-section %}}

{{% strategy-section style="adapt" %}}

{{% divwrap service-blue %}}
## Adapt to expanding constituencies
{{% /divwrap %}}

Members are at the heart of the Crossref community. Scholarly publishers are geographically expanding at a rapid pace and we currently have members in 140 countries. With that comes the need to increasingly and proactively work with emerging regions as they start to share research outputs globally. To this end, we will expand our geographic support through concerted efforts in international outreach, working with government education/science ministries and local Sponsors and Ambassadors, and developing as much localized content as we can.

Furthermore, funders and research institutions are increasingly involved in the scholarly publishing process. As the research landscape changes, we need to respond and ensure our relevance by evolving in a way that better reflects these shifts. Our overarching objective is to expand our value proposition to convince these new constituents of Crossref’s relevance, getting them into our system and using our infrastructure.

{{% strategy-table table %}}

{{% strategy-table row %}}

{{% strategy-table cell %}}

### Recently completed

{{% /strategy-table %}}

{{% strategy-table cell %}}

### In focus
* Sponsors program
* LIVE local educational events
* Research managers outreach
* Forum introduction (community.crossref.org)
* Ambassador program
* Multi-language webinars

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% strategy-table row %}}

{{% strategy-table cell %}}

### Scheduled

#### 2020
* Funder outreach
* Emerging Publisher Education Coalition
* Law journals

#### Pending
* Non-English language documentation
* Non-English language interfaces
* DOI linking in mainstream media

{{% /strategy-table %}}

{{% strategy-table cell %}}

### R&D

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% /strategy-section %}}



{{% strategy-section style="selectively" %}}

{{% divwrap service-darkgrey %}}
## Collaborate and partner
{{% /divwrap %}}

Crossref faces a tension. We want to—where possible—take advantage of existing organizations, services, tools and technologies. We aim to do more, more efficiently, by focusing on expanding existing infrastructure and organizations rather than creating things from scratch. We don’t want to reinvent the wheel.

So that our alliances with others have the greatest impact, we align our strategic plans for scholarly infrastructure with others, and ensure that the community has the most up-to-date and accurate information.

This is part and parcel of our role as an community-wide infrastructure provider as we achieve our mission by supporting the entire research ecosystem. But at the same time, we take care not to introduce risky dependencies for the entire community. Hence, the bulk of our collaborations are with open initiatives.

Some are led and driven by Crossref. Others are not.

{{% strategy-table table %}}

{{% strategy-table row %}}

{{% strategy-table cell %}}

### Recently completed
* ROR Registry launch

{{% /strategy-table %}}

{{% strategy-table cell %}}

### In focus
* Value proposition for DOI Foundation
* Persistent identifier infrastructure through FREYA project
* Advocacy for richer metadata through Metadata 2020
* Use of persistent identifiers in references with Wikimedia
* Research Organizations Registry (ROR) with Digital Science, CDL, and DataCite
* Distributed usage logging (DUL) with COUNTER
* Data citation with Scholix, RDA, STM Association, DataCite, and Make Data Count
* OJS development with Public Knowledge Project
* Open Funder Registry with Elsevier
* Similarity Check with Turnitin
* Joint value proposition with DataCite
* Foundational infrastructure with ORCID and DataCite
* PIDapalooza festival of open persistent identifiers

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% strategy-table row %}}

{{% strategy-table cell %}}

### Scheduled

#### 2020

#### Pending
* Emerging Publisher Education Coalition with DOAJ, COPE, and INASP
* Joint search with DataCite

{{% /strategy-table %}}

{{% strategy-table cell %}}

### R&D
* DOIs for static website generators
* Reference implementation for open platforms

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% /strategy-table %}}

{{% /strategy-section %}}

{{% /strategy-section %}}
