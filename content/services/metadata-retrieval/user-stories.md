+++
title = "Who relies on Crossref metadata?"
date = "2019-02-24"
draft = false
author = "Ginny Hendricks"
rank = 5
parent = "Metadata retrieval"
weight = 3
tags = ["Metadata", "APIs", "Metadata retrieval"]
+++

Over 100 million unique scholarly works are distributed into systems across the research enterprise 24/7 via our metadata APIs, at a rate of around 607 million queries a month. This is a collection of brief user stories from some of the people who rely on Crossref metadata.

## We use Crossref metadata to…

{{% quotecite "Gavin Reddick, Researchfish" %}}
Add funding info from publications to researchers’ portfolios, and report the publications as arising from the grant; to validate the data provided by universities; and we use the license and embargo period information to help understand the open access status of publications.
{{% /quotecite %}}

{{% quotecite "Polina Grinbaum, PLOS" %}}
Link references on our journal platforms, pull citations statistics for our Article-Level Metrics and ensure we are publishing unique science. Crossref metadata is vital to our everyday operations and the discovery of the research we publish.
{{% /quotecite %}}

{{% quotecite "Ulf Kronman, National Library of Sweden" %}}
Enhance and correct the metadata delivered to us, just with a correct DOI.
{{% /quotecite %}}

{{% quotecite "Rob O’Donnell, Rockefeller University Press" %}}
Verify and correct references, retrieve Funder Registry IDs, and include Cited-by links in published content, and so much more. As a small publisher, discoverability is of utmost importance, and Crossref is a discoverability hub. The inclusion of metadata in Crossref strengthens the content we publish.
{{% /quotecite %}}

{{% quotecite "Christian Herzog, Daniel Hook, Simon Porter, Dimensions, Digital Science" %}}
Know about our possible ‘universe’ of articles.
{{% /quotecite %}}

{{% quotecite "Damian Pattinson, ResearchSquare" %}}
Connect authors with research articles similar to their own, and help them decide where to submit their manuscripts for the best chance of success.
{{% /quotecite %}}

{{% quotecite "Michael Parkin, Europe PMC" %}}
Index preprints alongside traditional journal publications and we plan to: provide another means to access and discover preprints; help explore the role of preprints in the publishing ecosystem; support their inclusion in processes such as grant reporting and credit attribution systems.
{{% /quotecite %}}

{{% quotecite "Cameron Neylon, Centre for Culture and Technology, Curtin University" %}}
Look across the world at research outputs and understand how institutions and communities are making them more accessible. We're using Crossref metadata as the central reference point to handle objects from different sources and to have a consistent set of metadata on things like publication date that covers the full set.
{{% /quotecite %}}

{{% quotecite "Alexander Naydenov, PaperHive" %}}
Allow researchers to immediately interact with the readers of their works, if the publishers provide ORCID iDs of authors in their Crossref metadata.
{{% /quotecite %}}

{{% quotecite "Elizabeth Hull, Dryad" %}}
Check for the availability of articles associated with our data packages, and to verify some of our metadata. Being able to do this programmatically has revolutionized our data publication workflow.
{{% /quotecite %}}

{{% quotecite "David Sommer, Kudos" %}}
Retrieve Cited-by counts for a DOI so we can include them as part of the ‘basket of metrics’ we provide to our researchers. They can then understand the performance of their publications in context, and see the correlation between actions and results.
{{% /quotecite %}}

{{% quotecite "Alberto Pepe, Authorea" %}}
Enable authors to search and add references to their papers.
{{% /quotecite %}}

{{% quotecite "Mark Hahnel, Figshare" %}}
Enrich the metadata of our hosted preprints and link them to the manuscript or version of record. We also perform various analyses regarding the completeness and discoverability of our records.
{{% /quotecite %}}

{{% quotecite "Henning Schoenenberger, Springer Nature" %}}
Streamline research communication in a global, interconnected and evolving scholarly domain in order to support researchers even better and facilitate research in general.
{{% /quotecite %}}

{{% quotecite "Craig van Dyck, CLOCKSS" %}}
Identify articles unambiguously as we preserve scholarly literature in our system. Also, in the rare instances when we ‘trigger’ journals for open access, we want the reference-linking functionality to work, and we work with Crossref to point the URLs to our site for resolution.
{{% /quotecite %}}

{{% quotecite "Jason Priem, ImpactStory" %}}
Help Unpaywall identify and disambiguate over 20 million Open Access articles, and it is absolutely essential to our work. We 😍❤️🤗 Crossref!
{{% /quotecite %}}

---

You don't have to sign up to anything in order to use our REST API. That means we don't necessarily know who is using it, although we see millions of hits every day. If you are using it in your projects and would like to share, please [let us know](mailto:feedback@crossref.org) and we'll feature you on this page.
