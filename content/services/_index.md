+++
title = "Find a service"
date = "2021-04-09"
draft = false
image = "/images/banner-images/climbing-wall.jpg"
rank = 4
[menu.main]
weight = 2
+++

## Metadata enables connections

We offer a wide array of services to ensure that scholarly research metadata is registered, linked, and distributed. When members register their content with us, we collect both bibliographic and non-bibliographic metadata. We process it so that connections can be made between publications, people, organizations, and other associated outputs. We preserve the metadata we receive for the scholarly record. We also make it available across a range of interfaces and formats so that the community can use it and build tools with it.

Some of our services are only available to members - most of these services are included your membership but some involve an extra fee (marked \*). We also offer a range of services free of charge to the scholarly community, with the option of premium service versions at an extra charge (marked ~).

* Metadata retrieval (including Metadata Plus~)*
* Content Registration
* Reference linking
* Cited-by
* Funder Registry
* Similarity Check\*
* Crossmark
* Event Data

## Overview of our services<a id='00252' href='#00252'><i class='fas fa-link'></i></a>

|Crossref service |Do you need to be a member? |About the service and how it benefits you|
|:--|:--|:--|
|[Metadata retrieval](/services/metadata-retrieval/)|No | The collective power of our members’ metadata is available to use through a variety of tools and APIs—allowing anyone to search and reuse the metadata in sophisticated ways.<br>**Read the factsheet in your language:** <a href="/pdfs/about-metadata-retrieval.pdf" target="_blank">English</a>, <a href="/pdfs/about-metadata-retrieval-spanish.pdf" target="_blank">español</a>, <a href="/pdfs/about-metadata-retrieval-arabic.pdf" target="_blank">العربية</a>, <a href="/pdfs/about-metadata-retrieval-indonesian.pdf" target="_blank">bahasa Indonesia</a>, <a href="/pdfs/about-metadata-retrieval-portuguese.pdf" target="_blank">português do Brasil</a>|
|[Metadata Plus](/services/metadata-retrieval/metadata-plus/)|No | Metadata Plus gives you enhanced access to all our supported APIs, guarantees service levels and support, and additional features such as snapshots and priority service/rate limits.|
|[Content Registration](/services/content-registration/)|Yes | Content Registration allows members to register and update metadata via machine or human interfaces. <br>**Read the factsheet in your language:** <a href="/pdfs/about-content-registration.pdf" target="_blank">English</a>, <a href="/pdfs/about-content-registration-spanish.pdf" target="_blank">español</a>, <a href="/pdfs/about-content-registration-arabic.pdf" target="_blank">العربية</a>, <a href="/pdfs/about-content-registration-indonesian.pdf" target="_blank">bahasa Indonesia</a>, <a href="/pdfs/about-content-registration-portuguese.pdf" target="_blank">português do Brasil</a>|
|[Reference linking](/services/reference-linking)|No | Reference linking enables researchers to follow a link from the reference list to other full-text documents, helping them to make connections and discover new things. <br>**Read the factsheet in your language:** <a href="/pdfs/about-reference-linking.pdf" target="_blank">English</a>, <a href="/pdfs/about-reference-linking-spanish.pdf" target="_blank">español</a>, <a href="/pdfs/about-reference-linking-arabic.pdf" target="_blank">العربية</a>, <a href="/pdfs/about-reference-linking-indonesian.pdf" target="_blank">bahasa Indonesia</a>, <a href="/pdfs/about-reference-linking-portuguese.pdf" target="_blank">português do Brasil</a>|
|[Cited-by](/services/cited-by)|Yes | Cited-by shows how work has been received by the wider community; displaying the number of times it has been cited, and linking to the citing content. <br>**Read the factsheet in your language:** <a href="/pdfs/about-cited-by.pdf" target="_blank">English</a>, <a href="/pdfs/about-cited-by-spanish.pdf" target="_blank">español</a>, <a href="/pdfs/about-cited-by-arabic.pdf" target="_blank">العربية</a>, <a href="/pdfs/about-cited-by-indonesian.pdf" target="_blank">bahasa Indonesia</a>, <a href="/pdfs/about-cited-by-portuguese.pdf" target="_blank">português do Brasil</a>|
|[Funder Registry](/services/funder-registry)|No | The Funder Registry and associated funding metadata allows everyone to have transparency into research funding and its outcomes. It’s an open and unique registry of persistent identifiers for grant-giving organizations around the world.<br>**Read the factsheet in your language:** <a href="/pdfs/about-funder-registry.pdf" target="_blank">English</a>, <a href="/pdfs/about-funder-registry-spanish.pdf" target="_blank">español</a>, <a href="/pdfs/about-funder-registry-arabic.pdf" target="_blank">العربية</a>, <a href="/pdfs/about-funder-registry-indonesian.pdf" target="_blank">bahasa Indonesia</a>, <a href="/pdfs/about-funder-registry-portuguese.pdf" target="_blank">português do Brasil</a>|
|[Similarity Check](/services/similarity-check)|Yes | A service provided by Crossref and powered by iThenticate—Similarity Check provides editors with a user-friendly tool to help detect plagiarism.<br>**Read the factsheet in your language:** <a href="/pdfs/about-similarity-check-021422.pdf" target="_blank">English</a>, <a href="/pdfs/about-similarity-check-spanish.pdf" target="_blank">español</a>, <a href="/pdfs/about-similarity-check-arabic.pdf" target="_blank">العربية</a>, <a href="/pdfs/about-similarity-check-indonesian.pdf" target="_blank">bahasa Indonesia</a>, <a href="/pdfs/about-similarity-check-portuguese.pdf" target="_blank">português do Brasil</a>|
|[Crossmark](/services/crossmark)|Yes | The Crossmark button gives readers quick and easy access to the current status of an item of content, including any corrections, retractions, or updates to that record.<br>**Read the factsheet in your language:** <a href="/pdfs/about-crossmark.pdf" target="_blank">English</a>, <a href="/pdfs/about-crossmark-spanish.pdf" target="_blank">español</a>, <a href="/pdfs/about-crossmark-arabic.pdf" target="_blank">العربية</a>, <a href="/pdfs/about-crossmark-indonesian.pdf" target="_blank">bahasa Indonesia</a>, <a href="/pdfs/about-crossmark-portuguese.pdf" target="_blank">português do Brasil</a>|
|[Event Data](/services/event-data)|No | When someone links their data online, or mentions research on a social media site, we capture that Event and make it available for anyone to use in their own way. We provide the unprocessed data—you decide how to use it. |

## Register

>Rich metadata leads to greater discoverability

The more complete the metadata registered, the more accurate the view of the scholarly record and the more discoverable the content is to the scholarly community.

Through our [content registration](/services/content-registration) service, members register and maintain metadata for their content. We are interested in the full range of metadata for each publication, including information on:

* contributors (such as authors, editors, reviewers)
* funding (such as funding body, grant number)
* publication history (such as versions, updates, revisions, corrections, retractions, dates)
* peer review (such as status, type, reviews)
* access indicators (such as publication license, text & data mining URLs, machine mining URLs)
* related resources & associated research artifacts (such as preprints, figures & tables, datasets, software, protocols, research resource IDs).

Unique identifiers for authors, organizations, and associated scholarly outputs enhance precision and quality, members can deposit accurate funder acknowledgment metadata when they apply the unique funder identifier in Crossref’s [Funder Registry](/services/funder-registry) service, a regularly updated, industry-standard taxonomy of grant-giving organizations.

## Link

>Linking improves the scholarly enterprise

The complete set of scholarly links spans time, geography, and disciplinary boundaries.

We connect all the metadata elements we can accurately identify, from all phases of publication, across content records in our vast corpus. We link literature to people, literature to resources and associated research artifacts, and soon, literature to the activity surrounding the publication. Amongst the vast web of links, we connect research output content to external tools such as Turnitin’s iThenticate in the [Similarity Check](/services/similarity-check/) service, assisting members in plagiarism detection. With the references deposited by members, Crossref offers a [Cited-by](/services/cited-by/) service so that participating members can discover all the publications that have cited their content. The upcoming [Event Data](/services/event-data/) service will offer links between literature and various platforms where it is shared, discussed, mentioned, referenced, reviewed, and considered.

## Retrieve

>Metadata is meant to be used

Crossref delivers metadata to systems throughout scholarly communications making content easy to find, cite, link, assess, and reuse.

Our [metadata retrieval](/services/metadata-retrieval/) service supports a diverse range of systems by offering a wide range of formats and interfaces. We do this because the range of organizations who use it -- from publishers to libraries, to funders to startups--and how they use it, are diverse. Using metadata facilitates content discoverability--if it’s rich metadata, all the better. [Crossmark](/services/crossmark/) is a powerful example: Metadata is displayed on publication landing pages through the a widget that gives readers quick and easy access to the current status of an item of content. With one click, readers can see if content has been updated, corrected or retracted and access additional metadata provided by the member.
