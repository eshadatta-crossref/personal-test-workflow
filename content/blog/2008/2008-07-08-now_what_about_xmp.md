---
title: Now What About XMP?
author: thammond
authors:
  - thammond
date: 2008-07-08

categories:
  - Standards
archives:
  - 2008

---
With PDF now passed over to ISO as keeper of the format (as blogged [here][1] on CrossTech), Kas Thomas (on CMS Watch’s [TrendWatch][2]) blogs [here][3] that Adobe should now do the right thing by XMP and look to hand that over too in order to establish it as a truly open standard. As he says:

> _&#8220;Let’s cut to the chase. If Adobe wants to demonstrate its commitment to openness, it should do for XMP what it has already done for PDF: Put it in the hands of a legitimate standards body. Right now it’s open in name only. &#8220;_

And this:

> _&#8220;Adobe is pushing the XMP standard &#8230; at Adobe’s pace and in ways that benefit Adobe. (The parallels with PDF are numerous and obvious.) There are lingering technical issues waiting to be solved, however. Issues whose solutions shouldn’t have to be dependent on Adobe’s needs only.&#8221;_

He’s absolutely bang on. With XMP on the threshold of finally shining through we really could do with Adobe cutting it loose. It’s time to leave home.

 [1]: /blog/iso-standard-for-pdf/
 [2]: https://web.archive.org/web/20090703195909/http://www.cmswatch.com/Trends
 [3]: http://www.cmswatch.com/Trends/1295-PDF-now-has-a-standard-home,-but-whither-XMP?source=RSS
