---
title: 'CURIEs - A Cure for URIs'
author: thammond
authors:
  - thammond
date: 2008-12-03

categories:
  - Identifiers
archives:
  - 2008

---
A quick straw poll of a few folks at [London Online][1] yesterday revealed that they had not heard of CURIE’s. And there was I thinking that most everybody must have heard of them by now. 🙂 So anyway here’s something brief by way of explanation.

**_[CURIE][2]_** stands for **_Compact URI_** and does the signal job or rendering long and difficult to read URI strings into something more manageable. (URIs do have the particular gift of being &#8220;human transcribable&#8221; but in practice their length and the actual characters used in the URI strings tend to muddy things for the reader.) So given that the Web is built upon a bedrock of URIs, anything that then makes URIs easier to handle is going to be an important contributor to our overall ease of interaction with the Web.

(Continues)

<!--more-->



Ten years back (in February 1998) when XML was first introduced it presented a flat naming system for document markup. For purposes of modularity and markup reuse the XML Namespaces specification released the following year allowed for element and attribute names to be replaced by **expanded** names in which the hitherto simple names would be replaced by name pairs consisting of a **namespace** name and a **local** name. The use of URIs for the namespace name thus opened the doors to assigning globally unique names for XML element/attribute names. As a practical point (both to keep the names short and to deal with URI characters), the notion of a qualified name (or QName) was introduced, whereby the local name would be qualified by a prefix which stood in for the namespace name.

This was such a successful device that over time it was applied to URIs in general as a mechanism for abbreviation. Especially in RDF/XML schema elements were referenced by QName. And the practice has spilled over into non-XML syntaxes (e.g. the N3 and Turtle RDF grammars which use a &#8220;@prefix&#8221; directive). But there were problems since the device was grounded in XML the local names were constrained by allowable characters for XML elements and attributes (e.g. names cannot start with a digit character), as well as there being no specification for applying this same device to non-XML grammars.

CURIE is an initiative to generalize this notion of qualified names for URIs beyond the immediate XML context for naming elements and attributes (which would also allow their use in attribute values), to a more general use in applications beyond XML. The development of CURIE is based upon work done in the definition of [XHTML2][3], and upon work done by the [RDF-in-HTML Task Force][4], a joint task force of the [Semantic Web Best Practices and Deployment Working Group][5] and [XHTML 2 Working Group][6]. The Editor’s draft [CURIE Syntax 1.0][2] is currently a W3C Candidate Recommendation which is receiving comments through Jan 15, 2009, at which time it is intended to put it forward as a W3C Proposed Recommendation. Meantime, though, the new W3C Recommendation [RDFa Syntax in XHTML][7] (published Oct 14, 2008) has a normative section on CURIEs (see [Sect. 7][8]).

So, what do CURIEs look like? Taking a simple RDFa example for DOI we might have a fragment such as:

<pre>&lt;div xmlns:doi="https://doi.org/" xmlns:dcterms="http://purl.org/dc/terms/"&gt;
&nbsp;&nbsp;&lt;div <b>about="doi:10.1038/nature07184"</b>&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;span <b>property="dcterms:hasPart"</b> <b>resource="[doi:10.1038/nature07184]"</b>/&gt;
&nbsp;&nbsp;&lt;/div&gt;
&lt;/div&gt;</pre>

This would be processed by an RDFa processor to yield the RDF triple (in N3/Turtle):

<pre>&lt;doi:10.1038/nature07184&gt; dcterms:hasPart &lt;https://doi.org/10.1038/nature07184&gt; .</pre>

This triple (or fact) says that the resource identified by DOI 10.1038/nature07184 has as a component part (cf. [DCTERMS][9] vocabulary) the resource identified by <https://doi.org/10.1038/nature07184>. (The abstract work identified by the DOI has as a component part the splash page identified by the proxy URL.)

OK, so what’s going on? The &#8220;property&#8221; attribute takes a CURIE as value where the prefix &#8220;dcterms&#8221; is standing in for the XML namespace URI. The &#8220;about&#8221; and &#8220;resource&#8221; attributes both take a URI or CURIE as value, but because of any potential confusion a (so-called) &#8220;Safe CURIE&#8221; must be used which is a CURIE wrapped in brackets. The above example does not use brackets for the &#8220;about&#8221; attribute and therefore an RDFa processor would read this as being a full URI, i.e. &#038;lt’doi:10.1038/nature07184>, whereas it does use brackets for the &#8220;resource&#8221; attribute and therefore this would be read as being a (Safe) CURIE, i.e. <https://doi.org/10.1038/nature07184>.

We can turn this around as follows:

<pre>&lt;div xmlns:doi="https://doi.org/" xmlns:dcterms="http://purl.org/dc/terms/"&gt;
&nbsp;&nbsp;&lt;div <b>about="[doi:10.1038/nature07184]"</b>&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;span <b>property="dcterms:isPartOf"</b> <b>resource="doi:10.1038/nature07184"</b>/&gt;
&nbsp;&nbsp;&lt;/div&gt;
&lt;/div&gt;</pre>

This would be processed by an RDFa processor to yield the RDF triple (in N3/Turtle):

<pre>&lt;https://doi.org/10.1038/nature07184&gt; dcterms:isPartOf &lt;doi:10.1038/nature07184&gt; .</pre>

This triple (or fact) says that the resource identified by <https://doi.org/10.1038/nature07184> is a component part (cf. [DCTERMS][9] vocabulary) of the resource identified by DOI 10.1038/nature07184. (The splash page identified by the proxy URL is a component part of the abstract work identified by the DOI.)

So what do CURIEs give us? Nothing more than a generic means to be able to make human-friendly statements such as

<pre>&lt;doi:10.1038/nature07184&gt; dcterms:hasPart doi:10.1038/nature07184 .</pre>

instead of having to spell it out in full triples form using long-winded URIs:

<pre>&lt;doi:10.1038/nature07184&gt;
&nbsp;&nbsp;&lt;http://http://purl.org/dc/terms/hasPart&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;https://doi.org/10.1038/nature07184&gt; .</pre>

 [1]: https://web.archive.org/web/20081218013716/http://www.online-information.co.uk/index.html
 [2]: http://www.w3.org/MarkUp/2008/ED-curie-20081023/
 [3]: http://www.w3.org/TR/xhtml2
 [4]: http://www.w3.org/2001/sw/BestPractices/HTML/
 [5]: http://www.w3.org/2001/sw/BestPractices/
 [6]: http://www.w3.org/MarkUp/
 [7]: http://www.w3.org/TR/rdfa-syntax/
 [8]: http://www.w3.org/TR/rdfa-syntax/#s_curies
 [9]: http://dublincore.org/documents/dcmi-terms/
