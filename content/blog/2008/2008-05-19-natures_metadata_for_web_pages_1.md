---
title: 'Nature’s Metadata for Web Pages'
author: thammond
authors:
  - thammond
date: 2008-05-19

categories:
  - Metadata
archives:
  - 2008

---
Well, we may not be the first but wanted anyway to report that Nature has now embedded metadata ([HTML meta tags][1]) into all its newly published pages including full text, abstracts and landing pages (all bar four titles which are currently being worked on). Metadata coverage extends back through the Nature archives (and depth of coverage varies depending on title). This conforms to the W3C’s Guideline 13.2 in the [Web Content Accessibility Guidelines 1.0][2] which exhorts content publishers to &#8220;provide metadata to add semantic information to pages and sites&#8221;.

Metadata is provided in both DC and PRISM formats as well as in Google’s own bespoke metadata format. This generally follows the [DCMI recommendation][3] &#8220;_Expressing Dublin Core metadata using HTML/XHTML meta and link elements_, and the earlier [RFC 2731][4] &#8220;_Encoding Dublin Core Metadata in HTML&#8221;_. (Note that schema name is normalized to lowercase.) Some notes:

  * The DOI is included in the &#8220;`dc.identifier`&#8221; term in URI form which is the Crossref recommendation for citing DOI.
      * We could consider adding also &#8220;`prism.doi`&#8221; for disclosing the native DOI form. This requires the PRISM namespace declaration to be bumped to v2.0. We might consider synchronizing this change with our RSS feeds which are currently pegged at v1.2, although note that the RSS module [mod_prism][5] currently applies only to PRISM v1.2.
          * We could then also add in a &#8220;`prism.url`&#8221; term to link back (through the DOI proxy server) to the content site. The namespace issue listed above still holds.
              * The &#8220;`citation_`&#8221; terms are not anchored in any published namespace which does make this term set problematic in application reuse. It would be useful to be able to reference a namespace (e.g. &#8220;`rel="schema.gs" href="..."`&#8220;) for these terms and to cite them as e.g. &#8220;`gs.citation_title`&#8220;. </ul>
                The HTML metadata sets from an example [landing page][6] are presented below.

                <!--more-->



                If you view the page source you should see something like the text below. (Note that you may have to scroll past whitespace which is emitted by the HTML template generator.)

                <pre>&lt;link title="schema(DC)" rel="schema.dc" href="http://purl.org/dc/elements/1.1/" /&gt;
&lt;meta name="dc.publisher" content="Nature Publishing Group" /&gt;
&lt;meta name="dc.language" content="en" /&gt;
&lt;meta name="dc.rights" content="&#169; 2008 Nature Publishing Group" /&gt;
&lt;meta name="dc.title" content="Crystal structure of squid rhodopsin" /&gt;
&lt;meta name="dc.creator" content="Midori Murakami" /&gt;
&lt;meta name="dc.creator" content="Tsutomu Kouyama" /&gt;
&lt;meta name="dc.identifier" content="doi:10.1038/nature06925" /&gt;
&lt;link title="schema(PRISM)" rel="schema.prism" href="https://web.archive.org/web/20080516191035/http://www.prismstandard.org//namespaces/1.2/basic/" /&gt;
&lt;meta name="prism.copyright" content="&#169; 2008 Nature Publishing Group" /&gt;
&lt;meta name="prism.rightsAgent" content="permissions@nature.com" /&gt;
&lt;meta name="prism.publicationName" content="Nature" /&gt;
&lt;meta name="prism.issn" content="0028-0836" /&gt;
&lt;meta name="prism.eIssn" content="1476-4687" /&gt;
&lt;meta name="prism.volume" content="453" /&gt;
&lt;meta name="prism.number" content="7193" /&gt;
&lt;meta name="prism.startingPage" content="363" /&gt;
&lt;meta name="prism.endingPage" content="367" /&gt;
&lt;meta name="citation_journal_title" content="Nature" /&gt;
&lt;meta name="citation_publisher" content="Nature Publishing Group" /&gt;
&lt;meta name="citation_authors" content="Midori Murakami, Tsutomu Kouyama" /&gt;
&lt;meta name="citation_title" content="Crystal structure of squid rhodopsin" /&gt;
&lt;meta name="citation_volume" content="453" /&gt;
&lt;meta name="citation_issue" content="7193" /&gt;
&lt;meta name="citation_firstpage" content="363" /&gt;
&lt;meta name="citation_doi" content="doi:10.1038/nature06925" /&gt;
</pre>

                While it is not expected that search engines will index these terms directly and that no direct SEO is intended, we think there is enough value for applications to make use of these terms. The terms are reasonably accessible to simple scripts, etc. Note that even in [RFC 2731][4] (published in 1999) there is a Perl script listed in Section 9 which allows the metadata name/value pairs to be easily pulled out. Running this over the example page yields the following output:

                <pre>@(urc;
@|MISSING ELEMENT NAME; text/css
@|MISSING ELEMENT NAME; text/html; charset=iso-8859-1
@|robots; noarchive
@|keywords; Nature, science, science news, biology, physics, genetics, astronomy, astrophysics, quantum physics, evolution, evolutionary biology, geophysics, climate change, earth science, materials science, interdisciplinary science, science policy, medicine, systems biology, genomics, transcriptomics, palaeobiology, ecology, molecular biology, cancer, immunology, pharmacology, development, developmental biology, structural biology, biochemistry, bioinformatics, computational biology, nanotechnology, proteomics, metabolomics, biotechnology, drug discovery, environmental science, life, marine biology, medical research, neuroscience, neurobiology, functional genomics, molecular interactions, RNA, DNA, cell cycle, signal transduction, cell signalling.
@|description; Nature is the international weekly journal of science: a magazine style journal that publishes full-length research papers in all disciplines of science, as well as News and Views, reviews, news, features, commentaries, web focuses and more, covering all branches of science and how science impacts upon all aspects of society and life.
@|dc.publisher; Nature Publishing Group
@|dc.language; en
@|dc.rights; #169; 2008 Nature Publishing Group
@|dc.title; Crystal structure of squid rhodopsin
@|dc.creator; Midori Murakami
@|dc.creator; Tsutomu Kouyama
@|dc.identifier; doi:10.1038/nature06925
@|prism.copyright; &#169; 2008 Nature Publishing Group
@|prism.rightsAgent; permissions@nature.com
@|prism.publicationName; Nature
@|prism.issn; 0028-0836
@|prism.eIssn; 1476-4687
@|prism.volume; 453
@|prism.number; 7193
@|prism.startingPage; 363
@|prism.endingPage; 367
@|citation_journal_title; Nature
@|citation_publisher; Nature Publishing Group
@|citation_authors; Midori Murakami, Tsutomu Kouyama
@|citation_title; Crystal structure of squid rhodopsin
@|citation_volume; 453
@|citation_issue; 7193
@|citation_firstpage; 363
@|citation_doi; doi:10.1038/nature06925
@)urc;
</pre>

 [1]: http://www.w3.org/TR/html4/struct/global.html#h-7.4.4
 [2]: http://www.w3.org/TR/1999/WAI-WEBCONTENT-19990505/#gl-facilitate-navigation
 [3]: http://dublincore.org/documents/dc-html/
 [4]: http://www.ietf.org/rfc/rfc2731.txt
 [5]: https://web.archive.org/web/20080726155717/http://www.prismstandard.org/resources/mod_prism.html
 [6]: http://dx.doi.org/10.1038/nature06925
