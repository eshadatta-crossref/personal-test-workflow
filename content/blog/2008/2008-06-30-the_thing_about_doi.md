---
title: The Thing About DOI
author: thammond
authors:
  - thammond
date: 2008-06-30

categories:
  - Discussion
archives:
  - 2008

---
With Library of Congress sometime back (Feb. ’08) [announcing][1] [LCCN Permalinks][2] and NLM also (Mar. ’08) introducing [simplified web links](https://www.nlm.nih.gov/pubs/techbull/ma08/ma08_simplified_web_links.html) with its PubMed identifier one might be forgiven for wondering what is the essential difference between a DOI name and these (and other) seemingly like-minded identifiers from a purely web point of view. Both these identifiers can be accessed through very simple URL structures:

With Library of Congress sometime back (Feb. ’08) [announcing][1] [LCCN Permalinks][2] and NLM also (Mar. ’08) introducing [simplified web links][4] with its PubMed identifier one might be forgiven for wondering what is the essential difference between a DOI name and these (and other) seemingly like-minded identifiers from a purely web point of view. Both these identifiers can be accessed through very simple URL structures:


  * <https://lccn.loc.gov/2003556443>
      * <http://www.ncbi.nlm.nih.gov/pubmed/16481614> (although <https://web.archive.org/web/20090106151604/http://pubmed.com/1386390> also works as noted [here][5])</ul>
        And the DOI itself can be resolved using an equally simple URL structure:

          * <http://dx.doi.org/10.1000/1></ul>
            So, why does DOI not just present itself as a simple database number which is accessed through a simple web link and have done with it, e.g. a page for the object named by the DOI &#8220;10.1000/1&#8221; is retrieved from the DOI proxy server at <http://dx.doi.org/>?

            Essentially the typical DOI link presents an elementary web-based URL which performs a useful redirect service. What is different about this and, say a [PURL][6], which offers a similar redirect service? What’s the big deal?

            (Continues below.)

            <!--more-->

            Well, the thing about DOI is that it is built upon a directory service - the [Handle System][7] - and can be accessed either through native directory calls or more likely through standard web interfaces. From a web point of view we are usually interested in the latter. Differently from a simple lookup and/or redirect service which has a fixed entry point on the Web, the DOI can be serviced at _any_ DOI service access point on the Internet. There are potentially multiple entry points which can be hosted by different organizations with separate IP addresses and/or DNS names.

            For example, the [DOI proxy][8] (described [here][9]) is just _one instance_ of such a service. Others could equally exist. And, in fact, they do. The following handle web services will also take the DOI and do the business:

              * <http://hdl.handle.net/10.1000/1>
                  * <http://hdl.nature.com/10.1000/1></ul>
                    With handle we have in essence a redirect to a redirect. Or in the case of a web service, a redirect (from HTTP to HDL) to a redirect (from HDL to HDL) to a redirect (from HDL to HTTP). That is, switch down from the web interface to the native handle layer, route the call from this local handle sever (via the global handle server) to the DOI handle server, fetch the URL stored with the DOI and switch back to the Web at that location.

                    But there’s more. The standard URL redirect is just _one_ example of a DOI service. But multiple services can also be provided for the DOI. Currently the DOI travels light and is bound to the minimum of useful data, essentially just the URL for a splash page in the case of many Crossref DOIs. But it could also carry pointers to structured information or to relationships with other objects.

                    As yet, the DOI is a fledgling in terms of realizing its true potential as a seasoned actor that can play out many roles - assume many guises. A queen bee, in effect, with a hive of worker bees servicing it. It is not joined at the hip with any particular web service as might be commonly understood with the current simple redirect service. It offers much more.

                    It is, however, true that both for reasons of link persistency and in order to maintain link ranking with search crawlers that a preferred web entry point is via the [DOI proxy][8]. It just doesn’t have to be that way - that’s all. Hard linking is something we are beginning to unlearn and instead we are taking our first steps towards embracing service-mediated links such as OpenURL and DOI can both offer.

 [1]: http://catalogablog.blogspot.com/2008/02/lccn-permalink.html
 [2]: http://lccn.loc.gov/
 [3]: #
 [4]: http://www.nlm.nih.gov/pubs/techbull/ma08/ma08_simplified_web_links.html
 [5]: http://shelved.blogspot.com/2008/04/pubmed-sends-out-few-new-blooms.html
 [6]: http://purl.org/
 [7]: http://www.handle.net/
 [8]: http://dx.doi.org/
 [9]: https://web.archive.org/web/20080704170549/http://www.doi.org/handbook_2000/resolution.html#3.6.2
