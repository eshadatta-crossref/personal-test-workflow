---
title: Referencing OpenURL
author: thammond
authors:
  - thammond
date: 2008-05-29

categories:
  - Discussion
archives:
  - 2008

---
So, why is it just so difficult to reference OpenURL?

Apart from the standard itself (hardly intended for human consumption - see abstract page [here][1] and PDF and don’t even think to look at those links - they weren’t meant to be cited!), seems that the best reference is to the [Wikipedia page][2]. There is the OpenURL Registry page at [http://alcme.oclc.org/openurl/servlet/OAIHandler?verb=ListSets][3] but this is just a workshop. Not much there beyond the OpenURL registered items. (And why does the page seem uncertain as to whether it’s a &#8220;repository&#8221; or a &#8220;registry&#8221;? Is there no difference between those terms?) The only other links are to a mix of HTML and PDF resources. (There really should be a health warning on links to PDFs - they are just not browser friendly documents.) And, I do have to wonder at this: the registry page has a link to the unofficial 0.1 version but not to the 1.0 standard. Er, why? And don’t even try this link: <http://openurl.info/>. Not much info there.

Where else to go? The [NISO site][4] allows a search on &#8220;openurl&#8221; which returns links to the standard and to other related documents.

And then there’s the community site <https://web.archive.org/web/20091027024029/http://openurl.code4lib.org/> targeted at developers and its [Planet OpenURL][5] which is a useful source for current awareness.

Me, I’m sticking with the Wikipedia page as the best reference for OpenURL. How odd that OpenURL aimed at improving linking on the Web should not have it’s own simple access point. Thank heavens at least that DOI has a single reference point: <http://doi.org/>.

 [1]: http://www.niso.org/kst/reports/standards?step=2&#038;project_key=d5320409c5160be4697dc046613f71b9a773cd9e
 [2]: http://en.wikipedia.org/wiki/OpenURL
 [3]: http://openurl.info/registry
 [4]: http://niso.org/
 [5]: https://web.archive.org/web/20111104051428/http://openurl.code4lib.org/aggregator
