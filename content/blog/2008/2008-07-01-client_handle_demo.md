---
title: Client Handle Demo
author: thammond
authors:
  - thammond
date: 2008-07-01

categories:
  - Handle
archives:
  - 2008

---
This test form shows handle value data being processed by JavaScript **_in the browser_** using an [OpenHandle][1] service. This is different from the handle [proxy server][2] which processes the handle data on the server - the data here is processed by the client.
  
Enter a handle and the standard OpenHandle &#8220;Hello World&#8221; document is printed. Other processing could equally be applied to the handle values. (Note that the form may not work in web-based feed readers.)
  


<div id="response">
</div>

 [1]: http://code.google.com/p/openhandle/
 [2]: http://www.handle.net/