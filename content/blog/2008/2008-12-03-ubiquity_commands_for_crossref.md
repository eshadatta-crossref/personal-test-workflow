---
title: Ubiquity commands for Crossref services
author: Geoffrey Bilder
authors:
  - Geoffrey Bilder
date: 2008-12-03

categories:
  - Citation Formats
  - Linking
  - Programming
archives:
  - 2008

---
So the other day [Noel O’Boyle][1] made me feel guilty when he pinged me and asked about the possibility using one of the Crossref APIs for creating a [Ubiquity][2] extension. You see, I had played with the idea myself and had not gotten around to doing much about it. This seemed inexcusable- particularly given how easy it is to build such extensions using the API we developed for the [WordPress and Moveable Type plugins][3] that we [announced][4] earlier in the year. So I dug up my half-finished code, cleaned it up a bit and have [posted the results.][5]

Note that the back-end that supports the plugins has been moved to more stable machines and the index is now being automatically updated with journal and conference proceeding deposits (sorry, no books yet).

Also note that we are hoping that others will look at the code for the WordPress, Moveable Type and Ubiquity plugins and create more such extensions. If you do, please let us know about them at citation-plugin@crossref.org.

 [1]: http://baoilleach.blogspot.com/
 [2]: https://wiki.mozilla.org/Labs/Ubiquity
 [3]: http://sourceforge.net/projects/crossref-cite/
 [4]: /blog/crossref-citation-plugin-for-wordpress/
 [5]: https://www.crossref.org/labs/ubiquity-plugin/
