---
title: 'Exposing Public Data: Options'
author: thammond
authors:
  - thammond
date: 2008-07-01

categories:
  - Metadata
archives:
  - 2008

---
This is a follow-on to an [earlier post][1] which set out the lie of the land as regards DOI services and data for DOIs registered with Crossref. That post differentiated between a native DOI resolution through a public DOI service which acts upon the _&#8220;associated values held in the DOI resolution record&#8221;_ (per [ISO CD 26324][2]) and other related DOI protected and/or private services which merely use the DOI as a key into non-public database offering.
  
Following the service architecture outlined in that post, options for exposing public data appear as follows:

  1. Private Service <ol type="a">
      <li>
        Publisher hosted – Publisher private service</ol> <li>
          Protected Service <ol type="a">
            <li>
              Crossref hosted – Industry protected service <li>
                Crossref routed – Publisher private service</ol> <li>
                  Public Service <ol type="a">
                    <li>
                      Handle System (DOI handle) – Global public service (native DOI service) <li>
                        Handle System (DOI ‘buddy’ handle) – Publisher public service</ol> </ol> <p>
                          (Continues below.)
                        </p>
                        
                        <p>
                          <!--more-->
                          
                          <br /> Option #1 would make public data available through a private service at a publisher host based on the DOI. This places certain constraints on service discovery and persistence. Autodiscovery links can be placed into Web pages, but there is no opportunity to ‘embed’ the services into the DOI itself, and hence these cannot be considered native DOI services. Without a published API (and hence some degree of commitment from the publisher) the service access points (and possibly the services, too) are fragile.<br /> Option #2 would require Crossref to develop a service which would either a) deliver some public data on behalf of the publisher, or b) route requests through to a bespoke publisher service. Both options would require development at Crossref and an upload mechanism for the publisher to pass along data or service address. Both options would be offered as a new member service and would thus likely be subject to membership policy arrangements. One should consider that there would be some restrictions on service operation. One possible restriction might be that this would be a one-time service registration at Crossref and that any additional services would need to be added at the publisher end.<br /> Option #3 uses the existing <a href="http://www.handle.net/">Handle System</a> infrastructure and provides a public read service. There are two possibilities: a) add a record (or records) to the DOI handle, or b) add records to a DOI ‘buddy’ handle under publisher control. Both require further explanation:<br /> Option #3a would require Crossref consent. Unless these records (handle values) were registered by Crossref there would be concerns over interoperability. That and security concerns would almost certainly require that Crossref writes the record. But this would then need to be developed as per Option #2 above. And if a mechanism were put in place it could be restrictive in practice, e.g. not allowing additional records to be inserted (as already noted in Option #2).<br /> Option #3b requires no prior Crossref consent. It is an option available to publishers who run a handle server. This can best be viewed as deploying a platform (a DOI &#8216;buddy’ handle) for hosting service access points with an intention to upload into the DOI handle (effectively Option #3a) as common public services are developed. In short, a public service incubator. Meantime the platform provides for an independent deployment and multiple services can be added as required. An uplink from a so-called DOI ‘buddy’ handle to the DOI handle would be maintained, and also as Crossref allows a down link from the DOI handle to the DOI ‘buddy’ handle (a ‘see also’ type link) could be established thus pairing off these two handles. (Of course, additional values whether held in the DOI resolution record or especially in an associated DOI &#8216;buddy’ record would be subject to common typing constraints for semantic interoperability.)<br /> My personal feeling is that public data is best exposed via a public resolution record with no strings attached. That is the surest way to guarantee both data persistence and accessibility.
                        </p>

 [1]: /blog/exposing-public-data
 [2]: http://doi.org/ISO_Standard/sc9n475.pdf