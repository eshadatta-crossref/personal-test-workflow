---
title: Resource Maps Encoded in POWDER
author: thammond
authors:
  - thammond
date: 2008-12-05

categories:
  - Linking
archives:
  - 2008

---
Following right on from [yesterday’s post][1] on ORE and POWDER, I’ve attempted to map the worked examples in the [ORE User Guide for RDF/XML][2] (specifically [Sect. 3][3]) to POWDER to show that POWDER can be used to model ORE, see

> **[Resource Maps Encoded in POWDER][4]**

(A full explanation for each example is given in the [RDF/XML Guide, Sect. 3][3] which should be consulted.)

This could just all be sheer doolally or might possibly turn out to have a modicum of instructional value &ndash; I don’t know. (It would be real good to get some feedback here.) There are, however, a couple points to note in mapping ORE to POWDER:

  1. The POWDER form is actually more long-winded because it splits the RDF triples into subject and predicate/object divisions, with the first listed within an **iriset** and the second within a **descriptorset**. The net effect, however, may be somewhat cleaner since POWDER uses a simple XML format rather than RDF/XML. 
      * POWDER only supports simple object types (literals or resources) so the blank nodes in the RDF/XML examples for **dcterms:creator** cannot be mapped as such. I have chosen here to use either **foaf:name** or **foaf:page** value.
          * Likewise, and as far as I am aware, POWDER does not support datatyping but I could be wrong on this. I have thus dropped the datatypes on **dcterms:created** and **dcterms:modified**. </ol>
            This is a fairly na&#239;ve mapping. POWDER’s real strength comes in defining groups of resources with its powerful pattern matching capabilities, whereas here I am using a named single resource in each **iriset** through the **includeresource** element. I think, though, this does show how the abstract ORE data model can be serialized in yet another format.

 [1]: /blog/describing-resource-sets-ore-vs-powder/
 [2]: http://www.openarchives.org/ore/1.0/rdfxml
 [3]: http://www.openarchives.org/ore/1.0/rdfxml#Examples
 [4]: https://web.archive.org/web/20100901173559/http://nurture.nature.com/tony/demos/ore-ex/pwdr.htm
