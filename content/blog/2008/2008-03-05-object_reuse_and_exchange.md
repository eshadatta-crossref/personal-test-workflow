---
title: Object Reuse and Exchange
author: Chuck Koscher
authors:
  - Chuck Koscher
date: 2008-03-05

categories:
  - Standards
archives:
  - 2008

---
On March 3rd the Open Archives Initiative held a roll out meeting of the first alpha release of the ORE specification (http://www.openarchives.org/ore/) . According to Herbert Van de Sompel a beta release is planned for late March / early April and a 1.0 release targeted for September. The presentations focused on the aggregation concepts behind ORE and described an ATOM based implementation. ORE is the second project from the OAI but unlike its sibling PMH it is not exclusively a repository technology. ORE provides machine readable manifests for related Web resources in any context. For instance, DOI landing pages (aka splash page) are human readable resources containing links to any number of resources related to the work identified by the DOI. An ORE instance for the DOI (called a Rem or resource map) would describe the same set of resources in a machine friendly format. A standardized form of redirection understood by the DOI proxy would yield the Rem instead of normal page e.g.  

  `
  http://dx.doi.org/10.5555/abcd?type=rem
  `
  which could be useful for crawlers.

A second roll out meeting is planned during the Sparc-08 workshops in early April.
