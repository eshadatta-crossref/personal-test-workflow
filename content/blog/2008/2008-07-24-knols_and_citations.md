---
title: Knols and Citations
author: thammond
authors:
  - thammond
date: 2008-07-24

categories:
  - Linking
archives:
  - 2008

---
So, Google’s [Knol][1] is now live (see [this announcement][2] on Google’s Blog). There’ll be comment aplenty about the merits of this service and how it compares to other user contributed content sites. But one curious detail struck me. In terms of citeability, compare how a Knol contribution (or &#8220;knol&#8221;) may be linked to as may be a corresponding entry in Wikipedia (here I’ve chosen the subject &#8220;Eclipse&#8221;):

_Knol_</p>
:   <https://web.archive.org/web/20080730124803/http://knol.google.com/k/jay-pasachoff/eclipse/IDZ0Z-SC/wTLUGw></p>

    _Wikipedia_</p>

    :   <http://en.wikipedia.org/wiki/Eclipse></dl>

        The Knol link includes author name, subject, and service gunk, while the Wikipedia link includes only the subject. That makes the Wikipedia link both more readily citeable as well as being to some degree discoverable. I wonder what Google’s intentions, if any, are with respect to the citing of their pages (or &#8220;knols&#8221;) as authoritative sources of information. They don’t seem to be doing themselves many favours.

        I am minded of [this post][3] on Jeff Young’s [Q6][4] which cites this passage from the HTTP spec (see [RFC 2616, Sect. 3.2][5]):

        > _&#8220;As far as HTTP is concerned, Uniform Resource Identifiers are simply formatted strings which identify-via name, location, or any other characteristic-a resource.&#8221;_

        URIs bearing these so-called &#8220;characteristics&#8221; are what I would call a service URI in contrast to a name URI (something that I will elaborate on in a separate post). For now, however, I would just note that the Knol URI looks more like a service URI and the Wikipedia URI more like a name URI. I know which URI form I would prefer to cite.

 [1]: https://web.archive.org/web/20091108072655/http://knol.google.com/k
 [2]: http://googleblog.blogspot.com/2008/07/knol-is-open-to-everyone.html
 [3]: https://web.archive.org/web/20061103051120/http://q6.oclc.org/
 [4]: https://web.archive.org/web/20061103051120/http://q6.oclc.org/
 [5]: http://www.w3.org/Protocols/rfc2616/rfc2616-sec3.html#sec3.2
