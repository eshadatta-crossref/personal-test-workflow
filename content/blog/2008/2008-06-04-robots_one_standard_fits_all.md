---
title: 'Robots: One Standard Fits All'
author: thammond
authors:
  - thammond
date: 2008-06-04

categories:
  - Search
archives:
  - 2008

---
Interesting [post][1] from Yahoo! Search’s Director of Product Management, Priyank Garg, &#8220;_One Standard Fits All: Robots Exclusion Protocol for Yahoo!, Google and Microsoft_&#8220;. Interesting also for what it doesn’t talk about. No mention here of [ACAP][2].

 [1]: https://web.archive.org/web/20080630064024/http://www.ysearchblog.com/archives/000587.html
 [2]: https://web.archive.org/web/20080514202201/http://the-acap.org/
