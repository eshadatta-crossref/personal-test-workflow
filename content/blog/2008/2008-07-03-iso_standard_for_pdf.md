---
title: ISO Standard for PDF
author: thammond
authors:
  - thammond
date: 2008-07-03

categories:
  - PDF
archives:
  - 2008

---
I blogged [here][1] back in Jan. 2007 about Adobe submitting PDF 1.7 for standardization by ISO. From yesterday’s ISO [press release][2] this:

> _&#8220;The new standard, **ISO 32000-1, Document management – Portable document format – Part 1: PDF 1.7**, is based on the PDF version 1.7 developed by Adobe. This International Standard supplies the essential information needed by developers of software that create PDF files (conforming writers), software that reads existing PDF files and interprets their contents for display and interaction (conforming readers), and PDF products that read and/or write PDF files for a variety of other purposes (conforming products).&#8221;_

Congrats to Adobe Systems!

 [1]: /blog/an-open-pdf/
 [2]: https://web.archive.org/web/20131005054720/http://www.iso.org/iso/news.htm?refid=Ref1141
