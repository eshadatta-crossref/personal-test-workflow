---
title: Dark Side of the DOI
author: thammond
authors:
  - thammond
date: 2008-05-29

categories:
  - Handle
archives:
  - 2008

---
<a href="/wp/blog/images/openhandle_p5_radial.jpg" onclick="window.open('/wp/blog/images/openhandle_p5_radial.jpg','popup','width=902,height=603,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0'); return false"><img alt="openhandle_p5_radial.jpg" src="/wp/blog/images/openhandle_p5_radial.jpg" width="450" height="300" /></a>
  
(Click to enlarge.)
  
For infotainment only (and because it’s a pretty printing). Glimpse into the dark world of DOI. Here, the handle contents for [doi:10.1038/nature06930][1] exposed as a standard [OpenHandle][2] &#8216;Hello World’ document. Browser image courtesy of [Processing.js][3] and [Firefox 3 RC1][4].

 [1]: http://dx.doi.org/doi:10.1038/nature06930
 [2]: http://code.google.com/p/openhandle/
 [3]: http://ejohn.org/blog/processingjs/
 [4]: http://www.mozilla.com/en-US/firefox/all-rc.html