---
title: Word Add-in for Scholarly Authoring and Publishing
author: admin
authors:
  - admin
date: 2008-03-26

categories:
  - Data
  - Metadata
  - Publishing
  - Standards
  - XML
archives:
  - 2008

---
Last week Pablo Fernicola sent me email announcing that Microsoft have finally released a beta of their Word plugin for marking-up manuscripts with the NLM DTD. I say &#8220;finally&#8221; because we’ve know this was on the way and have been pretty excited to see it. We once even hoped that MS might be able to show the plug-in at the [ALPSP session on the NLM DTD][1], but we couldn’t quite manage it.

The plugin is targeted at production/editorial staff, but, of course, it will be interesting to see if any of this work can be pushed back to the author. I won’t hold my breath on the latter score, but it will be fun to watch.

One thing I would note is that the NLM DTD can also be used in the humanities and social sciences, so, frankly, I think they should market it more broadly.

Anyway- the plugin can be [downloaded][2] from the Microsoft site.

And Pablo has setup a [blog where testers can discuss][3] the add-in.

And there is also an [entry for the project][4] on the Microsoft Research site (an interesting place to peruse, if you have a moment).

Congatulations to Pablo and his team.

 [1]: http://www.alpsp.org.uk/ngen_public/article.asp?id=335&#038;did=47&#038;aid=1244&#038;st=&#038;oaid=-1
 [2]: http://www.microsoft.com/downloads/details.aspx?FamilyId=09C55527-0759-4D6D-AE02-51E90131997E&#038;displaylang=en
 [3]: https://web.archive.org/web/20080725223420/http://blogs.msdn.com/exscientia/archive/2008/03/20/Technology-Preview-Launch.aspx
 [4]: https://web.archive.org/web/20080411085902/http://www.microsoft.com/mscorp/tc/scholarly-publishing.mspx
