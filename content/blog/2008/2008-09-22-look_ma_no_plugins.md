---
title: Look Ma, No Plugins!
author: thammond
authors:
  - thammond
date: 2008-09-22

categories:
  - Handle
archives:
  - 2008

---
<tt>var f = function (OpenHandleJson) {<br /> &nbsp;&nbsp;var h = new OpenHandle(OpenHandleJson);<br /> &nbsp;&nbsp;var hv = h.getHandleValues();<br /> &nbsp;&nbsp;for (var i = 0; i < hv.length; i++) { &nbsp;&nbsp;&nbsp;&nbsp;var v = new HandleValue(hv[i]); &nbsp;&nbsp;&nbsp;&nbsp;if (v.hasType('URL')) { &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;print(v.getData()); &nbsp;&nbsp;&nbsp;&nbsp;} &nbsp;&nbsp;&nbsp;&nbsp;else if (v.hasType('HS_ADMIN')) { &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;var a = new AdminRecord(v.getData()); &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;print(a.getAdminPermissionString()) &nbsp;&nbsp;&nbsp;&nbsp;} &nbsp;&nbsp;} }</tt>

<table border="0" width="100%">
  <tr>
    <td align="right">
      <i>"And that, gentlemen, is how we do that." - Apollo 13</i>
    </td>
  </tr>
</table>

Following on from my earlier [Client Handle Demo][1] post, this entry is just to mention the availability of a port of (part of) the Handle client library (in Java) to JavaScript: openhandle-0.1.1.js which is being maintained on the [OpenHandle][2] site. The JavaScript module contains methods for three classes: <tt>OpenHandle</tt>, <tt>HandleValue</tt> and <tt>AdminRecord</tt>.

What does all that mean? It means that Handles and their constituent values and value fields can be accessed directly within **_any Web browser</b>_ (using an OpenHandle service) which allows a **_dynamic Handle client_** to be generated and presented within a **_user context_**. No plugins required. The port mirrors the class methods in the standard Java client library for Handle.

As a demo of this JavaScript module in action, see this [Inspector][3] app for a card index view of Handle (and by implication DOI) records.</p>

 [1]: /blog/client-handle-demo/
 [2]: https://web.archive.org/web/20160504131854if_/https://code.google.com/archive/p/openhandle/
 [3]: http://nurture.nature.com/openhandle/inspect.html
