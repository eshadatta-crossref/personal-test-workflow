---
title: OpenHandle JavaScript API
author: thammond
authors:
  - thammond
date: 2008-10-08

categories:
  - Handle
archives:
  - 2008

---
[<img border="0" alt="openhandle-api-small.png" src="/wp/blog/images/openhandle-api-small.png" width="340" height="257" />][1] (Click figure for [PDF][1].)

I just posted updated versions of the [OpenHandle][2] JavaScript Client Library ([v0.2.2][3]) and Utilities ([v0.2.2][4]) to the project site. Mainly this post is just by way of saying that there’s now a &#8220;cheat sheet&#8221; for the API (see figure above, click for [PDF][1]) which will give some idea of scope. The JavaScript API attempts to reflect the Java Client Library API for Handle data structures, and has in excess of 100 methods. A [change log][5] is available.

The new API supports:

  * Single namespace
      * Introspection
          * Unit testing, see [here][6]</ul>
            Why is this of interest to Crossref? Well, if DOIs are ever to begin take advantage of their innate [Multiple Resolution][7] capabiities there needs to be nimbler means of accessing the data items stored with a DOI. A JavaScript API would allow the data to be manipulated in the browser down by the user and so enable bespoke services. That, at least, is the idea.

 [1]: http://nurture.nature.com/openhandle/docs/openhandle-api.pdf
 [2]: http://code.google.com/p/openhandle/
 [3]: https://web.archive.org/web/20160405130901/http://openhandle.googlecode.com/files/openhandle-0.2.2.js
 [4]: https://web.archive.org/web/20160323010335/http://openhandle.googlecode.com/files/openhandle-utils-0.2.2.js
 [5]: http://nurture.nature.com/openhandle/lib/CHANGELOG.txt
 [6]: http://nurture.nature.com/openhandle/unit/test-openhandle.html
 [7]: /blog/multiple-resolution/
