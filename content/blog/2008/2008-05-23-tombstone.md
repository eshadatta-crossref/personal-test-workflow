---
title: Tombstone
author: thammond
authors:
  - thammond
date: 2008-05-23

categories:
  - Identifiers
archives:
  - 2008

---
So, the big guns have decided that [XRI][1] is out. In a [message][2] from the TAG yesterday, variously noted as being &#8220;categorical&#8221; (Andy Powell, [eFoundations][3]) and a &#8220;proclamation&#8221; (Edd Dumbill, [XML.com][4]), the co-chairs (Tim Berners-Lee and Stuart Williams) had this to say:

> _&#8220;We are not satisfied that XRIs provide functionality not readily available from http: URIs. Accordingly the TAG recommends against taking the XRI specifications forward, or supporting the use of XRIs as identifiers in other specifications.&#8221;_

Alas, poor XRI. But what might this also mean for other URI schemes (note the reference above to &#8220;http: URIs)? Well, the message starts out with this:

> _&#8220;In The Architecture of the World Wide Web [1] the TAG sets out the reasons why http: URIs are the foundation of the value proposition for the Web, and should be used for naming on the Web. &#8220;_

Now I’m not sure that this is quite what [AWWW][5] actually says. I don’t find it to be that insistent that &#8220;_http&#8221; URIs &#8230; should be used for naming on the Web_&#8221; but I would need to read it more carefully. Certainly, &#8220;http: URIs&#8221; fit the bill and are top of the class. But there is also a general recognition that other schemes than &#8220;http:&#8221; do exist.
  
Interesting times anyway with a &#8220;winner takes all&#8221; approach to identification. I wonder what this all means for DOI.

 [1]: http://www.oasis-open.org/committees/xri/
 [2]: http://lists.w3.org/Archives/Public/www-tag/2008May/0078
 [3]: http://efoundations.typepad.com/efoundations/2008/05/w3c-technical-a.html
 [4]: http://www.oreillynet.com/xml/blog/2008/05/xris_bad_uris_good.html
 [5]: http://www.w3.org/TR/webarch/