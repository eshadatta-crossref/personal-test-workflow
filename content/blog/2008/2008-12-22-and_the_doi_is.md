---
title: 'And the DOI is &#8230;'
slug: 'and-the-doi-is'
author: thammond
authors:
  - thammond
date: 2008-12-22

categories:
  - Metadata
archives:
  - 2008

---
Once structured metadata is added to a file then retrieving a given metadata element is usually a doddle. For example, for PDFs with embedded XMP one can use Phil Harvey’s excellent [Exiftool][1] utility.

Exiftool is a Perl library and application which I’ve blogged about [here][2] earlier which is available as a &#8216;`.zip`&#8216; file for Windows (no Perl required) or &#8216;`.dmg`&#8216; for MacOS. Note that Phil maintains this actively and has done so over the last five years. (And when I say actively I mean just that. I once made the mistake of printing out the change file.)

If Perl’s not your thing, then there’s a Ruby wrapper gem ([MiniExiftool][3]) to access the Exiftool command in trouper OO fashion. Here’s an example Ruby one-liner to get the DOI from a PDF (broken here to meet column width restriction):

> `% ruby -rubygems -e 'require "mini_exiftool";<br />
&nbsp;&nbsp;&nbsp;&nbsp;puts MiniExiftool.new("test.pdf")["doi"]'<br />
10.1038/nphoton.2008.200`

Of course, that could also have been run against an image, audio or video file with XMP packet.

(Makes one wonder vaguely about the feasibility of having a Swiss Army knife type of utility that could read **_any_** file to get the DOI using the embedded XMP, RDFa, RDF, HTML headers, COiNS, etc. Possibly even as last resort fall back to scanning the raw text - if any.)

 [1]: https://exiftool.org/
 [2]: /blog/exiftool/
 [3]: https://exiftool.org/
