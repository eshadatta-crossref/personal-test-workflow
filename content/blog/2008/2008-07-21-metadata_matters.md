---
title: Metadata Matters
author: thammond
authors:
  - thammond
date: 2008-07-21

categories:
  - Metadata
archives:
  - 2008

---
Andy Powell has [published on Slideshare](http://www.slideshare.net/eduservfoundation/does-metadata-matter) this talk about metadata - see his [eFoundations post][1] for notes. It’s 130 slides long and aims

> _&#8220;to cover a broad sweep of history from library cataloguing, thru the Dublin Core, Web search engines, IEEE LOM, the Semantic Web, arXiv, institutional repositories and more.&#8221;_

Don’t be fooled by the length though. This is a flip through and is a readily accessible overview on the importance of metadata. Slides 86-91 might be of interest here. 😉

 [1]: http://efoundations.typepad.com/efoundations/2008/07/does-metadata-m.html
