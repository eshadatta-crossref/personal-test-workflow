---
title: NIH Mandate and PMCIDs
author: Ed Pentz
authors:
  - Ed Pentz
date: 2008-04-15

categories:
  - Identifiers
archives:
  - 2008

---
The [NIH Public Access Policy][1] says &#8220;When citing their NIH-funded articles in NIH applications, proposals or progress reports, authors must include the PubMed Central reference number for each article&#8221; and the [FAQ][2] provides some examples of this:

Examples:

Varmus H, Klausner R, Zerhouni E, Acharya T, Daar A, Singer P. 2003. PUBLIC HEALTH: Grand Challenges in Global Health. Science 302(5644): 398-399. PMCID: 243493

Zerhouni, EA. (2003) A New Vision for the National Institutes of Health. Journal of Biomedicine and Biotechnology (3), 159-160. PMCID: 400215

It’s interesting to note that on PMC itself both the  [The [NIH Public Access Policy][1] says &#8220;When citing their NIH-funded articles in NIH applications, proposals or progress reports, authors must include the PubMed Central reference number for each article&#8221; and the [FAQ][2] provides some examples of this:

Examples:

Varmus H, Klausner R, Zerhouni E, Acharya T, Daar A, Singer P. 2003. PUBLIC HEALTH: Grand Challenges in Global Health. Science 302(5644): 398-399. PMCID: 243493

Zerhouni, EA. (2003) A New Vision for the National Institutes of Health. Journal of Biomedicine and Biotechnology (3), 159-160. PMCID: 400215

It’s interesting to note that on PMC itself both the][3] - but the DOI isn’t linked. Two things occur to me - 1) should Crossref map DOIs to PMCIDs and vice versa and make PMCIDs available in it’s query interfaces and 2) shouldn’t publishers ask that the PMC copy of the article link back to the publisher version? It would be very easy with the DOI.

 [1]: http://publicaccess.nih.gov/
 [2]: http://publicaccess.nih.gov/FAQ.htm#c6
