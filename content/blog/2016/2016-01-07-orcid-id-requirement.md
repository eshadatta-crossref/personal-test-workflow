---
title: ORCID tipping point?
author: Ginny Hendricks
authors:
  - Ginny Hendricks
date: 2016-01-07

categories:
  - ORCID
  - Auto-update
archives:
  - 2016

---
<p >
  <span >Today eight publishers have presented an open letter that sets out the rationale for <a href="https://info.orcid.org/requiring-orcid-in-publications/" target="_blank">making ORCID iDs a requirement</a> for all corresponding authors, a move that is being backed by even more publishers and researchers as the news spreads on twitter with <a href="https://twitter.com/hashtag/publishORCID?src=hash">#publishORCID</a>. Crossref is a founding organization of ORCID and an ongoing supporter so it’s great to see further uptake and even more benefit for the research community.</span><!--more-->
</p>

<p >
  <span >We encourage all our members to strive for complete metadata and that should include ORCID iDs, whether their workflows are able to require them at submission or not. Since we launched the <a href="/blog/auto-update-has-arrived-orcid-records-move-to-the-next-level/">ORCID auto-update process</a> a couple of months ago, over 10,000 authors have given Crossref permission to automatically update their ORCID records.</span>
</p>

<p >
  <span >The open letter—signed by eLife, PLOS, The Royal Society, AGU, EMBO, Hindawi, IEEE, and Science—also offers minimum implementation guidelines for the process:</span>
</p>

  1. <span ><span >Require</span>. ORCID iDs are required for corresponding authors of published papers, ideally at submission.</span>
  2. <span ><span >Collect</span>. The collection of ORCID iDs is done via the ORCID API, so authors are not asked to type in or search for their iD.</span>
  3. <span ><span >Auto-update</span>. Crossref metadata is updated to include ORCID iDs for authors, so this information can automatically populate ORCID records.</span>
  4. <span ><span >Publish</span>. Author/co-author ORCID iDs are embedded into article metadata.</span>

<p class="p1">
  <span ><a href="http://orcid.org/blog/2016/01/07/publishers-start-requiring-orcid-ids" target="_blank">ORCID’s own announcement</a> gives further background and describes the benefits for researchers, such as single sign-on across journals and ultimately, increased discovery of their works. Everybody wins.</span>
</p>
