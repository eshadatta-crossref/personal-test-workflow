---
title: '2016 upcoming events - we’re out and about!'
author: Rosa Clark
authors:
  - Rosa Clark
date: 2016-07-06

categories:
  - Meetings
  - Community
  - Collaboration
archives:
  - 2016

---
<div>
  <p>
    <span >Check out the events below where Crossref will attend or present in 2016. We have been busy over the past few months, and we have more planned for the rest of year. If we will be at a place near you, please come see us (and support these organizations and events)!</span>
  </p>

  <p>
    <span >Upcoming Events</span><br /> <span ><a href="http://www.share-research.org/2016/04/share-2016-community-meeting/">SHARE Community Meeting</a>, July 11-14, Charlottesville, VA, USA</span><br /> <span >Crossref Outreach Day - July 19-21 - Seoul, South Korea</span><br /> <span ><a href="http://asianeditor.org/event/2016/index.php">CASE 2016 Conference</a> - July 20-22 - Seoul, South Korea</span><br /> <span ><a href="http://theacse.com/meeting2016/">ACSE Annual Meeting 2016</a> - August 10-11 - Dubai, UAE</span><br /> <span ><a href="http://vivoconference.org/">Vivo 2016 Conference</a> - August 17-19 - Denver CO, USA</span><br /> <span ><a href="https://www.scidatacon.org/2016/">SciDataCon</a> - September 11-17 - Denver CO, USA</span><br /> <span ><a href="https://www.alpsp.org/2016-Programme">ALPSP</a> - September 14-16 - London, UK</span><br /> <span ><a href="http://oaspa.org/conference/">OASPA</a> - September 21-22 - Arlington VA, USA</span><br /> <span ><a href="http://altmetricsconference.com/">3:AM Conference</a> - September 26 - 28 - Bucharest, Romania</span><br /> <span ><a href="https://orcid.org/about/events/">ORCID Outreach Conference</a> - October 5-6 - Washington DC, USA</span><br /> <span ><a href="http://www.buchmesse.de/en/">Frankfurt Book Fair</a> - October 19-23 - Frankfurt, Germany (Hall 4.2, Stand #4.2 M 85)</span><br /> <span ><a href="https://www.eventbrite.com/e/crossref-annual-community-meeting-2016-tickets-25928526922">Crossref Annual Community Meeting #Crossref16</a> - November 1-2 - London, UK**</span><br /> <span ><a href="http://pidapalooza.org/">PIDapalooza</a> - November 9-10 - Reykjavik, Iceland</span><br /> <span ><a href="http://www.opencon2016.org/updates">OpenCon 2016</a> - November 12-14 - Washington DC, USA</span><br /> <span ><a href="http://www.stm-assoc.org/events/stm-digital-publishing-2016/">STM Digital Publishing Conference</a> - December 6-8 - London, UK</span>
  </p>
</div>

<div>
  <a href="/wp/blog/uploads/2016/06/DC4.jpeg"><img class="alignnone size-medium wp-image-1831" src="/wp/blog/uploads/2016/06/DC4-300x225.jpeg" alt="DC4" width="300" height="225" srcset="/wp/blog/uploads/2016/06/DC4-300x225.jpeg 300w, /wp/blog/uploads/2016/06/DC4-768x576.jpeg 768w, /wp/blog/uploads/2016/06/DC4.jpeg 948w" sizes="(max-width: 300px) 85vw, 300px" /></a> <a href="/wp/blog/uploads/2016/06/DC2.jpeg"><br /> </a>
</div>

<div>
  <span >The Crossref outreach team will host a number of outreach events around the globe. Updates about events are shared through social media so please connect with us via @CrossrefOrg.</span>
</div>

<div>
  <span > </span>
</div>

<div>
</div>
