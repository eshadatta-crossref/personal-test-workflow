---
title: 'Preprints and Crossref’s metadata services'
author: Chuck Koscher
authors:
  - Chuck Koscher
date: 2016-08-29

categories:
  - Preprints
archives:
  - 2016

---
We’re putting the final touches on the changes that will allow preprint publishers to register their metadata with Crossref and assign DOIs. These changes support Crossref’s CitedBy linking between the preprint and other scholarly publications (journal articles, books, conference proceedings). Full preprint support will be released over the next few weeks.

<!--more-->I’d like to mention one change that will be immediately visible to Crossref members who use our OAI based service to retrieve CitedBy links to their content.

This API, show in an example here, is intended to retrieve large quantities of data detailing all the CitedBy links to a given publication. The example request shows pulling the data for an IEEE conference proceeding.

<p >
  example:
</p>

<p >
  http://oai.crossref.org/OAIHAndler?verb=ListRecords&usr=*** pwd=****&set=B:10.1109:1070762&metadataPrefix=cr_citedby
</p>

<div class="" >
</div>

With the new change, results will now identify the type of content that is doing the citing. The example results below shows that the DOI 10.1109/CSMR.2012.14  is cited by five other items and displays the DOIs of those items and their content type.

<p >
  <a href="/wp/blog/uploads/2016/08/Screen-Shot-2016-08-29-at-9.12.24-AM.png"><img class="alignnone wp-image-2031" src="/wp/blog/uploads/2016/08/Screen-Shot-2016-08-29-at-9.12.24-AM-300x235.png" alt="Screen Shot 2016-08-29 at 9.12.24 AM" width="436" height="341" srcset="/wp/blog/uploads/2016/08/Screen-Shot-2016-08-29-at-9.12.24-AM-300x235.png 300w, /wp/blog/uploads/2016/08/Screen-Shot-2016-08-29-at-9.12.24-AM.png 536w" sizes="(max-width: 436px) 85vw, 436px" /></a>
</p>

<p >
  When preprint content that cites other scholarly work starts being registered with Crossref, members using this API will start seeing data like the following:
</p>

<p >
  <a href="/wp/blog/uploads/2016/08/Screen-Shot-2016-08-29-at-9.20.15-AM.png"><img class="alignnone wp-image-2032" src="/wp/blog/uploads/2016/08/Screen-Shot-2016-08-29-at-9.20.15-AM-300x197.png" alt="Screen Shot 2016-08-29 at 9.20.15 AM" width="432" height="284" srcset="/wp/blog/uploads/2016/08/Screen-Shot-2016-08-29-at-9.20.15-AM-300x197.png 300w, /wp/blog/uploads/2016/08/Screen-Shot-2016-08-29-at-9.20.15-AM.png 490w" sizes="(max-width: 432px) 85vw, 432px" /></a>
</p>

<p >
  For many users of Crossref metadata the introduction of preprints will be transparent until preprint content starts being registered. However, a few changes like the one above have benefits not limited to just preprints.
</p>
