---
title: Using AWS S3 as a large key-value store for Chronograph
author: Joe Wass
authors:
  - Joe Wass
date: 2016-08-02

categories:
  - Crossref Labs
  - DOIs
  - Event Data
  - Programming
  - Wikipedia
archives:
  - 2016

---
<span >One of the cool things about working in Crossref Labs is that interesting experiments come up from time to time. One experiment, entitled &#8220;what happens if you plot DOI referral domains on a chart?&#8221; turned into the <a href="http://chronograph.labs.crossref.org">Chronograph</a> project. In case you missed it, Chronograph analyses our DOI resolution logs and shows how many times each DOI link was resolved per month, and also how many times a given domain referred traffic to DOI links per day.</span>

<span >We’ve released a new version of Chronograph. This post explains how it was put together. One for the programmers out there.</span>

## <span >Big enough to be annoying</span>

<span >Chronograph sits on the boundary between normal-sized data and large-enough-to-be-annoying-size data. It doesn’t store data for all DOIs (it includes only those that are used on average once a day), but it has information on up to 1 million DOIs per month over about 5 years, and about 500 million data points in total.</span>

<span >Storing 500 million data points is within the capabilities of a well-configured database. In the first iteration of Chronograph a MySQL database was used. But that kind of data starts to get tricky to back up, move around and index.</span>

<span >Every month or two new data comes in for processing, and it needs to be uploaded and merged into the database. Indexes need to be updated. Disk space needs to be monitored. This can be tedious.</span>

## <span >Key values</span>

<span >Because the data for a DOI is all retrieved at once, it can be stored together. So instead of a table that looks like</span>

<table>
  <tr>
    <td>
      <span >10.5555/12345678</span>
    </td>

    <td>
      <span >2010-01-01</span>
    </td>

    <td>
      <span >5</span>
    </td>
  </tr>

  <tr>
    <td>
      <span >10.5555/12345678</span>
    </td>

    <td>
      <span >2010-02-01</span>
    </td>

    <td>
      <span >7</span>
    </td>
  </tr>

  <tr>
    <td>
      <span >10.5555/12345678</span>
    </td>

    <td>
      <span >2010-03-01</span>
    </td>

    <td>
      <span >3</span>
    </td>
  </tr>
</table>

<span >Instead we can store</span>

<table>
  <tr>
    <td>
      10.5555/12345678
    </td>

    <td>
      {&#8220;2010-01-01&#8221;: 5, &#8220;2010-02-01&#8221;: 7, &#8220;2010-03-01&#8221;: 3}
    </td>
  </tr>
</table>

<span >This is much lighter on the indexes and takes much less space to store. However, it means that adding new data is expensive. Every time there’s new data for a month, the structure must be parsed, merged with the new data, serialised and stored again millions of times over.</span>

<span >After trials with <a href="https://www.mysql.com/">MySql</a>, <a href="https://www.mongodb.com/">MongoDB</a> and <a href="http://www.mapdb.org/">MapDB</a>, this approach was taken with MySQL in the original Chronograph.</span>

## <span >Keep it Simple Storage Service Stupid</span>

<span >In the original version of Chronograph the data was processed using <a href="http://spark.apache.org/">Apache Spark</a>. There are various solutions for storing this kind of data, including Cassandra, time-series databases and so on.</span>

<span >The flip side of being able to do interesting experiments is wanting them to stick around without having to bother a sysadmin. The data is important to us, but we’d rather not have to worry about running another server and database if possible.</span>

<span >Chronograph fits into the category of &#8216;interesting’ rather than &#8216;mission-critical’ projects, so we’d rather not have to maintain expensive infrastructure if possible.</span>

<span >I decided to look into using Amazon Web Services <a href="https://aws.amazon.com/s3/">Simple Storage Service</a> (AWS S3) to store the data. AWS itself is a key-value store, so it seems like a good fit. S3 is a great service because, as the name suggests, it’s a simple service for storing a large number of files. It’s cheap and its capabilities and cost scale well.</span>

<span >However, storing and updating up to 80 million very small keys (one per DOI) isn’t very clever, and certainly isn’t practical. I looked at <a href="https://aws.amazon.com/documentation/dynamodb/">DynamoDB</a>, but we still face the overhead of making a large number of small updates.</span>

## <span >Is it weird?</span>

<span >In these days of plentiful databases with cheap indexes (and by &#8216;these days’ I mean the 1970s onward) it seems somehow wrong to use plain old text files. However, the whole Hadoop &#8220;Big Data&#8221; movement was predicated on a return to batch processing files. Commoditisation of services like S3 and the shift to do more in the browser have precipitated a bit of a rethink. The movement to abandon LAMP stacks and use static site generators is picking up pace. The term &#8216;serverless architecture’ is hard to avoid if you read <a href="https://hn.algolia.com/?query=serverless%20architecture&sort=byDate&prefix&page=0&dateRange=all&type=story">certain news sites</a>.</span>

<span >Using Apache Spark (with its brilliant <a href="http://spark.apache.org/docs/latest/programming-guide.html#resilient-distributed-datasets-rdds">RDD concept</a>) was useful for bootstrapping the data processing for Chronograph, but the new code has an entirely flat-file workflow. The simplicity of not having to unnecessarily maintain a <a href="https://hadoop.apache.org/docs/r1.2.1/hdfs_design.html">Hadoop HDFS</a> instance seems to be the right choice in this case.</span>

## <span >Repurposing the Wheel</span>

<span >The solution was to use S3 as a big <a href="https://en.wikipedia.org/wiki/Hash_table">hash table</a> to store the final data that’s served to users.</span>

<span >The processing pipeline uses flat files all the way through from input log files to projections to aggregations. At the penultimate stage of the pipeline blocks of CSV per DOI are produced that represent date-value pairs.</span>

<table>
  <tr>
    <td>
      10.5555/12345678
    </td>

    <td>
      2010-01
    </td>

    <td>
      2010-01-01,05<br /> 2010-02-01,02<br /> 2010-01-03,08<br /> &#8230;
    </td>
  </tr>

  <tr>
    <td>
      10.5555/12345678
    </td>

    <td>
      2010-02
    </td>

    <td>
      2010-02-1,10<br /> 2010-02-01,7<br /> 2010-02-03,22<br /> &#8230;
    </td>
  </tr>
</table>

<span >At the last stage, these are combined into blocks of all dates for a DOI</span>

<table>
  <tr>
    <td>
      10.5555/12345678
    </td>

    <td>
      2010-01
    </td>

    <td>
      2010-01-01,05<br /> 2010-02-01,02<br /> 2010-01-03,08<br /> &#8230;<br /> 2010-02-1,10<br /> 2010-02-01,7<br /> 2010-02-03,22<br /> &#8230;
    </td>
  </tr>
</table>

<span >The DOIs are then hashed into 12 bits and stored as chunks of CSV</span>

<span >day-doi.csv-chunks_8841:</span>

<pre class="">10.1038/ng.3020
2014-06-24,4
2014-06-25,4
2014-06-26,3
...

10.1007/978-94-007-2869-1_7
2012-06-01,12
2012-06-02,8
...

10.1371/journal.pone.0145509
2016-02-01,13
2016-02-02,75
2016-02-03,30
...</pre>

<span >There are 65,536 (0x000 to 0xFFFF) possible files, each with about a thousand DOIs worth of data in each.</span>

<span >When the browser requests data for a DOI, it is hashed and then the request for the appropriate file in S3 is made. The browser then has to perform a linear scan of the file to find the DOI it is looking for.</span>

<span >This is the simplest possible form of hash table: simple addressing with separate <a href="https://en.wikipedia.org/wiki/Hash_table#Separate_chaining_with_linked_lists">linear chaining</a>. The hash function is a 16-bit mask of MD5, chosen because of availability in the browser. It does a great job of evenly distributing the DOIs over all 65,536 possible files.</span>

## <span >Striking the balance</span>

<span >In any data structure implementation, there are balances to be struck. Traditionally these concern memory layout, the shape of the data, practicalities of disk access and CPU cost.</span>

<span >In this instance, the factors in play included the number of buckets that need to be uploaded and the cost of the browser downloading an over-large bucket. The size of the bucket doesn’t matter much for CPU (as far as the user is concerned it takes about the same time to scan 10 entries as it does 10,000), but it does make a difference asking  user to download a 10kb bucket or a 10MB one.</span>

<span >I struck the balance at 4096 buckets, resulting in files of around 100k, which is the size of a medium sized image.</span>

## <span >It works</span>

<span >The result is a simple system that allows people to look up data for millions of DOIs, without having to look after another server. It’s also portable to any other file storage service.</span>

<span >The approach isn’t groundbreaking, but it works.</span>
