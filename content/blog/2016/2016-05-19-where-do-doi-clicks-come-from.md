---
title: Where do DOI clicks come from?
author: Joe Wass
authors:
  - Joe Wass
date: 2016-05-19

categories:
  - Crossref Labs
  - DOIs
  - Persistence
  - Identifiers
  - Event Data
  - Linking
  - Wikipedia
tags:
  - chronograph
archives:
  - 2016

---
As part of our [Event Data][1] work we’ve been investigating where DOI resolutions come from. A resolution could be someone clicking a DOI hyperlink, or a search engine spider gathering data or a publisher’s system performing its duties. Our server logs tell us every time a DOI was resolved and, if it was by someone using a web browser, which website they were on when they clicked the DOI. This is called a referral.

This information is interesting because it shows not only where DOI hyperlinks are found across the web, but also when they are actually followed. This data allows us a glimpse into scholarly citation beyond references in traditional literature.

Last year Crossref Labs [announced Chronograph][2], an experimental system for browsing some of this data. We’re working toward a new version, but in the meantime I’d like to share the results for 2015 and some of 2016. We have filtered out domains that belong to Crossref member publishers to highlight citations beyond traditional publications.

## Top 10 DOI referrals from websites in 2015

This chart shows the top 10 referring non-primary-publisher domains of DOIs per month. Note that if browsers don’t send the referrer (e.g. from an HTTPS page), we don’t get to find out. Because the top 10 can be different month to month, the total number of domains mentioned can be more than 10. Subdomains are combined, which means that, for example, the wikipedia.org entry covers all Wikipedia languages. This chart covers all of 2015 and the first two months of 2016.

<img src="/wp/blog/uploads/2016/05/month-top-10-filtered-domains-1.png" alt="month-top-10-filtered-domains" class="img-responsive" />

The top 10 referring domains for the period:

  1. webofknowledge.com
  2. baidu.com
  3. serialssolutions.com
  4. scopus.com
  5. exlibrisgroup.com
  6. wikipedia.org
  7. google.com
  8. uni-trier.de
  9. ebsco.com
 10. google.co.uk

It’s not surprising to see some of these domains here: for example serialssolutions.com and exlibrisgroup.com are effectively proxies for link resolvers, Baidu and Google are incredibly popular search engines which would show up anywhere. But it is exciting to see Wikipedia ranked amongst these. For more detail look out for the new Chronograph.

## HTTP vs HTTPS in 2015

We’ve also seen a steady increase in HTTPS referral traffic, i.e. people clicking on DOIs from sites that are using HTTPS. While it is still dwarfed by HTTP, there was a steady uptick throughout 2015.

This chart shows HTTP vs HTTPS referrals per day, which shows up the weekly spikes. It doesn’t include resolutions where we don’t know the referrer.

<img src="/wp/blog/uploads/2016/05/day-code.png" alt="HTTP vs HTTPS DOI Referrals" class="img-responsive"/>

Increasing numbers of people are moving to HTTPS for reasons of security, privacy and protection from tampering. [Google has announced plans][3] to take HTTPS into account when ranking search results. Wikipedia has moved exclusively to HTTPS, and I’ll be telling the story of how Crossref and Wikipedia collaborated in an upcoming blog post.

## Chronograph

Another version of Chronograph will be available soon. It will contain full data for all non-primary-publisher referring domains. Stay tuned!

 [1]: http://eventdata.crossref.org
 [2]: /blog/introducing-chronograph/
 [3]: https://webmasters.googleblog.com/2014/08/https-as-ranking-signal.html
