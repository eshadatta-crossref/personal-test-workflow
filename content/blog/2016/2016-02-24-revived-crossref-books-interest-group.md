---
title: 'Revived: Crossref Books Interest Group'
author: April Ondis
authors:
  - April Ondis
date: 2016-02-24

categories:
  - Books
  - Publishing
archives:
  - 2016

---
<a href="/wp/blog/uploads/2016/02/books_interest_group_3.png" rel="attachment wp-att-1333"><img class="wp-image-1333 alignright" src="/wp/blog/uploads/2016/02/books_interest_group_3.png" alt="books_interest_group_3" width="312" height="312" srcset="/wp/blog/uploads/2016/02/books_interest_group_3.png 800w, /wp/blog/uploads/2016/02/books_interest_group_3-150x150.png 150w, /wp/blog/uploads/2016/02/books_interest_group_3-300x300.png 300w, /wp/blog/uploads/2016/02/books_interest_group_3-768x768.png 768w" sizes="(max-width: 312px) 85vw, 312px" /></a>We’re reviving the Books Interest Group, and inviting new members!

After a hiatus, Crossref’s Books Interest Group is back.  We’re excited to announce that Emily Ayubi of the American Psychological Association has agreed to chair the group.  

<span >In reviving the group, our intention is to create opportunities to talk about issues that are important to scholarly book publishers.  For example, we hope to explore whether it is time to revise the </span>[<span >Crossref best practices</span>][1] <span >for depositing, versioning, and linking book content.   </span>

<span >We are seeking interested members from the book publishing community, and want to hear your ideas for agenda items and topics for discussion.  </span>

<span >Our first meeting will be a teleconference held at 11:00 am Eastern time on Wednesday, March 23rd.  You will receive dial-in details by email. </span>** **

**If you’d like to join&#8212;and we’re hoping you will&#8212;please email me at **[**aondis@crossref.org**][2]**.**

 [1]: http://www.crossref.org/06members/best_practices_for_books.html
 [2]: mailto:aondis@crossref.org
