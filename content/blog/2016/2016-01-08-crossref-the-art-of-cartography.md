---
title: 'Crossref & the Art of Cartography: an Open Map for Scholarly Communications'
author: Jennifer Lin
authors:
  - Jennifer Lin
date: 2016-01-08

categories:
  - Crossref Labs
  - Data
  - Event Data
  - Research Funders
  - Identifiers
  - Linked Data
  - Metadata
  - ORCID
  - XML
archives:
  - 2016

---
&nbsp;

<span >In the <a href="/crossref-live-annual/archive/#2015">2015 Crossref Annual Meeting</a>, I introduced a metaphor for the work that we do at Crossref. I re-present it here for broader discussion as this narrative continues to play a guiding role in the development of products and services this year.</span>

##### <span ><b>Metadata enable connections</b></span>

<span ><span ><a href="/wp/blog/uploads/2016/01/pasted-image-0.png" rel="attachment wp-att-1214"><img class="alignright wp-image-1214" src="/wp/blog/uploads/2016/01/pasted-image-0-200x300.png" alt="Cartography Borges" width="250" height="375" srcset="/wp/blog/uploads/2016/01/pasted-image-0-200x300.png 200w, /wp/blog/uploads/2016/01/pasted-image-0.png 540w" sizes="(max-width: 250px) 85vw, 250px" /></a>At Crossref, we make research outputs easy to find, cite, link, and assess through DOIs. Publishers register their publications and deposit metadata through a variety of channels (XML, CSV, PDF, manual entry), which we process and transform into Crossref XML for inclusion into our corpus. This data infrastructure which makes possible scholarly communications without restrictions on publisher, subject area, geography, etc. is far more than a reference list, index or directory.</span> </span><!--more-->

<span >If research builds on what came before, one could claim that the process of knowledge production is partly the story of the very relationships between results disseminated (i.e., publications). So let’s consider each publication as a node in a graph where <a href="/wp/blog/uploads/2016/01/Map-entities.jpeg" rel="attachment wp-att-1247"><img class="wp-image-1250 alignright" src="/wp/blog/uploads/2016/01/Map-entities-300x237.jpeg" alt="" width="211" height="166" srcset="/wp/blog/uploads/2016/01/Map-entities-300x237.jpeg 300w, /wp/blog/uploads/2016/01/Map-entities.jpeg 651w" sizes="(max-width: 211px) 85vw, 211px" /></a>each has a coordinate and is connected by its citations to other publications (as well those that cite it). Additionally, each is associated with a set of people and places, along with a whole host of elements involved in the research and dissemination process.</span>

<span ><span >But take a wider berth, and we begin to capture relationships between all such contributing agents and objects involved in the research process. Here we find an array of entities belonging to the scholarly graph, including different types of research artifacts, publisher and journal, funders, ORCIDs, peer reviews, publication status updates (corrections, retractions, etc.), citations, license information, additional URLs (machine destinations, hosting platforms, etc.), underlying data, software and protocols, materials, discussions and blog posts, recommendations, reference work mentions, etc. The entities on the graph multiply at an even higher rate as researchers share more outputs across more channels. And over time, the graph expands exponentially, producing a webbing that is far more dense and far more vast than we can currently imagine. Perhaps even to the point we realize Borges’ story where a cartographer builds a map so large it replicates the territory itself (</span><em><a href="http://www.borges.pitt.edu/node/144"><span >On Exactitude in Science</span></a></em><span >)!</span></span>

<!--more-->

##### <span ><b>From graph to cartography</b></span>

<span >At the heart of Borges’s poignant story is the map. Crossref’s graph of scholarly communications could be seen in the same light. It has a representational aspect, which is not purely abstract and can be visualized. Here, a map becomes an incredibly potent metaphor. Each link enabled by publisher-deposited metadata is a new street, bridge, or highway that takes us to a particular place (i.e., entity) of interest. These roads lead to articles, researchers, funders, institutions, etc., and in doing so, make them discoverable. They tell a story about the roles of each in the broader research in the landscape dotted with a plethora of places. </span>

<span ><span >The scholarly web has a growing corpus of more than </span><a href="https://data.crossref.org/reports/statusReport.html"><span >78 million publications</span></a><span > at this very moment registered with Crossref. On average ten to fifteen thousand new objects appear every day. Maps are all the more essential for getting around in a bewildering environment of new and unfamiliar places, even for known ones in areas of exploding growth. They are critical for orienteering, discovering relationships, identifying sets of associated objects, naming new neighborhoods that emerge (i.e., new research specialties), etc. And if each connection on the map is seen as an event, maps can also represent micro-narratives about the research process and the agents involved. A multi-dimensional map containing all these entities, which serves as an evolving representation of spacetime that is constantly updated and always available, would finally begin to depict the process of scholarly activity as a dynamic, evolving, almost living system.</span></span>

##### <span ><b>An open map for scholarly communication</b></span>

<span ><span >Crossref builds such a scholarly map of the research enterprise and makes it openly available for the entire research ecosystem. Call this a meta map or, more recently, call it </span><a href="http://www.wired.com/2016/01/the-metastructure-transportation/"><span >metastructure</span></a><span >. No matter what name it goes by we call it infrastructure at Crossref.</span></span>

<span ><span >Crossref’s open map for scholarly communications is a core part of the open information infrastructure for scholarly research. Crossref map data are open, portable, as well as licensed and provisioned for maximum reuse to serve the whole community. This open resource has two entrances: one for humans, another for machines. The </span><a href="https://github.com/Crossref/rest-api-doc/blob/master/rest_api.md"><span >Crossref REST API</span></a><span > enables machines to traverse this environment and mine it in equal measure to the humans behind them. It is configured so that a robot can learn, a phone can access, and platforms can be built.</span></span>

<span ><a href="https://www.openstreetmap.org/"><span >OpenStreetMap</span></a><span > and </span><a href="https://developers.google.com/maps/?hl=en"><span >Google Maps</span></a><span >, both widely used and mature infrastructure maps, are instructive examples when we consider a map of this kind for scholarly communications. Map data can be represented in unlimited ways, depending on any variety of needs and users. Third parties can add content via </span><a href="http://googlegeodevelopers.blogspot.co.uk/2015/04/interactive-data-layers-in-javascript.html"><span >interactive layers</span></a><span > that tell different stories such as </span><a href="https://mapsengine.google.com/10237621067095735108-16932951632409324660-4/mapview/?authuser=0"><span >health expenditure by country based on GDP</span></a><span > and </span><a href="https://mapsengine.google.com/06900458292272798243-13579632754418963048-4/mapview/?authuser=0"><span >coral reefs at risk</span></a><span >. They have a broad base of users across business models from philanthropic services aimed at disaster relief (</span><a href="http://refugeemaps.eu/"><span >Refugeemaps.eu</span></a><span >) to commercial entities providing drivers with locations on open parking spaces (</span><a href="https://www.appyparking.com/"><span >AppyParking</span></a><span > on Google Map, </span><a href="https://twitter.com/pocketparker"><span >PocketParker</span></a><span > on OpenStreetMap). They power platforms and services that build maps for others (</span><a href="http://www.mapquest.com/"><span >MapQuest</span></a><span >, </span><a href="https://www.mapbox.com/"><span >MapBox</span></a><span >). They have applications far beyond the business of maps. For example, </span><a href="https://web.archive.org/web/20170716112842/https://developers.google.com/places/android-api/placepicker"><span >Place picker</span></a><span > is a Google Maps widget that supports easy auto-complete the entry of any place or location on a mobile app where typing is a chore. And as far use cases close to home, the two have served as raw data for academic research (ex: </span><a href="http://svn.vsp.tu-berlin.de/repos/public-svn/publications/vspwp/2011/11-10/2011-06-20_openstreetmap_for_traffic_simulation_sotm-eu.pdf"><span >workflow for generating multi-agent traffic simulation scenarios</span></a><span >, </span><a href="http://www.tandfonline.com/doi/abs/10.1080/13658816.2012.692791?journalCode=tgis20#.Vo11aJMrIo8"><span >automatic classification of GPS trajectories for transportation modes</span></a><span >, etc.).</span></span>

<span >In kind, the Crossref infrastructure map also supports: the development of any variety of new maps which re-present the data, the makers of map platforms that power the research enterprise, tools that use map data, as well as academic research (bibliometrics). We extract slices of data of common interest from the map and add them as additional layers by which anyone can access and create applications on or across these bands of data: </span>

  * <span >Contributors (authors, editors, reviewers)</span>
  * <span >Funding information (funding body, grant number)</span>
  * <span >Trial & study information (clinical trials registry number, registered report, replication study)</span>
  * <span >Publication history (versions, updates, revisions, corrections, retractions, dates received/accepted/published)</span>
  * <span >Peer review (status, type, reviews)</span>
  * <span >Access indicators (publication license for text & data mining, machine mining URLs)</span>
  * <span >Resources & associated research artifacts (preprints, figures & tables, datasets, software, protocols, research resource IDs)</span>
  * <span >Activity surrounding the publication (peer reviews, comments & discussions, bookmarks, social shares, recommendations).</span>

<span >Today, the map powers a host of public and commercial organizations alike for a wide range of scholarly and non-scholarly purposes:</span>

<table style="border: 1px solid #ffffff;" border="0" width="400" cellspacing="0" cellpadding="0">
  <tr>
    <td style="border: 1px solid #ffffff;">
      <ul>
        <li>
          <span >Publishers</span>
        </li>
        <li>
          <span >Funders</span>
        </li>
        <li>
          <span >Research institutions</span>
        </li>
        <li>
          <span >Archives & repositories</span>
        </li>
        <li>
          <span >Research councils</span>
        </li>
        <li>
          <span >Data centres</span>
        </li>
        <li>
          <span >Professional networks</span>
        </li>
        <li>
          <span >Patent offices</span>
        </li>
        <li>
          <span >Registration Agencies</span>
        </li>
      </ul>
    </td>

    <td style="border: 1px solid #ffffff;">
      <ul>
        <li>
          <span >Indexing services</span>
        </li>
        <li>
          <span >Publishing vendors</span>
        </li>
        <li>
          <span >Peer review systems</span>
        </li>
        <li>
          <span >Reference manager systems</span>
        </li>
        <li>
          <span >Lab & diagnostics suppliers</span>
        </li>
        <li>
          <span >Info management systems</span>
        </li>
        <li>
          <span >Educational tools</span>
        </li>
        <li>
          <span >Data analytics systems</span>
        </li>
        <li>
          <span >Literature discovery services</span>
        </li>
      </ul>
    </td>
  </tr>
</table>

<span >We will follow up this post to highlight a cross-section of these consumers in the Crossref map ecosystem and elaborate on what & how they have built from our data. An infrastructure map offers endless potential to third parties across publishers, funders, research institutions, and vendors working to serve the scholarly research enterprise.</span>

##### <span ><b>The art of cartography</b></span>

<span ><span >In the Crossref Product Management team, we have ambitious plans for map enhancements this year. They focus on expanding information density and ease of access to the data. In the former case, we will introduce a new class of locations where activity surrounding the publications are occurring when we launch the </span><a href="/blog/det-poised-for-launch/"><span >DOI Event Tracker</span></a><span >. We will also initiate an extensive publisher campaign to achieve full metadata deposit completeness across our membership. No one can keep pace with the sheer volume of research activity happening online nor wander the <a href="http://fusion.net/story/251095/lonely-web-the-dress-viral-social-media-profit/">Lonely Web</a> of research alone. The more metadata publishers provide for a publication, the more roads lead to its map location. After all, discoverability is closely associated with connectedness on a map.</span><span > And finally, in the latter case, we will refresh and enhance the user interface to make it more powerful for humans to traverse the ever-changing landscape (as easily as the REST API enables machines!).</span></span>

<span ><i><span >I gratefully acknowledge the feedback received from the following who served as  generous and insightful sounding boards: </span></i><i><a href="https://twitter.com/GinnyBarbour">Virginia Barbour</a></i><i><span >, </span></i><a href="https://twitter.com/TheoBloom"><i><span >Theo Bloom</span></i></a><i><span >, </span></i><a href="https://twitter.com/martin_eve"><i><span >Martin Eve,</span></i></a> <a href="https://twitter.com/danielskatz"><i><span >Daniel S. Katz</span></i></a><i><span >, </span></i><a href="https://twitter.com/AmyeKenall"><i><span >Amye Kenall</span></i></a><i><span >, </span></i><a href="https://twitter.com/catmacOA"><i><span >Catriona MacCullum</span></i></a><i><span >, </span></i><a href="https://twitter.com/CameronNeylon"><i><span >Cameron Neylon</span></i></a><i><span >, </span></i><a href="https://twitter.com/marknpatterson"><i><span >Mark Patterson</span></i></a><i><span >, </span></i><a href="https://twitter.com/KristenRatan"><i><span >Kristen Ratan</span></i></a><i><span >, </span></i><a href="https://twitter.com/carlystrasser"><i><span >Carly Strasser</span></i></a><i><span >, and </span></i><a href="https://twitter.com/kaythaney"><i><span >Kaitlin Thaney</span></i></a><i><span >.</span></i></span>

<a href="/wp/blog/uploads/2016/01/You-decide-where-to-go.001.jpeg" rel="attachment wp-att-1215"><img class="wp-image-1215 aligncenter" src="/wp/blog/uploads/2016/01/You-decide-where-to-go.001-300x169.jpeg" alt="Crossref map" width="405" height="228" srcset="/wp/blog/uploads/2016/01/You-decide-where-to-go.001-300x169.jpeg 300w, /wp/blog/uploads/2016/01/You-decide-where-to-go.001-768x432.jpeg 768w, /wp/blog/uploads/2016/01/You-decide-where-to-go.001.jpeg 960w" sizes="(max-width: 405px) 85vw, 405px" /></a>
