---
title: Linking Publications to Data and Software
author: Jennifer Lin
authors:
  - Jennifer Lin
date: 2016-09-07

categories:
  - APIs
  - Citation
  - DataCite
  - Data
  - Software
  - Research Nexus

archives:
  - 2016

---
### TL;DR

Crossref and Datacite provide a service to link publications and data. The easiest way for Crossref members to participate in this is to cite data using DataCite DOIs and to include them in the references within the metadata deposit. These data citations are automatically detected. Alternatively and/or additionally, Crossref members can deposit data citations (regardless of identifier) as a relation type in the metadata. Data & software citations from both methods are freely propagated. This blog post also describes how to retrieve the links collected between publication and data & software.

<!--
<span ><a href="/wp/blog/uploads/2016/09/Data-blog-post.002-1.jpeg"><img class="alignright wp-image-2075 " src="/wp/blog/uploads/2016/09/Data-blog-post.002-1-300x199.jpeg" width="280" height="186" srcset="/wp/blog/uploads/2016/09/Data-blog-post.002-1-300x199.jpeg 300w, /wp/blog/uploads/2016/09/Data-blog-post.002-1.jpeg 542w" sizes="(max-width: 280px) 85vw, 280px" /></a></span>
-->
*****

<p align="center">
<img src="/wp/blog/uploads/2016/09/Data-blog-post.002-1-300x199.jpeg"/>
</p>

Data & software citation is good research practice (<a href="http://www.stm-assoc.org/2012_06_14_STM_DataCite_Joint_Statement.pdf">DataCite-STM Joint Statement</a> and FORCE11 <a href="https://www.force11.org/group/joint-declaration-data-citation-principles-final">Joint Declaration of Data Citation Principles</a>) and is part of the scholarly ecosystem supporting research validation and reproducibility</span><span >. Data & software citation is also instrumental in enabling the reuse and verification of these research outputs, tracking their impact, and creating a scholarly structure that recognises and rewards those involved in producing them.

<span >Crossref supports the propagation of data & software citations alongside a publisher’s standard bibliographic metadata. members deposit the data citation link as part of the overall publication metadata when registering their content. Crossref partners with DataCite and together, we jointly provide a clearinghouse for the citations collected. These are all made freely available to the community as open data.</span>

Citation practices are evolving across different communities of practice. Crossref’s offering is flexible and easily accommodates variations and changes, since it does not rely on a specific set of citation metadata elements, citation format, nor manner of credit and attribution. Publishers deposit data & software citations in their metadata deposit via a) references and/or b) relation type.

### Method A: Bibliographic references

<span >Crossref and DataCite have partnered to provide automatic linking between publications registered with Crossref and datasets bearing DataCite DOIs. This is the most efficient and effective way to ensure that data citations are fully integrated into the scholarly research information network with full and accurate metadata.</span>

<span >All data & software citations that include datasets bearing a DataCite DOI are eligible for auto-update linking with Crossref. In this method: authors cite the dataset or software containing the DataCite DOI per journal article submission guidelines and add it to the article citation list (c.f. </span>[<span >FORCE11 citation placement</span>][1]<span >, </span>[<span >FORCE11 Software Citation Principles</span>][2]<span >). Publishers then deposit references as part of their standard practice when registering content. Crossref checks every reference deposited for a DOI. If the DOI is identified as DataCite’s, we automatically link it to the article. </span>**With this method, no additional action is needed when publishers register their content with Crossref.**

Data citation links to non-DataCite DOIs can only be exposed in the references if the publisher makes references openly available. Even in the event that the data citation is shared, it remains undifferentiated from other references. Method B described below offers another approach.

### Method B: Relation type

<span >Publishers can link their publication to a variety of associated research objects as part of the article metadata directly in the metadata deposited to Crossref, including data & software, protocols, videos, published peer reviews, preprints, conference papers, etc. Doing so not only groups digital objects together, but formally associates them with the publication. Each link is a relationship and the sum of all these relationships constitutes a &#8216;</span>[<span >research article nexus</span>][3]<span >.’ Data & software citations are a valuable part of this.</span>

<span >To tag the citation in the metadata deposit, we ask for: </span>
<li >
  <span >description of dataset or software (optional) </span>
</li>
<li >
  <span >dataset or software identifier </span>
</li>
<li >
  <span >identifier type</span>
</li>
<li >
  <a href="https://support.crossref.org/hc/en-us/articles/214357426"><span >relationship type</span></a><span >. </span>
</li>
<span >Crossref can accommodate research outputs with any identifier, though we currently only validate DOI relationships during metadata processing. Technical details are documented in the </span>[<span >Data & Software Citations Deposit Guide</span>][4]<span >. </span>

### Combining methods increases total available citations

<span >The two methods are independent and can be used exclusively or jointly. Each caters to a different set of conditions and their practical considerations. See </span>[<span >the comparison of benefits and limitations</span>][5] <span >for each method in the deposit guide. We recommend that publishers use both methods where possible at this time for optimum specificity and coverage. </span>

### How to access data & software citations

<span >Crossref and DataCite make the data & software citations deposited by Crossref members and DataCite data repositories openly available to a wide host of parties, including both Crossref and DataCite communities as well as the extended research ecosystem (funders, research organisations, technology and service providers, research data frameworks such as Scholix, etc.).</span>

<span >Data & software citations from references can be accessed via the </span>[<span >Crossref Event Data API</span>][6] <span > Citations included directly into the metadata by relation type can be accessed via </span>[<span >Crossref’s APIs</span>][7] <span >in a number of formats (REST, OAI-­PMH, OpenURL). (A single channel containing data & software citations across interfaces is in development and will be released next year.)</span>

<span >Publishers, visit our detailed </span>[<span >guide on how to deposit data and software citations</span>][4]<span >. We welcome your questions and concerns at </span>[<span >feedback@crossref.org</span>][8]<span >.</span>

&nbsp;

_<span >Special thanks to the following who provided valuable feedback in developing the guide: Martin Fenner (DataCite), Amye Kenall (Springer Nature), Brooks Hanson (AGU), Shelley Stall (AGU), and the </span>_[_<span >FORCE11 Data Citation Implementation Pilot publisher’s subgroup</span>_][9]_<span >.</span>_

 [1]: https://web.archive.org/web/20171019061351/https://force11.org/node/4771
 [2]: https://www.force11.org/software-citation-principles
 [3]: /blog/the-article-nexus-linking-publications-to-associated-research-outputs/
 [4]: http://support.crossref.org/hc/en-us/articles/215787303-Crossref-Data-Software-Citation-Deposit-Guide-for-Publishers
 [5]: http://support.crossref.org/hc/en-us/articles/215787303#benefits
 [6]: http://eventdata.crossref.org/guide/
 [7]: http://support.crossref.org/hc/en-us/articles/213420286
 [8]: mailto:Feedback@crossref.org
 [9]: https://web.archive.org/web/20201024154446/https://force11.org/group/dcip/eg3publisherearlyadopters
