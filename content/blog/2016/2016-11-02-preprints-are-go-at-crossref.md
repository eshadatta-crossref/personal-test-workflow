---
title: Preprints are go at Crossref!
author: Rachael Lammey
authors:
  - Rachael Lammey
date: 2016-11-02

categories:
  - Persistence
  - Preprints
archives:
  - 2016

---

<span >We’re excited to say that we’ve finished the work on our infrastructure to allow members to register preprints. Want to know why we’re doing this? Jennifer Lin </span>[<span >explains the rationale in detail</span>][2] <span >in an earlier post, but in short we want to help make sure that:</span>

* links to these publications persist over time
* they are connected to the full history of the shared research results
* the citation record is clear and up-to-date

Doing so will help fully integrate preprint publications into the formal scholarly record.

 <!--more-->

## What’s new?

<span >We’ve had to do some work on our own infrastructure to facilitate the inclusion of preprints, enabling: </span>

* Crossref membership for preprint repositories by updating our membership criteria and creating a </span><a href="http://support.crossref.org/hc/en-us/articles/213126346-Posted-content-includes-preprints-#policies"><span >policies</span></a><span > for preprints</span>
* The deposit of persistent identifiers for preprints to ensure successful links to the scholarly record over the course of time via the DOI resolver.
* Content Registration for preprints with </span><a href="http://support.crossref.org/hc/en-us/articles/213126346-Posted-content-includes-preprints-#depositing"><span >custom metadata</span></a><span > that reflect researcher workflows from preprint to formal publication (this custom metadata will then be visible to anyone using the Crossref metadata).
* Notification of links between preprints and formal publications that may follow (journal articles, monographs, etc.).
* <a href="https://info.orcid.org/auto-update-has-arrived-orcid-records-move-to-the-next-level/"><span >Auto-update of ORCID records</span></a><span > to ensure that preprint contributors get credit for their work.
* <a href="/blog/a-healthy-infrastructure-needs-healthy-funding-data/"><span >Preprint and funder registration</span></a><span > to automatically report research contributions based on funder and grant identification.
* It will also allow for the collection of “</span><a href="/blog/crossref-event-data-early-preview-now-available/"><span >event data</span></a><span >” that capture activities surrounding preprints (usage, social shares, mentions, discussions, recommendations, links to datasets and other research entities, etc.).
</li>

Now we’re ready to go!

## Early adopters

<span >We have been working with various preprint publishers who are launching (or planning to launch) their own preprint initiatives. </span>

<span >Preprints.org is the first to successfully make preprints deposits using the dedicated schema. For example, this preprint </span>[<span >https://doi.org/</span><span >10.20944/preprints201608.0191.v1</span>][3] <span >is registered with Crossref. It is linked to a published journal article </span>[<span >https://doi.org/10.3390/data1030014</span>][4] <span >both in the online display as well </span>[<span >the preprint’s Crossref metadata record</span>][5]<span >. Others are getting ready to go - will your organisation be next? (Technical documentation available </span>[<span >here</span>][6]<span >.)</span>

> Martyn Rittman, from Preprints, operated by MDPI said: Preprints.org is delighted to be the very first to integrate the Crossref schema for preprints. We believe it is an important step in allowing working papers and preliminary results to be fully citable as soon as they are available. It also makes it easy to link to the final peer-reviewed version, regardless of where it is published. Thanks to the hard work of Crossref and clear documentation, the schema was very simple to implement and has been applied retrospectively to all preprints at Preprints.org.</span>

> Jessica Polka, Director, ASAPbio adds: ASAPbio is a scientist-driven community initiative to promote the productive use of preprints in the life sciences. We’re thrilled to see Crossref’s development of a service that enables preprints to better contribute to the scholarly record. This infrastructure lays a necessary foundation for increasing acceptance of preprints as a valuable form of scientific communication among biologists.</span>

## Questions?

[<span >Get in touch</span>][7] <span >with any questions or comments, or join our upcoming </span>[<span >webinar</span>][8] <span >to talk about preprints, infrastructure and where we go from here. </span>

 [1]: /wp/blog/uploads/2016/10/As-content-evolves-connections-persist-and-new-links-are-added.png
 [2]: /blog/getting-ready-to-run-with-preprints-any-day-now
 [3]: https://doi.org/10.20944/preprints201608.0191.v1
 [4]: https://doi.org/10.3390/data1030014
 [5]: https://api.crossref.org/v1/works/10.20944/preprints201608.0191.v1/transform/application/vnd.crossref.unixsd+xml
 [6]: http://support.crossref.org/hc/en-us/articles/213126346-Posted-content-includes-preprints-
 [7]: mailto:feedback@crossref.org
 [8]: https://attendee.gotowebinar.com/register/7523925461867007490
