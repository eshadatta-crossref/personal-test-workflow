---
title: 'Included, registered, available: let the preprint linking commence.'
author: Rachael Lammey
authors:
  - Rachael Lammey
date: 2016-12-05

categories:
  - APIs
  - Linking
  - Metadata
  - Preprints
archives:
  - 2016

---

We [began accepting preprints](/blog/preprints-are-go-at-crossref) as a new content type last month (in a category known as “posted content” in our XML schema). Over 1,000 records have already been registered in the first few weeks since we launched the service.

By extending our existing services to preprints, we want to help make sure that:

* links to these publications persist over time
* they are connected to the full history of the shared research
* the citation record is clear and up-to-date.<!--more-->

It’s not just collecting the metadata however, it’s also making it available so that it can be as widely used as possible. Preprint metadata is no different. As with all supported content types, we make the metadata available for machine and human access, across multiple interfaces (e.g. [REST API](https://github.com/Crossref/rest-api-doc/blob/master/rest_api.md), [OAI-PMH](https://support.crossref.org/hc/en-us/articles/213679866-OAI-PMH-subscriber-only), [Crossref Metadata Search](https://web.archive.org/web/20131229210637/http://search.crossref.org//))

For example, you can see information on the preprint [https://doi.org/10.20944/preprints201608.0191.v1](https://doi.org/10.20944/preprints201608.0191.v1) in a number of ways:

* [https://api.crossref.org/v1/works/10.20944/preprints201608.0191.v1/transform/application/vnd.crossref.unixsd+xml](https://api.crossref.org/v1/works/10.20944/preprints201608.0191.v1/transform/application/vnd.crossref.unixsd+xml)
* [https://web.archive.org/web/20131229210637/http://search.crossref.org//?q=10.20944%2Fpreprints201608.0191.v1](https://web.archive.org/web/20131229210637/http://search.crossref.org//?q=10.20944%2Fpreprints201608.0191.v1)

If you want to see all the preprint metadata deposited so far, try [https://api.crossref.org/v1/types/posted-content/works](https://api.crossref.org/v1/types/posted-content/works). Over 1,000 records have already been registered in the first few weeks since we launched the service.

Crossref members depositing preprints need to make sure they:

* Register content using the [posted content](https://support.crossref.org/hc/en-us/articles/213126346-Posted-content-includes-preprints-#examples) metadata schema.
* Respond to our match notifications that a manuscript / version of record (AM/VOR) has been registered and link to that within seven days.
* Label the manuscript as a preprint clearly, above the scroll on the preprint landing page, and ensure that any link to the AM/VOR is also prominently displayed above the scroll.

> It’s important to clearly label the content type so we can ensure that the connections between preprints and the associated literature are clearly visible, to both humans and machines.

As with other content types, there is a registration fee to include content in the Crossref system. For preprints, it’s $0.25 fee for current preprint files and $0.15 for backfiles.

Are you an existing Crossref member who wants to assign preprint DOIs? [Let's talk about](mailto:support@crossref.org) getting started or migrating any existing content over to the dedicated preprint deposit schema.

Interested in becoming a Crossref member to assign DOIs to your preprints? [Contact our membership specialist](mailto:member@crossref.org) so we can answer any questions and get you set up as a member.
