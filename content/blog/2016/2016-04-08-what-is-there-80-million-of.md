---
title: What are there 80 million of?
author: Ginny Hendricks
authors:
  - Ginny Hendricks
date: 2016-04-08

categories:
  - Member Briefing
  - News Release
tags:
  - 80 million
  - 80000000
  - Crossref80mil
archives:
  - 2016

---
<span ><span >As of this week, there are 80,000,000 scholarly items registered with Crossref!</span></span>

<span >By the way, we update <a href="https://data.crossref.org/reports/statusReport.html">these interesting Crossref stats</a> regularly and you can <a href="https://web.archive.org/web/20131229210637/http://search.crossref.org//">search the metadata</a>.</span>

<span >The 80 millionth scholarly item is [drumroll&#8230;] <a href="http://doi.org/10.12816/0016504">Management Approaches in Beihagi History</a> from the journal <em>Oman Chapter of Arabian Journal of Business and Management Review</em><span class="s1">, p</span><span class="s1">ublished by <strong>Al Manhal</strong> in the United Arab Emirates.</span></span>

There have been loads of changes since Wiley registered "Designer selves: Construction of technologically mediated identity within graphical, multiuser virtual environments" with the DOI `http://dx.doi.org/10.1002/(SICI)1097-4571(1999)50:10<855::AID-ASI3>3.0.CO;2-6)`, which happens to have been Crossref’s first official DOI (after many prototype deposits).

<span ><span ><a href="/wp/blog/uploads/2016/04/Trending-Nations.png" rel="attachment wp-att-1507"><img class="alignright wp-image-1507" src="/wp/blog/uploads/2016/04/Trending-Nations-300x198.png" alt="Crossref Membership - Trending Nations" width="401" height="265" srcset="/wp/blog/uploads/2016/04/Trending-Nations-300x198.png 300w, /wp/blog/uploads/2016/04/Trending-Nations-768x508.png 768w, /wp/blog/uploads/2016/04/Trending-Nations.png 978w" sizes="(max-width: 401px) 85vw, 401px" /></a>In the beginning, most of our new members came from the United States and Europe.  Now, lots of our members and affiliates come from other parts of the world.<br /> </span></span><span ><a href="/wp/blog/uploads/2016/04/Crossref-Membership-Trending-Nations.png" rel="attachment wp-att-1503"><br /> </a></span>

<span >Ed Pentz was Crossref’s first (and only) employee in February 2000. Now it takes 30 of us to manage the 80 million records and over 5,300 participating organizations and to work on projects like <a href="/blog/event-data-open-for-your-interpretation/"><span >Crossref Event Data</span></a><span >,  </span><a href="/blog/community-responses-to-our-proposal-for-early-content-registration/"><span >'early content registration' </span></a><span >, and all the new stuff you’ll be hearing about later this year</span>.</span>

<span >Maybe in the context of social media services (e.g. Facebook users) 80,000,000 does not seem like such a big number. But 80,000,000 is an important milestone. Just think &#8212; </span>

<span >There are also <a href="http://doi.org/10.1186/2049-2618-2-41">80 million microbes in a 10 second kiss</a> [<span class="JournalTitle"><em>Microbiome</em>, </span><span class="ArticleCitation_Year">2014, </span><span class="ArticleCitation_Volume">2:41, </span>Kort et al].</span>

<span >And after <a href="http://dx.doi.org/10.1515/9781400874248" target="_blank">80 million years of extinction events</a>, we’re all still here!  </span>

## <span ><strong>What else is 80 million?</strong> Tell us in a tweet using <a href="https://twitter.com/search?f=tweets&q=%23crossref80mil&src=typd">#Crossref80mil</a>. There may be a prize!</span><figure id="attachment_1482"  class="wp-caption alignnone">

<a href="/wp/blog/uploads/2016/04/2.png" rel="attachment wp-att-1482"><img class="wp-image-1482 size-medium" src="/wp/blog/uploads/2016/04/2-300x150.png" alt="Crossref has 80 million registered content items" width="300" height="150" srcset="/wp/blog/uploads/2016/04/2-300x150.png 300w, /wp/blog/uploads/2016/04/2-768x384.png 768w, /wp/blog/uploads/2016/04/2.png 1024w" sizes="(max-width: 300px) 85vw, 300px" /></a><figcaption class="wp-caption-text">Crossref has 80 million registered content items</figcaption></figure>
