---
title: Crossref’s Annual Meeting is now Crossref LIVE16
author: April Ondis
authors:
  - April Ondis
  - Ginny Hendricks
date: 2016-09-02
categories:
  - Crossref LIVE
  - Annual Meeting
  - Community
  - Collaboration
archives:
  - 2016

---
Everyone is invited to our free annual event this 1-2 November in London. [(Register here)](https://www.eventbrite.com/e/crossref-live16-registration-25928526922?aff=ehomesaved)!

</p>

<p class="p1">
  In years past, only Crossref members typically attended the [Crossref Annual Meeting](/crossref-live-annual). <span class="s1" >This year, we looked at the event with new eyes. We realized that we’d have even richer conversations, more creative energy, and the meeting would be even better for our members if we could rally the entire community together.  So we decided to re-develop our annual event from the ground-up. </span>
</p>

<p class="p1">
  <span class="s1" ><a href="/wp/blog/uploads/2016/08/crossref_live16_rgb.jpg"><img class="size-medium wp-image-2008 alignleft" src="/wp/blog/uploads/2016/08/crossref_live16_rgb-300x115.jpg" alt="Logo for Crossref LIVE 16" width="300" height="115" srcset="/wp/blog/uploads/2016/08/crossref_live16_rgb-300x115.jpg 300w, /wp/blog/uploads/2016/08/crossref_live16_rgb-768x295.jpg 768w, /wp/blog/uploads/2016/08/crossref_live16_rgb-1024x393.jpg 1024w, /wp/blog/uploads/2016/08/crossref_live16_rgb-1200x461.jpg 1200w" sizes="(max-width: 300px) 85vw, 300px" /></a></span>
</p>

<p class="p1">
  <span class="s1" >The result is Crossref LIVE16, an event with a new format and a new focus on the entirety of the scholarly communications community.  We are opening doors for the whole community, welcoming publishers, librarians, researchers, funders, technology providers, and Crossref members alike. </span>
</p>

<ul class="ul1">
  <li class="li1">
    <span ><b></b><span class="s1"><b>1st November - Mashup Day, from 12 noon</b>: an afternoon of interactive activities including mingling with the Crossref team and special guests, trying out our services, live troubleshooting, and exclusive previews of some exciting things we’re working on. Plus entertainment and refreshments at an early evening reception.</span></span>
  </li>
</ul>

<ul class="ul1">
  <li class="li1">
    <span ><b></b><span class="s1"><b>2nd November - Conference Day</b>: a full-day plenary session with distinguished keynote speakers including <a href="http://nycdh.org/members/ah160/">April Hathcock</a> (NYU), <a href="https://strasser.github.io/">Carly Strasser</a> (Moore Foundation), <a href="https://www.digital-science.com/people/ian-calvert/">Ian Calvert</a> (Digital Science), and <a href="https://meta.wikimedia.org/wiki/User:Dario_(WMF)">Dario Taraborelli</a> (Wikimedia Foundation). We will provide the most important updates about our services, and share our vision and strategies for the future.</span></span>
  </li>
</ul>

<p class="p1">
  <span class="s1" ><em>Note:</em> You are welcome to join us for both days or just one day, as you like.</span>
</p>

<p class="p1">
  <span class="s1" ><b>Location: </b>The Royal Society, London, UK.   </span>
</p>

<p class="p1">
  <span ><b></b><span class="s1">We hope you will join us, and extend this invitation to your colleagues.</span></span>
</p>

<p class="p1">
  <span class="s1" >This is going to be fun.</span>
</p>

<p class="p1">
  <span ><a href="https://www.eventbrite.com/e/crossref-live16-registration-25928526922?aff=ehomesaved"><span class="s1"><b>Register here</b></span></a></span>
</p>
