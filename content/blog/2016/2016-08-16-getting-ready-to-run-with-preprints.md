---
title: Getting ready to run with preprints, any day now
author: Jennifer Lin
authors:
  - Jennifer Lin
date: 2016-08-16

categories:
  - Content Registration
  - Preprints
archives:
  - 2016

---

{{% imagewrap right %}}<img src="/wp/blog/uploads/2016/08/Preprints-ready-to-go-shoelaces.jpg" alt="run" width="300" height="200" />{{% /imagewrap %}}


While preprints have been a formal part of scholarly communications for decades in certain communities, they have not been fully adopted to date across most disciplines or systems. That may be changing very soon and quite rapidly, as new [initiatives](https://en.wikipedia.org/wiki/Preprint#Preprint_server_by_research_field) come thick and fast from researchers, funders, and publishers alike. This flurry of activity points to the realization from these parties of preprints’ potential benefits:

<li >
  <span >Accelerating the sharing of results; </span>
</li>
<li >
  <span >Catalyzing research discovery; </span>
</li>
<li >
  <span >Establishing priority of discoveries and ideas; </span>
</li>
<li >
  <span >Facilitating career advancement; and </span>
</li>
<li >
  <span >Improving the culture of communication within the scholarly community. </span>  
</li>  

<span >To acknowledge them as a legitimate part of the research story, we need to fully build preprints into the broader research infrastructure. Preprints need infrastructure support just like journal articles, monographs, and other formal research outputs. Otherwise, we (continue to) have a <span >two-tiered scholarly communications system</span>, unlinked and operating independently.<br /> </span>

##### <span ><b>Infrastructure for preprints</b></span>

<span ><span >For this reason, the team at Crossref is extending its infrastructure services to </span><a href="/blog/members-will-soon-be-able-to-assign-crossref-dois-to-preprints/"><span >allow members to register preprints</span></a><span >. This new development is designed to provide custom support for preprints. It will ensure that: links to these publications persist over time; they are connected to the full history of the shared research results; and the citation record is clear and up-to-date. We established this preprints service to fully integrate preprint publications into the formal scholarly record with features such as:</span></span>

<li >
  <span >Crossref membership for preprint repositories, joining the community of publishers who have made a commitment to maintain and connect scholarly publications.</span>
</li>
<li >
  <span >Persistent identifiers for preprints to ensure successful links to the scholarly record over the course of time via the DOI resolver.</span>
</li>
<li >
  <span >Content Registration for preprints with custom metadata that reflect researcher workflows from preprint to formal publication.</span>
</li>
<li >
  <span >Notification of links between preprints and formal publications that may follow (journal articles, monographs, etc.).</span>
</li>
<li >
  <span ><span >Collection of “</span><a href="/blog/crossref-event-data-early-preview-now-available/"><span >event data</span></a><span >” that capture activities surrounding preprints (usage, social shares, mentions, discussions, recommendations, links to datasets and other research entities, etc.).</span></span>
</li>
<li >
  <span ><span >Reference linking for preprints, connecting up the scholarly record to associated literature</span></span>
</li>
<li >
  <span ><span ><a href="https://info.orcid.org/auto-update-has-arrived-orcid-records-move-to-the-next-level/">Auto-update of ORCID records</a></span><span > to ensure that preprint contributors get credit for their work.</span></span>
</li>
<li >
  <span ><a href="/blog/a-healthy-infrastructure-needs-healthy-funding-data/"><span >Preprint and funder registration</span></a><span > to automatically report research contributions based on funder and grant identification.</span></span>
</li>

##### <span ><b>Supporting utility & effectiveness of preprints for all</b></span>

<span ><span >To build the service, we are listening to the research community tell us their vision of what preprints will do. </span><a href="/blog/hello-preprints-whats-your-story/"><span >We solicited </span></a><span >use cases from the community and have built a </span><a href="https://docs.google.com/spreadsheets/d/1UoTuzVVFe5qdMGenxAAbD9xEDOrnxuqpi29tO-frMXU/edit#gid=488933191"><span >registry of preprint user stories</span></a><span > with researchers, publishers, funding agencies, tenure and promotion committees in academic institutions, and technology providers. </span></span>

<span >To realize the user stories, the research enterprise will no doubt need brand new tools and existing systems enhancements. Crossref’s preprints infrastructure will support the development of all needs currently registered. The community at large can focus on building effective solutions, instead of finding or securing access to data. All data are available without restriction to all so that participants as well the services and systems supporting them can access the data and reuse it for advancing early dissemination, literature discovery, research tracking, promotion and funding assessment, etc. </span>

<span >These are exciting days for scholarly communications. Over time, we envision an even more vibrant ecosystem of research outputs that include existing artefacts linked up to preprints. And Crossref is committed to providing infrastructure for the dynamic enterprise all along the way.</span>

<span ><span >We plan to announce the availability of the preprints infrastructure and further technical details within the next few weeks. If you’re interested in learning more about how these will be supported, </span><a href="mailto:feedback@crossref.org"><span >get in touch</span></a></span><span ><span >!</span> </span>

&nbsp;
