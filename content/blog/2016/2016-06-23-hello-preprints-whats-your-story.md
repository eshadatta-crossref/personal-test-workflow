---
title: Hello preprints, what’s your story?
author: Jennifer Lin
authors:
  - Jennifer Lin
date: 2016-06-23

categories:
  - Content Registration
  - Preprints
archives:
  - 2016

---
### <span >The role of preprints</span>

<span ><span >Crossref provides infrastructure services and therefore we support scholarly communications as it evolves over time. Today, preprints are increasingly discussed as a valuable part of the research story (beyond physics, math, and a small set of sub-disciplines). Preprints might play a positive role in catalyzing research discovery, establishing priority of discoveries and ideas, facilitating career advancement, and improving the culture of communication within the scholarly community. </span> </span>

<span ><span >As we shared in an earlier blog post last month, </span><a href="/blog/members-will-soon-be-able-to-assign-crossref-dois-to-preprints/"><span >members will be able to register Crossref DOIs for preprints</span></a><span > later this year. We will connect the full history of a research work, and ensure the citation record is clear and up-to-date. As we build out this new content type, we’d love to hear how the research community envisions what preprints will do.</span></span>

### <span >What’s your story, preprint?</span>

<span ><span >So we can develop a service that supports the whole host of potential uses for all stakeholders, we ask the entire research community to contribute </span><b>preprints user stories</b><span >. </span><a href="https://en.wikipedia.org/wiki/User_story"><span >User stories</span></a><span > are concrete descriptions of a specific need, typically used in technology development: </span><i><span >As a [x], I want to [y] to that I can [z]</span></i><span >. User stories take the “end-user’s” perspective as they focus on a discrete result and its value. They are essential when implementing solutions that must meet a wide range of needs, across a diverse set of constituents. For e</span>xample:</span>

> <span >As an author, I want to share results before my paper is submitted to a journal so that I can get rapid feedback on it and make improvements before publication.</span>
>
> <span >As a researcher who is part of a tenure and promotion committee or funder review panel, I want to know the reach of early results published from the candidate so that I can more quickly track the impact of results, rather than relying only on journal articles that take much longer to publish.</span>
>
> <span >As a journal publisher, I want to know whether a preprint exists for a manuscript submitted to me so that I can decide whether I will accept the submission based on my editorial policy.</span>

<span ><span >We aim to assemble a full catalog that cuts across research disciplines and stakeholder groups. We want to hear from you: </span><b>researchers, publishers, funding agencies, scholarly societies, academic institutions, technology providers, other infrastructure providers</b>, etc<span >.</span></span>

### <span >Tell us your story here</span>

<span ><span >To ensure that your needs are included, please send us your user stories via this </span><a href="https://docs.google.com/spreadsheets/d/1UoTuzVVFe5qdMGenxAAbD9xEDOrnxuqpi29tO-frMXU/edit#gid=488933191"><span >user story &#8220;deposit&#8221; form</span></a><span >. They will be added to the </span><a href="https://docs.google.com/spreadsheets/d/1UoTuzVVFe5qdMGenxAAbD9xEDOrnxuqpi29tO-frMXU/edit#gid=488933191"><span >full registry of contributions</span></a><span > from the community, which we hope will serve as a key resource for all those developing preprints into a core part of scholarly communications (e.g., ASAPbio, etc.).</span></span>
