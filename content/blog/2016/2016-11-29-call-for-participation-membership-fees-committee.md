---
title: 'Call for participation: Membership & Fees Committee'
author: Ed Pentz
authors:
  - Ed Pentz
date: 2016-11-29

categories:
  - Committees
  - Member Briefing
  - Outreach
archives:
  - 2016

---
Crossref was founded to enable collaboration between publishers.  As our [membership has grown and diversified over recent years](/blog/the-membership-boom-why-metadata-isnt-like-beer/), it’s becoming even more vital that we take input from a representative cross-section of the membership. This is especially important when considering how fees and policies will affect our diverse members in different ways.<!--more-->

## About the M&F Committee

The Membership & Fees Committee (M&F Committee) was established in 2001 and plays an important role in Crossref’s governance.  Made up of 10-12 organizations of both board members and regular members, the group makes recommendations to the board about fees and policies for all of our services. They regularly review existing fees to discuss if any changes are needed. They also review new services while they are being developed, to assess if fees should be charged and if so, what those fees should be. For example, the committee recently made recommendations to the board about the fees for a new service called Event Data that we’ll launch soon, and the Content Registration fees for preprints - our newest content type.  In addition, the board can also ask the committee to address specific issues about policies and services. Increasingly, the committee works with the outreach team to include research and survey insights.

## About committee participation

The M&F Committee meets via one-hour conference calls about six times a year, although this can vary depending on what issues the committee is considering. Often proposals are developed by staff and then reviewed and discussed by the committee - so there is reading to do in preparation for the calls.<a href="/wp/blog/uploads/2016/11/header-chairs.jpg"><img class="alignright wp-image-2393 size-large" src="/wp/blog/uploads/2016/11/header-chairs-1024x509.jpg" alt="Join a Crossref committee" width="840" height="418" srcset="/wp/blog/uploads/2016/11/header-chairs-1024x509.jpg 1024w, /wp/blog/uploads/2016/11/header-chairs-300x149.jpg 300w, /wp/blog/uploads/2016/11/header-chairs-768x382.jpg 768w, /wp/blog/uploads/2016/11/header-chairs-1200x596.jpg 1200w" sizes="(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px" /></a>

This is very important work and in order to ensure that the committee is broadly representative of Crossref’s diverse membership we are seeking expressions of interest from members who would like to serve on the M&F Committee for 2017. Appointments are for one year and members can serve multiple terms.

## About you

In view of our commitment to be representative of the membership we are refreshing the committee and want to have engaged and interested people from a diverse set of members join.

If you are interested in joining the committee and helping Crossref fulfil its mission please email <a href="mailto:feedback@crossref.org">feedback@crossref.org</a> with your name, title, organization and a short statement about why you want to serve on the committee by December 19th, 2016.      

<strong>Scott Delman, Director of Group Publishing, ACM is the current Chair of the committee and will review the expressions of interest with me, Ed Pentz, Executive Director, to form the committee.</strong>

Thanks for your interest.
