---
title: 'Crossref metadata for bibliometrics'
author: Ginny Hendricks
draft: false
authors:
  - Ginny Hendricks
  - Dominika Tkaczyk
  - Patricia Feeney
date: 2020-02-21
categories:
  - Metadata
  - Bibliometrics
  - Citation Data
  - APIs
  - API Case Study
archives:
    - 2020
---

Our paper, [Crossref: the sustainable source of community-owned scholarly metadata](https://doi.org/10.1162/qss_a_00022), was recently published in [_Quantitative Science Studies_ (MIT Press)](https://www.mitpressjournals.org/loi/qss). The paper describes the scholarly metadata collected and made available by Crossref, as well as its importance in the scholarly research ecosystem.

<!--more-->

Containing over 106 million records and expanding at an average rate of 11% a year, Crossref's metadata has become one of the major sources of scholarly data for publishers, authors, librarians, funders, and researchers. The metadata set consists of 13 content types, including not only traditional types, such as journals and conference papers, but also data sets, reports, preprints, peer reviews, and grants. The metadata is not limited to basic publication metadata, but can also include abstracts and links to full text, funding and license information, citation links, and the information about corrections, updates, retractions, etc. This scale and breadth make Crossref a valuable source for research in scientometrics, including measuring the growth and impact of science and understanding new trends in scholarly communications. The metadata is available through a number of APIs, including REST API and OAI-PMH.

In the paper, we describe the kind of metadata that Crossref provides and how it is collected and curated. We also look at Crossref's role in the research ecosystem and trends in metadata curation over the years, including the evolution of its citation data provision. We summarize the research that used Crossref's metadata and describe plans that will improve metadata quality and retrieval in the future.
