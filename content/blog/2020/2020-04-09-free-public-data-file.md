---
title: 'Free public data file of 112+ million Crossref records'
author: Jennifer Kemp
draft: false
authors:
  - Jennifer Kemp
date: 2020-04-09
categories:
  - Metadata
  - Community
  - APIs

archives:
    - 2020
---

A lot of people have been using our public, open APIs to collect data that might be related to COVID-19. This is great and we encourage it. We also want to make it easier. To that end we have made a free data file of the public elements from Crossref’s 112.5 million metadata records.  

The file (65GB, in JSON format) is available via Academic Torrents here:  https://doi.org/10.13003/83B2GP  

**_It is important to note that [Crossref metadata](/education/retrieve-metadata/) is always openly available._** The difference here is that we’ve done the time-saving work of putting all of the records registered through March 2020 into one file for download.

The sheer number of records means that, though anyone can use these records anytime, downloading them all via our APIs can be quite time-consuming. We hope this saves the research community valuable time during this crisis.
## A few important notes

+ **All records are included.** In other words, the data file has every DOI ever registered with Crossref through March 31st, 2020. _This means it’s a large file, 65GB._
  + Metadata is supplied by our members and, as such, not all records have the same completeness (or quality) of metadata. Bibliographic metadata is generally [required](https://support.crossref.org/hc/en-us/articles/213077846-Required-Recommended-and-Optional-Elements). All other metadata, e.g. license and funding information, ORCIDs, etc. is optional (though very much encouraged).
  + [References](/documentation/principles-practices/) (i.e. authors’ cited sources) are also optional metadata. Nearly 50 million records include references and, of those, nearly 30 million have open references that are included in the data file. “Limited” and “Closed” references are not included in the data file. _[EDIT 6th June 2022 - all references are now open by default with the March 2022 board vote to remove any restrictions on reference distribution]._
  + If an error in the metadata is found, please report it directly to the publisher to correct.


+ **The records are in JSON.**

+ **New and updated records can be added incrementally** using our REST API, which includes a number of date filter options, e.g. index-date.  

+ **No registration is required** to use our [REST API](https://github.com/CrossRef/rest-api-doc) but we do strongly encourage being a [‘polite’](https://github.com/CrossRef/rest-api-doc#etiquette) (i.e. identified) user. It makes troubleshooting much easier and reduces the chance of negatively impacting other users.

Questions, comments and feedback are welcome at [support@crossref.org](mailto:support@crossref.org).  

We thank AcademicTorrents.com for helping us make this data available.
And we are grateful for the incredible efforts of everyone working to support research everywhere--stay safe and well.
