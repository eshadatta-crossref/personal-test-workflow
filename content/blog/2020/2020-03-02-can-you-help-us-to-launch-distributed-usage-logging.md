---
title: 'Can you help us to launch Distributed Usage Logging?'
author: Kirsty Meddings
draft: false
authors:
  - Kirsty Meddings
date: 2020-03-02
categories:
  - Members
  - Community
  - Collaboration
  - Standards
archives:
    - 2020
---
Update: Deadline extended to 23:59 (UTC) 13th March 2020.

[Distributed Usage Logging](/community/project-dul/) (DUL) allows publishers to capture traditional usage activity related to their content that happens on sites other than their own so they can provide reports of “total usage”, for example to subscribing institutions, regardless of where that usage happens.

<!--more-->

We are looking for a consultant to take the lead with DUL outreach, promoting the service and its benefits in order to solicit participation from publishers (receivers) and content-hosting platforms/scholarly collaboration networks (senders).

Crossref provides the infrastructure for DUL. The call for participation is being led by COUNTER and the selected consultant will be representing COUNTER, with additional support from Crossref

If you are interested in this opportunity, please download the [request for information](https://www.projectcounter.org/wp-content/uploads/2020/02/FINAL-RFI_-Distributed-Usage-Logging-DUL-Outreach-Consultant-1.pdf) (RFI).

The RFI response deadline is 23:59 (UTC) 13 March 2020.
