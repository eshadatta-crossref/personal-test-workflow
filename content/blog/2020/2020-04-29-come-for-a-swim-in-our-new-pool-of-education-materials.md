---
title: 'Come for a swim in our new pool of Education materials'
author: Laura J Wilkinson
draft: false
authors:
  - Laura J Wilkinson
date: 2020-04-29
categories:
  - Metadata
  - Education
  - APIs

archives:
    - 2020
---


After 20 years in operation, and as our system matures from experimental to foundational infrastructure, it’s time to review our documentation.

Having a solid core of education materials about the *why* and the *how* of Crossref is essential in making participation possible, easy, and equitable.

As our system has evolved, our membership has grown and diversified, and so have our tools - both for depositing metadata with Crossref, and for retrieving and making use of it.

Our new documentation gives the full picture, with each chapter explaining an aspect of Crossref and why it matters, followed by instructions on how to participate. As far as possible, these instructions are given for each of our deposit and retrieval methods.

The revised documentation has been edited for use of simple English, and consistent terminology. Specialist vocabulary is explained as it is introduced. Understanding what’s involved across the full range of Crossref services can often seem complicated. This makes the documentation easier for readers, and provides a good basis for human and machine translations.

The chapters and sections are modular, so you can approach and combine them in different ways according to your existing knowledge and what you wish to learn. This [Choose Your Own Adventure](https://en.wikipedia.org/wiki/Choose_Your_Own_Adventure) style means that sections don't overlap, avoiding problems of repetition and versioning, and helping us to keep the information current.

The revised documentation includes several new topics, including: 

-   [The importance of metadata](/education/metadata/), explaining why you might register metadata for different purposes (discoverability, research integrity, reproducibility, and reporting and assessment)

-   [Persistent identifiers (PIDs)](/education/metadata/persistent-identifiers/), explaining the structure of a DOI, and how you might use DOIs at different levels

-   [Choosing which way to register your content](/education/member-setup/choose-content-registration-method/), including suggested DOI registration workflow and suffix generator to make life easier

-   [Introduction to types of metadata](/education/content-registration#00116), including descriptive (bibliographic), administrative, and structural 

-   [Version control, corrections, and retractions](/education/crossmark/version-control-corrections-and-retractions/), including publication stages and DOIs

-   [Metadata stewardship](/documentation/register-maintain-records/), including maintaining your metadata, reports, understanding your member obligations, and maintaining your Crossref membership.

This new documentation is part of our efforts to make Crossref participation possible, easy, and rewarding for our members large and small, all over the world. It provides a concrete basis on which to build further education and outreach projects in the future. New members will start to see our paced member onboarding program, introducing them to parts of the documentation as and when it's useful to them. And like the rest of the Crossref website, it's all [licensed for reuse under CC-BY](https://creativecommons.org/licenses/by/4.0/).  

I would like to say a big thank you to the members of the Education Task Force, who helped guide the development of the new documentation, representing a diverse range of Crossref members large and small from around the world:
- Anjum Sherasiya - India, Editor-in-Chief of Veterinary World, Crossref Ambassador
- Budi Setiawan - Indonesia, Poltekkes Kemenkes Yogyakarta
- Caroline Breul - USA, BioOne
- Isabel Recavarren - Peru, Consejo Nacional de Ciencia, Tecnología e Innovación Tecnológica (CONCYTEC), Crossref Ambassador
- Mike Nason - Canada, Public Knowledge Project (PKP) and University of New Brunswick  
- Nadine van der Merwe - South Africa, Academy of Science of South Africa (ASSAf)  
- Roberto Camargo - Brazil, Associação Brasileira de Editores Científicos (ABEC)
- Sioux Cumming - UK, INASP
- Taeil Kim - South Korea, Korean Association of Medical Journal Editors (KAMJE)
- and from Crossref: Amanda, Esha, Geoffrey, Ginny, Isaac, Kirsty, Patricia, and Susan.  

Please explore the [new documentation](/education/), give us your feedback using the yellow "Docs feedback" button at the bottom of each page, and share this update to spread the word!
