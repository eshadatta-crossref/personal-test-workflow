---
title: 'Metadata Manager Update'
author: Bryan Vickery
draft: false
authors:
  - Bryan Vickery
date: 2020-03-24
categories:
  - Metadata
  - Content Registration
  - Identifiers
archives:
    - 2020
---

At Crossref, we're committed to providing a simple, usable, efficient and scalable web-based tool for registering content by manually making deposits of, and updates to, metadata records. Last year we launched Metadata Manager in beta for journal deposits to help us explore this further. Since then, many members have used the tool and helped us better understand their needs.

<!--more-->

What we've learned has made us realize how useful such a tool can be to both large and small publishers, but also that the approach we took with Metadata Manager needs to be changed - it's not flexible enough to easily add other content types, like books/book chapters, or to include any changes we may make to our input schema.

With that in mind, we're pausing development on Metadata Manager to allow us to properly evaluate what we've learned. If you're currently using Metadata Manager for journal deposits without any problems, please do continue - you're helping us learn a lot! But if you haven't used Metadata Manager before, or are having problems, please:

-   use our existing [Web Deposit Form](http://www.crossref.org/webDeposit) instead, or
-   upload XML directly through the [deposit system admin interface](https://doi.crossref.org/)

We won't be fixing bugs in Metadata Manager, except for providing any essential security updates. Of course, if you still need help please read our [Content Registration help pages](https://support.crossref.org/hc/en-us/categories/201752243-Registering-content), or contact the [Support team](mailto:support@crossref.org).

Metadata Manager's features will be reimagined as part of our planned Member Center (working title, subject to change) project, where we will start to bring together all business and technical information for our members, service providers and metadata users. The Member Center will be the heart of our strategy to make it easier for you to work with Crossref to:

- register and update metadata
- view, update and transfer titles
- visualize your activity/participation and act on problems with metadata
- understand your bills and invoices
- manage your users and service providers and their access and entitlements
- and more

We're in the early stages of planning for the Member Center and will be seeking feedback from members, service providers and metadata users in the coming months.
