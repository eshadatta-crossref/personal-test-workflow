---
title: 'Metadata Corrections, Updates, and Additions in Metadata Manager'
author: Shayn Smulyan
draft: false
authors:
  - Shayn Smulyan
date: 2020-01-13
categories:
- Metadata Manager
- Content Registration
- Citations
- Identifiers
archives:
    - 2020
---

It's been a year since [Metadata Manager](/education/member-setup/metadata-manager/) was first launched in Beta.  We've received a lot of helpful feedback from many Crossref members who made the switch from Web Deposit Form to Metadata Manager for their journal article registrations.  

The most common use for Metadata Manager is to register new DOIs for newly published articles. For the most part, this is a one-time process.  You enter the metadata, register your DOI, and success!

<!--more-->

But everything doesn't always go quite as expected.  Humans make mistakes, and typos in metadata are bound to happen on occasion, even for the most careful users.

We always want to make it as easy as possible for our members to find and correct metadata errors, and to add additional metadata when it becomes available.  Our [Schematron](https://support.crossref.org/hc/en-us/articles/213197406-Schematron-report), [Conflict](https://support.crossref.org/hc/en-us/articles/213197206-Conflict-report), and [Resolution](/blog/resolution-reports-a-look-inside-and-ahead/) reports can help you identify existing metadata errors. We never charge content registration fees for metadata updates, additions, or corrections, so cost won't be a barrier to getting the most accurate and thorough metadata possible.  And, now, Metadata Manager can make those corrections easier to do.

## Correcting Errors

Because accurate and comprehensive metadata is so important for the linking and discoverability of your publications, it's important to catch these occasional errors and correct them.

We send out [reports that automatically screen for particular types of metadata errors](https://support.crossref.org/hc/en-us/articles/213197406-Schematron-report), and we pass along comments from users who contact us with concerns about metadata quality to our contacts at the relevant publisher. 

The "Review all" feature in Metadata Manager also allows you to do a final check of all the metadata you entered right before you're about to submit your deposits.  So, we also rely on you to evaluate your own accuracy there as well.

<center><img src="/images/blog/2020/metadata manager review.png" alt="Metadata Manager Review All" width="550" class="img-responsive" /></center>

Once you’ve identified an error, you’ll need to correct it.   To do that, you must resubmit a whole new metadata deposit for the affected item.  The newly deposited metadata will entirely overwrite the previously deposited metadata.  

If you’re used to using the Web Deposit Form, you know that the redeposit can be a little tedious. For example, if you find that you misspelled an author’s last name, you’d have to manually type in or copy-paste not just the corrected last name, but all of the journal-level, issue-level, and article-level metadata that applies to the article.

Using Metadata Manager, the process is much simpler.  The full metadata record is retained or imported and you only need to correct the error itself.  

### For articles originally registered using Metadata Manager

If you find a metadata error in an article which you initially registered in Metadata Manager itself, you can locate the article in one of two ways:

1. Navigate through the list of Accepted articles within a given journal

    <center><img src="/images/blog/2020/Metadata Manager Accepted Articles.png" alt="Metadata Manager Accepted Articles" width="550" class="img-responsive" /></center>

2. Or, search by article title in the Deposit History

<center><img src="/images/blog/2020/Metadata Manager Deposit History.png" alt="Metadata Manager Deposit History" width="550" class="img-responsive" /></center>

Once you’ve located the relevant article, click on the article title to open the article’s metadata record.  From there, you can make the necessary corrections.  With the corrections complete, click “Continue” and then “Add to deposit.”   After that, the process is exactly the same as depositing a new article.


### For articles registered using the Web Deposit Form or any other deposit method

If you registered an article using the Web Deposit Form, an XML deposit, or the OJS plugin, you can still use Metadata Manager to quickly correct an error.  But, first you have to import the article’s metadata into Metadata Manager.  

To do this, click into the relevant journal from your Metadata Manager home page.  Then, search for the article title using the “Add existing article” search box. Select “Add” next to the article title in the search results, which will import the article’s metadata record into Metadata Manager.

<center><img src="/images/blog/2020/metadata manager search.png" alt="Metadata Manager Article Search" width="550" class="img-responsive" /></center>

From here, make any necessary corrections and click “Continue” and then “Add to deposit.”  Navigate to the “To deposit” tab and “Review all” to ensure that your metadata record is accurate. Then select “Deposit” to finalize your submission.  You’ll receive immediate feedback as to whether your metadata deposit was successful or not.

<center><img src="/images/blog/2020/Metadata Manager deposit submission.png" alt="Metadata Manager Deposit Submission" width="550" class="img-responsive" /></center>

## Adding additional metadata

Perhaps there are no problems with your metadata, and everything is completely accurate.  That's great! But, we encourage our members to submit metadata that is not just accurate, but also as thorough as possible.  Check your [Participation Report](https://www.crossref.org/members/prep/) to see if there are any types of metadata that you haven't been submitting yet, or that you haven't been submitting for certain journals.

Metadata Manager allows you to deposit references, licenses, and relationships between your articles and other DOIs, which weren’t possible to add using the Web Deposit Form.  The same process described above for corrections will allow you to import previously registered articles and add in these new metadata elements.

We also know that many of our members register DOIs for their articles when they’re first published online, but aren’t yet included in an issue.  When the articles are published in their final versions, there is important metadata added which wasn’t yet available when the DOI was first registered.  This includes things like volume number, issue number, page numbers, and full publication date, all of which are extremely important for linking and discoverability.  Sometimes the resolution URL changes when the article is moved from its pre-publication status to its final version.

So, when each issue is published, you can use Metadata Manager to pull up all the already-registered articles included in that issue and add in the newly relevant metadata like page numbers, issue number, URL, etc.  Then add them to a new deposit, review, and submit.

Please check out the full [Metadata Manager help documentation](/education/member-setup/metadata-manager/) for more details, or join us on an [upcoming workshop](/webinars/) to test out Metadata Manager in real-time with us.  And, as always, feel free to email us at <support@crossref.org> with any questions.
