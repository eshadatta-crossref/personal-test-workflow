---
title: 'Fast, citable feedback: Peer reviews for preprints and other content types'
author: Martyn Rittman
draft: false
authors:
  - Martyn Rittman
date: 2020-12-09
categories:
  - Citation
  - Event Data
  - Identifiers
  - Linking
  - Research Nexus

Archives:
  - 2020
---

Crossref has supported depositing metadata for preprints [since 2016](/blog/preprints-are-go-at-crossref/) and peer reviews [since 2018](/news/2018-06-05-introducing-metadata-for-peer-review/). Now we are putting the two together, in fact we will permit peer reviews to be registered for any [content type](/education/content-registration/content-types-intro/).  

<!--more-->

Currently, peer reviews can be registered for journal articles, but that means that they can only be related to some of the content our members deposit.  Preprints, books, chapters, working papers, dissertations, and a host of other works can also be registered with Crossref. A number of these frequently undergo some form of review and many of our members and voices in the community have called for us to widen the net on peer reviews, including journal publishers, book publishers, review platforms, and preprint servers. We've listened and taken action, and from now on Crossref members can add [relationship metadata](/education/content-registration/structural-metadata/relationships/) that links peer reviews to any content type. The metadata will also contain [the type of review](/education/schema-library/markup-guide/peer-reviews/), stating whether it is a referee report, author response, or community comment, etc. This allows accurate reporting on whether the peer review is happening within a traditional editorial process or elsewhere.

## Reviews for preprints

In the last decade there has been an increase in the number of disciplines using preprints. Since enabling registration of preprint metadata, it has become our fastest-growing content type. Preprints, working papers, and other forms of early publication help to accelerate dissemination of the latest research and discovery. They can also promote discussion on important topics, and help authors to improve papers before an editorial decision for journal publication. During the COVID-19 pandemic, preprints have become invaluable for speeding the publication of vital research and case studies.

On the other hand, preprints do not undergo formal review and editorial approval, leading to concerns about the dissemination of false information. While the issue of misinformation in preprints has been discussed for some time, the COVID-19 pandemic has brought it more sharply into focus. Organizations that post preprints need to balance the benefits of rapid dissemination with promoting their responsible use.

To support the feedback process, preprint servers along with a growing number of other platforms and services offer scholars the opportunity to post public comments on preprints. By doing this, they give extra context for readers, provide suggestions for authors, and raise awareness of work that could be flawed or too preliminary.

Another growing trend is journal publishers adopting editorial processes that involve preprint-first options and open peer review. As Dr. Stephanie Dawson from ScienceOpen says:

> "We have long believed in rewarding reviewers by assigning Crossref DOIs to their open reviews to make them citable objects and we were one of the first users of Crossref's peer review schema. However, a large percentage of the articles reviewed on ScienceOpen are publicly available preprints. The *UCL Open: Environment* journal hosted on the platform, for example, is based on a workflow of open peer review of preprints. Our customers, editors, reviewers and authors are therefore extremely happy that these reviews can now also be assigned a Crossref peer review DOI for more accountability and transparency in scholarly publishing."

At Crossref, we're continually looking to support more content types and relations between them to build trust, support reproducibility and increase discoverability of content. This is another small step in building the [research nexus](/blog/the-research-nexus-better-research-through-better-metadata/) and we look forward to working with members depositing peer reviews of preprints.
