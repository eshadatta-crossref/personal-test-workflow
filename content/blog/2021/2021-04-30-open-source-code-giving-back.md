---
title: 'Open-source code: giving back'
author: Joel Schuweiler
draft: false
authors:
  - Joel Schuweiler
date: 2021-04-30
categories:
  - Collaboration
  - Infrastructure
  - Community
Archives:
  - 2021
---

## TL:DR;

* Hi, I'm Joel
* GitLab UI unsatisfactory
* Wrote a UI to use the API
* Wrote a missing API
* Open company contributes changes back to another open company
* Now have a method for getting work done much easier
* Hurrah!

I'm Joel, a Senior Site Reliability Engineer here at Crossref. I have a long background in open source, software development, and solving unique problems. One of my earliest computer influences was my father. He wrote software to support scientists in search of things like the top quark, the most massive of all observed elementary particles.    

One day my father came home with over 40 floppy disks, excited to have this cool, free operating system called Linux. Together we installed Linux and ended up with a fully functional computer.  Learning and using Linux opened up an entirely new world to me of amazing open-source software that I could use freely. As I enjoyed all this new software now available to me, I tried to fix any bugs or problems I'd encounter and report solutions for them to the software developers. It felt great to be able to contribute back so others could benefit.

Software teams tend to manage their workflow by writing issues, reviewing them to make sure they make sense and have an achievable goal, estimate how much time it will take to complete, and finally––the crucial step––putting the issues in the order in which they should be completed. To manage my work, I’ve always used Jira––a product designed to help teams of all types prioritize work––and for the first time in over a decade, I find myself not using it in my work.  

## Product development tracking with GitLab

The Crossref team took the decision a few years ago to move all our development and product tracking work via [GitLab](https://gitlab.com/crossref/)––a commercial open-source product anyone can use to help keep track of software throughout the development life cycle––with an open-by-default policy. Work is tracked using the issues feature of Gitlab. GitLab will host it, so you don't have worry about maintenance and backups. One major drawback I discovered with GitLab, is a lack of maturity when it comes to doing light project management work.  

> This is where the trouble begins with GitLab.  

In the _board_ view of your issues, you can transition your issues from `waiting`, to `in progress`, from `in progress` to `done`. The problem with this view is its width-restricted, and things like tags on issues, which are used to help categorize, take up valuable vertical space. With enough tags and a long enough subject line, you can only see five issues at a time on a MacBook Pro monitor, for example.  

{{% imagewrap center %}}{{< figure src="/images/blog/2020/gitlab-board-view.png" alt="GitLab board view graphic" width="80%" >}}{{% /imagewrap %}}  

In the _list_ view of your issues, you get a clean compact view; the perfect view to order issues. However there's one major flaw, it's paginated. (You know when you're shopping and they make you click to see another page of goods? Yes, like that.) The problem with GitLab's implementation is you can drag and drop issues on a given page, but there is no way to move the issues to another page in the list of results. Additionally, all newly-created issues are added to the end of the list.  

{{% imagewrap center %}}{{< figure src="/images/blog/2020/gitlab-list-view.png" alt="GitLab list view graphic" width="80%" >}}{{% /imagewrap %}}  

## The solution  

I went about finding a solution by visiting GitLab's own public issue page and found that requests requiring user interface (UI) changes would languish; in some cases, they would go years without getting approval. Instead of putting in all the work to open an issue with them, only to have it be discarded or ignored, I decided to look for another way.    

GitLab has an API, what more could I need? I discovered I could log in and get a list of all the issues, by project, and by group. "This is perfect!",  I thought. I can write my own UI around it. It took three evenings writing a UI that was satisfactory to me. When I started writing javascript to interact with the UI, I learned that the 're-ordering of issues' didn't actually have an API. Further investigation lead me to the issue tracker where I found an issue by a GitLab employee asking for the same functionality––the ability to re-order issues.  

While in a chatroom for GitLab development, I was genuinely surprised by my experience. There was quick attentive help on locating the file I would need to implement the change, they set up a development environment, and even helped submit tests for my code while I worked on updating documentation and writing a changelog entry. It felt like GitLab must’ve designated an employee to work with the community on submitting improvements. In no time, the API for re-ordering was implemented. After the scheduled monthly release of GitLab rolled out with my new API, I was able to easily re-order issues.

GitLab's response when help was needed along the way was impressive. Now there is a much easier method for getting work done that everyone can use. It’s rewarding when you can contribute back to the community for all to benefit.

Is GitLab as polished as Jira? No. Did they embrace me making changes by being open from the start and providing help along the way? Yes. Do I see Jira shifting its culture to match? Unlikely.

By emulating GitLab, an open organization like Crossref has a shot at encouraging community development.
