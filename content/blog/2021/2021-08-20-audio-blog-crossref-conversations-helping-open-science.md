---
title: 'Crossref Conversations: audio blog about helping open science'
author: Rosa Clark
draft: false
authors:
  - Rosa Clark
  - Anna Tolwinska
date: 2021-08-20
categories:
  - Community
Archives:
  - 2021
---


Crossref Conversations is an audio blog we're trying out that will cover various topics important to our community.  This conversation is between colleagues Anna Tolwinska and Rosa Morais Clark, discussing how we can make research happen faster, with fewer hurdles, and how Crossref can help.  Our members have been asking us how Crossref can support open science, and we have a few insights to share. So we invite you to have a listen.   


[*UPDATE: Since this recording ROR IDs are now part of the Crossref schema.*]

<iframe title="Crossref Conversations: Anna and Rosa in conversation about open science" allowtransparency="true" height="150" width="100%" style="border: none; min-width: min(100%, 430px);" scrolling="no" data-name="pb-iframe-player" src="https://www.podbean.com/player-v2/?i=49ggy-1085496-pb&from=pb6admin&share=1&download=1&rtl=0&fonts=Arial&skin=f6f6f6&font-color=auto&btn-skin=60a0c8"></iframe>

## Helpful links

Here are links to all the sources mentioned in the recording.    

* [Recording transcript](https://docs.google.com/document/d/1wSmDfoX1SjeSnsX6L5JKzB3pMBLicEcClEBOQIEKJro/edit?usp=sharing)  
* [Lots of great information on our blog](/blog/)  
* [Send questions to: feedback@crossref.org](mailto:feedback@crossref.org)  
* [Let's continue the conversation on our Community Forum](https://community.crossref.org/)  
* [Metadata 20/20 - great information about how richer more open metadata can make research happen faster](http://www.metadata2020.org/)  
* [Crossref’s Board votes to adopt the Principles of Open Scholarly Infrastructure (POSI)](https://doi.org/10.24343/C34W2H)  
* [Helping researchers identify content they can text mine](https://www.crossref.org/blog/helping-researchers-identify-content-they-can-text-mine/)  

Thanks for listening!
