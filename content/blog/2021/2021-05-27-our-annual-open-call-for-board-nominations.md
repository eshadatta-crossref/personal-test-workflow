---
title: 'Our annual open call for board nominations'
author: Lucy Ofiesh
draft: false
authors:
  - Lucy Ofiesh
date: 2021-05-27
categories:
  - Member briefing
  - Community
  - Board
  - Governance
  - Election
Archives:
  - 2021
---

[Crossref's Nominating Committee](/committees/nominating/) is inviting expressions of interest to join the Board of Directors of Crossref for the term starting in 2022. The committee will gather responses from those interested and create the slate of candidates that our membership will vote on in an election in September. Expressions of interest will be due Friday, June 25th, 2021.

### Board roles and responsibilities
The role of the board at Crossref is to provide strategic and financial oversight of the organization, as well as guidance to the Executive Director and the staff leadership team, with the key responsibilities being:
- Setting the strategic direction for the organization;
- Providing financial oversight; and
- Approving new policies and services.

The board is representative of our membership base and guides the staff leadership team on trends affecting scholarly communications. The board sets strategic directions for the organization while also providing oversight into policy changes and implementation. Board members have a fiduciary responsibility to ensure sound operations. Board members do this by attending board meetings, as well as joining more specific board committees.

Crossref’s services provide central infrastructure to scholarly communications. Crossref’s board helps shape the future of our services, and by extension, impacts the broader scholarly ecosystem. We are looking for board members to contribute their experience and perspective.

### Who can apply to join the board?

Any active member of Crossref can apply to join the board. Crossref membership is open to organizations that produce content, such as academic presses, commercial publishers, standards organizations, and research funders. In fact, this year the board has specifically included in the committee’s remit to “propose at least one name from a funder member for the current round of elections.”

There is a link at the bottom of this post to submit your expression of interest.


### What is expected of board members?

Board members attend three meetings each year that typically take place in March, July, and November. Meetings have taken place in a variety of international locations and travel support is provided when needed. Following travel restrictions as a result of COVID-19, the board adopted a plan to convene at least one of the board meetings virtually each year and all committee meetings take place virtually. Most board members sit on at least one Crossref committee. Care is taken to accommodate the wide range of timezones in which our board members live.

While the expressions of interest are specific to an individual, the seat that is elected to the board belongs to the member organization. The primary board member also names an alternate who may attend meetings in the event that the primary board member is unable to. There is no personal financial obligation to sit on the board. The member organization must remain in good standing.

Board members are expected to be comfortable assuming the responsibilities listed above and to prepare and participate in board meeting discussions.

### About the election

The board is elected through the “one member, one vote” policy wherein every member organization of Crossref has a single vote to elect representatives to the Crossref board. Board terms are for three years, and this year there are five seats open for election.

The board maintains a balance of seats, with eight seats for smaller members and eight seats for larger members (based on total revenue to Crossref). This is in an effort to ensure that the diversity of experiences and perspectives of the scholarly community are represented in decisions made at Crossref.

This year we will elect two of the large member seats (membership tiers $3,900 and above) and three of the small member seats (membership tiers $1,650 and below). You don’t need to specify which seat you are applying for. We will provide that information to the nominating committee.

The election takes place online and voting will open in September. Election results will be shared at the November board meeting and new members will commence their term in 2022.

### About the nominating committee

The nominating committee will review the expressions of interest and select a slate of candidates for election. The slate put forward will exceed the total number of open seats. The committee considers the statements of interest, organizational size, geography, gender, and experience.

2021 Nominating Committee:
* Liz Allen, F1000/Taylor & Francis, London, UK, committee chair
* Melissa Harrison, eLife, Cambridge, UK
* Andrew Joseph, Wits University Press, Johannesburg, South Africa
* Abel Packer, SciELO, São Paulo, Brazil
* Lisa Scott, New England Journal of Medicine, Boston, USA

### How do you apply to join the board?

Please [click here to submit your expression of interest](https://docs.google.com/forms/d/e/1FAIpQLSe1UxsGdUkBL7z8ByfKviQAoJcmbnb5zk1qYzIp0XikzsXkbg/viewform?usp=sf_link) or contact [me](mailto:lofiesh@crossref.org).  
