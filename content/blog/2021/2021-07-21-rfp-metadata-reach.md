---
title: 'RFP: Help evaluate the reach and effects of metadata'
author: Jennifer Kemp
draft: false
authors:
  - Jennifer Kemp
date: 2021-07-21
categories:
  - Metadata
  - Community
---

UPDATE, 14 October 2021:   

We received several excellent proposals in response to this RFP and we’d like to thank everyone involved for their time and enthusiasm.

We are excited to announce the two projects that have been selected, to run through early 2023.  Stay tuned!

{{% row %}}
{{% column %}}

**With or Without: Measuring Impacts of Books Metadata**  
This project will test the premise that academic books metadata improves discoverability and usage by assessing the impact of book chapter records with DOIs (unique from metadata associated with the entire book) with associated chapter and book attributes. The study aims to prove or disprove its hypothesis and rank metadata attributes by their association with successful content discovery and access. The findings will be considered alongside similar metadata research in order to develop a metadata efficacy framework, which can be used to determine the return on metadata investments by publishers and service providers.  

**Lettie Y. Conrad and Michelle Urberg**, Independent consultants

{{% /column %}}
{{% column %}}

**Metadata For Everyone**  
This project will explore the metadata quality, consistency and completeness from various individual journals and communities. The project will pay special attention to elements that are most likely to vary across cultures, such as names and those that are potentially multi-lingual, with the understanding that metadata issues do not affect nor impact all communities in the same way.    

**Juan Pablo Alperin**, Associate Director of Research, Public Knowledge Project & Co-Director, Scholarly Communications Lab  
**Mike Nason**, Scholarly Communications & Publishing Librarian, University of New Bruinswick Libraries  
**Marco Tullney**, Head of Publishing Services & Coordination Open Access at TIB – Leibniz Information Centre for Science and Technology  

{{% /row %}}
{{% /column %}}

---

We’re excited (and a little nervous) to launch a new research project designed to assess the effects of metadata on research communications. We’re expecting this effort to be a significant contribution to the
[existing research](https://doi.org/10.3897/rio.5.e38698) on the topic and we’re really looking forward to getting started. We’re also a little nervous because of course we don’t know what the conclusions will be (after all, if we did, we wouldn’t be starting this project).

### Assume nothing
It seems logical and very widely accepted that more and better metadata leads to good things. Does it? If so, how and how do we know that? What does the ‘before and after’ look like when metadata is corrected or enhanced? There are so many questions, so many stakeholders and enough variation around content types (books come to mind) and disciplines (hello citation styles) that the topic warrants all the attention it gets and more. This project is designed to be very broad in scope, sampling from various criteria, and is expected to last about a year.

### Interested in getting involved?
If you’re a researcher involved in scientometrics or bibliometrics or if you’re a consultant with
experience in original research, please have a read of [the RFP](/pdfs/metadata-reach-and-return-rfp-2021.pdf) and get in touch with a statement of interest by 1st September or with questions in the meantime. We’re looking for an individual, research group or organization that will work with us over the course of the project to define terms, finalize the approach, analyze the data and communicate the results, whatever they may be.

**RFP responses are requested by 1st September** so don’t hesitate to <a href="mailto:feedback@crossref.org?subject=Metadata reach and return RFP">get in touch</a> with questions.   

If you’re interested in the project but not in responding to the RFP, you may still be able to help. We
would appreciate wide circulation of this announcement to help us find qualified respondents to the RFP so
please do share this with your network. And, of course, we hope you stay tuned for the outcome of the
work. Check back with us on that in about a year...
