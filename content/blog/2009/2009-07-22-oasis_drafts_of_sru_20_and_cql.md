---
title: OASIS Drafts of SRU 2.0 and CQL 2.0
author: thammond
authors:
  - thammond
date: 2009-07-22

categories:
  - Search
archives:
  - 2009

---
As posted [here][1] on the [SRU Implementors][2] list, the [OASIS Search Web Services Technical Committee][3] has announced the release of drafts of SRU and CQL version 2.0:

  * [sru-2-0-draft.doc][4] 
      * [cql-2-0-draft.doc][5] </ul> 
        The Committee is soliciting feedback on these two documents. Comments should be [posted to the SRU list][2] by August 13.

 [1]: http://listserv.loc.gov/cgi-bin/wa?A2=ind0907&#038;L=zng&#038;T=0&#038;P=52
 [2]: https://web.archive.org/web/20130303230855/http://sun8.loc.gov/listarch/zng.html
 [3]: http://www.oasis-open.org/committees/tc_home.php?wg_abbrev=search-ws
 [4]: http://www.oasis-open.org/committees/download.php/33498/sru-2-0-draft.doc
 [5]: http://www.oasis-open.org/committees/download.php/33497/cql-2-0-draft.doc