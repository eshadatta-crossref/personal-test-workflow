---
title: XMP Library for Flash
author: thammond
authors:
  - thammond
date: 2009-01-16

categories:
  - XMP
archives:
  - 2009

---
Update about new [XMP Library][1] from [Adobe Labs][2]:

> _&#8220;The new Adobe XMP Library for ActionScript is now available for download on Adobe Labs. Adobe Extensible Metadata Platform (XMP) is a labeling technology that allows you to embed data about a file, known as metadata, into the file itself. XMP is an open technology based on RDF and RDF/XML. **With this new library you can read existing XMP metadata from Flash based file formats via the Adobe Flash Player.**&#8220;_

Any volunteers?

 [1]: https://web.archive.org/web/20191031084320/http://www.adobe.com/devnet/xmp.html
 [2]: http://labs.adobe.com/
