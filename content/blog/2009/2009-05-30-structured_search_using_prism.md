---
title: Structured Search Using PRISM Elements
author: thammond
authors:
  - thammond
date: 2009-05-30

categories:
  - Search
archives:
  - 2009

---
We just registered in the [SRU][1] (Search and Retrieve by URL) search registry the following components:

**_Context Sets_**</p>
:     * [PRISM Context Set version 2.0][2]
          * [PRISM Context Set version 2.1][3]</ul>
            **_Schemas_**</p>

            :     * PRISM Aggregator Message Record Schema Version 2.0
                      * PRISM Aggregator Message Record Schema Version 2.1</ul> </dl>
                        This means that an [SRU][1] (Search and Retrieve by URL) search engine that supported one of the PRISM context sets registered above could accept [CQL][4] (Contextual Query Language) queries such as the following:

                          1. <tt>prism.doi = "10.1038/nature05398"</tt>
                              * <tt>prism.publicationName = "Nature" and prism.volume = "444" and prism.number = "7119" and prism.startingPage = "E9"</tt>
                                  * <tt>dc.identifier = "doi:10.1038/nature05398"</tt>
                                      * <tt>dc.creator = "Jones-Smith" and prism.publicationName = "Nature" and prism.publicationDate > "2006-01-01"</tt>
                                          * <tt>dc.title any "fractal pollock" and prism.publicationName = "Nature" sortBy prism.publicationDate/sort.descending</tt>
                                              * <tt>"fractal anlysis" and prism.publicationDate within "2005-01-01 2008-12-31" sortBy dc.creator/sort.ascending</tt></ol>
                                                (Note that the quotes are only needed above for the DOI strings which contain a &#8220;/&#8221; character. Otherwise they are optional in the above examples.)

                                                Any query such as one of the above (here #1) could be sent to the server on a querystring like so:</p>
                                                > <tt>?version=1.1&operation=searchRetrieve&query=prism.doi=%2210.1038/nature05398%22</tt>

                                                and if the server were also equipped to respond with [PAM][5] (PRISM Aggregator Message) format for result records, a response might look like this:

                                                <img alt="fractal-analysis-pam.jpg" src="/wp/blog/images/fractal-analysis-pam.jpg" width="686" height="450" />

                                                PAM was discussed [here][6] earlier.</p>
                                                Such a structured response would provide the metadata elements for applications to build various interfaces into the original article:

                                                <img alt="fractal-analysis.jpg" src="/wp/blog/images/fractal-analysis.jpg" width="459" height="401" />

                                                We think that these PRISM components (context sets and schemas) will be useful for structured search of scholarly publications.</p>

 [1]: http://www.loc.gov/standards/sru/
 [2]: http://www.loc.gov/standards/sru/cql/contextSets/prism-context-set-v2-0.html
 [3]: http://www.loc.gov/standards/sru/cql/contextSets/prism-context-set-v2-1.html
 [4]: http://www.loc.gov/standards/sru/specs/cql.html
 [5]: https://www.idealliance.org/pam
 [6]: /blog/prism-aggregator-message/
