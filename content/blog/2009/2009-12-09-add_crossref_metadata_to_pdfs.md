---
title: Add Crossref metadata to PDFs using XMP
author: Geoffrey Bilder
authors:
  - Geoffrey Bilder
date: 2009-12-09

categories:
  - Metadata
  - PDF
  - XMP
archives:
  - 2009

---
In order to encourage publishers and other content producers to embed metadata into their PDFs, we have released an experimental tool called &#8220;pdfmark&#8221;, This open source tool allows you to add [XMP][1] metadata to a PDF. What’s really cool, is that if you give the tool a Crossref DOI, it will lookup the metadata in Crossref and then apply said metadata to the PDF. More detail can be found on the [pdfmark page][2] on the [Crossref Labs][3] site. The usual weasels words and excuses about &#8220;experiments&#8221; apply.

 [1]: http://en.wikipedia.org/wiki/Extensible_Metadata_Platform
 [2]: http://labs.crossref.org/pdfmark/
 [3]: http://labs.crossref.org