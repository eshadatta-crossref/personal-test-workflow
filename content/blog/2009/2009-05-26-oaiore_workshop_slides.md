---
title: 'OAI-ORE: Workshop Slides'
author: thammond
authors:
  - thammond
date: 2009-05-26

categories:
  - Interoperability
archives:
  - 2009

---
<div  id="__ss_1465963">
  <a  href="http://www.slideshare.net/hvdsomp/an-overview-of-the-oai-object-reuse-and-exchange-interoperability-framework?type=presentation" title="An Overview of the OAI Object Reuse and Exchange Interoperability Framework">An Overview of the OAI Object Reuse and Exchange Interoperability Framework</a></p>

  <div >
    View more Microsoft Word documents from <a  href="http://www.slideshare.net/hvdsomp">hvdsomp</a>.
  </div>
</div>

This is a very slick presentation by Herbert Van de Sompel on [OAI-ORE][1] which he’s due to give today for a [workshop][2] at the [INFORUM 2009 15th Conference on Prrofessional Information Resources][3] in Prague. It’s on the long side at 167 slides but even if you just flip though or sample it selectively you’ll be bound to come away with something.

Describing aggregations of resources is a subject that really has to be of interest to Crossref publishers.

 [1]: http://www.openarchives.org/ore/
 [2]: https://web.archive.org/web/20090615233000/http://www.inforum.cz/en/workshop/
 [3]: https://web.archive.org/web/20090615233000/http://www.inforum.cz/en/workshop/
