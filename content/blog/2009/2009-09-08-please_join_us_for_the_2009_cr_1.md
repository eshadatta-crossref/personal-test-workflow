---
title: Please join us for the 2009 Crossref Technical Meeting.
author: Anna Tolwinska
authors:
  - Anna Tolwinska
date: 2009-09-08

categories:
  - News Release
archives:
  - 2009

---
Crossref Technical Meeting*

The Charles Hotel, Cambridge, MA

Monday, November 9th, 2009

2:00 pm - 5:00 pm

[Please register today!][1]

We also encourage you to register for our 10th Anniversary Celebration Dinner, which will take place Monday, November 9th, 2009 at 6:30 pm following the Crossref Technical Meeting at the Museum of Science in Boston, MA. Transportation from the Charles Hotel to the Museum of Science will be provided. Our 2009 Annual Meeting will take place on Tuesday, November 10th at 9:00 am in the Charles Hotel in Cambridge, MA and we urge you to register soon (if you haven’t already done so)

as space is limited. [You may register for both events here][1].

*Please note that this year’s Technical Meeting will be on Monday afternoon.

 [1]: https://www.crossref.org/crossref-live-annual
