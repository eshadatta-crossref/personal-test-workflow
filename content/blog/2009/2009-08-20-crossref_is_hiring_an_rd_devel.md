---
title: 'Crossref is hiring an R&D Developer in Oxford'
author: Geoffrey Bilder
authors:
  - Geoffrey Bilder
date: 2009-08-20

categories:
  - News Release
archives:
  - 2009

---
We are looking to hire an R&#038;D Developer in our Oxford offices. We are look for somebody who:

  * Is passionate about creating tools for online scholarly communication.
  * Relishes working with metadata.
  * Has experience delivering web-based applications using agile methodologies.
  * Wants to learn new skills and work with a variety of programming languages.
  * Enjoys working with a small, geographically dispersed team.
  * Groks mixed-content model XML.
  * Groks RDF.
  * Groks REST.
  * Has explored MapReduce-based database systems.
  * Is expert in one or more popular development language (Java, C, C++, C#).
  * Is expert in one or more popular scripting language (Ruby, Python, Javascript).
  * Has deployed and maintained Linux/BSD-based systems.
  * Understands relational databases (MySQL, Postgres, Oracle).
  * Tests first.

If you are interested, please see the [full job description][1]. If you are not interested, but know somebody who might be, please let them know about this great opportunity.

 [1]: http://oxford.crossref.org/jobs/rd_developer.html