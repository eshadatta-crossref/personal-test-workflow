---
title: 'nature.com OpenSearch: A Structured Search Service'
author: thammond
authors:
  - thammond
date: 2009-10-05

categories:
  - Search
archives:
  - 2009

---
<map name="GraffleExport">
  <area shape=rect coords="266,26,519,220" href="https://web.archive.org/web/20070815000000*/http://blogs.nature.com/wp/nascent/2009/10/naturecom_opensearch.html"> <area shape=rect coords="230,220,486,414" href="https://web.archive.org/web/20070815000000*/http://blogs.nature.com/wp/nascent/2009/10/desktop_widgets_naturecom_sear.html"> <area shape=rect coords="231,220,487,414" href="https://web.archive.org/web/20070815000000*/http://blogs.nature.com/wp/nascent/2009/10/desktop_widgets_naturecom_sear.html"> <area shape=rect coords="2,123,253,317" href="https://web.archive.org/web/20070815000000*/http://blogs.nature.com/wp/nascent/2009/10/web_clients_for_naturecom_open.html">
</map>

<img border="0" alt="opensearch-triptych.jpg" src="/wp/blog/images/opensearch-triptych.jpg" width="522" height="438" usemap="#GraffleExport" />

(Click panels in figure to read related posts.)

Following up on my earlier posts here about the structured search technologies [OpenSearch][1] and [SRU][2], I wanted to reference three recent posts on our web publishing blog Nascent which discuss our new [_nature.com OpenSearch_][3] service:

[1. Service][4]
:   Describes the new [_nature.com OpenSearch_][3] service which provides a structured resource discovery facility for content hosted on nature.com.

[2. Clients][5]
:   Points to a small gallery of [demo web clients][6] for _nature.com OpenSearch_ which all use the text-based JSON interface.

[3. Widgets][7]
:   Introduces the new _nature.com search_ [desktop widgets][8] which interface with the _nature.com OpenSearch_ service via an RSS feed. (See also the [screencast][9] posted to YouTube.)

We hope that this new search service will prove to be useful and may also provide a model for other implementations.

 [1]: http://www.opensearch.org/
 [2]: http://www.loc.gov/standards/sru/
 [3]: http://www.nature.com/opensearch
 [4]: https://web.archive.org/web/20070815000000*/http://blogs.nature.com/wp/nascent/2009/10/naturecom_opensearch.html
 [5]: https://web.archive.org/web/20070815000000*/http://blogs.nature.com/wp/nascent/2009/10/web_clients_for_naturecom_open.html
 [6]: http://nurture.nature.com/opensearch/apps
 [7]: https://web.archive.org/web/20070815000000*/http://blogs.nature.com/wp/nascent/2009/10/desktop_widgets_naturecom_sear.html
 [8]: https://web.archive.org/web/20110309190725/http://www.nature.com/libraries/public_interfaces/widgets.html
 [9]: http://www.youtube.com/watch?v=sqf_ew4o3U8
