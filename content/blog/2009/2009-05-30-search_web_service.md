---
title: Search Web Service
author: thammond
authors:
  - thammond
date: 2009-05-30

categories:
  - Search
archives:
  - 2009

---
[<img alt="search-web-service.png" src="/wp/blog/images/search-web-service.png" width="405" height="303" border="0" />][1]
  
(Click image to enlarge graphic.)
  
While the [OASIS Search Web Services TC][2] is currently working towards reconciling SRU and OpenSearch, I thought it would be useful to share here a simple graphic outlining how a search web service for structured search might be architected.
  
Basically there are two views of this search web service (described in separate XML description files and discoverable through autodiscovery links added to HTML pages):

  * [OpenSearch][3] 
      * [SRU (Search and Retrieve by URL)][4]</ul> 
        One can see at a glance that there’s more happening down in the SRU layer. The SRU layer implements a heavyweight, robust service which provides a detailed listing of search indexes and index relations in the description document (&#8216;SRU Explain’), is searchable using a standard query grammar - [CQL][5] (&#8216;Contextual Query Language’), responds with result sets inside a standard XML wrapper and expressed as an XML record set (e.g. PAM) that is validatable using W3C XML Schema, and makes available a full roster of diagnostics.
  
        By contrast the OpenSearch layer provides a lightweight view onto the search web service in which a simple opaque query string is sent to the server and a simple XML result set returned (usually RSS or Atom). Again a description document is made available (&#8216;OpenSearch Description’) but this is much more coarse grained than the SRU description - e.g. it does not specify query components such as indexes or relations.
  
        In practice, both views can be provided for by the same search web service. While OpenSearch does not specify any structured query it can make use of a CQL packaged query. That is, a single parameter value for the OpenSearch &#8216;query’ parameter can be unpacked by a CQL parser to yield a complex search query. The search query does not need to be splattered all over the URL querystring which is already using its parameter set to provide control information for the search (e.g. pagination, encoding and the like).
  
        And how would this relate to existing platform-hosted search services? Well, such services are usually bound to the host platform and are not intended to support remote applications. A search web service, on the other hand, would be ideally suited to offering direct support for running structured searches on platform-hosted content using off-platform apps.

 [1]: /wp/blog/images/search-web-service.png
 [2]: http://www.oasis-open.org/committees/tc_home.php?wg_abbrev=search-ws
 [3]: http://www.opensearch.org/
 [4]: http://www.loc.gov/standards/sru/
 [5]: http://www.loc.gov/standards/sru/specs/cql.html