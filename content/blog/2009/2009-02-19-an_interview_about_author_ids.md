---
title: 'An interview about &#8220;Author IDs&#8221;'
slug: 'an-interview-about-author-ids'
author: Geoffrey Bilder
authors:
  - Geoffrey Bilder
date: 2009-02-19

categories:
  - Identifiers
archives:
  - 2009

---
Over the past few months there seems to have been a [sharp upturn in general interest][1] around implementing an &#8220;author identifier&#8221; system for the scholarly community. This, in turn, has meant that more people have been getting in touch with us about our nascent &#8220;Contributor ID&#8221; project. The other day, after seeing my comments in the above thread, [Martin Fenner][2] asked if he could interview me about the issue of author identifiers for [his blog on Nature Networks, Gobbledygook][3]. I agreed and he [posted the interview][4] the other day.

I warn you ahead of time, I did ramble on a bit and the interview is long. There is a lot of stuff at the beginning about the DOI and it might seem off-topic, but I do think that there is a lot that we can learn from our DOI experiences which would apply to any author identifier. Just be thankful I didn’t start talking about the privacy issues that will inevitably arise from any author identifier system. If I had, the interview would have probably gone on for another six pages ;-).

Anyway, as most of our membership knows, we have a pilot project underway to explore what it would take to launch a &#8220;Crossref Contributor ID&#8221; system. We still haven’t concluded whether it makes sense for us to do it, but one thing is clear from the recent discussions we’ve had and that is that, if we don’t do it, somebody else almost certainly will.

 [1]: https://cameronneylon.net/blog/a-specialist-openid-service-to-provide-unique-researcher-ids/
 [2]: https://web.archive.org/web/20090219202623/http://network.nature.com/people/mfenner/profile
 [3]: https://web.archive.org/web/20090228185451/http://network.nature.com/people/mfenner/blog
 [4]: https://web.archive.org/web/20091225201433/http://network.nature.com/people/mfenner/blog/2009/02/17/interview-with-geoffrey-bilder
