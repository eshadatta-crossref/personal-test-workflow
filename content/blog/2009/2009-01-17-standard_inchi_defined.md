---
title: Standard InChI Defined
author: thammond
authors:
  - thammond
date: 2009-01-17

categories:
  - Identifiers
  - InChI
archives:
  - 2009

---
IUPAC has just released [the final version (1.02)][1] of its [InChI software][2], which generates Standard InChIs and Standard InChIKeys. (InChI is the IUPAC International Chemical Identifier.)

The Standard InChI _&#8220;removes options for properties such as tautomerism and stereoconfiguration&#8221;_, so that a molecule will always generate the same stable identifier - a unique InChI - which facilitates _&#8220;interoperability/compatibility between large databases/web searching and information exchange&#8221;_. Note also that any _&#8220;shortcomings in Standard InChI may be addressed using non-Standard InChI (currently obtainable using InChI version 1.02beta)&#8221;_.

On a practical level this means that the 27-character length InChIKeys (a hashed form of the InChI), with the following generic form

```AAAAAAAAAAAAAA-BBBBBBBBFV-P```

can now be readily and reliably generated and will start to be used in search indexing and linking applications.

 [1]: https://web.archive.org/web/20090616040900/http://www.iupac.org/inchi/release102final.html
 [2]: https://web.archive.org/web/20090206153708/http://iupac.org/inchi/download/index.html
