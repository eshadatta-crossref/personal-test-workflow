---
title: 'A ROR-some update to our API'
author: Rachael Lammey
draft: false
authors:
  - Rachael Lammey
date: 2022-01-19
categories:
  - ROR
  - Metadata
  - Infrastructure
  - APIs
  - Research Nexus

archives:
    - 2022
---

Earlier this year, Ginny posted [an exciting update on Crossref’s progress with adopting ROR](/blog/some-rip-roring-news-for-affiliation-metadata/), the Research Organization Registry for affiliations, announcing that we'd started the collection of [ROR](https://www.ror.org) identifiers in our metadata input schema. 🦁

The capacity to accept ROR IDs to help reliably identify institutions is really important but the real value comes from their open availability alongside the other metadata registered with us, such as for publications like journal articles, book chapters, preprints, and for other objects such as grants. So today's news is that ROR IDs are now connected in Crossref metadata and openly available via our APIs. 🎉

{{% imagewrap right %}}
{{< figure src="/images/blog/2022/research--nexus-2021.png" alt="visualizing the Research Nexus vision" width="50%" >}}
{{% /imagewrap %}}  

This means ROR can be used by and within all the tools services that integrate with Crossref APIs to analyse, search, recommend, or evaluate research. It’s an important element of **the Research Nexus**, our vision of a fully connected open research ecosystem, and helps identify, share, and link the affiliations of those producing and publishing different types of research or receiving grants.


Now that this metadata is available, it helps confer the downstream benefits of ROR for different (and interconnected) groups:

- It makes it easier for institutions to find and measure their research output by the articles their researchers have published, or perhaps make it easier to track the grants they’ve received.  
- Funders need to be able to discover and track the research and researchers they have supported.  
- Academic librarians need to easily find all of the publications associated with their campus.
- Journals need to know where authors are affiliated so they can determine eligibility for institutionally sponsored publishing agreements.  
- Editors can use more accurate information on author and reviewer institutions during the peer review process, which can help avoid potential conflicts of interest.  

Those are just a handful of use cases, which is why disseminating ROR affiliation identifiers via our APIs is so important; it lets others choose to do what they need to with the information, without restriction.

## The story so far

A growing number of our members have started to include ROR in the metadata they register with us, so we’re excited to be able to see this via simple API queries.

At the time of writing we can see [nearly 4,000 RORs being registered by these 21 members](http://api.crossref.org/works?filter=has-ror-id:t&facet=publisher-name:*) (we've removed test accounts). Note that many of these are being baked into metadata being registered for grant records, also [recently released and now findable](/blog/come-and-get-your-grant-metadata/) through the REST API:

``` JSON
"Wellcome": 2821,
"Natural Resources Canada/CMSS/Information Management": 277,
"University of Szeged": 139,
"RTI Press": 104,
"American Cancer Society": 103,
"University of Missouri Libraries": 77,
"Keldysh Institute of Applied Mathematics": 52,
"Boise State University, Albertsons Library": 52,
"Australian Research Data Commons (ARDC)": 52,
"The Neurofibromatosis Therapeutic Acceleration Program": 49,
"Boise State University": 12,
"The ALS Association": 11,
"Children's Tumor Foundation": 9,
"Episteme Health Inc": 3,
"The University of the Witwatersrand": 2,
"Office of Scientific and Technical Information": 2,
"AGH University of Science and Technology Press": 2,
"York University Libraries": 1,
"SZTEPress": 1,
"Masaryk University Press": 1,
"Institut für Germanistik der Universität Szeged": 1,
```

Our grants schema accommodated ROR first, so it's the funder members and grant records that dominate the adoption of ROR... so far! But there are a few articles and reports there too already. [These content types](https://api.crossref.org/works?filter=has-ror-id:t&facet=type-name:*) include ROR in their records:

``` JSON
"Grant": 3047,
"Report": 382,
"Dissertation": 164,
"Journal Article": 140,
"Conference Paper": 22,
"Posted Content": 12,
"Dataset": 7,
"Monograph": 6,
"Book": 3,
"Chapter": 2,
"Proceedings Series": 1,
"Peer Review": 1,
"Journal Issue": 1,
"Book Set": 1,
"Book Series": 1
```

We can currently see [205 different ROR IDs in Crossref metadata](https://api.crossref.org/works?filter=has-ror-id:t&facet=ror-id:*), with the most frequently provided ROR ID being: [https://ror.org/02jx3x895](https://ror.org/02jx3x895), or **University College London** as it’s also known as.

If you’re a Crossref member keen to assert affiliation identification in your content, our recent webinar, [Working with ROR as a Crossref member: what you need to know](https://www.youtube.com/watch?v=D9Mtqb64OEk), covers all the detail.

Interested in using the information? Dig into our [REST API documentation](https://api.crossref.org/swagger-ui/index.html) and into the API itself, use the polite pool if you can (i.e. identify yourself). There’s also a wealth of information on the [ROR support site](https://ror.readme.io/) or being shared among [integrators](https://ror.org/integrations/) in the growing ROR community.

Join us in doing more with ROR!
