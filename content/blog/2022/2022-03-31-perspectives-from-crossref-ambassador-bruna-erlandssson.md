---
title: 'Perspectives: Bruna Erlandsson on scholarly communications in Brazil'
author: Bruna Erlandsson
draft: false
authors:
  - Bruna Erlandsson
  - Rosa Clark
  - Vanessa Fairhurst
  - Ginny Hendricks
date: 2022-03-28
categories:
  - Community
  - Perspectives
archives:
    - 2022
---

{{% imagewrap left %}} <img src="/images/blog/2022/perspectives.png" alt="sound bar logo" width="150px" class="img-responsive" /> {{% /imagewrap %}}  

Join us for the first in our _Perspectives_ blog series. In this series of blogs, we will be meeting different members of our diverse, global community at Crossref. We learn more about their lives, how they came to know and work with us, and we hear insights about the scholarly research landscape in their country, challenges they face, and plans for the future.  

<!--more-->

In our first blog, we meet Bruna Erlandsson, Crossref Ambassador in Brazil, co-owner of Linceu Editorial, and client services manager at ABEC Brasil. Bruna has dedicated her career to scholarly publishing and has worked with Crossref for many years.  We invite you to have a read and a listen below to meet Bruna!   

{{% row %}} {{% column %}}

**<p align="center">English</p>**


<script src="https://fast.wistia.com/embed/medias/fexdojx291.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_embed wistia_async_fexdojx291" style="height:339px;position:relative;width:340px"><div class="wistia_swatch" style="height:5%;left:0;opacity:0;overflow:hidden;position:absolute;top:0;transition:opacity 200ms;width:50%;"><img src="https://fast.wistia.com/embed/medias/fexdojx291/swatch" style="filter:blur(5px);height:50%;object-fit:contain;width:50%;" alt="" aria-hidden="true" onload="this.parentNode.style.opacity=1;" /></div></div>


{{% /column %}}
{{% column %}}

**<p align="center">em Português</p>**

<script src="https://fast.wistia.com/embed/medias/r048w2d7yt.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_embed wistia_async_r048w2d7yt" style="height:339px;position:relative;width:340px"><div class="wistia_swatch" style="height:50%;left:0;opacity:0;overflow:hidden;position:absolute;top:0;transition:opacity 200ms;width:50%;"><img src="https://fast.wistia.com/embed/medias/r048w2d7yt/swatch" style="filter:blur(5px);height:50%;object-fit:contain;width:50%;" alt="" aria-hidden="true" onload="this.parentNode.style.opacity=1;" /></div></div>


{{% /column %}}{{% /row %}}

<br><br>

>Tell us a bit about your organization, your objectives, and your role  
​​Conte-nos um pouco sobre sua organização, seus objetivos e sua função

I am a co-founder of the company Linceu Editorial, dedicated to publishing scientific and technological research in ethical, creative, and innovative ways. We strive to provide quality editorial services that meet standard industry requirements and best practices, increase visibility, attract readers and potential authors, and ensure their work is properly cited. My personal goal is to be recognized by the scientific community for providing excellent service to our clients.  

Sou sócia proprietária da empresa Linceu Editorial, que se dedica à editoração de artigos científicos de inúmeras revistas, de forma ética, criativa e inovadora. Buscamos atribuir aos periódicos de nosso portfólio os requisitos de qualidade editorial alinhados às melhores práticas editoriais, de forma que aumentem sua visibilidade e atraiam leitores, potenciais autores e, não menos importante, que recebam citações em seus artigos. Meu objetivo pessoal é obter reconhecimento da comunidade científica por meio de uma prestação de serviço em nível de excelência.  

>What is one thing that others should know about your country and its research activity?  
O que os outros deveriam saber sobre seu país e sua atividade de pesquisa?

Brazil is the South American leader in publishing scientific articles in Open Access journals. However, it faces challenges due to the absence of a more comprehensive public policy to support scientific editors. As a result, most journals are produced by teaching and/or research institutions or scientific associations with volunteer editorial teams that, although lacking professional journal production skills, produce high-quality journals. Only a tiny percentage of Brazilian journals are published through commercial publishers.  

O Brasil é o líder sul-americano na publicação de artigos científicos, com destaque para as revistas em acesso aberto. No entanto, enfrenta desafios em função da ausência de uma política pública mais abrangente para apoio aos editores científicos. A maior parte dos periódicos é produzida por instituições de ensino/pesquisa ou Sociedades Científicas, tendo uma equipe editorial voluntária e carecendo de profissionalização em sua produção, embora, em muitos casos, apresentem boa qualidade. Apenas uma pequena porcentagem de periódicos brasileiros é publicada por meio de um publisher comercial.  


>Are there trends in scholarly communications that are unique to your part of the world?  
Existem tendências nas comunicações acadêmicas que são únicas em sua parte do mundo?

I wouldn't say unique. However, adherence to Open Science practices, such as preprints and making research data available, is already part of the editorial culture. On the other hand, open peer review is not yet well accepted by everyone in the scientific community, and only a few journals adopt it. In addition, in some areas of research, such as Education and Social Science, researchers are very active - on forums, in discussions lists and attending the same conferences - so there’s this feeling that ‘everyone knows everyone’ which can then lead to potential conflicts of interest and apprehensiveness around open peer review, particularly when it comes to publishing a negative review.  

Eu não diria única, mas penso que, no Brasil, a adesão às práticas da ciência aberta, como publicação em preprint e disponibilização de dados de pesquisa, já fazem parte da cultura editorial. Por outro lado, a revisão aberta ainda não é bem aceita por toda comunidade científica, sendo poucos os periódicos que o adotam. Além disso, em algumas áreas de conhecimento com grande produção local, como por exemplo a Ciências Sociais e Educação, a interação entre membros da comunidade é muito grande, visto que são pesquisadores muito ativos em fóruns, listas de discussões e conferências da área, causando a sensação de que "todo mundo conhece todo mundo", resultando em um possível conflito de interesse, visto que existe um grande receio em publicar um parecer aberto, especialmente se o caso for um parecer negativo.  

>What about any political policies, challenges, or mandates that you have to consider in your work?  
E as políticas, desafios ou mandatos políticos que você deve considerar em seu trabalho?

In Latin America we have a large indexing database, Redalyc, and a digital library of Open Access journals, which has recently excluded a number of journals for charging APCs (Article Processing Charges), upon the understanding that this would go against their Diamond Open Access requirement.  

However, in Brazil - in general - the understanding of Open Access is not so limited. Charging APCs are in fact encouraged by many as a form of self-sustainability of the journal while still being Open Access.  

As for challenges, one of the biggest is whether or not to publish in English. Although the number of Brazilian journals that publish exclusively in English or both languages (Portuguese and English) is remarkably high. There is still however a belief that local science is only of interest to the local public, and so some question whether there is a value in publishing in English (or other languages). For example, if an author writes a research paper about a small riverside community in the countryside of Acre state in Brazil, they might ask why someone outside the country would be interested in reading that.  

Aqui na América Latina, temos uma grande base indexadora, Redalyc, e biblioteca digital de periódicos de Acesso Aberto que, recentemente, excluíu da base um número considerável de periódicos que cobrassem qualquer tipo de taxa de publicação, por entender que isso iria contra os requisitos de seu modelo de Acesso Aberto Diamante (periódicos em acesso aberto livre de taxa de publicação).  

No entanto, no Brasil, em geral, o entendimento é outro, a cobrança de taxas de processamento não descaracteriza o acesso aberto, sendo, na verdade, encorajado por muitos como uma forma de auto-sustentabilidade do periódico.    

Já em relação a desafios, acredito que um dos maiores é a questão de publicar ou não em inglês. Embora seja notável o número de periódicos brasileiros que publicam exclusivamente em inglês ou ainda nos dois idiomas (português e inglês), existe ainda a crença de que a ciência local só teria interesse do público local, criando assim o questionamento se há ou não o valor em publicar em outro idioma. Por exemplo, se uma pesquisa estuda algo sobre uma comunidade ribeirinha no interior do estado do Acre, aqui no Brasil, é comum existir a dúvida se algo tão específico seria do interesse de alguém de fora do nosso país.  


>How would you describe the value of being part of the Crossref community; what impact has your participation had on your goals?  
Como você descreveria o valor de fazer parte da comunidade Crossref; que impacto teve sua participação em seus objetivos?  

I get immense value from being part of the Crossref community. Being a Crossref Ambassador brings greater recognition and legitimacy to my role working with editors and adds value to my company’s services as well. The title of Ambassador enhances trust in my opinions, presentations, and when providing support and clarification to those asking questions. However it also comes with a great responsibility to do this well, which motivates me to always keep up to date with developments at Crossref. Through the Ambassador Program I have given several webinars for Crossref and the Associação Brasileira de Editores Científicos (ABEC Brasil), which provide much needed information and support to Portuguese speaking Crossref members as well as enhancing the visibility of my professional activities at Linceu Editorial.  

É um valor enorme fazer parte da comunidade Crossref! Ser Embaixadora do Crossref traz um reconhecimento entre os editores e agrega valor aos serviços de minha empresa. Esse título assegura confiabilidade em minhas opiniões, apresentações, e esclarecimentos de dúvidas, o que traz junto uma grande responsabilidade que me motiva a me manter sempre atualizada com tudo em relação ao Crossref. Através do Programa de Embaixadores eu ministrei diversos webinários para a Crossref e também para a Associação Brasileira de Editores Científicos (ABEC Brasil), fornecendo muitas informações necessárias para os membros da Crossref que falam português, e também isso tudo acaba por retornar em visibilidade para as minhas atividades profissionais na Linceu Editorial.  

>For you, what would be the most important thing Crossref could change (do more of/do better in)?  
Para você, qual seria a coisa mais importante que o Crossref poderia mudar (fazer mais/fazer melhor)?  


I think there is still a need for more multilingual training both online and face-to-face, which has been particularly lacking during the pandemic, to provide more information on Crossref services beyond Content Registration. For example Similarity Check is a service that people still have a lot of questions about (such as ‘what is the magic similarity percentage score to identify plagiarism?’ Answer - there isn’t one!). Crossmark is another service where I believe people could benefit from more training on it’s importance in the publication process, not only in cases of retraction but also in guaranteeing that the article is up-to-date and trustworthy. In Brazil many people use Open Journal Systems (OJS) and so the development of Crossref service specific plugins and training on how to use them is really useful!  

Acho que ainda há necessidade de mais treinamentos multilíngues, tanto online quanto presencial – o que tem sido particularmente escasso durante a pandemia – para fornecer mais informações sobre os serviços do Crossref além do Registro de Conteúdo. Por exemplo, o Similarity Check é um serviço sobre o qual as pessoas ainda têm muitas dúvidas (como 'qual é a porcentagem de similaridade aceitável para identificar plágio?' Resposta - não existe!). O Crossmark é outro serviço onde acredito que as pessoas poderiam se beneficiar de mais treinamento sobre sua importância no processo de publicação, não apenas em casos de retratação, mas também para garantir que o artigo esteja sempre atualizado e confiável. No Brasil muitas pessoas usam o Open Journal Systems (OJS) e por isso o desenvolvimento de plugins específicos do serviço Crossref e treinamento sobre como usá-los seriam muito úteis!  

>Which other organizations do you collaborate with or are pivotal to your work in open science?  
Com quais outras organizações você colabora ou é fundamental para o seu trabalho em ciência aberta?  

I contribute to ABEC Brasil in a variety of ways including speaking on short courses about Crossref, designing content for lectures as part of an online program called ABEC Educação (which will be launched soon), and as a volunteer consultant to answer a variety of questions from editors regarding content registration at Crossref.  

Contribuo com a ABEC Brasil, participando tanto como ministrante de minicursos sobre ferramentas Crossref quanto como conteudista de um curso no Programa EaD ABEC Educação (que será lançado em breve), além de como consultora voluntária para atender a diversas dúvidas de editores em relação a depósito de conteúdo.  


>What are the post-pandemic challenges you are facing and how are you adapting to them?  
Quais são os desafios pós-pandemia que você está enfrentando e como você está se adaptando a eles?  


Considering the current situation in Brazil, I don’t think I would consider us having reached ‘post-pandemic’ just yet. Although vaccination is taking place successfully, there are still many uncertainties and fears. A good example of this is Crossref LIVE Brazil which was canceled at the start of the pandemic and at the moment we still don’t know when we will be able to reschedule this. It still feels too risky to bring a number of speakers from abroad to Brazil and too soon to hold such a large in-person event.  

However, if I had to highlight one challenge I've been facing, it would be something more personal rather than work-related. Beyond a shadow of a doubt, it would be the lack of human contact! It has been really hard to get use to not gathering together with family and friends and not being able to travel, meet new people, and experience new cultures. To deal with it, I  spend my free time planning the places I will go to and people I will visit as soon as this whole situation is over!  

Para ser honesta, considerando a realidade atual no Brasil, eu ainda não considero o momento atual "pós-pandemia". Embora a vacinação esteja ocorrendo com sucesso, ainda existem muitas incertezas e medos. Um exemplo bem claro é o Crossref Live in Brazil, que foi cancelado assim que a pandemia foi "anunciada" e, até hoje, não sabemos quando ocorrerá, pois ainda soa muito arriscado trazer palestrantes de fora para o Brasil e também se encontrar com diversas pessoas em um evento presencial.   

No entanto, se eu tivesse que destacar um desafio que tenho enfrentado, seria algo mais pessoal e não relacionado ao trabalho. E, sem sombras de dúvidas, seria a falta de contato humano! Está sendo realmente complicado se acostumar em não encontrar amigos e familiares, e também não poder viajar e conhecer novos lugares, pessoas e culturas – o jeito que encontrei para lidar com isso é gastar meu tempo livre planejando todos os lugares que irei e todas as pessoas que visitarei assim que essa situação toda passar.  

>What are your plans for the future?  
Quais são seus planos para o futuro?  

My plans for the future include continuously learning more and more about scholarly publishing including the various services that Crossref provides. I want to be able to help publishers implement valuable tools into their workflows such as Similarity Check and Crossmark, and contribute to greater scientific dissemination of Brazilian research so that Brazilian journals can get the global recognition, visibility and value they deserve.    

Meus planos para o futuro incluem aprender cada vez mais e mais sobre publicação científica, incluindo os vários serviços que o Crossref oferece. Quero poder ajudar os editores a implementar ferramentas valiosas em seus fluxos de trabalho, como Similarity Check e Crossmark, e contribuir para uma maior divulgação científica das pesquisas brasileiras para que os periódicos brasileiros possam obter o reconhecimento global, visibilidade e valor que merecem.  

Thank you, Bruna!  
Obrigado, Bruna!  
