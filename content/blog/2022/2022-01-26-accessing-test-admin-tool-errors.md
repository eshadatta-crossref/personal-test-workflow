---
title: 'Hiccups with credentials in the Test Admin Tool'
author: Isaac Farley
draft: false
authors:
  - Isaac Farley
date: 2022-01-26
categories:
  - Content Registration
  - Admin Tool
archives:
    - 2022

---

### TL;DR

We inadvertently deleted data in our authentication sandbox that stored member credentials for our Test Admin Tool - test.crossref.org. We’re restoring credentials using our production data, but this will mean that some members have credentials that are out-of-sync. Please contact support@crossref.org if you have issues accessing test.crossref.org.

### The details

Earlier today the credentials in our authentication sandbox were inadvertently deleted. This was a mistake on our end that has resulted in those credentials no longer being stored for our members using our Test Admin Tool - test.crossref.org.

{{% imagewrap right %}}
{{< figure src="/images/blog/2022/test_admin_tool_error.png" alt="access problems test Admin Tool" width="50%" >}}
{{% /imagewrap %}}

To be clear, this error has had no impact on the production Admin Tool - doi.crossref.org - or any member’s access to registering content therein. If you’re a member who registers content with us using our helper tools (e.g., the web deposit form) or OJS, you’re likely unfamiliar with the Test Admin Tool, and this issue will not affect you or your registration of content.

We don’t configure all member accounts for the Test Admin Tool, so, fortunately, this is an issue for the minority of our members. That said, for those members who do use the Test Admin Tool, this is not a trivial problem. And, we’re going to dedicate additional resources across the organization to ensure it is fixed.

### Next steps

We’ve repopulated the credentials in the Test Admin Tool based on our production accounts. It was our best option. While we don’t know your current credentials, our support and membership teams do know that the majority of our members using the Test Admin Tool have historically shared credentials between the Test Admin Tool and our production Admin Tool - doi.crossref.org. That means that many of you will be able to access the Test Admin Tool using those shared credentials; but some of you - who have used different credentials between the two systems - will not.

We also know that for many of you testing submissions is an integral step in your workflow, so we’ve determined this is an all-hands-on-deck situation and our staff, across the organization, will be assisting members who have issues with access to test.crossref.org. Starting today, we’re actively monitoring submissions to the Test Admin Tool for access errors through Friday, 11 February. We’ll be proactively contacting affected members to reset their passwords. If you encounter problems before we reach out to you, please do contact us at  at [support@crossref.org](mailto:support@crossref.org) and include ‘Accessing Test Admin Tool’ in your subject line.
