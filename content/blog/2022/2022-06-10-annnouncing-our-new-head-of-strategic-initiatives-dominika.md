---
title: 'Announcing our new Head of Strategic Initiatives: Dominika Tkaczyk'
author: Geoffrey Bilder
draft: false
authors:
  - Geoffrey Bilder
date: 2022-06-10
categories:
- Staff
- R&D
- Labs
archives:
    - 2022
---


## TL;DR

A year ago, we announced that we were putting the "R" back in R&D. That was when Rachael Lammey joined the R&D team as the Head of Strategic Initiatives.

And now, with [Rachael assuming the role of Product Director](/blog/announcing-our-new-director-of-product-rachael-lammey/), I'm delighted to announce that [Dominika Tkaczyk](https://www.crossref.org/people/dominika-tkaczyk/) has agreed to take over Rachael's role as the Head of Strategic Initiatives. Of course, you might already know her.

We will also immediately start recruiting for a new Principal R&D Developer to work with Esha and Dominika on the R&D team.

## What does this mean for R&D?

Before I talk about what Dominika's move means in practice, I just want to take a moment to thank Rachael for the time she spent working with us. Over the past year, she has injected a massive amount of energy into the group and rebuilt the team's momentum. This is exactly what we asked her to do.

Rachael's first task was to repatriate her two R&D colleagues, who we had loaned to work on other urgent projects. Dominika was the technical lead on the port and relaunching of the REST API. Esha was the technical lead for the ROR initiative. In addition, Rachael has been working with Esha, Dominika, Paul Davis, and me on several shorter-term strategic projects that are shaping our overall development strategy.

- Exploring and implementing a new approach to building content registration front ends. This approach is schema-driven and bakes in localization and accessibility support from the start. The new approach is currently the basis for the grant registration tool that our Product & Tech teams are now testing with our new funder members. 
- Exploring and ultimately rejecting a "pull-based" approach to registering metadata, where Crossref would harvest structured metadata from member landing pages instead of asking members to deposit it with us via XML. You are not really doing R&D unless some of your ideas fail. In this case, we quickly discovered that the logistics of crawling our members’ websites, combined with the sparsity of structured metadata in landing pages, made a pull-based approach fragile and impractical.
- Exploring the use of ML techniques to fill gaps in the journal classification data that is currently in the REST API. Gaining new data science badges in the process.     
- Exploring alternative approaches to building community-extendable reporting tools using standard data science tooling and techniques.
- Exploring how we can help reduce support toil by using data science tools like notebooks to create new support tools and self-serve UIs for information frequently requested by members that can otherwise prove difficult to get using our existing tools.
- Looking at extending the matching technology previously developed by labs to try and [better match funder grant-information research outputs](https://www.crossref.org/blog/follow-the-money-or-how-to-link-grants-to-research-outputs/).


And this is just a sample of projects Rachael helped promote and prioritize. It is the nature of many of the larger R&D projects that you don't see the immediate results until long after they've been conceived and put into motion. This means that Rachael has been working on some things over the past year that are not yet public.

But, with any luck, we may see some significant new developments in how Crossref collects and distributes information about significant updates to the scholarly record- including retractions and withdrawals. We are also likely to see more work to promote data citation amongst our members. And finally, we are likely to see an attempt to create a community-managed and open research classification taxonomy. Of course, as is the case with research projects, there is no guarantee that any of these nascent ideas/projects will make it into a production service. Still, if even one of them does, it will become as vital a part of open scholarly infrastructure as DOIs, ORCIDs, or ROR IDs are now. 

And we will have Rachael and the hard work of the R&D group, important cameos from others, and community input to thank for giving them the initial push to realization. So that's a pretty good track record for just a year in the R&D group.

## Passing the torch

And this is a track record I'm confident that Dominika can match as she takes over Rachael's role.  

Soon after Dominika joined the Crossref R&D team, she started to expand her activities to include more production engineering practice, team leadership, and community outreach. She has also worked extensively with support and outreach- providing them with data science consulting and mentoring in software development. Her new role as the Head of Strategic Initiatives will continue this trend. She will spend less time prototyping software and analyzing data and more time liaising with our members and the broader community to understand their needs and design R&D projects to test approaches to meeting those needs. This means a lot more liaising with other Crossref teams, speaking with our members and the wider community, and participating in working groups and conferences. 

It also probably means a lot less programming and analysis. But programming and building prototypes are critical to the R&D team. And so the first thing we will do is start recruiting for a new Principal R&D Developer to continue working along with Esha on conducting experiments and developing POCs.

I’m looking forward to the next year. With Rachael taking the role of Product Director and Dominika taking over as the Head of Strategic Initiatives, we are well-positioned to make profound technical and conceptual improvements to Crossref's services while simultaneously working with the community to line up our next strategic priorities.
