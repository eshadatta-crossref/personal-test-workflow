---
title: 'Refocusing our Sponsors Program; a call for new Sponsors in specific countries'
author: Susan Collins
draft: false
authors:
  - Susan Collins
date: 2023-02-06
categories:
  - Sponsors
  - GEM
  - Equity
  - Membership
archives:
      - 2023
---

Some small organizations who want to register metadata for their research and participate in Crossref are not able to do so due to financial, technical, or language barriers. To attempt to reduce these barriers we have developed several programs to help facilitate membership. One of the most significant---and successful---has been our [Sponsor program](/community/sponsors/).

Sponsors are organizations that are generally not producing scholarly content themselves but work with or publish on behalf of groups of smaller organizations that wish to join Crossref but face barriers to do so independently.  Sponsors work directly with Crossref in order to provide billing, technical, and, if applicable, language support to Members.

Because Sponsors are important partners in facilitating membership there is a high bar to meet to be accepted as a Sponsor. To ensure that an organization can accurately represent Crossref and has the resources to be successful we created [a set of criteria](/community/sponsors/#sponsor-criteria) that must be met to be considered.

Our Sponsors program has grown considerably over the last decade and has now become the primary route to membership for emerging markets and small or academic-adjacent publishing operations.

The program began in 2012 with four Sponsors, based primarily in South Korea and Turkey, representing fewer than 100 members. In the next stage of development, the program covered Brazil, India, and Ukraine, and nearly 1300 members. At the end of 2022, the program had grown to over 100 sponsors from [45 countries](/membership/about-sponsors/) representing over 11,000 of our members.

Though the program continues to expand, there are still regions where we lack Sponsors, while having an abundance in others. We are working with members, ambassadors, and the community to help identify organizations that may be a fit with the Sponsor program and based in those regions where coverage is lacking.

This January we announced our [Global Equitable Membership (GEM) Program](/gem/) which offers relief from membership and content registration fees for members in the least economically-advantaged countries in the world. Eligibility for the program is based on a member's country on our curated list.

Though the GEM program reduces financial barriers to becoming a member, many organizations still require technical assistance and local language support. Working with a Sponsor would help organizations overcome these burdens. However, there is little or no Sponsor coverage for organizations located in most GEM-eligible countries. That means that in places like Bangladesh, Nepal, and Senegal, where we've seen a lot of growth, more organizations could join us if a suitable local Sponsor could support them.

We have made the decision to pause accepting new Sponsors from regions where Sponsor numbers are already very high or not based in a GEM region. By doing so we can focus on growing the program in areas where there is the greatest need.

We are also going to focus on how best to support our current 100+ Sponsors and work with them to evaluate ways to improve the program. We will bolster the training and resources, outreach activities, and solicit feedback on additional ways we can help.

We would love to hear from organizations based in GEM countries who might consider becoming a Sponsor. But our invitation for Sponsors is not limited to the support for the GEM program. There are countries where the GEM program won't apply, but where growth is high and no Sponsor is present. In particular, we seek support in the following countries where member numbers are growing but could be better supported.

<center>

|Country/state   | Region  | No. Crossref members   |
|---|---|---|
|  Nigeria | Sub-Saharan Africa (Western)  |  99 |
|Philippines   | South-eastern Asia  | 81  |
| Kenya  | Sub-Saharan Africa (Eastern)  | 40  |
|  Egypt |  Northern Africa | 26  |
|  Sri Lanka |Southern Asia   | 13  |

</center>

If your organization is based in one of these regions and supports or provides services to scholarly publishers in one of the above countries ---please take a look [at the criteria](/community/sponsors/) set out on our website and do get in touch to start the conversation if you think you can meet them. We're excited to hear from you!