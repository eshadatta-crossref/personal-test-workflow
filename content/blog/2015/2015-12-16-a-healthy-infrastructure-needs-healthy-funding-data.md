---
title: A healthy infrastructure needs healthy funding data
author: Kirsty Meddings
authors:
  - Kirsty Meddings
date: 2015-12-16

categories:
  - Crossmark
  - Research Funders
  - Metadata
tags:
  - funding data
  - Open Funder Registry
archives:
  - 2015

---
<span ><span >We’ve been talking a lot about infrastructure here at Crossref, and how the metadata we gather and organize is the foundation for so many services - those we provide directly - and those services that use our APIs to access that metadata, such as </span><span ><a href="http://www.growkudos.com" target="_blank">Kudos</a></span><span > and </span><a href="http://www.chorusaccess.org/about/about-chorus/"><span >CHORUS</span></a><span >, which in turn provide the wider world of researchers, administrators, and funders with tailored information and tools.</span></span>

<span ><b>The initiative formerly known as FundRef </b></span>

<span >Together Crossref’s <a href="https://web.archive.org/web/20131229210637/http://search.crossref.org//funding" target="_blank">funding data</a> (previously known as FundRef  – we simplified the name)  and the <a href="/services/funder-registry/" target="_blank">Open Funder Registry</a>, our taxonomy of grant-giving organizations, comprise a hub for gathering and querying metadata related to the questions: </span>

<span ><b><i>“Who funded this research?” </i></b><span >and </span><b><i>“Where has the research we funded been published?”</i></b><!--more--></span>

<span >To support the funding data initiative, three key pieces of metadata are needed from publishers:</span>

  * <span >Funder ID </span>
  * <span >Funder Name  </span>
  * <span >DOI<i></i></span>

<span >Unfortunately only around half of the 950,000 Crossref DOIs with funding data contain funder IDs, the unique funder identifiers from the Open Funder Registry that are needed to link up all of the data.  So, only half of the data is useful. (And 950,000 DOIs is only a fraction of the 77 million DOIs in our database, but more on that later).</span>

<span >When we looked at the funding data that was coming in without funder IDs we were a little surprised. We had expected that most of these would be names that simply aren’t in the Open Funder Registry yet, and we thought there would be a certain amount of incorrect information that had been entered into the “funder_name” field. </span>

<span ><span >Instead, what we found was that many of the names were correct, and the funder IDs were just </span><i><span >missing</span></i><span >. </span></span>

<span ><b>Tidying the data</b></span>

<span >To help correct this, we decided to match incoming names to funder IDs where we could do so with the highest level of confidence. After much testing to minimize false positives, we switched this on at the end of August 2015. </span>

<span >Throughout September and October, we inserted funder IDs for about 25% of the names that have been deposited without IDs. For October, the real numbers were 68,000 funder names with no IDs deposited, and 18,000 funder IDs inserted by Crossref. </span>

<span >In the same period 42,000 funder IDs were deposited by publishers. With our matching on top of this, we are achieving a little over a 50% overall success rate of “good” funding data (funder names and funder IDs together). </span>

<span >We have been very careful to distinguish the funder IDs that we have added from those deposited by publishers - provenance of data is an extremely important part of what we do. All funder IDs are tagged as provided either by the publisher or Crossref. Every time we insert an ID into a deposit, the publisher is notified in the deposit report. </span>

<span ><span >We have also now added these tags to our </span><a href="https://api.crossref.org"><span >REST API</span></a><span > so that publishers can query to find out <a href="https://api.crossref.org/v1/works?filter=funder-doi-asserted-by:crossref&rows=100" target="_blank">exactly which DOIs</a> we have amended*. The ideal scenario at this point is that the publisher checks that they are happy with the matching and then redeposits the funding data for those DOIs, over-writing the </span><code>&lt;span >doi-asserted-by: “crossref”&lt;/span></code><span > tag and claiming the metadata as their own. </span></span>

<span ><b>Setting some limits </b></span>

<span ><span >The second largest problem with funding data was </span><i><span >incorrectly entered funder name</span></i><span > – e.g. concatenation of several names or authors entering overly long or vague program names instead of the official funder name. </span></span>

<span >To help weed this out, we have made a couple of changes to the funding data deposit system:</span>


* <span ><span >Funder_name field can no longer contain a numerical string over </span><b>4 digits</b></span>
* <span ><span >Funder_name field can no longer contain a text string over </span><b>200 characters</b></span>
* <span >Funder names that that do not adhere to these two rules will now cause the funding data section of the metadata deposit (not the whole deposit) to fail and return an error message.</span>

</span>

<span ><b>Getting the growth we need</b></span>

<span ><span >As of today, 198 publishers deposit funding data with Crossref. This amounts to about 3.5% of Crossref’s membership</span> <span >(although it’s a larger proportion of our total deposits). We need more publishers to deposit funding data so that </span><a href="https://web.archive.org/web/20131229210637/http://search.crossref.org//funding"><span >funding data search</span></a><span > can become a truly useful tool for the community. There’s no sign-up process or additional fee - read about how to </span><a href="/services/funder-registry/"><span >get started</span></a><span >, and take a look at our </span><a href="/blog/best-practices-for-depositing-funding-data/"><span >best practices for depositing funding data</span></a><span >.  </span></span>

<span ><b>Finally, we ask you: how can we get more and better funder metadata in 2016?<br /> </b></span>

<span >This is not a rhetorical question. Please tweet your thoughts @CrossrefOrg or email your replies to info@crossref.org. You will receive something special via snail mail if you reply to us – just Crossref’s way of saying thank you.<br /> </span>

<span ><sup><em>*At the time of posting our database is re-indexing and the &#8220;asserted-by&#8221; tags are still filtering through to the API. Check back in a day or two for the full picture. </em></sup></span>
