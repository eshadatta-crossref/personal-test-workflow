---
title: Best Practices for Depositing Funding Data
author: Kirsty Meddings
authors:
  - Kirsty Meddings
date: 2015-09-01

categories:
  - Crossmark
  - Research Funders
  - Metadata
archives:
  - 2015

---
Crossref’s funding data initiative (FundRef) encourages publishers to deposit information about the funding sources of authors’ research as acknowledged in their papers. The funding data comprises funder name and identifier, and grant number or numbers. Funding data can be deposited on its own or with the rest of the metadata for an item of content.

<!--more-->There are two ways that publishers can collect this funding information for any given piece of content: by asking authors to input the funder name(s) and award number(s) via their submission system, or extracting the funder names and award numbers from the acknowledgements in the paper.

The funding data is only useful if it is standardised, and so it is absolutely critical that funder names are deposited with their associated funder IDs from the [Funder Registry][1].

For publishers considering or about to start collecting and depositing funding data, and for those already doing so, we have drawn up some guidelines that will help you to ensure good quality metadata.

**If you are collecting funding information from authors via your submission system:**

  * Provide very clear instructions for your authors. Your submission system should prompt the author towards the canonical name from Crossref’s Funder Registry as they type, or guide them through a pick-list. Make it clear to authors that they should choose funder names from this list and not copy and paste from their manuscript.
  * Work with your submission system vendor or adapt your in-house system to make it easy for authors to select from the Funder Registry, and more difficult to paste incorrect names or ignore the suggested names. Consider a warning message if an unknown name is entered, and offer a list of close matches.
  * Instruct authors to look for the name of the funding body rather than a specific program or project.

**If you or one of your vendors is extracting funding information from papers:**

  * Provide the same clear instructions to your vendor(s). Stress the importance of matching the funder names in the acknowledgements to the names in the Funder Registry.
  * Look for common text-extraction errors such as concatenated funder names, punctuation errors, and stop words such as &#8220;of/for&#8221; that are commonly used interchangeably, or the presence or absence of &#8220;the&#8221; at the start of a funder name.

**For both workflows:**

  * Add QA into your workflow. Many of the names sent to Crossref without IDs are very obviously funders that are in the Registry, and a check by editorial or production staff could correct misspellings or fill in blanks. Check that grant numbers have been separated and are not being deposited as one long string.
  * Be aware that funder names deposited without IDs are not valid funding data and will be hidden from Crossref’s search tools and APIs until such time as they are updated with a funder ID.
  * The funding data section of a deposit (but not the rest of the deposit) will be rejected by the Crossref deposit system if 
      * The funder_name field contains a numerical string longer than 4 digits
      * The funder_id field contains a number that is not an ID from the Funder Registry
      * The funder_name contains text that exceeds 200 characters
  * Consider only depositing data that has funder IDs and holding the rest to re-poll against the Funder Registry at a later date when more funder names have been added. The Funder Registry is updated at approximately two-monthly intervals. You can sign up to be alerted to updates [here][2].
  * If there are funders that appear regularly in your particular subject or geographical area that are not in the Registry, send a list to funder.registry@crossref.org.

 [1]: https://www.crossref.org/services/funder-registry/
 [2]: http://visitor.r20.constantcontact.com/manage/optin?v=001Vzv-UqW3G57-t0YXoJQ2YghheQfSiYyOAlZ1dw67TbFqm0n5SVhTn3urBLe_9ZlAoeQapfs9PznTGUB97pFIdgExWoqkEBPsXyDwctEP7L9znpQ1xb6mqZeJQPsq76yE9nG7WXAqcooSo0WzTw5BdDRRzENtU2lqcwXjSRYMI_H7ojX16927cuXlBbOXiprZsZVoValPqpg=