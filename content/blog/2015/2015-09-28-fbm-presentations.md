---
title: Scheduled Booth Presentations at the Frankfurt Book Fair
author: Anna Tolwinska
authors:
  - Anna Tolwinska
date: 2015-09-29
categories:
  - Meetings
  - Community
  - Metadata
  - APIs
archives:
  - 2015

---
Oktoberfest is in full swing and that makes me think that it’s almost Frankfurt Book Fair time again!

This year in addition to individual meetings we’ll have scheduled flash presentations on our booth, **M91 in Hall 4.2**. These short (10-minute) presentations are great for anyone wanting a quick intro to what Crossref is all about. **Running on Wednesday, Thursday, and Friday** - at the following times each of those days:<!--more-->

* 10am - **Small Publisher Tools**  
* 12pm - **DOIs & Metadata Basics**  
* 3pm - **Exploring through APIs**  

If you’d like to meet with us (Ed Pentz, Ginny Hendricks, Rachael Lammey, or Anna Tolwinska) please contact [Rosa Clark][1] to set up a meeting.


{{< figure src="/images/blog/fbm-logo.png" alt="FBM logo" width="40%" >}}  

<br/>
We look forward to seeing you there!  

 [1]: mailto:rclark@crossref.org
