---
title: DOIs in Reddit
author: Joe Wass
authors:
  - Joe Wass
date: 2015-09-30
categories:
  - Crossref Labs
  - Event Data
  - Identifiers
archives:
  - 2015

---
<span >Skimming the headlines on Hacker News yesterday morning, I noticed something exciting. A dump of <a href="https://news.ycombinator.com/item?id=10289220">all the submissions to Reddit since 2006</a>. &#8220;How many of those are DOIs?&#8221;, I thought. Reddit is a very broad community, but has some very interesting parts, including some great science communication. How much are DOIs used in Reddit?</span>

<span >(There has since been a <a href="https://news.ycombinator.com/item?id=10309581">discussion about this blog post</a> on Hacker News)</span>

<span >We have a whole <a href="/categories/event-data">strategy for DOI Event Tracking</a>, but nothing beats a quick hack or is more irresistible than a data dump.</span><!--more-->

# <span >What is a DOI?</span>

<span >If you know what a DOI is, skip this! The DOI system (Digital Object Identifier) is a link redirection service. When a publisher puts some content online they could just hand out the URL. But the URL can change, and within a very short space of time, <a href="https://en.wikipedia.org/wiki/Link_rot">link-rot</a> happens. DOIs are designed to fight link rot. When a publisher mints a DOI to an article they just published, they can change the article’s URL and then update the DOI to point to the new place. DOIs are persistent. They are URLs. They’re also identifiers (kind of like ISBNs), and they’re used in scholarly publishing as to do citations.</span>

<span >Crossref is the DOI registration agency for scholarly publishing. That means mostly things like journal articles. There are other registration agencies, for example, DataCite, who do DOIs for research datasets. But at this point in time, most DOIs are Crossref’s.</span>

# <span >What does finding DOIs in Reddit mean?</span>

<span >It means someone used a DOI to cite something! DOIs can be used for any kind of content, but because of the sheer volume of scientific publishing, lots of DOIs are for science. Having a DOI doesn’t say anything about quality or content. But it does indicate that the person who created the DOI probably intended it to be cited. We care because it means that every time a DOI is used a tiny bit of link-rot doesn’t have the opportunity to take hold. Every time something is discussed on Reddit and the DOI is used, it means that archaeologists using the data dump in 100 years will have identifiers to find the things being discussed, even if the web and URLs have long since crumbled to dust.</span>

<span >Or, more likely, in five year’s time when a few URLs will have shuffled around.</span>

# <span >The results</span>

<span >DOIs have been used on Reddit since 2008 (the logs start in 2006). After a rocky start, we see hundreds being used per year.</span>

<img src="/wp/blog/uploads/2015/09/year-count.png" class="img-responsive" alt="DOI submissions per year" >

<span >That’s dozens per month.</span>

<img src="/wp/blog/uploads/2015/09/year-month-count.png" class="img-responsive" alt="DOI submissions per month" >

<span >The best subreddit to find DOIs is <a href="http://reddit.com/r/Scholar">/r/Scholar</a>, followed by <a href="http://reddit.com/r/science">/r/science</a>. And then a lot of others with one or two per year.</span>

<img src="/wp/blog/uploads/2015/09/year-subreddit-count.png" class="img-responsive" alt="DOI submissions per subreddit per year" >

# <span >Opportunities</span>

<span >It’s great to see DOIs being used in Reddit. But let’s be honest, it’s not a massive amount.</span>

<span >We have a list of domains that our DOIs point to. They mostly belong to publishers, so every time we see a link to a domain on the list, there’s a chance (not a certainty) that the link could have been made using a DOI. We found a large number of these, orders of magnitude more than DOIs. We’re still crunching the data.</span>

# <span >The data</span>

<span >The data is quite large. It’s a 40 Gigabyte download compressed, which comes to about 170 GB that uncompressed. It contains the submissions to reddit between 2006 and 2015, not the comments, so each data point represents a thread of conversation <em>about</em> a DOI.</span>

# <span >Reproducibility (updated)</span>

<span >You can find the source code and reproduce the figures at <a href="http://github.com/crossref/reddit-dump-experiment">http://github.com/crossref/reddit-dump-experiment</a>. We use Apache Spark for this kind of thing.</span>

<span >The data and methodology are very experimental. You can download all results here:</span>

<span ><a href="https://s3-eu-west-1.amazonaws.com/crossref-labs-data/2015-10-06/reddit-dump-experiment.zip">https://s3-eu-west-1.amazonaws.com/crossref-labs-data/2015-10-06/reddit-dump-experiment.zip</a></span>

<span >It includes all data for charts in this post, as well as the full list of DOIs, the full list of URLs that could possibly have DOIs, and the full JSON input line for each of these.</span>

## <span >More info</span>

<span >Read about our <a href="/categories/altmetrics">DOI Event Tracking strategy</a>, including <a href="/blog/real-time-stream-of-dois-being-cited-in-wikipedia/">our live stream of Wikipedia citations</a>.</span>
