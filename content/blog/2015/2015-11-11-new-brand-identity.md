---
title: The logo has landed
author: Ginny Hendricks
authors:
  - Ginny Hendricks
date: 2015-11-11

categories:
  - Brand
  - Collaboration
  - Crossref LIVE
  - Annual Meeting
  - Community
  - Member Briefing
  - News Release
archives:
  - 2015

---

{{% imagewrap left %}}{{< figure src="/wp/blog/uploads/2015/11/Crossref_Logo_Stacked_RGB_SMALL.png" width="100%" >}}{{%/imagewrap %}}

The rebranding of Crossref was top priority when I joined in May in a new role called "Director of Member & Community Outreach".  Since then I’ve been working to understand the array of services, attributes, and audiences we have developed; to answer the questions "What do we do, for whom, and why?"

As Crossref prepares to celebrate turning fifteen at our annual meeting next week, I am thrilled to present our new brand identity with key messages and logo. And along with “thrilled” you may also detect “nervous excitement”.

<!--more-->

Over the last few months we have reviewed earlier research and talked with a number of members, affiliates, and academics. Turns out we’re the plain talkers of the industry, the do-ers, the scrappy people who get stuff done, chivvy others along, and in some cases we are—dare I say it—the voice of reason!   

While balancing differing views within the scholarly community, we’re all about making connections – literally and figuratively. We help bring together people and metadata in pursuit of an excellent research communications system for all. And, to mirror one of Ed Pentz’s new catchphrases, we are "keeping it real"; with down-to-earth language.</span><figure id="attachment_978"  class="wp-caption alignnone">

{{< figure src="/wp/blog/uploads/2015/11/Screen-Shot-2015-11-09-at-16.52.41.png" alt="Crossref Key Messages" width="785" height="478" caption="_Crossref Key Messages_" >}}

New logos and names for all our products will come soon (in some cases it’ll be a ‘de-brand’ rather than a re-brand!). We’ll gradually phase in the new identity over the next month or two, starting with our annual meeting, and with a complete website relaunch following in 2016. We will contact all of our members and partners in the coming weeks with information about using the new logo, using a content delivery network (CDN) so that sites can reference the correct file.

## Why rebrand?  

We have not rebranded because we plan on doing something different but rather to better express the things we already do. Our ‘problem’ was that often people didn’t know Crossref was behind initiatives like CrossCheck, Crossmark and FundRef. Our products had become unlinked from the organisation. And since we’re all about linking things together, that just made no sense.  

- We needed an icon to give more flexibility across the web that a word mark cannot do alone. The icon is made up of two interlinked angle brackets familiar to those who work with metadata, and can also act as arrows depicting <span style="color: #3eb1c8;">Metadata In</span> and <span style="color: #3eb1c8;">Metadata Out</span>, two themes under which our services can generally be grouped.</span>
- Sentence case helps to avoid splitting the word; we do not want to tempt the Cross and the Ref to divide again. So that lowercase R you see in the middle of our name is indeed an official change. (Hopefully we can change the habit!)
- The palette gives a nod to the history of Crossref with red & dark grey, but brings in contemporary colors for a fresh palette that is distinctive in our industry (we researched a lot - everyone has circles, and traditional shades abound). Our aesthetic embodies classic Swiss design principles and is minimalist in keeping with our straight-talking personality.  

So, in the words of Board Chair, Ian Bannerman, <strong><span style="color: #3eb1c8;">it’s time for Crossref to step forward</span></strong>.

{{< figure src="/wp/blog/uploads/2015/11/Screen-Shot-2015-11-09-at-16.28.57.png" alt="About Crossref - Boilerplate copy" width="937" height="527" caption="_About Crossref_" >}}  

I’m looking forward to revealing more of the story at our annual meeting next week!  
