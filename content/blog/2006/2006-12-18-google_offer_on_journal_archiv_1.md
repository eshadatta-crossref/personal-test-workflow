---
title: 'Google offer on journal archives&#8230;'
slug: google-offer-on-journal-archiv-1
author: Ed Pentz
authors:
  - Ed Pentz
date: 2006-12-18

categories:
  - News Release
archives:
  - 2006

---
Peter Suber [reports][1] on his Open Access News that Google is offering to digitize journal backfiles. The full text articles are available as images and for free hosted by Google. The deal is non-exclusive and publishers retain copyright (but many backfiles will be out of copyright) but Google will not supply the publisher with the electronic files - so non-exclusive means that the publisher or someone else could digitize the backfile too (but how to recover the costs when it’s all free in Google?).
  
Dorothea Salo ([recent STM Innovations speaker][2]) over at Caveat Lector provides an excellent [review of the Google offer][3] with some good advice for publishers (&#8220;always control your bits&#8221;).

 [1]: http://www.earlham.edu/~peters/fos/2006_12_17_fosblogarchive.html#116637929327063772
 [2]: /blog/speaking-of-stm-innovations/
 [3]: https://web.archive.org/web/20080725070901/http://cavlec.yarinareth.net/archives/2006/12/17/control-your-bits/