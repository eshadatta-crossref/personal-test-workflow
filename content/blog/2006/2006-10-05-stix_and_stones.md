---
title: STIX and Stones
author: admin
authors:
  - admin
date: 2006-10-05

categories:
  - News Release
archives:
  - 2006

---
The [STIX Fonts][1] project funded by six major publishers to develop a comprehensive font set for STM publishing has completed its development phase and is about to move into beta testing (planned to commence in late October). Participation is open to all publishers - so now is the time to get involved to ensure your needs are met by this significant activity.

 [1]: http://www.stixfonts.org/