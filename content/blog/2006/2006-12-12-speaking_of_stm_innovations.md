---
title: 'Speaking of STM Innovations'
author: Ed Pentz
authors:
  - Ed Pentz
date: 2006-12-12

categories:
  - Conference
archives:
  - 2006

---
The STM Innovations meeting on December 7th in London was excellent. Leigh Dodds [has a short summary][1] of the day on his blog. Interestingly, I can’t find anything about the conference on the STM website.

 [1]: http://www.ldodds.com/blog/archives/000303.html