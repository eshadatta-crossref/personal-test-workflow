---
title: Welcome to CrossTech
author: Ed Pentz
authors:
  - Ed Pentz
date: 2006-08-22

categories:
  - News Release
archives:
  - 2006

---
Welcome to CrossTech, a new access-controlled blog to discuss developments in the online scholarly publishing world. Crossref’s mission is to foster dialogue and information sharing among publishers to enable innovation and collaboration. In order to do things collaboratively, publishers need to share information and communicate in an appropriate manner that takes into account anti-trust and competitive issues. The online publishing world changes quickly and many developments are driven by organizations outside of scholarly publishing so CrossTech provides publishers a &#8220;protected&#8221; space to discuss issues.

<!--more-->


  
Nature Publishing Group’s Xanadu blog is the model for CrossTech. Our hope is that CrossTech will build on the idea of Xanadu.
  
CrossTech Objectives: To provide a neutral forum where participants can post and discuss technical issues, link to relevant items on the Internet, make others aware of important developments and share and learn from each others’ experiences. CrossTech will promote collaboration and innovation among publishers in an appropriate manner taking account of anti-trust and competitive issues.
  
The main goals of CrossTech are:
  
- To provide a common forum for discussing new publishing technologies
  
- To develop a publisher technology community
  
- To determine common directions for key publishing technologies
  
- To foster best practices - and decide the best route to codify or standardize those practices
  
- To share experiences
  
- To act as an alerting mechanism for publishers to learn of relevant, new technology developments
  
Please let us know if you would like to participate. A username and password will be needed to read, post and comment. To obtain a username and password to post and comment, please email Anna Tolwinska <annat@crossref.org>.
  
We look forward to having you participate!