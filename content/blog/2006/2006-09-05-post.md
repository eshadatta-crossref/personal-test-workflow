---
title: Embedding standardized metadata in HTML
author: Ed Pentz
slug: post

authors:
  - Ed Pentz
date: 2006-09-05

categories:
  - Discussion
archives:
  - 2006

---
On the iSpecies blog Rod Page [describes how he extracts DOIs][1] from Google Scholar results - he does use the [Crossref OpenURL interface][2] and [Connotea][3] to get DOIs too. He also says &#8220;DOIs are pretty cool&#8221; which is good!

On another blog post to SemAnt Page [describes how he uses LSIDs and DOIs for Ant literature][4].

It seems that there is more and more of this type of use of the DOI so its great we have the OpenURL interface. Could the type of stuff that Page is doing be helped by publishers embedding metadata in their HTML pages? This could include licensing info and information for search engine crawlers.

<!--more-->



Ingenta and BMC embed metadata (are there others?) - here is a snippet from a BMC article -

<pre>&lt;cc:Work rdf:about="http://www.biomedcentral.com/1471-2148/3/16"&gt;
&lt;cc:license rdf:resource="http://creativecommons.org/licenses/by/2.0/"/&gt;
&lt;/cc:Work&gt;
&lt;cc:License rdf:about="http://creativecommons.org/licenses/by/2.0/"&gt;
&lt;cc:permits rdf:resource="http://web.resource.org/cc/Reproduction"/&gt;
&lt;cc:permits rdf:resource="http://web.resource.org/cc/Distribution"/&gt;
&lt;cc:requires rdf:resource="http://web.resource.org/cc/Notice"/&gt;
&lt;cc:requires rdf:resource="http://web.resource.org/cc/Attribution"/&gt;
&lt;cc:permits rdf:resource="http://web.resource.org/cc/DerivativeWorks"/&gt;
&lt;/cc:License&gt;
&lt;item rdf:about="http://www.biomedcentral.com/1471-2148/3/16"&gt;
&lt;title&gt;Inter-familial relationships of the shorebirds (Aves: Charadriiformes) based on nuclear DNA sequence data&lt;/title&gt;
&lt;dc:title&gt;Inter-familial relationships of the shorebirds (Aves: Charadriiformes) based on nuclear DNA sequence data&lt;/dc:title&gt;
&lt;dc:creator&gt;Ericson, Per GP&lt;/dc:creator&gt;
&lt;dc:creator&gt;Envall, Ida&lt;/dc:creator&gt;
&lt;dc:creator&gt;Irestedt, Martin&lt;/dc:creator&gt;
&lt;dc:creator&gt;Norman, Janette A&lt;/dc:creator&gt;
&lt;dc:identifier&gt;info:doi/10.1186/1471-2148-3-16&lt;/dc:identifier&gt;
&lt;dc:identifier&gt;info:pmid/12875664&lt;/dc:identifier&gt;
&lt;dc:source&gt;BMC Evolutionary Biology 2003, 3:16&lt;/dc:source&gt;
&lt;dc:date&gt;2003-07-23&lt;/dc:date&gt;
&lt;prism:publicationName&gt;BMC Evolutionary Biology&lt;/prism:publicationName&gt;
&lt;prism:publicationDate&gt;2003-07-23&lt;/prism:publicationDate&gt;
&lt;prism:volume&gt;3&lt;/prism:volume&gt;
&lt;prism:number&gt;1&lt;/prism:number&gt;
&lt;prism:section&gt;Research article&lt;/prism:section&gt;
&lt;prism:startingPage&gt;16&lt;/prism:startingPage&gt;
&lt;/item&gt;
&lt;/rdf:RDF&gt;
</pre>

 [1]: http://ispecies.blogspot.com/2006/08/extracting-dois.html
 [2]: https://www.crossref.org/education/retrieve-metadata/openurl/
 [3]: https://web.archive.org/web/20061205061750/http://www.connotea.org/
 [4]: http://semant.blogspot.com/2006/08/lsids-and-dois-for-ant-and-other.html
