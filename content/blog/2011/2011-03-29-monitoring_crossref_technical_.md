---
title: Monitoring Crossref Technical Developments
author: Anna Tolwinska
authors:
  - Anna Tolwinska
date: 2011-03-29

categories:
  - Support
archives:
  - 2011

---
Announcements regarding Crossref system status or changes are posted in an Announcements forum on our support portal (<http://support.crossref.org>). We recommend that someone from your organization monitor this forum to stay informed about Crossref system status, schema changes, or other issues affecting deposits and queries. Subscribe to this forum via RSS feed (<https://support.crossref.org/hc/en-us>) or select the &#8216;Subscribe’ option in the forum to subscribe by email.

The TWG Discussion forum replaces the TWG mailing list and can be accessed by members of the Crossref community who log in to our support portal. Intended topics include technical matters related to Crossref’s services, DOI issues and Crossref system operation.
