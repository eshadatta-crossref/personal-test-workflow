---
title: Celebrating ORCID at five
author: Ed Pentz
authors:
  - Ed Pentz
date: 2017-10-16
draft: false
categories:
  - ORCID
archives:
  - 2017

---

Happy birthday, ORCID! It's their fifth birthday today and it's gratifying to me---as a founding board member and former Chair of the board---to see how successful it has become. ORCID has a great staff, over 700 members from 41 countries and is quickly approaching 4 million ORCID iDs. Crossref---it's board, staff, and members---has been an ORCID supporter from the start. One example of this support is that we seconded Geoffrey Bilder to be ORCID's interim CTO for about eight months.

<!--more-->

Actually, Crossref has been involved with ORCID even before the start.

{{% imagewrap right %}} <img src="/images/blog/orcid-at-5.jpg" alt="ORCID turns five" width="300px" class="img-responsive" /> {{% /imagewrap %}}

ORCID's birthday recognizes when the registry went live in 2012 but the origins of what became ORCID stretch back to [a meeting that Crossref organized back in February 2007 on "Author IDs"](/blog/crossref-author-id-meeting). After this meeting there were many follow on discussions but it was clear that as an association of scholarly publishers Crossref didn't have suitable governance for an researcher identifier registry which needed support from a broader group of stakeholders.

Subsequent discussions between Nature and Thomson Reuters (represented by Howard Ratner Dave Kochalko) led---after many more meetings---to ORCID being set up as a new organization. ORCID was incorporated in September 2010 and the first meeting of the board of directors of ORCID was on October 8th, 2010.

A lot of people and organizations have contributed to getting ORCID to where it is today and it's been great to be a part of it and continue to contribute to their future.

> Reflecting on the creation of ORCID: it has shown the power of collaboration in improving scholarly research, and in making life easier and better for researchers.

Today they [celebrate in a number of fun ways](https://orcid.org/blog/2017/10/13/orcid5-coming) and, in particular, mark the occasion with the release of [a new set of educational resources](https://orcid.org/blog/2017/10/16/celebrating-orcid5-launch-new-resources).

From everyone in the Crossref community, here's to ORCID's continuing success!
