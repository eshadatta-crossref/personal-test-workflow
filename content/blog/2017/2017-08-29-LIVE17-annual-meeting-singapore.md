---
title: LIVE17 in Singapore is taking shape!
author: Ginny Hendricks
draft: false
authors:
  - Ginny Hendricks
date: 2017-08-29
categories:
  - News Release
  - Community
  - Crossref LIVE
  - Annual Meeting
archives:
  - 2017
---

Our annual meeting on 14th and 15th November, [LIVE17](/crossref-live-annual) is shaping up nicely with an exciting line-up of respected speakers talking around the theme of “Metadata + Infrastructure + Relations = Context”, with each half day covering some element of the main theme.
<!--more-->

* Day one, AM: *Metadata enables connections*
* Day one, PM: *How research and infrastructure is changing*
* Day two, AM: *Social challenges in the scholarly community*
* Day two, PM: *Who is using your metadata and what are they doing with it?*

This years updated format means both days will be packed with a mixture of plenary and breakout sessions and interactive activities. A cocktail reception with entertainment will be held in the Grand Marquee on the first evening.

A comprehensive agenda of the two-day event will be available shortly, but in the meantime we’ve provided a few talk teasers from six of our plenary speakers to whet your appetite:

|Speaker                                     |Title and Organization        | Talk title     |
|:-----------------------------------|:------------------------------|:-------------|
|[Theodora Bloom](#TB)|Executive Editor, The BMJ|Preparing to handle dynamic scholarly content: Are we ready?|
|[Casey Green](#CG)|Assistant Professor, Perelman School of Medicine, University of Pennsylvania|Research and literature parasites in a culture of sharing.|
|[Leonid Teytelman](#LT)|Co-founder and CEO, Protocols.io|A call to reduce random collisions with information; we can automatically connect scientists to the knowledge that they need.|
|[Nicholas Bailey](#NB)|Data Science Team, Royal Society of Chemistry|What does data science tell us about social challenges in scholarly publishing?|
|[Miguel Escobar Varela](#MV)|Assistant Professor of Theatre Studies, National University of Singapore|Digital Humanities in Singapore: some thoughts for the future.|
|[Kuansan Wang](#KW)|Managing Director, Microsoft Research Outreach|Democratize access to scholarly knowledge with AI.|
<br><br>

<a id="TB"></a>

###  Theodora Bloom - Preparing to handle dynamic scholarly content: Are we ready?

Historically, journals might expect a few 'Letters to the Editor" to discuss 'matters arising' after an article was published. But scholarly communications are becoming much more dynamic, with versions posted as 'preprints' before publication, corrections after publication, and potentially multiple versions of the same study appearing at different times. How should we handle this changing landscape for the benefits of researchers and consumers of the literature?

###  About Theodora Bloom

Theodora Bloom has been executive editor of The BMJ since June 2014. She has a PhD in developmental cell biology from the University of Cambridge and worked as a postdoctoral fellow at Harvard Medical School. She moved into publishing as an editor on the biology team at Nature, and in 1992 joined the fledgling journal Current Biology. After a number of years helping to develop Current Biology and its siblings Structure and Chemistry & Biology, Theo joined the beginnings of the open access movement. As the founding editor of Genome Biology she was closely involved in the birth of the commercial open access publisher BioMed Central. She joined the non-profit open access publisher Public Library of Science (PLOS) in 2008, first as chief editor of PLOS Biology and later as biology editorial director. She took the lead for PLOS on issues around data access and availability and launched PLOS's data sharing policy. At The BMJ she is responsible for operations, delivering the journal online and in print.

***

<a id="CG"></a>

### Casey Greene - Research and literature parasites in a culture of sharing.

Casey has been a strong champion of preprints and will discuss his efforts in this area including resources that he has shared to help advance the spread of preprints not only amongst researchers but publishers. These include letters to respond to journals that invite reviews but have unclear preprint policies. His lab members have also analyzed the licensing of preprints and the coverage of literature provided by the pirate repository, Sci-Hub. His talk will touch on each of these areas, and also a discussion of the Research Parasite and Symbiont Awards, which aim to advance recognition for data sharing and reuse.

###  About Casey Greene

Casey is an assistant professor in the Department of Systems Pharmacology and Translational Therapeutics in the Perelman School of Medicine at the University of Pennsylvania and the director of the Childhood Cancer Data Lab for Alex's Lemonade Stand Foundation. His lab develops deep learning methods that integrate distinct large-scale datasets to extract the rich and intrinsic information embedded in such integrated data. Before starting the Integrative Genomics Lab in 2012, Casey earned his PhD for his study of gene-gene interactions in the field of computational genetics from Dartmouth College in 2009 and moved to the Lewis-Sigler Institute for Integrative Genomics at Princeton University where he worked as a postdoctoral fellow from 2009-2012. The overarching theme of his work has been the development and evaluation of methods that acknowledge the emergent complexity of biological systems.

***

<a id="LT"></a>

### Leonid Teytelman - Call to reduce random collisions with information; we can automatically connect scientists to the knowledge that they need.

Every scientist knows that virtually all papers, including their own, contain mistakes. A key motivation for creating protocols.io was to make it possible to share corrections and optimizations of published research protocols and to have this information automatically reach the scientists using these methods. While pushing relevant knowledge to the users is built into all aspects of protocols.io, we can do a lot more. If publishers, Crossref, and reference management platforms collaborate, we can move beyond the search towards a point where important information automatically reaches the appropriate researchers.

### About Leonid (Lenny) Teytelman

Lenny is the Co-founder and CEO of protocols.io, an open access platform to share and discover research protocols. It enables scientists to make, exchange, improve and discuss protocols and it is poised to dramatically accelerate and to increase reproducibility of scientific research. Lenny did his graduate studies at UC Berkeley and finished his postdoctoral research at MIT. Lenny has a strong passion for sharing science and improving research efficiency through technology.

***

<a id="NB"></a>

### Nicholas Bailey - What does data science tell us about social challenges in scholarly publishing?

How can we facilitate the fair advancement and dissemination of knowledge? The risks and shortcomings within scholarly publishing are always under scrutiny, but some problems don’t seem to be going away. What should we do about obvious gender inequality within some disciplines, or the weight given to Impact Factor as a measure of quality? The Royal Society of Chemistry has a royal charter to publish scientific content in a way that serves the public interest, and as such its Data Science team devotes part of its time to analysing the social challenges facing scholarly publishing. In this talk, Nicholas Bailey will share some examples.

### About Nicholas Bailey

Nicholas Bailey is a web analytics expert, a swimmer, a father, and a data geek. After spending several years in the Marketing team at the Royal Society of Chemistry, ultimately managing the database marketing team, he moved out of Marketing and into the Data Science team in order to work more closely with agile teams of developers and strengthen his data analysis and coding skills. Nicholas has a lot to say about measuring digital products, machine learning, and the potential of data science to contribute to positive social outcomes.

***

<a id="MV"></a>

###  Miguel Escobar Varela - Digital Humanities in Singapore: some thoughts for the future.

Singapore-based researchers from a variety of disciplines are currently using digital tools to study the humanities, in areas as diverse as history and dance studies. This talk will present an overview of current projects and suggest a path for the growth of this field in Singapore. It argues that the future of DH requires better inter-institutional infrastructure for long-term data storage, clearer protocols for interoperability and more freely available and reusable datasets. This is easier said than done, but looking at the examples of other countries can provide some sources for inspiration.

### About Miguel Escobar Varela

Miguel Escobar Varela is an assistant professor in the University Scholars Programme (USP) at the National University of Singapore. At the USP, Dr. Varela teaches in the domain of Humanities and Social Sciences. He is a theatre researcher and software programmer. His interests are in teaching theatre through interactive websites and applying computational methods to study performances in Singapore and Indonesia.

***

<a id="KW"></a>

### Kuansan Wang - Democratize access to scholarly knowledge with AI.

With the advent of big data and cloud computing, artificial intelligence has made tremendous strides in recent years. Not only has machine surpassed humans in playing the chess game Go and Jeopardy game shows, reports of superhuman performance in other highly cognitive tasks, ranging from image classification to speech recognition, also abound. Have we reached a stage where the advancements in AI can help tackle a problem in scientific pursuits, namely, the access and the dissemination of scholarly knowledge? This talk describes Microsoft Academic, a project inside Microsoft Research that uses the state-of-the-art AI in natural language understanding and knowledge acquisition to harvest knowledge from scholarly communications and make it available on the web. The talk will describe the technical challenges that have been overcome, the world-wide research collaborations that have since been enabled, and discuss the potentials of making knowledge more readily available to the mass.

### About Kuansan Wang

Kuansan Wang is the Managing Director at Microsoft Research Outreach (MSR), where he started in March 1998 as a Researcher in the speech technology group working. In 2004, he moved to the speech product group and became a software architect where he helped create and ship the product Microsoft Speech Server, which is still powering the corporate call center for Microsoft. Since September 2007, he has been back at MSR, joining the newly founded Internet Service Research Center with a mission to revolutionize online services and make Web more intelligent.  In March 2016, he took on an additional role as a Managing Director of MSR Outreach, an organization with the mission to serve the research community.


[Read more about our annual events](/crossref-live-annual)<br>
[Register now for LIVE17](https://www.eventbrite.com/e/crossref-live17-singapore-november-14-15-crlive17-registration-34604951341?ref=ebtnebregn)
