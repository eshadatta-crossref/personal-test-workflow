---
title: Dr. Livingstone, I presume…a two month expedition deep into the heart of research publishing
author: Amanda Bartell
draft: false
authors:
  - Amanda Bartell
date: 2017-12-13
categories:
  - Community
  - User Experience
  - Member Experience
archives:
  - 2017
---
Hello there. I'm [Amanda Bartell](/people/amanda-bartell/), and I joined the [Crossref team](/people/org-chart/) in mid-October as the new Head of Member Experience. My new Member Experience team will be responsible for metadata users as well as members, onboarding new accounts, supporting existing ones, and making sure that everyone can make the most of Crossref services - an an easy and efficient way. I have spent the last couple of months exploring the world of academic publishing and what our members need - and it's been fascinating!

<!--more-->
## Expedition members

The new Member Experience team is made up of some people who are new to Crossref and Scholarly Publishing and some whose names you'll probably recognize!

-   [Anna Tolwinska](/people/anna-tolwinska/) (Member Experience Manager) will support existing members in understanding the quality of metadata they deposit with us, and how they can best make use of our other products and services.
-   [Paul Davis](/people/paul-davis/) (Product Support Specialist) and [Shayn Smulyan](/people/shayn-smulyan/) (Product Support Associate) will continue to provide excellent technical support to all creators and consumers of our metadata.
-   [Gurjit Bhullar](/people/gurjit-bhullar/)  (Membership Coordinator) will help new applicants who want to join Crossref understand the member obligations and have a smooth induction journey.

We'll be expanding the team in 2018 to support you further - [watch this space!](/blog/status-i-am-new/)

## What a diverse ecosystem

My background is educational publishing, so this has been my first foray into the world of scholarly publishing. In my first few months I've been lucky enough to attend three very different events with Crossref - Frankfurt Book Fair, our annual meeting ([LIVE17](/crossref-live-annual/)) in Singapore, and an OpenCon event in Oxford. Each one has given me the chance to talk to our members and other constituents, and I've been really struck by what a diverse bunch you are:  from small volunteer-led society journals through universities to commercial behemoths; from Albania to Zambia (and 125 countries in between); covering everything from Ancient History to X-Ray Spectrometry.

## Expedition equipment to suit the climate

This diversity gives my team a huge responsibility. We need to make sure that the support we provide to you can meet the needs of everyone -  whether you're a multinational publisher with a large team of xml specialists, or a small team of enthusiastic academics. Everyone should be able to clearly understand and take advantage of what Crossref offers both to you as an organization and to the wider scholarly community.

With this in mind, we're going to be making a few changes to the support materials we provide over the next 12 months---rewriting them so they're clearer for everyone, re-structuring our support center so there's a separate route through depending on your level of technical expertise and closer links with our main website, plus providing support in different languages and different formats.

## Sticking together in a harsh environment

As someone who has previously worked in commercial publishing, something else that has struck me about working in a member organization is the difference between members and traditional "customers". It's been fantastic to see how involved many of you are in Crossref. From taking part in our various committees and working groups, to helping to organize [LIVE Local events](/events/), to attending webinars and training, it's obvious that you feel a real sense of ownership over Crossref and our shared mission.

We're hoping to make use of that great sense of community in 2018 by improving our member center, giving you more access to see the level of metadata you're sharing with the community (and that others are sharing) and providing more options for you to communicate with, and support each other. We're also going to be improving the education we offer for new members, to make sure that everyone is aware of the joint mission we all have to improve research communications. Most long time members know it's so much more than just having a DOI, and we need to make sure that our new members are aware of this too and share our vision.

## Leaving no-one behind

We have a lot of plans for the Member Experience team in 2018, but it's key that everything we do meets the needs of all our members. If you have any suggestions for how we can improve your member experience, [do let me know](mailto:feedback@crossref.org?subject=Member Experience suggestion).
