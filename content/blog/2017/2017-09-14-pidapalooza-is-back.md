---
title: PIDapalooza is back and wants your PID stories
author: Ginny Hendricks
draft: false
authors:
  - Ginny Hendricks
date: 2017-09-14
categories:
  - PIDapalooza
  - Persistence
  - Identifiers
  - Collaboration
  - Community
  - Meetings
archives:
  - 2017
---

Now in its second year, this “open festival of persistent identifiers” brings together people from all walks of life who have something to say about PIDs. If you work with them, develop with them, measure or manage them, let us know your PID adventures, pitfalls, and plans by submitting a talk by September 18. It'll be in Girona, Spain, January 23-24, 2018.

<!--more-->

One of the great strengths of last year’s PIDapalooza was the number of people who spoke and all the conversations that were kindled. **So if you're thinking of going, we encourage you to [propose a talk](https://docs.google.com/forms/d/e/1FAIpQLSdR7TGVGMRUVVgMejMqJhgKa8xdL-GDGyv97g_RSRumBAjgTg/viewform), so we can hear what you're working on and you can get some feedback**.

At the inaugural PIDapalooza event Crossref took to the stage twice, with Ed Pentz covering Org IDs and Joe Wass talking about Event Data.

Here we have Joe’s memories of the event and Ed’s update on the Org ID status.

### Joe Wass reflects:

At Crossref, the subject of Persistent Identifiers is something we care deeply about, and linking between DOIs, ORCID iDs and other identifiers is the reason we get up in the morning. But a whole conference dedicated to them? If I'm honest, the first time I heard about PIDapalooza I thought the subject was rather niche.

How wrong I was. It turns out there are people from all walks of life who care about "things" using persistent identifiers to link, describe and reference them. There was a great balance between presenters and attendees, and the programme meant that lots of people had a chance to speak. We heard about identifiers for research vessels, pieces of scientific equipment, individual bottles of milk, plus the usual subjects like scholarly publishing, datasets, organisations and funders, and how to cite them.

Between sessions we chatted over a wide range of subjects, noted similarities between subject areas, offered advice and exchanged ideas. Who knew this stuff was all related?


### Ed Pentz on plans for the new PID on the block - Organization IDs

An important presentation at the 2016 PIDapalooza meeting was on organization identifiers. A week before the conference Crossref, DataCite and ORCID released three documents for public comment outlining a proposed way forward. The goal is launch and sustain an open, independent, non-profit organization identifier registry to facilitate the disambiguation of researcher affiliations. At the packed PIDapalooza session Crossref, DataCite and ORCID gave an update on their work over the previous year and their proposals going forward.

There was a lively discussion and debate about the issues. Following the meeting the three organizations set up the OI Project Working Group with a broad group of stakeholders. The group has been meeting over the last year and will release two documents next week - a set of Governance Recommendations and Product Principles and Recommendations for community feedback. So watch this space.

The PIDapalooza conference really helped galvanize the work in this area by bringing together a broad range of people interested in persistent identifiers. If you have an idea about PIDs, please come and tell us about it.

---
Check out the [decks from last year's talks](https://pidapalooza.figshare.com/), the [PIDapalooza website](https://www.pidapalooza.org/) with all the info, and [sumbit a proposal for your talk before September 18](https://docs.google.com/forms/d/e/1FAIpQLSdR7TGVGMRUVVgMejMqJhgKa8xdL-GDGyv97g_RSRumBAjgTg/viewform).
