---
title: Crossref and colleagues in South Korea
author: Ginny Hendricks
draft: false
authors:
  - Ginny Hendricks
date: 2017-06-30
categories:
  - ORCID
  - DataCite
  - Crossref LIVE
  - Community
  - Infrastructure
  - Collaboration
  - Education
archives:
  - 2017
---
### Connecting Crossref, ORCID, DataCite, and our communities

**Q:** What do you get if you combine our three organisations for a week to catch up with our Korean community - publishers, librarians, universities, researchers, and service providers?
**A:** Two events, plenty of meetings, great conversations and feedback, fabulous Korean hospitality, and a little jet-lag.<!--more-->

{{% imagewrap right %}}<img src="/images/blog/tweet-south-korea-blog.jpg" alt="tweet image" width="350px" />{{% /imagewrap %}}

Over the past few years, Crossref has seen huge growth in our members in Korea. We have nine [Sponsoring Affiliates](/community/sponsors) (who look after nearly 1,000 members between them), two Sponsoring Members and nearly 80 Library members. With the [International DOI Foundation (IDF)](http://www.doi.org) strategy meeting taking place in Daejon, it seemed sensible to combine that with our own events and meetings with key organizations. This also fitted nicely with some plans that ORCID and DataCite had, so we combined forces.

We (that's me, Rachael Lammey, Ed Pentz, and Geoffrey Bilder) hosted a [Crossref LIVE local](/events/) event on Monday 12th June for around 80 members and affiliates. We were joined by Alice Meadows and Nobuko Maiyairi (ORCID), Martin Fenner (DataCite), and Professor Sun-Tae Hong (Seoul National University) as co-presenters. We looked at the global reach of Korean research, and how registering content with Crossref and participating in services like Reference Linking helps create valuable connections between research outputs. With so many established members in Korea, we were able to go beyond the basics and emphasize the importance of metadata input, metadata delivery, and preview our upcoming [Event Data](/services/event-data/) service. We also talked data-sharing and the value of integrating ORCID iDs into publisher and institution workflows.

<img src="/images/blog/growth-research-outputs-asia-pacific.png" alt="Growth chart" class="img-responsive"/>
_Growth in research outputs in Asia Pacific 2009-2017. Source: Web of Science databases SCI-E, SSCI and AHCI only, downloaded 19/4/2017. Data provided by Wiley (thank you!)_

{{% imagewrap right %}}<img src="/images/blog/jgic-seoul.jpg" alt="JGIC image" width="350px" />{{% /imagewrap %}}

Later in the week we took a multi-pronged approach to highlight the many shared principles of our organizations and discuss the specific initiatives we’re collaborating on. We held the [Joint Global Infrastructure Conference](https://orcid.org/content/joint-global-infrastructure-conference) covering the global nature of what we do and the connections/interoperability between ORCID, DataCite and Crossref. This interoperability and our governance structures lend themselves to cooperation on other initiatives such as [Metadata 2020](https://twitter.com/metadata2020?lang=en) and [The OI Project](/blog/the-oi-project-gets-underway-planning-an-open-organization-identifier-registry/), which we were able to share.

> <a class="twitter-timeline"  href="https://twitter.com/hashtag/jgic_seoul" data-widget-id="879259929458225152">Check out all #jgic_seoul tweets.</a>

Guest speakers volunteered to talk about how they work with our organizations - we were joined by Choon Shil Lee from the [Korean Association of Medical Journal Editors (KAMJE)](https://www.kamje.or.kr/) to demonstrate their ORCID integrations, and Hideaki Takeda from the [Japan Link Centre (JaLC)](https://japanlinkcenter.org/top/english.html) who discussed the infrastructure and services they use to register and disseminate content globally. User stories like this are great - they highlight how people work with our services, give others ideas, and also flag up where we can do more.

Part of doing more involved providing clarification on Crossref’s position alongside other DOI Registration Agencies. With a new Registration Agency in Korea, we needed to communicate the global nature of what we do to help our members achieve their discoverability goals, as [not all DOIs are made equal](/membership/#member-obligations-and-benefits/). Through working with ORCID and DataCite colleagues we were able to place great importance both on our work worldwide, and on the benefits to Korean societies in collaborating outside national boundaries.

{{% imagewrap right %}}<img src="/images/blog/plug-image.jpg" alt="Plug socket image" width="300px" />{{% /imagewrap %}}

Combining talks from our three organizations was a great opportunity to emphasize the importance of shared global infrastructure. Geoffrey Bilder’s plug socket analogy is apt - services that work cross-border, cross-language, and cross-subject areas streamline processes for all of our different communities and enable research to travel beyond national boundaries and help it be found, linked, cited and assessed.

Want to find out more? Slides from both meetings are available [here](https://www.slideshare.net/Crossref/tag/live-seoul-2017) and [here](https://orcid.org/content/joint-global-infrastructure-conference), and watch out for further collaborative events.
