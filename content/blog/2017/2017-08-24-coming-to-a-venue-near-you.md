---
title: Coming to a venue near you
author: Vanessa Fairhurst
draft: false
authors:
  - Vanessa Fairhurst
date: 2017-08-24
categories:
  - Crossref LIVE
  - Community
  - Outreach
  - Education
archives:
  - 2017
---

First of all – hello! I’m Vanessa. I’m fairly new to Crossref, having just joined our outreach team a few weeks ago. I previously worked in International Development, enabling individuals and institutions in Africa, Asia and Latin America to access cutting edge scholarly research and knowledge, supporting national development and transforming lives.

<!--more-->

A firm belief in the importance of connecting research and information around the world led me to Crossref where my role of International Community Outreach Manager connects me with a range of different people working across diverse disciplines and sectors. I’ll be supporting the coordination of our local LIVE events and helping to set up an ambassador program (more information on this coming soon) to deepen regional connections around the globe. You can read more about myself and my colleagues at Crossref on our [People](/people/) page.

As Crossref membership continues to grow globally, it becomes increasingly important for us to look at new ways to engage with our international membership base.

You may have heard about our LIVE local events, or even attended one in person before. These are free-to-attend, one day, regional events (local to you), providing a tailored program of activities which include information on the key concepts of Crossref, the services we offer and our future plans.

In the past year we have held LIVE local events in Brazil, Beijing, Boston and most recently Seoul. We also have a [London LIVE](https://www.eventbrite.com/e/crossref-live-london-tickets-35757538761) event coming up soon. Next year we are aiming to be even more ambitious, hoping to expand our activities to a number of different countries around the world.

*Images left to right, Crossref LIVE participants in Seoul, Crossref LIVE speakers in Brazil, and literature we use at our LIVE events*

|<img src="/images/blog/live-seoul2-2017.jpg" alt="Participants at Crossref LIVE Seoul" height="250px" width="300px"/>|<img src="/images/blog/live-brazil2-2017.jpg" alt="Speakers at Crossref LIVE Brazil" height="250px" width="300px"/>|<img src="/images/blog/live-literature2.jpg" alt="LIVE literature" height="250px" width="300px"/>|

When running our LIVE local events, we collaborate with local organizations to ensure they are appropriate, accessible, and applicable to the country context. Members support us by lending their local expertise with regards to venue selection, suggestions for speakers, tailored content, translation of materials and participant enrolment. We collaborate on logistics, content, Crossref speakers and the promotion of the event to our members and the wider community.

When running our LIVE local events, we collaborate with local organizations to ensure they are appropriate, accessible, and applicable to the country context. Members support us by lending their local expertise with regards to venue selection, suggestions for speakers, tailored content, translation of materials and participant enrollment. We collaborate on logistics, content, Crossref speakers and the promotion of the event to our members and the wider community.

We will release more information of upcoming regional events in due course, but we are working on the following countries as priorities for 2018-19:

* Asia-Pacific: Malaysia, Indonesia, Japan, Taiwan, Australia
* Central Asia: India
* Latin America: Mexico, Colombia, Chile, Brazil
* Middle East: UAE (Dubai or Abu Dhabi)
* Africa: South Africa, Kenya
* Eastern Europe: Turkey, Greece, Bulgaria, Romania, Serbia, Poland
* Western Europe: Germany, Spain, UK
* North America: Canada, USA

If you are interested in hosting a LIVE local event or have any suggestions for one in your region, then we would love to hear from you. View more information on our [LIVE locals](/events/) page or [contact us](mailto:feedback@crossref.org) to hear more or get involved.

***
