---
title: The OI Project gets underway planning an open organization identifier registry
author: Ed Pentz
draft: false
authors:
  - Ed Pentz
date: 2017-03-28
categories:
  - Collaboration
  - DataCite
  - ORCID
  - CDL
  - Identifiers
  - Organization Identifier
  - Community
  - Infrastructure
  - Collaboration
archives:
  - 2017
---

At the end of October 2016, Crossref, DataCite, and ORCID [reported on](/blog/the-organization-identifier-project-a-way-forward/) collaboration in the area of organization identifiers. We issued three papers for community comment and after input we subsequently announced the formation of The OI Project, along with a call for expressions of interest from people interested in serving on the working group. <!--more-->

We had a great response and are happy to report that the Working Group has now been established, and is already underway with work to develop a plan for an open, independent, not-for-profit, sustainable, organization identifier registry.  <!--more-->

There is [information about the OI Project Working Group on the ORCID website](https://orcid.org/content/organization-identifier-working-group) including a list of the [17 working group members](https://orcid.org/content/organization-id-working-group). They represent a broad range of scholarly communications stakeholders. Our scope of work includes three separate but interdependent areas:

* Governance;
* Registry Product Definition; and
* Business Model & Funding.

The initial goal of the Working Group is to create a thorough and robust implementation plan by the end of 2017.

Please take a look at the website for more information and we’ll provide updates as things progress throughout the course of the year.

**Please [contact us](mailto:oi-project@orcid.org) with any questions.**
