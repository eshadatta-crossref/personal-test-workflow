---
title: Scenario planning for our future
author: Ed Pentz
draft: false
authors:
  - Ed Pentz
date: 2017-08-28
categories:
  - Board
  - Governance
  - Community
  - Election
  - Strategy
archives:
  - 2017
---


Crossref is governed by a [board of directors](/board-and-governance/)  that meets in person three times a year in March, July and November. At the July meeting the board typically spends a significant amount of time on strategic planning in addition to its usual activities such as financial oversight, approving investment in new services based on staff and committee recommendations, reviewing and approving policies and fees for new and existing services and generally making sure Crossref is healthy and well run.
<!--more-->

This year we worked with a facilitator to look farther into the future than normal using a technique called [scenario planning](https://en.wikipedia.org/wiki/Scenario_planning) to map out “strategic agendas” for the next five years. Scenario-based strategic planning doesn’t try to predict the future but allows us to be flexible in planning by looking at a range of different possible eventualities. This is particularly useful for Crossref because scholarly research and communications is changing rapidly and we operate in a very complex environment.

To prepare for the meeting our facilitator, Susan Stickely, prepared 12 “critical uncertainties” - impactful issues that could go either way and that will affect how Crossref works, its mission and even whether it needs to exist. To develop the critical uncertainties Susan interviewed Crossref staff, board members, general members and scholarly communications community influencers and we held a preparatory group exercise at the March board meeting. The critical uncertainties are:

1. **Scholarly Communication Landscape**: Increasing diversity? Or publishing disintermediated?
2. **Machine Learning / Artificial Intelligence**: Supporting? Or obsoleting the researcher and publishers?
3. **Policy and Regulation**: Limiting? Or visionary?
4. **Financing of Scholarly Communication**: Shrinking Pool? Or  Expanding Pool?
5. **Rise of Pre-print, New Content Sources**: New, non-traditional? Or  De-formalizing?
6. **Tracking and Privacy**: Increased Privacy? Or  Loss of Privacy?
7. **Cybersecurity**: Secure? Or  Vulnerable, Insecure?
8. **Publisher Sustainability**: Slow Progress? Or  Fast Progress?
9. **Impact of Open**: Open or Closed? Or Slow to Change?
10. **Source of Prestige and Recognition**: New Source? Or Publisher, Institution?
11. **Quality and Accuracy of Content**: High? Or Low?
12. **Geopolitical Stability and Stance**: Stable, Unified? Or  Unstable, Fragmented

In addition, from the interviews Susan was able to summarize Crossref’s distinctive competencies as:


* Having a reputation as a trusted, neutral one-stop source of metadata and services
* Managing scholarly infrastructure with technical knowledge and innovation
* Convening and facilitating scholarly communications community collaboration

To be successful Crossref will need to continue to invest in, apply, and evolve these distinctive competencies and strategic dilemmas and challenges.

Over a day and half of discussions and breakout sessions the board and staff drew up a number of scenarios and created a draft strategic agenda for Crossref. Over the next couple of months we’ll be working on refining the strategic agenda and will be presenting the results to members in the next couple of months.

One theme that emerged is for Crossref to engage more with funders and build on the work with done with them in creating the Crossref Funder Registry. We have started a new [Funder Advisory Group](/working-groups/funders) and, among other things, are working with them on a prototype for a new registry of grant identifiers.

In the regular board session the board approved three recommendations from the  [Membership and Fees Committee](/committees/membership-and-fees/):

1. To approve the recommendations with respect to volume discounts for current deposits of posted content (i.e. preprints).
2. To create a new “peer review report” content type with a specific metadata schema and a bundled fee of $1.25 to be charged for a content item and all the reports associated with it.
3. To update the metadata delivery offering to have a single agreement that covers all metadata APIs/delivery routes, to adopt a single (updated) fee structure, and to remove case-by-case opt-outs for metadata.

Item number 3 involves a number of big changes - for example the removal of the case-by-case opt outs requires a change to the main Membership Agreement - so we will be sending out more information to members and Affiliates in September and October about the changes and our implementation plans.

You can see the [full history of the motions from every Board meeting](/board-and-governance/#motions) on our website.

Another major issue that the board discussed is the upcoming [election for the board of directors](/board-and-governance/elections/). In order to broaden participation and be inclusive there was a new process this year. The Nominating Committee put out a call for expressions of interest for candidates to be on the slate for the election. We had a great response and there were 25 expressions of interest reviewed by the Nominating Committee who came up with a slate of nine excellent candidates for the six seats up for election. This is the first time that there are more candidates than seats on the slate so it’s particularly important for members to vote this year. See the recent [blog post about the election process and the slate](/blog/2017-election-slate/) for more details.

The next board meeting is in November  in conjunction with [Crossref LIVE17 in Singapore](/crossref-live-annual/).
