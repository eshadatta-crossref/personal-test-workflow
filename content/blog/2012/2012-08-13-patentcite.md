---
title: PatentCite
author: Geoffrey Bilder
authors:
  - Geoffrey Bilder
date: 2012-08-13

categories:
  - Crossref Labs
  - Interoperability
  - Patents
archives:
  - 2012

---
If you’ve ever thought that scholarly citation practice was antediluvian and perverse- you should check-out patents some day.

Over the past year of so Crossref has been working with [Cambia][1] and the [The Lens][2] to explore how we can better link scholarly literature to and from the patent literature. The first object of our collaboration was to attempt to link patents hosted on the new, beta version of The Lens to the Scholarly literature. To do this, Crossref and Cambia been enhancing Crossref’s citation matching mechanisms in order to better resolve the wide variety of eclectic and terse patent citation styles to Crossref DOIs.

You can see the results of these ongoing attempts on the The Lens beta site where all of The Len’s <strike>8 million+</strike> 80 million+ patents and applications (obtained through subscriptions with [WIPO][3], [USPTO][4], [EPO][5] and [IP Australia][6]) are starting to be linked directly to the scholarly literature. See, for example:

`http://beta.lens.org/lens/patent/US\_RE42150\_E1/citations`  
[_Editor's update: Link is broken. Removed January 2021_]

Crossref has taken this matched data and has now released a [Crossref Labs \*experimental\* service , called PatentCite][8], that allows you to take any Crossref DOI and see what Patents in the The Lens system cite it.

As with all Crossref Labs services- this one is likely to be:

a) As stable as the global economy

c) As reliable as a UK train

ii) Out-of-date. It is based on a snapshot of Crossref /Lens data.

1) As accurate as my list ordering

Howzat for an SLA?

As we get feedback from Crossref’s membership and as we gain more experience linking Patents to and from the scholarly literature, we will explore including this functionality in our production CitedBY service. But until then- please send us your feedback on this experimental service.

 [1]: https://web.archive.org/web/20201202050237/http://www.cambia.org/
 [2]: http://beta.lens.org/lens/
 [3]: http://www.wipo.int/
 [4]: http://www.uspto.gov/
 [5]: http://www.epo.org/
 [6]: mailto:http://www.ipaustralia.gov.au/
 [8]: https://web.archive.org/web/20121023015419/http://patents.labs.crossref.org/
