---
title: RSS Validator in the Spotlight
author: thammond
authors:
  - thammond
date: 2007-02-08

categories:
  - RSS
archives:
  - 2007

---
Sam Ruby [responds][1] to Brian Kelly’s [post][2] about the [RSS Validator][3] and its treatment of RSS 1.0, or rather, RSS 1.0 modules. As Ruby notes:

> _&#8220;There is no question that RSS 1.0 is widely deployed. [RSS 1.0][4] has a [minimal][5] core. The validation for that core is pretty solid.&#8221;_

Not sure if I’d seen that [RSS comparison table][5] before, but it is reassuring. (Oh, and see the really simple case off to the right. 😉
  
Good point, anyway about contributing test cases. I guess we should really submit a PRISM test case. And yes, the Validator is somewhat buggy as some recent testing confirms. On which more later.

 [1]: http://www.intertwingly.net/blog/2007/02/07/Validating-the-Validators
 [2]: http://ukwebfocus.wordpress.com/2007/02/07/validators-dont-always-work/
 [3]: http://feedvalidator.org/
 [4]: http://web.resource.org/rss/1.0/
 [5]: http://www.intertwingly.net/slides/2003/rssQuickSummary.html