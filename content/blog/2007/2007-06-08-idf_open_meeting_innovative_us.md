---
title: 'IDF Open Meeting: Innovative uses of the DOI system'
author: Ed Pentz
authors:
  - Ed Pentz
date: 2007-06-08

categories:
  - Conference
archives:
  - 2007

---
Please see the details of the IDF Annual Meeting and a related Handle System Workshop in Washington, DC on June 21 which may be of interest - [http://www.crossref.org/crweblog/2007/06/international\_doi\_foundation_a.html][1]

 [1]: http://www.crossref.org/crweblog/2007/06/international_doi_foundation_a.html