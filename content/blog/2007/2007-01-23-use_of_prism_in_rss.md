---
title: Use of PRISM in RSS
author: thammond
authors:
  - thammond
date: 2007-01-23

categories:
  - Metadata
archives:
  - 2007

---
Was rooting around for some information and stumbled across this page which may be of interest:
  
<http://googlereader.blogspot.com/2006/08/namespaced-extensions-in-feeds.html>
  
Namespaced Extensions in Feeds
  
Thursday, August 03, 2006
  
posted by Mihai Parparita
  
_“I wrote a small MapReduce program to go over our BigTable and get the top 50 namespaces based on the number of feeds that use them.”_<table border=0 cellpadding="5"> 

</table> 

Seems quite an impressive percentage for PRISM.