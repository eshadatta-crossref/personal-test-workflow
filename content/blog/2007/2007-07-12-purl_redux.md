---
title: PURL Redux
author: thammond
authors:
  - thammond
date: 2007-07-12

categories:
  - Identifiers
archives:
  - 2007

---
Seems that there’s life in the old dog yet. :~) See [this post][1] about PURL from Thom Hickey, OCLC, This extract:

> _OCLC has contracted with Zepheira to reimplement the PURL code which has become a bit out of date over the years. The new code will be in written in Java and released under the Apache 2.0 license._

 [1]: http://outgoing.typepad.com/outgoing/2007/07/purl2.html