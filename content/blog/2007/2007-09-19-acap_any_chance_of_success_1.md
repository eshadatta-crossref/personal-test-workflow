---
title: 'ACAP - Any chance of success?'
author: Ed Pentz
authors:
  - Ed Pentz
date: 2007-09-19

categories:
  - Search
archives:
  - 2007

---
ACAP has released [some documents][1] outlining the use cases they will be testing and some proposed changes to the Robots Exclusion Protocol (REP) - both robots.txt and META tags. There are some very practical proposals here to improve search engine indexing. However, the only search engine publicly participating in the project is <http://www.exalead.com/> (which according to Alexa attracted 0.0043% of global internet visits over the last three months). The main docs are &#8220;ACAP pilot Summary use cases being tested&#8221;, &#8220;ACAP Technical Framework - Robots Exclusion Protocol - strawman proposals Part 1&#8221;, &#8220;ACAP Technical Framework - Robots Exclusion Protocol - strawman proposals Part 2&#8221;, &#8220;ACAP Technical Framework - Usage Definitions - draft for pilot testing&#8221;.

What would cause other search engines to recognize the ACAP protocols rather than ignore them? A lot of publishers implementing this and requiring search engines to recognize it to index content could put pressure on the engines. Maybe.

 [1]: https://web.archive.org/web/20071007123940/http://www.the-acap.org/project-documents.php
