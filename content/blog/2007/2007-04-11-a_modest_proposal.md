---
title: A Modest Proposal
author: thammond
authors:
  - thammond
date: 2007-04-11

categories:
  - Linking
archives:
  - 2007

---
Was just reminded (thanks, Tim) of the possibility of using a special tag in bookmarking services to tag links to documents of interest to a given community. I think this is a fairly well-established practice. Note that e.g. the [OAI-ORE][1] project is using [Connotea][2] to bookmark pages of interest and tagging them &#8220;**oaiore**&#8221; which can then be easily retrieved using the link <http://web.archive.org/web/20160402182544/http://www.connotea.org/>.  

I would suggest that Crossref members might like to consider using the tag &#8220;**crosstech**&#8221; in bookmarking pages about publishing technology, so that the following links might be used to retrieve documents of interest to this readership:

  * del.icio.us - <https://web.archive.org/web/20071206033322/https://del.icio.us/
      * CiteULike - <http://www.citeulike.org/tag/crosstech>
          * Connotea - <http://web.archive.org/web/20160402182544/http://www.connotea.org/>
              * etc. </ul>

 [1]: http://www.openarchives.org/ore/
 [2]: https://web.archive.org/web/20061205061750/http://www.connotea.org/
