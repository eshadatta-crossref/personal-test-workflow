---
title: Marking up DOI
author: thammond
authors:
  - thammond
date: 2007-09-11

categories:
  - XMP
archives:
  - 2007

---
(**Update - 2007.09.15:** Clean forgot to add in the <tt>rdf:</tt> namespace to the examples for <tt>xmp:Identifier</tt> in this post. I’ve now added in that namespace to the markup fragments listed. Also added in a comment [here][1] which shows the example in RDF/XML for those who may prefer that over RDF/N3.)
  
So, as a preliminary to reviewing how a fuller metadata description of a Crossref resource may best be fitted into an XMP packet for embedding into a PDF, let’s just consider how a DOI can be embedded into XMP. And since it’s so much clearer to read let’s just conduct this analysis using RDF/N3. (Life is too short to be spent reading RDF/XML or C++ code. :~)
  
(And further to Chris Shillum’s comment [(**Update - 2007.09.15:** Clean forgot to add in the <tt>rdf:</tt> namespace to the examples for <tt>xmp:Identifier</tt> in this post. I’ve now added in that namespace to the markup fragments listed. Also added in a comment [here][1] which shows the example in RDF/XML for those who may prefer that over RDF/N3.)
  
So, as a preliminary to reviewing how a fuller metadata description of a Crossref resource may best be fitted into an XMP packet for embedding into a PDF, let’s just consider how a DOI can be embedded into XMP. And since it’s so much clearer to read let’s just conduct this analysis using RDF/N3. (Life is too short to be spent reading RDF/XML or C++ code. :~)
  
(And further to Chris Shillum’s comment][2] on my earlier post [Metadata in PDF: 2. Use Cases][3] where he notes that Elsevier are looking to upgrade their markup of DOI in PDF to use XMP, I’m really hoping that Elsevier may have something to bring to the party and share with us. A consensus rendering of DOI within XMP is going to be of benefit to all.)
  
(Continues.)

<!--more-->


  
Within an XMP packet our first idea might be to include the DOI using the Dublin Core (DC) schema element <tt>dc:identifier</tt> in minimalist fashion:

<pre>@prefix dc: &lt;http://purl.org/dc/elements/1.1/&gt; .
&lt;&gt;   dc:identifier "10.1038/nrg2158" .
</pre>

This simply says that the current document (denoted by the empty URI &#8220;<tt><></tt>&#8220;) has a string property <tt>"10.1038/nrg2158"</tt> which is of type <tt>identifier</tt> from the <tt>dc</tt> (or Dublin Core) schema which is identified by the URI <tt><http://purl.org/dc/elements/1.1/></tt>.
  
Now, since this is just a DOI and the wider public cannot be expected to know about DOIs, it would surely be better to present the DOI in URI form (<tt>doi:</tt>) as

<pre>@prefix dc: &lt;http://purl.org/dc/elements/1.1/&gt; .
&lt;&gt;   dc:identifier "doi:10.1038/nrg2158" .
</pre>

or, using a registered URI form (<tt>info:</tt>) as

<pre>@prefix dc: &lt;http://purl.org/dc/elements/1.1/&gt; .
&lt;&gt;   dc:identifier "info:doi/10.1038/nrg2158" .
</pre>

Aside: This shows up a limitation of XMP where the DC schema property value for <tt>dc:identifier</tt> is fixed as type **<tt>Text</tt>**. The natural way to express the above in RDF/N3 would be as:

<pre>@prefix dc: &lt;http://purl.org/dc/elements/1.1/&gt; .
&lt;&gt;   dc:identifier &lt;info:doi/10.1038/nrg2158&gt; .
</pre>

which says that the value is a URI (type **<tt>URI</tt>** in XMP terms), not a string (type **<tt>Text</tt>** in XMP terms). We either have to flout the XMP specification or else live with this restriction. We’ll opt for the latter for now.
  
But, the XMP Spec deprecates the use of <tt>dc:identifier</tt> since the context is not specific. (Note that that’s what was just discussed above. The limitation is built into XMP which builds on RDF but does not fully endorse the RDF world view.) Instead the XMP Spec recommends using <tt>xmp:Identifier</tt> since the context can be set using a qualified property as:

<pre>@prefix rdf: &lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#&gt; .
@prefix xmp: &lt;http://ns.adobe.com/xap/1.0/&gt; .
@prefix xmpidq: &lt;http://ns.adobe.com/xmp/Identifier/qual/1.0/&gt; .
&lt;&gt;   xmp:Identifier  [
a rdf:Bag;
rdf:_1  [
xmpidq:Scheme "DOI";
rdf:value "10.1038/nrg2158" ] ] .
</pre>

This says the string <tt>"10.1038/nrg2158"</tt> belongs to the scheme <tt>"DOI"</tt>.
  
Here we have used the scheme &#8220;DOI&#8221; and, as noted above, for wider recognition it would be better to employ one of the URI forms, e.g.

<pre>@prefix rdf: &lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#&gt; .
@prefix xmp: &lt;http://ns.adobe.com/xap/1.0/&gt; .
@prefix xmpidq: &lt;http://ns.adobe.com/xmp/Identifier/qual/1.0/&gt; .
&lt;&gt;   xmp:Identifier  [
a rdf:Bag;
rdf:_1  [
xmpidq:Scheme "URI";
rdf:value "doi:10.1038/nrg2158" ] ] .
</pre>

This says the string <tt>"doi:10.1038/nrg2158"</tt>belongs to the scheme <tt>"URI"</tt>.
  
But this is the unregistered URI form (<tt>doi:</tt>), so should we be using instead the registered form (<tt>info:</tt>)? Well, turns out that this construct for <tt>xmp:Identifier</tt> is an <tt>rdf:Bag</tt> so we can include more than one term. How about using this construct then:

<pre>@prefix rdf: &lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#&gt; .
@prefix xmp: &lt;http://ns.adobe.com/xap/1.0/&gt; .
@prefix xmpidq: &lt;http://ns.adobe.com/xmp/Identifier/qual/1.0/&gt; .
&lt;&gt;   xmp:Identifier  [
a rdf:Bag;
rdf:_1  [
xmpidq:Scheme "URI";
rdf:value "info:doi/10.1038/nrg2158" ];
rdf:_2  [
xmpidq:Scheme "URI";
rdf:value "doi:10.1038/nrg2158" ] ] .
</pre>

Now we’ve got both forms, which is fair enough since these are equivalent. In RDF terms we can make the statement that:

> <pre>doi:10.1038/nrg2158 owl:sameAs info:doi10.1038/nrg2158 .</pre>

which asserts that the two URIs are equivalent and that they reference the same resource.
  
So, what if we want to include a native DOI without the URI garb? We can easily do that:

<pre>@prefix rdf: &lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#&gt; .
@prefix xmp: &lt;http://ns.adobe.com/xap/1.0/&gt; .
@prefix xmpidq: &lt;http://ns.adobe.com/xmp/Identifier/qual/1.0/&gt; .
&lt;&gt;   xmp:Identifier  [
a rdf:Bag;
rdf:_1  [
xmpidq:Scheme "URI";
rdf:value "info:doi/10.1038/nrg2158" ];
rdf:_2  [
xmpidq:Scheme "URI";
rdf:value "doi:10.1038/nrg2158" ];
rdf:_3  [
xmpidq:Scheme "DOI";
rdf:value "10.1038/nrg2158" ] ] .
</pre>

OK, that takes care of the XMP direction to use <tt>xmp:Identifier</tt>, but, while deprecated by XMP, we note that back in the real world folks will be looking at the DC elements which is the schema with the greatest purchase. So, why not also add in a <tt>dc:identifier</tt> element such as would be used typically for DOI in citations. How about this:

<pre>@prefix dc: &lt;http://purl.org/dc/elements/1.1/&gt; .
@prefix rdf: &lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#&gt; .
@prefix xmp: &lt;http://ns.adobe.com/xap/1.0/&gt; .
@prefix xmpidq: &lt;http://ns.adobe.com/xmp/Identifier/qual/1.0/&gt; .
&lt;&gt;   xmp:Identifier  [
a rdf:Bag;
rdf:_1  [
xmpidq:Scheme "URI";
rdf:value "info:doi/10.1038/nrg2158" ];
rdf:_2  [
xmpidq:Scheme "URI";
rdf:value "doi:10.1038/nrg2158" ];
rdf:_3  [
xmpidq:Scheme "DOI";
rdf:value "10.1038/nrg2158" ] ];
dc:identifier "doi:10.1038/nrg2158" .
</pre>

Right, so we’ve taken care of the identfiers. But maybe there’s something missing? There’s no link to the DOI proxy. For widest applicability we should not assume prior knowledge of the DOI system. Perhaps we could include this link using the property <tt>dc:relation</tt>? Seems feasible though would really like to get some feedback on this. Any ideas?
  
So here, then, is a fairly full and complete expression of DOI within the XMP packet.

<pre>@prefix dc: &lt;http://purl.org/dc/elements/1.1/&gt; .
@prefix rdf: &lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#&gt; .
@prefix xmp: &lt;http://ns.adobe.com/xap/1.0/&gt; .
@prefix xmpidq: &lt;http://ns.adobe.com/xmp/Identifier/qual/1.0/&gt; .
&lt;&gt;   xmp:Identifier  [
a rdf:Bag;
rdf:_1  [
xmpidq:Scheme "URI";
rdf:value "info:doi/10.1038/nrg2158" ];
rdf:_2  [
xmpidq:Scheme "URI";
rdf:value "doi:10.1038/nrg2158" ];
rdf:_3  [
xmpidq:Scheme "DOI";
rdf:value "10.1038/nrg2158" ] ];
dc:identifier "doi:10.1038/nrg2158";
dc:relation "http://dx.doi.org/10.1038/nrg2158" .
</pre>

Ta-da!
  
(Of course, this is all premised on having freedom in writing out the XMP packet. If one is dependent on commercial applications to write out the packet then things may be different. Actually, they will be very different. They may not even be workable.)
  
Feedback would be very welcome.

 [1]: /blog/marking-up-doi
 [2]: /blog/metadata-in-pdf-2.-use-cases#comment-51907
 [3]: /blog/metadata-in-pdf-2.-use-cases