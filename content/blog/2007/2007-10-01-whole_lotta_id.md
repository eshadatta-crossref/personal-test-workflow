---
title: Whole Lotta ID
author: thammond
authors:
  - thammond
date: 2007-10-01

categories:
  - Identifiers
archives:
  - 2007

---
ISO has registered with the IANA a URN namespace identifier (&#8220;iso:&#8221;) for ISO persistent resources. From the Internet-Draft:

> _&#8220;This URN NID is intended for use for the identification of persistent resources published by the ISO standards body (including documents, document metadata, extracted resources such as standard schemata and standard value sets, and other resources).&#8221;_

The toplevel grammar rules (ABNF) give some indication of scope:

> <pre>NSS     = std-nss
std-nss = "std:" docidentifier *supplement *docelement [addition]</pre>

Just wanted to quote here one of the funkier examples cited in the document:

> <tt>urn:iso:std:iso:9999:-1:ed-1:v1-amd1.v1:en,fr:amd:2:v2:en:clause:3.1,a.2-b.9</tt>
  
> &nbsp;
  
> _&#8220;refers to (sub)clauses 3.1 and A.2 to B.9 in the corrected version of Amendment 2, in English, which amends the document comprising the 1st version of edition 1 of ISO 9999-1 incorporating the 1st version of Amendment 1, in English/French (bilingual document)&#8221;_

Wow! That’s some ID. That’s something else.
  
As far as DOI is concerned there is nothing obvious to be learned. It is interesting to see such a level of granularity supported though. And since all these documents issue from a central publisher they can be prescriptive about the identifier syntax. Something which cannot be mandated for the many Crossref publishers with their own commercial arrangements. Hence DOI is generally agnostic about suffix strings.
  
Seems to be a little confusion about the registration though. The NID was approved Jan. 15, ’07 by the IESG and the [IANA Registry of URN Namespaces][1] (last updated Aug. 22, ’07) lists the namespace &#8220;iso&#8221; with the provisional (unnumbered) RFC labelled &#8220;RFC-goodwin-iso-urn-01.txt&#8221; (being the -01 draft). However, the IETF I-D Tracker reports [this status][2] for draft-goodwin-iso-urn, which shows that a new I-D (an -02 draft) was submitted in Sept. 7, ’07:

> _&#8220;A Uniform Resource Name (URN) Namespace for the International Organization for Standardization (ISO), [draft-goodwin-iso-urn-02.txt][3]&#8220;_

 [1]: http://www.iana.org/assignments/urn-namespaces
 [2]: https://datatracker.ietf.org/idtracker/draft-goodwin-iso-urn/
 [3]: https://tools.ietf.org/html/draft-goodwin-iso-urn-02.html