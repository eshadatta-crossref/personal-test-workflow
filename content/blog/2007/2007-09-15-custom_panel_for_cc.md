---
title: Custom Panel for CC
author: thammond
authors:
  - thammond
date: 2007-09-15

categories:
  - Metadata
archives:
  - 2007

---
[Creative Commons][1] now have a custom panel for adding CC licenses using Adobe apps - see [here][2].
  
Interesting on two counts:

  * Machine readable licenses 
      * XMP metadata</ul> 
        But I still think that batch solutions for adding XMP metadata are really required for publishing workflows. And ideally there should be support for adding arbitrary XMP packets if we’re going to have truly rich metadata. I rather fear the constraints that custom panels place upon the publisher.

 [1]: http://creativecommons.org/
 [2]: http://creativecommons.org/weblog/entry/7648