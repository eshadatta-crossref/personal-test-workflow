---
title: Mars Bar
author: thammond
authors:
  - thammond
date: 2007-10-08

categories:
  - PDF
archives:
  - 2007

---
Just noticed that there is now (as of last month) a blog for [Mars][1] (&#8220;Mars: Comments on PDF, Acrobat, XML, and the Mars file format&#8221;). See this from the initial post:

> _&#8220;The [Mars Project][2] at [Adobe][3] is aimed at creating an XML representation for PDF documents. We use a component-based model for representing different aspects of the document and we use the Universal Container Format (a Zip-based packaging format) to hold the pieces. Mars uses XML to represent the individual components where that makes sense, but otherwise uses industry standard formats to represent other components. Examples of these include Fonts (we use OpenType), Images (PNG, GIF, JPEG, JPEG2000), Color (ICC Color Profiles), etc.. We use SVG to represent page content, which fits as both an XML format and an industry standard.&#8221;_

 [1]: http://blogs.adobe.com/
 [2]: https://web.archive.org/web/20071027131726/http://labs.adobe.com/technologies/mars/
 [3]: http://www.adobe.com/
