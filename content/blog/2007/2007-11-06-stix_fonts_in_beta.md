---
title: STIX Fonts in Beta
author: thammond
authors:
  - thammond
date: 2007-11-06

categories:
  - Standards
archives:
  - 2007

---
Well, Howard already [blogged on Nascent][1] last week about the [STIX fonts][2] (Scientific and Technical Information Exchange) being launched and now freely available in beta. And today the STM Association also have [blogged][3] this milestone mark. So, just for the record, I’m noting here on CrossTech those links for easy retrieval. As Howard says:

> _&#8220;I recommend all publishers download the fonts from the STIX web site at [www.stixfonts.org][2] today.&#8221;_

(And for those who want to see more of Howard, he can be found in interview [here][4] on the SIIA Executive FaceTime Webcast Series. 🙂

 [1]: https://web.archive.org/web/20070815000000*/http://blogs.nature.com/wp/nascent/2007/11/stix_fonts_go_beta.html
 [2]: http://www.stixfonts.org/
 [3]: https://web.archive.org/web/20080725054716/http://www.stm-assoc.org/home/stix-fonts-project-completes-design-phase.html
 [4]: http://www.scribemedia.org/2007/03/28/howard-ratner/
