---
title: 'What’s My Link?'
author: thammond
authors:
  - thammond
date: 2007-02-05

categories:
  - Linking
archives:
  - 2007

---
Simon Willison has a great piece [here][1] about disambiguating URLs. Best practice on creating and publishing URLs is obviously something of interest to any publisher. See this excerpt from Simon’s post:

_&#8220;Here’s a random example, plucked from today’s del.icio.us popular. convinceme.net is a new online debating site (tag clouds, gradient fills, rounded corners). It’s listed in del.icio.us a total of four times!

* https://web.archive.org/web/20070203050251/http://www.convinceme.net/ has 36 saves

* https://web.archive.org/web/20070202182238/http://www.convinceme.net/index.php has 148 saves

* https://web.archive.org/web/20070203050251/http://www.convinceme.net/ has 211 saves

* https://web.archive.org/web/20070202182238/http://www.convinceme.net/index.php has 38 saves

Combined that’s 433 saves; much more impressive, and more likely to end up at the top of a social sharing sites.&#8221;_

 [1]: https://web.archive.org/web/20070205131629/http://simonwillison.net/2007/Feb/4/urls/
