---
title: OAI-ORE Presentation at OAI5
author: thammond
authors:
  - thammond
date: 2007-05-02

categories:
  - Linking
archives:
  - 2007

---
<img alt="oai-ore-1.jpg" src="/wp/blog/images/oai-ore-1.jpg" width="308" height="241" />

I posted [here][1] about an initial meeting of the [OAI-ORE][2] Technical WG back in January. ORE is the &#8220;Object Reuse and Exchange&#8221; initiative which is aiming to provide a formalism for describing scholarly works as complete units (or packages) of information on the Web using resource maps which would be available from public access points. From a DOI perspective this work is intimately connected with multiple resolution. For further updates on this work, see [here][3] for a presentation by Herbert Van de Sompel on OAI-ORE at the OAI5 Workshop (5th Workshop on Innovations in Scholarly Communication) held a couple weeks back at CERN, Geneva, Switzerland.

The presentation gives an insight regarding the problem domain in which ORE operates, and in the evolving thinking regarding potential solutions. The presentation was recorded on video and is available for both streaming and download ([slides][4], [streaming video][5], [video download][6]).

Note that Michael Nelson of Old Dominion University also presented on behalf of the ORE effort at the recent [CNI Task Force Meeting][7] and at the [DLF Forum][8].

 [1]: /blog/digital-objects/
 [2]: http://www.openarchives.org/ore/
 [3]: https://indico.cern.ch/event/5710/contributions/1212289/attachments/988175/1405153/ore-oai5-hvds.pdf
 [4]: https://web.archive.org/web/20070709065314/http://indico.cern.ch/
 [5]: https://web.archive.org/web/20070709065314/http://indico.cern.ch/
 [6]: https://web.archive.org/web/20070709065314/http://indico.cern.ch/
 [7]: https://web.archive.org/web/20110106125135/http://www.cni.org/tfms/2007a.spring/abstracts/PB-update-lagoze.html
 [8]: https://web.archive.org/web/20070604163358/http://www.diglib.org/forums/spring2007/spring2007abstracts.htm
 [7]: https://web.archive.org/web/20070501150942/http://www.cni.org/tfms/2007a.spring/abstracts/PB-update-lagoze.html
 [8]: https://web.archive.org/web/20070604163358/http://www.diglib.org/forums/spring2007/spring2007abstracts.htm
