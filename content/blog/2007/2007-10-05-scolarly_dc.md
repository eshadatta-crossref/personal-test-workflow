---
title: Scholarly DC
author: thammond
authors:
  - thammond
date: 2007-10-05

categories:
  - Metadata
archives:
  - 2007

---
This [This][1] was just sent out to the [DC-GENERAL][2] mailing list about the new [DCMI Community for Scholarly Communications][3]. As Julie Allinson says:

> _&#8220;The aim of the group is to provide a central place for individuals and organisations to exchange information, knowledge and general discussion on issues relating to using Dublin Core for describing items of &#8216;scholarly communications’, be they research papers, conference presentations, images, data objects. With digital repositories of scholarly materials increasingly being established across the world, this group would like to offer a home for exploring the metadata issues faced.&#8221;_

There’s also a [DC-SCHOLAR][4] mailing list (subscribe [here][5]). Not too much there yet, but it may be useful to track - or even to participate. 🙂

 [1]: http://www.jiscmail.ac.uk/cgi-bin/webadmin?A2=ind0710&#038;L=dc-general&#038;T=0&#038;F=&#038;S=&#038;P=459
 [2]: http://www.jiscmail.ac.uk/archives/dc-general.html
 [3]: http://dublincore.org/groups/scholar/
 [4]: http://www.jiscmail.ac.uk/lists/DC-SCHOLAR.html
 [5]: http://www.jiscmail.ac.uk/cgi-bin/wa.exe?SUBED1=dc-scholar&#038;A=1