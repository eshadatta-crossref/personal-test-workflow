---
title: SearchULike
author: thammond
authors:
  - thammond
date: 2007-02-05

categories:
  - Search
archives:
  - 2007

---
Nelson Minar has a short [post][1] on Google’s [Search History][2] &#8216;feature’ and how it can be used to enhance your search experience. I guess that should be SearchULike.

 [1]: http://www.somebits.com/weblog/tech/bad/googleSearchHistory.html
 [2]: http://www.google.com/searchhistory