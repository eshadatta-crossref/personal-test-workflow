---
title: 'Chapter 9 - The Closed Book'
author: thammond
authors:
  - thammond
date: 2007-09-15

categories:
  - Discussion
archives:
  - 2007

---
Hadn’t really noticed before but was fairly gobsmacked by this notice I just saw on the DOI&reg; Handbook:

> **\*\*Please note that Chapter 9, Operating Procedures is for Registration Agency personnel only.\*\***
  
> DOI&reg; Handbook
  
> doi:10.1000/182
  
> <http://www.doi.org/hb.html> 

And, indeed, the Handbook’s TOC only reconfirms this:

> 9 Operating procedures*
  
> *The RA password is required for viewing Chapter 9.
  
> 9.1 Registering a DOI name with associated metadata
  
> 9.2 Prefix assignment
  
> 9.3 Transferring DOI names from one Registrant to another
  
> 9.4 Handle System&reg; policies and procedures
  
> 9.4.1 Overview
  
> 9.4.2 Policies and Procedures
  
> 9.4.3 Requirements for Administrators of Resolution Services
  
> 9.4.4 Protocols and Interfaces
  
> 9.5 DOI&reg; System error messages 

That’s spooky. A book with a hidden chapter. I **really** don’t like that at all. Especially on a book aiming to provide general information and guidance. Seems to be that if that information needs to be kept private to RA’s then it has no business rubbing shoulders with public information. I would suggest that the material be opened up or else moved out. Makes me feel so second class.