---
title: 'The Name’s The Thing'
author: thammond
authors:
  - thammond
date: 2007-09-20

categories:
  - XMP
archives:
  - 2007

---
I’m always curious about names and where they come from and what they mean. Hence, my interest was aroused with the constant references to &#8220;XAP&#8221; in XMP. As the [XMP Specification][1] (Sept. 2005) says:

> _&#8220;NOTE: The string “XAP” or “xap” appears in some namespaces, keywords, and related names in this document and in stored XMP data. It reflects an early internal code name for XMP; the names have been preserved for compatibility purposes.&#8221;_

Actually, it occurs in most of the core namespaces: XAP, rather than XMP.

(Continues.)

<!--more-->



An earlier XMP Specification from 2001 (v. 1.5 - and see [here][2] for an earlier post of mine about XMP’s missing version numbers, and [here][3] about Adobe’s lack of archiving for XMP specifications) says almost the same thing:

> _&#8220;NOTE: Many namespaces, keywords, and related names in this document are prefaced with the string “XAP”, which was an early internal code name for XMP metadata. Because the Acrobat 5.0 product shipped using those names and keywords, they were retained for compatibility purposes.&#8221;_

So, there’s no indication in either of these specifications as to what the original name signified.

But then I turned up [this issue][4] in the Adobe Developer Knowledgebase:

> _&#8220;Known Issue: The metadate framework name was changed from XAP to XMP

> &nbsp;

> Summary

> XAP (Extensible Authoring Publishing) was an early internal code name for XMP (Extensible Metadata Platform).

> &nbsp;

> Issue

> Why are many namespaces, keywords, data structures, and related names in the documents and XMP toolkit code prefaced with the string &#8220;XAP&#8221; rather than &#8220;XMP&#8221;?

> &nbsp;

> Solution

> XAP (Extensible Authoring and Publishing) was an early internal code name for XMP (Extensible Metadata Platform) metadata. Because Acrobat 5.0 used those names, they were retained for compatibility purposes. XMP is the formal name used the framework specification.&#8221;_

Aha! Now it’s all clear. And now I’m also wondering if this original name still reflects Adobe’s thinking on the purpose of XMP that it be primarily an authoring utility rather than a workflow utility. That is, is Adobe’s XMP more geared to individual authors of Adobe’s Creative Suite products entering in metadata by hand as part of the authoring act, rather than as a batch entry process within an automated publishing workflow? The emphasis that Adobe put on [Custom File Info panels][5] for their CS products would seem to foster the view that Adobe see XMP as an interactive authoring device for adding metadata. But what about the publishers and their workflows? The SDK is a rather poor effort at garnering any widespread support of XMP within the publishing industry.

 [1]: https://web.archive.org/web/20210811233806/https://www.adobe.com/devnet/xmp.html
 [2]: /blog/w5m0mpcehihzreszntczkc9d
 [3]: /blog/xmp-some-other-gripes/
 [4]: http://support.adobe.com/devsup/devsup.nsf/docs/51840.htm
 [5]: http://www.adobe.com/products/xmp/custompanel.html
