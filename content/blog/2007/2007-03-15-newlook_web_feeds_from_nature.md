---
title: New-Look Web Feeds from Nature
author: thammond
authors:
  - thammond
date: 2007-03-15

categories:
  - RSS
archives:
  - 2007

---
I just posted [this entry][1] on [Nascent][2], Nature’s Web Publishing blog, about Nature’s new look for web feeds which essentially boils down to our using the RSS 1.0 &#8216;mod_content’ module to add in a rich content description for human consumption to complement our long-standing commitment to machine-readable descriptions. We are thus able to deliver full citation details in our RSS feeds as XHTML in CDATA sections for humans and as DC/PRISM properties for machines, the whole encoded in our feed format of choice - RSS 1.0. Note also that we declared our intention to publish parallel feeds in Atom which again will carry both human- and machine-readable citations. Further details on the RSS 1.0/Atom paired feeds will be posted here in the near future.
  
Perhaps of special note we have added in the DOI in our descriptions in standard Crossref citation format and linked it to the DX resolver.

 [1]: https://web.archive.org/web/20070815000000*/http://blogs.nature.com/wp/nascent/2007/03/nature_web_feeds_a_new_look.html
 [2]: https://web.archive.org/web/20070815000000*/http://blogs.nature.com/wp/nascent/