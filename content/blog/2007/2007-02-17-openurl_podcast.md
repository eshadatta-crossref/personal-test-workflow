---
title: OpenURL Podcast
author: thammond
authors:
  - thammond
date: 2007-02-17

categories:
  - Linking
archives:
  - 2007

---
Jon Udell interviews [Dan Chudnov][1] about [OpenURL][2], see his [blog entry][3]: &#8220;A conversation with Dan Chudnov about OpenURL, context-sensitive linking, and digital archiving&#8221;. The podcast of the interview is available [here][4].

Interesting to see these kind of subjects beginning to be covered by a respected technology writer like Jon. As he says in his post:

> _&#8220;I have ventured into this confusing landscape because I think that the issues that libraries and academic publishers are wrestling with — persistent long-term storage, permanent URLs, reliable citation indexing and analysis — are ones that will matter to many businesses and individuals. As we project our corporate, professional, and personal identities onto the web, we’ll start to see that the long-term stability of those projections is valuable and worth paying for.&#8221;_

 [1]: https://web.archive.org/web/20070114212828/http://curtis.med.yale.edu/dchud/
 [2]: https://web.archive.org/web/20070206165948/http://www.niso.org/standards/standard_detail.cfm?std_id=783
 [3]: http://blog.jonudell.net/2007/02/16/a-conversation-with-dan-chudnov-about-openurl-context-sensitive-linking-and-digital-archiving/
 [4]: http://jonudell.net/podcast/ju_chudnov.mp3
