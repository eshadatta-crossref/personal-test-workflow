---
title: eprintweb.org
author: Ed Pentz
authors:
  - Ed Pentz
date: 2007-03-02

categories:
  - Web
  - Preprints
archives:
  - 2007

---
IOP has created an instance of the arXiv repository called eprintweb.org at <https://web.archive.org/web/20130803071935/http://eprintweb.org/S/>. What’s the difference from arXiv? From the eprinteweb.org site - &#8220;We have focused on your experience as a user, and have addressed issues of navigation, searching, personalization and presentation, in order to enhance that experience. We have also introduced reference linking across the entire content, and enhanced searching on all key fields, including institutional address.&#8221;

The site looks very good and it’s interesting to see a publisher developing a service directly engaging with a repository.

<!--more-->



Some interesting points to note: There are DOI links to published articles - ```http://www.eprintweb.org/S/article/astro-ph/0603001``` - which IOP gets from Crossref. References in the preprints are also linked - ```http://www.eprintweb.org/S/article/astro-ph/0603001/refs```

Crossref will soon be making available an author/title only query for repositories to use to find DOIs for published papers when the preprint doesn’t have the full citation. Many authors don’t go back to their preprints to update the reference to the published version but the new Crossref query will enable the repositories to do this automatically.
