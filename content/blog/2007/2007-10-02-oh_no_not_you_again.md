---
title: Oh No, Not You Again!
author: thammond
authors:
  - thammond
date: 2007-10-02

categories:
  - Identifiers
archives:
  - 2007

---
Oh dear. Yesterday’s post &#8220;Using ISO URNs&#8221; was way off the mark. I don’t know. I thought that walk after lunch had cleared my mind. But apparently not. I guess I was fixing on eyeballing the result in RDF/N3 rather than the logic to arrive at that result.
  
(Continues.)

<!--more-->


  
There are three namespace cases (and I was only wrong in two out of the three, I think):
  
1. &#8220;pdf:&#8221;
  
I was originally going to suggest the use of &#8220;data:&#8221; for the PDF information dictionary terms here but then lunged at using an HTTP URI (the URI of [the page][1] for the PDF Reference manual on the Adobe site) for regular orthodox conformancy and good churchgoing:

<pre>@prefix pdf: &lt;http://www.adobe.com/devnet/pdf/pdf_reference.html&gt; .
</pre>

This was wrong on two counts:
  
a) Afaik no such use for this URI as a namespace has ever been made by Adobe. And it is in the gift of the DNS tenant (elsewhere called &#8220;owner&#8221;) to mint URIs under that namespace and to ascribe meanings to those URIs.
  
b) Also the URI is not best suited to a role as namespace URI since RDF namespaces typically end in &#8220;/&#8221; or &#8220;#&#8221; to make the division between namespace and term clearer. (In XML it doesn’t make a blind bit of difference as XML namespaces are just a scoping mechanism.) So to have a property URI as

<pre>http://www.adobe.com/devnet/pdf/pdf_reference.htmlAuthor
</pre>

does the job but looks pretty rough and more importantly precludes (at least, complicates) the possibility of dereferencing the URI to return a page with human or machine readable semantics. Better in RDF terms is one of the following:

<pre>a) http://www.adobe.com/devnet/pdf/pdf_reference/Author
b) http://www.adobe.com/devnet/pdf/pdf_reference#Author
c) http://www.adobe.com/devnet/pdf/pdf_reference.html#Author
</pre>

In the absence of any published namespace from Adobe for these terms, I think it would have been more prudent to fall back on &#8220;data:&#8221; URIs. So

<pre>@prefix pdf: &lt;data:,&gt; .
</pre>

leading to

<pre>data:,Author
data:,CreationDate
data:,Creator
etc.
</pre>

This is correct (afaict) and merely provides a URI representation for bare strings.
  
Had we wanted to relate those terms to the PDF Reference we might have tried something like:

<pre>data:,PDF%20Reference:Author
data:,PDF%20Reference:CreationDate
data:,PDF%20Reference:Creator
etc.
</pre>

And if we had wanted to make those truly secondary RDF resources related to a primary RDF resource for the &#8220;namespace&#8221; we could have attempted something like:

<pre>data:,PDF%20Reference#Author
data:,PDF%20Reference#CreationDate
data:,PDF%20Reference#Creator
etc.
</pre>

Note though that the &#8220;data:&#8221; specification is not clear about the implications of using &#8220;#&#8221;. (Is it allowed, or isn;t it?) We must suspect that it is not allowed, but see [this mail][2] from Chris Lilley (W3C) which is most insightful.
  
2. &#8220;pdfx:&#8221;
  
The example was just for demo purposes, but (as per 1a above) it is incumbent on the namespace authority (here ISO) to publish a URI for the term to be used. Anyhow, the namespace URI I cited

<pre>@prefix pdfx: &lt;urn:iso:std:iso-iec:15930:-1:2001&gt; .
</pre>

would not have been correct and would have led to these mangled URIs:

<pre>urn:iso:std:iso-iec:15930:-1:2001GTS_PDFXVersion
urn:iso:std:iso-iec:15930:-1:2001GTS_PDFXConformance
</pre>

It should have been something closer to

<pre>@prefix pdfx: &lt;urn:iso:std:iso-iec:15930:-1:2001:&gt; .
</pre>

leading to

<pre>urn:iso:std:iso-iec:15930:-1:2001:GTS_PDFXVersion
urn:iso:std:iso-iec:15930:-1:2001:GTS_PDFXConformance
</pre>

3. &#8220;_usr:&#8221;
  
This was the one correct call in yesterday’s post.

<pre>@prefix _usr: &lt;data:,&gt; .
</pre>

The only problem here would be to differentiate these terms from the terms listed in the PDF Reference manual, although the PDF information dictionary makes no such distinction itself.
  
To sum up, perhaps the best way of rendering the PDF information dictionary keys in RDF would be to use &#8220;data:&#8221; URIs for all (i.e. a methodology for URI-ifying strings) and to bear in mind that at some point ISO might publish URNs for the PDF/X mandated keys: &#8216;<tt>GTS_PDFXVersion</tt>&#8216; and &#8216;<tt>GTS_PDFXConformance</tt>&#8216;. So,

<pre># document infodict (object 58: 476983):
@prefix: pdfx:  &lt;data:,&gt; .
@prefix: pdf:  &lt;data:,&gt; .
@prefix: _usr: &lt;data:,&gt; .
&lt;>   _usr:Apag_PDFX_Checkup "1.3";
pdf:Author "Scott B. Tully";
pdf:CreationDate "D:20020320135641Z";
pdf:Creator "Unknown";
pdfx:GTS_PDFXConformance "PDF/X-1a:2001";
pdfx:GTS_PDFXVersion "PDF/X-1:2001";
pdf:Keywords "PDF/X-1";
pdf:ModDate "D:20041014121049+10'00'";
pdf:Producer "Acrobat Distiller 4.05 for Macintosh";
pdf:Subject "A document from our PDF archive. ";
pdf:Title "Tully Talk November 2001";
pdf:Trapped "False" .
</pre>

 [1]: http://www.adobe.com/devnet/pdf/pdf_reference.html
 [2]: http://lists.w3.org/Archives/Public/www-style/2005May/0036.html