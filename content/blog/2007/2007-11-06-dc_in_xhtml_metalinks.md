---
title: DC in (X)HTML Meta/Links
author: thammond
authors:
  - thammond
date: 2007-11-06
categories:
  - Metadata
aliases: "/blog/dc-in-xhtml-meta/"

Archives:
  - 2007

---
This [message][1] posted out yesterday on the dc-general list (with following extract) may be of interest:

> _&#8220;Public Comment on encoding specifications for Dublin Core metadata in HTML and XHTML</p>
>
> 2007-11-05, Public Comment is being held from 5 November through 3 December 2007 on the DCMI Proposed Recommendation, &#8220;Expressing Dublin Core metadata using HTML/XHTML meta and link elements&#8221; [<<http://dublincore.org/documents/2007/11/05/dc-html/>>](http://dublincore.org/documents/2007/11/05/dc-html/) by Pete Johnston and Andy Powell. Interested members of the public are invited to post comments to the DC-ARCHITECTURE mailing list [<<http://www.jiscmail.ac.uk/lists/dc-architecture.html>>](http://www.jiscmail.ac.uk/lists/dc-architecture.html) , including &#8220;[DC-HTML Public Comment]&#8221; in the subject line. Depending on comments received, the specification may be finalized after the comment period as a DCMI Recommendation.&#8221;</i></blockquote>

 [1]: http://www.jiscmail.ac.uk/cgi-bin/webadmin?A2=ind0711&#038;L=dc-general&#038;T=0&#038;F=&#038;S=&#038;X=1DEA157B9F8232DF23&#038;Y=t.hammond%40nature.com&#038;P=969
