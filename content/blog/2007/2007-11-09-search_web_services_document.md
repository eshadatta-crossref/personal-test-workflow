---
title: Search Web Services Document
author: thammond
authors:
  - thammond
date: 2007-11-09

categories:
  - Search
archives:
  - 2007

---
The OASIS Search Web Services TC has just put out the following document for public review (Nov 7- Dec 7, 2007):

> _Search Web Services v1.0 Discussion Document</p> 
> 
>   * Editable Source: <http://docs.oasis-open.org/search-ws/v1.0/DiscussionDocument.doc> 
>       * PDF: <http://docs.oasis-open.org/search-ws/v1.0/DiscussionDocument.pdf> 
>           * HTML: <http://docs.oasis-open.org/search-ws/v1.0/DiscussionDocument.html> </ul> 
>             </i></blockquote> 
>             
>             From the OASIS announcement:
>             
>             > _&#8220;This document: &#8220;Search Web Services Version 1.0 - Discussion Document - 2 November 2007&#8221;, was prepared by the OASIS Search Web Services TC as a strawman proposal, for public review, intended to generate discussion and interest. It has no official status; it is not a Committee Draft. The specification is based on the SRU (Search Retrieve via URL) specification which can be found at <http://www.loc.gov/standards/sru/>. It is expected that this standard, when published, will deviate from SRU. How much it will deviate cannot be predicted at this time. The fact that the SRU spec is used as a starting point for development should not be cause for concern that this might be an effort to rubberstamp or fasttrack SRU. The committee hopes to preserve the useful features of SRU, eliminate those that are not considered useful, and add features that are not in SRU but are considered useful. &#8220;_