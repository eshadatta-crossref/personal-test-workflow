---
title: OpenDocument Adds RDF
author: thammond
authors:
  - thammond
date: 2007-10-14

categories:
  - Metadata
archives:
  - 2007

---
Bruce D’Arcus left a comment [here][1] in which he linked to post of his: &#8220;[OpenDocument’s New Metadata System][2]&#8220;. Not everybody reads comments so I’m repeating it here. His post is worth reading on two counts:

  1. He talks about the new metadata functionality for OpenDocument 1.2 which uses _generic_ RDF. As he says:

    > _&#8220;Unlike Microsoft’s custom schema support, we provide this through the standard model of RDF. What this means is that implementors can provide a generic metadata API in their applications, based on an open standard, most likely just using off-the-shelf code libraries.&#8221;_

    This is great. It means that description is left up to the user rather than being restricted by any vendor limitation. (Ideally we would like to see the same for XMP. But Adobe is unlikely to budge because of the legacy code base and documents. It’s a wonder that Adobe still wants XMP to breathe.)

      * He cites a wonderful passage from Rob Weir of IBM (something which I had been considering to blog but too late now) about the changing shape of documents. Can only say, go read [Bruce’s post][2] and then [Rob’s post][3]. But anyway a spoiler here:

        > _&#8220;The concept of a document as being a single storage of data that lives in a single place, entire, self-contained and complete is nearing an end. A document is a stream, a thread in space and time, connected to other documents, containing other documents, contained in other documents, in multiple layers of meaning and in multiple dimensions.&#8221;_</ol>

        I think the ODF initiative is fantastic and wish that Adobe could follow suit. However, I do still hold out something for XMP. After all, nobody else AFAICT is doing anything remotely similar for multimedia. Where’s the W3C and co. when you really need them? (Oh yeah, [faffing][4] about the new [Semantic Web logo][5]. 😉

 [1]: /blog/metadata-for-the-record/
 [2]: https://web.archive.org/web/20071117090331/http://netapps.muohio.edu/blogs/darcusb/darcusb/archives/2007/10/13/opendocuments-new-metadata-system
 [3]: http://www.robweir.com/blog/2007/10/odf-enters-semantic-web.html
 [4]: http://lists.w3.org/Archives/Public/semantic-web/2007Oct/0081.html
 [5]: http://www.w3.org/2007/10/sw-logos.html
