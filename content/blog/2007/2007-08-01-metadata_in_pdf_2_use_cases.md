---
title: 'Metadata in PDF: 2. Use Cases'
author: thammond
authors:
  - thammond
date: 2007-08-01

categories:
  - Metadata
archives:
  - 2007

---
Well, this is likely to be a fairly brief post as I’m not aware of many use cases of metadata in PDFs from scholarly publishers. Certainly, I can say for _Nature_ that we haven’t done much in this direction yet although are now beginning to look into this.
  
I’ll discuss a couple cases found in the wild but invite comment as to others’ practices. Let me start though with the CNRI handle plugin demo for Acrobat which I blogged [here][1].

<!--more-->


  
**_Handle Plugin_**
  
First off, the handle plugin PDF samples do include an embedded (test) DOI in both the document information dictionary

<pre>5 0 obj
&lt;&lt;
/CreationDate (D:20070614140125-04'00')
/Author (Simon)
/Creator (PScript5.dll Version 5.2.2)
/Producer (Acrobat Distiller 8.1.0 \(Windows\))
/ModDate (D:20070614140240-04'00')
<b>/HDL (10.5555/pdftest-crossref)</b>
/Title (Microsoft Word - crossref-rev.doc)
&gt;&gt;
endobj
</pre>

and in the (document) metadata stream

<pre>&lt;rdf:Description rdf:about="" xmlns:pdfx="http://ns.adobe.com/pdfx/1.3/"&gt;
<b>&lt;pdfx:HDL&gt;10.5555/pdftest-crossref&lt;/pdfx:HDL&gt;</b>
&lt;/rdf:Description&gt;
</pre>

Bar any fuller disclosure of metadata terms at large (and one of the demo cases makes use of DOI to retrieve metadata form Crossref) this is excellent. I would, however, quibble with the use of &#8220;HDL&#8221; as a foreign key for the information dictionary. I realize this is just a test but the term &#8220;HDL&#8221; (or &#8220;DOI&#8221;, for that’s what it really is) is somewhat specific and a more general term such as &#8220;Identifier&#8221; would probably have more mileage, e.g.

<pre>5 0 obj
&lt;&lt;
...
<b>/Identifier (doi:10.5555/pdftest-crossref)</b>
...
&gt;&gt;
endobj
</pre>

In the second example from the metadata dictionary I don’t think the term &#8220;HDL&#8221; from the PDF extension schema &#8220;pdfx&#8221; is very helpful. (Is that namespace actually defined anywhere?) From a descriptive metadata viewpoint a more usual schema such as DC would have wider coverage. So again the second example would be better rendered as

<pre>&lt;rdf:Description rdf:about="" xmlns:dc="http://purl.org/dc/elements/1.1/"&gt;
<b>&lt;dc:identifier&gt;doi:10.5555/pdftest-crossref&lt;/dc:identifier&gt;</b>
&lt;/rdf:Description&gt;
</pre>

or, alternately,

<pre>&lt;rdf:Description rdf:about="" xmlns:dc="http://purl.org/dc/elements/1.1/"&gt;
<b>&lt;dc:identifier&gt;info:hdl/10.5555/pdftest-crossref&lt;/dc:identifier&gt;</b>
&lt;/rdf:Description&gt;
</pre>

**_Elsevier_**
  
Well, we have Alexander Griekspoor’s [comment earlier][2] that Elsevier are including the DOI in their PDFs. I don’t know how consistently this is being done but I’ve checked a couple sample articles and it would seem that they have embedded the DOI (here from _Cancer Cell, doi:0.1016/j.ccr.2007.06.004_) in the title element which shows up in the information dictionary as

<pre>361 0 obj
&lt;&lt;
/Producer (Adobe LiveCycle PDFG 7.2)
/Creator (Elsevier)
/Author ()
/Keywords ()
<b>/Title (doi:10.1016/j.ccr.2007.06.004)</b>
/ModDate (D:20070630031637+05'30')
/Subject ()
/CreationDate (D:00000101000000Z)
&gt;&gt;
endobj
</pre>

and in the (document) metadata dictionary as

<pre>365 0 obj
&lt;&lt;
/Type /Metadata
/Subtype /XML
/Length 1526
&gt;&gt;
stream
&lt;?xpacket begin='' id='W5M0MpCehiHzreSzNTczkc9d' bytes='1526'?&gt;
&nbsp;
&lt;rdf:RDF xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#'
xmlns:iX='http://ns.adobe.com/iX/1.0/'&gt;
&nbsp;
&lt;rdf:Description about=''
xmlns='http://ns.adobe.com/pdf/1.3/'
xmlns:pdf='http://ns.adobe.com/pdf/1.3/'&gt;
&lt;pdf:Producer&gt;Adobe LiveCycle PDFG 7.2&lt;/pdf:Producer&gt;
&lt;pdf:ModDate&gt;2007-06-30T03:16:37+05:30&lt;/pdf:ModDate&gt;
<b>&lt;pdf:Title&gt;doi:10.1016/j.ccr.2007.06.004&lt;/pdf:Title&gt;</b>
&lt;pdf:Creator&gt;Elsevier&lt;/pdf:Creator&gt;
&lt;pdf:Author&gt;&lt;/pdf:Author&gt;
&lt;pdf:Keywords&gt;&lt;/pdf:Keywords&gt;
&lt;pdf:Subject&gt;&lt;/pdf:Subject&gt;
&lt;pdf:CreationDate&gt;0-01-01T00:00:00Z&lt;/pdf:CreationDate&gt;
&lt;/rdf:Description&gt;
&nbsp;
&lt;rdf:Description about=''
xmlns='http://ns.adobe.com/xap/1.0/'
xmlns:xap='http://ns.adobe.com/xap/1.0/'&gt;
&lt;xap:CreatorTool>Elsevier&lt;/xap:CreatorTool&gt;
&lt;xap:ModifyDate>2007-06-30T03:16:37+05:30&lt;/xap:ModifyDate&gt;
&lt;xap:Title&gt;
&lt;rdf:Alt&gt;
<b>&lt;rdf:li xml:lang='x-default'>doi:10.1016/j.ccr.2007.06.004&lt;/rdf:li&gt;</b>
&lt;/rdf:Alt&gt;
&lt;/xap:Title&gt;
&lt;xap:Author&gt;&lt;/xap:Author&gt;
&lt;xap:Description&gt;
&lt;rdf:Alt&gt;
&lt;rdf:li xml:lang='x-default'/&gt;
&lt;/rdf:Alt&gt;
&lt;/xap:Description&gt;
&lt;xap:CreateDate&gt;0-01-01T00:00:00Z&lt;/xap:CreateDate&gt;
&lt;xap:MetadataDate>2007-06-30T03:16:37+05:30&lt;/xap:MetadataDate&gt;
&lt;/rdf:Description&gt;
&nbsp;
&lt;rdf:Description about=''
xmlns='http://purl.org/dc/elements/1.1/'
xmlns:dc='http://purl.org/dc/elements/1.1/'&gt;
<b>&lt;dc:title>doi:10.1016/j.ccr.2007.06.004&lt;/dc:title&gt;</b>
&lt;dc:creator/&gt;
&lt;dc:description/&gt;
&lt;/rdf:Description&gt;
&nbsp;
&lt;/rdf:RDF>
&lt;?xpacket end='r'?&gt;
endstream
endobj
</pre>

Kudos anyway to Elsevier for emebedding this piece of information in their PDFs (if indeed it is a general practice). This has the merit of being picked up by Adobe apps and displayed in e.g. Reader. Also third party apps can pull this and use this to retrieve the metadata record from Crossref.
  
The only downside is that technically this seems to be a kludge to satisfy Adobe apps and is not the correct field for filing this information. I would have thought that some other information dictionary field (e.g. &#8220;Subject&#8221;) would be a better kludge, and then reserve the &#8220;Title&#8221; and &#8220;Author&#8221; fields for their proper purposes. The RDF/XML title fields would appear to be inherited from the &#8220;Title&#8221; field in the information dictionary. It’s a bit of a shame really because the DOI is embedded - it’s just in the wrong place(s). (OK, so that’s still way better, maybe, than not providing this information at all.)
  
Hopefully, with more examples to mull over and experiences to learn from we can arrive at a much better and more systematic way of including the DOI, and other key metadata fields, within a PDF so that this information can be gleaned easily and unambiguously by third party apps.

 [1]: /blog/handle-acrobat-reader-plugin/
 [2]: /blog/xmp-first-hacks/