---
title: 'What’s in a URI?'
author: thammond
authors:
  - thammond
date: 2007-01-08

categories:
  - Web
archives:
  - 2007

---
First off, a Happy New Year to all!

A [post of mine][1] to the [OpenURL list][2] may possibly be of interest. Following up the recent W3C TAG (Technical Architecture Group) Finding on &#8220;The Use of Metadata in URIs&#8221; I pointed out that the TAG do not seem to be aware of OpenURL: which is both a standard prescription for including metadata in URI strings and a US information standard to boot.

 [1]: https://web.archive.org/web/20130903193049/https://utils.its.caltech.edu/pipermail/openurl/2007-January/000376.html
 [2]: https://web.archive.org/web/20130903202546/https://utils.its.caltech.edu/mailman/listinfo/openurl
