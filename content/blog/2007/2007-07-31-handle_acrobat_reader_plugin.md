---
title: Handle Acrobat Reader Plugin
author: thammond
authors:
  - thammond
date: 2007-07-31

categories:
  - Metadata
archives:
  - 2007

---
Just announced on the [handle-info][1] list is a new plugin from CNRI for Acrobat Reader - see [here][2]. The announcement says:

> _&#8220;It is intended to demonstrate the utility of embedding a identifying
  
> handle in a PDF document.
  
> &nbsp;
  
> &#8230;
  
> &nbsp;
  
> A set of demonstration documents, each with an embedded identifying
  
> handle, is packaged with the plug-in to show potential uses. To make
  
> productive use of this technology, a given industry or community of
  
> users would have to agree on one or more specific applications and
  
> populate the relevant handle records accordingly.&#8221;_

Two immediate comments:

  * This is a Windows-only plugin (realized that right after hitting the download button and seeing the &#8216;.exe’ file) and also needs admin rights to install. (So I solved the first hurdle and am trying to clear the second hurdle. Lockdown is not an uncommon practice for enterprise or institutional computers.)  
    (**Update:** Actually, I think I got this wrong. I need admin privileges to install Adobe Acrobat 8. Still scuppered, though. Can’t even see the sample PDF files.) 
      * The plugin seems to be aimed at the user rather than at the user agent and thus is necessarily limited in scope, i.e. it needs a human driver. (Ideally content providers would embed metadata within media files using structured markup techniques which would be readily accessible to any downstream app which could leverage this data transparently to provide enhanced user services.) </ul> 
        Anyway, I’ll add something more when I can get it installed. I think this tool could be a useful addition to publishing toolkits but also that content providers could do much more for consumers by disclosing metadata for their digital assets in a neutral, structured form.

 [1]: http://www.handle.net/mailman/listinfo/handle-info
 [2]: http://www.handle.net/hs-tools/adobe/