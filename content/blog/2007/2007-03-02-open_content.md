---
title: Open Content
author: thammond
authors:
  - thammond
date: 2007-03-02

categories:
  - Publishing
archives:
  - 2007

---
In light of my [earlier post][1] on OTMI, the mail copied below from Sebastian Hammer at [Index Data][2] about open content may be of interest. They are looking to compile a listing of web sources of open content - see [this page][3] for further details.

(Via [XML4lib][4] and other lists.)

<!--more-->



_&#8220;Hi All,

(apologies for any cross-posting)

At Index Data, we have long felt that there were really interesting

sources of open content out there that was not being utilized as well as

it could be because it was hidden away in websites. We’re a software

company specializing in information retrieval applications, so

eventually we asked ourselves, &#8216;what could we all do with this stuff if

it were exposed using our favorite open standards’.

We thought it was worth finding out, so we have set up processes to

regularly retrieve indexes of major open content resources, and make

them available using SRU and Z39.50. We’ve started with the Open Content

Alliance and Project Gutenberg (two quite different approaches to

producing free eBooks), Wikipedia, the Open Directory Project, and

OAIster. More is on the way.

Connection information and more details are available at

https://web.archive.org/web/20070325152849/http://indexdata.com//opencontent/.

The kind of metadata you can get from these sources varies. The Open

Content Alliance captures MARC records along with the scanned books,

which makes for excellent metadata. Many of the others produce some

variation of DublinCore. Our service, through either Z39.50 or SRU/W,

exposes both MARC (or MARCXML) and DublinCore in XML for all sources.

We’ve created a new mailing list to help inform people of changes to the

services, new resources available, etc. Signup at

http://lists.indexdata.dk/cgi-bin/mailman/listinfo/oclist/ .

We sincerely hope you will find these resources exciting and useful.

Feel free to get in touch if you have questions or input.

-Sebastian

&#8212;

Sebastian Hammer, Index Data

quinn@indexdata.com www.indexdata.com

Ph: (603) 209-6853 Fax: (866) 383-4485&#8221;_


 [1]: /blog/otmi-an-update/
 [2]: https://web.archive.org/web/20061205050055/http://indexdata.com//
 [3]: https://www.indexdata.com/resources/open-content/
 [4]: https://web.archive.org/web/20070430002213/http://lists.webjunction.org/mailman/listinfo/xml4lib
