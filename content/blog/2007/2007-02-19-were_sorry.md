---
title: '&#8220;We’re sorry&#8230;&#8221;'
author: thammond
authors:
  - thammond
date: 2007-02-19

categories:
  - Search
archives:
  - 2007

---
**Update:** All apologies to Google. Apparently this was a problem at our end which our IT folks are currently investigating. (And I thought it was just me. 🙂
  
Just managed to get this page:
  
_&#8220;Google Error
  
We’re sorry&#8230;
  
&#8230; but your query looks similar to automated requests from a computer virus or spyware application. To protect our users, we can’t process your request right now.
  
We’ll restore your access as quickly as possible, so try again soon. In the meantime, if you suspect that your computer or network has been infected, you might want to run a virus checker or spyware remover to make sure that your systems are free of viruses and other spurious software.
  
We apologize for the inconvenience, and hope we’ll see you again on Google.
  
To continue searching, please type the characters you see below:&#8221;_
  
And my search request?
  
<tt>ark</tt>
  
(Actual query is [here][1] as argument to the <tt>continue</tt> parameter.)
  
Was hoping to find results related to the [The ARK Persistent Identifier Scheme][2]. Maybe I missed something but I’m not impressed.

 [1]: http://www.google.com/sorry/?continue=http://www.google.com/search%3Fclient%3Dfirefox-a%26rls%3Dorg.mozilla%253Aen-US%253Aofficial%26channel%3Ds%26hl%3Den%26q%3Dark%26btnG%3DGoogle%2BSearch
 [2]: http://www.ietf.org/internet-drafts/draft-kunze-ark-12.txt