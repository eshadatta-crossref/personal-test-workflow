---
title: IBM Article on PRISM
author: thammond
authors:
  - thammond
date: 2007-07-10

categories:
  - Metadata
archives:
  - 2007

---
Nice entry article on PRISM [here][1] by Uche Ogbuji, Fourthought Inc. on IBM’s DeveloperWorks.

 [1]: http://www.ibm.com/developerworks/xml/library/x-think13.html