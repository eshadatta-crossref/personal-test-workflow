---
title: URI Template Republished
author: thammond
authors:
  - thammond
date: 2007-07-28

categories:
  - Identifiers
archives:
  - 2007

---
Well, it all went very quiet for a while but glad to see that the [URI Template Internet-Draft][1] has just been republished:

> _&#8220;A New Internet-Draft is available from the on-line Internet-Drafts

> directories.

> Title : URI Template

> Author(s) : J. Gregorio, et al.

> Filename : draft-gregorio-uritemplate-01.txt

> Pages : 9

> Date : 2007-7-23

> URI Templates are strings that can be transformed into URIs after

> embedded variables are substituted. This document defines the

> syntax and processing of URI Templates.

> A URL for this Internet-Draft is:

> <https://github.com/jcgregorio/uri-templates/blob/master/draft-gregorio-uritemplate-01.txt>&#8221;

>_

URI templates should be a very useful publishing tool. Templates are already used by technologies such as OpenSearch - see [here][2].

 [1]: https://github.com/jcgregorio/uri-templates/blob/master/draft-gregorio-uritemplate-01.txt
 [2]: https://www.opensearch.org/
