---
title: Last Orders Please!
author: thammond
authors:
  - thammond
date: 2007-09-13

categories:
  - Metadata
archives:
  - 2007

---
Public comment period on the [PRISM 2.0][1] draft ends Saturday (Sept. 15) ahead of next week’s WG meeting to review feedback and finalize the spec.

(I put in some comments about XMP already. Hope they got that.)

 [1]: https://web.archive.org/web/20070929195327/http://www.prismstandard.org/
