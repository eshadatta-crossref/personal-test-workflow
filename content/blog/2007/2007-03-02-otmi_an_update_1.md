---
title: 'OTMI - An Update'
author: thammond
slug: "otmi-an-update"
authors:
  - thammond
date: 2007-03-02

categories:
  - OTMI
  - Text And Data Mining
archives:
  - 2007

---
We’ve just posted an [update about OTMI][1] (the Open Text Mining Interface) on our Web Publishing blog [Nascent][2]. This post details the following changes:

  * Contact email - <otmi@nature.com>
      * Wiki - <http://opentextmining.org/>
          * Repository - <https://web.archive.org/web/20090706181310/http://www.nature.com/otmi/journals.opml> </ul>
            The OTMI content repository currently provides two years’ worth of full text across five of our titles:

              * [_Nature_][3]
                  * [_Nature Genetics_][3]
                      * [_Nature Reviews Drug Discovery_][3]
                          * [_Nature Structural &#038; Molecular Biology_][3]
                              * [_The Pharmacogenomics Journal_][3] </ul>
                                See the [wiki][4] for draft technical specs and for a sample script to generate the OTMI files. And feel free to add to the wiki on existing pages or create new pages as required.

                                We’re very much looking forward to any feedback you may have on what we consider to be a very exciting new initiative for scholarly publishers.

 [1]: https://web.archive.org/web/20070815000000*/http://blogs.nature.com/wp/nascent/2007/02/open_text_mining_interface_upd.html
 [2]: https://web.archive.org/web/20070815000000*/http://blogs.nature.com/wp/nascent/
 [3]: http://www.nature.com/nature
 [4]: http://opentextmining.org/
