---
title: Digital Objects
author: thammond
authors:
  - thammond
date: 2007-01-30

categories:
  - DOIs
archives:
  - 2007

---
A couple weeks back there was a meeting of the [Open Archive Initiative][1]&#8216;s Object Reuse and Exchange ([OAI-ORE][2]) Technical Committee hosted in the Butler Library at Columbia University, New York.

<img alt="DSC00027.JPG" src="" width="204" height="153" />

Lorcan Dempsey of OCLC blogs [here][3] on the [report][4] (PDF format) that was generated from that meeting. As does Pete Johnston of Eduserv [here][5].

<!--more-->



**Background:**

[OAI-ORE][2] is being positioned as a companion activity to the more familiar [OAI-PMH][6] protocol for metadata harvesting. OAI-ORE relates to the expression and exchange of digital objects across repositories rather than just the exchange of metadata about those objects.

The basic problem is that scholarly communication deals in units which are compound resulting from a complex of documents and/or datasets expressed in multiple formats, versions, relationships, etc. The underlying web architecture provides a fairly simple model of resources (identified with URIs) which are interconnected and can be interacted with by retrieving representations of those resources. In practice, this usually results in unique URIs (and thus resources) for each representation - think of one URI for an HTML document, another for a PDF document of the same work, and yet new URIs for those same document formats for a new version of the work. Clearly, all these representations (or documents) are related, and more importantly relate to a single underlying &#8220;work&#8221;. Web architecture as generally practiced does not provide ready mechanisms to aggregate (and compartmentalize) related documents and datasets.

My fairly simple mental picture is that the web landscape is rather like the early universe in which energy (and matter) is distributed uniformly and there is little local &#8220;intelligence&#8221; which is gradually built up through time by matter formation and aggregations of this matter leading to the more familiar &#8220;clumpy&#8221; universe with its recognizable galaxies, stars and other objects. This &#8220;clumpiness&#8221; is precisely what we are missing in the scholarly web.

 [1]: http://www.openarchives.org/
 [2]: http://www.openarchives.org/ore/
 [3]: http://orweblog.oclc.org/archives/001254.html
 [4]: http://www.openarchives.org/ore/documents/OAI-ORE-TC-Meeting-200701.pdf
 [5]: http://efoundations.typepad.com/efoundations/2007/01/more_ore.html
 [6]: http://www.openarchives.org/pmh/
