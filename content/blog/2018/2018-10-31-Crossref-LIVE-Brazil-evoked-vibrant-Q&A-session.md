---
title: 'Crossref LIVE Brazil evoked vibrant Q&A session'
author: Susan Collins
draft: false
authors:
  - Susan Collins
date: 2018-10-31
categories:
  - Crossref LIVE
  - Community
  - Collaboration
archives:
  - 2018

---

There has been a steady increase in the growth of our membership in Latin America—and in Brazil in particular—over the past few years. We currently have more than 800 Brazil-based members; some as individual members, but most are sponsored by another organization. As part of our [LIVE Local program](/events/) Chuck Koscher and I traveled to meet some of these members in Goiânia and Fortaleza, where we co-hosted events with Associação Brasileira de Editores Científicos do Brasil (ABEC Brasil)—one of our largest [Sponsors](/community/sponsors/).

These events always provide a great opportunity for us to update our members on new and upcoming Crossref developments. They are also an important way for us to discover more about the varied needs of our members’ communities and learn how we can work together better.

The LIVE Brazil events were attended by more than two hundred members and were held at the Universidade Federal de Goiás and the Universidade de Fortaleza respectively. Chuck and I enthusiastically demonstrated two new tools from Crossref— [Participation Reports](/participation/) and [Metadata Manager](https://www.crossref.org/metadatamanager/), we discussed our newest content types—preprints and peer review reports, and continually highlighted the importance (and the uses) of quality metadata.

We were joined by some fantastic guest speakers; Milton Shintaku from ABEC explained how to register content using the Crossref/OJS deposit plugin and Crossref ambassador, Edilson Damasio, spoke about Similarity Check and gave a demonstration of how to use the iThenticate interface when checking papers for originality.

The vibrant Q&A sessions reflected the varying needs of the audience. We talked generally about the different Crossref services and went more in-depth with discussions around submitting [relationship](https://support.crossref.org/hc/en-us/articles/214357426) metadata for peer review and preprints. [Crossmark](/services/crossmark/) and its implementation was also a hot topic, as was how to benefit from [Similarity Check](/services/similarity-check/)—and in particular how to address cases of duplication in submitted manuscripts, and the setting up of plagiarism policies for each journal. There was also a lot of discussion around OJS integrations, and we were able to share that PKP/OJS is currently in the process of enhancing the Crossref/OJS integration, including the ability for publishers to deposit references.


We were also pleased to see so much interest in supplementing Crossref metadata with references, Similarity Check URLs, license information, etc. To address this we’re running a webinar in Brazilian Portuguese entitled: “Registering content and adding to your Crossref metadata in Portuguese” on 26th November. You can sign up [here](https://outreach.crossref.org/acton/fs/blocks/showLandingPage/a/16781/p/p-0051/t/page/fm/0) if you’d like to attend.

I’d like to thank Universidade Federal de Goiás and the Universidade de Fortaleza for hosting the events, providing the venues and the translation team, and of course, thanks to everyone who came!

A special mention of ABEC for their help in organizing and promoting the events. As a [Sponsor](/community/sponsors/), they relieve our team of an intense amount of technical support, billing, and other administrative burdens, saving us time and expense, while offering a localized service to Brazilian publishers.

{{% imagewrap %}} <img src="/images/blog/LIVE-Brazil-ABEC.png" alt=“Brazil LIVE Goiânia" height="150px" width="400px" class="img-responsive" /> {{% /imagewrap %}}


Crossref staff with co-hosts ABEC and representatives from UFG who helped with the event - thank you!

---
