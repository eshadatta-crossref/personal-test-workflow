---
title: SSP roadtrip for the Crossref team
author: Amanda Bartell
draft: false
authors:
  - Amanda Bartell
date: 2018-05-30
categories:
  - Service Providers
  - Meetings
  - Participation Reports
  - Education
  - Member Experience
archives:
  - 2018
---

What do you think of when you think of Chicago? Deep dish pizza? Art Deco architecture?

Well for one week only this year you can add scholarly publishing to the list as the #SSP2018 Conference comes to town. Some Crossref people are excited to be heading out for the conference, and we're looking forward to meeting as many of our members as possible.

Come along to **stand 212A** and talk to [Anna Tolwinska](/people/anna-tolwinska/) about Participation Reports. Although this new tool is still in beta, she's giving SSP attendees a sneak peek and the chance to get an early look at whether they (and over 10 000 other members) are registering the ten key elements that add context and richness to the basic required metadata. You'll get real insight into what metadata you're registering, even if this work is done by a third party or other department.

Thinking about registering preprints or including data citations? Want to find out more about our forthcoming Event Data service? Our product director [Jennifer Lin](/people/jennifer-lin/) will be able to give you the ins and outs of all our latest services so do keep an eye out for her at the conference.

Speaking of third parties, [I'll](/people/amanda-bartell/) will be popping along to the "Thinking the Unthinkable, or How to Prepare for a Platform Migration" pre meeting seminar on Wednesday with copies of our new [Platform Migration Checklist](/education/member-setup/working-with-a-service-provider/checklist-for-platform-migration/) and lots of hints and tips to help form a new platform migration guide which will help members have a smooth transition when thinking of moving providers.

[Shayn Smulyan](/people/shayn-smulyan/) will be attending the ORCID breakfast meeting on Thursday morning, so come and say hello if you have any questions about how ORCID and Crossref work together. Shayn is one of our support specialists, so he'll be able to help you with any other technical queries you may have.

Our tech director [Chuck Koscher](/people/chuck-koscher/) will be keen to hone in on members' advanced questions about Content Registration, citation matching, and any and all schema deets. So seek him out if you have deep technical questions.

Want to find out more about [Metadata 2020](http://www.metadata2020.org/), the new campaign to improve metadata for research? [Rosa Clark](/people/rosa-clark/) will be able to give you the lowdown, and even better - she has stickers!

And don't feel left out if you aren't a member but work closely with Crossref. [Jennifer Kemp](/people/jennifer-kemp/) will be on hand to answer all your metadata use and reuse questions, she'll be looking forward to chatting with all kinds of service providers, platforms, and tools.

We're looking forward to seeing you there!
