---
title: Hear this, real insight into the inner workings of Crossref
author: Joe Wass
draft: false
authors:
  - Joe Wass
  - Ginny Hendricks
date: 2018-04-01
categories:
  - Member Briefing
  - Event Data
  - Launch
archives:
  - 2018
---

## You want to hear more from us. We hear you.

We’ve spent the past year building Crossref Event Data, and hope to launch very soon. Building a new piece of infrastructure from scratch has been an exciting project, and we’ve taken the opportunity to incorporate as much feedback from the community as possible. We’d like to take a moment to share some of the suggestions we had, and how we’ve acted on them.

We asked a focus group “**What one thing would you change?**”. In hindsight, we could have done a better job with the question. We did get some enlightening answers but---for legal and practical reasons---we are unable to end either world hunger or global conflict, or do any of the other things we were invited to do. So we went back to our focus group and asked “What one thing would you change _about Crossref_?”.

The answers were illuminating. Some of you wanted mundane things like more data dumps. A disappointing number of people wanted us to put the capital ‘R’ back in our name. But two things we heard consistently, loud and clear, were:

1. “I want to hear more from Crossref”
2. “I want to know more about what’s going on inside Crossref”

One respondent said:

> I like the newsletters, and the Twitter visuals are nice enough, but I want to hear, you know, _more_ from them.

Another:

> Crossref is your typical quiet DOI Registration Agency. They make a big thing about being the background infrastructure you don’t notice. But infrastructure doesn’t have to be quiet. I live next to the M25, and I can tell you, that’s the sound of success. I mean, it’s loud.

One final quote which clinched it for us:

> The outreach team is doing a great job with their multilingual videos. But you can never cover every world language. In today’s connected world, you should be thinking about the _universal language_.

She clarified:

> No, I don’t mean XML.

We took this advice to heart. When we were building Crossref Event Data, we baked these features right in. Now you can hear what’s going on inside Crossref, any time, day or night.

## Introducing the Crossref Thing Action Service!

Turn up your speakers (about half-way, it would be foolhardy to turn them too high) and visit:

### [live.eventdata.crossref.org/thing-action-service.html](https://live.eventdata.crossref.org/thing-action-service.html)

It’s optimized for Google Chrome, but we’ve tested it in Firefox and Safari.

The **Thing Action Service** shows you, in excruciating sonorous detail, every single action that happens inside the Crossref Event Data system. Every time we receive live data from Twitter or Wikipedia. Every time we check a DOI. Every time we check an RSS feed. Every time we find a link to our Registered Content on the web.

In a pioneering move within the scholarly publishing space, you can hear the data as it’s being processed, live. Furthermore, we think we are the first DOI Registration Agency to offer our services in stereo.

John Chodacki, Professional Working Group Chair, said:

> We welcome this innovation. From my experience Chairing, well, everything, I’m certain that hearing-impaired users will like it especially.

So sit back, put the Thing Action Service on the speakers, and relax. You may find it difficult at first, but as you let the sound waves wash over you, think of all that data in flight. That beep could be someone criticizing the article you wrote on Twitter. But don’t worry, the next one might be someone defending it.

Think of it as _musique concrète_. That’s the Art of Persistence.
