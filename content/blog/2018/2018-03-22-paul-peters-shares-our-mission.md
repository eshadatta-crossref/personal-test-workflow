---
  title: New Board Chair Paul Peters shares our mission
  author: Ginny Hendricks
  draft: false
  authors:
    - Paul Peters
    - Ginny Hendricks
  date: 2018-03-22
  categories:
    - Board
    - Election
    - Crossref LIVE
    - Strategy
  archives:
    - 2018
---

At the end of last year, Paul Peters---CEO of our member _Hindawi_---became the new Chair of the Crossref Board. The announcement was made in Singapore at our first LIVE Annual ever held in Asia. I caught up with Paul back in London, UK, where he answered a few questions about what he hopes to bring to the Board, and to the Crossref community as a whole.

<!--more-->

### 1. Congratulations, Paul. How delighted were you to be voted in by your fellow board members, old and new?

That’s a rather leading question ;-)

Seriously though, I am incredibly honored to have been chosen to lead Crossref’s board at such an important point in the organization’s development. The current composition of the board is as diverse as it has ever been, which is essential if the board is to represent Crossref’s global membership, as well as the wide range of business and publication models that our members use. This diversity on the board will help to support Crossref’s aim of encouraging innovation in scholarly communication by providing open infrastructure that benefits all researchers.

### 2. You’ve been on our board for nine years. How has it changed in that time and what should the board be most proud of?

When I first joined the board, Crossref was at the stage where you had successfully established persistent reference linking as a standard practice among scholarly journal publishers. And, although this was the original purpose of Crossref, it was by no means an easy task, as it required a diverse group of competing publishers to work together in building shared infrastructure for the common good.

In the nine years since then, I’ve seen Crossref continue to build on this core foundation of technological expertise, the trust and goodwill of its membership, and the diverse skills of its small staff. The result has been the development of important new services (such as Similarity Check) that have become an essential component of the scholarly communications system, support new content types (including both preprints and peer review reports) that are becoming increasingly important in the move towards an Open Science future, and the expansion of Crossref’s membership to include almost 10,000  members of all shapes and sizes from 114 countries around the world.

With regard to the board itself, I have been pleased to see Crossref undergo important changes that have provided greater transparency in the organization's governance, as well as more active participation from its members. Last year Crossref put out an open call to invite members to put themselves forward for consideration on the board. As a result of holding its first contested election, Crossref saw a dramatic increase in the engagement of members in the election process. Not only is this important for ensuring that the board is truly representative of the diverse membership, but it will also help to actively engage a larger pool of members in the important work that lies ahead.

### 3. What do you see as Crossref’s strengths and role?

I believe that Crossref’s past and future success relies on two key strengths. The first is its ability to bring together a large and disparate community of organizations and individuals to create tools and services that no single organization could develop alone. People sometimes overlook how successful Crossref has been in building the trust and support of a diverse group of stakeholders, however I believe this has been an essential ingredient in the organization’s success and will be essential as Crossref develops new tools and services in the years to come.

Crossref’s other core strength has been the expertise, passion, and ambitious vision of its staff, many of whom I have had the pleasure of knowing since my first days on the board. The ability to develop and maintain real-time infrastructure serving millions of end-users, while simultaneously developing new products and services, requires an incredible range of skills from technology and product development, to marketing, community outreach, and customer support. Moreover, as a growing non-profit organization with thousands of members around the world, and an international staff working across national boundaries, Crossref’s legal, financial, and administrative support team have also been an essential ingredient in the organization’s success.

### 4. We’ve grown beyond just the publisher constituency to libraries, scholars, and platforms and tools, which constituencies do you see us involving next?

Over time I believe that Crossref’s constituency will grow to cover all organizations that contribute to the creation and dissemination of scholarly research, although I recognize this may take several years to achieve.

In the short-term, I believe that research funders are the most important stakeholder group for Crossref to focus on, for the following reasons:

- First, with the development of the open Funder Registry and the addition of structured funding data to the Crossref registry, Crossref has already become an important provider of open infrastructure for research funders.
- Second, as the result of several key initiatives within the Open Science movement I believe that research funders will play an increasingly important role in determining how scholarly research outputs are created, shared, evaluated, and re-used. Therefore, the active involvement of research funders in Crossref’s membership and governance is essential.
- Finally, I believe that there is an important opportunity for Crossref to enable a range of new services across the research lifecycle by providing persistent identifiers and structured metadata research grants. Given how critical grants are within the research process, I’m amazed by the lack of infrastructure to monitor, evaluate, and build upon grants as first-class research objects. In many cases there is minimal, if any, public information about the grants that have been awarded by a particular funder. Even in cases where such data is available, it is rarely structured in a way that enables it to be searched or analyzed across multiple funding agencies.

In the absence of a community-driven, non-profit organization like Crossref to provide this infrastructure on an open basis, there is a risk that funders will be forced to rely on proprietary alternatives that limit how this information is used and by whom. Fortunately there are already efforts underway within Crossref to develop both the tools and the community of funders that will be required to create persistent identifiers and structured metadata for grants and other forms of research funding.

### 5. What are the biggest challenges facing Crossref?

I believe that Crossref’s greatest challenge will be to continue to bring together a diverse group of stakeholders, some of whom are regularly at odds with each other, in order to collaborate in developing tools and services for the benefit of the research community.

As challenging as it has been for Crossref to bring together competing publishers to build the shared services that we have all come to depend on, I believe that keeping the community focused towards a common goal will become even more challenging as that community expands to include funders, universities, and the many other organizations involved in the scholarly communications ecosystem. However, I think that Ed and his team have as good of a chance of succeeding as anyone could hope for, which is why I am so excited about Crossref’s future in the years ahead.

### 6. How will things change with you as Chair? You’ll be busier I guess. But enough about you already, what can we expect as staff and Board?

As my first order of business I’ll be getting rid of Crossref’s corporate jet, lavish office spaces, and executive chef. `</sarcasm>`.

On a more serious note, my hope is that as Chair I will be able to work with the other members of the board in supporting Crossref’s staff as they work to achieve the ambitious goals we have set out during the past year. I believe that Crossref’s board members and staff are aligned in the desire to significantly expand the range of services Crossref provides, as well as the communities it serves.

The board still has an important role to play in shaping the organization’s strategic vision, while giving staff ample space to execute on this vision. Said another way, I hope to enable some lively strategic conversations among the board while making sure that we don’t get in the way of Ed and his team once it’s time to put ideas into action.

On a more personal note, I hope to be a good sounding board for Ed on any issues that he faces, either internally or externally, on the road ahead. Given my own experience in leading a growing organization through a period of significant change, I know how important it can be to have someone to talk to when difficult challenges arise, which they inevitably will. I hope that I can be a good advisor---and also a good friend---to Ed as he leads Crossref into the exciting future that lies ahead.

### Ginny: Thanks, Paul. I know Ed will miss his personal chef... but we look forward to working with you too!
