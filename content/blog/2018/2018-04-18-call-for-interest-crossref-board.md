---
title: Do you want to be on our Board?
author: Lisa Hart Martin
draft: false
authors:
  - Lisa Hart Martin
date: 2018-04-18
categories:
  - Board
  - Member Briefing
  - Governance
  - Election
archives:
  - 2018
---

> Do you want to effect change for the scholarly community?

The Crossref Nominating Committee is inviting expressions of interest to serve on the Board as it begins its consideration of a slate for the November 2018 election.

 <!--more-->

 The key responsibilities of the Board are:

 1. Setting the strategic direction for the organization;
 2. Providing financial oversight; and
 3. Approving new policies and services.

### Some of the decisions the board has made in recent years include:

 * Introduction of the Metadata APIs Plus service (to provide a paid-for premium service for machine access to metadata);
 * Updating the policy on open references (to increase links so that more readers can access content);
 * Establishing [the OI Project](/blog/the-oi-project-gets-underway-planning-an-open-organization-identifier-registry/) (to create a persistent Organization Identifier);
 * Inclusion of [preprints in the Crossref metadata](/news/2016-11-02-crossref-now-accepts-preprints/); and
 * Approval to develop [Event Data](/services/event-data/) (which will track online activity from multiple sources).

### What is expected of a Crossref Board member?

Board members should be able to attend all board meetings, which occur three times a year in different parts of the world. If you are unable to attend in person you must be able to attend via telephone.

Board members must:

* be familiar with the three key responsibilities listed above,
* actively participate and contribute towards discussions, and
* read the board documents and materials provided, prior to attending meetings.

### How to submit an expression of interest to serve on the Board

We are seeking people who know about scholarly communications and would like to be part of our future. If you have a vision for the international Crossref community, we are interested in hearing from you.

If you are a Crossref member, are [eligible to vote](/blog/are-you-having-an-identity-crisis/), and would like to be considered, you should complete and submit the [expression of interest](https://docs.google.com/forms/d/1AaPqLz4jBUeZ-VggkvRHBSYfwadrwfT2FP6YGcbyb48/edit) form with both your organization's statement and your personal statement before 18 May 2018.

It is important to note it is your organization who is the Crossref member—and therefore the seat will belong to your organization.

### About the election and our Board

We have a principle of  [“one member, one vote”](/truths/); our board comprises a cross-section of members and it doesn’t matter how big or small you are, every member gets a single vote. Board terms are three years, and one third of the Board is eligible for election every year. There are five seats up for election in 2018.

The board meets in a variety of international locations in March, July, and November each year. [View a list of the current Crossref Board members and a history of the decisions they’ve made (motions).](/board-and-governance/)

The election opens online in September 2018 and voting is done by proxy online, or in person, at the annual business meeting during ‘Crossref LIVE18’ on 13th November 2018 in Toronto, Canada. Election materials and instructions for voting will be available to all Crossref members online in September 2018.

### The role of the Nominating Committee

The Nominating Committee meets to discuss change, process, criteria, and potential candidates, ensuring a fair representation of membership. The Nominating Committee is charged with selecting a slate of candidates for election from those who have expressed an interest.

The selection of the slate (which is likely to exceed the number of open seats) is based on the quality of the expressions of interest and maintaining the balance and diversity of the board—especially in areas of organizational size, gender, geography and sector.

The Committee is made up of three board members not up for election, and two non-board members.  The current Nominating Committee members are:

* Mark Patterson, eLife (Chair);
* Chris Shillum, Elsevier;
* Amy Brand, MIT Press;
* Vincent Cassidy, The Institution of Engineering & Technology (IET); and
* Claire Moulton, The Company of Biologists.

Our board needs to be stay truly representative of Crossref’s global and diverse membership of organizations who publish. Please [submit your statements of interest](https://docs.google.com/forms/d/1AaPqLz4jBUeZ-VggkvRHBSYfwadrwfT2FP6YGcbyb48/edit) or reply to me with any questions to me at lhart@crossref.org.
