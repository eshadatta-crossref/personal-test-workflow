---
title: 'LIVE18: Roaring attendees, incomplete zebras, and missing tablecloths'
author: Rosa Clark
draft: false
authors:
  - Rosa Clark
date: 2018-12-27
categories:
  - Community
  - Crossref LIVE
  - Annual Meeting
archives:
    - 2018
---

Running a smooth event is always the goal, but not always the case! No matter how well managed an event is, there is always a chance that things will not go according to plan. And so it was with LIVE18.

{{% imagewrap right %}}<img src="/images/blog/notablecloth.png"  alt="image of tables" width="325" >{{% /imagewrap %}}

For the first day we were without the tablecloths we had ordered, which actually gave the room quite a nice, but unintentional, ‘rustic’ look. When they finally did arrive the following day, we realized we preferred the rustic look! Some of the merchandise we had prepared ended up sitting in Canadian Customs for a day and a half, which meant they arrived to us halfway through the first day of the event. Luckily attendees were distracted by the very cool ‘I heart metadata’ bags and didn’t seem to notice.

Unfortunately a significant number of registrants also had problems with Canadian regulations: they were denied visas to enter the country. Despite always trying to choose countries with international airport hubs and a welcoming policy, this was an unforseen blow.

But from setting up to take down, LIVE18 was truly a team effort. Even though many Crossref staff had traveled far and wide to get there, they all rallied to help the night before—hauling boxes through the streets of Toronto, stuffing attendee bags, hanging signage, and moving furniture around until 11:30 pm.

Because of these efforts---and despite the glitches---Crossref LIVE18 was a great success.

## How good is your metadata?

That was the framing question at Crossref LIVE 18 in Toronto which this year focused on all things metadata. Over the course of the two-day event, we heard from guest speakers on the importance of collaboration, the significance of metadata to metrics, and what good metadata looks like. In our usual lively way, Crossref staff introduced a variety of new services, initiatives, and collaborations.

> [Crossref LIVE](/crossref-live-annual) is helping surface key issues in the cleanup of metadata mismatch, after decades of the industry working in silos. I applaud Crossref for doing this. It’s great that we’re considering how to change the way we work and collaborate as an industry to make sure that we don’t run into metadata issues in this way again.

_- Keynote speaker, Kristen Ratan, Co-Founder of the Collaborative Knowledge Foundation (Coko)_

In her keynote speech, ‘Publishing Infrastructure: The Good, The Bad, and The Expensive’, Coko’s Kristen Ratan challenged the industry to rethink its slow, inefficient, and expensive resignation to infrastructure; and instead consider how a collaborative approach to sharing expertise in developing community-owned infrastructure could be faster, more flexible, and less costly.

- View Kristen’s talk, [The Good, The Bad, and The Expensive](https://youtu.be/V_Y5uSCL4ec)

{{% imagewrap right %}}<img src="/images/blog/cruse-ror.png"  alt="image of Patricia Cruse" width="350" >{{% /imagewrap %}}

## The collaborations

Collaboration was a running theme at LIVE18. Geoffrey Bilder provided an overview of Crossref’s selective collaborations; DataCite’s Patricia Cruse introduced [ROR](https://ror.org/), the community project to develop an open, sustainable, usable and unique identifier for every research organization in the world—and she got the crowd really engaged at the beginning of her talk by encouraging us all to ROAR out loud!; Clare Dean and Ravit David sketched out the evolution of [Metadata 2020](http://www.metadata2020.org/), and Shelley Stall from the AGU introduced the ways they are urging the scientific community to adopt FAIR data principles (using her first data collection as an 11-year-old as an example!)

- View Geoffrey’s talk, [How Crossref (selectively) collaborates with others](https://youtu.be/3_s6M9OKWp0)
- View Patricia’s talk, [ROR: The Research Oragnization Registry](https://youtu.be/TknM8YaTl8M) (Roar!) 🦁
- View Clare and Ravit’s talk, [Metadata 2020: This talk is sooo meta](https://youtu.be/QjvpQNwEmA8)
- View Shelley’s talk, [My first data collection: Was it FAIR?](https://youtu.be/VvZpTLjGWxs)

## The solutions

- Patricia Feeney, in the newly-created role of Head of Metadata, used a zebra to illustrate that not all of a publisher’s metadata is deposited with Crossref.
- View Patricia’s talk, [I am the boss of your Metadata](https://youtu.be/RHUCf3p-TUk) (this one has the zebras) and also her talk on [New content types in the works at Crossref](https://youtu.be/DHd6oRJiVE8).

<center><img src="/images/blog/crossref-zebra-unicorn-comic-strip.png" width="80%" /> </center>

## New tools

Jennifer Lin introduced [Event Data](/services/event-data/), the new API that Crossref and DataCite have built together, enabling organizations to capture what happens to a DOI, including all of the places it is mentioned and links from/to. She also talked about [Participation Reports](https://www.crossref.org/members/prep/), the new open dashboard to help members evaluate the completeness of their own metadata deposited with Crossref.

- View Jennifer’s talks on [Event Data](https://youtu.be/IkaNajvRXGY), and [Simplifying our services](https://youtu.be/c3oo31VLsiA)

## The community

We also heard from the community. Paul Dlug from the American Physical Society boldly gave his view on ‘Why Crossref sucks’, and, with a view to helping Crossref improve in key areas, surfaced issues that members struggle with. Ed Pentz, Executive Director, provided an overview of the direction that Crossref is headed towards. Ginny Hendricks, Director of Member & Community Outreach, updated everyone on the expanding Crossref community and all the outreach activities her team conducts to engage them. Isaac Farley, new Technical Support Manager in the community team, told of his vision for moving to a more public, open, support model. Lisa Hart, Director of Finance & Operations announcing the results of our members votes in this year's [board election](/board-and-governance/).

- View Paul’s talk, [Crossref sucks and how to cope!](https://youtu.be/TrYAsX4vjU0)
- View Ed’s talk, [Our strategic direction](https://youtu.be/z3sZVVvSExg)
- View Ginny's talk, [Expanding our constituencies](https://youtu.be/RtaJq-NUFJE)
- View Isaac's talk, [Open Support: From 1:1 to everyone](https://youtu.be/4F8Cv9NTaRQ)

### The perspectives
Guest speakers provided a range of fascinating perspectives from across scholarly communications. Graham Nott, who works with eLife, outlined how they were making their JATS to Crossref schema conversion tool openly available to the community for use. Jodi Schneider, Assistant Professor of Information Sciences at the University of Illinois at Urbana-Champaign, gave us an in-depth look at problem citations, with a focus on retractions.  Bianca Kramer from Utrecht University discussed Crossref metadata use in an open scholarly ecosystem. Stephanie Haustein from the University of Ottawa gave a researcher perspective on the problems with traditional journal metrics, and how they are dependent on metadata, which is essentially flawed. She outlined her efforts to increase metrics literacy, putting metrics in context with comprehensive metadata. Geoffrey Bilder talked about Dominika's work to evaluate our reference matching, and finally closed the show discussing the role of metadata in creating a provenance infrastructure, providing trustworthiness which is essential to progress the scholarly research cycle.

- View Graham’s talk, [JATS at eLife](https://youtu.be/W0xaEw4FDjs)
- View Jodi’s talk, [Trouble at The Academy: Problem Citations](https://youtu.be/vCQexoeGqjY)
- View Bianca’s talk, [DOIs for whom? Crossref metadata in an open scholarly ecosystem](https://youtu.be/IOMn5Brzxzs)
- View Stephanie’s talk, [Good metadata + metrics literacy = better academia](https://youtu.be/tlwSt9P4feo)
- View Geoffrey’s talks on [Reference matching](https://youtu.be/sq00YZt8TxQ), and [Metadata as a signal of trust](https://youtu.be/MLCAVbwBL5A).

As LIVE18 came to a close we took the opportunity to acknowledge and thank everyone once again for helping us reach the milestone of 100 million registered content items this September. Everyone took to the stage and waved their Crossref Bigger Ambitions flags.

## Thank you to everyone who participated in the event. Please save the dates for LIVE19 in Europe on 13-14 November, 2019!

<center><img src="/images/blog/100milgroup-small.png" alt="group of people holding flags" width="600" class="img-responsive" /></center>
