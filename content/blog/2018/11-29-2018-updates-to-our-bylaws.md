---
title: Updates to our by-laws
author: Lisa Hart Martin
draft: false
authors:
  - Lisa Hart Martin
date: 2018-11-29
categories:
  - Board
  - Governance
  - Election
archives:
  - 2018
---

Good governance is important and something that Crossref thinks about regularly so the board frequently discusses the topic, and this year even more so. At the November 2017 meeting there was a motion passed to create an ad-hoc Governance Committee to develop a set of governance-related questions/recommendations. The Committee has met regularly this year and the following questions are under deliberation regarding term limits, role of the Nominating Committee, implications of contested elections, and more.

<!--more-->

The full motion to create the committee is:

> The ad hoc Governance Committee should discuss and make specific recommendations (including where necessary proposing appropriate by-law amendments) about (i) the timing of the annual election of members and whether the newly elected Board can take office a fixed period after the election results are finalized; (ii) the role and responsibilities of the Nominating Committee and its relationship to the Board; (iii) the implications of having contested Board elections; (iv) the election of officers, Executive Committee members, and committee chairs, and v) options and required changes for board members to represent specific constituencies (e.g. based on membership types).

The Governance Committee members are:

- Paul Peters (Hindawi and Board Chair)
- Scott Delman (ACM and Board Treasurer)
- Chris Shillum (Elsevier and Executive Committee member)
- Mark Patterson (eLife)
- Ed Pentz (Crossref Executive Director)
- Lisa Hart (Crossref Finance & Operations Director)
- Emily Cooke (Pierce Atwood, legal counsel).

The committee’s goal was to try to maintain and increase transparency; consider practicality and impact of any changes and ensure continuity and balance.

At the March meeting the committee provided an overview of the issues they had discussed. There was consensus to accept the committee’s recommendation to address all of the governance matters comprehensively at the July 2018 meeting.

Discussions resulted in two changes to our [by-laws](/board-and-governance/bylaws/):

## 1. Membership eligibility

To provide clarity around membership qualification, we resolved to amend Article I Section 1 by replacing the text in its entirety with the following text:

  > Membership in Crossref shall be open to any organization that publishes professional and scholarly materials and content and otherwise meets the terms and conditions of membership established from time to time by the Board of Directors, and to such other entities as the Board of Directors shall determine from time to time.

## 2. Start date of board terms

We also resolved to amend Article V Section 4 to replace the phrase “on the day after” with the phrase “during the next calendar quarter immediately following”. This allows the Board to meet directly ahead of Crossref’s Annual Meeting and Board election (from 2019) instead of directly after.

The first change captures the fact that we have a very broad community beyond what is seen as traditional publishers, who themselves do not solely identify as publishers anymore. It reflects how our membership has evolved, and also includes organizations that publish that aren’t publishers (universities, government agencies, etc.)

The second change was a practical one. As Crossref had its first contested election in 2017, and in 2018 as well, it seemed unreasonable to have a brand new Board meet the day after the election, especially when there is potential for officers to not be re-elected. The old by-laws were very specific about holding the Board meeting the day after the election. With this change, starting with the March meeting, the new Board will have a full calendar year of meetings, which seems more practical, and we will establish a process for the election of officers.

During that meeting we deliberated the following questions/recommendations raised by the committee:

- Development of a policy on canvassing/campaigning by candidates in Board elections;
- Development of policies on nominations to the positions of Chair, Treasurer, Executive Committee members, the Nominating Committee Chair, and the Audit Committee Chair;
- Analysis of how best to achieve balance and representation on the Board going forward (designated seats and/or a binding Board remit to the Nominating Committee);
- Analysis as to whether to impose term limits on board members;
- Analysis as to how best to handle independent nominations to the Board (eliminate the option, or improve the process); and
- Review of our governing documents’ provisions on vacancies, to confirm that the Board follows the required steps on the filling of vacancies.

At the November 2018 Board meeting---following Crossref LIVE18---there were two more amendments to the bylaws:

## 3. Removal of independant nominations

To remove Art. V II Sec. 3 on independent nominations. This change reflects the consensus that there is no need for independent nominations with the introduction of contested elections.

## 4. Membership start date of record

To amend Art. I Sec. 2 to amend language dealing with record date of membership. This is a practical change following the July 2018 introduction of [new membership terms](/membership/terms) which are click-through online terms and don't need counter-signatures.

The new Board will resume the discussion on designated seats at our March 2019 meeting.
