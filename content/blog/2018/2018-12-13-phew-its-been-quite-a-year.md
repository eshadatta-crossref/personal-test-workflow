---
title: "Phew - its been quite a year"
author: Ed Pentz
draft: false
authors:
  - Ed Pentz
date: 2018-12-13
categories:
  - Members
  - Member Briefing
  - Community
  - Strategy
archives:
  - 2018
---

As the end of the year approaches it’s useful to look back and reflect on what we’ve achieved over the last 12 months—a lot! To be honest, there were some things we didn’t get done—or didn’t make as much progress with as we hoped—but that happens when you have an ambitious agenda. However, we also got some things done that we didn’t expect to or that weren’t even on our radar at the end of 2017—this is inevitable as the research and scholarly communications landscape is rapidly changing.

<!--more-->

In my [blog post](/blog/a-year-in-the-life-of-crossref/) from the beginning of the year, the key projects I highlighted were [Metadata Plus](/services/metadata-retrieval/metadata-plus/), [Event Data](/services/event-data/), [Organization IDs](/blog/org-id-a-recap-and-a-hint-of-things-to-come/), [Grant IDs](/blog/global-persistent-identifiers-for-grants-awards-and-facilities/) and [Metadata 2020](http://www.metadata2020.org), and that richer metadata and more content types were key goals. We did make very good progress on all of these projects as reported below.

For 2018 we were operating in the framework of the four strategic themes, or areas of focus, developed by the board and staff. These are: 1) Simplifying and enriching our services; 2) Improving our metadata; 3) Expanding constituencies, and 4) Selectively collaborating and partnering. These themes will also be guiding us in 2019.  

## Simplifying and enriching our services

### Upgrading our tools
Over the past year, we’ve been busy streamlining our processes, developing new tools and adding new services. A key new tool is [Metadata Manager](https://www.crossref.org/metadatamanager/) which supports the [Content Registration service](/services/content-registration/) by offering a simpler, more user-friendly, non-technical way to register and update metadata. It provides lots of context-sensitive help, registers content immediately, in real time, and provides guidance on how to make corrections—thereby ensuring each deposit is successful. Metadata Manager currently supports journal deposits (we would have liked to add more in 2018) but we will be adding other content types on 2019.

### Upgrading our services
[Crossref metadata](/services/metadata-retrieval/) has always been open through a number of interfaces without restriction, but this year we introduced an option for extra support and functionality, through [Metadata Plus](/services/metadata-retrieval/metadata-plus/). Metadata Plus provides guaranteed uptime, snapshots of the complete set of metadata and enhanced support for organizations (members or not) that want to use Crossref metadata in their own services and systems.

### Improving the member experience: New membership terms
This year we began to redesign the member experience and have made a lot of improvements to the sign-up and onboarding process, the most significant of which is the new click-through membership terms, introduced in July for new members and coming into effect for existing members in March 2019, which is proving to be a huge time saver for both our members and our team.

## Improving our metadata

Our objective this year was to better communicate what metadata best practice is, to equip our members with all the data and tools they need to meet this best practice, and to achieve closer cooperation from service providers.

### Best practice tools: Participation Reports
Released in Beta in August this year, [Participation Reports](https://www.crossref.org/members/prep/) provides a dashboard that gives a clear picture of the metadata that each member provides. This is a useful visualization of metadata that has long been available via our public REST API. Members can see where the gaps in the metadata are and get information on how to fill those gaps.

### Communicating metadata best practice: Data Citations
[The importance of linking data](/blog/why-data-citation-matters-to-publishers-and-data-repositories/) with literature can’t be understated. Research integrity and reproducibility depend on it. We're committed to exposing the links between the literature and the data or software that supports it, and earlier this year we partnered with [DataCite](https://www.datacite.org/) to make this a reality. All the data citations coming in from Crossref and DataCite are being pulled into Event Data.

### Equipping members with all the data: Event Data
[Event Data](/services/event-data/) reached technical readiness. Event Data captures and records “events” such as comments, links, shares, bookmarks, and references. It provides open, transparent, and traceable information about the provenance and context of every event.

## Expand constituencies

Crossref currently has 15,000 members in 140 countries. With that comes the need to increasingly and proactively work with emerging markets as they start to share research outputs globally.

### Ambassador program
[The Crossref Ambassador program](/community/ambassadors/) launched in January and now has a team of 16 trusted contacts who work within our communities (as librarians, researchers, publishers, and innovators) around the world. They share great enthusiasm and belief in our work. We provide them with training and support, and they help us improve education about global research infrastructure in general and the opportunities that are enabled through richer metadata.

### Funders and grant identifiers
I’m very happy to report that the Crossref board approved grants as a new content type to be rolled out in 2019 - we made faster progress on this than expected. The proposal for grant identifiers was developed by staff in collaboration with the [Crossref Funder Advisory Group](/working-groups/funders/) and the [Membership and Fees Committee](/committees/membership-and-fees/). This means that funders will be joining Crossref and registering a standard set of metadata and a persistence identifier - a DOI - for their grants.

## Collaborate and partner

So that our alliances with others have the greatest impact, we have aligned our strategic plans for scholarly infrastructure with others. Some of these alliances are led or driven by Crossref and with others we are involved but not leading.

### ROR
We are working with the [California Digital Library](https://www.cdlib.org/), [DataCite](https://www.datacite.org/) and [Digital Science](https://www.digital-science.com/) as the Steering group for [ROR](https://ror.org/) - the Research Organization Registry - which is a new, community-led project that is developing an open, sustainable, usable, and unique identifier for research organizations based on the work done by the [Organization Identifier Working Group](https://orcid.org/content/organization-identifier-working-group) in 2017 and 2018.

### Metadata 2020
[Metadata 2020](http://www.metadata2020.org) is a collaboration that advocates richer, connected, and reusable, open metadata for all research outputs, which will advance scholarly pursuits for the benefit of society. Over 140 volunteers—including publishers, librarians, researchers, platforms/tools, and other stakeholders—from 86 organizations, are working in six project groups. The projects are very strategically focused, looking at key issues like researcher communications, incentives, and shared best practices.

I can’t close off the year without mentioning the incredible milestone we reached this September when [the 100th million content item was registered](/blog/100000000-records-thank-you/) with Crossref. This was made possible by our members’ and the wider community’s commitment and contribution, so once again, thank you.

> Roll on 2019!


---
