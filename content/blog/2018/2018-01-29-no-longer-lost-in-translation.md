---
title: 'No longer lost in translation'
author: Christine Cormack Wood
draft: false
authors:
  - Christine Cormack Wood
date: 2018-01-30
categories:
  - Outreach
  - Members
  - Community
archives:
  - 2018
---

More than 80% of the record breaking 1,939 new members we welcomed in 2017 were from non-English speaking countries, and as our member base grows in its diversity, so does the need for us to share information about Crossref and its services in languages appropriate to our changing audience.

<!--more-->

So, early last year we started translating our service videos into six other languages: French, Spanish, Brazilian Portuguese, Chinese, Japanese, and Korean. However, the process of translating from one language to another is not always straightforward—but it is super important—as some things can get seriously lost in translation...


|<img src="/images/blog/dog.png" height="250px" width="300px"/>|<img src="/images/blog/foot.png" height="250px" width="300px"/>|<img src="/images/blog/luggage.png" height="250px" width="300px"/>|


In order to avoid such translation tragedies we created a foolproof process to get the text of the service videos translated and ready for production. (I am, I realize, exposing myself here—see what I did there? —by using a word like foolproof.)

First we produced the videos in English, setting the content to animation and sound (AKA audio visual or A/V to us marketing types), then we brought in a translation company to turn the English content into the six other languages. So far so good. However, as the above examples demonstrate, the *meaning* of words can get lost in translation. Also, what Crossref does isn’t the easiest thing in the world to translate (*are* there words for *metadata delivery* and *full-text XML* in Japanese?), so we added another stage to the process.

Next, we sent the translated scripts and their English counterparts to some very helpful international members who, as part of the scholarly research community, understand the complexities of our work and are therefore qualified to check that the text had remained *in context.*

Unfortunately, it hadn’t, as the text came back from them heavily edited. After round two of the editing process, the revised text was applied to the videos—but just to be 100% sure, we sent the completed videos back to our helpful international members for a final run through.

Multiply this painstaking process by 48 videos, throw numerous time zones into the mix and you can see why it took us nearly 12 months to complete them.

And so, it is with great pleasure that today we launch all eight of our service videos in six languages, just click the links below, and enjoy! Découvrez-les!​	¡Que los disfrutes!	Aproveite! 请欣赏!  どうぞお楽しみください！	즐거운 시간 되세요!


|   |   |   |                    View videos by language    |   |   |   |
:-:|:-:|:-:|:-:|:-:|:-:|:-:|
[English](https://www.youtube.com/playlist?list=PLe_-TawAqQj2f2I-TevZcFchyhEAhkQ0g)   | [French](https://www.youtube.com/watch?v=hK3LAAfm1-U&list=PLe_-TawAqQj22lY2dikyWA3XCvmDaZcEV)  | [Spanish](https://www.youtube.com/watch?v=G309-3KW7ok&list=PLe_-TawAqQj02nIuITrQdds9Vt8A2jKvm)  | [Brazilian Portuguese](https://www.youtube.com/watch?v=wI1peEvLINU&list=PLe_-TawAqQj37hN_S8Qice7DDB6cu1TPZ)  | [Simplified Chinese](https://www.youtube.com/watch?v=VXPCYulcEHs&list=PLe_-TawAqQj0zVsT6A3ym6HLMHAXMWORd)  | [Japanese](https://www.youtube.com/watch?v=IPvf4Zl2qLY&list=PLe_-TawAqQj05sOlOtYsV1uiBAydpvxKr)   | [Korean](https://www.youtube.com/watch?v=Q_yXjiinHG0&list=PLe_-TawAqQj2pHiy0XZRWctA-ac_hUcVx)   |
| English | français  | español  |  português do Brasil | 简体中文  | 日本語 | 한국어로 |



<br><br>
_We'd like to thank the following for their help in checking the video translations: Fabienne Meyers from IUCAP for the French versions, our very own resident translator Vanessa Fairhurst for the Spanish versions, Edilson Damasio from the University Library of Maringá for the Brazilian Portuguese versions, Guo Xiaofeng from Wanfang Data for the Chinese versions, Nobuko Miyairi from ORCID for the Japanese versions and Junghyo from Nurimedia and Jae Hwa Chang at infoLumi for the Korean versions._

***
