---
title: 'Crossref LIVE in Tokyo'
author: Rachael Lammey
draft: false
authors:
  - Rachael Lammey
date: 2018-03-08
categories:
  - Outreach
  - Crossref LIVE
  - Community
  - Collaboration
archives:
  - 2018

---
What better way to start our program of LIVE locals in 2018 than with a trip to Japan? With the added advantage of it being Valentine’s Day, it seemed a good excuse to share our love of metadata with a group who feel the same way!

<!--more-->

We’ve worked closely with the Japan Science and Technology Agency (JST) since 2002, and were delighted when they agreed to collaborate with us on a LIVE event at their offices in Tokyo.{{% imagewrap right %}} <img src="/images/blog/val-day.png" alt=“Valentines Day message" height="150px" width="400px" class="img-responsive" /> {{% /imagewrap %}}

With help from the team at JST, we welcomed around 80 attendees—a mix of editors, publishers and enthusiastic metadata users—who all enjoyed the talks from our guest speakers, Nobuko Miyari from ORCID, Ritsuko Nakajima from JST and Tatsuji Tomioka from Kyoto University Library (who talked about the use of DOIs and metadata in their research information repository, named KURENAI).

Vanessa Fairhurst and I also took part in the days program and talked about the different services that Crossref offers. With many of our members in Japan already well-versed in DOIs, we placed the focus of our sessions around the importance of accurate, complete metadata, and new content types (such as peer reviews and preprints). We also discussed our new community initiatives such as the [OI project](https://blog.datacite.org/next-steps/), [identifiers for grants](/blog/global-persistent-identifiers-for-grants-awards-and-facilities/) and [Metadata2020](http://www.metadata2020.org/).

We’d like to say a big thank you to Kentaro Kinoshita from JST for his help with organizing the event. We’d also like to thank the excellent team of translators who assisted us greatly by relaying the content to the audience in Japanese—being able to offer information and take questions in English and Japanese was an invaluable part of the day.

### Any questions?<br>
One day is never quite enough to cover all things Crossref, so we were happy to answer questions from the enthusiastic audience:

**What metadata is required to register peer review reports with Crossref?**<br>
To answer this we pointed them to this informative blog on [peer reviews](https://support.crossref.org/hc/en-us/articles/115005255706-Peer-Reviews).

**How can I find information on using your REST API?**<br>
This is a great starting point, and most information can be found here https://api.crossref.org

**Is the forthcoming Metadata Manager tool something I can use?**<br>
Yes! We hope it will make it much easier for you to deposit good metadata—and if you are in interested in participating in our open beta, [let us know](mailto:feedback@crossref.org).

We’re looking forward to continuing to collaborate with JST, and are really grateful for their help in working with us to make the event go so smoothly. Thank you to those who joined us, and we hope to see you again soon.

<br>
