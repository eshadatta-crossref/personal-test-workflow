---
title: 'License metadata FTW'
author: Rachael Lammey
draft: false
authors:
  - Rachael Lammey
date: 2019-06-28
categories:
  - Metadata
  - Licenses
  - Participation
  - Member Briefing
archives:
    - 2019
---

More and better license information is at the top of a lot of Christmas lists from a lot of research institutions and others who regularly use Crossref metadata. I know, I normally just ask for socks too. To help explain what we mean by this, we've collaborated with [Jisc](https://www.jisc.ac.uk/) to set out some guidance for publishers on registering this license metadata with us.
<!--more-->

At the most basic level, complete and accurate license metadata helps anyone interested in using a research work out how they can do so. Making the information machine-readable helps this to be done easily and at scale by all kinds of tools and services.

[In this best practice guide](/education/content-registration/administrative-metadata/license-information/#00481/), we’re specifically focusing on a use case for license metadata that comes from research institutions. They need to know which version of an article (or other content item) may be exposed in an open repository, and from what date, and tell anyone who comes across the piece of content in the repository what they can do with it once they find it there.

Without this being stated simply and clearly in the Crossref metadata, the institution won’t know which works they can make available and which they cannot, even if you as the publisher know that the item is open access, or is open access after a certain date. This can impact the research community’s capacity to find and use the research you publish.

The guidance offers advice on:     
1. the kind of license information it’s useful to link out to from the Crossref metadata
2. what the Crossref metadata might look like for:  
  * gold open access content  
  * green open access content with a Creative Commons License  
  * green open access content with a publisher-defined post-embargo license  
3. how to add this metadata to existing or new Crossref deposits  

### Take a look at [the full guidelines](/documentation/schema-library/markup-guide-metadata-segments/full-text-URLs/) here.

Maybe there’s more to the story than this, or more information that you need as a publisher or as a research institution - if so, [let us know](mailto:feedback@crossref.org) and we can adapt this document based on your feedback. Requests for socks may be declined.
