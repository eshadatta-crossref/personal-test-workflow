---
title: 'Speaking, Traveling, Listening, Learning'
author: Vanessa Fairhurst
draft: false
authors:
  - Vanessa Fairhurst
date: 2019-08-29
categories:
  - Outreach
  - Crossref LIVE
  - Collaboration
  - Community
archives:
    - 2019
---
2019 has been busy for the Community Outreach Team; our small sub-team travels far and wide, talking to members around the world to learn how we can better support the work they do. We run one-day [LIVE local events](/events/) alongside multi-language [webinars](/webinars/), with the addition of a new [Community Forum](https://community.crossref.org/), to better support and communicate with our global membership.   

This year we held a publisher workshop in London in collaboration with the British Library in February to talk about all things metadata and Open Access, before heading over to speak to members in Kyiv in March at the National Technical University of Ukraine. June saw our first ever non-English LIVE local event in Bogota held in collaboration with Biteca, and in an action-packed week in July, Rachael Lammey and myself jetted across to Kuala Lumpur and Bangkok where we collaborated with Malaysian Ministry of Education, USIM, Chulalongkorn University, iGroup, and ORCID to run two events for our South-East Asian members.   

Despite the varied locations, speakers and audiences at these events, some common themes emerged...   

## Language Matters   

We currently work with member organisations in over 125 countries around the world, spanning an even greater number of languages. Whilst, at the moment at least, it is not possible to provide support across all these languages, we are improving support for non-native English speakers. We now have service [videos](https://www.youtube.com/channel/UCO0pjPM4wCJRnjI6ivFXKGA/playlists?view=50&sort=dd&shelf_id=2), [factsheets](/services/), and brochures available in 8 languages including: French, Spanish, Brazilian Portuguese, Arabic, Chinese, Japanese, Korean, and Bahasa Indonesia. As well as expanding our webinars to include a series in Russian, Brazilian Portuguese, Arabic, Spanish and Turkish so far.   

Our global team of 24 [Ambassadors](/community/ambassadors/) have been key in helping us to provide translated documentation, to run multi-lingual webinars and in-person events, and to answer questions from our members across languages and timezones. Our LIVE local event in Bogota, saw us run our first ever Spanish event with support from our Latin American ambassador team.  

<p align="center">
<img src="/images/blog/2019/ambassadors-bogota.jpg" alt="Ambassadors in Bogota" width="500px" />
</p>

I know first hand how daunting public speaking can be, particularly in a second language. As a non-native Spanish speaker, the fear of being misunderstood or mis-pronouncing a word can be paralysing. Members come along to our events with a whole host of questions, sometimes preferring to come and speak to us one-on-one at the break or follow up with us after the event. Everyone has their own preferences, however, being able to communicate in the local language helps to break down barriers and boosts audience participation by taking away these added pressures.    

Additionally after running a number of these events, one of the key things we have learnt is how much content to cover in a day. Our LIVE locals are free to attend and open to the whole community. This however can mean that we have a very varied audience in terms of technical know-how and experience of working with our systems. At first we attempted to cover all we could, addressing as many needs, questions and uses of Crossref metadata that we could. However, creating content to please everyone is often a recipe for disaster and information overload. If you start to see your attendee’s eyes glaze over or they start answering emails on their smartphones, you’ve lost them.    

Instead we are now going to tailor our events a little more, asking registrants questions in advance, and selecting specific topics to cover. Having a good range of distinct topics and presenters, including local guest speakers, also helps to maintain momentum and avoid audience fatigue. Wider information and conversations will then continue on our [Community Forum](https://community.crossref.org/) as well as events being supplemented by [webinars in local languages and timezones](/webinars/).    

## Relationship status: It’s complicated   

A question we are often asked when talking to members is how to link distinct content items in the metadata - whether this be a data-set to the published results, a preprint with the version of record, or a translated version of an article with the original.
Linking these related research outputs is extremely important; researchers need to be able to cite the correct version of the work they have used in their research. Creating a network of these linkages between scholarly outputs also helps ourselves, our members, and the wider community better track how research is used and developed.  

English is by far the most common language used in international academic journals and often is required for publication, however the article can be published in two or more languages, enabling greater discovery and use of the research. A frequent question we get asked is how to register the two versions, whether they use the same DOI or whether each should be assigned its own identifier. Our [advice](https://support.crossref.org/hc/en-us/articles/214357426-Relationships-between-DOIs-and-other-objects) is that each version of the article should have it’s own DOI for citation reasons, but should be linked in the metadata of the translated version as in the xml example below:   

<p align="center">
<img src="/images/blog/2019/relationship-example-xml.png" alt="Relationship example xml" width="600px" />
</p>

However, our schema covers far more relationship types than purely translations. Another interesting area of discussion which has become increasingly prevalent in the last couple of years is around preprints. We began supporting the registration of preprints in November 2016, using their specific content type and enabling linking in the metadata to the version of record, providing a clear publication history for accurate citation. Today we have almost 150k registered in our system.   

In Kyiv, we had a request to talk more about data citation; the importance of making data available and persistently linked to. Although data is often shared, it is not routinely referenced in the same way as journal articles or other publications, and this is something we want to encourage. When data is cited it provides clarity and context about the research underpinning the published article, as well as enabling greater discovery and re-use of that data in future research and publications. You can do this in two ways at Crossref, either by including data citations your reference lists, or, again, by using the relations section of the schema. If you want to learn more about the ‘how’ of data citation, we have some [useful guidance](/blog/data-citation-what-and-how-for-publishers) you can take a look at.   
{{% imagewrap left %}} <img src="/images/blog/2019/Otters.png" alt=“Meaningful connections like the otters" height="100px" width="300px" class="img-responsive" /> {{% /imagewrap %}}

{{% imagewrap right %}} <img src="/images/blog/2019/Otters.png" alt=“Meaningful connections like the otters" height="100px" width="300px" class="img-responsive" /> {{% /imagewrap %}}

As we are always saying Crossref is all about making connections. Linking research objects by capturing and declaring relationships within your metadata helps to map the evolution of research. Making the distinct parts of the research and publication process accessible by both readers and machines, enables wider discovery, re-use, transparency, accuracy of citations and provides greater acknowledgment of contributors.



## Finding Solutions to Resolutions   

Reports are rarely the things that get pulses racing (you should probably take a long, hard look at yourself if so) but they are important and can be very useful to make sure your content and the associated metadata is being registered correctly.

We often get questions from members who want to better understand their resolution reports. These are reports generated on a monthly basis for each DOI prefix, sent to the business contact for your organization, which provide statistics on the resolution rates of your content. So what do we mean by a resolution? Well simply, when a reader clicks on a DOI link for an article, that counts as one DOI resolution. No information is captured about the user or where they are coming from. Although we work to filter out computer-generated usage, the numbers are not a precise measure of human click-throughs to a publishers website - cached articles, search engine crawlers, and traffic directed through a library link resolver can be included in these numbers. However, the reports still provide a good idea of traffic to your publications via the DOI.

Often the part of the report which is of particular interest is the resolution failure rate. Although in an ideal world this would be 0%, realistically 2-3% is the norm. Publishers who are new to Crossref or who have created a small number of DOIs may have a higher failure percentage and this isn’t necessarily a problem (for example, a publisher with 1 failure and 9 successes will have a 10% failure rate). A .csv file containing a list of all failed DOI resolution attempts for the month is attached to each report so that you can review any significant number of failures or any dramatic changes which may indicate a problem that needs to be solved.

**Possible reasons for DOI failures:**

* Bad links - check that your DOI is directing readers to the correct location of your full text or landing page.
* Undeposited DOIs - any DOIs that have been distributed or published should be deposited immediately. Simply adding a DOI to your content page will not automatically register this link.
* Similarly, if your DOI was deposited mid-month and distributed earlier, any attempts prior to this date will appear as failures on your report.
* User error - sometimes users can make mistakes when typing or copy-and-pasting DOIs. To minimize the risk of this keep your DOIs simple and short.   

It is also important to make sure you keep the contact details we have on file for your organization up to date. Otherwise you might miss out on receiving important information about your account. Where it is possible we ask members to submit at least three separate contacts and review this regularly as people often move within and between organizations. We want to keep in touch to give you helpful, essential and interesting information (no spam!)   

## Get involved   
{{% imagewrap  right%}} <img src="/images/blog/2019/live-bangkok.jpg" alt=“LIVE Bangkok" height="125px" width="375px" class="img-responsive" /> {{% /imagewrap %}}


Our next LIVE local event will be held in Oakland, California on 19 September, [registration is open](https://crossrefoakland.eventbrite.com) and spaces are still available. Alternatively you might want to sign up to one of our interactive [Metadata Manager webinars](/webinars/) to learn how to use our new content registration tool. Our plans for 2020 are still in the inception phase and we welcome any interest in collaboration, you can contact us at feedback@crossref.org or send us a message on the [Community Forum](https://community.crossref.org/c/crossref-calendar), where you can also keep up to date with our plans as well as giving us your feedback and suggestions. Speaking of feedback and, we have a survey which is trying to collect just that. Please [let us know what you value about Crossref](https://www.surveygizmo.com/s3/5151355/cabad33fcc9b) (and what you don’t) - we’d love to hear your thoughts.


<br/>
<br/>
<br/>

---
