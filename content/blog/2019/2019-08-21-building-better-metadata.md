---
title: 'Building better metadata with schema releases'
author: Patricia Feeney
draft: false
authors:
  - Patricia Feeney
date: 2019-08-21
categories:
  - Metadata
  - Schema
  - Content Registration
  - Member Briefing
archives:
    - 2019
---

This month we have officially released a new version of our input metadata schema. As well as walking through the latest additions, I'll also describe here how we're starting to develop a new streamlined and open approach to schema development, using GitLab and some of the ideas under discussion going forward.
<!--more-->

## What's included in version 4.4.2

The latest schema as of August 2019 is version 4.4.2 and this release now includes:

- Support for "[pending publication](/help/pending-publication/)"
- Support for JATS 1.2 abstracts
- Abstract support to dissertations, reports, and allow multiple abstracts wherever available
- Support for multiple dissertation authors
- A new `acceptance_date` element added to journal article, book, book chapter, and conference paper content types

"Pending publication" is the term we've coined for the phase where a manuscript has been accepted for publication but where the publisher needs to communicate a DOI much earlier than most article metadata is available. Some members asked for the ability to register and assign DOIs prior to online publication, even without a title, so this allows members to register a DOI with minimal metadata, temporarily, before online publication. There is of course no obligation to use this feature.

It's worth calling out the addition of `acceptance_date` too. This is a key attribute that is heavily requested by downstream metadata users like universities. Acceptance dates allow people to report on outputs much more accurately, so we do encourage all members to start including acceptance dates in their metadata. It's highly appreciated!

## Schema files public on GitLab

I’ve added our latest schema to a new [GitLab repository](https://gitlab.com/crossref/schema), There you’ll find the schema files, some documentation, and the opportunity to suggest enhancements. The schema has been released as bundle [0.1.1](https://gitlab.com/crossref/schema/-/releases) and also includes our new Grant metadata schema for members that fund research.

The schema has been available in some form for months but at this point we consider it ‘officially’ released to kick off our new but necessary practice of formal schema releases. Any forthcoming updates will be added to the next version.

## Schema management process

We’ve been adding sets of metadata and new content types over the years, but also need to have a defined process for small but vital pieces of metadata that you need to provide and retrieve from our metadata records. If you’re wondering what our procedure for updating our schema is, you are not alone! We have not had a formal process, instead relying on ad-hoc requests from our membership and working groups. Our release management and schema numbering has also not been consistent.

Going forward, I will ensure that all forthcoming versions of our metadata schema are be posted as a draft on GitLab for review and comment, and the final version will be officially released via GitLab as well.

It's important to note that when we talk about "the schema", we generally mean  the _input_ schema specifically i.e. what members of Crossref can register about the content they produce. As always, the output for retrieving that metadata is subject to separate development plans for our Metadata APIs. I'm working with our technical team so we can develop and introduce an 'end-to-end' approach that doesn't in future treat the input and the output as such separate considerations.

## What's next

Many of the updates in this latest release have been in the works for some time. Changes to our metadata both large and small are considered carefully, but I’d like to do this in a transparent and cooperative way with our community.

I recently set up the "Metadata Practitioners Interest Group" and we've just had our second call. A big topic was how to best manage the ideas and requests from the community. The ability for public comments on GitLab is a first step.

This most recent update contains a mix of long term projects and updates to keep our metadata current and useful. Other changes that are under discussion will require more development on our end. But stay tuned for more information about forthcoming changes, as well information about how you can contribute.
