---
title: 'What can often change, but always stays the same?'
author: Isaac Farley
draft: false
authors:
  - Isaac Farley
date: 2019-01-24
categories:
  - Content Registration
  - Title Transfers
  - Membership
  - Metadata
  - Identifiers
archives:
    - 2019
---

Hello. [Isaac](/people/isaac-farley/) here again to talk about what you can tell just by looking at the prefix of a DOI. Also, as we get a lot of title transfers at this time of year, I thought I’d clarify the difference between a title transfer and a prefix transfer, and the impact of each.

<!--more-->

When you join Crossref, you are provided with a unique prefix, you then add suffixes of your choice to your prefix and this creates the DOIs for your content.

<center><img src="/images/blog/DOI-structure.png" alt="Structure of a DOI directory suffix and prefix" width="550" class="img-responsive" /></center>

It’s a logical step then to assume you can tell just by looking at a DOI prefix who the current publisher is—but that’s not always the case. Things can (and often do) change. Individual journals get purchased by other publishers, and whole organizations get bought and sold.

What you can tell from looking at a DOI prefix is who originally registered it, but not necessarily who it currently belongs to. That’s because if a journal (or whole organization) is acquired, DOIs don’t get deleted and re-registered to the new owner. The update will of course be reflected in the relevant metadata, but the prefix itself will stay the same. It never changes—and that’s the whole point, that’s what makes the DOI persistent.

Here’s a breakdown of how this works internally at Crossref:

### Title transfers

Member A acquires a single title from member B. We transfer the title (and all relevant reports) over to member A. Member A must then register new content for that journal on their own prefix. The existing (newly acquired) DOIs maintain the ‘old’ prefix but member A can update metadata against these existing DOIs for that journal. Backfile and current DOIs for that journal may, therefore, have different prefixes—and that’s OK!

### Organization transfers

Member C acquires member D. We move the entire prefix (and all relevant reports) over to Member C, and close down Member D’s account with Crossref. Member C can continue to register DOIs on member D’s prefix (the original prefix) if they want to, or they can use their own existing prefix. So again, backfile and current DOIs for that journal may have different prefixes.

And by the way, if Member C uses a service provider to register metadata on their behalf, we will simply enable their username to work with the prefix.

### It’s now easier to transfer titles

We've recently made the process of [transferring journal titles](/blog/resolutions-2019-journal-title-transfers-metadata-manager/) a lot easier with our new Content Registration tool, [Metadata Manager](https://www.crossref.org/metadatamanager/).

___
