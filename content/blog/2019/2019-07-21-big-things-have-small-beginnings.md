---
title: 'Big things have small beginnings: the growth of the Open Funder Registry'
author: Kirsty Meddings
draft: false
authors:
  - Kirsty Meddings
date: 2019-07-21
categories:
  - Collaboration
  - Research Funders
  - Identifiers
  - Metadata
archives:
    - 2019
---

The [Open Funder Registry](/services/funder-registry/) plays a critical role in making sure that our members correctly identify the funding sources behind the research that they are publishing. It addresses a similar problem to the one that led to the creation of [ORCID](http://orcid.org): researchers' names are hard to disambiguate and are rarely unique; they get abbreviated, have spelling variations and change over time.

The same is true of organizations. You don’t have to read all that many papers to see authors acknowledge funding from the US National Institutes of Health as NIH, National Institutes for Health, National Institute of Health, etc. And wait, are you sure they didn’t mean National Institute for Health Research? (An entirely separate UK-based funder).

And a lot of countries have a National Science Foundation…

If each funder has a unique identifier, our members can [include it in the metadata](/education/funder-registry/) that they register with us, giving a clear and accurate link between the funder of the research and the published outcomes. And we can make that information available to everyone via our [API](/education/retrieve-metadata/rest-api/), and build [human interfaces](https://search.crossref.org/funding) so that you can look it up.

Many types of funding bodies are represented in the Funder Registry, from government agencies and large international foundations to small single-mission charities, and everything in between. As well as a unique DOI for each institution, the Registry contains additional metadata that can help to identify the funder such as country, abbreviated or alternate names, translated names, and so on.

The Registry also supports relationships between different funders. These can be hierarchical parent/child relationships for larger organizations, or connections between archival and current entries in instances where a funder has changed its name or become part of another body (to tell us about these kinds of changes you just need to [get in touch](mailto:funder.registry@crossref.org)).

The Registry was donated to Crossref by Elsevier when we first introduced funding information as part of our Content Registration schema back in 2012. We started out with a list of just over 4000 funders. Through an ongoing partnership the list has been - and continues to be -  updated on a monthly basis by Elsevier, and sent to Crossref as a formatted XML file that we process and release.

In return, Crossref sends Elsevier a feed of funder names that our members have registered with us that are not present in the Registry, which a team at Elsevier validates and adds to their databases, and then puts those newly-identified funders in to the next iteration of the list they send to us. It’s nice and circular and benefits both parties.

### We released [v1.27 of the Funder Registry](https://github.com/CrossRef/open-funder-registry) last week, and it contains entries for an impressive  21,356 funders.  

I’ve been involved in this project since its inception, and have enjoyed a productive and cooperative working relationship with the team at Elsevier, headed by Peter Berkvens (Senior Product Manager) and Paul Mostert (Director Product Management). I asked them to explain a little about the process from their side:

“Our team maintains a workflow in which Acknowledgement and Funding sections from articles are scanned for appearances of funding organizations using Natural Language Processing techniques. External Elsevier vendors then edit the data and add the validated names of the funders to what is called the Funding Bodies Taxonomy. The latter feeds Crossref’s Open Funder Registry.

Currently, the Taxonomy is nearing 22,000 Funders. It is expected it will grow to 25,000 Funders eventually. When this stage is reached, Elsevier believes that all existing Funders will be covered in the Funder Registry. Elsevier will continue to maintain the list adding new Funders as soon as they appear in scientific papers.

Elsevier’s Primary Articles production workflow for ScienceDirect uses the Funder Registry during the copyediting process, validating and tagging the Funders that appear in the accepted articles for Elsevier journals hosted by ScienceDirect. We then send the funder names and IDs to Crossref as part of our metadata.”    

Thanks to everyone involved for getting us ever-closer to a truly comprehensive list of funders.

And if you’re a member who’s not already registering funding information, why not look into [getting started?](/education/funder-registry/) It all leads to richer metadata which means more people can find, cite and re-use research -- and we all know that’s a [good thing](https://scholarlykitchen.sspnet.org/2019/06/11/better-metadata-could-help-save-the-world/)...
