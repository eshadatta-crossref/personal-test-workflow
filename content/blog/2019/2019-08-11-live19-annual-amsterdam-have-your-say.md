---
title: 'LIVE19, the strategy one: have your say'
author: Ginny Hendricks
draft: false
authors:
  - Ginny Hendricks
date: 2019-08-11
categories:
  - Community
  - Crossref LIVE
  - Annual Meeting
  - Member Briefing
  - Strategy
archives:
    - 2019
---

With a smaller group than usual, we're dedicating this year's annual meeting to hear what you value about Crossref. Which initiatives would you put first and/or last? Where would you have us draw the line between mission and ambition? What is “core” for you? How could/should we adapt for the future in order to meet your needs?
{{% divwrap align-right %}}<img src="/images/community-images/crossref-live-19-logo copy.jpg" alt="Crossref LIVE19 logo" width="200px" />{{% /divwrap %}}

## Striving for balance

Different people want different things from us. As Aristotle said: _"There is only one way to avoid criticism: do nothing, say nothing, and be nothing."_ As we prepare for our 20th year of operation, please join this unique meeting to help shape the future of Crossref.

There won't be any plenary talks about trends in scholarly communications, but instead workshop-style activities to help hone our strategy, do some scenario planning, and prioritize goals together, as a community.

## Have your say

Whether you can make it in person or not, you can still pitch in by giving us your opinion in advance. We're gathering broad input on what you think we're doing well, whether we're on the right track strategically, and how we can improve. There's never been such a comprehensive study of what value we offer so we hope to learn a lot and will adjust plans based on the results.
<br>
{{% divwrap blue-highlight %}}
Please take the "[Value of Crossref](https://www.surveygizmo.com/s3/5151355/blog)" survey. It'll take 10-12 minutes.
{{% /divwrap %}}

## At the meeting

Please join us at the Tobacco Theater in central Amsterdam on the afternoon of 13th November from 12:30 pm and for the full day of 14th November. The first afternoon will involve some scene-setting talks with key information you'll need for the following day's workshops, including the results of the survey above. There will also be some announcements, including who members have voted onto our board (this year's slate is yet to be communicated), and of course plenty of time for discussion and questions among peers.

In addition to the results of the survey, during the meeting each participant will be furnished with a 'fact pack' to reference in their discussions and recommendations. It will include answers to questions like `who pays to keep Crossref sustainable?`. I'm looking forward to busting some myths on that one! Everyone will be pre-assigned to a particular table/topic (like a wedding!) and will stay in those groups for roundtable discussions. There will be a community facilitator and a staff member on each table. You will be able to mingle more widely in the breaks and the evening drinks reception on the 13th.

Based on this provided data, we'll be asking participants to think about key questions such as:

- Who, ultimately, does Crossref serve?
- What should Crossref's product development priorities be?
- What (if anything) would be missed if Crossref went away? (i.e. what's our central value)
- What does 'community' really mean and how should Crossref work to better balance opposing priorities?

> Research is global, and supporting a diverse global community is a challenge. Come and have your say. [Register today](http://crossreflive19.eventbrite.com).

I can't wait to see you there and hear your thoughts.
