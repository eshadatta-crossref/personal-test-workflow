---
title: 'Resolutions 2019: Journal Title Transfers = Metadata Manager'
author: Isaac Farley
draft: false
authors:
  - Isaac Farley
date: 2019-01-03
categories:
  - Content Registration
  - Title transfers
  - Membership
  - Metadata
  - Identifiers
archives:
    - 2019
---
{{% divwrap blue-highlight %}}
UPDATE, 12 December 2022  
_Due to the scheduled [sunsetting of Metadata Manager](/blog/next-steps-for-content-registration/), this title transfer process has been deprecated.  Please find detailed guidance for transferring titles on our documentation site [here](/documentation/register-maintain-records/creating-and-managing-dois/transferring-responsibility-for-dois/)._
{{% /divwrap %}}

When you thought about your resolutions for 2019, Crossref probably didn’t cross your mind—but, maybe it should have...

<!--more-->

Because we know—with a high level of certainty—that [Shayn](/people/shayn-smulyan/), [Paul](/people/paul-davis/) and I will be spending the first few weeks of the year transferring the ownership of many journal titles. Last year we processed almost 60 journal transfer requests during this time, and we’re heading toward a similar number for 2019. There’s no objection; it’s a just a fact. We’re happy to do it, but there is another way.

Unlike previous years, we now have a tool that gives you the control to transfer titles without any intervention from the Crossref support team—[Metadata Manager](https://www.crossref.org/metadatamanager/). With just a few clicks, you, as the disposing publisher, can transfer your journal to the acquiring publisher yourself. Here’s how:

### Transferring your journal in five easy steps using Metadata Manager:

1)  Log into [Metadata Manager](https://www.crossref.org/metadatamanager/ ) using your username and password (the same one you use for the Crossref Web Deposit form).

<center><img src="/images/blog/mm-home.png" alt="metadata manager home screen" width="600" class="img-responsive" /></center>


2)  Find the journal you’re transferring on your Metadata Manager workspace using the “search publications” box and click to load the journal’s container (or, dashboard).

<center><img src="/images/blog/mm-journal.png" alt="select journal" width="600" class="img-responsive" /></center>


3)  Within the journal container, select **Transfer Title** from the **Action** drop-down.

<center><img src="/images/blog/mm-action.png" alt="action on drop down menu" width="600" class="img-responsive" /></center>


4)  On the transfer title screen select the acquiring (destination) publisher’s name and DOI prefix of where ownership will be transferred to. Click **Transfer**.

(In addition to transferring ownership of the title itself, all existing journal article DOIs previously registered will also be transferred to the new owner using this mechanism. They will persist on their original prefix, but the acquiring publisher will be able to update the metadata associated with these DOIs).

<center><img src="/images/blog/mm-transfer.png" alt="transfer to new owner" width="600" class="img-responsive" /></center>

5)  **Confirm** the title transfer. It may take up to 24 hours for the transfer to be reflected within Metadata Manager, and we’ll send a courtesy email to the acquiring (destination) publisher’s technical contact when the transfer has been completed.

<center><img src="/images/blog/mm-confirm.png" alt="confirm transfer" width="600" class="img-responsive" /></center>

As always, if you have questions, need guidance as you’re working through this process, or have recommendations on how we can improve title transfers—or anything else within Metadata Manager (the tool is in beta)–please let us know at [support@crossref.org](mailto:support@crossref.org ). There’s also comprehensive [support documentation](/education/member-setup/metadata-manager/) available for Metadata Manager to help and guide you.
