---
title: 'Rest in peace Christine Hone'
author: Crossref
draft: false
authors:
  - Crossref
date: 2019-06-09
categories:
  - Staff
archives:
    - 2019
---

_Our friend and colleague Christine Hone (née Buske) passed away in May from a short but brutal illness. Here is our attempt at 'some words', which we wrote for her funeral book and are posting here with her husband Dave's permission._

We are devastated to lose Christine as a colleague and friend. It’s hard to put into words the effect she had on our small organization in such a short time, and how much we’re already missing her. But here it goes.
<!--more-->

It was 2015 when some of us first met Chris, and we immediately saw how much of an asset she could be to our organization. She was very active in the community and well-known in many academic and publishing circles around the world. And she had an enviable combination of technical skills, a scientific mind, and a natural ability to engage people.

We tried to recruit her back then but she was in demand by others and it wasn’t until early 2018 that we succeeded. We finally got her! She became the Product Manager for a very advanced and complex system but she took to it perfectly, with real excitement and a complete understanding of how we (and therefore she) could help the research community all over the world see and make connections.

{{< figure src="/images/blog/2019/christine-headshot.jpg" caption="Christine's official Crossref headshot 😊" width="40%" >}}

With colleagues spread around the world, she joined an organization that had exciting opportunities and its share of challenges. Chris engaged with all of this head-on. She handled a constant stream of queries from people spread across time zones, whilst at the same time getting to grips with a service that was difficult to pin down. She balanced these tasks which were at very opposite ends of the spectrum. She added so much and with such energy and intelligence to everything she got involved in, always bringing human attention and creativity.

{{< figure src="/images/blog/2019/winners-chris-uksg-2018.jpg" caption="Chris was also on the winning team at 2018's UKSG quiz!" width="50%" >}} {{< figure src="/images/blog/2019/uksg-chris-2018.jpg" caption="Ed, Amanda, and Chris: the Crossref contingent of the winning quiz team" width="50%" >}}

In her talk at the 5:AM altmetrics conference she brought together technical detail, big-picture ideas, and her own particular passion. Her opening words were “My name is Christine and I’m a recovering fish scientist”. Never afraid to bring her personal brand of humour into the workplace, her opening slide was a photograph of her covered in rats. That presentation was the first time that much of the audience really understood our service. Having cracked the messaging for us, she was due to give the same talk at our annual meeting in Toronto a few months later…

{{< figure src="/images/blog/2019/5am-rats.png" caption="Chris's opening slide at her 5:AM talk" width="50%" >}} {{< figure src="/images/blog/2019/5am-chris.jpg" caption="Chris giving her now legendary talk at 5:AM on Event Data" width="50%" >}}

Many of us were in Toronto for that meeting; it was two weeks after we’d heard the news of her diagnosis. Some of us were able to visit her in the hospital where she told us of her and Dave’s decision to bring forward their wedding plans. It was a bittersweet announcement but, clearly, they adored each other and were determined to be happy together despite the challenging times ahead.

Over the last few months, even when she had little energy to spare, Chris popped in (virtually) to chat and update us, share pictures and, selflessly, to see how we were doing. Even people who never met or worked closely with her started to follow her vlog and exchange notes and news directly.

{{< figure src="/images/blog/2019/chris-message-may-9-2018.png" caption="Always checking in with us, an update from Chris shortly before she passed" width="60%" >}}

We have all been rocked by the news and there is a lot of sadness and grief among the Crossref staff and community. Even in the last moments we shared together Chris always asked about how her projects were going. Her passion for her products was a big part of what animated her when she first joined. Throughout her late-stage illness, this remained constant. She yearned to return to work. This zeal will forever be an inspiration to us all at Crossref.

[Christine](https://web.archive.org/web/20190609161008/https://www.crossref.org/people/christine-buske/) taught us a lot, through her work, with her attitude to life, and in [the manner that she dealt with this terrible illness](https://www.youtube.com/channel/UCeUvw-bWejaHH3bhaB6aEEg). We thank her for giving us so many great memories and we will never forget her.

{{< figure src="/images/blog/2019/chris4.png" caption="A Crossref photoshoot; our Christine ❤️" width="80%" >}}
