---
title: Many Metrics. Such Data. Wow.
author: Geoffrey Bilder
slug: "/many-metrics-such-data-wow"
authors:
  - Geoffrey Bilder
date: 2014-02-24

categories:
  - Altmetrics
  - Citation
  - Crossref Labs
  - Event Data
  - Interoperability
  - ORCID
  - Wikipedia
archives:
  - 2014

---
[<img class=" wp-image-302 alignnone" title="many metrics. such data. wow." src="/wp/blog/uploads/2014/02/many_metrics.jpg" alt="many_metrics" width="288" height="288" srcset="/wp/blog/uploads/2014/02/many_metrics.jpg 480w, /wp/blog/uploads/2014/02/many_metrics-150x150.jpg 150w, /wp/blog/uploads/2014/02/many_metrics-300x300.jpg 300w" sizes="(max-width: 288px) 85vw, 288px" />

> Crossref Labs loves to be the last to jump on an internet trend, so what better than than to combine the <a href="http://en.wikipedia.org/wiki/Doge_(meme)" target="_blank">Doge meme</a> with <a href="http://en.wikipedia.org/wiki/Altmetrics" target="_blank">altmetrics</a>?

**Note:** The API calls below have been superceeded with the development of the Event Data project. See [the latest API documentation](http://eventdata.crossref.org/) for equivalent functionality

Want to know how many times a Crossref DOI is cited by the Wikipedia?

```
http://det.labs.crossref.org/works/doi/10.1371/journal.pone.0086859
```

Or how many times one has been mentioned in Europe PubMed Central?

```
http://det.labs.crossref.org/works/doi/10.1016/j.neuropsychologia.2013.10.021
```

Or DataCite?

```
http://det.labs.crossref.org/works/doi/10.1111/jeb.12289
```

## Background


Back in 2011 <a href="http://www.plos.org/" target="_blank">PLOS</a> released its awesome <a href="https://web.archive.org/web/20190118175222if_/https://www.plos.org/article-level-metrics" target="_blank">ALM system</a> as <a href="http://en.wikipedia.org/wiki/Open-source_software" target="_blank">open source software</a> (OSS). At [Crossref Labs](/labs), we thought it might be interesting to see what would happen if we ran our own instance of the system and loaded it up with a few Crossref DOIs. So we did. And the code fell over. Oops. Somehow it didn’t like dealing with 10 million DOIs. Funny that.



But the beauty of OSS is that we were able to work with PLOS to scale the code to handle our volume of data. Crossref contracted with <a href="http://cottagelabs.com/" target="_blank">Cottage Labs</a>  and we both worked with PLOS to make changes to the system. These eventually got fed back into the main <a href="https://github.com/articlemetrics/alm/" target="_blank">ALM source on Github</a>. Now everybody benefits from our work. Yay for OSS.



So if you want to know technical details, skip to <a href="#details">Details for Propellerheads</a>. But if you want to know why we did this, and what we plan to do with it, read on.


## <span >Why?</span>

<p dir="ltr">
  <span >There are (cough) some problems in our industry that we can best solve with shared infrastructure. When publishers first put scholarly content online, they used to make bilateral reference linking agreements. These agreements allowed them to link citations using each other’s proprietary reference linking APIs. But this system didn’t scale. It was too time-consuming to negotiate all the agreements needed to link to other publishers. And linking through many proprietary citation APIs was too complex and too fragile. So the industry founded Crossref to create a common, cross-publisher citation linking API. Crossref has since obviated the need for bilateral linking arrangements.</span>
</p>

<p dir="ltr">
  <span >So-called <a href="http://en.wikipedia.org/wiki/Altmetrics" target="_blank">altmetrics</a> look like they might have similar characteristics. You have ~4000 Crossref member publishers and N sources (e.g. Twitter, Mendeley, Facebook, CiteULike, etc.) where people use (e.g. discuss, bookmark, annotate, etc.) scholarly publications. Publishers could conceivably each choose to run their own system to collect this information. But if they did, they would face the following problems:</span>
</p>

  * <span >The N sources will be volatile. New ones will emerge. Old ones will vanish.</span>
  * <span >Each publisher will need to deal with each source’s different APIs, rate limits, T&Cs, data licenses, etc. This is a logistical headache for both the publishers and for the sources.</span>
  * <span >If publishers use different systems which in turn look at different sources, it will be difficult to compare results across publishers.</span>
  * <span >If a journal moves from one publisher to another, then how are the metrics for that journal’s articles going to follow the journal? This isn’t a complete list, but it shows that there might be some virtue in publishers sharing an infrastructure for collecting this data. But what about commercial providers? Couldn’t they provide these ALM services? Of course - and some of them currently do. But normally they look on the actual collection of this data as a means to an end. The real value they provide is in the analysis, reporting and tools that they build on top of the data. Crossref has no interest in building front-ends to this data. If there is a role for us to play here, it is simply in the collection and distribution of the data.</span>

## <span >No, really, WHY?</span>

<p dir="ltr">
  <span >Aren’t these altmetrics <a href="https://web.archive.org/web/20170112105521/https://scholarlyoa.com/2013/08/01/article-level-metrics/" target="_blank">an ill-conceived and meretricious idea</a>? By providing this kind of information, isn’t Crossref just encouraging feckless, <a href="http://blogs.lse.ac.uk/impactofsocialsciences/2014/01/27/its-the-neoliberalism-stupid-kansa/" target="_blank">neoliberal university administrators</a> to hasten academia’s slide into a <a href="http://en.wikipedia.org/wiki/Stakhanovite_movement" target="_blank">Stakhanovite</a> dystopia? Can’t these systems be gamed?</span>
</p>

<p dir="ltr">
  <span >FOR THE LOVE OF <a href="http://en.wikipedia.org/wiki/Flying_Spaghetti_Monster" target="_blank">FSM</a>, WHY IS CROSSREF DABBLING IN SOMETHING OF SUCH QUESTIONABLE VALUE?</span>
</p>

<p dir="ltr">
  <span >takes deep breath. wipes spittle from beard</span>
</p>

<p dir="ltr">
  <span >These are all serious concerns. <a href="http://en.wikipedia.org/wiki/Goodhart's_law" target="_blank">Goodhart’s Law</a> and all that… If a university’s appointments and promotion committee is largely swayed by <a href="http://en.wikipedia.org/wiki/Impact_factor" target="_blank">Impact Factor</a>, it won’t improve a thing if they substitute or supplement Impact Factor with altmetrics. <a href="http://www.linkedin.com/profile/view?id=8488638&authType=NAME_SEARCH&authToken=6zaC&locale=en_US&srchid=4700671392208272787&srchindex=1&srchtotal=32&trk=vsrp_people_res_name&trkInfo=VSRPsearchId%3A4700671392208272787%2CVSRPtargetId%3A8488638%2CVSRPcmpt%3Aprimary" target="_blank">Amy Brand</a> has repeatedly pointed out, <a href="http://article-level-metrics.plos.org/files/2013/10/Brand.pptx" target="_blank">the best institutions simply don’t use metrics this way at all</a> (PowerPoint presentation). They know better.</span>
</p>

<p dir="ltr">
  <span >But yes, it is still likely that some powerful people will come to lazy conclusions based on altmetrics. And following that, other lazy, unscrupulous and opportunistic people will attempt to game said metrics. We may even see an industry emerge to exploit this mess and provide the scholarly equivalent of <a href="http://en.wikipedia.org/wiki/Search_engine_optimization" target="_blank">SEO</a>. Feh. Now I’m depressed and I need a drink.</span>
</p>

<p dir="ltr">
  <span >So again, why is Crossref doing this? Though we have our doubts about how effective altmetrics will be in evaluating the quality of content, we do believe that they are a useful tool for understanding how scholarly content is used and interpreted. <em>The most eloquent arguments against altmetrics for measuring quality, inadvertently make the case for altmetrics as a tool for monitoring attention.</em></span>
</p>

<p dir="ltr">
  <span >Critics of altmetrics point out that much of the attention that research receives outside of formal scholarly communications channels can be ascribed to:</span>
</p>

  * <span >Puffery. Researchers and/or university/publisher “<a href="http://www.dcscience.net/?p=6369" target="_blank">PR wonks</a>” over-promoting research results.</span>
  * <span >Innocent misinterpretation. A lay audience simply doesn’t understand the research results.</span>
  * <span >Deliberate misinterpretation. Ideologues misrepresent research results to support their agendas.</span>
  * <span >Salaciousness. The research appears to be about sex, drugs, crime, video games or other popular bogeymen.</span>
  * <span >Neurobollocks. <a href="https://web.archive.org/web/20160405135736/http://www.wired.co.uk/news/archive/2012-11/08/neurobollocks" target="_blank">A category unto itself these days</a>.</span>

<p dir="ltr">
  <span >In short, scholarly research might be misinterpreted. Shock horror. Ban all metrics. Whew. That won’t happen again.</span>
</p>

<p dir="ltr">
  <span >Scholarly research has always been discussed outside of formal scholarly venues. Both by scholars themselves and by interested laity. Sometimes these discussions advance the scientific cause. Sometimes they undermine it. The University of Utah didn’t depend on widespread Internet access or social networks to promote <a href="http://en.wikipedia.org/wiki/Cold_fusion" target="_blank">yet-to-be peer-reviewed claims about cold fusion</a>. That was just old-fashioned analogue puffery. And the Internet played no role in the Laetrile or<a href="http://www.cancer.org/treatment/treatmentsandsideeffects/complementaryandalternativemedicine/pharmacologicalandbiologicaltreatment/dmso" target="_blank"> DMSO crazes of the 1980s</a>. You see, there were once these things called “<a href="http://en.wikipedia.org/wiki/Newspaper" target="_blank">newspapers.</a>” And another thing called “<a href="http://en.wikipedia.org/wiki/Television" target="_blank">television.</a>” And a sophisticated <a href="http://www.urbandictionary.com/define.php?term=meatspace" target="_blank">meatspace</a>-based social network called a “<a href="http://en.wikipedia.org/wiki/Town_square" target="_blank">town square</a>.”</span>
</p>

<p dir="ltr">
  <span >But there are critical differences between then and now. As <a href="https://obamawhitehouse.archives.gov/blog/2013/02/22/expanding-public-access-results-federally-funded-research" target="_blank">citizens get more access to the scholarly literature</a>, it is far more likely that research is going to be discussed outside of formal scholarly venues. Now we can build tools to help researchers track these discussions. Now researchers can, if they need to, engage in the conversations as well. One would think that conscientious researchers would see it as their responsibility to remain engaged, to know how their research is being used. And especially to know when it is being misused.</span>
</p>

<p dir="ltr">
  <span >That isn’t to say that we expect researchers will welcome this task. We are no Pollyannas. Researchers are already famously overstretched. They <a href="https://ddoi.org/10.1016/j.lisr.2009.02.002" target="_blank">barely have time to keep up with the formally published literature</a>. It seems cruel to expect them to keep up with the firehose of the Internet as well.</span>
</p>

<p dir="ltr">
  <span >Which gets us back to the value of altmetrics tools. Our hope is that, as altmetrics tools evolve, they will provide publishers and researchers with an efficient mechanism for monitoring the use of their content in non-traditional venues. Just in the way that citations were used before they were distorted into proxies for credit and kudos.</span>
</p>

<p dir="ltr">
  <span >We don’t think altmetrics are there yet. Partly because some parties are still tantalized by the prospect of usurping one metric for another. But mostly because the entire field is still nascent. People don’t yet know how the information can be combined and used effectively. So we still make naive assumptions such as “link=like” and “more=better.” Surely it will eventually occur to somebody that, instead, there may be a connection between <a href="http://www.nytimes.com/2013/04/28/magazine/diederik-stapels-audacious-academic-fraud.html?_r=1&" target="_blank">repeated headline-grabbing research and academic fraud</a>. A neuroscientist might be interested in a tool that alerts them if the MRI scans in their research paper are being misinterpreted on the web to promote neurobollocks. An immunologist may want to know if their research is being misused by the anti-vaccination movement. Perhaps the real value in gathering this data will be seen when somebody builds tools to help researchers DETECT puffery, social-citation cabals, and misinterpretation of research results?</span>
</p>

<p dir="ltr">
  <span >But Crossref won’t be building those tools. What we might be able to do is help others overcome another hurdle that blocks the development of more sophisticated tools; getting hold of the needed data in the first place. This is why we are dabbling in altmetrics.</span>
</p>

<p dir="ltr">
  <span >Wikipedia is already the 8th largest referrer of Crossref DOIs. Note that this doesn’t just mean that the Wikipedia cites lots of Crossref DOIs, it means that people actually click on and follow those DOIs to the scholarly literature. As scholarly communication transcends traditional outlets and as the audience for scholarly research broadens, we think that it will be more important for publishers and researcher to be aware of how their research is being discussed and used. They may even need to engage more with non-scholarly audiences. In order to do this, they need to be aware of the conversations. Crossref is providing this experimental data source in the hope that we can spur the development of more sophisticated tools for detecting and analyzing these conversations. Thankfully, this is an inexpensive experiment to conduct - largely thanks to the decision on the part of PLOS to open source its ALM code.</span>
</p>

## What Now?

<p dir="ltr">
  Crossref’s instance of PLOS’s ALM code is an experiment. We mentioned that we had encountered scalability problems and that we had resolved some of them. But there are still big scalability issues to address. For example, assuming a response time of 1 second, if we wanted to poll the English-language version of the Wikipedia to see what had cited each of the 65 million DOIs held in Crossref, the process would take years to complete. But this is how the system is designed to work at the moment.<span > It polls various source APIs to see if a particular DOI is “mentioned”. Parallelizing the queries might reduce the amount of time it takes to poll the Wikipedia, but it doesn’t reduce the work. Another obvious way in which we could improve the scalability of the system is to add a push mechanism to supplement the pull mechanism. Instead of going out and polling the Wikipedia 65 million times, we could establish a &#8220;scholarly <a href="http://en.wikipedia.org/wiki/Linkback" target="_blank">linkback</a>” mechanism that would allow third parties to alert us when DOIs and other scholarly identifiers are referenced (e.g. cited, bookmarked, shared). If the Wikipedia used this, then even in an extreme case scenario (i.e. everything in Wikipedia cites at least one Crossref DOI), this would mean that we would only need to process ~ 4 million trackbacks.</span>
</p>

<p dir="ltr">
  <span >The other significant advantage of adding a push API is that it would take the burden off of Crossref to know what sources we want to poll. At the moment, if a new source comes online, we’d need to know about it and build a custom plugin to poll their data. This needlessly disadvantages new tools and services as it means that their data will not be gathered until they are big enough for us to pay attention to. If the service in question addresses a niche of the scholarly ecosystem, they may never become big enough. But if we allow sources to push data to us using a common infrastructure, then new sources do not need to wait for us to take notice before they can participate in the system.</span>
</p>

<p dir="ltr">
  <span >Supporting (potentially) many new sources will raise another technical issue- tracking and maintaining the provenance of the data that we gather. The current ALM system does a pretty good job of keeping data, but if we ever want third parties to be able to rely on the system, we probably need to extend the provenance information so that the data is cheaply and easily auditable.</span>
</p>

<p dir="ltr">
  <span >Perhaps the most important thing we want to learn from running this experimental ALM instance is: what it would take to run the system as a production service? What technical resources would it require? How could they be supported? And from this we hope to gain enough information to decide whether the service is worth running and, if so, by whom. Crossref is just one of several organizations that could run such a service, but it is not clear if it would be the best one. We hope that as we work with PLOS, our members and the rest of the scholarly community, we’ll get a better idea of how such a service should be governed and sustained.</span>
</p>

## <span >Details for Propellerheads</span> {#details}

<h3 dir="ltr">
  <span >Warning, Caveats and Weasel Words</span>
</h3>

<p dir="ltr">
  <span >The Crossref ALM instance is a <a href="http://labs.crossref.org" target="_blank">Crossref Labs</a> project. It is running on R&D equipment in a non-production environment administered by an orangutang on a diet of Redbulls and vodka.</span>
</p>

<h3 dir="ltr">
  <span >So what is working?</span>
</h3>

<p dir="ltr">
  <span >The system has been initially loaded with 317,500+  Crossref DOIs representing publications from 2014. We will load more DOIs in reverse chronological order until we get bored or until the system falls over again.</span>
</p>

<p dir="ltr">
  <span >We have activated the following sources:</span>
</p>


<li dir="ltr">
  <span >PubMed</span>
</li>
<li dir="ltr">
  <span >DataCite</span>
</li>
<li dir="ltr">
  <span >PubMedCentral Europe Citations and Usage</span>
</li>


<p dir="ltr">
  <span >We have data from the following sources but will need some work to achieve stability:</span>
</p>


<li dir="ltr">
  <span >Facebook</span>
</li>
<li dir="ltr">
  <span >Wikipedia</span>
</li>
<li dir="ltr">
  <span >CiteULike</span>
</li>
<li dir="ltr">
  <span >Twitter</span>
</li>
<li dir="ltr">
  <span >Reddit</span>
</li>

<p dir="ltr">
  <span >Some of them are faster than others. Some are more temperamental than others. WordPress, for example, seems to go into a sulk and shut itself off  after approximately 1,300 API calls.</span>
</p>

<p dir="ltr">
  <span >In any case, we will be monitoring and tweaking the sources as we gather data. We will also add new sources as we get requested API keys. We will probably even create one or two new sources ourselves. Watch this blog and we’ll update you as we add/tweak sources.</span>
</p>

<h3 dir="ltr">
  <span >Dammit, shut up already and tell me how to query stuff.</span>
</h3>

<p dir="ltr">
  <span >You can <a href="#" target="_blank">login to the Crossref ALM instance</a> simply using a <a href="" target="_blank">Mozilla Persona</a> (yes, we’d eventually like to support ORCID too). Once logged-in, <a href="" target="_blank">your account page</a> will list an API key. Using the API key, you can do things like:</span>
</p>

```
http://det.labs.crossref.org/api/v5/articles?ids=10.1038/nature12990
```

<span >And you will see that (as of this writing), said Nature article has been cited by the Wikipedia article here:</span>

<span ><code><a href="http://en.wikipedia.org/wiki/HE0107-5240">https://en.wikipedia.org/wiki/HE0107-5240#cite_ref-Keller2014_4-0;</code></span>

<p dir="ltr">
  <span >PLOS has provided <a href="#" target="_blank"> lovely detailed instructions for using the API</a>- <span >So, please, play with the API and see what you make of it. On our side we will be looking at how we can improve performance and expand coverage. We don’t promise much- the logistics here are formidable. As we said above, once you start working with millions of documents, the polling process starts to hit API walls quickly. But that is all part of the experiment. We appreciate your helping us and would like your feedback. We can be contacted at:</span></span>
</p>

[<img class="alignnone size-full wp-image-261" src="/wp/blog/uploads/2013/01/labs_email.png" alt="labs_email" width="233" height="42" />][2]

 [1]: /wp/blog/uploads/2014/02/many_metrics.jpg
 [2]: /wp/blog/uploads/2013/01/labs_email.png
