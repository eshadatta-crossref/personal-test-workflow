+++
date = "2019-06-25"
title = "Subscribe to Metadata Plus "
type = "retrieve"
+++

## Before you start
Make sure you’re in the right place. This page is for those of you who wish to retrieve and make use of the metadata that has been registered with Crossref using the paid-for [Metadata Plus service](/services/metadata-retrieval/metadata-plus/).

If you’d actually like to join our organization in order to register DOIs with us and (deposit your own metadata), visit our [membership page](/membership/).

In the right place? Great, please continue.