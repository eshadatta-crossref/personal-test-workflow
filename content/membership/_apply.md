+++
title = "Apply for membership"
date = "2021-05-10"
draft = false
author = "Amanda Bartell"
parent = "Become a member"
Rank = 4
weight = 3
+++

## Ready to apply? What you will need

Make sure you’re in the right place. This application form is for those of you who wish to join Crossref independently as a member, in order to register content and DOIs and deposit metadata relating to that content.

(If you wish to join Crossref through a Sponsor instead, you'll need to fill out a different form provided by your sponsor - please request the link from your sponsor. And if you don't want to register content but instead are interested in retrieving and making use of the metadata that’s been registered with Crossref, then you don't need to become a member. Please visit our [metadata retrieval](/education/retrieve-metadata/) page for more information.)

There are a few steps to joining and it usually takes a week or so to complete the process, depending on how many extra questions we have to ask you. Make sure you have all the information you need before you start. We’re going to ask you lots of important questions so we can get you set up as quickly as possible. It will save a lot of time if you can put the right information into the form now rather than having to change it later:
- **Your organization name** - you should apply to join in the name of your parent publishing organization, rather than in the name of your individual journal, conference, imprint or university department. Your organization will then be able to use this single member account to register DOIs for all the content that your organization publishes and only pay one membership fee.
- **Your mailing address and your billing address** - some of you will have a different address for invoices, and it’s important that we collect the correct address now. The billing address you provide in this application form is what will appear on the invoice we send you for your first year of membership as part of this application process. Before you apply, check what this should be, and also whether your finance team needs other details quoted on the invoice (for example, your tax number).

- **The publishing revenue of your organization** - we don’t need to know exact amounts, but we use this information to decide which tier of our annual membership fee you need to pay. We only need to know about the revenue for the part of your organization that deals with publishing.  if you don’t have publishing revenue, we look at publishing expenditure. Our [annual membership tiers](/fees) start at US$275 for organizations with revenue under USD 1 million per year and go up to to USD 50,000.00 for organizations with revenue over USD 500 million.

- **Contact details for three separate people at your organization** - we ask you to provide contact details (name and email address) for a range of different contact types at your organization. Make sure you have at least three separate contacts ready to cover these five roles. (If you are a very small organization and don't have at least three different names and email addresses to cover these five roles, please [contact our membership team](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001618152) to discuss the best options.)  
    - The **Primary** contact - this person will be our key contact at your organization. They receive product and service updates, and we contact them about things like changes to terms or service agreements. They also receive our monthly [resolution reports](/documentation/reports/resolution-report) showing failed resolutions on your DOIs.
    - The **Voting** contact - this person will vote in our Board elections. The Voting contact is often the same person as the Primary contact.  
    - The **Technical** contact - this person will receive technical updates, [DOI error reports](/documentation/reports/doi-error-report), and [conflict reports](/documentation/reports/conflict-report) to help you solve problems with your content quickly. We encourage you to use a shared, generic email address for this contact.  
    - The **Metadata quality** contact - this person will be responsible for fixing any metadata errors that are spotted by the scholarly community. The Metadata Quality contact is often the same person as the Technical contact.    
    - The **Billing contact** - this person will receive invoices from us and pay the [annual membership and ongoing content registration fees](/fees). They will also receive reminder emails about unpaid invoices. We encourage you to use a shared, generic email address for this contact.  

- **How you plan to register your content with us** - it's useful to know how you plan to register your content with us before you join. You can [find out more](/documentation/content-registration/choose-content-registration-method/) here.

## What happens next?

{{< snippet "/_snippet-sources/application-process.md" >}}
{{< snippet "/_snippet-sources/email-address.md" >}}

## Global Equitable Membership applicants
If you are eligible for the [GEM program](/gem), we will not send you a membership order for your first year of membership - you will not be charged membership fees or any content registration fees. On the application form, please just just add your mailing address into the billing address field. Please do still add a contact into the billing contact field. We will only need to use this if you take on a paid service (such as Similarity Check) in the future.

{{< joinlink >}}

---

Please contact our [membership specialist](mailto:member@crossref.org) with any questions.
