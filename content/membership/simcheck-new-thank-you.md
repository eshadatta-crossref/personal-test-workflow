+++
title = "Thank you for your application"
date = "2021-11-03"
draft = false
image = "/images/banner-images/speed-boat.jpg"
slug = "simcheck-new-thank-you"
+++

## Thanks for applying for the Similarity Check service.

Once we've confirmed that you meet the obligations of having full-text URLs supplied in over 90% of your registered content, we'll work with the team at Turnitin to check that they can index your content. If they have any problems, they'll contact the technical contact you have just supplied.

Once your content has been indexed, Turnitin will provide you with your login details for the iThenticate system, and we send you more details to help you get underway.

---
