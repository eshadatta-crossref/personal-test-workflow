+++
title = "Thank you for your application"
date = "2018-10-06"
type = 'join-thank-you'
draft = false
image = "/images/banner-images/speed-boat.jpg"
slug = "join-thank-you"
+++

## Thanks for applying - you're on your way!

### Here's what happens next:

We'll take a look at your application within two working days. We may need to come back to you with a couple of questions. We then send you a pro-rated membership order (this is basically an invoice) for your first year of membership.  

Once our billing team confirms that this has been paid, we'll send you your new Crossref DOI prefix and login details within two working days. Once you've received these, you can start registering your content straight away. Don't forget that the membership order only covers your membership - there are also charges for the content you register, which will be invoiced quarterly in arrears.

Please note: If you are eligible for the [GEM programme](/gem), we won’t send you a membership order when you first join, and we won't send you any further membership invoices either. You also won’t receive invoices for content registration.

### Important
Our replies come from the email address member@crossref.org. Please add this email address to your contacts list, whitelist it or add it to your safe senders list to make sure that you receive our replies.

### Please make sure you have read and understood:
* The [payment schedule](/fees) you're agreeing to
* The [member terms](/membership/terms) you're agreeing to

If you or your billing team has more questions about the future billing schedule, you can [read more](/members-area/all-about-invoicing-and-payment/) on our website.

We're looking forward to your participation in our community!

---
Please contact our [membership team](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001618152) if you have any questions in the meantime.
