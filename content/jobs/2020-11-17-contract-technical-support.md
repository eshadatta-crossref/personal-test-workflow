+++
title = "Technical Support Contractor"
author = "Isaac Farley"
date = 2020-11-17
draft = false
image = "/images/banner-images/jobs-robots.jpg"
weight = 3
parent = "Jobs"
+++

{{% divwrap red-highlight %}} Applications for this position are closed. {{% /divwrap %}}

## Request for services: Technical Support Contractor

**Location:** Remote  
**Closing date:** Wednesday 2021 May 12 <br>

## About the role
The Technical Support Contractor will work closely with our Member Experience team, part of Crossref’s Outreach team, a fifteen-strong distributed team with members across the US and Europe. We’re at the forefront of Crossref’s growth, building relationships with new communities in new markets in new ways. We’re aiming for a more open approach to having conversations with people all around the world - including within our growing [community forum](https://community.crossref.org/), which the right candidate will help us expand, in multiple languages. We’re looking for a Technical Support Contractor to provide front-line help to our international community of publishers, librarians, funders, researchers and developers on a range of services that help them deposit, find, link, cite, and assess scholarly content.

We’re looking for contractors able to work remotely. There is no set schedule and contractors bill their hours monthly.

## Key responsibilities
Replying to and solving community queries using the Zendesk support system.
Using our various tools and APIs to find the answers to these queries, or pointing users to support materials that will help them.
Working with colleagues on particularly tricky tickets, escalating as necessary.
Working efficiently but also kindly and with empathy with our very diverse, global community.
This position is for an independent contractor.
## About the team
You’ll be working closely with six other technical and membership support colleagues to provide support and guidance for people with a wide range of technical experience. You’ll help our community create and retrieve metadata records with tools ranging from simple user interfaces to robust APIs.
## About Crossref
Crossref makes research objects easy to find, cite, link, assess, and reuse. We’re a not-for-profit membership organization that exists to make scholarly communications better. We rally the community; tag and share metadata; run an open infrastructure; play with technology; and make tools and services—all to help put research in context. It’s as simple—and as complicated—as that.
Since January 2000 we have grown from strength to strength and now have over 15,000 members across 140 countries, and thousands of tools and services relying on our metadata.

---

## To apply

Please send a cover letter, and your resume to: [jobs@crossref.org](mailto:jobs@crossref.org).

Crossref is committed to a policy of non-discrimination and equal opportunity for all employees and qualified applicants for employment without regard to race, color, religion, sex, pregnancy or a condition related to pregnancy, sexual orientation, gender identity or expression, national origin, ancestry, age, physical or mental disability, genetic information, veteran status, uniform service member status, or any other protected class under applicable law. Crossref will make reasonable accommodations for qualified individuals with known disabilities, in accordance with applicable law.
