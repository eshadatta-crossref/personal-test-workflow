+++
title = "Senior Front-end Developer"
date = 2020-11-26
draft = false
image = "/images/banner-images/jobs-robots.jpg"
author = "Joe Wass"
weight = 3
parent = "Jobs"
+++


{{% divwrap red-highlight %}} Applications for this position are closed {{% /divwrap %}}



## About the role

You will play a key role in the software development team, being the technical lead for the front-end components of a number of services that are critical to thousands of publishers in the global scholarly community. You will design and build infrastructure that serves our newer smaller members as well as large high-volume publishers.

You will contribute to our **JavaScript** and **Java** codebases, building new functionality using **Vue.js** and maintaining our **React.js** codebase.

As a technical lead on our front-end you will collaborate with the Product and Infrastructure teams to specify, design and implement our new features and services as part of our full stack. You will have a key voice in discussions about technical approaches to front-end architecture and how they relate to our back-end, infrastructure and operational considerations. You will always keep an eye on software quality and ensure that the code you and your colleagues produce is maintainable, well tested and of high standard.

## Key responsibilities

 - Understand Crossref’s mission, our role in the scholarly community, and how we support it with our services.
 - Understand and help to guide our data models, and translate them into usable, accessible interfaces.
 - Pursue continuous improvement and quality.
 - Work flexibly in multi-functional project teams to design and develop services and ensure that our systems are reliable, responsive, and efficient.
 - Work closely with the Head of Software Development to solve strategic problems, maintain and improve our services and execute technology changes.
 - Help to guide our legacy front-end migration strategy.
 - Maintain expertise in front-end technologies.
 - Provide code reviews and guidance to other developers regarding development practices and help maintain and improve our development environment.
 - Identify vulnerabilities and inefficiencies in our system architecture and processes, particularly regarding cloud operations, metrics and testing.
 - Communicate proactively with membership and technical support colleagues ensuring they have all the information and tools required to serve our users.
 - Openly document and share development plans and workflow changes.
 - Be an escalation point for technical support; investigate and respond to occasional but complex user issues; help minimize support demands related to our systems; be part of our on-call team responding to service outages.

## About you

We don't expect a successful candidate to tick all of these boxes right away!

 - An interest in scholarly communication and open infrastructure.
 - An expert senior developer with experience in JavaScript and other front-end technologies, preferably Vue.js, and have a proven track record of picking up new technologies.
 - Familiarity with Java-based back-end technology.
 - A focus on the particular needs of our diverse users.
 - Experienced with continuous integration, testing and delivery frameworks, and cloud operations concepts and techniques.
 - Experience with Python, preferably including Jupyter notebooks.
 - Experience with static site generators such as Hugo.
 - Experience working on open source code.
 - Familiar with AWS, Docker and infrastructure management using Terraform.
 - Able to quickly pick up, understand and improve legacy code.
 - Self-directed, a good manager of your own time, with the ability to focus.
 - Curious and tenacious at learning new things and getting to the bottom of problems.
 - Strong at written and verbal communication skills, able to communicate clearly, simply, and effectively.
 - Outstanding at interpersonal relations and relationship management. Comfortable collaborating with colleagues across the organisation.
 - Assuming that international travel ever becomes possible again, the applicant should expect they will need to travel internationally to work with colleagues for about 5-10 days a year.  


You can find more about our latest plans from our recent LIVE Annual event: [https://doi.org/10.13003/5gq8v1q](https://doi.org/10.13003/5gq8v1q).

This position is full time and, as for all Crossref employees, location is flexible - you can work remotely with a 2 to 3-hour overlap with UTC-0. We provide a competitive benefits package.

## About the team

Our colleagues are spread across Europe and North America. The software development team can be found in the US east-coast, the UK, Ireland and France.

We build and maintain services for the Crossref community. Our DOI registration, metadata pipeline, reference matching, search and querying play a part in the operations of 15,000 members, who have registered the metadata for over 100 million content items. Our systems have evolved over our 20 year history, and we're continuing to proactively update them. New code and services are written in modern Java, Clojure and JavaScript and run in AWS, making use of Kafka and Elastic Search.

Issue tracking and all new code is open source. We strongly believe in open scholarly infrastructure and openness at all stages of the software development lifecycle. As a membership organization we keep closely in touch with our users, and encourage our developers to be familiar with our community. The Development, Product and Infrastructure teams are tightly knit and we work in 2 week sprints.

## About Crossref

Crossref makes research objects easy to find, cite, link, assess, and reuse. We’re a not-for-profit membership organization that exists to make scholarly communications better. We rally the community; tag and share metadata; run an open infrastructure; play with technology; and make tools and services—all to help put research in context. It’s as simple—and as complicated—as that.

Since January 2000 we have grown from strength to strength and now have over 12,000 members across 120 countries and thousands of tools and services relying on our metadata.  

Crossref is committed to a policy of non-discrimination and equal opportunity for all employees and qualified applicants for employment without regard to race, color, religion, sex, pregnancy or a condition related to pregnancy, sexual orientation, gender identity or expression, national origin, ancestry, age, physical or mental disability, genetic information, veteran status, uniform service member status, or any other protected class under applicable law. Crossref will make reasonable accommodations for qualified individuals with known disabilities, in accordance with applicable law.  
