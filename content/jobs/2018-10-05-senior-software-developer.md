+++
title = "Senior Software Developer"
date = 2018-10-05
draft = false
image = "/images/banner-images/jobs-robots.jpg"
author = "Chuck Koscher"
weight = 3
rank = 4
parent = "Jobs"
+++

{{% divwrap red-highlight %}} Applications for this position are closed {{% /divwrap %}}

## Come and work with us as our **Senior Software Developer**. It’ll be fun!

- **Location:** Flexible. Either office-based (Lynnfield, MA or Oxford, UK) or remote/home-based in North America or Europe
- **Reports to:** Chuck Koscher, Director of Technology
- **Salary and benefits:** Competitive

## About the role

This role provides a technology cornerstone to our development team responsible for the ongoing development, maintenance, and operation of our Content Registration system. Metadata is critically important to Crossref’s mission to make research communications better, and the registration system is where it all starts. You will report to the director of technology, joining a team of three developers and one system administrator. You will also work extensively with all other teams across our small but impactful organization.

Our stack is mainly a backend system written in Java on Spring and utilizing SQL on MySQL and Oracle. We’re still running our own hardware but are moving to AWS and already use S3, RDS, and other Amazon services. You will be responsible for ensuring that the Content Registration system is reliable and responsive as well as making sure it is able to evolve quickly to support the new requirements and new services that we are developing for its membership and metadata subscribers. As such, you will need to work closely with product management and the strategic initiatives teams.

This position also provides programming and workflow guidance to the entire team by guiding concept formulation, design, and implementation. Our processes are built on Jira, SVN, Zendesk, and Git and we’re starting to use agile methods. We communicate via Slack and Google apps. You will help improve our quality control initiatives, review methodologies and help develop a culture of continuous testing and deployment.

> Your challenge will be to accomplish this, whilst simultaneously driving the modernization of our current software stack, infrastructure, and software engineering culture.

## Key responsibilities

- Understand Crossref’s mission and how it that applies to the Content Registration service.
- Work in multi-functional project teams to scope, specify design and develop services and ensure that the Content Registration system is reliable, responsive, and efficient.
- Work very closely with the Director of Technology to solve problems, maintain and improve the registration service.
- Recommend and execute technology changes, for example upgrading to Java 8, or other tools and off-the-shelf solutions that might improve operations, visibility or monitoring.
- Provide guidance to other developers regarding coding practices and help maintain and improve our development environment.
- Identify vulnerabilities and inefficiencies in our system architecture and development processes, particularly regarding DevOps procedures, unit and regression testing.
- Communicate proactively with membership and technical support colleagues ensuring they have all the information and tools required to serve our users.
Openly document and share development plans and workflow changes.
- Be an escalation point for technical support; investigate and respond to occasional but complex user issues; help minimize support demands related to the Content Registration system; be part of our on-call team responding to service outages.

## About you

You are:

- An expert Java developer with a solid understanding of Spring and with a lot of SQL and MySQL experience at the application level and with infrastructure level issues dealing with JDBC, connection pooling, table optimization, index construction, charset, and driver issues.
- Proficient in one other language and expert scripting skills.
- Experienced with full backend stack (Java, Spring, MySQL, Tomcat) continuous testing/delivery frameworks, and DevOps concepts and techniques.
- Experience with or a working understanding of XML and document-oriented systems such as MongoDB, Solr, and Elasticsearch.
- Experience with AWS services, containerization with tools like Docker and infrastructure management using tools like Terraform.
- Very much self- directed, must be a good manager of your own time and have ability to focus even when other things compete for your time.
- Curious and tenacious at learning new things and getting to the bottom of problems.
- Strong at written and verbal communication skills, able to communicate clearly, simply, and effectively.
- Outstanding at interpersonal relations and relationship management, and comfortable working with other developers, product management, outreach, membership, and technical support teams.
- If remote, able to travel occasionally to meet with colleagues at either Lynnfield MA or Oxford UK office.

## About Crossref

Crossref makes research objects easy to find, cite, link, assess, and reuse. We’re a not-for-profit membership organization that exists to make scholarly communications better. We rally the community; tag and share metadata; run an open infrastructure; play with technology; and make tools and services—all to help put research in context. It’s as simple—and as complicated—as that.

Since January 2000 we have grown from strength to strength and now have over 11,000 members across 118 countries, and thousands of tools and services relying on our metadata.

---

## To apply

If you’d like to join the Crossref tea, and contribute to [our mission](/community/about), please send a cover letter and your resume to [jobs@crossref.org](mailto:jobs@crossref.org). We can't wait to read all about you!
