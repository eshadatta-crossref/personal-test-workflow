+++
title = "Director of Product"
date = 2019-05-23
draft = false
image = "/images/banner-images/jobs-robots.jpg"
author = "Lindsay Russell"
weight = 3
rank = 4
parent = "Jobs"
+++

{{% divwrap red-highlight %}} Applications for this position closed on 10th June 2019. {{% /divwrap %}}

## Come and work with us as our **Director of Product**. It’ll be fun!

**Location:** Flexible - Crossref has offices in Oxford, UK and Lynnfield, MA, USA but being office-based is not a requirement.  

**Reports to:** Ed Pentz, Executive Director

**Salary and benefits:** Competitive

## About the role

The Director of Product is a key member of the senior leadership team at Crossref and responsible for the strategy, delivery, adoption, and success of the Crossref service. The role centers around a deep understanding of community needs balanced with technical credibility and credentials.  

## Key responsibilities

The director manages and leads the product team (3 Product Managers, 1 UX Designer), planning and managing the development of new services and the enhancement of existing ones. The role will also work closely with:  

-   The Technology & Research team to agree on architecture and development approaches, and technical resource allocation.  
-   The Outreach team including technical support, to research and gather insights and ensure projects stay community-led; openly communicating through planning and running projects, working groups, and collaborations.  

### Product strategy

-   Create and maintain the product strategy, which supports and advances the organization's strategic agenda.
-   Work with the Technology & Research team to ensure that all software development work is product-focused.
-   Build and maintain the product roadmap, ensuring all areas of the organization are up-to-date on plans and timelines.
-   Be responsible for the product team's work in scoping and planning new feature development, running pilots and beta test phases, and facilitating internal project teams and external Working Groups to deliver on time.
-   Develop introduction plans to roll out and embed new features with Outreach, Support, Billing, and Technology maintenance.
-   Maintain a balance between developing new things and maintaining existing features and services.

### Community focus

-   Research community needs and engage people through Advisory Groups for each key area of our service as well as Working Groups for new initiatives.
-   Work closely with the Outreach Director to set development priorities based on user needs.
-   Enable members and users to comment on and contribute to our roadmap so that they can plan for their own adoption of new features.
-   Be an evangelist for Crossref as open scholarly infrastructure and its services at conferences and events, engage on social media and the community forum, and blog about our services and plans.

### Leadership and management

-   Manage and develop three Product Managers and one UX/UI Designer.
-   Reinforce and promote the value and role of product management within Crossref.
-   Ensure that the product team culture reflects and exemplifies the Crossref culture (dedicated, open, transparent).
-   Contribute to developing Crossref's overall strategy as part of the senior leadership team of staff directors.
-   Report to the board regularly.
-   Write papers for consideration by the leadership team, board committees, and the board.  

## About you

We are looking for an experienced product person and strategic thinker to join the senior leadership team and contribute to the ongoing success of the organization. Our transparency principle, [What you see, what you get](/truths/), means that using open methodologies is a must. We are also looking for someone with:

- Understanding of current trends in scholarly communications.
- Strong technical understanding, ability to establish credibility with software developers.
- A background in product management for mission-driven organizations and/or open-source tech.
- Experience planning and launching new features and services with buy-in from a community of users.
- Experience working globally across time zones and within multicultural groups both internally and externally.
- Experience working remotely and managing remote staff.
- Leadership and management experience.
- Experience overseeing and improving internal processes and systems.
- Ability to be a hands-on technical product manager when needed.
- Experience developing wireframes and product specifications.
- Awareness of new approaches to product management and development methodologies.
- Creativity, being resourceful as needed for a small non-corporate organization.
- Collaboration skills and the confidence to accomplish goals while engaging the community.
- Excellent communication with strong writing and public speaking skills.
- Experience managing budgets, external consultants, and oversight of project management.

## Process & timeline

Please email a cover letter and CV to [jobs@crossref.org](mailto:jobs@crossref.org) by June 10th, 2019.

## About Crossref

Crossref makes research objects easy to find, cite, link, assess, and reuse. We’re a not-for-profit membership organization that exists to make scholarly communications better. We rally the community; tag and share metadata; run an open infrastructure; play with technology; and make tools and services—all to help put research in context. It’s as simple—and as complicated—as that.  

Since January 2000 we have grown from strength to strength and now have over 12,000 members across 120 countries, and thousands of tools and services relying on our metadata.  

We can offer the successful candidate a challenging and fun environment to work in. We’re fewer than 40 professionals but together we are dedicated to our global mission. We are constantly adapting to ensure we get there, and we don’t tend to take ourselves too seriously along the way.   


---
