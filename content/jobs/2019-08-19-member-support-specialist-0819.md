+++
title = "Member Support Specialist "
date = 2019-08-19
draft = false
image = "/images/banner-images/jobs-robots.jpg"
author = "Amanda Bartell"
weight = 3
rank = 4
parent = "Jobs"
+++

{{% divwrap red-highlight %}} Applications for this position are closed {{% /divwrap %}}

## Come and work with us as our **Member Support Specialist**. It’ll be fun!

- **Location:** Flexible - Crossref has members globally and offices in Oxford, UK and Lynnfield, MA, USA but being office-based is not necessary.
- **Reports to:** Amanda Bartell, Head of Member Experience  
- **Salary and benefits:** Competitive


Do you want to make scholarly communications better? Are you a customer support specialist or editorial assistant who’s keen to have more in-depth conversations with publishers across the globe?  

## About Crossref

Crossref makes research objects easy to find, cite, link, assess, and reuse. We’re a not-for-profit membership organization that exists to make scholarly communications better. We rally the community; tag and share metadata; run an open infrastructure; play with technology; and make tools and services—all to help put research in context.  

## About the role

This busy role is a real mix of involved member consultations and detailed systems and administrative work. You’ll need to have an understanding of the academic publishing community, great attention to detail, the ability to ask probing questions of applicants and a logical, systematic approach to work. You’ll be working with publishers to really understand their approach and workflows, and then recommending the best Crossref membership option for them. You’ll take them through our application process, setting them up carefully in our CRM and other systems paying extremely close attention to detail. Once they’re members, you’ll continue to work with them closely - answering their questions via email, Twitter and our community forum. You’ll help them take on new Crossref services, navigate platform migrations, and understand how to set up service providers to work with us on their behalf. It’s a very diverse role and is a great opportunity to get wide-ranging experience within Crossref and scholarly communications.  

This is a pivotal role in the Member Experience team, part of Crossref’s Outreach team, a fourteen-strong team split between offices in Boston and Oxford, plus dispersed team members across the US and Europe. We’re at the forefront of Crossref’s growth, building relationships with new communities in new markets in new ways. We’re embarking on a new onboarding program for the thousands of publishers that join as members every year and currently rolling out an educational program for existing members and users. And we’re aiming for a more open approach to having conversations with people all around the world, in multiple languages.  


## Key responsibilities

- Work with new applicants to understand their internal structures and help them understand the various membership options available to them.  
- Own and drive the administrative process for new applicants - ensuring we have all the information we need to help them get started and setting them up correctly in our central systems.  
- Support existing members in meeting the obligations necessary for taking on new services and get them set up for these services.
- Broker conversations between publishers, platforms and service providers to ensure the member is able to fulfill their aims while still meeting their membership obligations.  
- Manage queries from applicants and members via email, Twitter, our community forum and other channels.
- Ensure that the information in our CRM is kept clean and up-to-date.
- Work closely with the billing team.  

## About you

We’re looking for a smart, savvy person who’s able to work with our global, diverse membership to really get to the bottom of their needs. You’ll need to adapt quickly within a changing environment while still maintaining accuracy. You’ll be a quick learner of new technologies and enjoy improving systems and processes, but you’ll also be able to build relationships with our members and serve their very diverse needs - from handholding those with basic queries to really digging into some knotty organizational relationships.  

- Able to balance a very busy role while still paying close attention to detail and keeping member experience at the forefront of everything you do.  
- Experience in helping customers and solving problems in creative and unique ways.  
- Strong written and verbal communication skills with the ability to communicate clearly - able to use open questions to get to the bottom of things when members don’t seem to make sense.
- A truly global perspective - we have 10,000 member organizations from 118 countries across numerous time zones.  
- Quick learner of new technologies and can rapidly pick up new programs and systems.
- Extremely organized and attentive to detail.
- Bachelor's degree.  
- Experience with Zendesk or similar support system ideal.
Familiar with the publishing industry with knowledge of XML, metadata, scholarly research or information science a bonus.
- Other spoken languages will help.  

---

## To apply

If you’d like to join the Crossref team, and contribute to [our mission](/community/about), please send a cover letter and your CV to [jobs@crossref.org](mailto:jobs@crossref.org). We can't wait to read all about you!
