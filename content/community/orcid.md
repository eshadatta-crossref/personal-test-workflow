+++
title = "ORCID auto-update"
date = "2017-01-13"
draft = false
author = "Ginny Hendricks"
rank = 3
parent = "Get involved"
weight = 10
aliases = [
    "/01company/orcid.html",
    "/orcid_autoupdate/index.html"
]
+++

An ORCID iD is a persistent identifier for individual researchers and scholarly contributors. It allows everyone (authors, publishers, funders, and research institutions) to uniquely identify the work that they do, and accurately attribute it.


_Enter once, when you submit a paper, then watch as your ORCID record is automatically updated as your work is published, registered with Crossref, and enters the global citation network!_

## Who is it for?
* Authors: save yourself time and reduce the burden of manual data entry, and easily keep your ORCID record up-to-date
* Publishers: help automate processes for your authors and enhance the discoverability of their work and your content
* Funders, research administrators, librarians and anyone else interested in tracking research outputs.

## How does it work?

Crossref’s Auto Update is a classic example of open scholarly infrastructure at work. Registering and sharing metadata and persistent identifiers—such as ORCID iDs and Digital Object Identifiers— means systems can communicate with each other to save everyone a lot of time and effort.  

When Crossref members register their content with us, we encourage the inclusion of the ORCID iDs belonging to the individual(s) who contributed to that publication—whether they are authors, peer reviewers, or editors. When ORCID iDs are included in the metadata provided to Crossref along with other information about the work such as title, date, and DOI, we can [automatically update the associated ORCID record(s) upon publication](https://support.orcid.org/hc/en-us/articles/360006896394-Auto-updates-time-saving-and-trust-building) (with permission from the record holder(s)).


## How do I get started?

* Authors: [register for an ORCID iD](https://orcid.org/register), and be sure to supply it when you submit your article. Once your article is published, you will receive a notification from ORCID asking you to grant Crossref permission to update your record. Once granted, Crossref will automatically update  your record with any future publications that contain your ORCID ID. If you change your mind, you can revoke Crossref’s permission at any time—you can do this directly from our ORCID record ([learn more](https://info.orcid.org/researchers/)). As an ORCID record holder, you are in control of your record and can grant and revoke access of trusted parties, including Crossref, at any time. By providing  your ORCID iD when you submit a paper, you can be sure that as your work takes on a life of its own, you will always be credited.  
* Publishers: Encourage your authors to sign up and submit their ORCID iDs when submitting papers. Let authors know that by doing this, the article will automatically be added to their ORCID record once published, and that they should look out for a notification if/when their paper is published to prompt them grant permission for ORCID to add their publication to their ORCID record. They can grant long-lived permissions so that they enable the automatic addition of any further publications registered with Crossref to their record. Build awareness among editors of the importance of collecting this persistent identifier.


---

Please consult other users on our forum [community.crossref.org](https://community.crossref.org) or open a ticket with our [technical support specialists](mailto:support@crossref.org) with any questions.
