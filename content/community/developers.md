+++
title = "For developers"
date = "2021-02-26"
draft = false
author = "Rachael Lammey"
rank = 5
parent = "Working for you"
weight = 17
+++

If you develop tools and software to find, cite, link, and/or assess research outputs, you can integrate our metadata about scholarly content into your project, through our open APIs.

## Trying to get data from us?

We have an open [REST API](https://github.com/CrossRef/rest-api-doc/blob/master/rest_api.md) that provides a flexible way of accessing and using the most up-to-date metadata we have. We store bibliographic metadata related to currently {{< total-results "/works?rows=0" >}} publications, coupled with funding information, license details, ORCID iDs, full-text links and much more.

The API makes it easy to facet, filter, or search the metadata we hold. There's no need to sign-up, but we’d love you to tell us how you’re using it or check out [some examples](/categories/api-case-study) for inspiration.

If the REST API isn’t the best fit for what you’re doing, we have a number of other [delivery options](/services/metadata-retrieval) and are happy to discuss to get you fixed-up with one of those.

## Working on Crossref services

* Implementing Crossmark? Publishers participate in Crossmark by depositing additional metadata for their content and adding a snippet of code to their DOI landing pages to generate the Crossmark button and link. Details for each of these steps are on in [our documentation](/education/crossmark/).
* Interested in text mining? Our REST API is designed to allow harvesting full-text from participating members for the purpose of text mining. We [walk you through the process](/education/retrieve-metadata/rest-api/text-and-data-mining-for-researchers/) if you’re keen to explore.
* Want to [register content with us](/education/content-registration/), [upload files or do bulk queries](/education/member-setup/direct-deposit-xml/https-post/)?

Stuck? Email our [technical support specialists](mailto:support@crossref.org) and we’ll try to help.

## Labs

Want to see what our R&D team have been working on? Check out [labs](/labs) to find experiments and documentation, or [share your own ideas](mailto:labs@crossref.org).
