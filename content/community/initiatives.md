+++
title = "Initiatives"
date = "2021-01-07"
draft = false
image = "/images/banner-images/train-light.jpg"
[menu.main]
rank = 1
parent = "Get involved"
author = "Rachael Lammey"
weight = 5
+++

Our communities' work to solve problems and support research doesn't stand still. Many people come to Crossref for help either to lead, advise, host, or support new strategic ideas and initiatives.

Some of them are key to [our mission](/community/about) and stand to provide value to our members and the wider research ecosystem. We’ve identified some as *special programs* so that we can devote more time and attention to them. This support can take a variety of formats: promoting, prototyping, chairing or board/working group participation, adopting, co-developing, resourcing, financing, or advising - all while listening and learning too.

Strategic programs we’ve taken on in the past include ORCID, preprints, and CHORUS. And of course we still support them, but that support has become part of our regular practices and processes and those initiatives have graduated to become fully-embedded services or separate organizations.

We’re currently focusing our time and effort on three special programs: [Research Organizations Registry (ROR)](/community/ror); [the registration of grants with Crossref](/community/grants); and [data citation (including Scholix and Make Data Count)](/community/data-citation). You can read more about all of these by following the links at the side of the page, and let us know if you have any questions about how you or your organization can get involved.

It can be hard to keep track of everything, so if we’ve missed something you or your new idea is keen to develop with Crossref support, then do [get in touch](mailto:feedback@crossref.org).
