+++
title = "Working for you"
date = "2020-11-08"
draft = false
image = "/images/banner-images/working-for-you.jpeg"
author = "Rachael Lammey"
[menu.main]
parent = "Get involved"
weight = 10
rank = 4
+++

Innovation never slows, and the ways research is communicated is no different. Crossref has evolved to work with and for many new and emerging organizations and innovations. Find out how our services can work for you, [take a look](https://www.crossref.org/members/prep/) at our current members metadata coverage and [get in touch](mailto:feedback@crossref.org) if you'd like to see us develop something new.

{{% row %}}
{{% column %}}

## For Publishers
Increase discoverability; put your content on the map so that it’s easy to find, cite, link, assess, and reuse. We help you create persistent links between research outputs such as articles, books, references, data, components, versions, and more. Read more about our [services for Publishers](/community/publishers/).

---

## For Editors
Your decisions influence what research is communicated and how. Demonstrate your editorial integrity with tools that help you assess a paper’s originality, and properly label and connect updates, corrections, and retractions. Read more about [what we offer Editors](/community/editors).

---

##  For Researchers
Find other researchers’ work and let them find yours. Through registering DOIs, we collect and share comprehensive information about research such as citations, mentions, and other relationships. Thousands of tools and services then harness this information---for search, discovery, and measurement---through our open APIs. Read more about [how Researchers benefit](/community/researchers).  

---

## For Developers
If you develop tools and software to find, cite, link, and/or assess research outputs, you can integrate our metadata about scholarly content into your project, through our open APIs. Read more about our [tools for Developers](/community/developers).

{{% /column %}}
{{% column %}}

## For Research Funders
Connect your grants and grantees with their published outputs. We collect and share metadata about published research---such as Funder IDs, ORCID iDs, licenses and clinical trials---all of which helps funders measure reach and return. Read more about our [services for Funders](/community/funders). Crucially, funders are joining Crossref to [register their grants](/community/grants) so that they can more easily and accurately track the outputs connected to the research they support.

---

## For Librarians
Enhance your metadata and connect your discovery and linking services with our metadata records---they’re all available in XML and JSON through open APIs and search. Read on for our [services for Libraries](/community/librarians).

---

## For Preprint Servers
Add your preprints, working papers, and more to the corpus of works that are easy to find, cite, link, assess, and reuse. With a persistent identifier and related metadata, you can create links between different versions of the same document, including those published in journals and elsewhere. Authors can easily cite their preprints in articles, grant applications, and for research assessment. Read more about our support for [preprints and related metadata](/community/preprints).

---

## For Research Institutions
Your institution can use Crossref to help with the management, analyses, and reporting of their research activities. Through our open metadata, you can support researchers to bring their ideas to life, identify groups to work with, demonstrate compliance, and share and report on outcomes. Read more about we help [Research Institutions](/community/research-administrators).

{{% /column %}}
{{% /row %}}
