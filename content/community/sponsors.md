
+++
title = "Sponsors program"
date = "2021-05-11"
draft = false
image = "/images/banner-images/buildings-sky-sponsors.jpg"
maskcolor = "crossref-blue"
rank = 4
author = "Susan Collins"
[menu.main]
parent = "Get involved"
weight = 15
aliases = [
    "/02publishers/30sponsor_member.html",
]
+++

Some small organizations who want to register metadata for their research and participate in Crossref but are not able to join directly due to financial, administrative, or technical barriers. Our Sponsors program has grown over the last decade and has now become the primary route to membership for emerging markets and small or academic-adjacent publishing operations.

More members now join Crossref via a Sponsor than directly. So Sponsors are important partner organizations in helping to scale to meet the growing demand of new members.

Sponsors work closely with us in order to provide administrative, billing, technical, and---if applicable---language support, to the members they work with. They each have an agreement with us as well as with every member they represent, and they submit an annual report to us detailing their outreach and training activities. Sponsors are not members themselves---and therefore cannot participate in our community governance---but the organizations that work with them are full members and may vote in (or stand as a candidate for) our board elections.

In turn, we’re keen to support our Sponsors too, by running events and other outreach activities with them and listening to their feedback on what they need from us. You can find a list of our current Sponsors [on this page](/membership/about-sponsors).

Please note, we are [pausing the acceptance of new Sponsors](/blog/refocusing-our-sponsors-program-a-call-for-new-sponsors-in-specific-countries/) from regions where Sponsor numbers are already very high or not based in a [GEM program](/gem/) country. Additionally, we are focusing on regions where growth is high and no Sponsor is present. By doing so we can focus on growing the program in areas where there is the greatest need.  

## Sponsor criteria

Sponsors are key partners in making Crossref membership benefits available to all and keeping them to their membership obligations. There is quite a high bar to meet to be a Sponsor as we need to be sure you would represent Crossref accurately and successfully, and add value in the community.  

Please make sure that your organization would be able to meet the criteria below.

### About you as a Sponsor
+ You are a recognized organization in good standing with the scholarly community with a clear online presence describing your services. Your services are a good fit with Crossref services.
+ You exhibit a clear knowledge of Crossref and our services, and of what is achieved by registering content with Crossref.
+ You work with a particular segment or region of the research community who wouldn’t otherwise be able to work with Crossref due to barriers such as:
  - Lack of resources either technically, financially, or operationally (or all)  
  - Need for support in languages other than English  
+ You have the technical know-how and resources to facilitate Content Registration with Crossref on behalf of members and you understand the importance of complete and accurate metadata, not solely registering a DOI.
+ You are aware of the criteria that need to be met on an ongoing basis to participate in optional Crossref services e.g. Similarity Check, Cited-by. You understand that services such as these can only be offered to the Crossref members you work with, and is based on their eligibility for these services.  
+ You are aware that Crossref membership is not open to organizations or individuals who are subject to sanctions in the US, UK or EU - [more information here](/operations-and-sustainability/membership-operations/sanctions/).  
+ You have a financial/funding model and are capable of covering the membership fees for the members you represent and the content registration fees they incur. You can handle billing on behalf of members and will pay invoices within agreed payment terms.  
+ You can demonstrate the above in a documented plan that you would share with us and update and report on annually.

### Your role as a Sponsor
+ You work with organizations to enable them to be members of Crossref. This includes:
  - Managing the membership set-up and joining process in collaboration with the members and our staff.
  - Ensuring that organizations are clear about what they are agreeing to when becoming a Crossref member and joining Crossref through you as their Sponsor.
  - Ensuring relevant contracts are completed by the member, that accurate information about members is sent to Crossref and kept up-to-date,
  - Sharing and explaining member-specific communications and changes, and helping members adapt to new processes or obligations related to Crossref services.
+ You perform checks to make sure the members you are working with are not prohibited from joining Crossref due to OFAC sanctions.
+ You provide support for and promotion of Crossref activities and services. This includes being the first line of technical support for the members you work with, potentially providing training.
+ You communicate with our staff in a timely manner.
+ You positively contribute to the reputation of Crossref and its inclusive mission, adhering to the code of conduct and best practice guidelines that protect and enhance other members of the Crossref community collectively.  

## When Sponsors stop being Sponsors

Sometimes, things don't work out, or plans change. We try to make sure that every Sponsor we accept will be able to commit to helping our members long-term. But if they change paths or stop being able to fulfill the role described above, we sometimes need to give notice of termination and to work with the members to find an alternative Sponsor. It is rare, but in such cases we will endeavour to follow these steps:

1. Letter sent to Sponsor setting out concerns and asking for a plan to improve within a reasonable time period. We offer to help with this improvement plan, if relevant.
2. Review performance and if no improvement, we inform the Sponsor that they will no longer be able to sign up new members for a short period.
3. Assess progress and if no improvements we formally notify the Sponsor that we're terminating our agreement, which includes 30 days' notice.
4. We contact the relevant members to inform them of their options and the timeline.
5. Depending on how many members the Sponsor represents, we may work with them for longer than 30 days to transition the members to new Sponsors or to direct membership.
6. Members who don't move to a new Sponsor will be moved to direct membership and will be responsible for content registration, fees, and administration going forward.

## Apply to become a Sponsor

If you are located in one of the regions where we are seeking new Sponsors and if you are able to fulfil the above criteria, and are ready to apply to become a Sponsor, please [contact us](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001618152) with your organization name and website, plus your plans for how you could support Crossref members as a sponsor. After that, we'll ask you to provide additional information so that we can find out more about your motivations, who you would help, your planned activities, and how you would fulfill the role. We normally make a decision about your application within a month after this review.

We look forward to hearing from you and working together to lower barriers to participation around the world.

## Looking for a Sponsor?

If you have content you want to register with us but are not able to join directly, [find a Sponsor](/membership/about-sponsors/) to contact about representation, rather than applying to join Crossref directly.

---
For more information, please contact our [membership specialist](mailto:member@crossref.org).
