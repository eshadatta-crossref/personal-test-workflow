+++
title = "For research managers"
date = "2017-01-10"
draft = false
author = "Rachael Lammey"
rank = 5
parent = "Working for you"
weight = 25
+++

Crossref helps research institutions with the management, analyses, and reporting of their research activities. Through our open metadata, institutions can:

* Support researchers in identifying possible collaborators, partner organizations, related research and funding opportunities  
* Locate, enhance, and analyze information about outcomes of previous projects to show impact and influence  
* Help researchers demonstrate compliance with funder policies  
* Enhance assessment activities by streamlining reporting on a range of published outputs  
* Verify and group outputs related to particular grants, projects, and people to better understand the research profile of their organisation.  

### More than just bibliographic metadata

The basic bibliographic metadata we collect from our members is really useful in helping the identification and discovery of content.

However, there’s lots more to the metadata than that. We collect additional information that helps track and link research outputs to researchers, funders and institutions. This includes:

* Information on who funded the research 
* The ORCID iDs of the authors, and we can [push the articles to their ORCID records automatically](/community/orcid)
* License information and embargo dates
* Author affiliation information (soon to be supported by the collection of [ROR ids](/community/ror/))
* Links to full text, or different versions of content e.g. accepted manuscript, version of records
* Third party archive arrangements
* Abstracts  
* Acceptance dates

We allow publishers [to register DOIs upon manuscript acceptance](/education/content-registration/content-types-intro/pending-publications/), even if the manuscript has not been made available online. This will help notify you and your institution of impending publications by your researchers as early in the process as possible, and track them through to publication and beyond.


### Using this information to track research outputs

This information is freely available for you to use and integrate into your own systems via our REST API and search interfaces. We have [a detailed guide](https://github.com/CrossRef/rest-api-doc/blob/master/funder_kpi_metadata_best_practice.md) on what this information looks like in our metadata to help you search, facet or filter the metadata.

Interested in what others are doing? Read our [case study](/blog/using-the-crossref-rest-api.-part-6-with-nls/) on how the National Library of Sweden is using our REST API in two of their projects, *Open APC Sweden* and in their *local analysis database* for publication statistics used in negotiations with publishers.

---

Please [get in touch](mailto:feedback@crossref.org) if you have any questions or suggestions: we’d be happy to provide help, advice or update you on publisher uptake and our future plans.
