+++
title = "Funders"
date = "2020-12-01"
draft = false
author = "Rachael Lammey"
rank = 5
parent = "Working for you"
weight = 2
aliases = [
	"/services/content-registration/grants",
	"/services/content-registration/grants/"
]
+++

## How Crossref fits with funders

We collect and share metadata about research published in articles, books, preprints, reviews, and more---such as licenses, clinical trials, and retractions---all of which helps funders measure reach and return.

Our members register DOIs and bibliographic metadata with us for journal articles, [preprints](/education/content-registration/content-types-intro/posted-content-includes-preprints/), books, peer reviews, [and more](/services/content-registration/#content-types). We have a growing number of funder members, and encourage funders to become involved in our committees, advisory groups and overall governance.

> Crossref metadata is the bedrock for many thousands of platforms and services from search and discovery to research and assessment tools.

We make this information available via a [funder search interface](https://search.crossref.org/funding) and via our [public REST API](https://api.crossref.org) so that it can be seamlessly integrated into downstream systems.

This helps report on:
* Published outputs: where and how are researchers publishing, when, which institutions are they from, who is funding them?   
* Data sharing and citation: has underlying/related data been linked to or made available?  
* The citation and sharing of these published outputs  

The openness of this information is a key part of Crossref’s support of the [Principles of Open Scholarly Infrastructure](https://openscholarlyinfrastructure.org/), which [our board voted to adopt in November 2020](/blog/crossrefs-board-votes-to-adopt-the-principles-of-open-scholarly-infrastructure/). The principles are a set of guidelines by which open scholary infrastructure organizations and initiatives that support the research community can be run and sustained. We think that these principles help all those that support scholarly communications and research hold each other accountable, and ensure that vital open infrastructure can be stakeholder-governed, transparent and maintained for as long as it serves its purpose.


## Funding data and the funder registry

Watch the video below to find out more about [funding data and the registry](/services/funder-registry/):

<script src="https://fast.wistia.com/embed/medias/ea9qsekzri.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_ea9qsekzri popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;width:100%">&nbsp;</span></div></div>

As the video shows, we take this largely unstructured grant data, and help the research community match products and people to funding. In turn, funders use this metadata to track the impacts of their investment, and understand similar investments made by other funders.

This metadata will always be open and freely available but we also offer a more dedicated service with more predictable API response times, through our [Plus service](/services/metadata-retrieval/metadata-plus/).

## Registering research grants

Reporting at the funder-level can be useful, but we’ve been working with our [Funder Advisory Group](/working-groups/funders/) to support the registration of grant metadata so that funders can see published outputs connected to specific research grants. You can find out more about why funders are registering research grants and how you can get involved in this initiative by visiting our [dedicated grants page](/community/grants).

---

If you have any questions about membership or registering grants, then [our membership team](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001618152) can help. Our [technical support specialists](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001642691) can also help with questions about the funder registry, our APIs or grant registration.
