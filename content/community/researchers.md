+++
title = "For researchers"
date = "2017-01-06"
draft = false
rank = 5
parent = "Working for you"
author = "Rachael Lammey"
weight = 10
aliases = [
    "/05researchers/18demo.html",
    "/05researchers/18gallery.html",
    "/05researchers/index.html"
]
+++


Find other researchers’ work and let them find yours. Through registering DOIs, we collect and share comprehensive information about research such as citations, mentions, and other relationships. Thousands of tools and services then harness this information—-for search, discovery, and measurement—-through our open APIs.

## Persistent linking

Our members register the content they publish with us to tell us it exists - this includes bibliographic (and other) information, and persistent identifiers.

This helps make content discoverable by uniquely identifying the work, and giving a means to link to it long-term. When you click on the Digital Object Identifier (DOI) in a reference list, you’ll be reliably taken to the content you’re interested in, regardless of the publication or publisher. If the content moves to a new website, the publisher will come to us and register the new location of the content so that the link doesn’t break and researchers can continue to navigate between content without any snags.

We also disseminate that metadata so that other systems - search or subject databases, library systems and scholarly sharing networks - can employ it to help their users find the research they’re interested in.

Registering content with us helps the content you publish be found, cited, linked to, and used by other researchers. Watch the video below to find out more:

<script src="//fast.wistia.com/embed/medias/2fkzp1visa.jsonp" async></script><script src="//fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_2fkzp1visa popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;width:100%">&nbsp;</span></div></div>

## Add DOIs to your reference lists

Adding DOIs to your reference lists means that readers can link persistently to related works.

You can search for DOIs via our [search service](https://search.crossref.org) or add a list of references to our [query tool](/education/retrieve-metadata/simple-text-query/) to have the DOIs we can find returned for those.

You’re also welcome to access and use our metadata via our [REST API](https://api.crossref.org) - no sign-up is required and it’s free to use.

## Have an ORCID iD?
Speaking of identifiers, we’re big fans of [ORCID iDs](https://orcid.org).

It’s simple (and free) to [register for an ORCID iD](https://orcid.org/register), and providing your ORCID iD when you submit a paper or publish content provides more ways for people to discover your research.

Many members collect ORCID iDs from their authors, and if they deposit them with us when they register content we can push the publications to the author’s ORCID record automatically. It’s called [auto-update](/community/orcid) and means researchers can skip re-keying information and have their ORCID record show the most complete record of their publications.

You can also update your ORCID record by adding existing works that have a Crossref DOI by taking the following steps:  
1. Start from [https://search.crossref.org/](https://search.crossref.org/) and click “Sign in” at the top right of the screen
2. Sign in with your ORCID account credentials, or “Register now” if you don’t yet have an ORCID iD
3. You’ll then see the following message: “Crossref Metadata Search has asked for the following access to your ORCID Record:
  - Add/update your research activities (works, affiliations, etc)  
  - Read your information with visibility set to Trusted Parties”
4. Click <i>Authorize</i>
5. Search for any of your publications that have a Crossref DOI. You can do this by the title, authors or publication.
6. Click “Add to ORCID” to the right of each publication to add it to your ORCID record.


## Finding important updates to content

If you see this button when reading a paper, make sure you click on it!

<img src="https://crossmark-cdn.crossref.org/widget/v2.0/logos/CROSSMARK_Color_horizontal.svg" alt="Crossmark v2.0 button" width="25%" />

Using [Crossmark](/services/crossmark) gives you quick and easy access to the current status of a work. With one click, you can see if content has been updated, corrected or retracted meaning you can have confidence in the status what you’re reading or citing. You can also see useful additional information on things like who funded the work, what licenses apply to the content and much more.

## Interested in text mining?

Our REST API is designed to allow researchers to harvest full-text from participating members for the purpose of text mining.

Publishers deposit license information and links to the full-text of the content with us, and researchers interested in mining cross-publisher content can use that information to find out where the content is located, and what they are allowed to do with it. A GET request then allows you to download the full-text from the publisher’s site (if you have access to it). We walk you through the process in our [documentation](/education/retrieve-metadata/rest-api/text-and-data-mining-for-researchers/).

---

Questions about any of this? [Get in touch!](mailto:feedback@crossref.org)
