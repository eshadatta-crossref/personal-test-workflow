+++
title = "History"
date = "2020-02-02"
draft = false
author = "Ed Pentz"
parent = "About us"
rank = 2
weight = 6
+++

Crossref was born of the radical changes in the 1990s brought on by the spread of the Internet and development of the World Wide Web and other technologies (HTML, SGML, XML). Everything started moving online, including research and scholarly communications.

Our roots go back to 1996 when the Enabling Technologies Committee of the Association of American Publishers put out a call for a persistent identifier system for online content and the Corporation for National Research Initiatives (CNRI) answered the call with the Handle system. A couple of years of work and discussions led to the founding of the International DOI Foundation to develop and govern the DOI (Digital Object Identifier) System which was the application of the Handle System to the digital content space.

1999 was the year things came together enabling the formation of Crossref. It was a big year: there was the Y2K/Millennium bug--an example of infrastructure causing problems--and there was the launch of Napster signaling massive disruption for the music industry. On more positive notes, 1999 also saw the release of version 1.0 of the [Bluetooth specification](https://en.wikipedia.org/wiki/Bluetooth_Special_Interest_Group) which enables connectivity across millions of devices, and the formation of the [WIFI Alliance](https://en.wikipedia.org/wiki/Wi-Fi_Alliance) which is a nonprofit association tasked with promoting the adoption and development of wireless technologies across all vendors. Bluetooth and WIFI have been incredibly successful standards backed up by non-profit organizations that foster collaboration, adoption and development to benefit a wide range of stakeholders. Remind you of anyone in the scholarly communications space?

With respect to Crossref’s formation, things really kicked off in 1999. A prototype project by Academic Press, Wiley, and the DOI-X project, created the technical foundations for reference linking based on centralized metadata and the assignment of [Digital Object Identifiers (DOIs)](https://www.doi.org). The prototype system was demonstrated at the Frankfurt Book Fair in 1999. Publishers quickly rallied around and in December 1999 a working group of 12 organizations met and decided to form Crossref as an independent, not-for-profit organization. Crossref was incorporated in January 2000 - as Publishers International Linking Association, Inc. (PILA). The Crossref system, the first collaborative reference linking system, went live in June 2000.

Crossref has grown steadily over the years - from the original 12 founding members to the over 17,000 organizations who are currently members of Crossref. To see more details of Crossref’s history and developments over the years please see our [Annual Reports](/operations-and-sustainability/annual-report).

In celebration of Crossref's 10th Anniversary in 2009, Crossref commissioned [The Formation of Crossref: A Short History](/pdfs/CrossRef10Years.pdf). A [Japanese translation](/pdfs/CrossRef10Years_Ja.pdf) is also available.
