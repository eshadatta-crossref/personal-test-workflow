+++
title = "Registering research grants"
date = "2021-01-07"
draft = false
author = "Ginny Hendricks"
[menu.main]
rank = 2
parent = "Initiatives"
weight = 9
+++

In 2017, our board agreed that working more closely with research funding organizations should be a key strategic priority for Crossref community. As part of that we reconvened our [Funder Advisory Group](/working-groups/funders) and started discussing what more Crossref could do to integrate the work of funders more closely with research objects and publications. We worked with the Funder AG alongside DataCite and ORCID to create a new strategic plan and in 2019 launched our registration service for grants. This means that many funders are members of Crossref and that hundreds of grants already have metadata records (identified by DOIs) that are able to be linked with outputs, activities, people, and organizations. Read more about how [Crossref services support funder goals](/community/funders).

## Background to grant registrations

Whilst the scholarly community has adopted standard persistent identifiers (PIDs)---for people (e.g. ORCID), content (e.g. [DOIs](http://doi.org), PMIDs), and now organizations ([ROR](http://ror.org/)) including funders (Open Funder Registry)---the record of the award was not captured in a consistent way across funders worldwide. These awards were not easily linked up with the literature or with researchers or with institutions.

With tens of thousands of funding organizations in the Open Funder Registry, we needed to find a way for all of them––small and large, private and government––to register their grants, whilst making it easy for researchers to include this information in their submissions to publishers and data repositories. One thing to note about Crossref grant records is that they can be registered for all sorts of support for research, such as awards, use of facilities, sponsorship, training, or salary awards. Essentially any form of support provided to a research group.

For this to work more smoothly and accurately, we need grant identifiers. Our [Funder Advisory Group](/working-groups/funders) worked from 2017 to 2019 to create such a system on a global scale. Part of that work was to agree on a sustainability model along the lines of our current approach for [Content Registration](/services/content-registration) of articles and books, and to decide on a schema that would capture enough relevant metadata about grants to satisfy community needs.

Robert Kiley and Nina Frentrop of Wellcome explain in their guest blog post their [vision for an open and global grant identifier system](/blog/wellcome-explains-the-benefits-of-developing-an-open-and-global-grant-identifier/):

> Currently, researchers are typically asked to manually disclose what outputs have arisen from their funding. In the future, such disclosures would be fully automated. We are already seeing how publishers---who collect ORCIDs through their manuscript submission system---automatically update the author’s ORCID record with details of new publications. [With a global ID system for grants], publishers and repositories could also require these to be disclosed on submission, and this data could then programmatically be passed to researcher assessment platforms, like [ResearchFish](https://www.researchfish.net).

## Benefits

When funders register research grants with Crossref, they join the rest of our member metadata, bringing benefits for everyone from funders, researchers, publishers and institutions to platforms and tools.

<img src="/images/services-images/funder-visual.png" alt="Benefits of registering grants with Crossref for funders" width="80%">  

{{% accordion %}}
{{% accordion-section "Benefits for funders" %}}  

**During research management (primarily coming from activity reporting):**

- Improved analytics and data quality
- More complete picture of outputs and impact
- Better value from investments in reporting services
- Improved timeliness, completeness and accuracy of reporting
- More complete information to support analysis and evaluation
- Streamlined discovery of funded content

**During reporting and evaluation (with a special component for policy compliance):**

- Better information about publication pathways and policy compliance
- Better/more comprehensive data about the impact and outcomes of their policies
- Improved data on policy compliance
- Improved data on policy progress and impact
- Streamlined discovery of funded content
- Better understanding of the effects of investments on the research landscape
- Clearer data on impact and ‘ROI’ for facility/infrastructure investments
- Improved analysis and evidence of outcomes of career support
- Improved publication ethics and research integrity (COIs, funding transparency etc.)
- Improved picture of long-term ROI and impact

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "Benefits for content hosts" %}}

Content hosts include publishers, data repositories, and hosting platforms

- Improved publication ethics and research integrity
- Improved services to authors
- Improved transparency on content access
- More connections within and between platforms and content
- New platform opportunities and value added services
- Reduced administrative and information management/verification overhead
- New value add services
- Greater ecosystem integration
- Improved user experiences

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "Benefits for research organizations" %}}

This includes benefits for research administrators and managers, resource managers, project managers, and institutional policy makers.

**Research administrators and managers benefit from:**

- Opportunities to provide additional effective and constructive support for proposal preparation (pre-award)
- Find it easier to perform due diligence (pre-award)
- Reduced overhead in data collection (research management)
- Reduced overhead in compliance and data checking (research management)
- Reduced time/effort and improved data quality (reporting and evaluation)
- Improved evidence for decision making (reporting and evaluation)
- Better evidence for career and organizational impact (reporting and evaluation)

**Resource managers benefit from:**

- Better intelligence on funding sources and dynamics (pre-award)
- Better understanding of who is using their facilities (research management)
- Clearer links to downstream benefits of their work and provision (reporting and evaluation)
- Improved reporting/analysis capacity (reporting and evaluation)
- Improved data quality (research management)
- Simplified data sharing (research management)

**Institutional policy makers and strategists benefit from:**

- Understanding funder portfolios to improve grant targeting (pre-award)
- Reduced data gathering overhead and improved intelligence about their portfolio of outputs (research management)
- Richer understandings of their research activity portfolio (research management)
- Better management of APC budgets (research management)
- Greater insight and evidence for stronger strategic planning (research management)
- More complete information to support analysis and evaluation (reporting and evaluation)
- Improved analytics and data quality (reporting and evaluation)
- Better understanding of outcomes of studentships and postdoctoral positions (reporting and evaluation)
- Improved connections to alumni (reporting and evaluation)
- Better data for benchmarking (reporting and evaluation)

{{% /accordion %}}
{{% /accordion-section %}}

{{% accordion %}}
{{% accordion-section "Benefits for researchers" %}}

**In applying for funding, researchers benefit from:**

- Reduced data entry and improved reusability of information in applications
- Better tailored institutional support
- Improved targeting and design of career supporting interventions from funders
- Improved review
- Easier completion of applications

**In conducting research, researchers can benefit from:**

- Boosted current awareness
- Easier access to facilities
- Reduced administrative overhead

**In publishing researchers benefit as authors and as readers from:**

- Shorter publication delays
- Simplified acknowledgement processes
- Critical awareness of any potential bias
- Richer context and simplified discovery
- Reduced uncertainty and administration around policy compliance

**In reporting on their activities to funders:**

- Improved reporting experiences
- A shift from data collation/entry to verification
- Easier acknowledgement of support for their careers

**In building their careers:**

- Boosted impact and enhanced visibility
- As collaborators, from better understanding of the contributions of others and improved recognition for their own contributions
- Clearer, more complete and complex career records
- Enhanced career recognition and support
- More diverse data sources for recognition and reward

**At every stage, the core benefits for researchers include:**

- Better career representations and reputational enhancement
- Simplified administration, reporting and application processes with reduced overhead and duplication of effort
- Better intelligence about research support and future opportunities for funding and collaboration

{{% /accordion %}}
{{% /accordion-section %}}

---

## Membership & fees

Funders who would like to register their research grants should apply to [join](/membership) as a member. In some cases, your organization may already be a member - so we'll check on that for you as you may be able to register grants under you existing membership. Membership comes with obligations to maintain the metadata record for the long term; our [membership terms](/membership/terms) sets these out. You will also be able to participate in Crossref [governance](/board-and-governance/) such as voting in or standing for our annual board elections. Joining is via a form and a click-through agreement to minimize paperwork. Your first year's membership invoice needs to be settled before a DOI prefix is assigned and your grant registrations can begin.

Membership of Crossref can be layered; you could join directly, or under a [Sponsor](/community/sponsors) such as Europe PMC who handles the technical side of things on your behalf. We have a dedicated fee structure for funders which allows for a much lower annual membership fee (from USD 200.00 to USD 1200.00 depending on annual award value) and a higher per record fee of USD 2.00 for current grants and USD 0.30 for older grants. This allows the cost to be budgeted into the grant itself if desired, rather than using often non-existent administration or operations budgets. Please see our [fees](/fees) page for more information.

## Getting started

If you're reading this far you must be about ready to get going. You'll be joining Wellcome, Australia Research Data Commons (ARDC), Melanoma Research Alliance, OSTI (DOE), and many other Crossref funder members. Welcome aboard!

You need to be a member in order to register grants with us; please sign up as described above. Once you're a member (or in preparation for becoming one), take a look at [our education documentation on registering grants](/documentation/content-registration/content-types-intro/grants) which will walk you through what you need to know and what information you need to send us in order to do this.

---

Please [contact our membership specialist](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001618152) with any questions about joining, or our [technical support specialists](https://support.crossref.org/hc/en-us/requests/new?ticket_form_id=360001642691) with questions about the grants schema or how to register your grants.
