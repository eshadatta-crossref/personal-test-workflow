+++
title = "Research Organization Registry (ROR)"
date = "2022-11-29"
draft = false
author = "Amanda French"
[menu.main]
rank = 2
parent = "Initiatives"
weight = 10
+++

Crossref supports the collection of ROR IDs in the metadata we collect in order to help with the reliable identification and downstream use of affiliation data connected to research outputs.

 > "Author affiliations, and the ability to link them to publications and other scholarly outputs, are vital for numerous stakeholders across the research landscape. ROR is completely open, specifically focused on identifying affiliations, and collaboratively developed by, with, and for key stakeholders in scholarly communications."" - Maria Gould, ROR Project Lead 

## What is ROR?

ROR is the [Research Organization Registry](https://ror.org) -- a global, community-led registry of open persistent identifiers for research organizations. The registry currently includes globally-unique persistent identifiers and associated metadata for [more than 103,000 research organizations](https://ror.org/search).

ROR IDs are specifically designed to be implemented in any system that captures institutional affiliations and to enable connections (via persistent identifiers and networked research infrastructure) between research organizations, research outputs, and researchers. ROR IDs are interoperable with those in other identifier registries, including GRID (which provided the seed data that ROR launched with), the Crossref Funder Registry, ISNI, and Wikidata. ROR data is available under a CC0 Public Domain waiver and can be accessed via [a public API](https://api.ror.org/organizations) and a [data dump](https://doi.org/10.5281/zenodo.6347574).  

<img src="/images/services-images/ror-record.png" alt="The ROR record for the University of St Andrews [https://ror.org/02wn5qz54](https://ror.org/02wn5qz54)" width="60%">  

## Who is ROR?

ROR is operated as a joint initiative by Crossref, [DataCite](https://datacite.org/), and the [California Digital Library](https://cdlib.org/), and was launched with seed data from GRID in collaboration with [Digital Science](https://www.digital-science.com/). These organizations have invested resources into building an open registry of research organization identifiers that can be embedded in scholarly infrastructure to effectively link research to organizations. ROR is not a membership organization (or an organization at all!) and charges no fees for use of the registry or the API. [Read more about ROR's sustainability model.](https://ror.org/blog/2022-10-10-strengthening-sustainability/) 

## Why ROR IDs are an important element of Crossref metadata

For a long time, Crossref only collected affiliation metadata as free-text strings, which made for ambiguity and incomplete data. An author affiliated with the University of California at Berkeley might give the name of the university in any of several common ways: 

* University of California, Berkeley  
* University of California at Berkeley  
* University of California Berkeley  
* UC Berkeley  
* Berkeley
* And likely more…  

While it isn’t too difficult for a human to guess that “UC Berkeley,” “University of California, Berkeley,” and “University of California at Berkeley” are all referring to the same university, a machine interpreting this information wouldn’t necessarily make the same connections. If you are trying to easily find all of the publications associated with UC Berkeley, you would need to run and reconcile multiple searches at best, or miss data completely at worst. 

This is where an affiliation identifier comes in: a single, unambiguous, standardized identifier that will always stay the same. For UC Berkeley, that would be [https://ror.org/01an7q238](https://ror.org/01an7q238).

Crossref members indicated that the ability to associate research outputs with organizations in a clean and consistent fashion was one of their most desired improvements to Crossref metadata. In January of 2022, therefore, Crossref [added support for ROR IDs](/blog/a-ror-some-update-to-our-api/) in its metadata schema and APIs. 

Publishers and service providers can  [implement ROR](https://ror.readme.io/docs/create-affiliation-selection-dropdowntypeahead-widgets) in their systems so that submitting authors and co-authors can easily choose their affiliation from a ROR-powered list instead of typing in free text. Authors themselves do not  have to provide a ROR ID or even know that a ROR ID is being collected. This affiliation information can then be sent to Crossref alongside other publication information. 

<a href="https://gyazo.com/65ef42890287ae978f61add5d36b1d31"><img src="https://i.gyazo.com/65ef42890287ae978f61add5d36b1d31.gif" alt="Collection of ROR IDs in Dryad" width="780"/></a>  

If the submission system you use does not yet support ROR, or if you don't use a submission system, you'll still be able to provide ROR IDs in your Crossref metadata. ROR IDs can be [added to JATS XML](https://ror.readme.io/docs/include-ror-ids-in-jats-xml), and Crossref helper tools will start to support the deposit of ROR IDs. There's also an [OpenRefine reconciler](https://github.com/ror-community/ror-reconciler) that can map your internal identifiers to ROR identifiers.

ROR IDs for affiliations stand to transform the usability of Crossref metadata. While it’s crucial to have IDs for affiliations, it’s equally important that the affiliation data can be easily used. The ROR dataset is CC0, so ROR IDs and associated affiliation data can be freely and openly used and reused without any restrictions.

The ROR IDs registered by members in their Crossref metadata are available via [Crossref’s open APIs](/services/metadata-retrieval/) so that it can be reused by tools, services and anyone interested in this information. Examples include 

* Institutions who want to monitor and measure their research output by the articles their researchers have published
* Funders who want to be able to discover and track the research and researchers they have supported 
* Academic librarians who want to find all of the publications associated with their campus 
* Journals who want to know where authors are affiliated so they can determine eligibility for institutionally sponsored publishing agreements 

The inclusion of ROR IDs in Crossref metadata will eventually help all these entities make all these connections much more easily.

## Get ready to ROR 🦁!

ROR is already working with publishers, funders and service providers who are integrating ROR in their systems, mapping their affiliation data to ROR IDs, and/or including ROR IDs in publication metadata. Libraries and institutional repositories are also beginning to build ROR into their systems and to send ROR IDs to Crossref in their metadata. See the growing list of [active and in-progress ROR integrations](https://tinyurl.com/ror-integrations) for more stakeholders who are supporting ROR.

For information on how ROR IDs are supported in the Crossref metadata, you can take a look at this [.xsd file](https://gitlab.com/crossref/schema/-/blob/5.0/schemas/common5.0.xsd) (under the ‘institution’ element) or in this [journal article example XML](https://gitlab.com/crossref/schema/-/blob/5.0/examples/journal_article_5.0.xml). ROR also has some great [help documentation](https://ror.readme.io/) for publishers and anyone else working with the ROR Registry.

---
[Get in touch with ROR](mailto:support@ror.org) if you have questions or want to be more involved in the project. If you have questions about adding ROR IDs to your Crossref metadata, get in touch with our [support specialists](mailto:support@crossref.org) or ask on the [Crossref Community Forum](https://community.crossref.org/tags/c/content-registration/24/ror). 
