+++
title = "Crossref LIVE Annual"
date = "2022-07-18"
draft = false
author = "Ginny Hendricks"
image = "/images/banner-images/live22-blank.png"
rank = 3
parent = "Get involved"
weight = 9
+++

{{% divwrap blue-highlight %}}
#### Save the date - Crossref Annual Meeting and Board Election #CRLIVE23 - October 31  
{{% /divwrap %}}

## LIVE22 online, October 26, 2022

Our annual meeting, LIVE22, was held online on October 26 at 4:00 PM UTC (universal coordinated time). We invited all our members from 140+ countries, and everyone in our community, to hear the results of our board election and team updates.

Here are some of the outputs from the full session:  

- Ed Pentz spoke about our vision, mission, strategic goals, update on our efforts toward POSI, and our role in ISR (inclusive scholarly record).  
- Vanessa Fairhurst and Isaac Farley highlighted contributors from those in our community who help make our work possible.  
- Kora Korzec led our community speaker session, "Building the Research Nexus together: flash talks", with [presentations by](https://community.crossref.org/c/live22-presentations/36) Hans de Jonge, Bianca Kramer, Javier Arias, Julie Lambert, Lettie Y. Conrad, and Edilson Damasio.   
- Amanda Bartell and Patricia Feeney talked about the state of membership and metadata members register with us.
- Dominika Tkaczyk talked about our work around linking grants to research outputs.
- Lucy Ofiesh led our annual meeting, and board election results looked at our financial performance and the 2023 draft budget.
- [Led by our ambassadors, four in-person 'LIVE' satellite events took place in Lithuania, Brazil, Turkey, and Kenya](https://twitter.com/CrossrefOrg/status/1585305657049509895). Some included a watch party of the meeting, and all included good talks and discussions about metadata and the scholarly record.  

Please check out the materials from LIVE22 below, and cite the outputs as `Crossref Annual Meeting LIVE22, Ocrober 26, 2022 retrieved [date], [https://doi.org/10.13003/i3t7l9ub7t]`:

- [YouTube recording](https://www.youtube.com/watch_popup?v=csy4mf8QNtY) and [Wistia](https://crossref.wistia.com/medias/alpbj23mry?wtime=0s)    
- [Recording transcript](https://docs.google.com/document/d/1h2Pc0orQqnMJoNGx4E6W_Yhr1fiZaeZRqF5ItcjEjkc/preview)    
- [Zoom Q&A transcript](https://docs.google.com/spreadsheets/d/1sPlXW8rg3jMN2IiBbihdAuDc_zacUvIR8vXSiHEbNN0/preview#gid=0)    
- [Google slides](https://docs.google.com/presentation/d/1CWoueRlXDBr764zDxw3docHDWosD8UCqlXzE9zZxyRI/preview?slide=id.gf67b34ab3e_0_8) or [pdf slides](/pdfs/crossref-annual-meeting-live22-presentation-shared.pdf)
- [#CRLIVE22 Twitter stream](https://twitter.com/CrossrefOrg/status/1585301047748222979)   
- [Posters from community guest speakers](https://community.crossref.org/c/live22-presentations/36)    

[Board election results](/board-and-governance/elections)  


---


## The annual meeting archive

Browse our [archive of annual meetings](/crossref-live-annual/archive) with agendas and links to previous presentations from 2001 through 2015. Its a real trip down memory lane!

---

Please [contact us](mailto:feedback@crossref.org) with any questions.
