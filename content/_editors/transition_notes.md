+++
title = "Transition notes"
name = "Transition notes"
date = "2017-01-07"
draft = false
description = "Notes on transitioning from old site to new"
author = "Geoffrey Bilder"

+++

## Steps for Transfering site to Gitlab & AWS

- ~~Setup AWS S3 buckets~~
- ~~Replace Travis CI with Gitlab CI~~
- ~~Handle local redirects in Hugo~~
- ~~Use Hugo's new, modular config mechnism~~
- ~~Understand current search broken-ness~~

- Replace Algolia update script to encode index settings in code.
- Replace member data file generation
- Get working with feature branches
