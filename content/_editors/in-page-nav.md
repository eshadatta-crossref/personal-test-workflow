+++
title = "In Page Nav How To"
name = "In Page Nav How To"
date = "2017-12-11T12:00:00-00:00"
author = "Zach Anthony"
draft = false
type = "Help"
bgimage = "/images/slots/slot1.jpg"

+++

## How to control the In Page Navigation

The "In Page Nav", aka "child nav" or "grand child nav" is the menu composed of grey boxes at the right side of most pages (or at the bottom of page if you are viewing the site on a mobile).

Whereas the main navigation at the left side of the page covers the entire website, the In Page Nav at right is just for pages that are *one level down* from the current one (in other words, child pages).

### Add or Remove pages to the In Page Nav
The In Page Nav is automatically built based on the child pages that exist under the current page, so you can't add or remove things from it directly. In the image below, ```co-access.md``` and all the other pages except ```_index.md``` will automatically be added to the In Page Nav for the main FAQs page, ```../content/faqs/_index.md```, because:

1. all of these files live in the ```/faqs``` folder;
2. and because ```_index.md``` is always the top level or parent page in a folder.

![screenshot image](/images/documentation/child-pages.png "Screenshot of child pages")

In this example, to add a new item to the In Page Nav for the FAQs page you would just add `some-other-page.md``` to the ```/faqs``` folder. Similarly, to remove ```reports.md``` from the menu you would just delete the page (or mark it as draft).

### Changing the order of pages in the In Page Nav
Pages in the In Page Nav are ordered from top to bottom based on the ```weight``` frontmatter for each page. If ```weight``` isn't present, the pages will be listed in the order that they were created (newest at top). When changing the ```weight``` of a page remember that any other page with the same ```weight``` may conflict, so you may sometimes have to update all of the child pages.

### Changing the text that appears in the In Page Nav
When the In Page Nav is automatically built, each box is created from the ```title``` and ```summary``` frontmatter for a page. If ```summary``` isn't set for a page, the first 70 words will be used instead. You can also control ```summary``` by placing ```&lt;!--more--&gt``` in the content; any text above this will be used as the summary.
