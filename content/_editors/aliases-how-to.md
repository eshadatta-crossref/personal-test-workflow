+++
title = "Aliases How To"
name = "Aliases How To"
date = "2020-08-11"
draft = false
author = "Geoffrey Bilder"
aliases = [
    "/_editors/redirects-how-to"
]

+++

## Aliases How To

Let's say that Crossref launches a service called "dipsyxology." and we create a web page for the site at the following url:

```
https://crossref.org/services/dipsyxology
```

Later we conclude that "dipsyxology" was a really stupid name and we decide to rebrand it to "snall" instead. The new page for the re-branded service lives at:

```
https://crossref.org/services/snall
```

But we want anybody who follows the old url to be automatically redirected to the new url.

To to this, we can add the following to the metadata of the *new* snall page.

```
aliases = [
    "/services/dipsyxology"
]
```
And then when hugo builds the site, it will automatically create the redirect for the old "dipsyxology" page. Note that you can add any number of aliases to the list. So, if before the service was called "dipsyxology", the beta name was "wastone", you could have that redirect as well.

```
aliases = [
    "/services/wastone",
    "/services/dipsyxology"
]
```

The frontmatter format for blogs is different that other pages.  To add an alias to a blog, add the alias above "Archives:" as in the example below.

```
aliases: "/blog/similarity-check-news-introducing-the-next-generation-ithenticate/"
aliases: "/blog/planning-platform-move/"
```


Cool, right?

You can see how this works if you edit the markdown for this very document you are reading.

I originally called this "redirects-how-to." But you can get to this page with either of these URLS.  

- [https://crossref.org/_editors/redirects-how-to](/_editors/redirects-how-to)
- [https://crossref.org/_editors/aliases-how-to](/_editors/aliases-how-to)


Also, note that the `aliases` directive *must* occur in the main metadata section of the header.  It will not work if it is included in a menu (e.g. below `[menu.main]` ).

Furthermore, you can’t redirect a page that still exists, it takes precedence, e.g., an _index.md page.  You could delete the page and that will allow the page to be redirected.  If for some reason you need to keep the page, change the file name to something else, e.g., fee-assistance to fee-assistance-old, and make it draft=true.  By making it draft=true, you're making it seem as if it’s still indexable, so even though you are redirecting the content is ‘live’ in the sense of it being in search engines.  Note that if it gets un-drafted at any point and has the same name/location, it will break that alias.  

Have fun.
