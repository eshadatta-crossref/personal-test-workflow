+++
title = "Preview Local How-to"
name = "Preview Local How-to"
date = "2021-03-25"
draft = false
description = "How preview-local.command works, and when to use it"
author = "Zach Anthony"

+++

## What is "preview-local"?
*preview-local* is a small script, or string of commands linked together, that has been created to make it easier to check any changes or updates you make to the Crossref website before you commit them to the repository. This applies both to straightforward textual changes, like adding a blog post, or changes to the functionality or visual design of the site. 

**Note: These directions apply to Mac users only.** If you work with the site on a Windows or Linux computer, *preview-local* won't work for you. However, if that's the case you probably don't need *preview-local* to get a local copy of the site running. If you need any pointers open `preview-local.command` (it lives in the root of the repository) and take a look at what it does. 

## When and why to use *preview-local*
Reviewing and testing changes locally - on your own computer - before you submit them has two main advantages:

1. **It's much quicker.** Making a change, committing it, pushing it to the repository and then waiting for the development or staging site to rebuild can take several minutes. Using *preview-local* typically takes just a couple of seconds before you can view your work, and any amends you make are automatically reflected. 
2. **It's 'nicer'.** Every time you commit and push a change to the repository, it kicks off a rebuild of the entire development or staging site. This takes server resources, and if multiple people are making changes to the site it can result in a queue of rebuilds - even more resources. If you make a change, commit and push, then review the change and find you need to fix something or make another change, it all adds up. It's just 'nicer' to do all your work locally and then commit and push changes only once you are happy that they are complete. 

## How to use *preview-local*
*preview-local* exists in the `cr-site` site repository, so if you have cloned the repository to your laptop or computer, you already have it. You'll also need to make sure that you have the Google Chrome browser installed on your Mac. 

To use `preview-local` follow these steps:
1. Open Atom and look for `preview-local.command` in the file view at the left of the window. Right click on this and select **Reveal in Finder**. 
2. In Finder, double-click `preview-local.command` to run it. This will open a Terminal window on your Mac and run the script. 

If you are comfortable using Finder and already know where to find the `cr-site` repository, you can skip to Step 2 above. If you prefer to use the Terminal, just `cd` to the `cr-site` folder and type `sh preview-local.command` and hit **Enter**. 

### The first time you run *preview-local*
The first time *preview-local* runs you will see it takes several minutes to download a number of necessary applications to your computer, including Homebrew and Hugo. Homebrew helps your computer to run a website locally and is unique to Macs. Hugo is the application that actually powers the website - it also runs on the development, staging and live web servers.

### Regular use 
Once these are installed or if you've run *preview-local* before, after you run it you'll quickly see various messages that might say `Hugo already installed` and possibly a few error messages. These are usually fine. If you eventually see the message `Opening in existing browser session` and then a small table listing all the pages in the site, things are going well. 

Eventually you'll see the final message `Web Server is available at http://localhost:1313/ (bind address 127.0.0.1)` and Chrome will automatically be opened at an address of `localhost:1313`. At first you'll see a `This site can't be reached` error, but after a second or two the site will come up.

You can now navigate this special local version of the Crossref site in Chrome just as you would the live site. Swap back to Atom and make any changes you need to, such as creating a new blog page, and each time you save the file(s) you are working on, you can swap back over to Chrome to review your changes. It may take a second or two for changes to be visible in Chrome; you'll see the site refresh when it's ready.







