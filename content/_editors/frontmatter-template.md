+++
title = "Frontmatter templates"
name = "Frontmatter templates"
draft = false
author = "Zach Anthony"

+++

## Re-usable, pre-formatted frontmatter templates
The frontmatter templates below can be used to quickly and confidently create new pages in the site. Just copy the contents of the boxes and paste them into the top of a new markdown (.md) file.

Refer to the [frontmatter primer](../frontmatter-primer) for more information on proper frontmatter formatting and syntax.

### Top level page
As this is a top level page (i.e. it appears in the main menu at the top-most level) note that it does not have a ```parent``` specified.

```
+++
title = "Sample title of a top level page"
draft = false
[menu.main]
name = "Short title"
+++
This is the body text of the page. iLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
```

### Sub page
```
+++
title = "Page title"
draft = false
[menu.main]
name = "Menu title"
parent = "Sample title of a top level page"
+++
This is the body text of the page. iLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
```

### About Us page
```
+++
title = "About us"
date = "2016-04-19T11:13:07+02:00"
draft = false
author = "Ginny Hendricks"
[menu.main]
name = "About us"
weight = 1
+++
```

### Blog page
It isn't necessary to specify the ```parent``` for a blog page.

```
+++
title = "Page title"
draft = false
[menu.main]
name = "Menu title"
categories = "Category 1, Category 2"
+++
This is the body text of the page. iLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
```

### Get Involved page
```
+++
title = "Affiliates"
name = "Affiliates"
date = "2016-05-09T13:27:07-05:00"
draft = false
[menu.main]
parent = "Get Involved"
weight = 1
+++
```

### Learn More page
```
+++
title = "Cited by Policy"
name = "Cited by Policy"
date = "2016-08-11T14:52:00-05:00"
draft = false
author = "Susan Collins"
[menu.main]
parent = "Get help"
weight = 1
+++
```


### Services page
```
+++
title = "Cited-by Linking"
name = "Cited-by Linking"
date = "2016-05-09T13:27:07-05:00"
draft = false
[menu.main]
parent = "Find a service"
author = "Ginny Hendricks"
weight = "1"
+++
```
