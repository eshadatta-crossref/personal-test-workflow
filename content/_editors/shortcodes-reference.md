+++
title = "Shortcodes Reference"
name = "Shortcodes Reference"
date = "2018-01-25"
draft = false
author = "Bruce Murray"
toc = true

+++

## Introduction
Shortcodes are a Hugo method for controlling and layout of content without resorting to hand-coding HTML and CSS. So for example, a shortcode will let you define an area of the page to be laid out in two columns rather than the standard one. See below for more info. Like HTML, shortcodes must come before and after the area you wish to layout.

Shortcodes can be entered directly into the markdown like this:

&#123;&#123;% shortcode %&#125;&#125; *some content* &#123;&#123;% /shortcode %&#125;&#125;

The curly brackets and %s tell Hugo that you are using a shortcode.

## Built-in Shortcodes
Hugo contains a number of built-in shortcodes. More info on these is available on the [Hugo website](https://gohugo.io/content-management/shortcodes/).

## Custom Shortcodes
We have also developed a number of custom shortcodes that are only available here on the Crossref site. These are detailed below.

### Two column layouts
To create a two column layout, use two pairs of shortcodes to designate the start and finish of **both** columns. You also need to add a 'row' code surrounding the whole block (not each column) which ensures the columns don't run into other text on the page. (you can also use the shortcode column-halves to align the naming with the 2 other column shortcodes listed below)

For example:

<pre>
&#123;&#123;% row %&#125;&#125;
&#123;&#123;% column %&#125;&#125;
This content will end up in Column 1, the left column of two. You can format this like any other part of the page.
&#123;&#123;% /column %&#125;&#125;

&#123;&#123;% column %&#125;&#125;
This content will end up in Column 2, the right column of two. You can format this like any other part of the page.
&#123;&#123;% /column %&#125;&#125;
&#123;&#123;% /row %&#125;&#125;
</pre>

produces:

{{% row %}}
{{% column %}}
This content will end up in Column 1, the left column of two. You can format this like any other part of the page.
{{% /column %}}

{{% column %}}
This content will end up in Column 2, the right column of one. You can format this like any other part of the page.
{{% /column %}}
{{% /row %}}

### Three column layouts
To create a three column layout, use three pairs of shortcodes to designate the start and finish of **each** column. You also need to add a 'row' code surrounding the whole block (not each column) which ensures the columns don't run into other text on the page. This is similar to the shortcode for 2 above but uses a slightly different shortcode name as specified in the example below

For example:

<pre>
&#123;&#123;% row %&#125;&#125;
&#123;&#123;% column-thirds %&#125;&#125;
This content will end up in Column 1, the left column of three. You can format this like any other part of the page.
&#123;&#123;% /column-thirds %&#125;&#125;

&#123;&#123;% column-thirds %&#125;&#125;
This content will end up in Column 2, the center column of three. You can format this like any other part of the page.
&#123;&#123;% /column-thirds %&#125;&#125;

&#123;&#123;% column-thirds %&#125;&#125;
This content will end up in Column 3, the right column of three. You can format this like any other part of the page.
&#123;&#123;% /column-thirds %&#125;&#125;
&#123;&#123;% /row %&#125;&#125;
</pre>

produces:

{{% row %}}
{{% column-thirds %}}
This content will end up in Column 1, the left column of three. You can format this like any other part of the page.
{{% /column-thirds %}}

{{% column-thirds %}}
This content will end up in Column 2, the center column of three. You can format this like any other part of the page.
{{% /column-thirds %}}

{{% column-thirds %}}
This content will end up in Column 3, the right column of three. You can format this like any other part of the page.
{{% /column-thirds %}}
{{% /row %}}

### Four column layouts

Similar to above - specified here

<pre>
&#123;&#123;% row %&#125;&#125;
&#123;&#123;% column-quarters %&#125;&#125;
This content will end up in Column 1, the left column of four. You can format this like any other part of the page.
&#123;&#123;% /column-quarters %&#125;&#125;

&#123;&#123;% column-quarters %&#125;&#125;
This content will end up in Column 2, the second column of four. You can format this like any other part of the page.
&#123;&#123;% /column-quarters %&#125;&#125;

&#123;&#123;% column-quarters %&#125;&#125;
This content will end up in Column 3, the third column of four. You can format this like any other part of the page.
&#123;&#123;% /column-quarters %&#125;&#125;

&#123;&#123;% column-quarters %&#125;&#125;
This content will end up in Column 4, the right column of four. You can format this like any other part of the page.
&#123;&#123;% /column-quarters %&#125;&#125;
&#123;&#123;% /row %&#125;&#125;
</pre>

produces:

{{% row %}}
{{% column-quarters %}}
This content will end up in Column 1, the left column of four. You can format this like any other part of the page.
{{% /column-quarters %}}

{{% column-quarters %}}
This content will end up in Column 2, the second column of four. You can format this like any other part of the page.
{{% /column-quarters %}}

{{% column-quarters %}}
This content will end up in Column 3, the third column of four. You can format this like any other part of the page.
{{% /column-quarters %}}

{{% column-quarters %}}
This content will end up in Column 4, the right column of four. You can format this like any other part of the page.
{{% /column-quarters %}}
{{% /row %}}

### Services blocks
These are blocks of text that are given a particular style, and you can control the color of the heading. Heading colors can be any of the main Crossref brand colors: red, blue, yellow, darkgrey or lightgrey (in the format `service-[color]`.)

<pre>
&#123;&#123;% divwrap service-red %&#125;&#125;
## Heading

Some text

&#123;&#123;% /divwrap %&#125;&#125;
</pre>

produces:

{{% divwrap service-red %}}


### Modal image popups

#### The minimum
<pre>
&#123;&#123;&lt; figure-linked
	src="/images/documentation/metadata-users-uses.png"
	alt="Metadata users and uses: metadata from Crossref APIs is used for a variety of purposes by many tools and services"
	id="image31"
&gt;&#125;&#125;
</pre>

#### With large and title parameters included
<pre>
&#123;&#123;&lt; figure-linked
	src="/images/documentation/metadata-users-uses.png"
	large="/images/documentation/metadata-users-uses.png"
	alt="Metadata users and uses: metadata from Crossref APIs is used for a variety of purposes by many tools and services"
	title="This is the title"
	id="image31"
&gt;&#125;&#125;
</pre>

Need some sort of unique id field too so that the button opens the right popup.

The modal width in the documentation section is set to much wider than standard here so it will popup to about 90% of the screen width.

These produce the following.

{{< figure-linked
	src="/images/documentation/metadata-users-uses.png"
	alt="Metadata users and uses: metadata from Crossref APIs is used for a variety of purposes by many tools and services"
	id="image31"
>}}



## Heading

Some text

{{% /divwrap %}}

### Highlight boxes
These are blocks of text with a colored background used to call out key information. Background colors can be any of the main Crossref brand colors: red, blue, yellow, darkgrey or lightgrey (in the format `*color*-highlight`.)


<pre>
&#123;&#123;% divwrap blue-highlight %&#125;&#125; A bit of text &#123;&#123;% /divwrap %&#125;&#125;
</pre>

Produces:

{{% divwrap blue-highlight %}} Use blue-highlight for a blue box. [link](#) {{% /divwrap %}}

<br />and similarly:

{{% divwrap red-highlight %}} Use red-highlight for a red box. [link](#) {{% /divwrap %}}

{{% divwrap yellow-highlight %}} Use yellow-highlight for a yellow box. [link](#) {{% /divwrap %}}

{{% divwrap lightgrey-highlight %}} Use lightgrey-highlight for a light grey box. [link](#) {{% /divwrap %}}

{{% divwrap darkgrey-highlight %}} Use darkgrey-highlight for a dark grey box. [link](#) {{% /divwrap %}}


### Highlight box sizing
Optional layout attrbute (you can see it in action on /content/members-area/_index.md)

{{% row layout=equal %}}

It will mean that all the divwraps within columns, within that row, will be the same height.

The magic is you don't have to set any lengths - the column with the most content will auto size to the content height, and the shorter one will stretch to equal it. So it'll stretch to whatever the length of the longest column is.    


### Float left/right or center
These blocks allow you to float a block of content, such as an image or text, to the left or right of the content area or center. By 'float' we mean that the object sits to the left or right of content, and other content wraps around it. This floated element will have a width of 50% of the content area. Conversely if you center something it, it will sit by itself in the middle of the main content area. You can use imagewrap to force a `figure` (see below) to be left, right or center aligned.

<pre>
&#123;&#123;% divwrap align-left %&#125;&#125; - start the block
&#123;&#123;% /divwrap %&#125;&#125; - end the block
</pre>

or:

<pre>
&#123;&#123;% divwrap align-right %&#125;&#125; - start the block
&#123;&#123;% /divwrap %&#125;&#125; - end the block
</pre>

The divwrap shortcode does add some padding/margin which may not be needed so you can alternatively use the following which will just float the image left or right.

<pre>
&#123;&#123;% imagewrap left %&#125;&#125; - start the block
&#123;&#123;% /imagewrap %&#125;&#125; - end the block
</pre>

or:

<pre>
&#123;&#123;% imagewrap right %&#125;&#125; - start the block
&#123;&#123;% /imagewrap %&#125;&#125; - end the block
</pre>

**Note that Hugo contains a number of built-in shortcodes, and one in particular, ```figure```, can be very useful for controlling how images are displayed. More information on this shortcode is available on the [Hugo website](https://gohugo.io/content-management/shortcodes/#figure).**


### Quotes with and without citations

If you want to add a citation and some special styling, a special shortcode has been setup: `quotecite`. Using it you can specify an alternate citation (attribution) for any quote.

Entering the following:

<pre>
&#123;&#123;% quotecite "Geoffrey Bilder" %&#125;&#125;
Go placidly amid the noise and waste, And remember what comfort there may be in owning a piece thereof.
&#123;&#123;% /quotecite %&#125;&#125;
</pre>

Produces:

{{% quotecite "Geoffrey Bilder" %}}
Go placidly amid the noise and waste, And remember what comfort there may be in owning a piece thereof.
{{% /quotecite %}}

If you don't have a citation, just leave out the `name` part of the shortcode. **Be sure to place the quote on its own line(s) between the opening and closing shortcodes. If you put it on the same line, it will break formatting.**

### Simple Accordion

An accordion can be used as a way to compress longer content when a user first arrives on the page. There is a [good example](https://www.crossref.org/membership/about-sponsors/) of it in use on the live site. Structurally, an accordion is made up of a wrapper like so:

<pre>
&#123;&#123;% accordion %&#125;&#125;
...
&#123;&#123;% /accordion %&#125;&#125;
</pre>


And one or more sections:

<pre>
&#123;&#123;% accordion-section "Section Title" %&#125;&#125;
Section content...
&#123;&#123;% /accordion-section %&#125;&#125;
</pre>

So for example the below markdown:

<pre>
&#123;&#123;% accordion %&#125;&#125;

&#123;&#123;% accordion-section "Show me some cheese" %&#125;&#125;

Melted cheese the big cheese boursin. Lancashire queso melted cheese caerphilly brie swiss paneer paneer. Melted cheese cheddar cheesy feet cauliflower cheese gouda babybel ricotta cheese and biscuits. Cottage cheese feta st. agur blue cheese cheese and biscuits goat cauliflower cheese cottage cheese mascarpone. Cheesy feet.

&#123;&#123;%/accordion-section %&#125;&#125;

&#123;&#123;% accordion-section "Show me some more cheese!" %&#125;&#125;

Lancashire jarlsberg st. agur blue cheese. Boursin croque monsieur croque monsieur bavarian bergkase halloumi the big cheese squirty cheese pepper jack. Brie port-salut cream cheese cream cheese rubber cheese taleggio mozzarella the big cheese. Paneer say cheese cheesecake croque monsieur red leicester ricotta camembert de normandie cheese triangles. Lancashire bavarian bergkase cheese triangles.

&#123;&#123;% /accordion-section %&#125;&#125;

&#123;&#123;% /accordion %&#125;&#125;
</pre>

produces:

{{% accordion %}}
{{% accordion-section "Show me some cheese" %}}
Melted cheese the big cheese boursin. Lancashire queso melted cheese caerphilly brie swiss paneer paneer. Melted cheese cheddar cheesy feet cauliflower cheese gouda babybel ricotta cheese and biscuits. Cottage cheese feta st. agur <a href="#">blue cheese cheese and biscuits</a> goat <a href='#'>cauliflower cheese cottage cheese</a> mascarpone. Cheesy feet.
{{% /accordion-section %}}
{{% accordion-section "Show me some more cheese!" %}}
Lancashire jarlsberg st. agur blue cheese. Boursin croque monsieur croque monsieur bavarian bergkase halloumi the big cheese squirty cheese pepper jack. Brie port-salut cream cheese cream cheese rubber cheese taleggio mozzarella the big cheese. Paneer say cheese cheesecake croque monsieur red leicester ricotta camembert de normandie cheese triangles. Lancashire bavarian bergkase cheese triangles.
{{% /accordion-section %}}
{{% /accordion %}}

It is also possible to create a link anywhere in the site that will take the user to a page with accordion items, and to have a specific accordion open on landing. To do this just add a unique ID to the `accordion-section` shortcode like so (no spaces or special characters):

<pre>
 &#123;&#123;% accordion-section "Show me some cheese" "a-unique-code" %&#125;&#125;  
</pre>

 And then wherever you create your link to the accordion item, append the unique code onto it like so:

<pre>
 Here's an example of a link to an accordion that you'd like to be [open on arrival](https://www.crossref.org/membership/billing/#a-unique-code).
</pre>  


### Boxes with borders

To force borders in and around a markdown table use the `table-bordered` shortcode:

<pre>
&#123;&#123;% table-bordered %&#125;&#125;
table content here
&#123;&#123;% /table-bordered %&#125;&#125;
</pre>

The table will then appear as follows.

{{% table-bordered %}}
| Document     | Description     |
| :------------- | :------------- |
| [Copy guidelines](/_editors/copy-guidelines)       | Guidelines on tone, language and style.       |
| [Branding guidelines](https://assets.crossref.org/private/docs/Crossref_BrandGuides_March2016.pdf) | Our latest branding guidelines. |
{{% /table-bordered %}}

Compared to

| Document     | Description     |
| :------------- | :------------- |
| [Copy guidelines](/_editors/copy-guidelines)       | Guidelines on tone, language and style.       |
| [Branding guidelines](https://assets.crossref.org/private/docs/Crossref_BrandGuides_March2016.pdf) | Our latest branding guidelines. |


### Total Results For an API call

The `total-results` shortcode is a way to access statistical data from the Crossref API. There are various ways to use it, and they all depend on the next part of the shortcode - ie the parameter passed.

**Example 1:**
If you use: 
<pre>
 &#123;&#123;< total-results "/works?rows=0" >&#125;&#125;
</pre>

then it just produces a hard-coded number e.g. 134524363 out.

**Example 2:**
If you use: 
<pre>
 &#123;&#123;< total-results "/members?rows=0" >&#125;&#125;
 </pre>

then it counts the number of members in the overnight generated /data/dashboard/member-list.json and produces this.

If it’s anything else then it will attempt to get the ‘total-results’ element from an API call passed in the second variable (i.e. not the downloaded file).

### Icons from font-awesome

You can easily show and style icons from [Font-Awesome](https://origin.fontawesome.com/icons?d=gallery) with the shotcode shown below:

<pre>
&#123;&#123;< icon name="fa-star">&#125;&#125;
</pre>

Which outputs {{< icon name="fa-star">}} in black using the standard current font-size.

You can also optionally specify the **style**, **color** and **size** of icon as shown below.

{{< icon name="fa-star" color="crossref-blue" size="small" >}}
`< icon name="fa-star" color="crossref-blue" size="small" >`

{{< icon name="fa-star" color="crossref-red" size="medium" >}}
`< icon name="fa-star" color="crossref-red" size="medium" >`

{{< icon style="far" name="fa-star" color="crossref-red" size="medium" >}}
`< icon style="far" name="fa-star" color="crossref-red" size="medium" >`

{{< icon name="fa-star" color="crossref-lightgrey" size="large" >}}
`< icon name="fa-star" color="crossref-lightgrey" size="large" >`

{{< icon name="fa-star" color="crossref-yellow" size="xlarge" >}}
`< icon name="fa-star" color="crossref-yellow" size="xlarge" >`

If you do not specify a style it will default to "fa". Note that icons marked as "Pro" on the Font Awesome website are not supported.

Sizes can be specfied as *small*, *medium*, *large* and *xlarge* as shown above. It isn't currently possible to specify an exact size. *small* is equivalent to normal text size and will not cause an issue with line height.

Colors can be specified as *crossref-red*, *crossref-blue*, *crossref-yellow*, *crossref-lightgrey* and *crossref-darkgrey*. These correspond with the Crossref brand palette. It isn't currently possible to specify an exact colour.

### Brand keywords

You can now easily drop in individual blocks for the 5 brand keywords: Rally, Tag, Run, Play, Make. Each block is called using the following shortcode:

<pre>
&#123;&#123;% keyword "rally" %&#125;&#125;
</pre>

where *rally* in the example above can be any of "rally", "tag", "run", "play" or "make" (must be lowercase, no other words are supported).

This produces:

{{% keyword "rally" %}}

Keywords can be stacked as seen at the top of the [Strategy page](/strategy). Note that there is no spacing automatically added above or below one of the keywords blocks, so you may need to add a ```&nbsp;``` above and/or below it to add spacing.

*NB: The text of the various keywords can be updated in ```/themes/crossref/layouts/shortcodes/keyword.html```.*

### Prefilled Contact Formatting

The shortcode is called as follows

&#123;&#123;< contact-form-prefilled url="/contact" text="linking to option 3" option="3" >&#125;&#125;

Which results in a link as follows.
{{< contact-form-prefilled url="/contact" text="linking to option 3" option="3" >}}

This will take you to the contact form with Option 3 pre-selected

You can view the link in the example to see how to use it directly as an URL without the need for a shortcode.

