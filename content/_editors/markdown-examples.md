+++
title = "Markdown examples"
name = "Markdown Examples"
date = "2016-12-21T06:36:41+01:00"
draft = false
description = "Markdown examples"
author = "Geoffrey Bilder"

+++

## H2 header

**(never use an H1 header, always start with H2 so- `##`)**

Paragraphs are separated by a blank line.

2nd paragraph.

You can format with *Italic*, **bold**, `monospace` and ~~strikethrough~~.

Formatting can be combined:

*Now is the **time** for all `good` people...*

A horizontal rule follows.

***

Itemized lists look like:

* foo
* bar
* baz


Here's a numbered list:

1. first item
2. second item
3. third item

Now a nested list:

1. First, get these ingredients:
  * carrots
  * celery
  * lentils
2. Boil some water.
3. Dump everything in the pot and follow this algorithm:
  * find wooden spoon
  * uncover pot
  * stir
  * cover pot
  * balance wooden spoon precariously on pot handle
  * wait 10 minutes
  * goto first step (or shut off burner when done)

   Do not bump wooden spoon or it will fall.

Sometimes you want to quote something...

> Block quotes are written like so.
>
> They can span multiple paragraphs, if you like.
>
> Capitalise on low hanging fruit to identify a ballpark value added activity to beta test. Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on focusing solely on the bottom line.

Use 3 dashes for an em-dash (aka em rule). For example:

> A flock of sparrows---some of them juveniles---alighted and sang.


 Use 2 dashes for ranges. For example:

> it's all in chapters 12--14

  Three dots ... will be converted to an ellipsis.

  Sometimes Hugo will try to helpfully interpret characters and convert them into a special character. So, for example it will change the sequence `(c)` as a shorthand for the copyright symbol and convert it to "©" symbol. There are two ways you can avoid this behavior:

  - "code-fence" the characters as we have done above. But this will make the text look like a code block and will mess up the formatting:

  501\`(c)\`6 (displays as 501`(c)`6)

  - Escape the characters using a backslash like this:

  `501\(c\)6` (displays as 501\(c\)6)



Unicode is supported. The next time you are in a Sichuan restaurant, order the 重庆辣子鸡.

So is emoji- 💩. But if you use emoji, we will judge you.

## An h2 header

You can show inline code using the monospace function. Here is some Python3: `print("hello, world")`. Please don't use any `<blink>` tags.

You can use Github style code fences to show blocks of code. This makes copying & pasting code easier.

```
# Let me re-iterate ...
for i in 1 .. 10 { do-something(i) }
```

You can even specify syntax highlighting.

## HTML

``` html
<section id="main">
  <div>
    <h1 id="title">{{ .Title }}</h1>
    {{ range .Data.Pages }}
      {{ .Render "summary"}}
    {{ end }}
  </div>
</section>
```


## Python

``` Python
import time
# Quick, count to ten!
for i in range(10):
    # (but not *too* quick)
    time.sleep(0.5)
    print i
```

## XML

``` XML
<foo>
  <bar piffle="twaddle">wiggle</bar>
</foo>
```

### JSON

The JSON example here:

``` JSON
status: "ok",
message-type: "work-list",
message-version: "1.0.0",
message: {
query: {
search-terms: null,
start-index: 0
},
items-per-page: 0,
items: [ ],
total-results: 21119562,
facets: { }
```

JS here:

``` JS
status: "ok",
message-type: "work-list",
message-version: "1.0.0",
message: {
query: {
search-terms: null,
start-index: 0
},
items-per-page: 0,
items: [ ],
total-results: 21119562,
facets: { }
```

Note that, because Github code fences are not considered standard Markdown, the above may not display properly if you are using a WYSIWYG MarkDown editor. However, it will render properly once published to the Hugo system.

### An h3 header

Here's a link to [a website](http://foo.bar), and to a [local
doc](local-doc.html).

Sometimes link URLs will contain characters that are meaningful in markdown and will be interpreted as mmarkdown, not park of the URL (e.g. parentheses, brackets, etc). To deal with these you can escape the characters in one of two ways:


You can escape the characters:

[501\(c\) corporation](https://en.wikipedia.org/wiki/501\(c\)_organization)

Or you can encode the characters:

[W501\(c\) corporation](https://en.wikipedia.org/wiki/501%28c%29_organization)

For external sites that take the user out of the website you can use html to make the links open in a new tab, e.g:

`<a href="http://example.com/" target="_blank">Hello, world!</a>`

So your link would work like this if you wanted to link to <a href="http://search.crossref.org/" target="_blank">Metadata Search</a> which is a rather excellent resource.

You can also link to a [section heading in the current
doc](#an-h2-header) or [another doc](../data-how-to#the-global-footer). In both cases the link target is simply a heading you want to target, in slug format (i.e. lowercase, hyphens instead of spaces). So for example if the heading is:

```
## Heading Target
```

your link would look like:

```
[I'm the link text](heading-target)
```

Here's a footnote [^1].

[^1]: Footnote text goes here.

## Tables

Tables can look like this:

size | material | color
--- | --- | ---
9 | leather | brown
10  | hemp canvas | natural
11 | glass | transparent

Or you can control the alignment of cells in a table using colons:

| left aligned  | centered  | right aligned     |
|:--------------|:---------:|------------------:|
|foo            |bar        |1.00               |
|baz            |bonk       |.50                |
|wibble         |wobble     |25.00              |

The key to tables is proper formatting in the markdown. Here is an example:
```
size | material | color
--- | --- | ---
9 | leather | brown
10  | hemp canvas | natural
11 | glass | transparent
```
Note the lack of leading and following |'s, as well as the lack of tabs and proper column alignment - just a space before and after each | simple is sufficient. **And always make sure that there is a blank line above a table.**

If you want a little help generating tables, you can use this handy [online markdown table generator](http://www.tablesgenerator.com/markdown_tables) or this one, [Convert Excel to Markdown Table](https://tableconvert.com/excel-to-markdown), and remove some of the tedium.



## Images

It is best practice to follow Hugo markdown guidelines over ours.  So the **preferred** method of adding an image is by using the [Hugo figure shortcode](https://gohugo.io/extras/shortcodes/) which will also give you more control over the placement of an image. This will not display correctly in WYSIWYG editors, but it will render correctly once processed by Hugo.  

Images can be specified like so:

```
&#123;&#123;< figure src="http://assets.crossref.org/logo/crossref-logo-200.svg" alt="Crossref logo">&#125;&#125;
```

You can find more information on Hugo's built-in shortcodes [here](https://gohugo.io/content-management/shortcodes/#use-hugo-s-built-in-shortcodes) or on our [editor's shortcode reference page](/_editors/shortcodes-reference).

Other methods to add images though not best practice:  

```
![example image](http://assets.crossref.org/logo/crossref-logo-200.svg "An exemplary image")
```

which gives you:
![example image](http://assets.crossref.org/logo/crossref-logo-200.svg "An exemplary image")

If you need more control over the size of an image you'll need to turn to simple HTML code, as markdown doesn't really allow for sizing. In this case you can give a specific width and/or height in pixels, or you can use a percentage figure so that your image works responsively (you'll probably want to do it this latter way). So for example you can make the above image scalable like so:

```
<img src="http://assets.crossref.org/logo/crossref-logo-200.svg" alt="An exemplary image" width="100%" />
```
where ```width="100%"``` tells the browser to make the image as wide as its containing element - in this case the body area of the page. This code gives you the following (drag the size of your browser window larger/smaller to see the scaling):

<img src="http://assets.crossref.org/logo/crossref-logo-200.svg" alt="An exemplary image" width="100%" />

However you could equally constrain the size of the image to something quite small like so:

```
<img src="http://assets.crossref.org/logo/crossref-logo-200.svg" alt="An exemplary image" width="200px" />
```

which gives you:
<img src="http://assets.crossref.org/logo/crossref-logo-200.svg" alt="An exemplary image" width="200px" />


Display math using LaTeX by surrounding the LaTex in double-dollarsigns:

$$I = \int \rho R^{2} dV$$

$$\Sigma_{x=2}^{10} 1/x$$

Note that math support is not standard MarkDown and, as such, will probably not display in WYSIWYG editors- it will, however, render correctly when processed by Hugo.

And note that you can backslash-escape any punctuation characters
which you wish to be displayed literally, ex.: \`foo\`, \*bar\*, etc.

## Embed custom HTML, e.g. videos
It is possible to simply copy and paste any block of HTML text into a Markdown file. For example if you paste the following YouTube embed code into a page:

```
<iframe width="520" height="315" src="https://www.youtube.com/embed/L0GOa859dZk" frameborder="0" allowfullscreen></iframe>  

```

You will end up with:

<iframe width="520" height="315" src="https://www.youtube.com/embed/L0GOa859dZk" frameborder="0" allowfullscreen></iframe>  

Another way to embed a YouTube video, is to use the portion of the video URL in the following shortcode.  Use the letters at the end of the URL as in the example below:    

\(https://youtu.be/**Csh6GRegWxw**\)  

Add the portion of the URL in the shortcode like this:  \{\{< youtube **Csh6GRegWxw** css-class >\}\}  

You will end up with
{{< youtube Csh6GRegWxw css-class >}}


## DOIs

## XML


Hugo will always try to interpret XML code in a page, probably because it thinks it is HTML - they are very similar. When it does this, it hides the actual <tag>...</tag> code and all the remaining test is bunched together without any formatting. So for example when what you want is:

```
<doi_batch xmlns="http://www.crossref.org/schema/4.4.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:fr="http://www.crossref.org/fundref.xsd" xsi:schemaLocation="http://www.crossref.org/schema/4.4.2 http://www.crossref.org/depositSchema/crossref4.4.2.xsd" version="4.4.2">
<head>
<doi_batch_id>org.crossref.early.001</doi_batch_id>
<timestamp>000002</timestamp>
<depositor>
<depositor_name>Crossref</depositor_name>
<email_address>jhanna@crossref.org</email_address>
</depositor>
<registrant>Crossref</registrant>
</head>
```

Instead you get:

<doi_batch xmlns="http://www.crossref.org/schema/4.4.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:fr="http://www.crossref.org/fundref.xsd" xsi:schemaLocation="http://www.crossref.org/schema/4.4.2 http://www.crossref.org/depositSchema/crossref4.4.2.xsd" version="4.4.2">
<head>
<doi_batch_id>org.crossref.early.001</doi_batch_id>
<timestamp>000002</timestamp>
<depositor>
<depositor_name>Crossref</depositor_name>
<email_address>jhanna@crossref.org</email_address>
</depositor>
<registrant>Crossref</registrant>
</head>

For this reason you should always use a special Markdown code to encapsulate (surround) blocks of XML code: &lsquo;&lsquo;&lsquo;. Using &lsquo;&lsquo;&lsquo; preserves the line-by-line formatting of XML code without requiring adding breaks, and surrounds the code snippet with special formatting designed to pick it out from standard text.

&lsquo;&lsquo;&lsquo; should be placed at the start and end of a block of code, on its own blank line, like so:

<table>
    <thead>
        <tr>
            <th>
                This Markdown
            </th>
            <th>
                Produces:
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                &lsquo;&lsquo;&lsquo;<br />
                    &lt;depositor><br />
                        &lt;depositor_name>Crossref&lt;/depositor_name><br />
                        &lt;email_address>jhanna@crossref.org&lt;/email_address><br />
                    &lt;/depositor><br />
                &lsquo;&lsquo;&lsquo;
            </td>
            <td>
                <code>
                    &lt;depositor><br />
                        &lt;depositor_name>Crossref&lt;/depositor_name><br />
                        &lt;email_address>jhanna@crossref.org&lt;/email_address><br />
                    &lt;/depositor>
                </code
            </td>
        </tr>
    </tbody>
</table>


You can also use this code to encapsulate just a word or sentence, inline, if you prefer, like so: &lsquo;&lsquo;&lsquo;&lt;title>This is a title&lt;/title>&lsquo;&lsquo;&lsquo;, which produces ```<title>This is a title</title>```.

You can use &lsquo;&lsquo;&lsquo; inside accordions or other shortcodes, but in this case its always a good idea to leave a blank line before and after a shortcode:

&#123;&#123;% accordion-section %&#125;&#125;

&lsquo;&lsquo;&lsquo;
&lt;title>This is a title&lt;/title>
&lsquo;&lsquo;&lsquo;

&#123;&#123;% accordion-section %&#125;&#125;

doi: 10.5555/12345678

## A collapsible section containing markdown

<pre>
< details >
  < summary >Click to expand!< / summary >

  ## Heading
  1. A numbered
  2. list
     * With some
     * Sub bullets
< / details >
</pre>

produces:

<details>
<summary>Click to expand!</summary>

  ## Heading
  1. A numbered
  2. list
     * With some
     * Sub bullets
</details>  
<br/>

NB: Do not include spaces between text in the `< >`, e.g. \<details\>, \<\/details\>.    
NB: Make sure you have an empty line after the closing </summary> tag, otherwise the markdown/code blocks won't show correctly.  
NB: Make sure you have an empty line after the closing </details> tag if you have multiple collapsible sections.  
