+++
title = "Embedding videos"
name = "Embedding videos"
date = "2020-11-18"
draft = false
author = "Rosa Clark"

+++

There are two platforms we use to embed video on our web pages:  Wistia and YouTube.  

Wistia is only used to embed videos on our services pages because of the ‘clean’ look of the player.  YouTube is used to host all other videos.  (See sample screenshots below.) 

See Ginny or Rosa if you need a ‘clean’ look to your video and do not want to host it on YouTube.  

{{< figure src="/images/documentation/embed-videos.png" alt="screenshot of video player" width="70%" >}}  
