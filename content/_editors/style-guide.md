+++
title = "Web Style Guide"
name = "Web Style Guide"
date = "2022-11-09T12:00:00-00:00"
draft = false

+++

## Web Style Guide: matching the style of the new Crossref website in external applications

Note (as of Feb 2021) that this was created in 2017 and needs to be reviewed by UI/frontend colleagues.

This page provides information on how to style external applications to match the visual style of the new Crossref website using HTML snippets and existing stylesheets.

All pages are compiled from templates that are contained within the [/themes/crossref/layouts](https://github.com/CrossRef/cr-site/tree/master/themes/crossref/layouts) directory of the GitHub repository for the site. Although they include proprietary tags (look for the curly brackets) used by Hugo the static site generator, Hugo, for the most part layout HTML should be clear. **Do not make changes to the template files contained here.**

You may also wish to simply use a browser to view source on any page in the site with a template that is suitable for your use, and pull whatever you need from there.  

### Stylesheets, fonts, layout
The new site uses Bootstrap for layout, and stylesheets are written in SASS. The font used is standard per Crossref brand guidelines, Helvetica Neue, and appropriate font files are available via the site. An icon font from Font Awesome is also used. Links to compiled stylesheets and the icon font are as follows:

```
<link href="/css/bootstrap.min.css" rel="stylesheet">

<link href="/css/crossref.css" rel="stylesheet">

<link href="/css/partials/_fonts.css" rel="stylesheet">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
```

The original SASS files can be found in the GitHub repo, in the [/static](https://github.com/CrossRef/cr-site/tree/master/static) folder.  **Do not make changes to any of the files contained in the ../static directory.**

### Layout HTML
Pre-compiled templates containing HTML for Common page elements like the header (including navigation) and the footer can be found in in [/themes/crossref/layouts/partials](https://github.com/CrossRef/cr-site/tree/master/themes/crossref/layouts/partials).

### Elements not covered by existing stylesheets or templates
If you have an element that isn't covered by existing styles or layout HTML, please contact [Ginny Hendricks](mailto:ghendricks@crossref.org).

### Font samples

#### Direct comparison

- {{% custom-font "HelveticaNeueETW01-55Rg" %}}The quick red fox jumped over the lazy brown dog. 01234567{{% /custom-font %}}
- {{% custom-font "AH-Regular" %}}The quick red fox jumped over the lazy brown dog. 01234567{{% /custom-font %}}
- {{% custom-font "FSMeTrial-Regular" %}}The quick red fox jumped over the lazy brown dog. 01234567{{% /custom-font %}}

#### {{% custom-font "HelveticaNeueETW01-55Rg" %}}Helvetica Neue{{% /custom-font %}}

- <span style="font-family: HelveticaNeueETW01-45Lt;">Helvetica Light</span>
- <span style="font-family: HelveticaNeueETW01-46Lt;">Helvetica Light Italic</span>
- <span style="font-family: HelveticaNeueETW01-55Rg;">Helvetica Regular</span>
- <span style="font-family: HelveticaNeueETW01-56Rg;">Helvetica Regular Italic</span>
- <span style="font-family: HelveticaNeueETW01-65Md;">Helvetica Medium</span>
- <span style="font-family: HelveticaNeueETW01-65Md;">Helvetica Medium Italic</span>
- <span style="font-family: HelveticaNeueETW01-75Bd;">Helvetica Bold</span>
- <span style="font-family: HelveticaNeueETW01-76Bd;">Helvetica Bold Italic</span>

<span style="font-family: HelveticaNeueETW01-55Rg;">Ontwerpers hebben vaak een haat-liefde relatie met “lorem Ipsum-teksten”. <span style="font-family: HelveticaNeueETW01-75Bd;">Ze leveren wel eens discussies op.</span> Sommige ontwerpers hanteren immers de ‘content is koning’-filosofie, dus die beginnen met het ontwerpen van een website of mobile app zonder dat ze content hebben om mee te werken. 01234567890</span>

#### {{% custom-font "AH-Regular" %}}Atkinson Hyperlegible{{% /custom-font %}}

- <span style="font-family: AH-Regular; color:#a9a9a9;">(There is no Atkinson Hyperlegible Light font)</span>
- <span style="font-family: AH-Regular; color:#a9a9a9;">(There is no Atkinson Hyperlegible Light Italic font)</span>
- <span style="font-family: AH-Regular;">Atkinson Hyperlegible Regular</span> 
- <span style="font-family: AH-RegularItalic;">Atkinson Hyperlegible Regular Italic</span> 
- <span style="font-family: AH-Regular; color:#a9a9a9;">(There is no Atkinson Hyperlegible Medium font)</span>
- <span style="font-family: AH-Regular; color:#a9a9a9;">(There is no Atkinson Hyperlegible Medium Italic font)</span>
- <span style="font-family: AH-Bold;">Atkinson Hyperlegible Bold</span> 
- <span style="font-family: AH-BoldItalic;">Atkinson Hyperlegible Bold Italic</span> 

<span style="font-family: AH-Regular;">Ontwerpers hebben vaak een haat-liefde relatie met “lorem Ipsum-teksten”. <span style="font-family: AH-Bold;">Ze leveren wel eens discussies op.</span> Sommige ontwerpers hanteren immers de ‘content is koning’-filosofie, dus die beginnen met het ontwerpen van een website of mobile app zonder dat ze content hebben om mee te werken. 01234567890</span>

#### {{% custom-font "FSMeTrial-Regular" %}}FS Me Trial (no glyphs incl in trial){{% /custom-font %}}

- <span style="font-family: FSMeTrial-Light;">FS Me Light</span> 
- <span style="font-family: FSMeTrial-LightItalic;">FS Me Light Italic</span> 
- <span style="font-family: FSMeTrial-Regular;">FS Me Regular</span> 
- <span style="font-family: FSMeTrial-RegularItalic;">FS Me Regular Italic</span> 
- <span style="font-family: FSMeTrial-Medium;">FS Me Medium (really Bold)</span> 
- <span style="font-family: FSMeTrial-MediumItalic;">FS Me Medium Italic (really Bold Italic)</span> 
- <span style="font-family: FSMeTrial-Bold;">FS Me Bold (really Heavy)</span> 
- <span style="font-family: FSMeTrial-BoldItalic;">FS Me Bold Italic (really Heavy Italic)</span> 

<span style="font-family: FSMeTrial-Regular;">Ontwerpers hebben vaak een haat-liefde relatie met “lorem Ipsum-teksten”. <span style="font-family: FSMeTrial-Bold;">Ze leveren wel eens discussies op.</span> Sommige ontwerpers hanteren immers de ‘content is koning’-filosofie, dus die beginnen met het ontwerpen van een website of mobile app zonder dat ze content hebben om mee te werken. 01234567890</span>

#### {{% custom-font "Inter-Regular" %}}Inter{{% /custom-font %}}

- <span style="font-family: Inter-Light;">Inter Light</span> 
- <span style="font-family: Inter-LightItalic;">Inter Light Italic</span> 
- <span style="font-family: Inter-Regular;">Inter Regular</span> 
- <span style="font-family: Inter-RegularItalic;">Inter Regular Italic</span> 
- <span style="font-family: Inter-Medium;">Inter Medium</span> 
- <span style="font-family: Inter-MediumItalic;">Inter Medium Italic</span> 
- <span style="font-family: Inter-Bold;">Inter Bold</span> 
- <span style="font-family: Inter-BoldItalic;">Inter Bold Italic</span> 

<span style="font-family: Inter-Regular;">Ontwerpers hebben vaak een haat-liefde relatie met “lorem Ipsum-teksten”. <span style="font-family: Inter-Bold;">Ze leveren wel eens discussies op.</span> Sommige ontwerpers hanteren immers de ‘content is koning’-filosofie, dus die beginnen met het ontwerpen van een website of mobile app zonder dat ze content hebben om mee te werken. 01234567890</span>

{{% row %}}

#### Samples of each font for comparison. 
All are placed at the same font size and weight, 14px Regular.

{{% column-quarters %}}
{{% custom-font "Inter-Bold" %}}Inter{{% /custom-font %}}<br />
<span style="font-family: Inter-Regular;">Ontwerpers hebben vaak een haat-liefde relatie met “lorem Ipsum-teksten”. <span style="font-family: Inter-Bold;">Ze leveren wel eens discussies op.</span> Sommige ontwerpers hanteren immers de ‘content is koning’-filosofie, dus die beginnen met het ontwerpen van een website of mobile app zonder dat ze content hebben om mee te werken. 01234567890. Å Ä Ö Ë Ü Ï Ÿ å ä ö ë ü ï ÿ Ø ø</span>
{{% /column-quarters %}}

{{% column-quarters %}}
{{% custom-font "HelveticaNeueETW01-75Bd" %}}Helvetica Neue{{% /custom-font %}}<br />
<span style="font-family: HelveticaNeueETW01-55Rg;">Ontwerpers hebben vaak een haat-liefde relatie met “lorem Ipsum-teksten”. <span style="font-family: HelveticaNeueETW01-75Bd;">Ze leveren wel eens discussies op.</span> Sommige ontwerpers hanteren immers de ‘content is koning’-filosofie, dus die beginnen met het ontwerpen van een website of mobile app zonder dat ze content hebben om mee te werken. 01234567890. Å Ä Ö Ë Ü Ï Ÿ å ä ö ë ü ï ÿ Ø ø</span>
{{% /column-quarters %}}

{{% column-quarters %}}
{{% custom-font "AH-Bold" %}}Atkinson Hyperlegible{{% /custom-font %}}<br />
<span style="font-family: AH-Regular;">Ontwerpers hebben vaak een haat-liefde relatie met “lorem Ipsum-teksten”. <span style="font-family: AH-Bold;">Ze leveren wel eens discussies op.</span> Sommige ontwerpers hanteren immers de ‘content is koning’-filosofie, dus die beginnen met het ontwerpen van een website of mobile app zonder dat ze content hebben om mee te werken. 01234567890. Å Ä Ö Ë Ü Ï Ÿ å ä ö ë ü ï ÿ Ø ø</span>
{{% /column-quarters %}}

{{% column-quarters %}}
{{% custom-font "FSMeTrial-Bold" %}}FS Me Trial (no glyphs incl in trial){{% /custom-font %}}<br />
<span style="font-family: FSMeTrial-Regular;">Ontwerpers hebben vaak een haat-liefde relatie met “lorem Ipsum-teksten”. <span style="font-family: FSMeTrial-Bold;">Ze leveren wel eens discussies op.</span> Sommige ontwerpers hanteren immers de ‘content is koning’-filosofie, dus die beginnen met het ontwerpen van een website of mobile app zonder dat ze content hebben om mee te werken. 01234567890. Å Ä Ö Ë Ü Ï Ÿ å ä ö ë ü ï ÿ Ø ø</span>
{{% /column-quarters %}}

{{% /row %}}


#### New shortcode for fonts

{{% custom-font "FSMeTrial-BoldItalic" %}}There is now a shortcode `custom-font` that you can use to set any text in one of the currently available fonts. Just use the font name as seen in `../themes/crossref/assets/css/partials/_fonts.scss`. View the Markdown version of this page to see how to use the shortcode (it was used for this paragraph).{{% /custom-font %}}