+++
title = "Related Content Linking"
draft = false
author = "Zach Anthony"
date = "2018-02-07"
tags = ["help"]
+++

Starting with version .27, Hugo supports automatically linking related content. Hugo determines what constitutes "related" content by using defined frontmatter elements such as ```tags``` or ```categories```. **Actual textual content is not used for creating relationships (yet).**

## How to link blog posts
Blog posts are automatically linked as related content using the ```categories``` frontmatter element. So long as any blog post has ```categories``` defined AND there are other blog posts with the same categories, links should be created - depending on the threshold that has been set (see below).

See below for a sample of how to configure ```categories``` in a blog post's frontmatter.
```
categories:
     - category 1
     - category 2
     - category 3
     - category 4
```

## How to link other pages
Other pages in the site (basically those that don't have ```categories``` set in the frontmatter) rely on the ```tags``` frontmatter element to form links. Like blog posts, so long as any two or more pages have ```tags``` defined AND there are other pages with the same tags, links should be created - again depending on the threshold that has been set (see below).

See below for a sample of how to configure ```tags``` in a standard page's frontmatter. **NB: note that the proper format for ```tags``` differs from ```categories```.**

```
tags = ["tag 1", "tag 2", "tag 3", "tag 4"]

```

## Configuring threshold and other aspects of related content linking in Hugo  
There are a small number of settings that can be configured to control how Hugo builds related content links. These are all set in the site's .toml file (```testweb.toml``` for testweb etc.). More info on these is available on the [Hugo site](https://gohugo.io/content-management/related/#readout).

**Threshold** is perhaps the most important setting, as the number set here determines which pages will be seen as related and how many total pages will be seen as related. Hugo docs are a bit unclear but basically the number must be between 0-100, and the lower the number the more likely a page will be linked as related.

Since for now Hugo is only looking at tags and categories to create related links, a lower number should mean that less commonality between two page's tags/categories will still create a link. So if two documents have 2 out of 5 of the same tags/categories they might be linked. A higher number here would require more of an exact match between tags.

The **number** of links to related files is set in the template file ```../themes/crossref/partials/related.html``` and is currently set to 5.
