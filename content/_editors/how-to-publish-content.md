+++
title = "How to publish content and deploy updates (updated March 2023)"
date = "2021-03-25"
author = "Zach Anthony"
draft = false
toc = true
+++

## Introduction

  

The Crossref website is stored in GitHub, a collaborative tool for software developers, as a code repository. The Crossref repository has three *branches*, `crossref-website`, `staging` and `development`. A branch is an isolated “snapshot” of the site at a given point in time.

  

- The `crossref-website` branch contains the latest approved content and functionality updates to the site (including bug fixes). **No changes (i.e. git commits) should be made to this branch directly.** The main branch is visible on the live website at [https://www.crossref.org](https://www.crossref.org).

- The `staging` branch is the only branch where new content should be added, amended or deleted. **When new content has been added to the staging branch and is fully approved, a senior editor can merge these updates to the crossref-website branch, effectively making them live on www.crossref.org.** The staging branch is visible on the staging website at [https://www.staging.crossref.org](https://www.staging.crossref.org).

- The `sandbox` branch is used only for development. New functionality is added here, and tested. It may change frequently and may even be broken at times. The sandbox branch is visible on the sandbox website at [http://www.sandbox.crossref.org](http://www.sandbox.crossref.org).

  

Using GitHub provides greater control over the deployment process, ensures that many people can work on content and functionality amends simultaneously and most importantly means that we aways have backups of every version of the site, at every step of its growth.

  

## Content editing guidelines - please read

  

Below are a few simple rules that you should follow when adding or amending content. Following these rules can help prevent failed builds or later issues. Please also consult the [copy guidelines](/_editors/copy-guidelines) for tone and tips.

  

1. If you haven't already got the idea, **never make any commits to the `crossref-website` branch directly.**

1. Always ensure you are using the latest version of the GitHub Desktop software available from the [GitHub Desktop website](https://desktop.github.com).

1. Because other colleagues are likely to be working on content at the same time, it's a good idea to Fetch origin from time to time while you work, and also to commit your own work fairly regularly. Otherwise your changes or someone else's may cause a [merge conflict](#merge-conflicts-what-they-are-and-how-to-fix) and you will have to manually pick through both sets of changes to make sure everything is correctly updated.

1. When you enter a Summary for a commit, be succinct, clear and descriptive. Remember that someone else may have to search through previous commits (sometimes several weeks or months old) to try and find where and when something went wrong, so try to describe things in a way that will help this process.

1. Do not use absolute URLs (e.g. *www.crossref.org/path/to/some/page*) for any links within the www.crossref.org site. Always use relative URLs (e.g. */path/to/some/page*) instead.

1. Always wrap textual [frontmatter](/_editors/frontmatter-guide/) and the `date` in quotes. If you don't, it may break the site.

  

## The content publishing workflow (updated October 2020)

  

Adding or editing content can be as simple as making a couple of changes to a single file, or it can involve lots of small or larger changes across multiple files. In either case, the workflow below lays out a process for making and reviewing changes on your **own computer before publishing them to the staging site** for final review that can save you a lot of time, as it means you won't have to commit, push and then wait for every change to be rendered on the staging site.

  

As stated above, **all new and updated content should be added to the `staging` branch only**. If you're a content editor your workflow for adding, changing and deleting content is as follows:

  

1. Open GitHub Desktop and ensure that you are working with the `staging` branch - look for the Current Branch box in the top center of the GitHub Desktop window. If it doesn't say `staging`, click to drop it down and select `staging` from the list.

1. In GitHub Desktop click **Fetch origin** near the top right to synchronise the latest updates from the staging branch onto your computer. **This is extremely important** and should always be your first step.

1. Switch to your markdown editor such as [Visual Studio Code](https://code.visualstudio.com/download) add make your content additions or changes. You can also delete pages here (if you do this be sure any links to the deleted page(s) have also been updated).

1. It is helpful to check your changes locally **before** pushing them to the staging website. A small programme called [*preview-local*](../preview-local-how-to) has been created to allow you to bring up the Crossref website on your own Mac. It lets you work more quickly, making and checking several changes on your own computer, rather than committing each change and waiting for the staging site to rebuild.

1. Once you are happy with all of your changes, switch back to GitHub Desktop. You'll see that the **Changes** tab will list all the files you have updated. Below these you'll see a space to enter a Summary of the changes you've made, and optionally a Description (see the [Guidelines](#content-editing-guidelines-please-read)) section below for info on how to enter a useful Summary). When you are ready click the **Commit to staging** button. Note that you can always **Undo** this step if you find you have additional amends to make.

1. Finally and **most importantly** click **Push origin** near the top right of the GitHub Desktop window. **Only at this point will your changes be sent to the staging website, and after a few minutes for the site to be rebuilt, published to where you can see them at https://staging.crossref.org.**

  

<img src="/images/deployment-how-to/screenshot-content-editors.png" alt="GitHub Desktop screenshot for content editors" style="border: 1px solid grey; width: 100%;"  />

  

## Merge conflicts: what they are and how to fix

  

If at any point above you receive a warning about a "Merge Conflict" it probably means that you forgot to **Fetch origin** before you started working, and have made edits to a page that someone else has also edited since the last time you synchronised the staging branch to your computer. In this case you have two options: you can discard your changes to the page, then sync (Pull origin), and then apply your changes to the latest version of the site. Or you can try to merge, in which case GitHub Desktop will mark all the points where there is a conflict between your edits and your colleague's like so:

  

```

<<<<<<<<< HEAD

This line was commited by someone else.

=========

This line was commitec by you. GitHub doesn't know which one is the "correct" amend.

>>>>>>> 4e2b407f501b68f8588aa645acafffa0224b9b78:mergetest

```

  

The solution is to go back to your markdown editor and remove the incorrect update, or resolve them into one single update. **Be sure to also remove all the lines beginning with <<<<<<<<<, ========= and >>>>>>>>> or these will be visible on the website itself.**