+++
title = "Test"
date = "2019-02-11"
draft = false
+++

{{% divwrap blue-highlight %}}

## Our Truths

### Come one, come all

We define publishing broadly. If you communicate research and care about preserving the scholarly record, join us. We are a global community of members with content in all disciplines, in may formats, and with all kinds of business models.

### One member, one vote

Help us set the agenda. It doesn't matter how big or small you are, every member gets a single vote to create a board that represents all types of members.

### Smart alone, brilliant together

Collaboration is at the core of everything we do. We involve the community through active working groups and committees. Our focus is on things that are best achieved by working together.

### Love metadata, love technology

We do R&D to support and expand the shared infrastructure we run for the scholarly community. We create open tools and APIs to help enrich and exchange metadata with thousands of third parties, to drive discoverability of our members' content.

### What you see, what you get

Ask us anything. We'll tell you what we know. Openness and transparency are principles that guide everything we do.

### Here today, here tomorrow

We're here for the long haul. Our obsession with persistence applies to all things&emdash;metadata, links, technology, and the organisation. But "persistent" doesn't mean "static"; as research communications continues to evolve, so do we.

{{% /divwrap %}}
{{% divwrap red-highlight %}}

## Our Truths

### Come one, come all

We define publishing broadly. If you communicate research and care about preserving the scholarly record, join us. We are a global community of members with content in all disciplines, in may formats, and with all kinds of business models.

### One member, one vote

Help us set the agenda. It doesn't matter how big or small you are, every member gets a single vote to create a board that represents all types of members.

### Smart alone, brilliant together

Collaboration is at the core of everything we do. We involve the community through active working groups and committees. Our focus is on things that are best achieved by working together.

### Love metadata, love technology

We do R&D to support and expand the shared infrastructure we run for the scholarly community. We create open tools and APIs to help enrich and exchange metadata with thousands of third parties, to drive discoverability of our members' content.

### What you see, what you get

Ask us anything. We'll tell you what we know. Openness and transparency are principles that guide everything we do.

### Here today, here tomorrow

We're here for the long haul. Our obsession with persistence applies to all things&emdash;metadata, links, technology, and the organisation. But "persistent" doesn't mean "static"; as research communications continues to evolve, so do we.

{{% /divwrap %}}
{{% divwrap yellow-highlight %}}

## Our Truths

### Come one, come all

We define publishing broadly. If you communicate research and care about preserving the scholarly record, join us. We are a global community of members with content in all disciplines, in may formats, and with all kinds of business models.

### One member, one vote

Help us set the agenda. It doesn't matter how big or small you are, every member gets a single vote to create a board that represents all types of members.

### Smart alone, brilliant together

Collaboration is at the core of everything we do. We involve the community through active working groups and committees. Our focus is on things that are best achieved by working together.

### Love metadata, love technology

We do R&D to support and expand the shared infrastructure we run for the scholarly community. We create open tools and APIs to help enrich and exchange metadata with thousands of third parties, to drive discoverability of our members' content.

### What you see, what you get

Ask us anything. We'll tell you what we know. Openness and transparency are principles that guide everything we do.

### Here today, here tomorrow

We're here for the long haul. Our obsession with persistence applies to all things&emdash;metadata, links, technology, and the organisation. But "persistent" doesn't mean "static"; as research communications continues to evolve, so do we.

{{% /divwrap %}}
{{% divwrap lightgrey-highlight %}}

## Our Truths

### Come one, come all

We define publishing broadly. If you communicate research and care about preserving the scholarly record, join us. We are a global community of members with content in all disciplines, in may formats, and with all kinds of business models.

### One member, one vote

Help us set the agenda. It doesn't matter how big or small you are, every member gets a single vote to create a board that represents all types of members.

### Smart alone, brilliant together

Collaboration is at the core of everything we do. We involve the community through active working groups and committees. Our focus is on things that are best achieved by working together.

### Love metadata, love technology

We do R&D to support and expand the shared infrastructure we run for the scholarly community. We create open tools and APIs to help enrich and exchange metadata with thousands of third parties, to drive discoverability of our members' content.

### What you see, what you get

Ask us anything. We'll tell you what we know. Openness and transparency are principles that guide everything we do.

### Here today, here tomorrow

We're here for the long haul. Our obsession with persistence applies to all things&emdash;metadata, links, technology, and the organisation. But "persistent" doesn't mean "static"; as research communications continues to evolve, so do we.

{{% /divwrap %}}
{{% divwrap darkgrey-highlight %}}

## Our Truths

### Come one, come all

We define publishing broadly. If you communicate research and care about preserving the scholarly record, join us. We are a global community of members with content in all disciplines, in may formats, and with all kinds of business models.

### One member, one vote

Help us set the agenda. It doesn't matter how big or small you are, every member gets a single vote to create a board that represents all types of members.

### Smart alone, brilliant together

Collaboration is at the core of everything we do. We involve the community through active working groups and committees. Our focus is on things that are best achieved by working together.

### Love metadata, love technology

We do R&D to support and expand the shared infrastructure we run for the scholarly community. We create open tools and APIs to help enrich and exchange metadata with thousands of third parties, to drive discoverability of our members' content.

### What you see, what you get

Ask us anything. We'll tell you what we know. Openness and transparency are principles that guide everything we do.

### Here today, here tomorrow

We're here for the long haul. Our obsession with persistence applies to all things&emdash;metadata, links, technology, and the organisation. But "persistent" doesn't mean "static"; as research communications continues to evolve, so do we.

{{% /divwrap %}}
