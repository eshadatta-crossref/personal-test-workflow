## /about
```
aliases = [
    "/01company/02history.html",
    "/01company/16fastfacts.html",
    "/01company/jobs.html",
    "/02publishers/14demo.html",
    "/03libraries/16fastfacts.html"
]
```
## /contact
```
aliases = [
    "/01company/03contact.html",
    "/01company/contactus.html"
]
```
## /people
```
aliases = [
    "/01company/04staff.html"
]
```
## /board-and-governance
```
aliases = [
    "/01company/05board.html"
]
```
## /community
```
aliases = [
    "/01company/06publishers.html",
    "/01company/17crossref_members.html"
]
```
## /community/librarians
```
aliases = [
    "/01company/07libraries.html",
    "/01company/free_services_agreement.html",
    "/03libraries/16lib_how_to.html",
    "/03libraries/index.html"
]
```
## /affiliates/
```
aliases = [
    "/01company/08affiliates.html"
]
```
## /news
```
aliases = [
    "/01company/09news_releases.html",
    "/01company/09press_releases.html"
]
```
## /industry-events
```
aliases = [
    "/01company/11conferences.html",
    "/crossmark/AboutEvents.htm"
]
```
## /members-area
```
aliases = [
    "/01company/credit_card_payment.html",
    "/06members/",
    "/06members/index.html",
    "/members_only"
]
```
## /membership
```
aliases = [
    "/01company/join_crossref.html",
    "/02publishers/00success.html",
    "/02publishers/22request_mem.html",
    "/02publishers/22request_mem_test.html",
    "/join_crossref.html"
]
```
## /community/orcid
```
aliases = [
    "/01company/orcid.html"
]
```
## /webinars
```
aliases = [
    "/01company/webinar.html",
    "/01company/webinars.html"
]
```
## /
```
aliases = [
    "/02publishers/18gallery.html"
]
```
## /fees
```
aliases = [
    "/02publishers/20pub_fees.html",
    "/04intermediaries/34affiliate_fees.html"
]
```
## /community/sponsors
```
aliases = [
    "/02publishers/30sponsor_member.html"
]
```
## /member-obligations
```
aliases = [
    "/02publishers/59pub_rules.html"
]
```
## /services/cited-by
```
aliases = [
    "/02publishers/citedby_linking.html",
    "/06members/citedby_linking_signup.html",
    "/citedby.html",
    "/citedby/",
    "/citedby/index.html"
]
```
## /services/content-registration/content-types
```
aliases = [
    "/02publishers/dois_for_books.html"
]
```
## /community/librarians/
```
aliases = [
    "/03libraries/27forlibraries.html"
]
```
## /affiliates
```
aliases = [
    "/04intermediaries/60affiliate_rules.html"
]
```
## /community/researchers
```
aliases = [
    "/05researchers/18demo.html",
    "/05researchers/index.html"
]
```
## /community/researchers/
```
aliases = [
    "/05researchers/18gallery.html"
]
```
## /brand
```
aliases = [
    "/06members/49logos.html"
]
```
## /crossref-live
```
aliases = [
    "/10meetings/landing_page.html",
    "/annualmeeting/agenda.html",
    "/annualmeeting/index.html"
]
```
## /blog
```
aliases = [
    "/CrossTech/",
    "/crosstech",
    "/crweblog/"
]
```
## /service-providers/platform-checklist/
```
aliases = [
    "/affiliates/platform-checklist/"
]
```
## /annual-report
```
aliases = [
    "/annual_report_archive/",
    "/crossref_annual_report_2014/index.html"
]
```
## /_apply/member
```
aliases = [
    "/apply",
    "/membership/apply"
]
```
## /affiliates/migrating-platforms
```
aliases = [
    "/blog/planning-platform-move"
]
```
## /blog/similarity-check-news-introducing-the-next-generation-ithenticate./
```
aliases = [
    "/blog/similarity-check-news-introducing-the-next-generation-ithenticate/"
]
```
## /blog/event-data-is-production-ready;
```
aliases = [
    "/blog/welcome-aboard-event-data-is-production-ready"
]
```
## /brand/badges;
```
aliases = [
    "/brand/member-badges"
]
```
## /services/metadata-delivery
```
aliases = [
    "/cms/cms_policies.html",
    "/cms/index.html",
    "/metadata_services.html"
]
```
## /services/similarity-check
```
aliases = [
    "/crosscheck/",
    "/crosscheck/crosscheck_for_researchers.html",
    "/crosscheck/crosscheck_inthenews.html",
    "/crosscheck/index.html"
]
```
## services/similarity-check/
```
aliases = [
    "/crosscheck_terms.html"
]
```
## /services/crossmark
```
aliases = [
    "/crossmark/AboutFAQs.htm",
    "/crossmark/AboutGallery.htm",
    "/crossmark/AboutJoin.htm",
    "/crossmark/AboutTermsConditions.htm",
    "/crossmark/Affiliates.htm",
    "/crossmark/",
    "/crossmark/PublishersFAQs.htm"
]
```
## /services/content-registration/funding-data/
```
aliases = [
    "/fundingdata/"
]
```
## /services/funder-registry
```
aliases = [
    "/fundingdata/faqs.html",
    "/fundingdata/index.html"
]
```
## /services/funder-registry/
```
aliases = [
    "/fundref/",
    "/fundref/fundref_registry.html",
    "/fundref/index.html"
]
```
## /services/similarity-check/
```
aliases = [
    "/get-started/similarity-check/"
]
```
## /membership/terms
```
aliases = [
    "/member-obligations"
]
```
## /apply
```
aliases = [
    "/membership/join/"
]
```
## /affiliates/platform-migration-checklist
```
aliases = [
    "/platform-migration-checklist"
]
```
## /members/prep/
```
aliases = [
    "/prep/"
]
```
## /services/metadata-delivery/plus-service/terms/
```
aliases = [
    "/services/metadata-delivery/plus-service/agreement/"
]
```
## /blog/newly-approved-membership-terms-will-replace-existing-agreement/
```
aliases = [
    "/terms"
]
```
