+++
title = "Preprint advisory group"
date = "2022-12-16"
draft = false
author = "Martyn Rittman"
rank = 3
[menu.main]
parent = "Working groups"
weight = 10
+++

The purpose of the preprint Advisory Group is to support Crossref to collect and improve the quality of metadata for preprints. The group is comprised of both our members as well as non-members (third party platforms and organizations) who are interested in preprints.

## Group Members

Chair: Oya Rieger, Ithaka  
Facilitator: Martyn Rittman, Crossref

* Alainna Wrigley, California Digital library
* Alberto Pepe, Authorea
* Alex Mendonca, SciELO
* Ben Mudrak, ChemRxiv
* Bianca Kramer, Utrecht University Library
* Ioana Craciun, Preprints
* Dasapta Erwin Irawan, RINarxiv
* Emily Marchant, Cambridge University Press
* Ginny Hendricks, Crossref
* Gunther Eysenbach, JMIR
* Jeff Beck, NCBI, US National Library of Medicine
* Jessica Polka, ASAPbio
* Jingyu Liu, ChinaXiv
* Johanna Havemann, AfricaArxiv
* Johannes Wagner, Copernicus
* Katharine Hancox, IET
* Frederick Atherden, Elife
* Michael Markie, F1000 Research
* Michael Parkin, Europe PMC
* Michele Avissar-Whiting, Research Square
* Nici Pfeiffer, Center for Open Science
* Patricia Feeney, Crossref
* Richard Sever, BioRxiv
* Richard Wynne, Rescognito
* Shirley Decker-Lucke, SSRN
* Tony Alves, HighWire Press
* Thomas Lemberger, EMBO
* Wendy Patterson, Beilstein-Institut

---
## How the group works (and the guidelines)

The preprint Advisory Group is led by a Chair and a Crossref Facilitator, who together help to develop meeting agendas, lead discussions, outline group actions and rally the community outside of the Advisory Group for support with the service where appropriate.

The group is currently active. Please contact [Martyn Rittman](mailto:mrittman@crossref.org) with any questions.

## Outputs

Over its first year of operation, the Advisory Group has developed recommendations in four key areas of preprint metadata. These are:
 - preprint withdrawal and removal
 - preprints as an article type
 - versioning of preprints
 - preprint relationship metadata.

In July 2022, the AG published a set of recommendations ([https://doi.org/10.13003/psk3h6qey4](https://doi.org/10.13003/psk3h6qey4)) and invited public comment, available on our [Community Forum](https://community.crossref.org/t/share-your-thoughts-on-preprint-metadata/2800). An in-depth report of discussions of the AG is available at [https://doi.org/10.31222/osf.io/qzusj](https://doi.org/10.31222/osf.io/qzusj).

## Minutes

Minutes of the group are available on [this page](/working-groups/preprints-meeting-notes).