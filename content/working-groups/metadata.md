+++
title = "Metadata Practitioner Interest Group"
date = "2019-10-07"
draft = false
author = "Patricia Feeney"
rank = 2
[menu.main]
parent = "Working groups"
weight = 14
+++

The Metadata Practitioners Interest Group advises Crossref on publishing needs and trends as they impact Crossref metadata. We want our metadata to be [compatible, complete, credible, and curated](http://www.metadata2020.org/resources/metadata-principles/). Our metadata comes from our members, and development efforts need to be community-led.

We are currently working on:

* Updating the Crossref metadata schema
* Identifying metadata weak spots
* Redefining preprints
* Working with JATS and JATS4R

Just like working and advisory groups, the interest group is open to all members of the Crossref community, members and users alike. However, interest groups tend to be a bit 'looser', and can come together sporadically. Your participation can be passive or active, enthusiastic or occasional. This group consists of people with a shared interest in shaping the metadata we collect. Contact Patricia ([pfeeney@crossref.org](mailto:pfeeney@crossref.org)) to be added to our monthly call and mailing list.

## Participants

{{% row %}}
{{% column %}}
* Midori Baer, National Academy of Sciences
* Debra Borrelli, West Virginia University
* Iwan Joe Dewanto, Pengurus Besar Persatuan Dokter Gigi Indonesia
* Angela Herrera, Universidad Nacional de Educacion Enrique Guzman y Valle
* Cyrenes Moncawe, National Fisheries Research and Development Institute
* Melissa Harrison, eLife
* Johannes Gottschalt, Bohlau Verlag
* James Phillpotts, OUP
* Asbjørn Dahl, National Library of Denmark
* Iwan Joe Dewanto, Pengurus Besar Persatuan Dokter Gigi Indonesia
* Uli Fechner, Beilstein Institut
* Favio Andres Florez, Editorial Pontificia Universidad Javeriana
* April Gilbert, San Jose State University
* Mark Gillespie, Reactome
* Johannes Gottschalt, Bohlau Verlag
* Xiaofeng Guo, Wanfang Data
* Melissa Harrison, eLife
* Angela Herrera, Universidad Nacional de Educacion Enrique Guzman y Valle
* Helen King, BMJ
* Cyrenes Moncawe, National Fisheries Research and Development Institute
* Mike Nason, PKP/UNB
* Jaime de la Ossa, Universidad de Sucre
* James Phillpotts, OUP
* Carly Robinson, OSTI
