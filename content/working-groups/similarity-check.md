+++
title = "Similarity Check advisory group"
date = "2021-04-12"
draft = false
author = "Fabienne Michaud"
[menu.main]
parent = "Working groups"
weight = 12
+++

The purpose of the Similarity Check Advisory Group is to provide Crossref with policy and technical advice on changes and improvements to the Crossref Similarity Check service. The group is comprised of Crossref members, all of whom are active users of Similarity Check. The Similarity Check Advisory Group is led by a Chair and a Crossref Facilitator, who together help to develop meeting agendas, lead discussions and outline group actions in an effort to help drive service improvements. Colleagues from Turnitin will be invited to attend meetings at the discretion of the Chair and Facilitator.

## Group members

Chair: Lauren Flintoft, IOP Publishing   
Facilitator: Fabienne Michaud, Crossref

* Adya Misra, Sage
* Barbara Ryan, Association for Computing Machinery
* Catriona Fennell, Elsevier
* Corrie Petterson, Institute of Electrical and Electronics Engineers
* Gráinne McNamara, Karger
* Harini Varadarajan, Taylor & Francis
* John Dufour, American Chemical Society
* John Sivo, Institute of Electrical and Electronics Engineers
* Lois Jones, American Psychological Association
* Luigi Longobardi, Institute of Electrical and Electronics Engineers
* Marie Soulière, Frontiers
* Michael Willis, Wiley
* Mihail Grecea, Elsevier
* Sam Parsons, IOP Publishing
* Simone Ragavooloo, BMJ
* Tamara Welschot, Springer Nature (co-Chair)

---
## How the group works (and the guidelines)

With the exception of Crossref staff, the group will be limited to one representative from each participating publisher, unless particular agenda items or topics call for additional expertise from additional colleagues or departments from within a single organization. Members are, however, free to discuss the information shared during meetings with colleagues or any external party. Members can choose to leave the Advisory Group at any time but are asked to send their resignation in writing to the Chair and Facilitator.  

Advisory Group members commit to attend all meetings by conference call, and may choose to send a named proxy if they are not available. The schedule of meetings is at the discretion of the Chair and Facilitator and may vary depending on whether there are relevant topics for discussion but are usually held three - four times per year. Notes are circulated by the Facilitator after each call, and any members who were unable to attend a call are asked to ensure they read these and take note of any action items.

Members are asked not to invite colleagues or any external party to join Advisory Group meetings unless they have discussed this with the Chair and Facilitator prior to the call. This ensures a consistency in development approach and a level of fluency during meetings.

---
Please contact [Fabienne Michaud](mailto:fmichaud@crossref.org) with any questions or to apply to join the advisory group.
