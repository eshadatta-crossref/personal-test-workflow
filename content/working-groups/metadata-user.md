+++
title = "Metadata User Working Group"
date = "2021-10-13"
draft = false
author = "Jennifer Kemp"
rank = 2
[menu.main]
parent = "Working groups"
weight = 15
+++


## Overview

Despite a large, international and growing community of users, the particulars of metadata user workflows are still too often unclear or undocumented. Questions of efficiencies, thresholds, how records and elements are evaluated for usefulness and how multiple integrations are managed will be explored as a group, through a series of calls and asynchronous work. The group is expected to run through 2022.

## Goals

* Document workflows
* Highlight the efforts of metadata users in enabling discovery/discoverability
* Determine directions for improved engagement
* Inform approaches to product planning

## Participants

The group is a mix of service subscribers using different interfaces:

* Achraf Azhar, Centre pour la Communication Scientifique Directe (CCSD)
* Satam Choudhury, HighWire Press
* Nees van Eck, CWTS-Leiden University
* Bethany Harris, Jisc
* Ajay Kumar, Nova Techset
* David Levy, Pubmill
* Bruno Ohana, biologit
* Michael Parkin, European Bioinformatics Institute (EMBL-EBI)
* Axton Pitt, Litmaps
* Dave Schott, Copyright Clearance Center (CCC)
* Stephan Stahlschmidt, German Centre for Higher Education Research and Science Studies (DZHW)

Once concluded, results of the discussions will be shared with the community.
