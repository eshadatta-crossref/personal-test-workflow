+++
title = "DUL Project working group"
date = "2021-11-24"
draft = false
author = "Martyn Rittman"
parent = "Working groups"
weight = 100
rank = 1
+++

The distributed usage logging (DUL) working group ran until 2020 and was the community group driving the [distributed usage logging project](/community/project-dul/).

The objectives of the DUL working group were to address the following:

* Define a way for DOIs to advertise endpoints to which event data may be submitted, including a mechanism to specify the payload schemas that the endpoint accepts.
* Pilot the transmission of COUNTER-usage events from platforms providing direct access to full text to publishers responsible for that full text, using the above mechanism, in a secure manner
* Work out the "rules of the game" for the COUNTER use cases, including message semantics, responsibility for anti-gaming mechanism, etc.

## What we’re working on

The working group has now retired.


## Group members
The group comprised some of our members as well as some third party platforms who were actively interested in participating in a secure exchange of usage records.

Facilitator: Martyn Rittman, Crossref

* Esther Heuver, Elsevier (_Chair_)
* Paul Dlug, American Physical Society
* Lorraine Estelle, Project COUNTER
* John Chodacki, California Digital Library
* Paul Needham, Cranfield University
* Johannes Buchmann, De Gruyter
* Nicko Goncharoff, Digital Science
* Oliver Pesch, EBSCO
* Ian Hayes, Atypon
* Tom Beyer, PubFactory
* Maciej Rymarz, Mendeley
* Robert McGrath, ReadCube
* Kimberly Tryka, National Institute of Standards and Technology
* John Connolly, Springer Nature
* Jo Cross, Taylor & Francis
* Clara Brown, United States Geological Survey
* Greg Hargrave, Wiley
* Stuart Maxwell, Scholarly iQ
* Aaron Wood, American Psychological Association

Please contact [Martyn Rittman](mailto:dul@crossref.org) with any questions.
