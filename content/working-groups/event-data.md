+++
title = "Event Data advisory group"
date = "2021-02-26"
draft = false
author = "Martyn Rittman"
[menu.main]
parent = "Working groups"
weight = 10
+++

The purpose of the Event Data Advisory Group is to provide Crossref with policy and technical advice regarding developments, changes and improvements to the Crossref Event Data service. The group is comprised of both our members as well as non-members (third party platforms and organizations) who are interested in consuming our Event Data records for their own use case.

## Group Members

Chair: John Chodacki, California Digital Library
Facilitator: Martyn Rittman, Crossref

* Euan Adie, Altmetric
* Tim Stevenson, BioMed Central
* Theo Bloom, BMJ
* Martin Fenner, DataCite
* Mike Taylor, Digital Science
* Mark Patterson, eLife
* Craig Jurney, Highwire
* Sebastian Pöhlmann, Mendeley
* Maciej Rymarz, Mendeley
* Juan Pablo Alperin, Public Knowledge Project
* Katie Hickling, PLOS
* Lorraine Estelle, COUNTER
* Damian Pattinson, Research Square
* Martijn Roelandse, Springer Nature
* Christian Hauschke, TIB Leibniz Universität Hannover
* Mike Thelwell, University of Wolverhampton
* Liz Ferguson, Wiley
* Christina Lohr, Elsevier
* Kaveh Bazargan, River Valley Technologies

---
## How the group works (and the guidelines)

The Event Data Advisory Group is led by a Chair and a Crossref Facilitator, who together help to develop meeting agendas, lead discussions, outline group actions and rally the community outside of the Advisory Group for support with the service where appropriate.

The Working Group is currently not active, however we continue to maintain the Event Data API.

---
Please contact [Martyn Rittman](mailto:eventdata@crossref.org) with any questions.
