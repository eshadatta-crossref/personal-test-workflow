+++
title = "Crossmark advisory group"
date = "2021-02-26"
draft = false
author = "Martyn Rittman"
rank = 2
[menu.main]
parent = "Working groups"
weight = 13
+++

The Crossmark Advisory Group’s role is to provide Crossref with policy and technical advice on changes and developments to Crossmark. The group is comprised of Crossref members who have implemented or are planning to implement Crossmark on their publications, and is lead by a Chair and Crossref staff facilitator. It is currently not active.

## Group members

Chair: TBC
Facilitator: Martyn Rittman, Crossref

* Christopher McMahon, AIP
* Emily-Sue Sloane, AIP
* Theo Bloom, BMJ
* Keith Waters, CUP
* Egbert van Wezenbeek, Elsevier
* Omer Gazit, F1000 Research
* Michael Evans, F1000 Research
* Peter Strickland, IUCr
* Joseph Brown, PLOS
* Rob O’Donnell, Rockefeller University Press
* Michael Waters, Springer
* David Burgoyne, T&F
* Nicholas Everitt, T&F
* Edward Wates, Wiley

---
## How the group works (and the guidelines)

Members commit to attend all meetings by conference call, and may choose to send a named proxy if they are not available. Meeting notes will be circulated to all by the facilitator. The schedule of meetings is at the discretion of the chair and facilitator and may vary depending on whether there are relevant topics for discussion, but will not be more than one per quarter.

With the exception of Crossref staff, the group will be limited to one representative from each participating organization, unless particular agenda items or topics call for domain expertise from specific colleagues or departments. Members are, however, free to discuss the information shared during meetings with colleagues or any external party.

---
Please contact [Martyn Rittman](mailto:support@crossref.org) with any questions or to apply to join the advisory group.
