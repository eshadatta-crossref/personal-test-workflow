+++
title = "This tool is no longer available"
date = "2022-09-28"
draft = false
author = "Rachael Lammey"
image = "/images/banner-images/time-to-say-goodbye.jpg"
maskcolor = "crossref-yellow"
Weight = 50
rank = 1
aliases = [
	"/reporting/members-with-closed-references",
	"/reporting/members-with-closed-references/",
	"/reporting/members-with-open-references",
	"/reporting/members-with-open-references/",
  "/reports/members-with-open-references",
  "/reports/members-with-open-references/",
  "/reports/members-with-closed-references",
  "/reports/members-with-closed-references/",
	"/requestaccount",
]

+++

You may have been redirected here from a tool that that no longer exists.

From time to time we need to sunset tools or services. This might be because we've changed our policies, developed newer technologies to replace the old, perhaps low usage means maintenance costs are too high, or sometimes the need simply passes as the community changes.

An archive of deprecated tools is below.  

## 2022-September-28: Removing reference visibility functionality from REST API

Following the change in our reference distribution policy noted below, we have removed all reference visibility functionality from the REST API. This includes the `reference-visibility` filter, the `reference-visibility` and `public-references` fields available via the `/members` route, and `open-references` coverage calculations available via the `/journals` and `/members` routes. You can read about the [board vote](https://www.crossref.org/board-and-governance/#march-2022-board-meeting) and the [membership terms change](https://www.crossref.org/blog/amendments-to-membership-terms-to-open-reference-distribution-and-include-uk-jurisdiction/). All the members that previously had limited or closed references have now been set to open.

## 2022-June-06: Removing reports of members with open/closed references

Since 2017 we hosted API generated tables of members with open and closed references here on our website. The board voted in March 2022 to remove the ability for members to limit the distribution of references, to be more in line with all other metadata which is default open. You can read about the [board vote](/board-and-governance/#march-2022-board-meeting) and the [membership terms change](/blog/amendments-to-membership-terms-to-open-reference-distribution-and-include-uk-jurisdiction/). All the members that previously had limited or closed references have now been set to open.


## 2022-January-12: Sunsetting Simple Text Query Upload

The Simple Text Query Upload (STQ Upload) service allows users to upload a text file containing references, with the results emailed back to the user in an HTML file. STQ Upload is lightly used and redundant with the functionality in the [Simple Text Query](https://doi.crossref.org/simpleTextQuery) service. In the interest of consolidating and improving our reference matching services, we have deprecated STQ Upload and plan to retire it in August 2022.

## 2021-December-10: Distributed Usage Logging (DUL) key registry moves to STM Solutions

Through 2020, we re-evaluated the progress of Distributed Usage Logging (DUL) and how it fits with other Crossref services. While technically we had reached the stage of releasing a proof-of-concept service, it became clear to us that Crossref is not best placed to expand the project in the future and increase participation. At the November 2020 Crossref Board meeting a motion was passed “that the Crossref Board supports another organization’s taking ownership of the Distributed Usage Logging (“DUL”) initiative.” We have therefore been seeking other partners to take DUL forward.

We are delighted that STM Solutions has agreed to take on a crucial part of the infrastructure for DUL: maintaining a registry of public keys that can be used to authenticate messages. From the end of 2021, the registry will be fully transferred to STM and Crossref’s version of the registry will be removed in early 2022.

Crossref will continue to collect DUL endpoints for individual works, as part of the metadata deposited by our members.

## 2021-July-07: v1 Deposit API via OJS

Crossref and PKP have collaborated for some time to help publishers using [Open Journal Systems](https://pkp.sfu.ca/ojs/) (OJS) to benefit from Crossref services.

Before 2019 (OJS 3.1.1 and older) OJS integrated with the dedicated v1 OJS deposit API. From 2019 (OJS 3.1.2 and higher) a newer more reliable API (v2 deposit API) was made available and v1 was deprecated and unsupported. In July 2021 we turned off the v1 deposit API. For members who are still using an older version of OJS (OJS 3.1.1 and older)

{{% divwrap yellow-highlight %}}
* you can continue to export your xml, and upload it to Crossref’s systems; or
* you could—-and probably should—-upgrade your OJS instance to a supported version that makes use of the v2 deposit API (OJS 3.1.2 or higher).
{{% /divwrap %}}

### 2020-November-24: Click-through Service for text and data mining

The Click-through Service for text and data mining was a registry of additional TDM license agreements, posted by Crossref members, which researchers could review and accept and then use the API token provided when requesting full-text from the publisher.

Given the low take-up of the service by both publishers and researchers, its goals are no longer being met. Therefore we will retire the service on 31 December 2020. Until that date, it will still operate for the two publishers and various researchers who use it while they finish implementing their alternative plans. For more details on this, and our continuing support for text and data mining, please read the blog post [Evolving our support for text-and-data mining](/blog/evolving-our-support-for-text-and-data-mining/).

{{% divwrap yellow-highlight %}}
Note, Crossref will continue to collect member-supplied TDM licensing information in metadata for individual works, and researchers can continue to find this via the Crossref APIs.
{{% /divwrap %}}


## 2020-November-03: Guest service query accounts

Crossref provides various interfaces for query services.

The Crossref Query Services interfaces are:

* [OpenURL](/documentation/retrieve-metadata/openurl/)
* [HTTPS](/documentation/retrieve-metadata/xml-api/using-https-to-query/)

Registered Crossref Members, Libraries and Affiliates are able to use these interfaces with their previously supplied system account credentials.

Following a recent change, guest users no longer need to register for a free Guest Services Query account. You must include your email address in your queries. The purpose of requiring an email address is simply to monitor usage to balance system demand and to identify problems.

This brings querying our [XML API](/documentation/retrieve-metadata/xml-api/) and [Open URL](/documentation/retrieve-metadata/openurl/) services inline with our [REST API](/documentation/retrieve-metadata/rest-api/).

{{% divwrap yellow-highlight %}}
Note, if you are not a member, you still need to supply your email address in the query; this is only used to contact you if there is a problem with your query.
{{% /divwrap %}}

---
We always give advanced warning so those using the services can prepare and transition away from using it. Please check out information about such changes on our [community forum](https://community.crossref.org/), [blog](/blog/), or subscribe to our [newsletter]().

Contact our [support group](mailto:support@crossref.org) with any questions.
