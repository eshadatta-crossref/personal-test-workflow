+++
title = "Webinars_old"
date = "2019-04-02"
draft = false
author = "Rosa Clark"
parent = "Get involved"
weight = 20
rank = 3
+++

Our regular webinars are a great way to find out more about the various Crossref services.


{{% divwrap yellow-highlight %}}
[We are now doing face-to-face meetings online. You can find our virtual events on our events page.](/events).
{{% /divwrap %}}

---

[Look back at some recent recordings](#previous) such as [an update on Crossmark](#crossmark), the [Funder Registry](#fundingfunder), getting started with [Content Registration](#contentreg), [maintaining your metadata](#metadata), or the latest on [Similarity Check](#simcheck).

---

<br>
<a id="previous"></a>  

## Recordings of recent webinars  

{{% row %}} {{% column %}}  

#### **Our getting started series**  
- Get started with Content Registration [Slides](https://www.slideshare.net/CrossRef/getting-started-with-content-registration), [Recording](https://youtu.be/G9mIUvXrTLc)  
- Get started with Reference Linking [Slides](https://www.slideshare.net/CrossRef/getting-started-with-reference-linking), [Recording](https://youtu.be/WvPhmRMCICg)
- Getting started as a new Crossref member [Slides](https://www.slideshare.net/CrossRef/new-member-webinar-052418), [Recording](https://youtu.be/GAUEXizVcHw)  
- Getting started with looking up metadata [Slides](https://www.slideshare.net/CrossRef/getting-started-with-looking-up-metadata), [Recording](https://youtu.be/9_RrWFRHEbI)  

#### **Our how-to webinar series**  
- [Crossmark how-to](#crossmarkhowto)  
- [Crossref Cited-by how-to](#citedbyhowto)
- [Participation Reports](#prepwebinaroct7)
- [The ins and outs of our Admin Tool](#admintool)

#### **About our services**  
- [Content Registration maintaining metadata](#contentregistration)
- [Crossmark update](#crossmark)
- [Crossref Cited-by](#citedby)  
- [Funding data and the Funder Registry](#fundingfunder)
- [Introduction to Crossmark](#introcrossmark)  
- [Introduction to Similarity Check](#simcheckintro)  
- [Research infrastructure with and for funders](#fundersinfrastructure)
- [Similarity Check update](#simcheck)  
- [Similarity Check Members update](#turnitin)

#### **Webinars in Arabic**   
- [Getting Started with Content Registration - in Arabic](#gettingstartedarabic)
- [Introduction to Crossref  - in Arabic](#introarabic)  
- [Reference Linking & Cited-By - in Arabic](#referencelinkingarabic)  
- [ندوه عن كيفية استخدام كروس مارك باللغة العربية | Crossmark How-To Arabic webinar](#crossmark-arabic)

#### **Webinars in Indonesian**   
- Crossref LIVE Indonesia webinar series: Introduction to Crossref, Content Registration, The Value of Crossref metadata - July 13 - 15 - [Online Recordings](https://www.youtube.com/playlist?list=PLe_-TawAqQj0H-vrtBLT_y0BOgYqtBILm)  

#### **Webinars in Portuguese**  
- [Funder Registry - in Portuguese](#fundingportuguese)
- [Getting started with Content Registration: Portuguese Webinar](#contentregportuguese)
- [Introduction to Participation Reports webinar in Portuguese](#prepportuguese)
- [Introduction to Similarity Check webinar - in Portuguese](#simcheckportuguese)
- [Reference Linking & Cited By webinar - in Portuguese](#reflinkingportuguese)
- [Registering content and adding to your Crossref metadata - in Portuguese](#brazilmm)
- [Melhores Práticas para Registro de Conteúdo/Crossref Content Registration in Brazilian Portuguese](#contentregistration-portuguese)  
- [Introduction to Crossmark/Crossmark: O que é e como usar](#introcrossmarkspanish2020)


{{% /column %}}
{{% column %}}

#### **Webinars in Russian**   
- Crossref and OJS - in Russian [Recording](https://youtu.be/VKgZIoIBTPs)  
- Introduction to Crossmark - in Russian [Slides](https://www.slideshare.net/CrossRef/introduction-to-crossmark-russian-webinar), [Recording](https://www.youtube.com/watch?v=2PRYbK0t-SY)  
- Understanding Crossref reports - in Russian [Slides](https://www.slideshare.net/CrossRef/crossref-reports-in-russian), [Recording](https://youtu.be/3HOUJPyyl6M)  
- Crossref LIVE, in Russian: Value & Use of Metadata [Slides](https://www.slideshare.net/CrossRef/crossref-the-value-and-use-of-crossref-metadata-crossref-live-russian-17-september-2021), [Recording]( https://youtu.be/SFFB9EdRNDQ )

#### **Webinars in Spanish**  
- [Introduction to Crossref and Content Registration - in Spanish](#introcrossrefspanish)
- [Reference linking and Cited-by - in Spanish](#citedbyrefspanish)
- [Registro y actualización de contenido en Crossref / Register and update content in Crossref](#contentreginspanish)
- [Crossref ‘Similarity Check’, en español](#simcheckspanish)
- [Seminario web de Informes de Participación / Participation Reports Webinar, in Spanish](#prepspanish042020)
- [Crossmark (en español](#crossmarkspanish2021)

#### **Webinars in Turkish**  
- [Introduction to Crossref: Turkish Seminar](#introturkish)
- [Crossref İçerik Kaydı Webinarı, Türkçe | Content Registration at Crossref , Turkish Webinar](#turkishcontentreg)


#### **Webinars in Ukrainian**  
- [Participation Reports webinar in Ukrainian](#prepukrainian)

#### **Other**  
- [Asia Pacific community webinar](#asia)
- [Beyond OpenURL: Making the most of Crossref metadata](#openurl)
- [Crossref and DataCite joint data citation webinar](#datacitation)
- [Getting started with books at Crossref](#bookswebinar)
- [Introduction to ROR](#introror)
- [Maintaining your metadata](#metadata)
- [Participation Reports webinar](#prepwebinar)
- [Preprints and scholarly infrastructure](#preprints)
- [Proposed schema changes - have your say](#proposedschema)
- [Using ORCID in publishing workflows](#usingorcid)
- [Where does publisher metadata go and how is it used?](#publishermetadata)  
- Not just identifiers: why Crossref DOIs are important - [Slides](https://www.slideshare.net/CrossRef/not-just-identifiers-why-crossref-dois-are-important), [Recording](https://youtu.be/TVWbzb6zJV0)  
- [How to manage your metadata with Crossref](#managemetadata)
- [Finding your way with Crossref: getting started & additional services](#findingyourway)
- [SE Asia webinar series](https://www.youtube.com/playlist?list=PLe_-TawAqQj3n4B-xa9kgeTV4qX0O7aKt)
- [OpenCon Oxford 2020](#openconoxford2020)
- [Making the world a PIDder place: it’s up to all of us! (Co-hosted by DataCite, Crossref, ORCID & ROR)](https://datacite.zoom.us/webinar/register/WN_fXrn4czCSn-lsSBjcpymUg)  - Sep 22, 2021 05:00 PM in Amsterdam, Berlin, Rome, Stockholm, Vienna   


{{% /column %}} {{% /row %}}

If you’re interested in viewing recordings of past LIVE events, check out our [YouTube channel](https://www.youtube.com/playlist?list=PLe_-TawAqQj3qgd6Pj35ORSwJcgwtcs20).  


---

<a id="crossmarkspanish2021"></a>   
### **Crossmark (en español)**   
September 30, 2021  
[Recording](https://youtu.be/9oEwaLkuTDk)  
{{< youtube 9oEwaLkuTDk css-class >}}   


<a id="prepspanish042020"></a>  
### **Seminario web de Informes de Participación / Participation Reports Webinar, in Spanish**  
[Webinar Recording](https://youtu.be/F7vfe731rgk)  
{{< youtube F7vfe731rgk css-class >}}   


<a id="introcrossmarkspanish2020"></a>  
### **Introduction to Crossmark/Crossmark: O que é e como usar**  
[Webinar Recording](https://youtu.be/FEvilhMnCuU)  
{{< youtube FEvilhMnCuU css-class >}}   



<a id="simcheckspanish"></a>  
### **Crossref ‘Similarity Check’, en español**  
[Webinar Recording](https://youtu.be/hwqcNTKVrPM)  
{{< youtube hwqcNTKVrPM css-class >}}   


<a id="openconoxford2020"></a>  
### **OpenCon Oxford 2020**  
[Webinar Recording](https://youtu.be/Y8nEPMUo9R0)  
{{< youtube Y8nEPMUo9R0 css-class >}}   


<a id="turkishcontentreg"></a>  
### **Crossref İçerik Kaydı Webinarı, Türkçe | Content Registration at Crossref , Turkish Webinar**  
[Webinar slides](https://www.slideshare.net/CrossRef/crossref-content-registration-turkish-webinar)  
[Webinar Recording](https://youtu.be/0UWUw0eftH8)  
{{< youtube 0UWUw0eftH8 css-class >}}   


<a id="managemetadata"></a>  
### **How to manage your metadata with Crossref**  
[Webinar slides](https://www.slideshare.net/CrossRef/how-to-manage-your-metadata-with-crossref)  
[Webinar Recording](https://youtu.be/Muy-vrupsnY)  
{{< youtube Zn-Muy-vrupsnY css-class >}}   

---

<a id="findingyourway"></a>  
### **Finding your way with Crossref: getting started & additional services**  
[Webinar slides](https://www.slideshare.net/CrossRef/finding-your-way-with-crossref-getting-started-additional-services)  
[Webinar Recording](https://youtu.be/KooOKVfsyPI)  
{{< youtube KooOKVfsyPI css-class >}}   

---

<a id="contentregistration-portuguese"></a>  
### **Melhores Práticas para Registro de Conteúdo/Crossref Content Registration in Brazilian Portuguese**  
Date: Wednesday, October 7, 2020  
[Webinar slides](https://www.slideshare.net/CrossRef/crossref-content-registration-in-brazilian-portuguese-pt1)  
[Webinar Recording](https://youtu.be/Zn-0Mmykc-U)  
{{< youtube Zn-0Mmykc-U css-class >}}   

---

<a id="contentreginspanish"></a>  
### **Registro y actualización de contenido en Crossref / Register and update content in Crossref**  
Date: Thursday, October 1, 2020  
[Webinar slides](https://www.slideshare.net/CrossRef/register-and-update-content-in-crossref-in-spanish)  
[Webinar Recording](https://youtu.be/bzuKf4EoeH4)  
{{< youtube bzuKf4EoeH4 css-class >}}   

---

<a id="crossmark-arabic"></a>  
### **ندوه عن كيفية استخدام كروس مارك باللغة العربية | Crossmark How-To Arabic**  
Date: Tuesday, September 15, 2020  
[Webinar slides](https://www.slideshare.net/CrossRef/crossmark-howto-arabic-webinar)  
[Webinar Recording](https://youtu.be/sZME-mbmig4)  
{{< youtube sZME-mbmig4 css-class >}}   

---

<a id="bookswebinar"></a>  
### **Getting started with books at Crossref**  
Date: Wednesday, July 22, 2020  
[Webinar slides](https://www.slideshare.net/CrossRef/getting-started-with-books-at-crossref)  
[Webinar Recording](https://youtu.be/pQPKlhJfzIM)  
{{< youtube pQPKlhJfzIM css-class >}}   

---

<a id="simcheckintro"></a>
### **Introduction to Similarity Check**  
Date: Thursday, April 30, 2020    
Speaker: Vanessa Fairhurst, Susan Collins    
[Webinar Slides](https://www.slideshare.net/CrossRef/introducing-crossref-similarity-check)   
[Webinar Recording](https://youtu.be/7GV95FzbjmM)  
{{< youtube 7GV95FzbjmM css-class >}}   

---
<a id="introror"></a>  
### **Introduction to ROR**  
Date: Wednesday, April 29, 2020    
[Webinar Recording](https://youtu.be/W61JMsC3Dho)  
{{< youtube W61JMsC3Dho css-class >}}   

---

<a id="admintool"></a>  
### **The ins and outs of our Admin tool**   
Date: Thursday, March 5, 2020      
Speakers: Isaac Farley, Paul Davis, Kathleen Luschek      
[Webinar Recording](https://youtu.be/Csh6GRegWxw)  
{{< youtube Csh6GRegWxw css-class >}}  

---

<a id="prepwebinaroct7"></a>
### **Participation Reports webinar**   
Date: Wednesday, October 7, 2020      
Speakers: Anna Tolwinska  
[Webinar Slides](https://www.slideshare.net/CrossRef/participation-reports-webinar-october-2020-238797935)  
[Webinar Recording](https://youtu.be/U8VpRTBSnQ4)  
{{< youtube U8VpRTBSnQ4 css-class >}}  

---

<a id="prepukrainian"></a>
### **Вебінар “Інструмент Participation Reports” (Українською мовою) - Participation Reports webinar in Ukrainian**   
Date: Tuesday, August 11, 2020      
Speakers: Anna Danilova and Anna Tolwinska  
[Webinar Slides](https://www.slideshare.net/CrossRef/participation-reports)  
[Webinar Recording](https://youtu.be/Zu0eT8wQE7w)  
{{< youtube Zu0eT8wQE7w css-class >}}  

---

<a id="prepportuguese"></a>
### **Introduction to Participation Reports webinar in Portuguese**   
Date: Wednesday, October 30, 2019      
Speakers: Rachael Lammey, Edilson Damasio  
[Webinar Slides](https://www.slideshare.net/CrossRef/crossref-participation-reports-webinar-slides-in-brazilian-portuguese)  
[Webinar Recording](https://youtu.be/-HM3N8CWlIo)  
{{< youtube -HM3N8CWlIo css-class >}}   

---

<a id="proposedschema"></a>
### **Proposed schema changes - have your say**   
Date: Thursday, January 2, 2020      
Speakers: Patrica Feeney  
[Webinar Slides](https://www.slideshare.net/CrossRef/proposed-schema-changes-have-your-say)  
[Webinar Recording](https://youtu.be/ytBA2Ygq48I)   
{{< youtube ytBA2Ygq48I css-class >}}    

---

<a id="usingorcid"></a>
### **Using ORCID in publishing workflows**  
Date: Monday, September 16, 2019      
Speakers: Estelle Cheng / Rachael Lammey  
[Webinar Recording](https://youtu.be/Um0sc06OR-c)   
{{< youtube Um0sc06OR-c css-class >}}   

---

<a id="fundersinfrastructure"></a>
### **Research infrastructure with and for funders**  
Date: Thursday, September 6, 2019       
Speakers: Josh Brown / Rachael Lammey  
[Webinar Recording](https://youtu.be/va9J3XVYFfw)   
{{< youtube va9J3XVYFfw css-class >}}   

---

<a id="reportsrussian"></a>
### **Understanding Crossref Reports - presented in Russian**  
Date: Thursday, April 18, 2019    
Speakers: Rachael Lammey / Andrii Zolkover  
[Webinar Slides](https://www.slideshare.net/CrossRef/crossref-reports-in-russian)  
[Webinar Recording](https://youtu.be/3HOUJPyyl6M)  
{{< youtube 3HOUJPyyl6M css-class >}}   

---

<a id="reflinkingportuguese"></a>
### **Reference Linking & Cited By webinar - in Portuguese**  
Date: Tuesday, April 16, 2019      
Speakers: Edilson Damasio / Rachael Lammey  
[Webinar Recording](https://youtu.be/JWwvzKSHvDI)   
{{< youtube JWwvzKSHvDI css-class >}}   

---

<a id="datacitation"></a>
### **Crossref and DataCite joint data citation webinar**  
Date: Monday, Feb 4, 2019  
Speakers: Rachael Lammey, Helena Cousijn, Patricia Feeney, Robin Dasler  
[Webinar Slides](https://www.slideshare.net/CrossRef/crossref-datacite-joint-data-citation-webinar)  
[Webinar Recording](https://youtu.be/U-k6cuNvLms)  
{{< youtube U-k6cuNvLms css-class >}}   

---

<a id="crossmarkrussian"></a>
### **Introduction to Crossmark - in Russian**  
Date: Thursday, December 6, 2018      
Speakers: Maxim Mitrofanov  
[Webinar Slides](https://www.slideshare.net/CrossRef/introduction-to-crossmark-russian-webinar)  
[Webinar Recording](https://www.youtube.com/watch?v=2PRYbK0t-SY)  
{{< youtube 2PRYbK0t-SY css-class >}}   

---

<a id="brazilmm"></a>
### **Registering content and adding to your Crossref metadata - in Portuguese**  
Date: Monday, November 26, 2018      
Speakers: Edilson Damasio / Rachael Lammey  
[Webinar Slides](https://www.slideshare.net/CrossRef/registering-and-adding-to-your-metadata-at-crossref-in-portuguese)  
[Webinar Recording](https://youtu.be/Yg5a5GsANzw)  
{{< youtube Yg5a5GsANzw css-class >}}   

---

<a id="crossrefojs"></a>
### **Crossref and OJS - presented in Russian**  
Date: Thursday, November 22, 2018    
Speakers: Rachael Lammey / Andrii Zolkover  / Vitaliy Bezsheiko  
[Webinar Recording](https://youtu.be/VKgZIoIBTPs)  
{{< youtube VKgZIoIBTPs css-class >}}   

---

<a id="referencelinkingarabic"></a>
### **Reference Linking & Cited-By - in Arabic**  
Date: Thursday, November 8, 2018    
Speakers: Mohamad Mostafa / Vanessa Fairhurst  / Rachael Lammey   
[Webinar Slides](https://www.slideshare.net/CrossRef/reference-linking-citedby-arabic-webinar)  
[Webinar Recording](https://youtu.be/dfNG57jkIdw)  
{{< youtube dfNG57jkIdw css-class >}}   

---

<a id="citedbyrefspanish"></a>
### **Introduction to Reference Linking and Cited-by webinar - in Spanish**  
Date: Monday, November 7, 2018      
Speakers: Susan Collins / Vanessa Fairhurst / Arley Soto   
[Webinar Slides](https://www.slideshare.net/CrossRef/reference-linking-and-cited-by-in-spanish)  
[Webinar Recording](https://youtu.be/eDRDglEBybM)  
{{< youtube eDRDglEBybM css-class >}}   

---

<a id="introcrossrefspanish"></a>
### **Introduction to Crossref and Content Registration - in Spanish**  
Date: Tuesday, October 24, 2018  
Speakers: Susan Collins / Vanessa Fairhurst  / Arley Soto  
[Webinar Slides](https://www.slideshare.net/CrossRef/introduction-to-crossref-and-content-registration-in-spanish)   
[Webinar Recording](https://youtu.be/6qiIVQRxv2w)  
{{< youtube 6qiIVQRxv2w css-class >}}

---

<a id="publishermetadata"></a>
### **Where does publisher metadata go and how is it used?**  
Date: Tuesday, September 11, 2018    
Speakers: Laura J. Wilkinson, Anna Tolwinska, Stephanie Dawson, and Pierre Mounier  
[Webinar Slides: Laura Wilinson](https://www.slideshare.net/CrossRef/crossref-webinar-laura-wilkinson-where-does-publisher-metadata-go-and-how-is-it-used-110918)  
[Webinar Slides: Anna Tolwinska](https://www.slideshare.net/CrossRef/crossref-webinar-anna-tolwinska-crossref-participation-reports-metadata-091118)  
 [Webinar Slides: Stephanie Dawson](https://www.slideshare.net/CrossRef/crossref-webinar-stephanie-dawson-sciencopen-metadata-091118/CrossRef/crossref-webinar-stephanie-dawson-sciencopen-metadata-091118)  
[Webinar Slides: Pierre Mounier](https://www.slideshare.net/CrossRef/crossref-webinar-pierre-mounier-where-does-publisher-metadata-go-and-how-is-it-used-91118)  
[Webinar Recording](https://youtu.be/RJhDHWhFFAs)  
{{< youtube RJhDHWhFFAs css-class >}}  

---

<a id="gettingstartedarabic"></a>
### **Getting Started with Content Registration - in Arabic**
Date: Monday, September 17, 2018    
Speakers: Mohamad Mostafa/Rachael Lammey/Vanessa Fairhurst   
[Webinar Slides](https://www.slideshare.net/CrossRef/getting-started-with-content-registration-arabic-webinar)  
[Webinar Recording](https://youtu.be/zavLltyFRYs)  
{{< youtube zavLltyFRYs css-class >}}  

---

<a id="introarabic"></a>
### **Introduction to Crossref - in Arabic**
Date: Monday, August 6, 2018    
Speaker: Rachael Lammey/Mohamad Mostafa  
[Webinar recording](https://youtu.be/NGY3Ypyf1Jc)
{{< youtube NGY3Ypyf1Jc css-class >}}

---

<a id="newmember"></a>
### **Getting started as a new Crossref member**  
Date: Thursday, May 24, 2018    
Speaker: Rachael Lammey  
[Webinar Slides](https://www.slideshare.net/CrossRef/new-member-webinar-052418)  
[Webinar Recording](https://youtu.be/GAUEXizVcHw)  
{{< youtube GAUEXizVcHw css-class >}}    


---

<a id="referencelinking"></a>
### **Reference Linking**  
Date: Date: Wednesday, May 23, 2018   
Speaker: Patricia Feeney  
[Webinar Slides](https://www.slideshare.net/CrossRef/getting-started-with-reference-linking)  
[Webinar Recording](https://youtu.be/WvPhmRMCICg)  
{{< youtube WvPhmRMCICg css-class >}}   

---

<a id="crossmarkhowto"></a>
### **Crossmark how-to**  
Date: Tuesday, May 15, 2018  
Speaker: Kirsty Meddings  
[Webinar Slides](https://www.slideshare.net/CrossRef/cross-mark-webinar-how-to)  
[Webinar Recording](https://youtu.be/em0IVJf-UNo)   
{{< youtube em0IVJf-UNo css-class >}}   

---

<a id="metadata"></a>  
### **Maintaining your metadata**  
Date: Tuesday, April 24, 2018  
Speaker: Patricia Feeney  
[Webinar Slides](https://www.slideshare.net/CrossRef/maintaining-your-metadata-95497150)  
[Wistia recording](https://crossref.wistia.com/medias/bl0vdzzes1)  
[Webinar Recording](https://youtu.be/oR4d5YMPEFM)  
{{< youtube oR4d5YMPEFM css-class >}}  

---

<a id="simcheckportuguese"></a>
### **Introduction to Similarity Check - in Portuguese**  
Date: Tuesday, April 10, 2018    
Speaker: Rachael Lammey/Edilson Damasio    
[Webinar Slides](https://www.slideshare.net/CrossRef/similarity-check-portuguese)  
Q & A:  http://bit.ly/simcheck-portuguese  
[Webinar Recording](https://youtu.be/458YidNCJk4)  
{{< youtube 458YidNCJk4 css-class >}}   

---

<a id="metadatalookup"></a>  
### **Getting started with looking up metadata**  
Date: Thursday, March 8, 2018  
Speaker: Patricia Feeney  
[Webinar Slides](https://www.slideshare.net/CrossRef/getting-started-with-looking-up-metadata)  
[Webinar Recording](https://youtu.be/9_RrWFRHEbI)  
{{< youtube 9_RrWFRHEbI css-class >}}  

---  

<a id="contentregportuguese"></a>
### **Getting started with Content Registration: Portuguese Webinar**  
Date: Tuesday, September 5, 2017    
Speaker: Rachael Lammey/Edilson Damasio  
[Webinar Slides](https://www.slideshare.net/CrossRef/getting-started-with-content-registration-portuguese-webinar-introduo-ao-registro-de-contedo-webinar-em-portugus)  
[Webinar Recording](https://youtu.be/fY5ZxJHd17E)  
{{< youtube fY5ZxJHd17E css-class >}}    

---
<a id="citedbyhowto"></a>   
### **Crossref Cited-by how-to**   
Date: Tuesday, June 13, 2017  
Speaker: Patrica Feeney  
[Webinar Slides](https://www.slideshare.net/CrossRef/cited-by-howto)  
[Webinar Recording](https://youtu.be/tY7W2IWt7tE)  
{{< youtube tY7W2IWt7tE css-class >}}  

---  

<a id="openurl"></a>
### **Beyond OpenURL: Making the most of Crossref metadata**  
Date: Wednesday, July 12, 2017    
Speaker: Patricia Feeney   
[Webinar Slides](https://www.slideshare.net/CrossRef/beyond-openurl)  
[Webinar Recording](https://youtu.be/DmrFnLFu6iU)  
{{< youtube DmrFnLFu6iU css-class >}}   

---

<a id="citedby"></a>  
### **Crossref Cited-by**  
Date: Thursday, June 8. 2017  
Speaker: Anna Tolwinska  
[Webinar Slides](https://www.slideshare.net/CrossRef/crossref-cited-by)  
[Webinar Recording](https://youtu.be/31u6Iz_ENC8)  
{{< youtube 31u6Iz_ENC8 css-class >}}  

---

<a id="fundingfunder"></a>  
### **Funding data & the Funder Registry**  
Date: Tuesday, April 4, 2017  
Speaker: Patricia Feeney  
[Webinar Slides](https://www.slideshare.net/CrossRef/funding-data-and-funder-registry)  
[Webinar Recording](https://youtu.be/NyBufFcN2nA)  
{{< youtube NyBufFcN2nA css-class >}}   

---

<a id="preprints"></a>  
### **Preprints and scholarly infrastructure**  
Date: Monday, January 30, 2017   
Speaker: Rachael Lammey  
[Webinar Slides](http://www.slideshare.net/Crossref/preprints-scholarly-infrastructure-013017)  
[Webinar Recording](https://youtu.be/cFpuQ3lwpME)  
{{< youtube cFpuQ3lwpME css-class >}}  

---

<a id="crossmark"></a>
### **Crossmark update**  
Date: Thursday, February 23, 2017  
Speaker: Kirsty Meddings  
[Webinar Slides](https://www.slideshare.net/Crossref/crossmark-update-webinar)  
[Webinar Recording](https://youtu.be/qkwN49lgH9k)  
{{< youtube qkwN49lgH9k css-class >}}  

---

<a id="contentreg"></a>
### **Get Started with Content Registration**  
Date: Tuesday, October 17, 2017  
Speaker: Patricia Feeney  
[Webinar Slides](https://www.slideshare.net/CrossRef/getting-started-with-content-registration)  
[Webinar Recording](https://youtu.be/G9mIUvXrTLc)  
{{< youtube G9mIUvXrTLc css-class >}}  

---

<a id="introcrossmark"></a>
### **Introduction to Crossmark**  
Date: Tuesday, Nov 21, 2017  
Speaker: Kirsty Meddings    
[Webinar Slides](https://www.slideshare.net/CrossRef/introductiontocrossmarklastest)  
[Webinar Recording](https://youtu.be/mHMoQWp_cM4)  
{{< youtube mHMoQWp_cM4 css-class >}}  

---

<a id="introturkish"></a>
### **Introduction to Crossref: Turkish Seminar**  
Date: Thursday, Nov 2, 2017  
Speaker: Rachael Lammey/Prof. Dr. Serkan Eryilmaz
Translation: Dogan Kusmus   
[Webinar Recording](https://youtu.be/gc_MUolaBKc)  
{{< youtube gc_MUolaBKc css-class >}}  

---

<a id="simcheck"></a>
### **Similarity Check update**  
Date: Tuesday, September 20, 2016  
Speaker: Madeleine Watson  
[Webinar Slides](http://www.slideshare.net/Crossref/crossref-similarity-check-update-webinar-sept-2016)  

---

<a id="fundingportuguese"></a>
### **Funder Registry - in Portuguese**  
Date: Monday, September 26, 2016  
Speaker: Rachael Lammey  
[Webinar Slides](http://www.slideshare.net/Crossref/funding-data-orcid-webinar-in-portuguese)  

---

<a id="contentregistration"></a>
### **Content Registration maintaining metadata**
Date: Tuesday, May 17, 2017  
Speaker: Rachael Lammey  
[Webinar Slides](https://www.slideshare.net/CrossRef/content-registration-maintaining-metadata)  
[Webinar Recording](https://youtu.be/xwbghcX7wW0)
{{< youtube xwbghcX7wW0 css-class >}}

---

<a id="asia"></a>  
### **Asia Pacific community webinar**  
Date: Thursday, December 14, 2016  
[Webinar Slides](http://www.slideshare.net/Crossref/crossref-community-webinar-asia-pacific-12142016)  

---

<a id="turnitin"></a>
### **Crossref Similarity Check members update**
Date: Thursday, March 2  
Speaker: Gareth Malcolm, Turnitin  
[Webinar Recording (must register first to view)](https://event.on24.com/eventRegistration/EventLobbyServlet?target=reg20.jsp&referrer=&eventid=1371000&sessionid=1&key=B41D99989415D914443947C706FFED1F&regTag=&sourcepage=register)  


---

If you have a question or would like us to hold a webinar on another topic, please contact our [outreach team](mailto:feedback@crossref.org) with your ideas.
